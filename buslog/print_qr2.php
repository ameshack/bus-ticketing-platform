<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle; ?></title>
<link href="<?=base_url()?>css/style.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css"> 
@page {
  size: A6 landscape;
}

body {
    margin:0;
    width: auto;
    font-family:Verdana, Geneva, sans-serif; 
	size: A6 landscape;
	padding-left:5px;
	
	
}
#printwrapper {
	width:790px;
	display:block;
	font-size:12px;
	margin:0;
	
}
#nleftpane {
	width:250px;
	height:200px;
	padding:0px;
	float:left;
	border-left:1px solid #000;
	border-top:1px solid #000;
}
#nleftpane1 {
	width:180px;
	height:205px;
	padding:0px;
	float:left;
	border:1px solid #000;
}
#nleftpane2 {
	width:280px;
	height:100px;
	padding:0px;
	float:left;
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-right:1px solid #000;
}

#nleftpane3 {
	width:240px;
	height:105px;
	padding:5px;
	float:left;
	
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-bottom:1px solid #000;
}

#nleftpane5 {
	width:520px;
	height:105px;
	padding:5px;
	float:left;
	border:1px solid #000;
}

#nleftpane4 {
	width:432px;
	height:60px;
	padding:5px;
	float:left;
	border:1px solid #000;
}
.t90
{

/* Rotate div */

transform:rotate(-90deg);
-ms-transform:rotate(-90deg);  /*Internet Explorer */
-moz-transform:rotate(-90deg); /*Firefox */
-webkit-transform:rotate(-90deg);  /*Safari and Chrome*/
-o-transform:rotate(-90deg); /* Opera */
}
</style>
</head>
<body onLoad="window.print(); window.close();">
<br>
<center>
<br>
<p>&nbsp;</p><p>&nbsp;</p><br>
<div id="printwrapper">
<div id="nleftpane">
<img src="<?=base_url()?>images/qrs/<?=$pack->Pack_Tracking_No?>.jpg" align="absmiddle" width="245" height="185" /><br>
      <?="* ".$pack->Pack_Tracking_No." *"?>
</div>

<div id="nleftpane">
<font size="-1">
<strong>Sender: ID No: <?=$pack->Pack_S_IDNo?><br>
      <?=$pack->Cust_First_Name." ".$pack->Cust_Last_Name ?><br>
      <?=$pack->Cust_Street_Address?><br>
      <?=$pack->Cust_Postal_Address?><br>
      Contact: <?=$pack->Cust_First_Name." ".$pack->Cust_Last_Name ?><br>
      Tel: <?="+".$pack->Cust_Mobile_No?><br><br>
      <?=$pack->Pack_from?>
      </p>
      <br>
      Sender Ref No: 
      <?=$pack->Pack_Reference_No?>
</strong></font></div>

<div id="nleftpane2"><font size="-1">
<strong>Delivery Address:</strong> <br>
    <strong>
    <?=$pack->Rec_First_Name." ".$pack->Rec_Last_Name?>
    <br>
    <?=$pack->Rec_Street_Address." ".$pack->Rec_City?><br>
    Contact: <?=$pack->Rec_First_Name." ".$pack->Rec_Last_Name?><br>
    Tel: <?="+".$pack->Rec_Mobile_No?>
    </strong><br>
    <?=$pack->Pack_to?>
      </font>
</div>

<div id="nleftpane2"><font size="-1"><strong>Shipping Date:</strong> <strong>
  <?=$pack->Pack_sending_date?>
  <br>
    Description of Goods: 
    <?php
    $packdescr = $this->main_model->get_package_description($pack->Pack_id);
	echo $packdescr->pack_descr;
	?><br>
    Package Type: 
    <?php
    $packtypez = $this->main_model->get_package_typez($pack->Pack_id);
	echo $packtypez->pack_type;
	?><br>
    Services &amp; Options</strong><br>
36 hours SLA <strong></strong></font></div>



<div id="nleftpane3">
  <h3><strong>No of Pieces </strong><br>
    <strong>
      <?=$pack->Pack_Quantity?>
      </strong><br>
    <strong>Consignment Weight</strong><br>
    <strong>
      <?php
    $weighttot = $this->main_model->get_package_weight_total($pack->Pack_id);
	echo $weighttot->PD_Weight." kgs";
	?>
    </strong></h3>
</div>



<div id="nleftpane5">
 <br>
  <p><font color="#000000" size="-2"><strong>MCCL's LIABILITY FOR LOSS, DAMAGE AND DELAY IS LIMITED BY OUR TERMS AND CONDITIONS. THE SENDER AGREES THAT THE GENERAL CONDITIONS ARE ACCEPTABLE AND GOVERN THIS CONTRACT.</strong></font></p>
</div>
</div>
</center>
</body>
</html>