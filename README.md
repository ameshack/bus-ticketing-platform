Web Ticketing & Reservation System – back-end internal facing

-Includes multiple user names, permissions, access points for viewing/entering/changing data and reports
-Will allow Bus companies to set prices, number of seats, dates of travel, routes, etc. for
-Bus companies ticketing offices and website to sell tickets in real-time 
-Bus companies employees
-Bus companies sub-contractors
-Allows price discrimination for weekends, peak and off peak days and times, and the ability to change price for future dates 
-Tickets can be booked round-trip with return tickets 



Bus scheduling:

-resetting tickets to another date or time with set penalty amounts which can be overwritten by managers.
-Arrival of bus at destination – closing comments from managers who have checked the bus after arrival.
-Bus manifest
-Bus maintenance including alerts when maintenance is due 
-Route set up
-Bus fare set up including price discrimination based on date of travel
-Ticketing back office
-Banking
-Petty Cash
-Open Bookings
-Shift Cash Transfer
-Booking more than 1 ticket at a time
-Loyalty system for frequent travelers 

Reporting: 
-Average cost per ticket per bus, route, branch, etc.
-The mode should be shown. Which seats are sold more?
-At what time are most tickets sold
-Each ticket shows the time and date it was issued.
-The most tickets sold by a clerk in a branch either per day, week or month.
-Printing of tickets from system via loading points and via new ticketing systems 
-Display of seat selection, buses and routes at booking offices on monitor displays Online PC Web Bus Booking & Payment System - customer facing
-Enables customers to book and pay online via their PC via PayPal, credit cards, Bus companies gift cards/vouchers, and mobile money solutions 
-Allows users to check their loyalty points online 
-Loyalty + Gift couponing system
