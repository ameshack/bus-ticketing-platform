<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tms extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('membership_model');
		$this->auth = new stdClass;
		$this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');  
		$this->load->library('flexi_auth');
			}

	public function index()
	{
		$this->load->view('home');
	}
	function verifylogin()
	{	
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
   
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('home');
		}
		else 
		{
     //Go to admin area
		redirect('admins/members_area');
		}
}
 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->membership_model->login($username, $password);

   if($result)
   {
     foreach($result as $row)
     {
	 $data = array(
         'id' => $row->AdminId,
         'name' => $row->AdminName,
		 'is_logged_in' => true
			);
	   
       $this->session->set_userdata($data);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }

}
