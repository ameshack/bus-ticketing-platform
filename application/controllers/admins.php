<?php
ini_set('allow_url_fopen', 1);
ini_set('memory_limit', '-1');
session_start();
class Admins extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		//$this->is_logged_in();
		$this->load->model('bus_model');
		$this->auth = new stdClass;
		$this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');  		$this->load->library('flexi_auth');
		if ( $this->flexi_auth->is_logged_in()) { 
		$this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$this->name= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
		}

		date_default_timezone_set("Africa/Nairobi"); 
		
		$this->dtime = date('Y-m-d H:i:s');
                //local time zone

	}
	function index()	
	{
		redirect('index.php');		
	}
	function members_area()
	{
            
	if ($this->flexi_auth->is_logged_in()){
	    $this->data['message'] = $this->session->flashdata('message');
		$data['name']=$this->name;
		$data['dtime']=$this->dtime;
                $this->data['messages'] = $this->bus_model->getmessagesnotice();
                $this->data['notifications'] = $this->bus_model->notifications();
		$this->load->view('admin_home',$this->data);
				}else {
                 redirect('auth','refresh');
            }
	}
	
	function getRoute(){
        $data['name']=$this->bus_model->get_name_by_code($this->input->post('routeid'));
		
		$route=json_encode($data['name']);
		
		echo $route;
    } 
		function getTown(){
        $towns=$this->bus_model->get_town_by_id($this->input->post('townid'));
		$town=json_encode($towns);
		$this->session->set_userdata($town);
		echo $town;
    } 
			function seat_list($idq)
	{	
	    if ($this->flexi_auth->is_logged_in()){
		$bq = explode(".", $idq);
        $id = $bq[0];
        $data['quan'] = $bq[1];
        $townf = $bq[2];
        $townt = $bq[3];
        $data['bookeds'] = $this->bus_model->getBusSchedule($id);
        //echo $id; exit;
        
        $rt = $this->bus_model->getSpecBusSchedule($id);
        $bookedseatsf = $this->bus_model->getBookedSeats_t_f($id,$rt->From,$rt->To);
                $sbk = trim($bookedseatsf->booked,',');
                //echo $sbk; exit;
        $towns = explode(",",trim($rt->towns,','));
        $tcount = count($towns);
        $firsttown = array_search($townf, $towns);
        $secondtown = array_search($townt, $towns);
        
        
        $cm = "";
        $prevtowns = "0,";
        $aftowns = "0,";
        for ($i=0;$i<$tcount;$i++)
        {
            $townpos = array_search($towns[$i], $towns);
            if ($townpos < $secondtown)
            {
                $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$townt);
                $cm .=$bookedseats->booked.",";
            }
            if ($firsttown < $townpos)
            {
                $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$townf,$towns[$i]);
                $cm .=$bookedseats->booked.",";
                //echo $townf."-".$towns[$i]."<br>";
            }
            //$z = $i+1;
            //for ($z=$i+1;$z<$tcount;$z++)
            //{
                //$bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$towns[$z]);
                //$cm .=$bookedseats->booked.",";
            //}
            //strpos
            if (strpos(trim($rt->towns,','), $townf) > strpos(trim($rt->towns,','), $towns[$i]))
            {
                $prevtowns .=$towns[$i].',';
            }
            if (strpos(trim($rt->towns,','), $townt) < strpos(trim($rt->towns,','), $towns[$i]))
            {
                $aftowns .=$towns[$i].',';
            }
        }
        if ($prevtowns!="0," and $aftowns!="0,")
        {
            $pta = explode(",",trim($prevtowns,','));
            $ata = explode(",",trim($aftowns,','));
            for ($j=0;$j<count($pta);$j++)
            {
                for ($k=0;$k<count($ata);$k++)
                {
                   $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$pta[$j],$ata[$k]);
                    $cm .=$bookedseats->booked.","; 
                }
            }
            
        }
        for ($i=0;$i<$tcount;$i++)
        {
            
            $ptz = explode(",",trim($prevtowns,','));

            if ($prevtowns!="0,")
            {
                for ($k=0;$k<count($ptz);$k++)
                {
                    $townposz = array_search($ptz[$k], $towns);
                    $townposi = array_search($towns[$i], $towns);
                    //if (($townposi <> $firsttown))//$i=BUSIA, $k=MOMBASA
                    if (($townposi >= $firsttown) and ($townposi <> $firsttown))//$i=BUSIA, $k=MOMBASA
                    {
                        $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$ptz[$k],$towns[$i]);
                        $cm .=$bookedseats->booked.",";
                    }
                    //if ($townposi < $secondtown)//BUSIA-KAMPALA
                    //{
                        //$bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$ptz[$k]);
                        //$cm .=$bookedseats->booked.",";
                    //}

                }
            }
            if ($aftowns!="0,")
            {
                
            $atb = explode(",",trim($aftowns,','));
                for ($k=0;$k<count($atb);$k++)
                {
                    $townposz = array_search($atb[$k], $towns);
                    $townposi = array_search($towns[$i], $towns);
                    //if (($townposz < $firsttown) and ($townposi < $secondtown))//$i=BUSIA, $k=MOMBASA
                    //{
                        $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$atb[$k]);
                        $cm .=$bookedseats->booked.",";
                    //}
                    //if ($townposi < $secondtown)//BUSIA-KAMPALA
                    //{
                        //$bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$ptz[$k]);
                        //$cm .=$bookedseats->booked.",";
                    //}

                }
            }
        }
        
        //echo $cm;
                       
                        
        
		//$bookedseatsf = $this->bus_model->getBookedSeats($id);
                //$sbk = trim($bookedseatsf->booked,',');
                //$bookedseats = $this->bus_model->getBookedSeats_t_f($id,$townf,$townt);
		//echo $sbk; exit;
		$data['bookedseats']=$sbk.','.trim($cm,',');
                //$data['bookedseats']=$bookedseats->booked;
		
		//get station id from session
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $branchid = $this->user[0]->t_station;
		//$branchid=5;
	    $schedules = $this->bus_model->getBusSchedule($id);
		$data['rcode']= $schedules->RouteCode;
		$data['bustype']=$schedules->BusTypeId;
		$data['title']=''.$schedules->RouteCode.' '.$schedules->BusType.' CHART '.$schedules->DepartureDateTime.''; 
		$data['id'] = $id;
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($schedules->DepartureDateTime)));
                if ($ds<>NULL){
		$data['discountedseats'] = $ds->seat_values;
		$data['discountedamount'] = $ds->Disc_Amountke;
                }
                else {
                    $data['discountedseats'] = '';
		$data['discountedamount'] = 0;
                }
		$this->load->view('seat_list',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
			function seat_list2($id)
	{	
	    if ($this->flexi_auth->is_logged_in()){
                        $td= explode(".", $this->uri->segment('4'));
           		$returntownfid = $td[0];
                        $returntowntid = $td[1];
                        $seatid = $td[3];
                        $data['seatprice']=$td[4];
                        $data['quan']=$seatid;
						$id=$this->uri->segment('3');
                                                $data['scheduleid'] = $id;
                                                //$data['id'] = $id;
        //$data['bookeds'] = $this->bus_model->getBusSchedule($id);
		$bookedseats = $this->bus_model->getBookedSeats($id);
		
		//$data['bookedseats']=$bookedseats->booked;
                $rt = $this->bus_model->getSpecBusSchedule($id);
                $bookedseatsf = $this->bus_model->getBookedSeats_t_f($id,$returntownfid,$returntowntid);
                $sbk = trim($bookedseatsf->booked,',');
                $townf = $returntownfid;
                $townt = $returntowntid;
                $towns = explode(",",trim($rt->towns,','));
        $tcount = count($towns);
        $firsttown = array_search($townf, $towns);
        $secondtown = array_search($townt, $towns);
        
        
        $cm = "";
        $prevtowns = "0,";
        $aftowns = "0,";
        for ($i=0;$i<$tcount;$i++)
        {
            $townpos = array_search($towns[$i], $towns);
            if ($townpos < $secondtown)
            {
                $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$towns[$i],$townt);
                $cm .=$bookedseats->booked.",";
            }
            if ($firsttown < $townpos)
            {
                $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$townf,$towns[$i]);
                $cm .=$bookedseats->booked.",";
                //echo $townf."-".$towns[$i]."<br>";
            }
            //strpos
            if (strpos($rt->towns, $townf) > strpos($rt->towns, $towns[$i]))
            {
                $prevtowns .=$towns[$i].',';
            }
            if (strpos($rt->towns, $townt) < strpos($rt->towns, $towns[$i]))
            {
                $aftowns .=$towns[$i].',';
            }
        }
        if ($prevtowns!="0," and $aftowns!="0,")
        {
            $pta = explode(",",trim($prevtowns,','));
            $ata = explode(",",trim($aftowns,','));
            for ($j=0;$j<count($pta);$j++)
            {
                for ($k=0;$k<count($ata);$k++)
                {
                   $bookedseats = $this->bus_model->getBookedSeats_t_f($id,$pta[$j],$ata[$k]);
                    $cm .=$bookedseats->booked.","; 
                }
            }
        }
		$data['bookedseats']=$sbk.','.trim($cm,',');
		
		//get station id from session
		$branchid=$returntownfid;
	    $schedules = $this->bus_model->getBusSchedule($id);
		$data['rcode']= $schedules->RouteCode;
		$data['bustype']=$schedules->BusTypeId;
		$data['title']=''.$schedules->RouteCode.' '.$schedules->BusType.' CHART '.$schedules->DepartureDateTime.''; 
		$data['id'] = $id;
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($schedules->DepartureDateTime)));
                if ($ds<>NULL){
		$data['discountedseats'] = $ds->seat_values;
		$data['discountedamount'] = $ds->Disc_Amountke;
                }
                else {
                    $data['discountedseats'] = '';
		$data['discountedamount'] = 0;
                }
                $data['rep'] = 1;
                        $data['townf'] = $returntownfid;
                        $data['townt'] = $returntowntid;
		$this->load->view('seat_list2',$data);
		}else {
                 redirect('auth','refresh');
            }
	}

function cloneregion($quantityscheduleid)
	{
    $qs = explode(".", $quantityscheduleid);
    $quantity = $qs[0];
    $scheduleid = $qs[1];
    $townf = $qs[2];
    $townt = $qs[3];
	$seatarray1 = $qs[4];
	$seatarray=explode(',',$seatarray1);
	$seatcla=implode(",",$seatarray);
	//$seatarray=explode('',$seatcla);
	$towns = $this->bus_model->getplainresults('towns_lkp');
                $seatclass = $this->bus_model->getplainresults('seats_lkp');
                $nationalities = $this->bus_model->getcountries('countrylist');
				//get routecode,bustype to help get seatprice
				$sched = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $scheduleid);
				$buslookup=$sched->BusTypeLkp;
				$routelookup=$sched->RouteLkp;
				$datetime=$sched->DepartureDateTime;
				//get parameters for return ticket
				$returndetails=$this->bus_model->getBusSchedule($scheduleid);
				$datetime=$returndetails->DepartureDateTime;
				$dateonly=date('Y-m-d',strtotime($datetime));
				$fromstation=$returndetails->To;
                                $tostation=$returndetails->From;
				$farecode = $this->bus_model->getonerowfromonetbl('bus_routes', 'RouteId ', $routelookup);
				$routecde =$farecode->RouteCode;
	    if ($this->flexi_auth->is_logged_in())
          {
               //echo $quantity;
              $html = "";
			  $html.="<input type=\"hidden\" name=\"totalselected\" id=\"totalselected\" value=\"".$quantity."\" > ";
			for($i=1;$i<=$quantity;$i++)
			 {
			 $seatcla=explode("-",$seatarray[$i-1]);
			 $seatno=$seatcla[0];
			 $seatclas=$seatcla[1];
			// print_r($seatarray[$i-1]);
			 //get seat price
			 $seatprice=$this->bus_model->routeseatprice($buslookup,$seatclas,$townf,$townt); 
                         //get seat price return
                         $seatpricereturn=$this->bus_model->routeseatprice($buslookup,$seatclas,$townt,$townf);
			 //print_r($seatprice);
			 //get discount
		$branchid=$townf;
                
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($datetime)));
                if ($ds==NULL){
                    $discountamount = 0;
		$discountedseats = 0;
                }
                else {
                     $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
                        if ($cur==1):
                    $discountamount = $ds->Disc_Amountke;
                endif; if ($cur==2):
                    $discountamount = $ds->Disc_Amountug;
                endif; if ($cur==3):
                    $discountamount = $ds->Disc_Amounttz;
                endif;
		$discountedseats = $ds->seat_values;
                }
		
		$discounted=explode(',',$discountedseats);
		if (in_array($seatno, $discounted)) {
		$seatprice = $seatprice - $discountamount;
		$discountamount=$discountamount;
		} 
			 
$html .= "</div><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"table1\">
          <tr>
          <td>Seat Number:</td>
          <td><span class=\"red\">";
$html .= form_error('seatno'.$i);
$html .= "</span>
           <input type=\"text\" name=\"seatno".$i."\" id=\"seatno".$i."\" value=\"".$seatno."\" readonly  onChange=\"sumcost()\" ></td>
          <td>Seat Class:</td>
          <td><span class=\"red\">".form_error('seatclass'.$i)."</span><select name=\"seatclass".$i."\" id=\"seatclass".$i."\" class=\"smallselect\"  onfocus=\"this.defaultIndex=this.selectedIndex;\" onchange=\"this.selectedIndex=this.defaultIndex;\">";
 foreach($seatclass as $s){
     $html .= "
	               <option value=\"".$s->SeatTypeId."\"";
                  if($s->SeatTypeId==$seatclas):$html .= 'selected';endif; 
                  $html .= ">".$s->SeatType."</option>
              "; } 
               $html .= "
          </select></td>
        </tr>
        <tr>
          <td style=\"font-weight:bolder;\">Traveller's Name:</td>
          <td><span class=\"red\">".form_error('clientname'.$i)."</span><input type=\"text\" name=\"clientname".$i."\" id=\"clientname".$i."\" value=\"".$this->input->post('clientname')."\" />";
                       if ($i=="1" and $quantity>1)
                       {
                           $html .= "Copy Details? <select name=\"copydets\" class=\"smallselect\" onchange=\"copydetails(this.value);\">
              <option selected=\"selected\" value=\"1\">No</option><option value=\"2\">Yes</option></select>";
                       }
                       $html .= "</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style=\"font-weight:bolder;\">Id/Passport Number:</td>
          <td><span class=\"red\">".form_error('identity'.$i)."</span><input type=\"text\" name=\"identity".$i."\" id=\"identity".$i."\" value=\"".$this->input->post('identity')."\" /></td>
          <td>Nationality:</td>
          <td><span class=\"red\">".form_error('nationality'.$i)."</span><select name=\"nationality".$i."\" id=\"nationality".$i."\" class=\"smallselect\">
              
			 ";
              foreach($nationalities as $s){ 
                  $html .= "
              <option value=\"".$s->Name."\"";
                  if($s->Name==1):$html .= 'selected';endif; 
                  $html .= ">".$s->Name."</option>
              "; } 
              $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Has Smart Card:</td>
          <td><label>
              <input type=\"radio\" name=\"smartcard".$i."\" value=\"Yes\" id=\"smartcard".$i."\" onClick=\"showcardBox".$i."()\" />
              Yes</label>
            <br />
            <label>
              <input name=\"smartcard".$i."\" type=\"radio\" id=\"smartcard".$i."\" onChange=\"hidecardBox".$i."()\" onClick=\"hidecarddet".$i."()\" value=\"No\" checked=\"checked\"  />
              No <span class=\"wantcard".$i."\"><input type=\"checkbox\" name=\"addcard".$i."\" value=\"1\" onChange=\"addcardBox".$i."()\">Want a card?</span>
              <span class=\"addcarded".$i."\" style=\"display:none;\"><br>Collect station?<select name=\"addtown$i\" class=\"smallselect\">
              <option disabled=\"disabled\" selected=\"selected\">---select town---</option>";
               
			  
			  foreach($towns as $town){
                              $tid = $town->TownId;
                              $tname = $town->TownName;
             $html .= " <option value=\"".$tid."\">".$tname."</option>";
                 }
              
        $html .= "  </select></span></label> 
<p class=\"swipecard".$i."\" style=\"display:none;\">Swipe Card                              
  <input type=\"text\" size=\"25\" name=\"carddetails".$i."\" id=\"carddetails".$i."\" class=\"smartcard".$i."\" onchange=\"getcardinfo".$i."(this.value);\"/><input type=\"button\" value=\"Ok\" onsubmit=\"return false;\" /></p></td>
          <td style=\"font-weight:bolder;\">Mobile Number :</td>
          <td><span class=\"red\">".form_error('mobile'.$i)."</span><input type=\"text\" name=\"mobile".$i."\" id=\"mobile".$i."\" value=\"".$this->input->post('mobile')."\" />
              <input type=\"hidden\" name=\"seatprice".$i."\" id=\"seatprice".$i."\" value=\"".$this->input->post('seatprice1'.$i)."\"></td>
        </tr>
		<tr>
          <td>Return Ticket:</td>
          <td><label>
              <input type=\"radio\" name=\"return".$i."\" value=\"Yes\" id=\"return_0".$i."\" onClick=\"showReturn".$i."()\" onchange=\"doreturnfigure()\" onchange=\"togglereturn(returncost".$i.");\" />
              Yes</label>
            <br />
            <label>
              <input name=\"return".$i."\" type=\"radio\" id=\"return_1".$i."\" onClick=\"hideReturn".$i."()\" onchange=\"undoreturnfigure()\" value=\"No\" checked=\"checked\" />
              No</label><p class=\"dateofreturn".$i."\" style=\"display:none\">Return Date:                             
  <input type=\"text\" class=\"returndates\" name=\"returndate".$i."\" id=\"returndate".$i."\" readonly onchange=\"getval".$i."(this);\" /><br />
Return Seat: <input type=\"text\" name=\"returnseat".$i."\" id=\"returnseat".$i."\" onclick=\"window.open('".base_url()."admins/listbuschedulesreturn/'+2.1.2013-04-23.2,'popup','width=400,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=400,top=30'); return false\" onChange=\"sumcost()\"/>";
 $atts = array(
              'width'      => '400',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
			//$html.=anchor_popup('admins/listbuschedulesreturn/'.$townt.'.'.$townf.'.'.$dateonly.'.'.$i.'.'.$seatprice, 'Book Return Seat', $atts);
                        $html.="";
$html .="</p></td>
          <td>Seat Cost Return</td>
          <td><span id=\"returncost".$i."\" style=\"display:block;\" ><input type=\"hidden\" readonly=\"readonly\" name=\"returncost".$i."\" id=\"returncost".$i."\" value=\"0\" ></span><input type=\"hidden\"  name=\"returnschedule".$i."\" id=\"returnschedule".$i."\" value=\"\" ></td>
        </tr>
		<tr><td>Seat Cost</td><td><input type=\"text\" readonly=\"readonly\" name=\"cost".$i."\" id=\"cost".$i."\" value=\"".$seatprice."\" onclick=\"alert(this.value)\" ><input type=\"hidden\" readonly=\"readonly\" name=\"onecost".$i."\" id=\"onecost".$i."\" value=\"".$seatprice."\" ></td><td>Discount</td><td>";if(in_array($seatno, $discounted)) { $html .= $discountamount;}else{$html .= '0';} $html.="</td></tr></table></div>";
              
       		 }
                 echo $html;
                 
		 }
	}

function cloneregionvoucher($quantityscheduleid)
	{
    $qs = explode(".", $quantityscheduleid);
    $quantity = $qs[0];
    $scheduleid = $qs[1];
    $townf = $qs[2];
    $townt = $qs[3];
	$seatarray1 = $qs[4];
	$ticketid = $qs[5];
	$seatarray=explode(',',$seatarray1);
	$seatcla=implode(",",$seatarray);
	//$seatarray=explode('',$seatcla);
        $td = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $ticketid);
        $data['ticketid'] = $ticketid;
	$towns = $this->bus_model->getplainresults('towns_lkp');
                $seatclass = $this->bus_model->getplainresults('seats_lkp');
                $nationalities = $this->bus_model->getcountries('countrylist');
				//get routecode,bustype to help get seatprice
				$sched = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $scheduleid);
				$buslookup=$sched->BusTypeLkp;
				$routelookup=$sched->RouteLkp;
				$datetime=$sched->DepartureDateTime;
				//get parameters for return ticket
				$returndetails=$this->bus_model->getBusSchedule($scheduleid);
				$datetime=$returndetails->DepartureDateTime;
				$dateonly=date('Y-m-d',strtotime($datetime));
				$fromstation=$returndetails->To;
                                $tostation=$returndetails->From;
				$farecode = $this->bus_model->getonerowfromonetbl('bus_routes', 'RouteId ', $routelookup);
				$routecde =$farecode->RouteCode;
	    if ($this->flexi_auth->is_logged_in())
          {
               //echo $quantity;
              $html = "";
			  $html.="<input type=\"hidden\" name=\"totalselected\" id=\"totalselected\" value=\"".$quantity."\" > ";
			for($i=1;$i<=$quantity;$i++)
			 {
			 $seatcla=explode("-",$seatarray[$i-1]);
			 $seatno=$seatcla[0];
			 $seatclas=$seatcla[1];
			// print_r($seatarray[$i-1]);
			 //get seat price
			 $seatprice=$this->bus_model->routeseatprice($buslookup,$seatclas,$townf,$townt); 
                         //get seat price return
                         $seatpricereturn=$this->bus_model->routeseatprice($buslookup,$seatclas,$townt,$townf);
			 //print_r($seatprice);
			 //get discount
		$branchid=$townf;
                
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($datetime)));
                if ($ds==NULL){
                    $discountamount = 0;
		$discountedseats = 0;
                }
                else {
                     $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
                        if ($cur==1):
                    $discountamount = $ds->Disc_Amountke;
                endif; if ($cur==2):
                    $discountamount = $ds->Disc_Amountug;
                endif; if ($cur==3):
                    $discountamount = $ds->Disc_Amounttz;
                endif;
		$discountedseats = $ds->seat_values;
                }
		
		$discounted=explode(',',$discountedseats);
		if (in_array($seatno, $discounted)) {
		$seatprice = $seatprice - $discountamount;
		$discountamount=$discountamount;
		} 
			 
$html .= "</div><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"table1\">
          <tr>
          <td>Seat Number:</td>
          <td><span class=\"red\">";
$html .= form_error('seatno'.$i);
$html .= "</span>
           <input type=\"text\" name=\"seatno".$i."\" id=\"seatno".$i."\" value=\"".$seatno."\" readonly  onChange=\"sumcost()\" ></td>
          <td>Seat Class:</td>
          <td><span class=\"red\">".form_error('seatclass'.$i)."</span><select name=\"seatclass".$i."\" id=\"seatclass".$i."\" class=\"smallselect\"  onfocus=\"this.defaultIndex=this.selectedIndex;\" onchange=\"this.selectedIndex=this.defaultIndex;\">";
 foreach($seatclass as $s){
     $html .= "
	               <option value=\"".$s->SeatTypeId."\"";
                  if($s->SeatTypeId==$seatclas):$html .= 'selected';endif; 
                  $html .= ">".$s->SeatType."</option>
              "; } 
               $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Traveller's Name:</td>
          <td><span class=\"red\">".form_error('clientname'.$i)."</span><input type=\"text\" name=\"clientname".$i."\" id=\"clientname".$i."\" value=\"".$td->clientname."\" /><select name=\"copydets\" class=\"smallselect\">
              <option selected=\"selected\" value=\"1\">No</option><option value=\"2\">Yes</option></select>";
                       if ($i=="1" and $quantity>1)
                       {
                           $html .= "Copy Details? <select name=\"copydets\" class=\"smallselect\" onchange=\"copydetails(this.value);\">
              <option selected=\"selected\" value=\"1\">No</option><option value=\"2\">Yes</option></select>";
                       }
                       $html .="</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Id/Passport Number:</td>
          <td><span class=\"red\">".form_error('identity'.$i)."</span><input type=\"text\" name=\"identity".$i."\" id=\"identity".$i."\" value=\"".$td->t_idno."\" /></td>
          <td>Nationality:</td>
          <td><span class=\"red\">".form_error('nationality'.$i)."</span><select name=\"nationality".$i."\" id=\"nationality".$i."\" class=\"smallselect\">
              
			 ";
              foreach($nationalities as $s){ 
                  $html .= "
              <option value=\"".$s->Name."\"";
                  if($s->Name==1):$html .= 'selected';endif; 
                  $html .= ">".$s->Name."</option>
              "; } 
              $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Has Smart Card:</td>
          <td><label>
              <input type=\"radio\" name=\"smartcard".$i."\" value=\"Yes\" id=\"smartcard".$i."\" onClick=\"showcardBox".$i."()\" />
              Yes</label>
            <br />
            <label>
              <input name=\"smartcard".$i."\" type=\"radio\" id=\"smartcard".$i."\" onChange=\"hidecardBox".$i."()\" onClick=\"hidecarddet".$i."()\" value=\"No\" checked=\"checked\"  />
              No <span class=\"wantcard".$i."\"><input type=\"checkbox\" name=\"addcard".$i."\" value=\"1\" onChange=\"addcardBox".$i."()\">Want a card?</span>
              <span class=\"addcarded".$i."\" style=\"display:none;\"><br>Collect station?<select name=\"addtown$i\" class=\"smallselect\">
              <option disabled=\"disabled\" selected=\"selected\">---select town---</option>";
               
			  
			  foreach($towns as $town){
                              $tid = $town->TownId;
                              $tname = $town->TownName;
             $html .= " <option value=\"".$tid."\">".$tname."</option>";
                 }
              
        $html .= "  </select></span></label> 
<p class=\"swipecard".$i."\" style=\"display:none;\">Swipe Card                              
  <input type=\"text\" size=\"25\" name=\"carddetails".$i."\" id=\"carddetails".$i."\" class=\"smartcard".$i."\" onchange=\"getcardinfo".$i."(this.value);\"/><input type=\"button\" value=\"Ok\" onsubmit=\"return false;\" /></p></td>
          <td>Mobile Number:</td>
          <td><span class=\"red\">".form_error('mobile'.$i)."</span><input type=\"text\" name=\"mobile".$i."\" id=\"mobile".$i."\" value=\"".$td->mobileno."\" />
              <input type=\"hidden\" name=\"seatprice".$i."\" id=\"seatprice".$i."\" value=\"".$this->input->post('seatprice1'.$i)."\"></td>
        </tr>
		<tr>
          <td>Return Ticket:</td>
          <td><label>
              <input type=\"radio\" name=\"return".$i."\" value=\"Yes\" id=\"return_0".$i."\" onClick=\"showReturn".$i."()\" onchange=\"doreturnfigure()\" onchange=\"togglereturn(returncost".$i.");\" />
              Yes</label>
            <br />
            <label>
              <input name=\"return".$i."\" type=\"radio\" id=\"return_1".$i."\" onClick=\"hideReturn".$i."()\" onchange=\"undoreturnfigure()\" value=\"No\" checked=\"checked\" />
              No</label><p class=\"dateofreturn".$i."\" style=\"display:none\">Return Date:                             
  <input type=\"text\" class=\"returndates\" name=\"returndate".$i."\" id=\"returndate".$i."\" readonly onchange=\"getval".$i."(this);\" /><br />
Return Seat: <input type=\"text\" name=\"returnseat".$i."\" id=\"returnseat".$i."\" onclick=\"window.open('".base_url()."admins/listbuschedulesreturn/'+2.1.2013-04-23.2,'popup','width=400,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=400,top=30'); return false\" onChange=\"sumcost()\"/>";
 $atts = array(
              'width'      => '400',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
			//$html.=anchor_popup('admins/listbuschedulesreturn/'.$townt.'.'.$townf.'.'.$dateonly.'.'.$i.'.'.$seatprice, 'Book Return Seat', $atts);
                        $html.="";
$html .="</p></td>
          <td>Seat Cost Return</td>
          <td><span id=\"returncost".$i."\" style=\"display:block;\" ><input type=\"hidden\" readonly=\"readonly\" name=\"returncost".$i."\" id=\"returncost".$i."\" value=\"0\" ></span><input type=\"hidden\"  name=\"returnschedule".$i."\" id=\"returnschedule".$i."\" value=\"\" ></td>
        </tr>
		<tr><td>Seat Cost</td><td><input type=\"text\" readonly=\"readonly\" name=\"cost".$i."\" id=\"cost".$i."\" value=\"".$seatprice."\" onclick=\"alert(this.value)\" ><input type=\"hidden\" readonly=\"readonly\" name=\"onecost".$i."\" id=\"onecost".$i."\" value=\"".$seatprice."\" ></td><td>Discount</td><td>";if(in_array($seatno, $discounted)) { $html .= $discountamount;}else{$html .= '0';} $html.="</td></tr></table></div>";
              
       		 }
                 echo $html;
                 
		 }
	}

function cloneregioncorp($quantityscheduleid)
	{
    $qs = explode(".", $quantityscheduleid);
    $quantity = $qs[0];
    $scheduleid = $qs[1];
    $townf = $qs[2];
    $townt = $qs[3];
	$seatarray1 = $qs[4];
	$seatarray=explode(',',$seatarray1);
	$seatcla=implode(",",$seatarray);
	//$seatarray=explode('',$seatcla);
	
                $seatclass = $this->bus_model->getplainresults('seats_lkp');
                $nationalities = $this->bus_model->getcountries('countrylist');
				//get routecode,bustype to help get seatprice
				$sched = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $scheduleid);
				$buslookup=$sched->BusTypeLkp;
				$routelookup=$sched->RouteLkp;
				$datetime=$sched->DepartureDateTime;
				//get parameters for return ticket
				$returndetails=$this->bus_model->getBusSchedule($scheduleid);
				$datetime=$returndetails->DepartureDateTime;
				$dateonly=date('Y-m-d',strtotime($datetime));
				$fromstation=$returndetails->To;
                                $tostation=$returndetails->From;
				$farecode = $this->bus_model->getonerowfromonetbl('bus_routes', 'RouteId ', $routelookup);
				$routecde =$farecode->RouteCode;
	    if ($this->flexi_auth->is_logged_in())
          {
               //echo $quantity;
              $html = "";
			  $html.="<input type=\"hidden\" name=\"totalselected\" id=\"totalselected\" value=\"".$quantity."\" > ";
			for($i=1;$i<=$quantity;$i++)
			 {
			 $seatcla=explode("-",$seatarray[$i-1]);
			 $seatno=$seatcla[0];
			 $seatclas=$seatcla[1];
			// print_r($seatarray[$i-1]);
			 //get seat price
			 $seatprice=$this->bus_model->routeseatprice($buslookup,$seatclas,$townf,$townt); 
                         //get seat price return
                         $seatpricereturn=$this->bus_model->routeseatprice($buslookup,$seatclas,$townt,$townf);
			 //print_r($seatprice);
			 //get discount
		$branchid=$townf;
                
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($datetime)));
                if ($ds==NULL){
                    $discountamount = 0;
		$discountedseats = 0;
                }
                else {
                     $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
                        if ($cur==1):
                    $discountamount = $ds->Disc_Amountke;
                endif; if ($cur==2):
                    $discountamount = $ds->Disc_Amountug;
                endif; if ($cur==3):
                    $discountamount = $ds->Disc_Amounttz;
                endif;
		$discountedseats = $ds->seat_values;
                }
		
		$discounted=explode(',',$discountedseats);
		if (in_array($seatno, $discounted)) {
		//$seatprice = $seatprice - $discountamount;
		$discountamount=$discountamount;
		} 
                 //$this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
						
                    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    $custd = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
                $seatprice = $seatprice - $custd->discountamount;
			 
$html .= "</div><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"table1\">
          <tr>
          <td>Seat Number:</td>
          <td><span class=\"red\">";
$html .= form_error('seatno'.$i);
$html .= "</span>
           <input type=\"text\" name=\"seatno".$i."\" id=\"seatno".$i."\" value=\"".$seatno."\" readonly  onChange=\"sumcost()\" ></td>
          <td>Seat Class:</td>
          <td><span class=\"red\">".form_error('seatclass'.$i)."</span><select name=\"seatclass".$i."\" id=\"seatclass".$i."\" class=\"smallselect\"  onfocus=\"this.defaultIndex=this.selectedIndex;\" onchange=\"this.selectedIndex=this.defaultIndex;\">";
 foreach($seatclass as $s){
     $html .= "
	               <option value=\"".$s->SeatTypeId."\"";
                  if($s->SeatTypeId==$seatclas):$html .= 'selected';endif; 
                  $html .= ">".$s->SeatType."</option>
              "; } 
               $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Traveller's Name:</td>
          <td><span class=\"red\">".form_error('clientname'.$i)."</span><input type=\"text\" name=\"clientname".$i."\" id=\"clientname".$i."\" value=\"".$this->input->post('clientname'.$i)."\" />";
                       if ($i=="1" and $quantity>1)
                       {
                           $html .= "Copy Details? <select name=\"copydets\" class=\"smallselect\" onchange=\"copydetails(this.value);\">
              <option selected=\"selected\" value=\"1\">No</option><option value=\"2\">Yes</option></select>";
                       }
                       $html .="</td>
          <td>Traveller Email</td>
          <td><span class=\"red\">".form_error('clientemail'.$i)."</span><input type=\"text\" name=\"clientemail".$i."\" id=\"clientemail".$i."\" value=\"".$this->input->post('clientemail'.$i)."\" /></td>
        </tr>
        <tr>
          <td>Id/Passport Number:</td>
          <td><span class=\"red\">".form_error('identity'.$i)."</span><input type=\"text\" name=\"identity".$i."\" id=\"identity".$i."\" value=\"".$this->input->post('identity')."\" /></td>
          <td>Nationality:</td>
          <td><span class=\"red\">".form_error('nationality'.$i)."</span><select name=\"nationality".$i."\" id=\"nationality".$i."\" class=\"smallselect\">
              
			 ";
              foreach($nationalities as $s){ 
                  $html .= "
              <option value=\"".$s->Name."\"";
                  if($s->Name==1):$html .= 'selected';endif; 
                  $html .= ">".$s->Name."</option>
              "; } 
              $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Has Smart Card:</td>
          <td><label>
              <input type=\"radio\" name=\"smartcard".$i."\" value=\"Yes\" id=\"smartcard".$i."\" onClick=\"showcardBox".$i."()\" />
              Yes</label>
            <br />
            <label>
              <input name=\"smartcard".$i."\" type=\"radio\" id=\"smartcard".$i."\" onChange=\"hidecardBox".$i."()\" onClick=\"hidecarddet".$i."()\" value=\"No\" checked=\"checked\"  />
              No</label><p class=\"swipecard".$i."\" style=\"display:none;\">Swipe Card                              
  <input type=\"text\" size=\"25\" name=\"carddetails".$i."\" id=\"carddetails".$i."\" class=\"smartcard".$i."\" onchange=\"getcardinfo".$i."(this.value);\"/><input type=\"button\" value=\"Ok\" onsubmit=\"return false;\" /></p></td>
          <td>Mobile Number:</td>
          <td><span class=\"red\">".form_error('mobile'.$i)."</span><input type=\"text\" name=\"mobile".$i."\" id=\"mobile".$i."\" value=\"".$this->input->post('mobile')."\" />
              <input type=\"hidden\" name=\"seatprice".$i."\" id=\"seatprice".$i."\" value=\"".$this->input->post('seatprice1'.$i)."\"></td>
        </tr>
		<tr>
          <td>Return Ticket:</td>
          <td><label>
              <input type=\"radio\" name=\"return".$i."\" value=\"Yes\" id=\"return_0".$i."\" onClick=\"showReturn".$i."()\" onchange=\"doreturnfigure()\" onchange=\"togglereturn(returncost".$i.");\" />
              Yes</label>
            <br />
            <label>
              <input name=\"return".$i."\" type=\"radio\" id=\"return_1".$i."\" onClick=\"hideReturn".$i."()\" onchange=\"undoreturnfigure()\" value=\"No\" checked=\"checked\" />
              No</label><p class=\"dateofreturn".$i."\" style=\"display:none\">Return Date:                             
  <input type=\"text\" class=\"returndates\" name=\"returndate".$i."\" id=\"returndate".$i."\" readonly onchange=\"getval".$i."(this);\" /><br />
Return Seat: <input type=\"text\" name=\"returnseat".$i."\" id=\"returnseat".$i."\" onclick=\"window.open('".base_url()."admins/listbuschedulesreturn/'+2.1.2013-04-23.2,'popup','width=400,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=400,top=30'); return false\" onChange=\"sumcost()\"/>";
 $atts = array(
              'width'      => '400',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
			//$html.=anchor_popup('admins/listbuschedulesreturn/'.$townt.'.'.$townf.'.'.$dateonly.'.'.$i.'.'.$seatprice, 'Book Return Seat', $atts);
                        $html.="";
$html .="</p></td>
          <td>SMS Ticket?</td>
          <td>
          <input type=\"checkbox\" name=\"smssend".$i."\" id=\"smssend".$i."\" value=\"5\" onchange=\"CHeck".$i."();\" />
          Check this to receive ticket details via sms at cost of ksh 5
          
<span id=\"returncost".$i."\" style=\"display:block;\" ><input type=\"hidden\" readonly=\"readonly\" name=\"returncost".$i."\" id=\"returncost".$i."\" value=\"0\" ></span><input type=\"hidden\"  name=\"returnschedule".$i."\" id=\"returnschedule".$i."\" value=\"\" ></td>
        </tr>
		<tr><td>Seat Cost</td><td><input type=\"text\" readonly=\"readonly\" name=\"cost".$i."\" id=\"cost".$i."\" value=\"".$seatprice."\" onclick=\"alert(this.value)\" ><input type=\"hidden\" readonly=\"readonly\" name=\"onecost".$i."\" id=\"onecost".$i."\" value=\"".$seatprice."\" ></td><td></td><td></td></tr></table></div>";
              
       		 }
                 echo $html;
                 
		 }
	}

	  function crud_ticketing($scheduleidtownftownt)
	{	
		if ($this->flexi_auth->is_logged_in()){
                    $sft = explode(":", $scheduleidtownftownt);
                    $scheduleid = $sft[0];
                    $townf = $sft[1];
                    $townt = $sft[2];
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
				$data['schedules'] = $this->bus_model->getBusSchedule($scheduleid);
                                $data['townfdz'] = $townf;
                                $data['towntdz'] = $townt;
		//print_r($data['schedules']);
     	$this->load->view('manage_ticketing',$data);
				}else {
                 redirect('auth','refresh');
            }
	}

	  function crud_ticketing_conf($scheduleidtownftowntticketid)
	{	
		if ($this->flexi_auth->is_logged_in()){
                    $sft = explode(":", $scheduleidtownftowntticketid);
                    $scheduleid = $sft[0];
                    $townf = $sft[1];
                    $townt = $sft[2];
                    $ticketid = $sft[3];
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
				$data['schedules'] = $this->bus_model->getBusSchedule($scheduleid);
                                $data['townfdz'] = $townf;
                                $data['towntdz'] = $townt;
                                $data['ticketid'] = $ticketid;
                                $data['html'] = $this->confclone($ticketid);
		//print_r($data['schedules']);
     	$this->load->view('manage_ticketing_conf',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function confclone($ticketid)
        {
            //$qs = explode(".", $quantityscheduleid);
    $quantity = 1;
    
     $td = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $ticketid);
    $scheduleid = $td->schedule_id;
    $townf = $td->fromtown;
    $townt = $td->totown;
    $seatno = $td->seatno;
    $seatclas = $td->seat_class;
    $travellername = $td->clientname;
    $travellerid = $td->t_idno;
    $travellermobile = $td->mobileno;
    
	//$seatarray1 = $qs[4];
	//$seatarray=explode(',',$seatarray1);
	//$seatcla=implode(",",$seatarray);
	//$seatarray=explode('',$seatcla);
	$towns = $this->bus_model->getplainresults('towns_lkp');
                $seatclass = $this->bus_model->getplainresults('seats_lkp');
                $nationalities = $this->bus_model->getcountries('countrylist');
				//get routecode,bustype to help get seatprice
				$sched = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $scheduleid);
				$buslookup=$sched->BusTypeLkp;
				$routelookup=$sched->RouteLkp;
				$datetime=$sched->DepartureDateTime;
				//get parameters for return ticket
				$returndetails=$this->bus_model->getBusSchedule($scheduleid);
				$datetime=$returndetails->DepartureDateTime;
				$dateonly=date('Y-m-d',strtotime($datetime));
				$fromstation=$returndetails->To;
                                $tostation=$returndetails->From;
				$farecode = $this->bus_model->getonerowfromonetbl('bus_routes', 'RouteId ', $routelookup);
				$routecde =$farecode->RouteCode;
	    if ($this->flexi_auth->is_logged_in())
          {
               //echo $quantity;
              $html = "";
			  $html.="<input type=\"hidden\" name=\"totalselected\" id=\"totalselected\" value=\"".$quantity."\" > ";
			for($i=1;$i<=$quantity;$i++)
			 {
			 //$seatcla=explode("-",$seatarray[$i-1]);
			 //$seatno=$seatcla[0];
			 //$seatclas=$seatcla[1];
			// print_r($seatarray[$i-1]);
			 //get seat price
			 $seatprice=$this->bus_model->routeseatprice($buslookup,$seatclas,$townf,$townt); 
                         //get seat price return
                         $seatpricereturn=$this->bus_model->routeseatprice($buslookup,$seatclas,$townt,$townf);
			 //print_r($seatprice);
			 //get discount
		$branchid=$townf;
                
		$ds= $this->bus_model->getdiscountedseats($branchid,date('Y-m-d',strtotime($datetime)));
                if ($ds==NULL){
                    $discountamount = 0;
		$discountedseats = 0;
                }
                else {
                     $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
                        if ($cur==1):
                    $discountamount = $ds->Disc_Amountke;
                endif; if ($cur==2):
                    $discountamount = $ds->Disc_Amountug;
                endif; if ($cur==3):
                    $discountamount = $ds->Disc_Amounttz;
                endif;
		$discountedseats = $ds->seat_values;
                }
		
		$discounted=explode(',',$discountedseats);
		if (in_array($seatno, $discounted)) {
		$seatprice = $seatprice - $discountamount;
		$discountamount=$discountamount;
		} 
			 
$html .= "</div><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"table1\">
          <tr>
          <td>Seat Number:</td>
          <td><span class=\"red\">";
$html .= form_error('seatno'.$i);
$html .= "</span>
           <input type=\"text\" name=\"seatno".$i."\" id=\"seatno".$i."\" value=\"".$seatno."\" readonly  onChange=\"sumcost()\" ></td>
          <td>Seat Class:</td>
          <td><span class=\"red\">".form_error('seatclass'.$i)."</span><select name=\"seatclass".$i."\" id=\"seatclass".$i."\" class=\"smallselect\"  onfocus=\"this.defaultIndex=this.selectedIndex;\" onchange=\"this.selectedIndex=this.defaultIndex;\">";
 foreach($seatclass as $s){
     $html .= "
	               <option value=\"".$s->SeatTypeId."\"";
                  if($s->SeatTypeId==$seatclas):$html .= 'selected';endif; 
                  $html .= ">".$s->SeatType."</option>
              "; } 
               $html .= "
          </select></td>
        </tr>
        <tr>
          <td style=\"font-weight:bolder;\">Traveller's Name:</td>
          <td><span class=\"red\">".form_error('clientname'.$i)."</span><input type=\"text\" name=\"clientname".$i."\" id=\"clientname".$i."\" value=\"".$travellername."\" />";
                       if ($i=="1" and $quantity>1)
                       {
                           $html .= "Copy Details? <select name=\"copydets\" class=\"smallselect\" onchange=\"copydetails(this.value);\">
              <option selected=\"selected\" value=\"1\">No</option><option value=\"2\">Yes</option></select>";
                       }
                       $html .= "</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style=\"font-weight:bolder;\">Id/Passport Number:</td>
          <td><span class=\"red\">".form_error('identity'.$i)."</span><input type=\"text\" name=\"identity".$i."\" id=\"identity".$i."\" value=\"".$travellerid."\" /></td>
          <td>Nationality:</td>
          <td><span class=\"red\">".form_error('nationality'.$i)."</span><select name=\"nationality".$i."\" id=\"nationality".$i."\" class=\"smallselect\">
              
			 ";
              foreach($nationalities as $s){ 
                  $html .= "
              <option value=\"".$s->Name."\"";
                  if($s->Name==1):$html .= 'selected';endif; 
                  $html .= ">".$s->Name."</option>
              "; } 
              $html .= "
          </select></td>
        </tr>
        <tr>
          <td>Has Smart Card:</td>
          <td><label>
              <input type=\"radio\" name=\"smartcard".$i."\" value=\"Yes\" id=\"smartcard".$i."\" onClick=\"showcardBox".$i."()\" />
              Yes</label>
            <br />
            <label>
              <input name=\"smartcard".$i."\" type=\"radio\" id=\"smartcard".$i."\" onChange=\"hidecardBox".$i."()\" onClick=\"hidecarddet".$i."()\" value=\"No\" checked=\"checked\"  />
              No <span class=\"wantcard".$i."\"><input type=\"checkbox\" name=\"addcard".$i."\" value=\"1\" onChange=\"addcardBox".$i."()\">Want a card?</span>
              <span class=\"addcarded".$i."\" style=\"display:none;\"><br>Collect station?<select name=\"addtown$i\" class=\"smallselect\">
              <option disabled=\"disabled\" selected=\"selected\">---select town---</option>";
               
			  
			  foreach($towns as $town){
                              $tid = $town->TownId;
                              $tname = $town->TownName;
             $html .= " <option value=\"".$tid."\">".$tname."</option>";
                 }
              
        $html .= "  </select></span></label> 
<p class=\"swipecard".$i."\" style=\"display:none;\">Swipe Card                              
  <input type=\"text\" size=\"25\" name=\"carddetails".$i."\" id=\"carddetails".$i."\" class=\"smartcard".$i."\" onchange=\"getcardinfo".$i."(this.value);\"/><input type=\"button\" value=\"Ok\" onsubmit=\"return false;\" /></p></td>
          <td style=\"font-weight:bolder;\">Mobile Number :</td>
          <td><span class=\"red\">".form_error('mobile'.$i)."</span><input type=\"text\" name=\"mobile".$i."\" id=\"mobile".$i."\" value=\"".$travellermobile."\" />
              <input type=\"hidden\" name=\"seatprice".$i."\" id=\"seatprice".$i."\" value=\"".$this->input->post('seatprice1'.$i)."\"></td>
        </tr>
		<tr>
          <td>Return Ticket:</td>
          <td><label>
              <input type=\"radio\" name=\"return".$i."\" value=\"Yes\" id=\"return_0".$i."\" onClick=\"showReturn".$i."()\" onchange=\"doreturnfigure()\" onchange=\"togglereturn(returncost".$i.");\" />
              Yes</label>
            <br />
            <label>
              <input name=\"return".$i."\" type=\"radio\" id=\"return_1".$i."\" onClick=\"hideReturn".$i."()\" onchange=\"undoreturnfigure()\" value=\"No\" checked=\"checked\" />
              No</label><p class=\"dateofreturn".$i."\" style=\"display:none\">Return Date:                             
  <input type=\"text\" class=\"returndates\" name=\"returndate".$i."\" id=\"returndate".$i."\" readonly onchange=\"getval".$i."(this);\" /><br />
Return Seat: <input type=\"text\" name=\"returnseat".$i."\" id=\"returnseat".$i."\" onclick=\"window.open('".base_url()."admins/listbuschedulesreturn/'+2.1.2013-04-23.2,'popup','width=400,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=400,top=30'); return false\" onChange=\"sumcost()\"/>";
 $atts = array(
              'width'      => '400',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
			//$html.=anchor_popup('admins/listbuschedulesreturn/'.$townt.'.'.$townf.'.'.$dateonly.'.'.$i.'.'.$seatprice, 'Book Return Seat', $atts);
                        $html.="";
$html .="</p></td>
          <td>Seat Cost Return</td>
          <td><span id=\"returncost".$i."\" style=\"display:block;\" ><input type=\"hidden\" readonly=\"readonly\" name=\"returncost".$i."\" id=\"returncost".$i."\" value=\"0\" ></span><input type=\"hidden\"  name=\"returnschedule".$i."\" id=\"returnschedule".$i."\" value=\"\" ></td>
        </tr>
		<tr><td>Seat Cost</td><td><input type=\"text\" readonly=\"readonly\" name=\"cost".$i."\" id=\"cost".$i."\" value=\"".$seatprice."\" onclick=\"alert(this.value)\" ><input type=\"hidden\" readonly=\"readonly\" name=\"onecost".$i."\" id=\"onecost".$i."\" value=\"".$seatprice."\" ></td><td>Discount</td><td>";if(in_array($seatno, $discounted)) { $html .= $discountamount;}else{$html .= '0';} $html.="</td></tr></table></div>";
              
       		 }
                 return $html;
                 
		 }
        }
                	  function crud_ticketing_voucher($scheduleidtownftownttickid)
	{	
		if ($this->flexi_auth->is_logged_in()){
                    $sft = explode(":", $scheduleidtownftownttickid);
                    $scheduleid = $sft[0];
                    $townf = $sft[1];
                    $townt = $sft[2];
                    $data['ticketid'] = $sft[3];
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
				$data['schedules'] = $this->bus_model->getBusSchedule($scheduleid);
                                $data['townfdz'] = $townf;
                                $data['towntdz'] = $townt;
		//print_r($data['schedules']);
     	$this->load->view('manage_ticketing_voucher',$data);
				}else {
                 redirect('auth','refresh');
            }
	}

	  function crud_ticketing_corp($scheduleidtownftownt)
	{	
		if ($this->flexi_auth->is_logged_in()){
                    $sft = explode(":", $scheduleidtownftownt);
                    $scheduleid = $sft[0];
                    $townf = $sft[1];
                    $townt = $sft[2];
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
				$data['schedules'] = $this->bus_model->getBusSchedule($scheduleid);
                                $data['townfdz'] = $townf;
                                $data['towntdz'] = $townt;
		//print_r($data['schedules']);
     	$this->load->view('manage_ticketing_corp',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
	  function xmlschedule($datettownfidtowntid)
	{
              
			  $dft = explode(":",$datettownfidtowntid);
                $townfid = $dft[1];
                 $towntid = $dft[2];
                $datet = $dft[0];
		$data['schedules'] = $this->bus_model->getBusSchedulesbyStation($datet,$townfid,$towntid);
		$data['datettownfidtowntid']=$datettownfidtowntid;
		     	$this->load->view('xml/schedules',$data);
	}
	function js()
	{
		echo '{"buslist":[{"busid":"3","busname":"Oxygen","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"},{"busid":"2","busname":"Normal","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"},{"busid":"3","busname":"Oxygen","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"},{"busid":"2","busname":"Normal","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"},{"busid":"3","busname":"Oxygen","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"},{"busid":"2","busname":"Normal","routecode":"NRB-MSA11:00PM","reptime":"21:30","deptime":"21:30","vipseats":"4,1","fclassseats":"6,5,3,2","bizseats":"8,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45","vipprice":"1600.00","fclassprice":"1400.00","bizprice":"1200.00"}]}';
	}
	  function schedulesjson($datettownfidtowntid)
	{
		$dft=$datettownfidtowntid;
$dft1 = explode(":",$dft);
                $townfname = $dft1[0];
                 $towntto = $dft1[1];
                $datet = date('Y-m-d',strtotime($dft1[2]));
//$townfid=$this->bus_model->getfieldvaluefromrow('towns_lkp','TownId','TownName',$townfname); 
//$towntid=$this->bus_model->getfieldvaluefromrow('towns_lkp','TownId','TownName',$towntto);
$tfid=$this->bus_model->getonerowfromonetbl_array('towns_lkp','TownName',$townfname); 
$townfid = $tfid['TownId'];
$ttid=$this->bus_model->getonerowfromonetbl_array('towns_lkp','TownName',$towntto);
$towntid = $ttid['TownId'];
$datettownfidtowntid=$datet.":".$townfid.":".$towntid;
		$schedules = $this->bus_model->getBusSchedulesbyStationrr($datet,$townfid,$towntid);
		/*$townid=2;//nairobi station
		$date=date('m/d/Y H:i');
		//print_r($schedules);
		//$schedules = $this->bus_model->getBusSchedulesbyStation($townid,$to,$from,$date);*/
		
		//$schedules = $this->bus_model->getBusSchedulesbyStation($datet,$townfid,$towntid);
		//print_r($schedules);
		$townfromobj=$this->bus_model->getonerowfromonetbl_array('towns_lkp','TownId', $townfid);
		$currency=$townfromobj['Currency'];
		if($currency==3){$amount_column='TzAmount'; $curr='TZS';} elseif($currency==2) {$amount_column='UgAmount'; $curr='UGX';}else{$amount_column='KeAmount'; $curr='KES';}
	//	header ("content-type: text/xml");
	//$xml = '';
		 $xml = "[";   
        if ($schedules<>NULL)
        {
                foreach ($schedules as $p)
                {
                    $totbooked = $this->bus_model->getBusSchedulestotseats($p['ScheduleId'],$townfid,$towntid);
                    if (($p['BusType']=="Oxygen" && $totbooked < 47) || ($p['BusType']=="Normal" && $totbooked < 45) || ($p['BusType']=="Irizar" && $totbooked < 49) || ($p['BusType']=="Minibus" && $totbooked < 33))
                    
                    {//$bustype = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$p->BusTypeLkp);
                        $towns = explode(",",$p['towns']);
                        $firsttown = array_search($townfid, $towns); 
                        $secondtown = array_search($towntid, $towns);
                        
                        $twns = $this->bus_model->getscheduletimes($p['RouteLkp'],$townfid);
				$deptime = date('g:i A',strtotime($twns->ArrivalDeparture));
				$reptime = date('g:i A',strtotime($twns->ArrivalDeparture. ' - 30 minute'));
                        if ($secondtown>$firsttown) //check order of towns in route
                        {
                            
                            $ftm = $this->bus_model->gettownsinroutes_first($p['RouteId']);
                            $firstime = $ftm->departuretime;
                            $firstdate = $p['DepartureDateTime'];
                           $tm = $this->bus_model->gettownsinroutes($p['RouteId'],$townfid);
                           if ($p['NewDepartureTime']==NULL)
                           {
                           if ($tm->departuretime < $firstime)
                           {
                               $newdate = date('Y-m-d',strtotime($firstdate.' + 1 day'));
                               $depztm = $newdate.' '.$tm->departuretime;
                           }
                           else {
                               $depztm =$firstdate.' '.$tm->departuretime;
                           }
                           }
                           else {
                               $depztm = $p['NewDepartureTime'];
                           }
                           if ($depztm >= date('Y-m-d H:i:s'))
                           {
                            $tcount = count($towns);
                            //$firsttown = array_search($townfid, $towns);
                            //$secondtown = array_search($towntid, $towns);
                            $cmz = "";
                            for ($i=0;$i<$tcount;$i++)
                            {
                                $townpos = array_search($towns[$i], $towns);
                                if ($townpos < $secondtown)
                                {
                                    $bookedseats = $this->bus_model->getBookedSeats_t_f($p['ScheduleId'],$towns[$i],$towntid);
                                    $cmz .=$bookedseats->booked.",";
                                }
                                if ($firsttown < $townpos)
                                {
                                    $bookedseats = $this->bus_model->getBookedSeats_t_f($p['ScheduleId'],$townfid,$towns[$i]);
                                    $cmz .=$bookedseats->booked.",";
                                    //echo $townf."-".$towns[$i]."<br>";
                                }
                            }
                            
		//$bookedseats = $this->bus_model->getBookedSeats($id);
				//$bookedt=$this->bus_model->getBookedSeatsrr($p['ScheduleId']);
				//$booked=$bookedt['booked'];
                                $bookedr=explode(',',$cmz);
				//$xml .= '<buslist>';
				$xml .= "{";
                $xml .= "\"busid\":\"".$p['ScheduleId']."\",";
				$xml .= "\"busname\":\"".$p['BusType']."\",";
				$xml .= "\"routecode\":\"".str_replace(" ","",str_replace("-","_",$p['RouteCode']))."\",";
				$xml .= "\"reptime\":\"".$reptime."\",";
				$xml .= "\"deptime\":\"".$deptime."\",";
				$xml .= "\"vipseats\":\"";
				$vs = "";
				
                                    
                                        if ($p['BusTypeId']==1)
                                        {
                                            $ec = explode(",","1,4");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $vs.= $ec[$i]." Vip,";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==2)
                                        {
                                                $ec = explode(",","1,2,5,6");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $vs.= $ec[$i]." Vip,";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==3)
                                        {
                                            
                                            $ec = explode(",","1,2,3,4");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $vs.= $ec[$i]." Vip,";
                                                    }
                                                }
                                        }
                                        if ($p['BusTypeId']==4)
                                        {
                                            
                                            $ec = explode(",","1,2,3,4,5,6");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $vs.= $ec[$i]." Vip,";
                                                    }
                                                }
                                        }
				$xml .=trim($vs,",");
				$xml .= "\",\"fclassseats\":\"";
				$fs = "";
                                 if ($p['BusTypeId']==1)
                                        {
                                            $ec = explode(",","2,3,5,6");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $fs.= $ec[$i]." Fcls,";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==2)
                                        {
                                                $ec = explode(",","3,4,7,8");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $fs.= $ec[$i]." Fcls,";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==3)
                                        {
                                            
                                            $ec = explode(",","800,900");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $fs.= $ec[$i]." Fcls,";
                                                    }
                                                }
                                        }
                                        if ($p['BusTypeId']==4)
                                        {
                                            
                                            $ec = explode(",","800,900");
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $fs.= $ec[$i]." Fcls,";
                                                    }
                                                }
                                        }
				$xml .=trim($fs,",");
				$xml .= "\",\"bizseats\":\"";
				$bs = "";
                                if ($p['BusTypeId']==1)
                                        {
                                    $cs = '';
                                            for($j=7;$j<=47;$j++)
                                            {
                                                $cs .=$j.',';
                                            }
                                            $ec = explode(",",$cs);
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $bs.= $ec[$i].",";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==2)
                                        {
                                            $cs = '';
                                            for($j=9;$j<=45;$j++)
                                            {
                                                $cs .=$j.',';
                                            }
                                                $ec = explode(",",$cs);
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $bs.= $ec[$i].",";
                                                    }
                                                }
                                            
                                        }
                                        if ($p['BusTypeId']==3)
                                        {
                                            $cs = '';
                                            for($j=5;$j<=45;$j++)
                                            {
                                                $cs .=$j.',';
                                            }
                                            $ec = explode(",",$cs);
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $bs.= $ec[$i].",";
                                                    }
                                                }
                                        }
                                        if ($p['BusTypeId']==4)
                                        {
                                            $cs = '';
                                            for($j=7;$j<=33;$j++)
                                            {
                                                $cs .=$j.',';
                                            }
                                            $ec = explode(",",$cs);
                                                for($i=0;$i<count($ec);$i++)
                                                {
                                                    if (!in_array($ec[$i], $bookedr))
                                                    {
                                                        $bs.= $ec[$i].",";
                                                    }
                                                }
                                        }
				$xml .=trim($bs,",");
				$xml .= "\",\"vipprice\":\"";
                                $vpr = $this->bus_model->routeseatpricearray($p['BusTypeId'],1,$townfid,$towntid);
                                //echo $vpr;
				//foreach ($this->bus_model->getseatcost(1,$townfid,$towntid,$p['BusTypeId']) as $c)
                //{
				$xml .= $vpr;
				//}
				$xml .= "\",\"fclassprice\":\"";
				//$fcpr = $this->bus_model->getseatcost(2,$townfid,$towntid,$p['BusTypeId']);
                                $fcpr = $this->bus_model->routeseatpricearray($p['BusTypeId'],2,$townfid,$towntid);
				//if ($fcpr<>NULL)
				//{
				//foreach ($this->bus_model->getseatcost(2,$townfid,$towntid,$p['BusTypeId']) as $c)
               // {
				$xml .= $fcpr;
				//}
				//}
				$xml .= "\",\"bizprice\":\"";
				//$bzpr = $this->bus_model->getseatcost(3,$townfid,$towntid,$p['BusTypeId']);
                                $bzpr = $this->bus_model->routeseatpricearray($p['BusTypeId'],3,$townfid,$towntid);
				//if ($bzpr<>NULL) 
				//{
				//foreach ($this->bus_model->getseatcost(3,$townfid,$towntid,$p['BusTypeId']) as $c)
                //{
				$xml .= $bzpr;
				//}
				//}
				$xml .= "\"},";
                } }}
        }
				
            	echo trim($xml,",")."]";
		 }
		
		/*
	$data['datettownfidtowntid']=$datettownfidtowntid;
		     	$this->load->view('xml/schedulesjson',$data);
				*/
	}
	  function customer_points_device()
	{
		$customerid=$_GET['customerid'];
		$data['points'] = $this->bus_model->getcustomerpoints($customerid);
		$this->load->view('xml/points',$data);
	}
	  function customer_voucher_device()
	{
		$vouchercode=$_GET['vouchercode'];
		$customerid=$_GET['customerid'];
		$data['voucher'] = $this->bus_model->getcustomerVoucher($vouchercode,$customerid);
		
		$this->load->view('xml/voucher',$data);
	}
		  function customer_mpesa_device()
	{
		$mobile=$_GET['mobile'];
		$transid=$_GET['transid'];
		$amount=$_GET['amount'];
		$data['mpesa'] = $this->bus_model->validatecustomermpesa($mobile,$transid,$amount);
		
		$this->load->view('xml/mpesa',$data);
	}
	  function jsonschedule()
	{
		     	$this->load->view('xml/jsonschedule');
	}
	  function jsonpoints()
	{
		     	$this->load->view('xml/jsonpoints');
	}
		  function jsonvoucher()
	{
		     	$this->load->view('xml/jsonvoucher');
	}
			  function jsonmpesa()
	{
		     	$this->load->view('xml/jsonmpesa');
	}
	  function ticket_station()
	{	
		if ($this->flexi_auth->is_logged_in()){
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		
     	$this->load->view('ticket_station',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
	  function ticket_station_voucher($townftownttckid)
	{	
		if ($this->flexi_auth->is_logged_in()){
                   $tt = explode(":",$townftownttckid);
                   $data['townf'] = $tt[0];
                   $data['townt'] = $tt[1];
                   $data['ticketid'] = $tt[2];
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		
     	$this->load->view('ticket_station_voucher',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
	  function ticket_station_corp()
	{	
		if ($this->flexi_auth->is_logged_in()){
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		
     	$this->load->view('ticket_station_corp',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function crud_bus()
	{	
                    //$this->output->cache(10080);
	       if ($this->flexi_auth->is_logged_in()){
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['makes']=$this->bus_model->getMakes();
		
		$data['models']=$this->bus_model->getModels();
		
		$data['positions']=$this->bus_model->getplainresults('seat_position_lkp');
			
		$data['buses']=$this->bus_model->getBuses();
		
		$this->load->view('manage_bus',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function update_bus_location($busid)
	{	
                    //$this->output->cache(10080);
	       if ($this->flexi_auth->is_logged_in()){
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['makes']=$this->bus_model->getMakes();
		
		$data['models']=$this->bus_model->getModels();
		
		$data['positions']=$this->bus_model->getplainresults('seat_position_lkp');
			
		$data['buses']=$this->bus_model->getBuses();
                
                $data['bus'] = $this->bus_model->getonerowfromonetbl('buses', 'BusId', $busid);
                
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                
                $data['busid'] = $busid;
		
		$this->load->view('manage_bus',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
			function update_bus_location_update()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	

		$busid = $this->input->post('busid');
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
		   
               $data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['makes']=$this->bus_model->getMakes();
		
		$data['models']=$this->bus_model->getModels();
		
		$data['positions']=$this->bus_model->getplainresults('seat_position_lkp');
			
		$data['buses']=$this->bus_model->getBuses();
                
                $data['bus'] = $this->bus_model->getonerowfromonetbl('buses', 'BusId', $busid);
                
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                
                $data['busid'] = $busid;
		
		$this->load->view('manage_bus',$data);
		}
		else
		{	
                        
                        $updatedata4 = array(
			'station_at' => $this->input->post('town'),
			'IsActive' => 1							
		);
                        $tablename4 = 'buses';
                        $pkname4 = 'BusId';
                        $pkvalue4 = $busid;//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename4, $updatedata4, $pkname4, $pkvalue4);
                        $data['url'] = 'admins/crud_bus';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function crud_report_center()
	{	
                    //$this->output->cache(10080);
	       if ($this->flexi_auth->is_logged_in()){
		
		$this->load->view('reports/manage_report_center');
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_settings()
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['item']=$this->bus_model->getonerowfromonetbl('exchange_rates', 'exchange_rates_id', 1);
		
		$this->load->view('manage_settings',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function calchange($amountcurusercur)
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $acu = explode(".",$amountcurusercur);
                   $data['amount'] = $acu[0];
                   $data['cur'] = $acu[1];
                   $data['usercur'] = $acu[2];
                   $data['rep'] = 0;
                   $data['errormsg'] = "";
		
		$this->load->view('calchange',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function calchange2()
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $amount = $this->input->post('amount');
                   $amountpaid = $this->input->post('amountpaid');
                   $cur = $this->input->post('cur');
                   $usercur = $this->input->post('usercur');
		if ($amountpaid < $amount) {
                     $data['amount'] = $amount;
                   $data['cur'] = $cur;
                   $data['usercur'] = $usercur;
                   $data['rep'] = 0;
                   $data['errormsg'] = "Enter valid amount";
                    $this->load->view('calchange',$data);
                }
                else {
                    $change = abs($amountpaid - $amount);
                    if ($cur==$usercur):
                        $newchange = $change;
                    else:
                        $newchange = $this->doexchange($cur,$usercur,$change);
                    endif;
                     $data['amount'] = $amount;
                   $data['cur'] = $cur;
                   $data['usercur'] = $usercur;
                   $data['rep'] = 1;
                   $data['change'] = $newchange;
                    $this->load->view('calchange',$data);
                    
                }
		
		}else {
                 redirect('auth','refresh');
            }
	}
        
        function doexchange($cur1,$cur2,$amount)
        {
            $newamount = "";
            $ex=$this->bus_model->getonerowfromonetbl('exchange_rates', 'exchange_rates_id', 1);
            if ($cur1==$cur2){
                $newamount = $amount;
            }
             if ($cur1==1 && $cur2==2){
                 
                $newamount = round(($amount * $ex->ugx),2);
            }
             if ($cur1==1 && $cur2==3){
                 
                $newamount = round(($amount * $ex->tzs),2);
            }
             if ($cur1==1 && $cur2==4){
                 
                $newamount = round(($amount * $ex->usd),2);
            }
             if ($cur1==1 && $cur2==5){
                 
                $newamount = round(($amount * $ex->euro),2);
            }
             if ($cur1==2 && $cur2==1){
                 
                $newamount = round(($amount / $ex->ugx),2);
            }
             if ($cur1==2 && $cur2==3){
                 
                $newamount = round(($amount * ($ex->ugx / $ex->tzs)),2);
            }
             if ($cur1==3 && $cur2==1){
                 
                $newamount = round(($amount / $ex->tzs),2);
            }
             if ($cur1==3 && $cur2==2){
                 
                $newamount = round(($amount * ($ex->tzs / $ex->ugx)),2);
            }
            return $newamount;
        }
		function crud_agents()
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
		
		$this->load->view('manage_agents',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_customers()
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_acc_statements()
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
						
                    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    
                   $custamountcre=$this->bus_model->get_customer_total_credited($cust->cust_id);
			$data['totcredits'] = abs($custamountcre->camountcred);
                         $custamountdeb=$this->bus_model->get_customer_total_debited($cust->cust_id);
                 $data['tickets'] = NULL;
                         $data['deposits'] = NULL;
			$data['totdebits'] = $custamountdeb->camountdeb;
                        
                       if ($this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id)->cust_type==1)
                       {
                           $data['acctype'] = "Deposit";
                       }
                       else {
                           $data['acctype'] = "Contractual";
                       }
		$data['cust_id'] = $cust->cust_id;
		$this->load->view('manage_corp_accounts',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function corp_get_tickets()
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
						
                    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    
                   $custamountcre=$this->bus_model->get_customer_total_credited($cust->cust_id);
			$data['totcredits'] = abs($custamountcre->camountcred);
                         $custamountdeb=$this->bus_model->get_customer_total_debited($cust->cust_id);
                 $data['tickets'] = $this->bus_model->getcorptickets($cust->cust_id,$this->input->post('datef'),$this->input->post('datet'));
                         $data['deposits'] = $this->bus_model->get_customer_credited($cust->cust_id,$this->input->post('datef'),$this->input->post('datet'));
			$data['totdebits'] = $custamountdeb->camountdeb;
                        
                       if ($this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id)->cust_type==1)
                       {
                           $data['acctype'] = "Deposit";
                       }
                       else {
                           $data['acctype'] = "Contractual";
                       }
		$data['cust_id'] = $cust->cust_id;
		$this->load->view('manage_corp_accounts',$data);
		}else {
                 redirect('auth','refresh');
            }
        }	
            function corp_get_tickets_head()
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   
                    $custid = $this->input->post('custid');
                         $data['deposits'] = $this->bus_model->get_customer_credited($custid,$this->input->post('datef'),$this->input->post('datet'));
			
                        
                       if ($this->bus_model->getonerowfromonetbl('customer','Cust_id', $custid)->cust_type==1)
                       {
                           $data['acctype'] = "Deposit";
                       }
                       else {
                           $data['acctype'] = "Contractual";
                       }
		$data['custid'] = $custid;
                
                $data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
                
		}else {
                 redirect('auth','refresh');
            }
	}
		function corp_get_deposits()
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
						
                    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    
                   $custamountcre=$this->bus_model->get_customer_total_credited($cust->cust_id);
			$data['totcredits'] = abs($custamountcre->camountcred);
                         $custamountdeb=$this->bus_model->get_customer_total_debited($cust->cust_id);
                 $data['tickets'] = $this->bus_model->getcorptickets($cust->cust_id,$this->input->post('datef'),$this->input->post('datet'));
			$data['totdebits'] = $custamountdeb->camountdeb;
                         $data['deposits'] = $this->bus_model->get_customer_credited($cust->cust_id,$this->input->post('datef'),$this->input->post('datet'));
                        
                       if ($this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id)->cust_type==1)
                       {
                           $data['acctype'] = "Deposit";
                       }
                       else {
                           $data['acctype'] = "Contractual";
                       }
		$data['cust_id'] = $cust->cust_id;
		$this->load->view('manage_corp_accounts',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_staff()
	{	
	if ($this->flexi_auth->is_logged_in()){
		$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		
		$data['staff']=$this->bus_model->getStaff();
		
		$this->load->view('manage_staff',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function update_staff_location($staffid)
	{	
	if ($this->flexi_auth->is_logged_in()){
		$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		$data['towns']=$this->bus_model->getplainresults('towns_lkp');
		
		$data['staff']=$this->bus_model->getStaff();
                $data['staffloc'] = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $staffid);
		$data['staffid'] = $staffid;
		$this->load->view('manage_staff',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
			function update_staff_location_update()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	

		$staffid = $this->input->post('staffid');
		$this->form_validation->set_rules('town', 'Bus', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
		   
               $data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		$data['towns']=$this->bus_model->getplainresults('towns_lkp');
		$data['staffid'] = $staffid;
		
		$data['staff']=$this->bus_model->getStaff();
                $data['staffloc'] = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $staffid);
		
		$this->load->view('manage_staff',$data);
		}
		else
		{	
                        
                        $updatedata4 = array(
			'station_at' => $this->input->post('town'),
			'IsActive' => 1							
		);
                        $tablename4 = 'bus_staff';
                        $pkname4 = 'StaffId';
                        $pkvalue4 = $staffid;//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename4, $updatedata4, $pkname4, $pkvalue4);
                        $data['url'] = 'admins/crud_staff';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function crud_maintenance()
	{	
	    if ($this->flexi_auth->is_logged_in()){
		$data['maintenance']=$this->bus_model->getBusMaintenance();
		
		$data['regions']=$this->bus_model->getRegions();
		
		$data['maintenancetypes']=$this->bus_model->getMaintenanceTypes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$this->load->view('manage_bus_maintenance',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function manage_bus_seats($busid)
	{	
	    if ($this->flexi_auth->is_logged_in()){
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['makes']=$this->bus_model->getMakes();
		
		$data['models']=$this->bus_model->getModels();
		
		$data['positions']=$this->bus_model->getplainresults('seat_position_lkp');
			
		$data['buses']=$this->bus_model->getBuses();
		
		$data['seats']=$this->bus_model->getaddedseats($busid);
		
	    $bus = $this->bus_model->getonerowfromonetbl('buses', 'BusId', $busid);
	   
       $data['bus'] = $bus->RegistrationNumber;
		
		$this->load->view('manage_bus',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
			function add_bus_seats($busid)	
	{	
		if ($this->flexi_auth->is_logged_in())
            {		
		$this->form_validation->set_rules('seatnumber[]', 'Seat Number', 'trim|required');
		
		$this->form_validation->set_rules('seatposition[]', 'Seat Position', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
		$data['positions']=$this->bus_model->getplainresults('seat_position_lkp');
		
	    $bus = $this->bus_model->getonerowfromonetbl('buses', 'BusId', $busid);
	   
       $data['bus'] = $bus->RegistrationNumber;
	   
		
		$this->load->view('manage_bus',$data);
		}
		else
		{		
			
         $this->bus_model->savebusseats($busid);
		 
	     //redirect('admins/crud_bus','refresh');
                        $data['url'] = 'admins/crud_bus';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function add_cust_users($custid)	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
                  //$custid = $this->input->post('custid');  
		$this->form_validation->set_rules('name[]', 'Name', 'trim|required');
		
		$this->form_validation->set_rules('username[]', 'Username', 'trim|required');
		
		$this->form_validation->set_rules('passwordu[]', 'Password', 'trim|required|min_length[8]');
		
		$this->form_validation->set_rules('useremail[]', 'Email', 'trim|required|valid_email');
		
		if($this->form_validation->run() == FALSE)
		{
                $data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}
		else
		{		
			$count = sizeof($this->input->post('name', TRUE));
                        $names = $this->input->post('name', TRUE);
                        $usernames = $this->input->post('username', TRUE);
                        $passwords = $this->input->post('passwordu', TRUE);
                        $emails = $this->input->post('useremail', TRUE);
                        //echo $count;
         for ($i=0;$i<$count;$i++) {
                
              $email = $emails[$i];
                $username = $usernames[$i];
                $password = $passwords[$i];
                $name = $names[$i];
                
                $group_id = 5;
                $activate = FALSE;
                $t_station = 1;
                
                //echo $email." | ".$username." | ".$password." | ".$name." | ".$group_id." | ".$t_station."<br />";
                //$user_data = array(
                  //      'custid' => $custid
                //);
                
                $user_data = array(
				'upro_first_name' => $name,
				'upro_last_name' => $name,
				'upro_phone' => '0710228024',
				'upro_newsletter' => 0
			);

                $response = $this->flexi_auth->insert_user($email, $username, $password, $user_data, $group_id, $activate, $t_station);
                    if ($response==FALSE)
                    {
                       $data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
                    }
                    $insdata = array(
			'cust_id' => $custid,
			'uname' => $name,
			'usermail' => $email					
		);
					// run insert model to write data to db
                        $tablename = 'cust_users';
                        $pkname = 'cust_users_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                }
		 
	     //redirect('admins/crud_customers','refresh');
                        $data['url'] = 'admins/crud_customers';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function add_towns()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {		
		$this->form_validation->set_rules('town[]', 'Town', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
		$data['towns']=$this->bus_model->getplainresults('towns_lkp');

		$this->load->view('manage_bus_routes',$data);
		}
		else
		{		
			
        $this->bus_model->savetowns();
		 
	    // redirect('admins/crud_routes','refresh');
                        $data['url'] = 'admins/crud_routes';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function crud_petty_cash()
	{	
	    if ($this->flexi_auth->is_logged_in()){
		
		$data['termini']=$this->bus_model->getBusTermini();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['codes']=$this->bus_model->getplainresults('account_codes');
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['pettycash']=$this->bus_model->getpettyCash();
		
                $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$usergrp = $user[0]->uacc_group_fk;
                if ($usergrp==1)
                {
                    redirect('admins/crud_petty_cash_clerk');
                }
                if ($usergrp==2)
                {
                    redirect('admins/crud_petty_cash_branch');
                }
     	$this->load->view('manage_petty_cash',$data);
		}else {
                 redirect('auth','refresh');
            }
	}

		function crud_petty_cash_clerk()
	{	
	    if ($this->flexi_auth->is_logged_in()){
		
		//$data['termini']=$this->bus_model->getBusTermini();
		
            $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                $data['userid'] = $user[0]->uacc_id;
                $data['pickuppoint'] = $user[0]->t_termini;
                $data['town'] = $user[0]->t_station;
		
		//$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['codes']=$this->bus_model->getplainresults('account_codes');
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['pettycash']=$this->bus_model->getpettyCash();
		
     	$this->load->view('manage_petty_cash_clerk',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_termini()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['termini']=$this->bus_model->getBusTermini();
		
		$data['regions']=$this->bus_model->getRegions();
		
		$data['towns']=$this->bus_model->getTowns();
		
     	$this->load->view('manage_bus_termini',$data);
		
		}else {
                 redirect('auth','refresh');
            }
	}
	  function crud_hiring()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['buses']=$this->bus_model->getplainresults('buses');
		
		$data['bushire']=$this->bus_model->getBusHire();
		
     	$this->load->view('manage_bus_hire',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
	  function crud_messages()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['messages']=$this->bus_model->getmessages();
		
		$data['messagedet']=NULL;
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['sentmessages'] = $this->bus_model->getmessagessent();
		
     	$this->load->view('manage_messages',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
	  function crud_banking()
	{	
		if ($this->flexi_auth->is_logged_in()){
		//$data['bankingdetails']=$this->bus_model->getonerowfromonetbl('banking','BankingId',$bankingid);
		$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
		//$data['collections'] = number_format($this->bus_model->gettotalcollectionperstation(3,'2013-02-01'),0);
		//$data['dailypettycash'] = number_format($this->bus_model->gettotalpettyperstation(3,'2013-04-15'),0);
     	$this->load->view('manage_banking',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function view_booked($id)
	{
		if ($this->flexi_auth->is_logged_in()){
				$data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		 //$tickets = $this->bus_model->getBusBooking($id);
                 $data['tickets'] = $this->bus_model->getBusBooking($id);
                 //var_dump($tickets);
                 //echo "ww";
     	 $this->load->view('ticket_station',$data);
				}else {
                 redirect('auth','refresh');
            }
            
            
	}
        
        function search_tickets()
	{	
		if ($this->flexi_auth->is_logged_in()){
				$data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                 $data['tickets'] = $this->bus_model->search_tickets();
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		
     	$this->load->view('ticket_station',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function search_tickets_r()
	{	
		if ($this->flexi_auth->is_logged_in()){
				$data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['nationalities'] = $this->bus_model->getplainresults('nationalities_lkp');
                $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
                if (isset($_POST['searchtype']))
                {
                    $data['tickets'] = $this->bus_model->search_tickets();
                }
                else {
                    $data['tickets'] = NULL;
                }
                 
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
		
     	$this->load->view('search_tickets',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_ticket($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
		 $data['row'] = $this->bus_model->gettickettoprint($id);
                 $data['tids'] = $id;
     	 $this->load->view('print_ticket',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_ticket_corp($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
		 $data['row'] = $this->bus_model->gettickettoprint($id);
                 $data['tids'] = $id;
     	 $this->load->view('print_ticket_corp',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_ticket_email($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
		 $data['row'] = $this->bus_model->gettickettoprint($id);
                 $data['tids'] = $id;
     	 $this->load->view('print_ticket_email',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function manifest_chart()
	{	
		if ($this->flexi_auth->is_logged_in()){
                    $tft = explode("-",$this->input->post('route'));
                    $townf = $tft[0];
                    $townt = $tft[1];
                  $maniid = $this->bus_model->getlastmanifestno($this->input->post('schedid'),$townf,$townt);
                  $mani = $this->bus_model->getonerowfromonetbl('manifest', 'manifest_id', $maniid);
                  $data['manifestno'] = $mani->manifest_no;
                  $data['sched'] = $this->bus_model->getBusSchedule($this->input->post('schedid'));
		 $data['tickets'] = $this->bus_model->gen_manifest_tickets($this->input->post('schedid'));
                 $data['maniid'] = $maniid;
                 $data['schedid'] = $this->input->post('schedid');
                 $data['from'] = $townf;
                 $data['to'] = $townt;
                 //echo $data['schedid']; exit;
                 if ($this->input->post('maniorchart')=="1")
                 {
                     $page = 'manifest';
                 }
                 else {
                     $page = 'chart';
                     //$this->load->view('chart',$data);
                     //redirect('admins/print_chart/'.$maniid);
                 }
                 $this->load->view($page,$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_manifest($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
                  $data['manifestno'] = $this->bus_model->getlastmanifestno($id);
                  $data['sched'] = $this->bus_model->getBusSchedule($id);
		 $data['tickets'] = $this->bus_model->gen_manifest_tickets($id);
     	 $this->load->view('manifest',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_chart($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
                  $data['manifestno'] = $this->bus_model->getlastmanifestno($id);
                  $data['sched'] = $this->bus_model->getBusSchedule($id);
		 $data['tickets'] = $this->bus_model->gen_manifest_tickets($id);
     	 $this->load->view('chart',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_cash_recon()
	{	
		if ($this->flexi_auth->is_logged_in()){
                  $clerk = $this->input->post('clerk');
		 $data['tickets'] = $this->bus_model->gen_clerk_tickets_today($clerk);
		 $data['openticketstotal'] = $this->bus_model->gen_clerk_tickets_today($clerk);
		 $data['pcs'] = $this->bus_model->getpettyCashByClerkToday($clerk);
                 $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$data['username']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                 $this->user1=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$data['printeruser']= $this->user1[0]->upro_first_name.' '.$this->user1[0]->upro_last_name;
     	 $this->load->view('cash_reconciliation',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function print_cash_recon_special()
	{	
		if ($this->flexi_auth->is_logged_in()){
                  $clerk = $this->input->post('clerk');
		 $data['tickets'] = $this->bus_model->gen_clerk_tickets_today_special($clerk);
		 $data['openticketstotal'] = $this->bus_model->gen_clerk_tickets_today($clerk);
		 $data['pcs'] = $this->bus_model->getpettyCashByClerkToday($clerk);
                 $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$data['username']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                 $this->user1=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$data['printeruser']= $this->user1[0]->upro_first_name.' '.$this->user1[0]->upro_last_name;
     	 $this->load->view('cash_reconciliation',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function crud_cash_recon_clerk()
	{	
		if ($this->flexi_auth->is_logged_in()){
                  $clerk = $this->flexi_auth->get_user_id();
		 $data['tickets'] = $this->bus_model->gen_clerk_tickets_today($clerk);
		 $data['openticketstotal'] = $this->bus_model->gen_clerk_tickets_today($clerk);
		 $data['pcs'] = $this->bus_model->getpettyCashByClerkToday($clerk);
                 $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$data['username']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                 $this->user1=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$data['printeruser']= $this->user1[0]->upro_first_name.' '.$this->user1[0]->upro_last_name;
     	 $this->load->view('cash_reconciliation',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function open_ticket($id)
	{	
		if ($this->flexi_auth->is_logged_in()){
		$data['row'] = $this->bus_model->gettickettoprint($id);
                $deptime = $data['row']->departuretime;
                $newdeptime = date('G',strtotime($deptime));
                //echo $newdeptime;
                //$now = date('G');
                $now = date('G',strtotime("22:00:00"));
                //$twohoursfromnow = date('G',strtotime(' - 2 hour'));
                //$sixhoursfromnow = date('G',strtotime(' - 6 hour'));
                $twohoursfromdep = date('G',strtotime($deptime.' - 2 hour'));
                $sixhoursfromdep = date('G',strtotime($deptime.' - 6 hour'));
                //echo $twohoursfromdep;
                $vouchercharge = 0;
                if ($data['row']->t_userid==1)
                {
                    $amount = $data['row']->amount;
                }
                if ($data['row']->t_userid==2)
                {
                    $amount = $data['row']->amountug;
                }
                if ($data['row']->t_userid==3)
                {
                    $amount = $data['row']->amounttz;
                }
                if ($data['row']->t_userid==4)
                {
                    $amount = $data['row']->amountusd;
                }
                if ($data['row']->t_userid==5)
                {
                    $amount = $data['row']->amounteuro;
                }
                
                if ($now > 7 && $now < 23) {
                    if ($twohoursfromdep <= $now)
                    {
                        $vouchercharge = round($amount/2);
                    }
                    elseif($sixhoursfromdep <= $now)
                    {
                        $vouchercharge = 300;
                    }
                    else {
                        $vouchercharge = 0;
                    }
                }
                else {
                    $vouchercharge = round($amount/2);
                }
                 $data['tids'] = $id;
                 $data['vouchercharge'] = $vouchercharge;
                 
     	 $this->load->view('ticket_voucher_charge',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
        function open_ticket2()
	{	
		
                    
                    
                    $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
            $ticketid = $this->input->post('ticketid');
		if ($this->flexi_auth->is_logged_in())
            {	
                $data['ticketid'] = $ticketid;
                
                $data['message'] = NULL;		
		$this->form_validation->set_rules('reason', 'Cancel Reason', 'trim|required');
		
	   if($this->form_validation->run() == FALSE)
		{
                $data['row'] = $this->bus_model->gettickettoprint($ticketid);
                 $data['tids'] = $ticketid;
     	 $this->load->view('ticket_voucher_charge',$data);
		}
		else
		{		
			$updatedata = array(
			'cancel_reason' => $this->input->post('reason'),
			'cancelled_by' => $username,
			'cancel_date' => date('Y-m-d'),
			'ticketstatus' => 'Open'							
		);
					// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $pkvalue = $this->input->post('ticketid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			    
                      
                      $id = $this->input->post('ticketid');
                    $currency = $this->input->post('currency');
                    if ($currency==1):
                    $opentable = 'openticket_charge';
                    $data['openchargeamnt'] = "ksh ".$this->input->post('vouchercharge');
                    endif;
                    if ($currency==2):
                    $opentable = 'openticket_chargeug';
                    $data['openchargeamnt'] = "Ush ".$this->input->post('vouchercharge');
                    endif;
                    if ($currency==3):
                    $opentable = 'openticket_chargetz';
                    $data['openchargeamnt'] = "Tsh ".$this->input->post('vouchercharge');
                    endif;if ($currency==4):
                    $opentable = 'openticket_chargeusd';
                    $data['openchargeamnt'] = "USD ".$this->input->post('vouchercharge');
                    endif;
                     $vn = $this->bus_model->_generateVoucherCode();
                    $updatedata = array(
			'ticketstatus' => 'Open',
                        'voucherno' => $vn,
                        'open_date' => date('Y-m-d'),
                        $opentable => $this->input->post('vouchercharge')
		);
					// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $pkvalue = $id;//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                     $data['voucherno'] = $vn;
		$data['row'] = $this->bus_model->gettickettoprint($id);
                 $data['tids'] = $id;
                 $data['reason'] = $this->input->post('reason');
     	 $this->load->view('ticket_voucher',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
                    
                    
                    
                    
				
	}
	  function listbuschedules($townid)
	{	
                $schedules = $this->bus_model->getBusSchedulesbyTown($townid);
				//print_r($schedules);
                
                if ($schedules<>NULL)
                {
                    //
                    foreach ($schedules as $r)
                    {   $html = "<tr>";
                        $bustype = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$r->BusTypeLkp);
                        $html .= "<td><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a></td><td>". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td>".$r->DepartureDateTime."</td><td>".$r->Busno."</td>";
						$html .= "</tr>";echo $html;
                    }
                    //
                    
                }
                else {
                    echo "<tr><td colspan=\"5\">No Bus Schedules created yet. Please contact admin.</td></tr>";
                }
		
     	
				
	}
	  function get_schedules($townftowtdatet)
	{	
              if ($this->flexi_auth->is_logged_in()){ 
                  $ftd = explode(":", $townftowtdatet);
                  $townfid = $ftd[0];
                  $towntid = $ftd[1];
                  $tdate = $ftd[2];
                  
                  //$townfid = 1;
                  //$towntid = 2;
                  //$tdate = "2013-05-08";
                //$schedules = $this->bus_model->getBusSchedulesbyTownFTownToDate($townfid,$towntid,$tdate);
                $schedules = $this->bus_model->getBusSchedulesbysearch($tdate,$townfid,$towntid);
                //echo $townftowtdatet;
                //var_dump($schedules);
				
                if ($schedules<>NULL)
                {
                    //
                    foreach ($schedules as $r)
                    {   $html = "<tr>";
                        $bustype = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$r->BusTypeLkp);
                        $towns = explode(",",$r->towns);
                        $firsttown = array_search($townfid, $towns); 
                        $secondtown = array_search($towntid, $towns);
                        if ($secondtown>$firsttown) //check order of towns in route
                        {
                            $ftm = $this->bus_model->gettownsinroutes_first($r->RouteId);
                            $firstime = $ftm->departuretime;
                            $firstdate = $r->DepartureDateTime;
                           $tm = $this->bus_model->gettownsinroutes($r->RouteId,$townfid);
                           if ($r->NewDepartureTime==NULL)
                           {
                           if ($tm->departuretime < $firstime)
                           {
                               $newdate = date('Y-m-d',strtotime($firstdate.' + 1 day'));
                               $depztm = $newdate.' '.$tm->departuretime;
                           }
                           else {
                               $depztm =$firstdate.' '.$tm->departuretime;
                           }
                           }
                           else {
                               $depztm = $r->NewDepartureTime;
                           }
                           if (date('Y-m-d H:i:s',strtotime($depztm.'+ 1 hour')) >= date('Y-m-d H:i:s'))
                           {
                               $totbooked = $this->bus_model->getBusSchedulestotseats($r->ScheduleId,$townfid,$towntid);
                               //echo $townfid; exit;
                               if ($r->NewDepartureTime<>NULL)
                               {
                                   $nd = " | ".date('g:i A', strtotime($r->NewDepartureTime));
                               }
                               else {
                                   $nd = "";
                               }
                               if ($bustype->BusType=="Oxygen")
                               {
                                   if ($totbooked < 47)
                                   {
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd."</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                                   else {
                                       //$html.="<td colspan=\"4\">Sorry, <a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a> bus is full</td>";
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd." bus already full</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                               }
                               else if ($bustype->BusType=="Normal")
                               {
                                   if ($totbooked < 45)
                                   {
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd."</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                                   else {
                                       //$html.="<td colspan=\"4\">Sorry, ".$r->RouteCode." bus is full</td>";
                                       //$html.="<td colspan=\"4\">Sorry, <a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a> bus is full</td>";
                                       
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd." bus already full</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                               }
                               else if ($bustype->BusType=="Irizar")
                               {
                                   if ($totbooked < 49)
                                   {
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd."</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                                       //$html.="<td colspan=\"4\">Sorry, <a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a> bus is full</td>";
                           
                                   }
                                   else {
                                       //$html.="<td colspan=\"4\">Sorry, ".$r->RouteCode." bus is full</td>";
                                       //$html.="<td colspan=\"4\">Sorry, <a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a> bus is full</td>";
                                       
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd." bus already full</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                               }
                               //if ($bustype->BusType=="Minibus")
                               else
                               {
                                   if ($totbooked < 33)
                                   {
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd."</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                                   else {
                                       //$html.="<td colspan=\"4\">Sorry, ".$r->RouteCode." bus is full</td>";
                                       //$html.="<td colspan=\"4\">Sorry, <a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a> bus is full</td>";
                                       
                                       $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a>".$nd." bus already full</td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           
                                   }
                               }
                               
                            }
                        
                           }
                        else {
                            $html .= "";
                        }
						$html .= "</tr>";echo $html;
                    }
                    //
                    
                }
                else {
                    echo "<tr><td colspan=\"5\">No Bus Schedules created yet. Please contact admin.</td></tr>";
                }
                
               
              }
                else {
                 redirect('auth','refresh');
            }
		
     	
				
	}
	  function get_schedules_voucher($townftowtdatettickid)
	{	
              if ($this->flexi_auth->is_logged_in()){ 
                  $ftd = explode(":", $townftowtdatettickid);
                  $townfid = $ftd[0];
                  $towntid = $ftd[1];
                  $tdate = $ftd[2];
                  $ticketid = $ftd[3];
                  
                  //$townfid = 1;
                  //$towntid = 2;
                  //$tdate = "2013-05-08";
                //$schedules = $this->bus_model->getBusSchedulesbyTownFTownToDate($townfid,$towntid,$tdate);
                $schedules = $this->bus_model->getBusSchedulesbysearch($tdate,$townfid,$towntid);
                //echo $townftowtdatet;
                //var_dump($schedules);
				
                if ($schedules<>NULL)
                {
                    //
                    foreach ($schedules as $r)
                    {   $html = "<tr>";
                        $bustype = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$r->BusTypeLkp);
                        $towns = explode(",",$r->towns);
                        $firsttown = array_search($townfid, $towns); 
                        $secondtown = array_search($towntid, $towns);
                        if ($secondtown>$firsttown) //check order of towns in route
                        {
                            $ftm = $this->bus_model->gettownsinroutes_first($r->RouteId);
                            $firstime = $ftm->departuretime;
                            $firstdate = $r->DepartureDateTime;
                           $tm = $this->bus_model->gettownsinroutes($r->RouteId,$townfid);
                           if ($tm->departuretime < $firstime)
                           {
                               $newdate = date('Y-m-d',strtotime($firstdate.' + 1 day'));
                               $depztm = $newdate.' '.$tm->departuretime;
                           }
                           else {
                               $depztm =$firstdate.' '.$tm->departuretime;
                           }
                           if ($depztm >= date('Y-m-d H:i:s'))
                           {
                        //$html .= "<td><a href=".base_url()."admins/crud_ticketing_voucher/".$r->ScheduleId.":".$townfid.":".$towntid.":".$ticketid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a></td><td>". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td>".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td>".$r->Busno."</td>";
                        $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing_voucher/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a></td><td width=\"150\">". anchor('admins/view_booked/'.$r->ScheduleId.'/#tab2',''.$r->RouteName.'')."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                           }
                        
                           }
                        else {
                            $html .= "";
                        }
						$html .= "</tr>";echo $html;
                    }
                    //
                    
                }
                else {
                    echo "<tr><td colspan=\"5\">No Bus Schedules created yet. Please contact admin.</td></tr>";
                }
                
               
              }
                else {
                 redirect('auth','refresh');
            }
		
     	
				
	}
	  function get_schedules_corp($townftowtdatet)
	{	
              if ($this->flexi_auth->is_logged_in()){ 
                  $ftd = explode(":", $townftowtdatet);
                  $townfid = $ftd[0];
                  $towntid = $ftd[1];
                  $tdate = $ftd[2];
                  
                  //$townfid = 1;
                  //$towntid = 2;
                  //$tdate = "2013-05-08";
                //$schedules = $this->bus_model->getBusSchedulesbyTownFTownToDate($townfid,$towntid,$tdate);
                $schedules = $this->bus_model->getBusSchedulesbysearchcorp($tdate,$townfid,$towntid);
                //echo $townftowtdatet;
                //var_dump($schedules);
				
                if ($schedules<>NULL)
                {
                    //
                    foreach ($schedules as $r)
                    {   $html = "<tr>";
                        $bustype = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$r->BusTypeLkp);
                        $towns = explode(",",$r->towns);
                        $firsttown = array_search($townfid, $towns); 
                        $secondtown = array_search($towntid, $towns);
                        if ($secondtown>$firsttown) //check order of towns in route
                        {
                        //$html .= "<td><a href=".base_url()."admins/crud_ticketing_corp/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a></td><td>".$r->RouteName."</td><td>".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td>".$r->Busno."</td>";
                        $html .= "<td width=\"141\"><a href=".base_url()."admins/crud_ticketing_corp/".$r->ScheduleId.":".$townfid.":".$towntid." id=".$r->ScheduleId." class=clicks>".$r->RouteCode."</a></td><td width=\"150\">".$r->RouteName."</td><td width=\"174\">".date('d/m/Y',strtotime($r->DepartureDateTime))."</td><td width=\"194\">".$bustype->BusType."(".$r->Busno.")</td>";
                        }
                        else {
                            $html .= "";
                        }
						$html .= "</tr>";echo $html;
                    }
                    //
                    
                }
                else {
                    echo "<tr><td colspan=\"5\">No Bus Schedules available. Please contact modern coast express ltd.</td></tr>";
                }
                
               
              }
                else {
                 redirect('auth','refresh');
            }
		
     	
				
	}
 	  function listbuschedulesreturn($townfidtowntiddate)
	{	       
	if ($this->flexi_auth->is_logged_in()){ 
                        $td= explode(".", $townfidtowntiddate);
           		$returntownfid = $td[0];
                        $returntowntid = $td[1];
                        $seatid = $td[3];
                        $data['seatprice'] = $td[4];
            	$returndate = $td[2];
                //$data['schedules'] = $this->bus_model->getBusSchedulesbyTownFTDate($returntownfid,$returntowntid,$returndate); 
                $data['schedules'] = $this->bus_model->getBusSchedulesbysearch($returndate,$returntownfid,$returntowntid);
                //$data['schedules'] = $this->bus_model->getBusSchedulesbysearch($returndate,$returntownfid,$returntowntid);
                $data['rep'] = NULL;
                $data['quan']=$seatid;
                $data['townfid'] = $returntownfid;
                $data['towntid'] = $returntowntid;
				$data['scheduleid']=NULL;
                //echo $townfidtowntiddate;
		     	$this->load->view('seat_list2',$data);
				}else {
                 redirect('auth','refresh');
            }
     	
				
	}  
	  function listbustermini($townid)
	{	
                $termini = $this->bus_model->getresultfromonetbl('bus_termini','TownLkp',$townid);
                
                if ($termini<>NULL)
                {
                    $html = "<select name=\"pickuppoint\" id=\"pickuppoint\" class=\"smallselect\">";
                    foreach ($termini as $t)
                    {
                        $html .= "<option value=\"".$t->TerminusId."\">".$t->TerminusName."</option>";
                      
                    }
                    $html .= "</select>";
                    echo $html;
                }
                else {
                    echo "<tr><td colspan=\"5\">No Termini added to this Town. Please contact admin.</td></tr>";
                }
		
     	
				
	}
			  function getcarddetails($id)
	{	
	$id = str_replace("%","",$id);
	$id = str_replace("?","",$id);
                $details = $this->bus_model->getonerowfromonetbl('card_details','card_no',$id);
                echo json_encode($details);

}
			  function getnewseatpriceoncurrency($bustypeidseatclassroutefromrouteto)
	{	
                              $bsrt = explode(":",$bustypeidseatclassroutefromrouteto);
                              $bustypeid = $bsrt[0];
                              $seatclass = $bsrt[1];
                              $routefrom = $bsrt[2];
                              $routeto = $bsrt[3];
                              $cur =  $bsrt[4];
                              $details = $this->bus_model->routeseatpricewithcurrency($bustypeid,$seatclass,$routefrom,$routeto,$cur);
               
                echo $details;

}
		  function listbuschedule($scheduleid)
	{	
	if ($this->flexi_auth->is_logged_in()){
                $schedules = $this->bus_model->getBusSchedule($scheduleid);
                echo json_encode($schedules);
								}else {
                 redirect('auth','refresh');
            }
}
        
	  function crud_loyalty()
	{	
		if ($this->flexi_auth->is_logged_in()){
		//$data['bankingdetails']=$this->bus_model->getonerowfromonetbl('banking','BankingId',$bankingid);
		//$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                //$data['customers'] = $this->bus_model->getplainresults('customers');
		$data['rep'] = NULL;
		$data['rep1'] = NULL;
		$data['rep2'] = NULL;
     	$this->load->view('manage_loyalty',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        function gains_burns()
	{	
		if ($this->flexi_auth->is_logged_in()){
		$pg = $this->bus_model->get_total_points_gained_per_customer($this->input->post('custcardno'));
                if ($pg==NULL or $pg->pointsgained==NULL or $pg->pointsgained=="" or $pg->pointsgained==0)
                {
                    $pgd = 0;
                }
                else {
                    $pgd =$pg->pointsgained;
                }
                $data['pointsgained'] = $pgd;
		$pb = $this->bus_model->get_total_points_burned_per_customer($this->input->post('custcardno'));
                if ($pb==NULL or $pb->pointsburned==NULL or $pb->pointsburned=="" or $pb->pointsburned==0)
                {
                    $pbd = 0;
                }
                else {
                    $pbd= abs($pb->pointsburned);
                }
                $data['pointsburned'] = abs($pbd);
		$data['rep'] = 1;
		$data['rep1'] = NULL;
		$data['rep2'] = NULL;
                $data['balpoints'] = $pgd - $pbd;
                $cst = $this->bus_model->getonerowfromonetbl('card_details', 'card_no', $this->input->post('custcardno'));
                $data['custname'] = $cst->f_name." ".$cst->m_name." ".$cst->l_name;
                    $this->load->view('manage_loyalty',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        function points_history()
	{	
            
		if ($this->flexi_auth->is_logged_in()){
		$data['phs'] = $this->bus_model->get_points_history_per_customer_daterangw($this->input->post('custcardno1'),$this->input->post('datef'),$this->input->post('datet'));
                $data['pb'] = $this->bus_model->getonerowfromonetbl('points_burn', 'points_burn_id', 1);
               $data['rep'] = NULL;
		$data['rep1'] = 1;
		$data['rep2'] = NULL;
                $cst = $this->bus_model->getonerowfromonetbl('card_details', 'card_no', $this->input->post('custcardno1'));
                if ($this->input->post('datef')==FALSE):
                    $datef = "";
                else:
                    $datef = date('d/m/Y g:i A',strtotime($this->input->post('datef')));
                endif; 
                if ($this->input->post('datet')==FALSE):
                    $datet = "";
                else:
                    $datet = date('d/m/Y g:i A',strtotime($this->input->post('datet')));
                endif;
                $data['custname'] = $cst->f_name." ".$cst->m_name." ".$cst->l_name." | From ".$datef." To ".$datet;
                    $this->load->view('manage_loyalty',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		  function cancel_ticket($ticketid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		//$data['bankingdetails']=$this->bus_model->getonerowfromonetbl('banking','BankingId',$bankingid);
		//$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                //$data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['ticketid'] = $ticketid;
                
                $data['message'] = NULL;
		
     	$this->load->view('cancel_ticket',$data);
				}
                                else {
                 redirect('auth','refresh');
            }
	}
		 function ticket_cancel_reason($ticketid)
        {
            
		 if ($this->flexi_auth->is_logged_in()){
			$data['row'] = $this->bus_model->getonerowfromonetbl('ticketing','TicketId',$ticketid);
            $this->load->view('ticket_cancel_reason',$data);
	        }else {
                 redirect('home','refresh');
            }
        }
        function confirm_booking($ticketid)	
	{	

		if ($this->flexi_auth->is_logged_in())
            {	
	
			$updatedata = array(
			'ticketstatus' => 'Active',
			't_userid' => $this->flexi_auth->get_user_id()							
		);
					// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $pkvalue = $ticketid;//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                         //redirect('admins/ticket_station','refresh');
                           $data['tids'] = $ticketid.",";
                                  //echo $tids;
     	 $this->load->view('print_ticket',$data);
	
		}else {
                 redirect('auth','refresh');
            }
		
	}
         function cancel_this_ticket()	
	{	
	        $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
            $ticketid = $this->input->post('ticketid');
		if ($this->flexi_auth->is_logged_in())
            {	
                $data['ticketid'] = $ticketid;
                
                $data['message'] = NULL;		
		$this->form_validation->set_rules('reason', 'Cancel Reason', 'trim|required');
		
	   if($this->form_validation->run() == FALSE)
		{
                $data['ticketid'] = $ticketid;
                
                $data['message'] = NULL;
		
     	$this->load->view('cancel_ticket',$data);
		}
		else
		{		
			$updatedata = array(
			'cancel_reason' => $this->input->post('reason'),
			'cancelled_by' => $username,
			'cancel_date' => date('Y-m-d'),
			'ticketstatus' => 'Cancelled'							
		);
					// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $pkvalue = $this->input->post('ticketid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			    $data['message'] = "Booking successfully cancelled!";
                $this->load->view('cancel_ticket',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

	  function upload_bank_slip($bankingid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		//$data['bankingdetails']=$this->bus_model->getonerowfromonetbl('banking','BankingId',$bankingid);
		//$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                //$data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['bankingid'] = $bankingid;
                
                $data['message'] = NULL;
		
     	$this->load->view('upload_bank_slip',$data);
				}else {
                 redirect('auth','refresh');
            }
	}

	  function download_bank_slip($bankingid)
	{	
		if ($this->flexi_auth->is_logged_in()){
                $file = $this->bus_model->getonerowfromonetbl('banking', 'BankingId', $bankingid);
                $this->load->helper('download');
                $data = file_get_contents(base_url().'bankslips/'.$file->bankslip); // Read the file's contents
                $name = $file->bankslip;

                force_download($name, $data);
		
				}else {
                 redirect('auth','refresh');
            }
	}
	  function add_bus_to_schedules($scheduleid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		   
                $data['scheduleid'] = $scheduleid;
                
                $data['message'] = NULL;
		
     	$this->load->view('add_bus_to_schedule',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
			function add_bus_to_schedule_add()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	

		$scheduleid = $this->input->post('scheduleid');
		$this->form_validation->set_rules('bus', 'Bus', 'trim|required');
		
		$this->form_validation->set_rules('driver[]', 'driver', 'trim|required|callback_driver_check');
		
		$this->form_validation->set_rules('conductor', 'Conductor', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
		   
                $data['scheduleid'] = $scheduleid;
                
                $data['message'] = NULL;
                //$array = $this->input->post('driver');
                //echo count(array_unique($array)); exit;
                //echo count($array); exit;
		
		$this->load->view('add_bus_to_schedule',$data);
		}
		else
		{	
                        //update bus schedule	
			$updatedata = array(
			'BusLkp' => $this->input->post('bus'),
			'Conductor' => $this->input->post('conductor'),
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $this->input->post('scheduleid');//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                        
                        //add driver schedules
                        $count = sizeof($this->input->post('driver', TRUE));
                        $driver = $this->input->post('driver', TRUE);
                        $towncodef = $this->input->post('towncodef', TRUE);
                        $towncodet = $this->input->post('towncodet', TRUE);
                        //print_r($seatposition);
                        $drvs = "";
                        for ($i=0;$i<$count;$i++) {
                            $dra = explode(",",trim($drvs,','));
                            if (!in_array($driver[$i], $dra))
                            {
                          $insdata = array(
                            'ScheduleId'=> $this->input->post('scheduleid'),
                            'driver_id'=> $driver[$i],
                            'townfrom'=> $towncodef[$i],
                            'townto'=> $towncodet[$i],
                            'date_available_from'=> $this->input->post('date_aval_frm'),
                            'time_available_from'=>$this->input->post('time_aval_frm')
                          );
                          $pkvalue = 0;
                          $iflastid = 0;
                        $this->bus_model->dbinsert('driver_schedules',$insdata,'driver_schedules_id',$pkvalue,$iflastid);
                        $drvs .=$driver[$i].',';
                        //update driver next date available
                        $schedtimes = $this->bus_model->getBusSchedulesbySchedIdandTown($this->input->post('scheduleid'),$towncodet[$i]);
                        if ($schedtimes==NULL)
                        {
                            $date_available_from = $this->input->post('date_aval_frm');
                            $time_available_from = $this->input->post('time_aval_frm');
                        }
                        else {
                            if (strtotime($this->input->post('time_aval_frm_initial')) >= strtotime($schedtimes->ArrivalDeparture))
                            {
                                $date_available_from = date('Y-m-d', strtotime($this->input->post('date_aval_frm_initial').' + 1 day'));
                            }
                            else {
                                $date_available_from = date('Y-m-d', strtotime($this->input->post('date_aval_frm_initial')));
                            }
                            $time_available_from = $schedtimes->ArrivalDeparture;
                        }
                        $updatedata4 = array(
			'station_at' => $towncodet[$i],
			'date_available_from' => $date_available_from,
			'time_available_from' => $time_available_from,
			'IsActive' => 0							
		);
                        $tablename4 = 'bus_staff';
                        $pkname4 = 'StaffId';
                        $pkvalue4 = $driver[$i];//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename4, $updatedata4, $pkname4, $pkvalue4);
                        }
                        }
                        
                        //update conductor next date available
                        $updatedata2 = array(
			'station_at' => $this->input->post('station_at'),
			'date_available_from' => $this->input->post('date_aval_frm'),
			'time_available_from' => $this->input->post('time_aval_frm'),
			'IsActive' => 0							
		);
                        $tablename2 = 'bus_staff';
                        $pkname2 = 'StaffId';
                        $pkvalue2 = $this->input->post('conductor');//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename2, $updatedata2, $pkname2, $pkvalue2);
                        //update bus next time available
                         $updatedata3 = array(
			'station_at' => $this->input->post('station_at'),
			'date_available_from' => $this->input->post('date_aval_frm'),
			'time_available_from' => $this->input->post('time_aval_frm'),
			'IsActive' => 0							
		);
                        $tablename3 = 'buses';
                        $pkname3 = 'BusId';
                        $pkvalue3 = $this->input->post('bus');//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename3, $updatedata3, $pkname3, $pkvalue3);
                        
						$data['message']='Adding bus to schedule successful';
                                                $data['scheduleid'] = $scheduleid;
			            $this->load->view('add_bus_to_schedule',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		  function apply_discount()
	{	
		if ($this->flexi_auth->is_logged_in()){
		$data['seats']=$this->bus_model->getBusSeats($this->uri->segment('3'),$this->uri->segment('4'));
		//$data['seats']=$this->bus_model->getwojoinedtbresults('seats','bus_termini','TerminusId');
               $data['busid'] = $this->uri->segment('3');
			   
			   $data['busname']=$this->bus_model->getfieldvaluefromrow('buses','RegistrationNumber','BusId',$this->uri->segment('3'));
			   
			   $data['seattype'] = $this->uri->segment('4');
             
                $data['message'] = NULL;
		
     	        $this->load->view('apply_discount',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function apply_discount_to_seats()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('DiscountType', 'Discount Type', 'trim|required');
		
		$this->form_validation->set_rules('DiscountAmount', 'Discount Amount', 'trim|required');

		
		if($this->form_validation->run() == FALSE)
		{	          
		      $data['seats'] = $this->bus_model->getBusSeats($this->input->post('busid'),$this->input->post('seattype'));
		
              $data['busid'] = $this->input->post('busid');
			  
			  $data['busname']=$this->bus_model->getfieldvaluefromrow('buses','RegistrationNumber','BusId',$this->input->post('busid'));

              $data['seattype'] = $this->input->post('seattype');
			  
			  $data['message'] = NULL;
			  
			  $this->load->view('apply_discount',$data);
		}
		else
		{	
		      $this->bus_model->apply_seat_discount($this->input->post('busid'),$this->input->post('seattype'));
        		      
			  $data['seats'] = $this->bus_model->getBusSeats($this->input->post('busid'),$this->input->post('seattype'));
		
              $data['busid'] = $this->input->post('busid');
			  
			  $data['busname']=$this->bus_model->getfieldvaluefromrow('buses','RegistrationNumber','BusId',$this->input->post('busid'));

              $data['seattype'] = $this->input->post('seattype');
			  
   		      $data['message']='Discount applied to selected seats successfully.';						
	
		      $this->load->view('apply_discount','$data');

		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
	  
        function edit_banking($bankingid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['row']=$this->bus_model->getonerowfromonetbl('banking','BankingId',$bankingid);
                $data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                $data['termini'] = $this->bus_model->getplainresults('bus_termini');
		
     	$this->load->view('manage_banking',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
        
		function edit_customer($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function edit_agent($aid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('agent', 'Agent_id', $aid);
		//$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $aid);
                        $data['aid'] = $aid;
		
		$this->load->view('manage_agents',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function view_agent($aid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('agent', 'Agent_id', $aid);
		//$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $aid);
                        $data['aid'] = $aid;
		
		$this->load->view('manage_agents',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function view_customer($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function make_deposit($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                $data['deposits'] = NULL;
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function manage_customer_users($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function manage_agent_users($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
                
                $data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('agent', 'Agent_id', $aid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'Agent_id', $aid);
                        $data['aid'] = $aid;
		
		$this->load->view('manage_agents',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function manage_customer_discount($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
		$data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function update_customer_discount($custid)
	{	
	       if ($this->flexi_auth->is_logged_in()){
                   $updatedata = array('discountamount'=>  $this->input->post('discamount', TRUE));
                   $this->bus_model->dbupdate('customer', $updatedata, 'Cust_id', $custid);
                   redirect('admins/manage_customer_discount/'.$custid.'#tab6','refresh');
		}else {
                 redirect('auth','refresh');
            }
	}
			function read_message() 
	{
		if ($this->flexi_auth->is_logged_in()){
		
        $data['messagedet'] = $this->bus_model->getspecmessage($this->uri->segment(3));
		
		$data['messages']=$this->bus_model->getmessages();
		
		$data['sentmessages'] = $this->bus_model->getmessagessent();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
      
	    $this->bus_model->message_is_read($this->uri->segment(3));
		
		$this->load->view('manage_messages',$data);
						}else {
                 redirect('auth','refresh');
            }
	}
			function read_sent() 
	{
		if ($this->flexi_auth->is_logged_in()){
		
        $data['messagedet'] = $this->bus_model->getspecmessage($this->uri->segment(3));
		
		$data['messages']=$this->bus_model->getmessages();
		
		$data['sentmessages'] = $this->bus_model->getmessagessent();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
      
	    //$this->bus_model->message_is_read($this->uri->segment(3));
		
		$this->load->view('manage_messages',$data);
						}else {
                 redirect('auth','refresh');
            }
	}
		function edit_bus_hire() 
	{
		if ($this->flexi_auth->is_logged_in()){
		
		$data['buses']=$this->bus_model->getplainresults('buses');
		
		$data['bushire']=$this->bus_model->getBusHire();

		$data['row']=$this->bus_model->getwojoinedtbrowswithwhereclause('bus_hire','hirers','HirerId','BusHireId',$this->uri->segment(3));
		
		$this->load->view('manage_bus_hire',$data);
								}else {
                 redirect('auth','refresh');
            }
	}
		function authorize_bus_hire() 
	{
		if ($this->flexi_auth->is_logged_in()){
		
		$data['hireid']=$this->uri->segment(3);
		
		$data['busid']=$this->uri->segment(4);
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['message']=NULL;
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$this->load->view('authorize_bus_hire',$data);
								}else {
                 redirect('auth','refresh');
            }
	}
			function bus_return() 
	{
		if ($this->flexi_auth->is_logged_in()){
		
		$data['hireid']=$this->uri->segment(3);
		
		$data['busid']=$this->uri->segment(4);
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['message']=NULL;
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$this->load->view('confirm_bus_return',$data);
								}else {
                 redirect('auth','refresh');
            }
	}
	
		function crud_prices()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
				
     	$this->load->view('manage_route_price',$data);
										}else {
                 redirect('auth','refresh');
            }
	}
	
		function crud_prices2()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['townstot']=$this->bus_model->getTownstot();
                $data['rep'] = NULL;
                $data['reporttitle'] = 1;
				
     	$this->load->view('manage_route_price',$data);
										}else {
                 redirect('auth','refresh');
            }
	}
	
		function crud_prices3()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
                
                $bt = $this->bus_model->getonerowfromonetbl('bustype_lkp', 'BusTypeId', $this->input->post('bustype'));
		if ($bt->ifalltowns==1)
                {
		$data['towns']=$this->bus_model->getTowns();
		
		$data['townstot']=$this->bus_model->getTownstot();
                }
                else {
                   $data['towns'] = $this->bus_model->getwojoinedtbresultswithwhereclause('bus_type_towns', 'towns_lkp', 'TownId', 'BusTypeId', $this->input->post('bustype'));
                   $data['townstot'] = $this->bus_model->getwojoinedtbtotrowswithwhereclause('bus_type_towns', 'towns_lkp', 'TownId', 'BusTypeId', $this->input->post('bustype'));
                }
                $data['rep'] = 1;
                $st = $this->bus_model->getonerowfromonetbl('seats_lkp', 'SeatTypeId', $this->input->post('seat'));
                $data['reporttitle'] = "Bus Type: ".$bt->BusType." | Seat Class: ".$st->SeatType;
                $data['seattypeid'] = $this->input->post('seat');
                $data['bustypeid'] = $this->input->post('bustype');
				
     	$this->load->view('manage_route_prices2',$data);
										}else {
                 redirect('auth','refresh');
            }
	}
	
		function edit_season($sid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['townstot']=$this->bus_model->getTownstot();
                $data['rep'] = NULL;
                $data['reporttitle'] = 1;
                $data['seasondetails'] = $this->bus_model->getonerowfromonetbl('season_lkp', 'SeasonId', $sid);
                $data['sid'] = $sid;
				
     	$this->load->view('manage_route_prices2',$data);
										}else {
                 redirect('auth','refresh');
            }
	}
		function crud_schedules()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
                
                $data['sms'] = NULL;
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function update_schedule_time($schedid = FALSE)
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
                
                $data['sms'] = NULL;
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                
                $data['schedid'] = $schedid;
                
                $data['sched'] = $this->bus_model->getBusSchedulesbySchedId($schedid);
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function crud_schedules_sms($schedid = FALSE)
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
                
                $data['sms'] = 1;
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['schedid'] = $schedid;
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
		function refine_schedules()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		$branchf = $this->input->post('townf');
		$brancht = $this->input->post('townt');
		$datef = $this->input->post('datef');
		$datet = $this->input->post('datet');
		$data['schedules']=$this->bus_model->getBusSchedules_refine($branchf,$brancht,$datef,$datet);
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
			function crud_payments()
	{	
		if ($this->flexi_auth->is_logged_in()){
		$data['buses']=$this->bus_model->getBuses();
		 $data['bus'] = "All";
		
		$data['payments']=$this->bus_model->getallpayments('','');
		
    	$this->load->view('manage_payments',$data);
				}else {
                 redirect('auth','refresh');
            }
	}
	
			function filter_payments_by_bus()
	{	if ($this->flexi_auth->is_logged_in())
            {		
         $this->form_validation->set_rules('busid', 'Bus', 'required');
			
		$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
	
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
		$data['buses']=$this->bus_model->getBuses();
		
		$data['payments']=$this->bus_model->getallpayments('','');
        $data['bus'] = "All";
    	$this->load->view('manage_payments',$data);
		}
		else // passed validation proceed to post success logic
		{
		$data['buses']=$this->bus_model->getBuses();
        $data['bus'] = $this->input->post('busid'); 
		$data['payments'] = $this->bus_model->getallpayments($this->input->post('busid'),$this->input->post('collecteddate'));
		$this->load->view('manage_payments',$data);
		}
	}else {
                 redirect('auth','refresh');
            }
	}
		function getDistance(){
		$URL = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$this->input->post('townFrom')."&destinations=".$this->input->post('townTo')."&mode=driving&language=en-EN&sensor=false";
		$ch = curl_init($URL);
			
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$results = curl_exec($ch);
		
		curl_close($ch);		
		
		$result = json_decode($results);
		
		//$data['distance'] = $result->rows[0]->elements[0]->distance->text;
		
		//echo $data['distance'];

		}
		
                function crud_routes()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$this->load->view('manage_bus_routes',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		
                function edit_town($townid)
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
                
                $data['stown'] = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townid);
                $data['townid'] = $townid;
		
		$this->load->view('manage_bus_routes',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		
                function update_town()
	{	
		if ($this->flexi_auth->is_logged_in()){
		
		$updatedata =  array(
                                    'TownName'=> $this->input->post('town'),
                                    'TownCode'=> $this->input->post('towncode'),
                                    'TownSCode'=> $this->input->post('stowncode'),
                                    'Currency'=> $this->input->post('currency'),
                                    );
           
            // run insert model to write data to db
                       $tablename = 'towns_lkp';
                        $pkname = 'TownId';
                        $pkvalue = $this->input->post('townid');//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
            //redirect('admins/crud_routes#tab5','refresh');
                        $data['url'] = 'admins/crud_routes#tab5';
                        $this->load->view('data_success',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        function upload_slip() 
	{
           $bankingid = $this->input->post('bankingid');
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
                        $config['upload_path'] = './bankslips/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('bankslip');
                        $data3 =  $this->upload->data();
                        $slipname = $data3['file_name'];
            $updatedata =  array(
                                    'bankslip' => $slipname
                                    );
           
            // run insert model to write data to db
                       $tablename = 'banking';
                        $pkname = 'BankingId';
                        $pkvalue = $this->input->post('bankingid');//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			 $data['bankingid'] = $bankingid;
                
                $data['message'] = "Bankslip successfully uploaded";
                $this->load->view('upload_bank_slip',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function update_exchange_rate() 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'kes' => $this->input->post('kes'),
                                    'ugx' => $this->input->post('ugx'),
                                    'tzs' => $this->input->post('tzs'),
                                    'usd' => $this->input->post('usd'),
                                    'euro' => $this->input->post('euro'),
                                    'dateadded' => date('Y-m-d H:i:s'),
                                    );

            
            // run insert model to write data to db
                       $tablename = 'exchange_rates';
                        $pkname = 'exchange_rates_id';
                        $pkvalue = 1;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_settings');
                        $data['url'] = 'admins/crud_settings';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function lock_schedule($schedid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'lockstatus' => 1
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $schedid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_vouchers#delete');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function unlock_schedule($schedid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'lockstatus' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $schedid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_vouchers#delete');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function delete_voucher($voucherid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'vouchers';
                        $pkname = 'vouchers_id';
                        $pkvalue = $voucherid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_vouchers#delete');
                        $data['url'] = 'admins/crud_vouchers#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function delete_customer($custid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'customer';
                        $pkname = 'Cust_id';
                        $pkvalue = $custid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			redirect('admins/crud_vouchers#delete');
            }else {
                 redirect('auth','refresh');
            }

	}
			function delete_agent($aid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'agent';
                        $pkname = 'Agent_id';
                        $pkvalue = $aid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_agents');
                        $data['url'] = 'admins/crud_agents';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function delete_season($seasonid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $pkvalue = $seasonid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_prices2#tab3');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function approve_price($priceid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
                        $data =  array(
                                    'ApprovedBy' => $this->flexi_auth->get_user_id(),
                                    'ApprovedStatus' => 1
                                    );
                                    $this->db->where('PriceId',$priceid);
            $this->db->update('route_prices',$data);
           

			//redirect('admins/crud_prices2');
                        $data['url'] = 'admins/crud_prices2';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function activate_season($seasonid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
                        $data =  array(
                                    'IsActive' => 0
                                    );
                                    $this->db->where('IsActive',1);
            $this->db->update('season_lkp',$data);
            $updatedata =  array(
                                    'IsActive' => 1
                                    );

            
            // run insert model to write data to db
                       $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $pkvalue = $seasonid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_prices2#tab3');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function deactivate_season($seasonid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
                        
            $updatedata =  array(
                                    'IsActive' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $pkvalue = $seasonid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_prices2#tab3');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
			function default_season($seasonid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
                        $data =  array(
                                    'IsDefault' => 0
                                    );
                                    $this->db->where('IsDefault',1);
            $this->db->update('season_lkp',$data);
            $updatedata =  array(
                                    'IsDefault' => 1
                                    );

            
            // run insert model to write data to db
                       $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $pkvalue = $seasonid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_prices2#tab3');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
		function delete_bus($busid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'buses';
                        $pkname = 'BusId';
                        $pkvalue = $busid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_bus#delete');
                        $data['url'] = 'admins/crud_bus#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
		function delete_banking($bankingid) 
	{
	            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'banking';
                        $pkname = 'BankingId';
                        $pkvalue = $bankingid;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_banking#delete');
                        $data['url'] = 'admins/crud_banking#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
		function delete_bus_maintenance($id) 
	{
		            if ($this->flexi_auth->is_logged_in())
            {
            //$campusdet = $this->main_model->getonerowfromonetbl('campus','campus_id',$campid);//pass tablename
            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_maintenance';
                        $pkname = 'MaintenanceId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

		//redirect('admins/crud_maintenance#delete');
                        $data['url'] = 'admins/crud_maintenance#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
		function delete_bus_terminus($id) 
	{
				            if ($this->flexi_auth->is_logged_in())
            {		
			$data['termini']=$this->bus_model->getBusTermini();

            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_termini';
                        $pkname = 'TerminusId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

		//redirect('admins/crud_termini#delete');
                        $data['url'] = 'admins/crud_termini#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }

	}
		function delete_staff($id) 
	{
	   if ($this->flexi_auth->is_logged_in())
            {		
			$data['staff']=$this->bus_model->getStaff();

            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_staff';
                        $pkname = 'StaffId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

				//redirect('admins/crud_staff#delete');
                        $data['url'] = 'admins/crud_staff#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }
	}
		function delete_bus_route($id) 
	{
		   if ($this->flexi_auth->is_logged_in())
            {		

            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_routes';
                        $pkname = 'RouteId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

		//redirect('admins/crud_routes#delete');
                        $data['url'] = 'admins/crud_routes#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }
		
	}
		function delete_route_price($id) 
	{
		   if ($this->flexi_auth->is_logged_in())
            {		

            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'route_prices';
                        $pkname = 'PriceId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

	    //redirect('admins/crud_prices#delete');
                        $data['url'] = 'admins/crud_prices#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }
		
		
	}
		function delete_bus_schedule($id) 
	{
			   if ($this->flexi_auth->is_logged_in())
            {		

            $updatedata =  array(
                                    'status' => 0
                                    );

            
            // run insert model to write data to db
                       $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $id;//do function to generate pkvalue
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

	    //redirect('admins/crud_schedules#delete');
                        $data['url'] = 'admins/crud_schedules#delete';
                        $this->load->view('data_success',$data);
            }else {
                 redirect('auth','refresh');
            }
		
		
	}
		function edit_bus() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['makes']=$this->bus_model->getMakes();

		$data['busdetails']=$this->bus_model->getThisBus($this->uri->segment(3));
		
		$this->load->view('manage_bus',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function edit_bus_terminus() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['regions']=$this->bus_model->getRegions();
		
		$data['termini']=$this->bus_model->getBusTermini();
		
		$data['towns']=$this->bus_model->getTowns();

		$data['terminusdetails']=$this->bus_model->getThisTerminus($this->uri->segment(3));
		
		$this->load->view('manage_bus_termini',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
	
		function edit_bus_maintenance() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$data['maintenance']=$this->bus_model->getBusMaintenance();
		
		$data['maintenancetypes']=$this->bus_model->getMaintenanceTypes();

		$data['maintenancedetails']=$this->bus_model->getThisMaintenance($this->uri->segment(3));
		
		$this->load->view('manage_bus_maintenance',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function edit_staff() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['staff']=$this->bus_model->getStaff();
		
		$data['titles']=$this->bus_model->getplainresults('job_title_lkp');;
		
		$data['row']=$this->bus_model->getonerowfromonetbl('bus_staff','StaffId',$this->uri->segment(3));
		
		$this->load->view('manage_staff',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function edit_route_price() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
		
		$data['prices']=$this->bus_model->getRoutePrices();

		$data['pricedetails']=$this->bus_model->getThisRoutePrice($this->uri->segment(3));
		
		$this->load->view('manage_route_prices2',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function edit_bus_schedule() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
		
		$data['scheduleroute']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffId',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffId',2);
		
		$data['scheduledetails']=$this->bus_model->getThisBusSchedule($this->uri->segment(3));
                
                
                
                $data['prices']=$this->bus_model->getRoutePrices();
		
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$this->load->view('manage_bus_schedule',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_manifest() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
		
		$data['scheduleroute']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffId',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffId',2);
		
		
		$data['scheduledetails']=$this->bus_model->getThisBusSchedule($this->uri->segment(3));
		$data['manifest']=$this->bus_model->getSpecBusSchedule($this->uri->segment(3));
                
                $data['schedid'] = $this->uri->segment(3);
                
                $data['prices']=$this->bus_model->getRoutePrices();
		
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$this->load->view('manage_manifest',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function crud_arrivals() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
                            
		$data['manifests'] = NUll;
                $data['manifestlist'] = NUll;
		$this->load->view('manage_arrivals',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function search_manifest() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
                $manifestno = $this->input->post('manifestno');  
                //$manifestno = 23;
                //echo $manifestno;
		$data['manifests'] = $this->bus_model->getbusmanifest($manifestno);
                $data['manifestlist'] = 1;
		$this->load->view('manage_arrivals',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function confirm_arrival($manid) 
	{
			if ($this->flexi_auth->is_logged_in())
            {	 
                $data['manid'] = $manid;
                $data['message'] = NULL;
		$this->load->view('arrival_confirm',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
	function confirm_arrival_confim()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$manid = $this->input->post('manid');
		
		$this->form_validation->set_rules('comments', 'Comments', 'trim|required');
		
		$this->form_validation->set_rules('arrtype', 'Arrival', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
                     $data['manid'] = $manid;
                $data['message'] = NULL;
		$this->load->view('arrival_confirm',$data);
		}
		else
		{		
			$updatedata = array(
			'arrivalstatus' => 1,
			'arrivalconfirmedby' => $this->flexi_auth->get_user_id(),
			'arrivaldate' => date('Y-m-d H:i:s'),
			'arrivalcomment' => $this->input->post('comments'),
			'arrivaltype' => $this->input->post('arrtype')							
		);
					// run insert model to write data to db
                        $tablename = 'manifest';
                        $pkname = 'manifest_id';
                        $pkvalue = $manid;
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      $man = $this->bus_model->getonerowfromonetbl('manifest','manifest_id',$manid);
                      $sched = $this->bus_model->getonerowfromonetbl('bus_schedule','ScheduleId',$man->ScheduleId);
                      if ($sched->BusLkp<>NULL or $sched->BusLkp<>"")
                      {
                          $rt = $this->bus_model->getonerowfromonetbl('bus_routes','RouteId',$sched->RouteLkp);
                          $townsforroute = trim($rt->towns,',');
                          $towns = explode(",",$townsforroute);
                          $townstot = count($towns);
                          $manifeststarttownpos = array_search($man->townf, $towns);
                          $manifestendtownpos = array_search($man->townt, $towns);
                          if ($manifestendtownpos == ($townstot-1))
                          {
                      $updatedata1 = array(
			'IsActive' => 1						
		);
					// run insert model to write data to db
                        $tablename1 = 'buses';
                        $pkname1 = 'BusId';
                        $pkvalue1 = $sched->BusLkp;
                      $this->bus_model->dbupdate($tablename1, $updatedata1, $pkname1, $pkvalue1);
                      
                      //update conductor active
                        $updatedata2 = array(
			'IsActive' => 1							
		);
                        $tablename2 = 'bus_staff';
                        $pkname2 = 'StaffId';
                        $pkvalue2 = $sched->Conductor;//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename2, $updatedata2, $pkname2, $pkvalue2);
                          }
                        //get drivers
                      $drivers = $this->bus_model->getresultfromonetbl('driver_schedules','ScheduleId',$man->ScheduleId);
                      if ($drivers<>NULL)
                      {
                          
                          foreach ($drivers as $dr)
                          {
                              $driverstarttownpos = array_search($dr->townfrom, $towns);
                              $driverendtownpos = array_search($dr->townto, $towns);
                              if ($driverstarttownpos <= $manifeststarttownpos and $driverendtownpos >= $manifestendtownpos)
                              {
                                  //update conductor active
                                    $updatedata3 = array(
                                    'IsActive' => 1							
                                        );
                                    $tablename3 = 'bus_staff';
                                    $pkname3 = 'StaffId';
                                    $pkvalue3 = $dr->driver_id;//do function to generate pkvalue
                                    $this->bus_model->dbupdate($tablename3, $updatedata3, $pkname3, $pkvalue3);
                              }
                              
                          }
                      }
                        
                      }
			 $data['manid'] = $manid;
                $data['message'] = "Bus arrival successfuly confirmed!";
		$this->load->view('arrival_confirm',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function close_schedule($scheduleid) 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                $data['rep'] = 1;
                $tickets = $this->bus_model->getticketsbyschedule($scheduleid);
                $data['scheduleid'] = $scheduleid;
                if ($tickets==NULL):
                    $data['tickets'] = $tickets;
                /*
                $updatedata = array(
			'IsActive' => 0						
		);
					// run insert model to write data to db
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $scheduleid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      */
                $data['closedata'] = "Since there were no tickets booked or reserved, the schedule was successfully closed!";
                else:
                    $data['tickets'] = $tickets;
                    $data['closedata'] = "There are booked/reserved tickets in this schedule. <br />";
                endif;
				
                $this->load->view('manage_bus_schedule',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function close_bus_schedule() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
                $updatedata = array(
			'IsActive' => 0						
		);
					// run insert model to write data to db
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $this->input->post('scheduleid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      
                //redirect('admins/crud_schedules','refresh');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
		function refund_ticket($ticketid) 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		
                $updatedata = array(
			'cancel_reason' => "Bus Schedule Closed",
			'cancelled_by' => $this->flexi_auth->get_user_id(),
			'cancel_date' => date('Y-m-d'),
			'ticketstatus' => 'Cancelled'							
		);
					// run insert model to write data to db
                        $tablename1 = 'ticketing';
                        $pkname1 = 'TicketId';
                        $pkvalue1 = $ticketid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename1, $updatedata, $pkname1, $pkvalue1);
			$td = $this->bus_model->getonerowfromonetbl($tablename1, $pkname1, $pkvalue1);
                if ($td->t_currency==1):
                        $curtable = 'KeAmount';
                        $amount = $td->amount;
                    endif;
                    if ($td->t_currency==2):
                        $curtable = 'UgAmount';
                        $amount = $td->amountug;
                    endif;
                    if ($td->t_currency==3):
                        $curtable = 'TzAmount';
                        $amount = $td->amounttz;
                    endif;
                    if ($td->t_currency==4):
                        $curtable = 'UsdAmount';
                        $amount = $td->amountusd;
                    endif;
                    if ($td->t_currency==5):
                        $curtable = 'EurAmount';
                        $amount = $td->amounteuro;
                    endif;
                    $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                    $terd = $this->bus_model->getonerowfromonetblgreaterthanzero('bus_termini','TownLkp',$tid);
                    if($terd==NULL):
                        $terminus = 0;
                    else:
                        $terminus = $terd->TerminusId;
                    endif;
                        $pettno = $this->bus_model->_generatePettyCashNo();
			$insdata = array(
			'StationId' => $tid,
			'SubstationId' => $terminus,
			'CashDate' => date('Y-m-d'),
			'CashierId' => $this->flexi_auth->get_user_id(),
			'AccountCodeId' => 1,
			'Description' => "Closed bus schedules",
			'IssuedTo' => $td->clientname,
			'Particulars' => "Closed bus schedules",
			'AuthorizedBy' => $this->flexi_auth->get_user_id(),
			$curtable => $amount,
			'Currency' => $td->t_currency,
                            'pettycashno' => $pettno);
                        
					// run insert model to write data to db
                        $tablename = 'petty_cash';
                        $pkname = 'PettyCashId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        $pd = $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                        
			redirect('admins/print_pettycash/'.$pd->$pkname,'refresh');
		}else {
                 redirect('auth','refresh');
            }
	}
        
		function move_ticket_to_new_schedule($scheduleidticketidoldscheduleid) 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$st = explode(":",$scheduleidticketidoldscheduleid);
		$scheduleid = $st[0];
		$ticketid = $st[1];
		$oldscheduleid = $st[2];
                    
                $updatedata = array(
			'schedule_id' => $scheduleid						
		);
					// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $pkvalue = $ticketid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                redirect('admins/close_schedule/'.$oldscheduleid.'/#tab5');
				
                
		}else {
                 redirect('auth','refresh');
            }
	}
		function edit_bus_route() 
	{
			if ($this->flexi_auth->is_logged_in())
            {	
		$data['regions']=$this->bus_model->getRegions();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['times'] = $this->bus_model->getplainresults('reporting_departure_times_lkp');
		
		$data['codes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['routedetails']=$this->bus_model->getThisRoute($this->uri->segment(3));
		
		$this->load->view('manage_bus_routes',$data);
		}else {
                 redirect('auth','refresh');
            }
	}
			function create_staff()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		
		$this->form_validation->set_rules('idnumber', 'Id Number', 'trim|required');
		
		$this->form_validation->set_rules('staffnumber', 'Staff Number', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		
                        $data['staff']=$this->bus_model->getStaff();

                        $this->load->view('manage_staff',$data);
		}
		else
		{		
                    $config['upload_path'] = './staffphotos/';
                        $config['allowed_types'] = '*';
                        if (!empty($_FILES['sphoto']['name']))
                        {
                        $this->load->library('upload', $config);

                       $this->upload->do_upload('sphoto');
                        $data3 =  $this->upload->data();
                        $photoname = $data3['file_name'];
                        }
                        else {
                            $photoname = NULL;
                        }
                        if (!empty($_FILES['licencecopy']['name']))
                        {
                        $this->load->library('upload', $config);

                       $this->upload->do_upload('licencecopy');
                        $data4 =  $this->upload->data();
                        $licencecopy = $data4['file_name'];
                        }
                        else {
                            $licencecopy = NULL;
                        }
                        if (!empty($_FILES['psvcopy']['name']))
                        {
                        $this->load->library('upload', $config);
                        
                        $this->upload->do_upload('psvcopy');
                        $data5 =  $this->upload->data();
                        $psvcopy = $data5['file_name'];
                        }
                        else {
                            $psvcopy = NULL;
                        }
			$insdata = array(
			'StaffName' => $this->input->post('name'),
			'StaffIdNumber' => $this->input->post('idnumber'),
			'StaffNumber' => $this->input->post('staffnumber'),
			'StaffJobLkp' => $this->input->post('title'),
			'StaffAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),		
			'SPhoto' => $photoname,
			'LicenceNo' => $this->input->post('licenceno'),
			'LicenceCopyFile' => $licencecopy,
			'LicenceExpiry' => $this->input->post('expirydate'),
			'PSVNo' => $this->input->post('licenceno'),
			'PSVCopy' => $psvcopy,			
			'station_at' => 1,
			'date_available_from' => date('Y-m-d'),
			'time_available_from' => date('H:i')							
		);
					// run insert model to write data to db
                        $tablename = 'bus_staff';
                        $pkname = 'StaffId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      //redirect('admins/crud_staff','refresh');
                        $data['url'] = 'admins/crud_staff';
                        $this->load->view('data_success',$data);
			//$this->load->view('manage_staff');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
	function create_banking()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		//$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                //$data['termini'] = $this->bus_model->getplainresults('bus_termini');
		
		$this->form_validation->set_rules('terminusid', 'Terminus', 'trim|required');
		
		$this->form_validation->set_rules('currency', 'Currency', 'trim|required');
		
		$this->form_validation->set_rules('totalamount', 'Total Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('bankingdate', 'Banking Date', 'required');
		
		$this->form_validation->set_rules('refno', 'Reference Number', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
                    $data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                $data['termini'] = $this->bus_model->getplainresults('bus_termini');
			$this->load->view('manage_banking',$data);
		}
		else
		{		
			$insdata = array(
			'TerminusId' => $this->input->post('terminusid'),
			'BCurrency' => $this->input->post('currency'),
			'TotalAmount' => $this->input->post('totalamount'),
			'BankingDate' => $this->input->post('bankingdate'),
			'AddedBy' => $this->flexi_auth->get_user_id(),
			'refno' => $this->input->post('refno')							
		);
					// run insert model to write data to db
                        $tablename = 'banking';
                        $pkname = 'BankingId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_banking');
                        $data['url'] = 'admins/crud_banking';
                        $this->load->view('data_success',$data);
                      //$this->load->view('manage_staff');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
        
	function corp_depost_cash()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('mpesatransno', 'Mpesa Transaction Number', 'trim|required|callback_mpesa_check');
		
		if($this->form_validation->run() == FALSE)
		{
                    $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
						
                    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    
                   $custamountcre=$this->bus_model->get_customer_total_credited($cust->cust_id);
			$data['totcredits'] = abs($custamountcre->camountcred);
                         $custamountdeb=$this->bus_model->get_customer_total_debited($cust->cust_id);
			$data['totdebits'] = $custamountdeb->camountdeb;
                 $data['tickets'] = NULL;
                         $data['deposits'] = NULL;
                        
                       if ($this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id)->cust_type==1)
                       {
                           $data['acctype'] = "Deposit";
                       }
                       else {
                           $data['acctype'] = "Contractual";
                       }
		$data['cust_id'] = $cust->cust_id;
		$this->load->view('manage_corp_accounts',$data);
		}
		else
		{	$amt = $this->bus_model->get_mpesa_trans_det($this->input->post('mpesatransno'));	
			$insdata = array(
			'cust_id' => $this->input->post('cust_id'),
			'camount' => '-'.$amt->empesa_amount,
			'deptype' => 3,
			'mpesarefno' => $this->input->post('mpesatransno'),
			'user' => $this->flexi_auth->get_user_id(),
                        'approvedstatus' => 1						
		);
					// run insert model to write data to db
                        $tablename = 'corp_transactions';
                        $pkname = 'corp_transactions_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      
                      $updatedata1 = array(
			'trans_status' => 1						
		);
					// run insert model to write data to db
                        $tablename1 = 'mpesa_trans';
                        $pkname1 = 'mpesa_id';
                        $pkvalue1 = $amt->mpesa_id;//do function to generate pkvalue
                      
                      $this->bus_model->dbupdate($tablename1, $updatedata1, $pkname1, $pkvalue1);
                      
			//redirect('admins/crud_acc_statements');
                        $data['url'] = 'admins/crud_acc_statements';
                        $this->load->view('data_success',$data);
                      //$this->load->view('manage_staff');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
        
	function corp_deposit_payment()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
                    $custid = $this->input->post('custid');
		if ($this->input->post('paymode')==1)
                {
                    $this->form_validation->set_rules('depamount', 'Cash Amount', 'trim|required|is_numeric');
                }
                if ($this->input->post('paymode')==2)
                {
                    $this->form_validation->set_rules('chequerefno', 'Ref No', 'trim|required');
                    $this->form_validation->set_rules('cheqamount', 'Cheque Amount', 'trim|required|is_numeric');
                }
                if ($this->input->post('paymode')==3)
                {
		$this->form_validation->set_rules('mpesatransno', 'Mpesa Transaction Number', 'trim|required|callback_mpesa_check');
                }
		
		if($this->form_validation->run() == FALSE)
		{
                    $data['countries']=$this->bus_model->getcountries();
			
		$data['customers']=$this->bus_model->getplainresults('customer');
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
                        $data['custid'] = $custid;
		
		$this->load->view('manage_corp_customers',$data);
		}
		else
		{
                    if ($this->input->post('paymode')==1)
                {	
			$insdata = array(
			'cust_id' => $custid,
			'camount' => '-'.$this->input->post('depamount'),
			'deptype' => 1,
			'user' => $this->flexi_auth->get_user_id(),
                        'approvedstatus' => 1
		);
                }
                if ($this->input->post('paymode')==2)
                {
			$insdata = array(
			'cust_id' => $custid,
			'camount' => '-'.$this->input->post('cheqamount'),
			'deptype' => 2,
			'chequerefno' => $this->input->post('chequerefno'),
			'user' => $this->flexi_auth->get_user_id(),
                        'approvedstatus' => 0
		);
                }
                     if ($this->input->post('paymode')==3)
                {
                    $amt = $this->bus_model->get_mpesa_trans_det($this->input->post('mpesatransno'));	
			$insdata = array(
			'cust_id' => $custid,
			'camount' => '-'.$amt->empesa_amount,
			'deptype' => 3,
			'mpesarefno' => $this->input->post('mpesatransno'),
			'user' => $this->flexi_auth->get_user_id(),
                        'approvedstatus' => 1						
		);
                }
					// run insert model to write data to db
                        $tablename = 'corp_transactions';
                        $pkname = 'corp_transactions_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                       if ($this->input->post('paymode')==3)
                {
                      $updatedata1 = array(
			'trans_status' => 1						
		);
					// run insert model to write data to db
                        $tablename1 = 'mpesa_trans';
                        $pkname1 = 'mpesa_id';
                        $pkvalue1 = $amt->mpesa_id;//do function to generate pkvalue
                      
                      $this->bus_model->dbupdate($tablename1, $updatedata1, $pkname1, $pkvalue1);
                }
			//redirect('admins/crud_customers');
                        $data['url'] = 'admins/crud_customers';
                        $this->load->view('data_success',$data);
                      //$this->load->view('manage_staff');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
	
	function mpesa_check($mpesatransno)
	{
		if ($this->bus_model->is_mpesatransno_valid($mpesatransno))
		{
			return TRUE;
		}
		else
		{
		    $this->form_validation->set_message('mpesa_check', 'Invalid Transaction No');
			return FALSE;
		}
	}
	
	function date_check()
	{
            $fromdate = $this->input->post('departuredatetime');
            $todate = $this->input->post('departuredatetime2');
		if (strtotime($fromdate) <= strtotime($todate))
		{
			return TRUE;
		}
		else
		{
		    $this->form_validation->set_message('date_check', 'From date cannot be greater than To date!');
			return FALSE;
		}
	}
	
	function pettycash_check($amount)
	{
            $currency = $this->input->post('currency');
            //$amount = $this->input->post('currency');
		if ($this->bus_model->is_pettycash_available($currency,$amount))
		{
			return TRUE;
		}
		else
		{
		    $this->form_validation->set_message('pettycash_check', 'You do not have enough money in your till!');
			return FALSE;
		}
	}
	
	function voucher_check()
	{
            if ($this->input->post('bookingmode')==3) 
            {
                $voucheramnt = $this->input->post('voucheramnt');
                $addedvoucheramnt = $this->input->post('addedvoucheramnt');
                $totalamount = $this->input->post('amount');
                $totvoucher = $voucheramnt + $addedvoucheramnt;
                if ($totalamount <= $totvoucher)
                {
                    return TRUE;
                }
                else {
                    $this->form_validation->set_message('voucher_check', 'Voucher Amount Less');
			return FALSE;
                }
            }
            else {
                return TRUE;
            }
	}
	
	function driver_check()
	{
            $array = $this->input->post('driver');
            if(count(array_unique($array))<count($array))
            {
                // Array has duplicates
                $this->form_validation->set_message('driver_check', 'Same driver has been assigned more than once!');
                    return FALSE;
            }
            else
            {
                // Array does not have duplicates
                return TRUE;
            }
	}
        
        function update_banking()	
	{	
            $bankingid = $this->input->post('bankingid');
            $data['bankingid'] = $bankingid;
		if ($this->flexi_auth->is_logged_in())
            {	
                    
		$data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
                $data['termini'] = $this->bus_model->getplainresults('bus_termini');
		
		$this->form_validation->set_rules('terminusid', 'Terminus', 'trim|required');
		
		$this->form_validation->set_rules('currency', 'Currency', 'trim|required');
		
		$this->form_validation->set_rules('totalamount', 'Total Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('bankingdate', 'Banking Date', 'required');
		
		$this->form_validation->set_rules('refno', 'Reference Number', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
             $data['banking']=$this->bus_model->getwojoinedtbresults('banking','bus_termini','TerminusId');
             $data['termini'] = $this->bus_model->getplainresults('bus_termini');
			 $this->load->view('manage_banking',$data);
		}
		else
		{		
			$updatedata = array(
			'TerminusId' => $this->input->post('terminusid'),
			'BCurrency' => $this->input->post('currency'),
			'TotalAmount' => $this->input->post('totalamount'),
			'BankingDate' => $this->input->post('bankingdate'),
			'refno' => $this->input->post('refno')							
		);
					// run insert model to write data to db
                        $tablename = 'banking';
                        $pkname = 'BankingId';
                        $pkvalue = $this->input->post('bankingid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_staff','refresh');
			//redirect('admins/crud_banking','refresh');
                        $data['url'] = 'admins/crud_banking';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
        
        function autoschedulebuses(){
		//get available buses
		//$availablebuses = 
		$scheduledbuses = $this->bus->model->getbusesforscheduling();
		
		
		
		}
		

		function create_petty_cash()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['users']=$this->bus_model->getplainresults('admins');
		
		$data['towns']=$this->bus_model->getplainresults('towns_lkp');
		
		$data['termini']=$this->bus_model->getplainresults('bus_termini');
		
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		
		$this->form_validation->set_rules('pickuppoint', 'Terminus', 'trim|required');
		
		$this->form_validation->set_rules('issueddate', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('currency', 'Currency', 'required');
		
		$this->form_validation->set_rules('accountcode', 'Account Code', 'trim|required');
		
		$this->form_validation->set_rules('issuedto', 'Issued To', 'trim|required');
		
		$this->form_validation->set_rules('particulars', 'Particulars', 'trim|required');
		
		$this->form_validation->set_rules('authorizedby', 'Authorized By', 'trim|required');
		
		$this->form_validation->set_rules('clerk', 'Clerk', 'trim|required');
		
		$this->form_validation->set_rules('keamount', 'Amount', 'trim|required|is_numeric');
		
		
		if($this->form_validation->run() == FALSE)
		{
		$data['termini']=$this->bus_model->getBusTermini();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['codes']=$this->bus_model->getplainresults('account_codes');
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['pettycash']=$this->bus_model->getpettyCash();
		
		$this->load->view('manage_petty_cash',$data);
		}
		else
		{	
                    if ($this->input->post('currency')==1):
                        $curtable = 'KeAmount';
                    endif;
                    if ($this->input->post('currency')==2):
                        $curtable = 'UgAmount';
                    endif;
                    if ($this->input->post('currency')==3):
                        $curtable = 'TzAmount';
                    endif;
                    if ($this->input->post('currency')==4):
                        $curtable = 'UsdAmount';
                    endif;
                    if ($this->input->post('currency')==5):
                        $curtable = 'EurAmount';
                    endif;
                    
                    
                        $pettno = $this->bus_model->_generatePettyCashNo();
			$insdata = array(
			'StationId' => $this->input->post('town'),
			'SubstationId' => $this->input->post('pickuppoint'),
			'CashDate' => $this->input->post('issueddate'),
			'CashierId' => $this->input->post('clerk'),
			'AccountCodeId' => $this->input->post('accountcode'),
			'Description' => $this->input->post('particulars'),
			'IssuedTo' => $this->input->post('issuedto'),
			'Particulars' => $this->input->post('particulars'),
			'AuthorizedBy' => $this->input->post('authorizedby'),
			$curtable => $this->input->post('keamount'),
			'Currency' => $this->input->post('currency'),
                            'pettycashno' => $pettno);
                        
					// run insert model to write data to db
                        $tablename = 'petty_cash';
                        $pkname = 'PettyCashId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        $pd = $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                        
			redirect('admins/print_pettycash/'.$pd->$pkname,'refresh');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		

		function create_petty_cash_clerk()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		//$data['users']=$this->bus_model->getplainresults('admins');
		
		//$data['towns']=$this->bus_model->getplainresults('towns_lkp');
		
		//$data['termini']=$this->bus_model->getplainresults('bus_termini');
		
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		
		$this->form_validation->set_rules('pickuppoint', 'Terminus', 'trim|required');
		
		$this->form_validation->set_rules('issueddate', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('currency', 'Currency', 'required');
		
		$this->form_validation->set_rules('accountcode', 'Account Code', 'trim|required');
		
		$this->form_validation->set_rules('issuedto', 'Issued To', 'trim|required');
		
		$this->form_validation->set_rules('particulars', 'Particulars', 'trim|required');
		
		//$this->form_validation->set_rules('authorizedby', 'Authorized By', 'trim|required');
		
		$this->form_validation->set_rules('clerk', 'Clerk', 'trim|required');
		
		$this->form_validation->set_rules('keamount', 'Amount', 'trim|required|is_numeric|callback_pettycash_check');
		
		
		if($this->form_validation->run() == FALSE)
		{
		$user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                $data['userid'] = $user[0]->uacc_id;
                $data['pickuppoint'] = $user[0]->t_termini;
                $data['town'] = $user[0]->t_station;
		
		//$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['codes']=$this->bus_model->getplainresults('account_codes');
		
		$data['towns']=$this->bus_model->getTowns();
		
		$data['pettycash']=$this->bus_model->getpettyCash();
		
     	$this->load->view('manage_petty_cash_clerk',$data);
		}
		else
		{	
                    if ($this->input->post('currency')==1):
                        $curtable = 'KeAmount';
                    endif;
                    if ($this->input->post('currency')==2):
                        $curtable = 'UgAmount';
                    endif;
                    if ($this->input->post('currency')==3):
                        $curtable = 'TzAmount';
                    endif;
                    if ($this->input->post('currency')==4):
                        $curtable = 'UsdAmount';
                    endif;
                    if ($this->input->post('currency')==5):
                        $curtable = 'EurAmount';
                    endif;
                    
                    
                        $pettno = $this->bus_model->_generatePettyCashNo();
			$insdata = array(
			'StationId' => $this->input->post('town'),
			'SubstationId' => $this->input->post('pickuppoint'),
			'CashDate' => $this->input->post('issueddate'),
			'CashierId' => $this->input->post('clerk'),
			'AccountCodeId' => $this->input->post('accountcode'),
			'Description' => $this->input->post('particulars'),
			'IssuedTo' => $this->input->post('issuedto'),
			'Particulars' => $this->input->post('particulars'),
			//'AuthorizedBy' => $this->input->post('authorizedby'),
			$curtable => $this->input->post('keamount'),
			'Currency' => $this->input->post('currency'),
                            'pettycashno' => $pettno);
                        
					// run insert model to write data to db
                        $tablename = 'petty_cash';
                        $pkname = 'PettyCashId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        $pd = $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                        
			redirect('admins/print_pettycash/'.$pd->$pkname,'refresh');
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
        
        
        function print_pettycash($pid){
            
            $pt = $this->bus_model->getonerowfromonetbl('petty_cash', 'PettyCashId', $pid);
            if ($pt->Currency==1):
                        $curtable = 'KeAmount';
            $shortcurrency = 'Ksh';
                    endif;
                    if ($pt->Currency==2):
                        $curtable = 'UgAmount';
            $shortcurrency = 'Ush';
                    endif;
                    if ($pt->Currency==3):
                        $curtable = 'TzAmount';
            $shortcurrency = 'Tsh';
                    endif;
                    if ($pt->Currency==4):
                        $curtable = 'UsdAmount';
            $shortcurrency = '$';
                    endif;
                    if ($pt->Currency==5):
                        $curtable = 'EurAmount';
            $shortcurrency = 'EUR';
                    endif;
		//$this->user = $this->flexi_auth->get_user_by_id($pt->CashierId)->result();
                        //$data['payee'] = $this->user[0]->upro_first_name." ".$this->user[0]->upro_last_name;
                        $data['payee'] = $pt->IssuedTo;
                        $data['particulars'] = $pt->Description;
                        $ac = $this->bus_model->getonerowfromonetbl('account_codes', 'AccountCodeId', $pt->AccountCodeId);
                        $data['ac'] = $ac->AccountCode;
                        $data['amount'] = $pt->$curtable;
                        $data['pettno'] = $pt->pettycashno;
                        $this->user2 = $this->flexi_auth->get_user_by_id($pt->CashierId)->result();
                        $data['preparedby'] = $this->user2[0]->upro_first_name." ".$this->user2[0]->upro_last_name;
                        $data['amountinwords'] = $this->bus_model->convert_number_to_words($pt->$curtable);
                        $data['shortcurrency'] = $shortcurrency;
                        $this->load->view('pettycash',$data);
		
		}
  		function create_ticket_device()	
	{	
             // http://178.238.232.121/tms/admins/create_ticket_device?scheduleid=68&stationfrom=Mombasa&stationto=Nairobi&clientname=MESHACK&idnumber=635874&mobileno=0710228024&seatno=11&currency=KES&voucherno=None&mpesaref=None&bookingmode=1&userid=1&nationality=Kenya&channel=2&cardno=xx                        

		$townfid=$this->bus_model->getfieldvaluefromrow('towns_lkp','TownId','TownName',$_GET['stationfrom']); 
$towntid=$this->bus_model->getfieldvaluefromrow('towns_lkp','TownId','TownName',$_GET['stationto']);	
 if ($_GET['currency']=="KES")
                             {
                                 $cur = 1;
                             }
                             if ($_GET['currency']=="UGX")
                             {
                                 $cur = 2;
                             }
                             if ($_GET['currency']=="TZS")
                             {
                                 $cur = 3;
                             }
		if ($cur==1)
                         {
                             $amounttable = 'amount';
                         } 
                         if ($cur==2)
                         {
                             $amounttable = 'amountug';
                         } 
                         if ($cur==3)
                         {
                             $amounttable = 'amounttz';
                         } 
                         if ($cur==4)
                         {
                             $amounttable = 'amountusd';
                         }
						  $this->user=$this->flexi_auth->get_user_by_id($_GET['userid'])->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                $userid = $this->user[0]->uacc_id;
                if ($_GET['userid']==0)
                {
                    $userid = 0;
                }
				
				 $status='Active';
			 if($_GET['bookingmode']=='MPesa'){$mpesaref=$_GET['mpesaref'];} else { $mpesaref='';}
                         $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $_GET['scheduleid']);
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townfid);
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townfid);
                             if ($towncode->TownName=="Nairobi" || $towncode->TownName=="Mombasa"):
                                 if ($towncode->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
                         $sched = $this->bus_model->getBusSchedule($_GET['scheduleid']);
                         $times = $this->bus_model->getscheduletimes($sched->RouteId,$townfid);
                        
                         if (strpos($_GET['seatno'], "Vip")<>FALSE)
                         {
                             $seatclass = 1;
                             
                             $amount =$this->bus_model->routeseatpricewithcurrency($sched->BusTypeId,1,$townfid,$towntid,$cur);
                         }
                         elseif (strpos($_GET['seatno'], "Fcls")<>FALSE)
                         {
                             $seatclass = 2;
                             $amount =$this->bus_model->routeseatpricewithcurrency($sched->BusTypeId,2,$townfid,$towntid,$cur);
                         }
                         else {
                             $seatclass = 3;
                             $amount =$this->bus_model->routeseatpricewithcurrency($sched->BusTypeId,3,$townfid,$towntid,$cur);
                         }
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => date('g:i A',strtotime($times->ArrivalDeparture.' - 30 minutes')),
			'clientname' => str_replace("_"," ",str_replace("%20"," ",$_GET['clientname'])),
			't_idno' => $_GET['idnumber'],
			'seatno' => str_replace(" Fcls","",str_replace(" Vip","",$_GET['seatno'])),
			$amounttable => $amount,
			'reg_no' => $sched->Busno,
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $townfid,
			'mobileno' => $_GET['mobileno'],
			'fromtown' => $townfid,
			'totown' => $towntid,
			'routecode' => $sched->RouteCode,
			't_currency' => $cur,
			'ticketstatus' => $status,
			'pick_point' => 0,			
                        'bus_type' => $sched->BusTypeId,
			'final_town' => $towntid,
			'bus_number' => $sched->Busno,
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $seatclass,
			'voucherno' => $_GET['voucherno'],
                        'nationality' => $_GET['nationality'],
			'mpesaref' => $mpesaref,
			'channel' => $_GET['channel'],
			'paymode' => $_GET['bookingmode'],
                        't_userid' => $userid,
                        'schedule_id' => $_GET['scheduleid']
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        $ticketer = "";
                        if ($this->bus_model->checkifseatalreadybooked($_GET['seatno'],$townfid,$towntid,$_GET['scheduleid'])==FALSE){
                        echo "[{\"ticketno\":\"None\",\"Busno\":\"None\",\"Status\":\"0\"}]";
                        }
                                  else{
                                     $this->db->insert($tablename, $insdata);
                                    $ticketid = $this->db->insert_id();
                                  $row = $this->bus_model->gettickettoprint($ticketid);
                                  $ticketn = $row->newticketno;
                                  $busno = $row->Busno;
                                     //echo "[{\"ticketno\":\"$ticketn\",\"Busno\":\"$busno\",\"Status\":\"1\",\"Points\":\"$points\"}]";
                                     echo "[{\"ticketno\":\"$ticketn\",\"Busno\":\"$busno\",\"Status\":\"1\"}]";
                                      //if smartcard used for loyalty
                                      $cardno = str_replace("?","",str_replace("%","",$_GET['cardno']));
                                      $points = 0;
                                      if ($cardno<>0 || $cardno<>"")
				  {
				  		$paidamnt = $amount;
						$currency = $cur;
						$query45 = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query45->last_row_array();
						$pntz = $pnt['points'];
						$amntperpointke = $pnt['kes'];
						$amntperpointug = $pnt['ugx'];
						$amntperpointtz = $pnt['tzs'];
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						//$cardno = str_replace("%","",$_GET['cardno']);
						//$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				  }
                                   //echo "[{\"ticketno\":\"$ticketn\",\"Busno\":\"$busno\",\"Status\":\"1\",\"Points\":\"$points\"}]";
                                  }
						
				  
				  
				   
                                  //echo $ticketer;
                                 // echo "[{\"ticketno\":\"None\"},{\"Busno\":\"None\"}]";
	} 
        
        function login_device()
        {
            //http://178.238.232.121/tms/admins/login_device?username=68&password=12345
            $username = $_GET['username'];
            $password = $_GET['password'];
            $remember_user = 0;
            if ($this->flexi_auth->login($username, $password, $remember_user)==TRUE)
            {
                //$this->user=$this->flexi_auth->get_user_by_identity($username)->result();
               $us = $this->bus_model->getonerowfromonetbl_status('user_accounts', 'uacc_username', $username);
		//$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                $userid = $us->uacc_id;
                echo "[{\"userid\":\"$userid\"}]";
            }
            else {
                echo "[{\"userid\":\"None\"}]";
            }
        }
  		function print_ticket_device()	
	{	
             // http://178.238.232.121/tms/admins/print_ticket_device?ticketno=68                        

		$td=$this->bus_model->gettickettoprint_device($_GET['ticketno']);
                if ($td==NULL)
                {
                echo "[{\"Name\":\"None\",\"From\":\"None\",\"To\":\"None\"\"Amount\":\"None\"\"BusType\":\"None\"\"Busno\":\"None\"\"Seatno\":\"None\"\"Reporttime\":\"None\"\"Deptime\":\"None\"\"Ticketno\":\"None\"}]";
                }
                else {
                    if ($td->t_currency==1)
                    {
                        $amount = $td->amount;
                    }
                    elseif ($td->t_currency==2)
                    {
                        $amount = $td->amountug;
                    }
                    elseif ($td->t_currency==3)
                    {
                        $amount = $td->amounttz;
                    }
                    elseif ($td->t_currency==4)
                    {
                        $amount = $td->amountusd;
                    }
                    elseif ($td->t_currency==5)
                    {
                        $amount = $td->amounteuro;
                    }
                    echo "[{\"Name\":\"$td->clientname\",\"From\":\"$td->TownFrom\",\"To\":\"$td->TownTo\"\"Amount\":\"$amount\"\"BusType\":\"$td->BusType\"\"Busno\":\"$td->Busno\"\"Seatno\":\"$td->seatno\"\"Reporttime\":\"$td->reporttime\"\"Deptime\":\"".date('g:i A',strtotime($td->reporttime.' + 30 minute'))."\"\"Ticketno\":\"$td->newticketno\"}]";
                }
	} 
        
	function create_banking_device()	
	{	
		//$xml = simplexml_load_string($source);
	
			$insdata = array(
			'TerminusId' => $_GET['TerminusId'],
			'TotalAmount' => $_GET['TotalAmount'],
			'BankingDate' => date('Y-m-d'),
			'AddedBy' => $_GET['userid'],
			'refno' => $_GET['refno']							
		);
					// run insert model to write data to db
                        $tablename = 'banking';
                        $pkname = 'BankingId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      //$this->load->view('manage_staff');

		
	}
		function create_petty_cash_device()	
	{	
			//$xml = simplexml_load_string($source);
			$insdata = array(
			'StationId' => $_GET['StationId'],
			'CashDate' => date('Y-m-d'),
			'CashierId' => $_GET['cashierid'],
			'AccountCodeId' => $_GET['account'],
			'Currency' => $_GET['currency'],
			'Amount' => $_GET['amount']);
					// run insert model to write data to db
                        $tablename = 'petty_cash';
                        $pkname = 'PettyCashId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                        $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
		
	}

		function create_ticket()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('from', 'From Town', 'trim|required');
		$this->form_validation->set_rules('to', 'TO Town', 'trim|required');
		$this->form_validation->set_rules('pickuppoint', 'Pickup point', 'trim|required');
		$this->form_validation->set_rules('busno', 'Bus number', 'trim|required');
		$this->form_validation->set_rules('bustype', 'Bus type', 'trim|required');
		$this->form_validation->set_rules('traveldate1', 'Travel date', 'trim|required');
		$this->form_validation->set_rules('routename', 'Route name', 'trim|required');
		//$this->form_validation->set_rules('seatclass', 'Seat class', 'trim|required');
		//$this->form_validation->set_rules('seatno', 'Seat number', 'trim|required|is_numeric');
                for ($t=1;$t<=$this->input->post('quantity');$t++):
                    
                
		$this->form_validation->set_rules('clientname'.$t, 'Client name', 'trim|required');   
		$this->form_validation->set_rules('identity'.$t, 'Identity', 'trim|required');
		$this->form_validation->set_rules('mobile'.$t, 'Mobile', 'trim|required');
                endfor;
		
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|is_numeric|callback_voucher_check');
		
		//$this->form_validation->set_rules('cashier', 'Cashier', 'trim|required');
		if($this->input->post('bookingmode')=='MPesa'){$this->form_validation->set_rules('mpesaref', 'Mpesa Ref', 'trim|required');}
		if($this->input->post('bookingmode')=='Voucher'){$this->form_validation->set_rules('vouchernumber', 'Voucher No', 'trim|required');}
		//$this->form_validation->set_rules('seatclass', 'Seat Class', 'trim|required');
		
		
		if($this->form_validation->run() == FALSE)
		{
		      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
		}
		else
		{
		$tids = "";
                //echo $_POST['quantity'];
                
                         
                         if ($this->input->post('currency')==1)
                         {
                             $amounttable = 'amount';
                         } 
                         if ($this->input->post('currency')==2)
                         {
                             $amounttable = 'amountug';
                         } 
                         if ($this->input->post('currency')==3)
                         {
                             $amounttable = 'amounttz';
                         } 
                         if ($this->input->post('currency')==4)
                         {
                             $amounttable = 'amountusd';
                         }
                         
                         $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                $userid = $this->user[0]->uacc_id;
            if($this->input->post('submit')=='Issue Ticket'):$status='Active';elseif($this->input->post('reserve')=='Reserve Seat'): $status='Reserved'; else: $status='Active'; endif;
			 if($this->input->post('bookingmode')=='4'){$mpesaref=$this->input->post('mpesaref');} else { $mpesaref='';}
                         
                    for ($i=1;$i<=$this->input->post('quantity');$i++)
                    {
        
                         $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('scheduleid'));
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $towncode1 = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             if ($towncode1->TownName=="Nairobi" || $towncode1->TownName=="Mombasa"):
                              if ($towncode1->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode1->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         if ($this->input->post('bookingmode')=='3')
                         {
                             $ticketno = $this->input->post('voucherticketnumber');
                         }
                         else {
                             
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
                         }
            
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $this->input->post('reporttime'),
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('seatno'.$i),
			$amounttable => $this->input->post('onecost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('from'),
			'totown' => $this->input->post('to'),
			'routecode' => $this->input->post('routecode'),
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('to'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
			'voucherno' => $this->input->post('vouchernumber'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 1,
			'paymode' => $this->input->post('bookingmode'),
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('scheduleid')
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        if ($this->bus_model->checkifseatalreadybooked($this->input->post('seatno'.$i),$this->input->post('from'),$this->input->post('to'),$this->input->post('scheduleid'))==TRUE):
                            
                            if (isset($_POST['ticketid']))
                         {
                  //$ticketidz=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  //$ticketid=$ticketidz->TicketId;
                                  $this->bus_model->dbupdate($tablename, $insdata, $pkname, $this->input->post('ticketid'));
                                  $ticketid = $this->input->post('ticketid');
                                  
                                  //$ticketid = $this->input->post('vouchertickid');
                         }
                         else {
                            
                       if ($this->input->post('bookingmode')=='3')
                         {
                  //$ticketidz=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  //$ticketid=$ticketidz->TicketId;
                                  $this->bus_model->dbupdate($tablename, $insdata, $pkname, $this->input->post('vouchertickid'));
                                  $ticketid = $this->input->post('vouchertickid');
                                  
                                  //if ($this->input->post('addedvoucheramnt')>0)
                                  $voucherupdatedata = array('voucheramount'=>$this->input->post('addedvoucheramnt'),
                                      'voucheredeemedby'=>$this->flexi_auth->get_user_id(),
                                      'voucheredeemedate'=>date('Y-m-d H:i:s'));
                                  $this->bus_model->dbupdate($tablename, $voucherupdatedata, $pkname, $this->input->post('vouchertickid'));
                                  //$ticketid = $this->input->post('vouchertickid');
                         }
                         else {
                             if ($this->input->post('seatno'.$i)<>0)
                             {
                             $ticketidz=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  $ticketid=$ticketidz->TicketId;
                             }
                             
                             else {
                                 $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
                             }
                         }
                         }
				  
                                  $tids.=$ticketid.","; 
                                  
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query45 = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query45->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'.$i));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				  }
                                  else {
                                      if (isset($_POST['addcard'.$i]))
                                      {
                                          if ($this->bus_model->getonerowfromonetbl('card_details', 'idno_or_passportno', $this->input->post('identity'.$i))==NULL)
                                          {
                                          $insdataz = array(
										'f_name' => $this->input->post('clientname'.$i),
                                                                                'idno_or_passportno' => $this->input->post('identity'.$i),
										'phoneno' => $this->input->post('mobile'.$i),
                                                                                'nationality' => $this->input->post('nationality'.$i),
										'stationtaken' => $this->input->post('from'),
										'stationcollect' => $this->input->post('addtown'.$i),
									);
									// run insert model to write data to db
									$tablenamez = 'card_details';
									$pknamez = 'card_details_id';
									$campuscodez = "";
									$pkvaluez = $campuscodez."_1";//do function to generate pkvalue
									$iflastidz = 0;
							  $this->bus_model->dbinsert($tablenamez, $insdataz, $pknamez, $pkvaluez, $iflastidz);
                                          }
                                      }
                                  }
               
                                  
                                  if ($this->input->post('return'.$i)=="Yes")
                                  {
                                      ///start return
									 
            //$ticketno = "123456";
            $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('returnschedule'.$i));
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $towncode1 = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             if ($towncode1->TownName=="Nairobi" || $towncode1->TownName=="Mombasa"):
                              if ($towncode1->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode1->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
            $schedata = $this->bus_model->getBusSchedule($this->input->post('returnschedule'.$i));
			$reptime = date('m/d/Y H:i',strtotime($schedata->DepartureDateTime.' - 30 minute'));
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $reptime,
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('returnseat'.$i),
			$amounttable => $this->input->post('returncost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('to'),
			'totown' => $this->input->post('from'),
			'routecode' => $schedata->RouteCode,
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('from'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
			'voucherno' => $this->input->post('vouchernumber'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 1,
			'paymode' => $this->input->post('bookingmode'),
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('returnschedule'.$i),
                        'ticketidreturn' => $ticketid
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        if ($this->input->post('returnseat'.$i)<>0)
                             {
                  $ticketidzr=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  $ticketidr=$ticketidzr->TicketId;
                             }
                             else {
                                 $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
                             }
				  
                                  $tids.=$ticketidr.",";
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'.$i));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				   
                }
									  //end return
                                  }endif;
                    }
                                  //$data['row'] = $this->bus_model->gettickettoprint($ticketid);
                    if ($this->input->post('bookingmode')==3)
                    {
                        $voucherupdatedata = array('ticketstatus'=>'Redeemed');
                        $this->bus_model->dbupdate('ticketing', $voucherupdatedata, 'TicketId', $this->input->post('vouchertickid'));
                    }
                                  $data['tids'] = $tids;
                                  if ($data['tids']=="")
                                  {
                                      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
                                  }
                                  else {
                                  //echo $tids;
                                  if ($status=='Active'):
     	 $this->load->view('print_ticket',$data);
                                  else:
                                      redirect('admins/ticket_station','refresh');
                                  endif;
                                  }
         //print ticket if fully booked
		 
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function create_ticket_voucher()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('from', 'From Town', 'trim|required');
		$this->form_validation->set_rules('to', 'TO Town', 'trim|required');
		$this->form_validation->set_rules('pickuppoint', 'Pickup point', 'trim|required');
		$this->form_validation->set_rules('busno', 'Bus number', 'trim|required');
		$this->form_validation->set_rules('bustype', 'Bus type', 'trim|required');
		$this->form_validation->set_rules('traveldate1', 'Travel date', 'trim|required');
		$this->form_validation->set_rules('routename', 'Route name', 'trim|required');
		//$this->form_validation->set_rules('seatclass', 'Seat class', 'trim|required');
		//$this->form_validation->set_rules('seatno', 'Seat number', 'trim|required|is_numeric');
                for ($t=1;$t<=$this->input->post('quantity');$t++):
                    
                
		$this->form_validation->set_rules('clientname'.$t, 'Client name', 'trim|required');   
		$this->form_validation->set_rules('identity'.$t, 'Identity', 'trim|required');
		$this->form_validation->set_rules('mobile'.$t, 'Mobile', 'trim|required');
                endfor;
		
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|is_numeric|callback_voucher_check');
		
		//$this->form_validation->set_rules('cashier', 'Cashier', 'trim|required');
		if($this->input->post('bookingmode')=='MPesa'){$this->form_validation->set_rules('mpesaref', 'Mpesa Ref', 'trim|required');}
		if($this->input->post('bookingmode')=='Voucher'){$this->form_validation->set_rules('vouchernumber', 'Voucher No', 'trim|required');}
		//$this->form_validation->set_rules('seatclass', 'Seat Class', 'trim|required');
		
		
		if($this->form_validation->run() == FALSE)
		{
		      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
		}
		else
		{
		$tids = "";
                //echo $_POST['quantity'];
                
                         
                         if ($this->input->post('currency')==1)
                         {
                             $amounttable = 'amount';
                         } 
                         if ($this->input->post('currency')==2)
                         {
                             $amounttable = 'amountug';
                         } 
                         if ($this->input->post('currency')==3)
                         {
                             $amounttable = 'amounttz';
                         } 
                         if ($this->input->post('currency')==4)
                         {
                             $amounttable = 'amountusd';
                         }
                    for ($i=1;$i<=$this->input->post('quantity');$i++)
                    {
        $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                $userid = $this->user[0]->uacc_id;
            if($this->input->post('submit')=='Issue Ticket'):$status='Active';elseif($this->input->post('reserve')=='Reserve Seat'): $status='Reserved'; else: endif;
			 if($this->input->post('bookingmode')=='4'){$mpesaref=$this->input->post('mpesaref');} else { $mpesaref='';}
                         $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('scheduleid'));
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             if ($towncode->TownName=="Nairobi" || $towncode->TownName=="Mombasa"):
                              if ($towncode->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         
                         $tdz = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $this->input->post('ticketid'));
                         $ticketno = $tdz->newticketno;
            
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $this->input->post('reporttime'),
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('seatno'.$i),
			$amounttable => $this->input->post('onecost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('from'),
			'totown' => $this->input->post('to'),
			'routecode' => $this->input->post('routecode'),
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('to'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
			'voucherno' => $this->input->post('vouchernumber'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 1,
			'paymode' => $this->input->post('bookingmode'),
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('scheduleid')
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        if ($this->bus_model->checkifseatalreadybooked($this->input->post('seatno'.$i),$this->input->post('from'),$this->input->post('to'),$this->input->post('scheduleid'))==TRUE):
                            
                       
                  //$ticketidz=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  //$ticketid=$ticketidz->TicketId;
                                  $this->bus_model->dbupdate($tablename, $insdata, $pkname, $this->input->post('ticketid'));
                                  $ticketid = $this->input->post('ticketid');
                         
				  
                                  $tids.=$ticketid.","; 
                                  
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query45 = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query45->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'.$i));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				  }
                                  else {
                                      if (isset($_POST['addcard'.$i]))
                                      {
                                          if ($this->bus_model->getonerowfromonetbl('card_details', 'idno_or_passportno', $this->input->post('identity'.$i))==NULL)
                                          {
                                          $insdataz = array(
										'f_name' => $this->input->post('clientname'.$i),
                                                                                'idno_or_passportno' => $this->input->post('identity'.$i),
										'phoneno' => $this->input->post('mobile'.$i),
                                                                                'nationality' => $this->input->post('nationality'.$i),
										'stationtaken' => $this->input->post('from'),
										'stationcollect' => $this->input->post('addtown'.$i),
									);
									// run insert model to write data to db
									$tablenamez = 'card_details';
									$pknamez = 'card_details_id';
									$campuscodez = "";
									$pkvaluez = $campuscodez."_1";//do function to generate pkvalue
									$iflastidz = 0;
							  $this->bus_model->dbinsert($tablenamez, $insdataz, $pknamez, $pkvaluez, $iflastidz);
                                          }
                                      }
                                  }
               
                                  
                                  if ($this->input->post('return'.$i)=="Yes")
                                  {
                                      ///start return
									  $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
            if($this->input->post('submit')=='Add Ticket'):$status='Active';elseif($this->input->post('reserve')=='Reserve Seat'): $status='Reserved'; else: endif;
			 if($this->input->post('bookingmode')=='MPesa'){$mpesaref=$this->input->post('mpesaref');} else { $mpesaref='';}
            //$ticketno = "123456";
            $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('returnschedule'.$i));
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->user[0]->t_station);
                             $towncode1 = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             if ($towncode1->TownName=="Nairobi" || $towncode1->TownName=="Mombasa"):
                              if ($towncode1->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode1->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
            $schedata = $this->bus_model->getBusSchedule($this->input->post('returnschedule'.$i));
			$reptime = date('m/d/Y H:i',strtotime($schedata->DepartureDateTime.' - 30 minute'));
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $reptime,
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('returnseat'.$i),
			$amounttable => $this->input->post('returncost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('to'),
			'totown' => $this->input->post('from'),
			'routecode' => $schedata->RouteCode,
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('from'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
			'voucherno' => $this->input->post('vouchernumber'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 1,
			'paymode' => $this->input->post('bookingmode'),
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('returnschedule'.$i),
                        'ticketidreturn' => $ticketid
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                  $ticketidzr=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  $ticketidr=$ticketidzr->TicketId;
				  
                                  $tids.=$ticketidr.",";
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'.$i));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				   
                }
									  //end return
                                  }endif;
                    }
                                  //$data['row'] = $this->bus_model->gettickettoprint($ticketid);
                    if ($this->input->post('bookingmode')==3)
                    {
                        $voucherupdatedata = array('ticketstatus'=>'Redeemed');
                        $this->bus_model->dbupdate('ticketing', $voucherupdatedata, 'TicketId', $this->input->post('vouchertickid'));
                    }
                                  $data['tids'] = $tids;
                                  if ($data['tids']=="")
                                  {
                                      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket',$data);
                                  }
                                  else {
                                  //echo $tids;
                                  if ($status=='Active'):
     	 $this->load->view('print_ticket',$data);
                                  else:
                                      redirect('admins/ticket_station','refresh');
                                  endif;
                                  }
         //print ticket if fully booked
		 
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function create_ticket_corp()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('from', 'From Town', 'trim|required');
		$this->form_validation->set_rules('to', 'TO Town', 'trim|required');
		$this->form_validation->set_rules('pickuppoint', 'Pickup point', 'trim|required');
		$this->form_validation->set_rules('busno', 'Bus number', 'trim|required');
		$this->form_validation->set_rules('bustype', 'Bus type', 'trim|required');
		$this->form_validation->set_rules('traveldate1', 'Travel date', 'trim|required');
		$this->form_validation->set_rules('routename', 'Route name', 'trim|required');
		//$this->form_validation->set_rules('seatclass', 'Seat class', 'trim|required');
		//$this->form_validation->set_rules('seatno', 'Seat number', 'trim|required|is_numeric');
                for ($t=1;$t<=$this->input->post('quantity');$t++):
                    
                
		$this->form_validation->set_rules('clientname'.$t, 'Client name', 'trim|required');   
		$this->form_validation->set_rules('identity'.$t, 'Identity', 'trim|required');
		$this->form_validation->set_rules('mobile'.$t, 'Mobile', 'trim|required');
                endfor;
		
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|is_numeric');
		
		
		if($this->form_validation->run() == FALSE)
		{
		      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket_corp',$data);
                      //echo $this->input->post('amount');
		}
		
		else
		{
                    if ($this->input->post('camount') < $this->input->post('amount')) {
                     $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket_corp',$data);
                     //echo "acc amount less than ticket amount";
                } 
                else {
		$tids = "";
                //echo $_POST['quantity'];
                
                         
                         
                             $amounttable = 'amount';
                    for ($i=1;$i<=$this->input->post('quantity');$i++)
                    {
        $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                $userid = $this->user[0]->uacc_id;
            if($this->input->post('submit')=='Issue Ticket'):$status='Active';elseif($this->input->post('reserve')=='Reserve Seat'): $status='Reserved'; else: endif;
			 if($this->input->post('bookingmode')=='MPesa'){$mpesaref=$this->input->post('mpesaref');} else { $mpesaref='';}
                         $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('scheduleid'));
                         
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
            
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $this->input->post('reporttime'),
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('seatno'.$i),
			$amounttable => $this->input->post('onecost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('from'),
			'totown' => $this->input->post('to'),
			'routecode' => $this->input->post('routecode'),
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('to'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 3,
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('scheduleid')
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                        if ($this->bus_model->checkifseatalreadybooked($this->input->post('seatno'.$i),$this->input->post('from'),$this->input->post('to'),$this->input->post('scheduleid'))==TRUE):
                            
                       
                  $ticketidz=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  $ticketid=$ticketidz->TicketId; 
                                  
                                  $insdata2 = array(
			'cust_id' => $this->input->post('custid'),
			'camount' => $this->input->post('onecost'.$i),
			'user' => $userid,
			'ticket_id' => $ticketid
                                      );
                                  $tablename2 = 'corp_transactions';
                                    $pkname2 = 'corp_transactions_id';
                                    $pkvalue2 = $campuscode."_1";//do function to generate pkvalue
                                    $iflastid2 = 0;
                                    $this->bus_model->dbinsert($tablename2, $insdata2, $pkname2, $pkvalue2, $iflastid2);
				  
                                     $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '25';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;
                         $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                         $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $this->user[0]->uacc_email);
                         $custdetz = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
                        $html112 = "Hello ".$this->input->post('clientname'.$i).", <p>A modern coast bus ticket has been booked for you by ".$custdetz->Cust_Company.". Please find
                             attached, a copy of the ticket which you can print at your own convenience.</p> <p>Thank you for using Modern Coast Express Ltd</p>";
                        $tk = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $ticketid);
                                 $html112 .= "<li>".$tk->newticketno."</li>";
                                 $data3['tids'] = $ticketid;
                                 //$html .= $this->load->view('print_ticket_email',$data3);

                        $this->email->initialize($config);
                        //$this->email->clear();
                        $this->email->from('info@modern.co.ke', 'Modern Coast Express Ltd');
                        $this->email->to($this->input->post('clientemail'.$i));
                        if ($this->input->post('clientemail'.$i)!=$this->input->post('usermail'))
                        {
                        $this->email->cc($this->input->post('usermail'));
                        }
                        $this->email->subject('Modern Coast Express Ltd: Booked Tickets');
                        $this->email->message($html112.$this->load->view('print_ticket_email', $data3, TRUE));

                        $this->email->send();
                        
                        if (isset($_POST['smssend'.$i]))
                        {
                            $frm = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                            $townfrom = $frm->TownName;
                            $to = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('to'));
                            $townto = $to->TownName;
                            $seatno = $this->input->post('seatno'.$i);
                            
                            $strimmedphoneno = str_replace (" ", "", $this->input->post('mobile'.$i));
                    $sphoneno = $strimmedphoneno;
                    $countrycode = "254";
                    if (substr($strimmedphoneno, 0,2)=="07")
                    {
                        $sphoneno = (int)$countrycode.substr($strimmedphoneno, -9);
                    }
                    else if (substr($strimmedphoneno, 0,4)=="+254" || substr($strimmedphoneno, 0,4)=="+255" || substr($strimmedphoneno, 0,4)=="+256")
                    {
                        $sphoneno = (int)substr($strimmedphoneno, -12);
                    }
					else {
						$sphoneno = $strimmedphoneno;
					}
                            
                           $phoneno = $this->input->post('mobile'.$i);
                            $this->_sendsmsticket($ticketno,$townfrom,$townto,$seatno,$phoneno);
                        }
                                    
                                  $tids.=$ticketid.","; 
                                  
                                 
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				  }
               
                                  
                                  if ($this->input->post('return'.$i)=="Yes")
                                  {
                                      ///start return
									  $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
            if($this->input->post('submit')=='Add Ticket'):$status='Active';elseif($this->input->post('reserve')=='Reserve Seat'): $status='Reserved'; else: endif;
			 if($this->input->post('bookingmode')=='MPesa'){$mpesaref=$this->input->post('mpesaref');} else { $mpesaref='';}
            //$ticketno = "123456";
            $schedulespecialstatus = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $this->input->post('returnschedule'.$i));
                         if ($schedulespecialstatus->IsSpecial==0)
                         {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             $tcode = $towncode->TownCode;
                             $specialstat = "NORMAL";
                         }
                         else {
                             $towncode = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                             if ($towncode->TownName=="Nairobi" || $towncode->TownName=="Mombasa"):
                              if ($towncode->TownName=="Nairobi"):
                                     $tcode = "NA";
                                 endif;
                                 if ($towncode->TownName=="Mombasa"):
                                     $tcode = "MA";
                                 endif;
                             $specialstat = "SPECIAL";
                             else:
                                 $tcode = $towncode->TownCode;
                                 $specialstat = "NORMAL";
                             endif;
                         }
                         $lastticket = $this->bus_model->getlastinsertedticketfromtown($tcode, $specialstat);
                         if ($lastticket==NULL)
                         {
                             $ticketno = $tcode."1";
                         }
                         else {
                             if (empty($lastticket->newticketno) or is_null($lastticket->newticketno)){
                                 $ticketno = $tcode."1";
                             }
                             else {
                                $lastno = (int)(substr($lastticket->newticketno,2));
                                $newlastno = $lastno+1;
                                $ticketno = $tcode.$newlastno;
                             }
                         }
            $schedata = $this->bus_model->getBusSchedule($this->input->post('returnschedule'.$i));
			$reptime = date('m/d/Y H:i',strtotime($schedata->DepartureDateTime.' - 30 minute'));
			$insdata = array(
			'bookdate' => date('Y-m-d'),
			'booktime' => date('H:i:s'),
			'reporttime' => $reptime,
			'clientname' => $this->input->post('clientname'.$i),
			't_idno' => $this->input->post('identity'.$i),
			'seatno' => $this->input->post('returnseat'.$i),
			$amounttable => $this->input->post('returncost'.$i),
			'reg_no' => $this->input->post('busno'),
			'transdate' => date('Y-m-d H:i:s'),
			't_username' => $username,///flexi user
			't_station' => $this->input->post('t_station'),
			'mobileno' => $this->input->post('mobile'.$i),
			'fromtown' => $this->input->post('to'),
			'totown' => $this->input->post('from'),
			'routecode' => $schedata->RouteCode,
			't_currency' => $this->input->post('currency'),
			'ticketstatus' => $status,
			'pick_point' => $this->input->post('pickuppoint'),
			'bus_type' => $this->input->post('bustype'),
			'final_town' => $this->input->post('from'),
			'bus_number' => $this->input->post('busno'),
			'newticketno' => $ticketno,
			'specialstatus' => $specialstat,
			'seat_class' => $this->input->post('seatclass'.$i),
                        'nationality' => $this->input->post('nationality'.$i),
			'mpesaref' => $mpesaref,
			'channel' => 3,
                        't_userid' => $userid,
                        'schedule_id' => $this->input->post('returnschedule'.$i),
                        'ticketidreturn' => $ticketid
		);
		// run insert model to write data to db
                        $tablename = 'ticketing';
                        $pkname = 'TicketId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                  $ticketidzr=$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
				  $ticketidr=$ticketidzr->TicketId;
                                  
                                  $insdata1 = array(
			'cust_id' => $this->input->post('custid'),
			'camount' => $this->input->post('returncost'.$i),
			'user' => $userid,
			'ticket_id' => $ticketidr
                                      );
                                  $tablename1 = 'corp_transactions';
                                    $pkname1 = 'corp_transactions_id';
                                    $pkvalue1 = $campuscode."_1";//do function to generate pkvalue
                                    $iflastid1 = 0;
                                    $this->bus_model->dbinsert($tablename1, $insdata1, $pkname1, $pkvalue1, $iflastid1);
                                    
                                    $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '25';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;
                         $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                         $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $this->user[0]->uacc_email);
                         $custdetz = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
                        $html1 = "Hello ".$this->input->post('clientname'.$i).", <p>A modern coast bus ticket has been booked for you by ".$custdetz->Cust_Company.". Please find
                             attached, a copy of the ticket which you can print at your own convenience.</p> <p>Thank you for using Modern Coast Express Ltd</p>";
                        $tk = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $ticketid);
                                 $html1 .= "<li>".$tk->newticketno."</li>";
                                 $data4['tids'] = $ticketidr;
                                 //$html21 = $this->load->view('print_ticket_email',$data4);

                        $this->email->initialize($config);
                        $this->email->clear();
                        $this->email->from('info@modern.co.ke', 'Modern Coast Courier Ltd');
                        $this->email->to($this->input->post('clientemail'.$i));
                        if ($this->input->post('clientemail'.$i)!=$this->input->post('usermail'))
                        {
                        $this->email->cc($this->input->post('usermail'));
                        }
                        $this->email->subject('Modern Coast Courier Ltd: Booked Ticket');
                        $this->email->message($html1.$this->load->view('print_ticket_email', $data4, TRUE));

                        $this->email->send();
                        
                        
                        
                        if (isset($_POST['smssend'.$i]))
                        {
                            $frm = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('to'));
                            $townfrom = $frm->TownName;
                            $to = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                            $townto = $to->TownName;
                            $seatno = $this->input->post('returnseat'.$i);
                            
                            $strimmedphoneno = str_replace (" ", "", $this->input->post('mobile'.$i));
                    $sphoneno = $strimmedphoneno;
                    $countrycode = "254";
                    if (substr($strimmedphoneno, 0,2)=="07")
                    {
                        $sphoneno = (int)$countrycode.substr($strimmedphoneno, -9);
                    }
                    else if (substr($strimmedphoneno, 0,4)=="+254" || substr($strimmedphoneno, 0,4)=="+255" || substr($strimmedphoneno, 0,4)=="+256")
                    {
                        $sphoneno = (int)substr($strimmedphoneno, -12);
                    }
					else {
						$sphoneno = $strimmedphoneno;
					}
                            
                            //$phoneno = "254710228024";
                            $this->_sendsmsticket($ticketno,$townfrom,$townto,$seatno,$sphoneno);
                        }
				  
                                  $tids.=$ticketidr.",";
				  //if smartcard used for loyalty
				  if ($this->input->post('smartcard'.$i) && $this->input->post('carddetails'.$i))
				  {
				  		$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query = $this->db->select('*')->from('point_gain')->where('status !=',0)->get();
						$pnt = $query->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						if ($currency=="1")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointke);
						}
						if ($currency=="2")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointug);
						}
						if ($currency=="3")
						{
							$points = floor(($paidamnt*$pntz)/$amntperpointtz);
						}
						$cardno = str_replace("%","",$this->input->post('carddetails'));
						$cardno = str_replace("?","",$cardno);
						$insdata = array(
										'CustomerLkp' => $cardno,
										'Points' => $points,
                                                                                'TicketId' => $ticketid
									);
									// run insert model to write data to db
									$tablename = 'customer_loyalty_points';
									$pkname = 'LoyaltyId';
									$campuscode = "";
									$pkvalue = $campuscode."_1";//do function to generate pkvalue
									$iflastid = 0;
							  $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
						
				   
                }
									  //end return
                                  }
                                  
                                  endif;
                    }
                                  //$data['row'] = $this->bus_model->gettickettoprint($ticketid);
                                  //$data['tids'] = $tids;
                                  if ($tids=="")
                                  {
                                      $data['uri'] = $this->input->post('schedfrto');
                                      $this->load->view('failed_ticket_corp',$data);
                                      //echo "no tids".$tids;
                                  }
                                  else {
                                  //echo $tids;
                                  if ($status=='Active'):
                                      $ntids = trim($tids,',');
                                  $ntd = explode(",",$ntids);
                                  $tdcount = count($ntd);
                                   $html = "The following are the ticket numbers for the modern coast tickets that you have booked.!<ul>";
                                  for ($y=0;$y<$tdcount;$y++):
                                      $tk = $this->bus_model->getonerowfromonetbl('ticketing', 'TicketId', $ntd[$y]);
                                 $html .= "<li>".$tk->newticketno."</li>";
                                  endfor;
                                  $html .= "</ul><br /><br />Please save them and present when going to travel. Thank you for choosing Modern Coast Express Ltd.";
                                  $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '25';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;

                        $this->email->initialize($config);
                        $this->email->from('info@modern.co.ke', 'Modern Coast Courier Ltd');
                        $this->email->to($this->input->post('usermail'));
                        $this->email->subject('Modern Coast Courier Ltd: Booked Tickets');
                        $this->email->message($html);

                        $this->email->send();
                        $data['tids'] = $tids;
                        $this->load->view('success_ticket_corp',$data);
                                  else:
                                      redirect('admins/ticket_station_corp','refresh');
                                  endif;
                                  }
         //print ticket if fully booked
                }
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
        
        
	  function getseatclass(){
  echo 'success';
  }
  function getcustomerpoints($cardno = NULL){
      if (!empty($cardno) or !is_null($cardno))
      {
          
  						$cardno = str_replace("%","",$cardno);
						$cardno = str_replace("?","",$cardno);
  //$paidamnt = $this->input->post('amount');
						$currency = $this->input->post('currency');
						$query = $this->db->select('*')->from('points_burn')->where('status !=',0)->get();
						
						$pnt = $query->last_row();
						$pntz = $pnt->points;
						$amntperpointke = $pnt->kes;
						$amntperpointug = $pnt->ugx;
						$amntperpointtz = $pnt->tzs;
						
						$query1 = $this->db->select('SUM(Points) as spoints')->from('customer_loyalty_points')->where('status !=',0)->where('CustomerLkp',$cardno)->get();
						
						if ($query1->num_rows()==0)
						{
							$spnts = 0;
                                                        $kes = 0;
						$ugx = 0;
						$tzs = 0;
						}
						else {
						$spnt = $query1->row();
						$spnts = $spnt->spoints; }
						
						$kes = floor(($spnts*$amntperpointke)/$pntz);
						$ugx = floor(($spnts*$amntperpointug)/$pntz);
						$tzs = floor(($spnts*$amntperpointtz)/$pntz);
						
						echo "Loyalty Points: ".$spnts." | Worth Kshs ".$kes.", UGX ".$ugx.", TZS ".$tzs;
                                                }
  else {
      echo "Please enter a loyalty card number";
  }
  }

  
  
	        function send_message()
        {
        $this->form_validation->set_rules('rec', 'To', 'required');			
		$this->form_validation->set_rules('subject', 'Subject', 'required|max_length[200]');			
		$this->form_validation->set_rules('message', 'Message', 'required|trim');
			
		$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
	
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
		$data['messages']=$this->bus_model->getmessages();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['sentmessages'] = $this->bus_model->getmessagessent();
		
     	$this->load->view('manage_messages',$data);
		}
		else // passed validation proceed to post success logic
		{
		 	// build array for the model
                   
                         $this->load->helper('date');
                         $ts = "%H:%i:%s";
						 $dids = $_POST['rec'];
                        // $tot = count($_POST['rec']);
						$userids = "";
						 foreach ($dids as $did)
						 {
							 $userids .= $did.",";
						 }
						// echo $userids;
			$message_form_data = array(
					       	'm_header' => set_value('subject'),
					       	'm_message' => set_value('message'),
					       	'm_date' => date('Y-m-d'),
					       	'm_time' => mdate($ts),
					       	'm_senderid' => $this->flexi_auth->get_user_id(),
					       	'm_recipientid' => $userids
						);
                        
					
			// run insert model to write data to db
			$mid = $this->bus_model->add_message_SaveForm($message_form_data);
		    $nmid = $mid->m_id;
				foreach ($dids as $did)
				{
				$messagerec_form_data = array(
									'MR_MID' => $nmid,
									'MR_RID' => $did
								);
				
				$this->bus_model->add_messagerec_SaveForm($messagerec_form_data);
                                /*
				if(isset($_POST['sendemail']))
				{
                                    $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
    
    $useremail = $this->user[0]->uacc_email;
					$suserdet = $this->bus_model->get_spec_user($this->flexi_auth->get_user_id());
					$suseremail = $useremail;
					$ruserdet = $this->main_model->get_spec_user($did);
					$ruseremail = $ruserdet->User_E_Mail;
					 $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '25';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;

                        $this->email->initialize($config);
                        $this->email->from($suseremail, $suserdet->User_First_Name." ".$suserdet->User_Last_Name);
                        $this->email->to($ruseremail);
                        //$this->email->bcc('a.marcel77@gmail.com');
                        $this->email->subject($this->input->post('subject'));
                        $this->email->message($this->input->post('message'));

                        $this->email->send();
				}
                                */
			}
                        $data['messages']=$this->bus_model->getmessages();
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$data['sentmessages'] = $this->bus_model->getmessagessent();
		$this->load->view('manage_messages',$data);  
		}
        }	
	        function send_message_sms()
        {		
		$this->form_validation->set_rules('message', 'Message', 'required|trim');
			
		$this->form_validation->set_error_delimiters('<br /><span class="error">', '</span>');
	
		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
		$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
                
                $data['sms'] = 1;
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                $data['schedid'] = $this->input->post('schedid');
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
		}
		else // passed validation proceed to post success logic
		{
		 	// build array for the model
                   $nos = $this->bus_model->getresultfromonetbl('ticketing', 'schedule_id', $this->input->post('schedid'));
                   $ns = "";
                   if ($nos<>NULL)
                   {
                       foreach ($nos as $no)
                       {
                           if ($no->nationality=="Kenya")
                           {
                               $countrycode = "254";
                           }
                           if ($no->nationality=="Uganda")
                           {
                               $countrycode = "256";
                           }
                           if ($no->nationality=="Tanzania")
                           {
                               $countrycode = "255";
                           }
                           $strimmedphoneno = str_replace (" ", "", $no->mobileno);
                            $sphoneno = $strimmedphoneno;
                            if (substr($strimmedphoneno, 0,2)=="07")
                            {
                                $sphoneno = $countrycode.substr($strimmedphoneno, -9);
                            }
                            else if (substr($strimmedphoneno, 0,4)=="+254" || substr($strimmedphoneno, 0,4)=="+255" || substr($strimmedphoneno, 0,4)=="+256")
                            {
                                $sphoneno = substr($strimmedphoneno, -12);
                            }
                            else {
                                    $sphoneno = $strimmedphoneno;
                            }
                            $ns .=$sphoneno.",";
                               }
                           }
                           //echo trim($ns,','); exit;
                           $this->_sendsms_schedules($this->input->post('message'), trim($ns,','));
                           //redirect('crud_schedules');
                           $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
                        
		}
        }	
		function confirm_authorization()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('user', 'User', 'trim|required');
		
		$this->form_validation->set_rules('password', 'Password', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
		$data['hireid']=$this->input->post('hireid');
		
		$data['busid']=$this->input->post('busid');
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$this->load->view('authorize_bus_hire');
		}
		else
		{		
			$updatedata = array(
			'AuthorizedBy' => $this->input->post('user'),
			'AuthorizedStatus' => 'authorized',
		);
                        $tablename = 'bus_hire';
                        $pkname = 'BusHireId';
                        $pkvalue = $this->input->post('hireid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
					  
					  //make bus unavailable
					  			$updatedata = array(
			'IsActive' => 0,
		);
					    $tablename = 'buses';
                        $pkname = 'BusId';
                        $pkvalue = $this->input->post('busid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
					  $data['message']='Bus hire authorized successfully';
			$this->load->view('authorize_bus_hire',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function confirm_bus_return()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		$this->form_validation->set_rules('user', 'User', 'trim|required');
		
		$this->form_validation->set_rules('password', 'Password', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
		$data['hireid']=$this->input->post('hireid');
		
		$data['busid']=$this->input->post('busid');
		
		$sql_select = array('uacc_id','upro_first_name','upro_last_name');
       
	    $sql_where = array();
		
		$data['users'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		
		$this->load->view('confirm_bus_return');
		}
		else
		{		
			$updatedata = array(
			'ReturnedBy' => $this->input->post('user'),
			'AuthorizedStatus' => 'returned',
		);
                        $tablename = 'bus_hire';
                        $pkname = 'BusHireId';
                        $pkvalue = $this->input->post('hireid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
					  
					  //make bus unavailable
					  			$updatedata = array(
			'IsActive' => 1,
		);
					    $tablename = 'buses';
                        $pkname = 'BusId';
                        $pkvalue = $this->input->post('busid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
					  $data['message']='Bus return successful';
			$this->load->view('confirm_bus_return',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function update_staff()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		
		$this->form_validation->set_rules('idnumber', 'Id Number', 'trim|required');
		
		$this->form_validation->set_rules('staffnumber', 'Staff Number', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['titles']=$this->bus_model->getplainresults('job_title_lkp');
		
                        $data['staff']=$this->bus_model->getStaff();

                        $this->load->view('manage_staff',$data);
		}
		else
		{	
                        
                        if (!empty($_FILES['sphoto']['name']))
                        {
                            $config['upload_path'] = './staffphotos/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);
                       $this->upload->do_upload('sphoto');
                        $data3 =  $this->upload->data();
                        $photoname = $data3['file_name'];
                        }
                        
                        if (!empty($_FILES['licencecopy']['name']))
                        { 
                            $config['upload_path'] = './staffphotos/';
                        $config['allowed_types'] = '*';
                        $this->load->library('upload', $config);
                        
                        if ( ! $this->upload->do_upload('licencecopy'))
     {
      print_r($this->upload->display_errors());
      
     } 
     else
     {
         echo 'success';
          //$this->upload->do_upload('licencecopy');
                        $data4 =  $this->upload->data();
                        $licencecopy = $data4['file_name'];
     }

                        //$this->upload->do_upload('licencecopy');
                        //$data4 =  $this->upload->data();
                        //$licencecopy = $data4['file_name'];
                        }
                        if (!empty($_FILES['psvcopy']['name']))
                        {
                            $config['upload_path'] = './staffphotos/';
                        $config['allowed_types'] = '*';
                        $this->load->library('upload', $config);
                        
                        $this->upload->do_upload('psvcopy');
                        $data5 =  $this->upload->data();
                        $psvcopy = $data5['file_name'];
                        }
                        if (!empty($_FILES['sphoto']['name']))
                        {
                            $sphototable = 'SPhoto';
                            $sphotovalue = $photoname;
                        }
                        else {
                            $sphototable = 'StaffName';
                            $sphotovalue = $this->input->post('name');
                        }
                        if (!empty($_FILES['licencecopy']['name']))
                        {
                            $licencetable = 'LicenceCopyFile';
                            $licencevalue = $licencecopy;
                        }
                        else {
                            $licencetable = 'StaffName';
                            $licencevalue = $this->input->post('name');
                        }
                        if (!empty($_FILES['psvcopy']['name']))
                        {
                            $psvtable = 'PSVCopy';
                            $psvvalue = $psvcopy;
                        }
                        else {
                            $psvtable = 'StaffName';
                            $psvvalue = $this->input->post('name');
                        }
                        echo $licencetable.' - '.$licencevalue;
			$updatedata = array(
			'StaffName' => $this->input->post('name'),
			'StaffIdNumber' => $this->input->post('idnumber'),
			'StaffNumber' => $this->input->post('staffnumber'),
			'StaffJobLkp' => $this->input->post('title'),
			'StaffAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),	
			$sphototable => $sphotovalue,
			'LicenceNo' => $this->input->post('licenceno'),
			$licencetable => $licencevalue,
			'LicenceExpiry' => $this->input->post('expirydate'),
			'PSVNo' => $this->input->post('licenceno'),
			$psvtable => $psvvalue												
		);
                        
                        
                        $tablename = 'bus_staff';
                        $pkname = 'StaffId';
                        $pkvalue = $this->input->post('staffid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_staff','refresh');
                        $data['url'] = 'admins/crud_staff';
                        $this->load->view('data_success',$data);
			
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function create_bus()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('bustypelkp', 'Bus Type', 'trim|required');
		
		$this->form_validation->set_rules('ExternalColour', 'External Colour', 'trim|required');
		
		$this->form_validation->set_rules('InternalColour', 'Internal Colour', 'trim|required');
		
		$this->form_validation->set_rules('regno', 'Registration Number', 'trim|required|callback_bus_unique');
		
		$this->form_validation->set_rules('makelkp', 'Make', 'trim|required');
		
		$this->form_validation->set_rules('insexpiry', 'Insurance Expiry Date', 'trim|required');
		
		//$this->form_validation->set_rules('modellkp', 'Model', 'trim|required');
		//$this->form_validation->set_rules('seats', 'Seats', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['bustypes']=$this->bus_model->getBusTypes();
		
                        $data['makes']=$this->bus_model->getMakes();

                        $data['models']=$this->bus_model->getModels();

                        $data['positions']=$this->bus_model->getplainresults('seat_position_lkp');

                        $data['buses']=$this->bus_model->getBuses();

                        $this->load->view('manage_bus',$data);
		}
		else
		{		
                    $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('inscopy');
                        $data3 =  $this->upload->data();
                        $inscopy = $data3['file_name'];
            
                        $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('inscomesacopy');
                        $data4 =  $this->upload->data();
                        $comesacopy = $data4['file_name'];
                        
                        $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('logbookcopy');
                        $data5 =  $this->upload->data();
                        $logbookcopy = $data5['file_name'];
                        
			$insdata = array(
			'BusTypeLkp' => $this->input->post('bustypelkp'),
			'ExternalColour' => $this->input->post('ExternalColour'),
			'InternalColour' => $this->input->post('InternalColour'),
			'RegistrationNumber' => $this->input->post('regno'),
			'MakeLkp' => $this->input->post('makelkp'),
			'DateBought' => $this->input->post('datebought'),
			'BusName' =>  $this->input->post('busname'),
			'BusNumber' =>  $this->input->post('busnumber'),
			'IsActive' =>  $this->input->post('isactive'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),	
			'InsuranceExpiry' => $this->input->post('insexpiry'),
			'CopyOfInsurance' =>  $inscopy,
			'CopyOfComesa' =>  $comesacopy,
			'CopyOfLogbook' =>  $logbookcopy,		
			'station_at' =>  1,
			'date_available_from' =>  date('Y-m-d'),
			'time_available_from' =>  date('H:i')							
		);
					// run insert model to write data to db
                        $tablename = 'buses';
                        $pkname = 'BusId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_bus','refresh');
                        $data['url'] = 'admins/crud_bus';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function create_agents()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		 $this->form_validation->set_rules('c_fname', 'First Name', 'required|trim|max_length[20]');			
		$this->form_validation->set_rules('c_lname', 'Last Name', 'max_length[20]');			
		$this->form_validation->set_rules('c_company', 'Company', 'trim|max_length[30]');			
		$this->form_validation->set_rules('c_mobileno', 'Mobile', 'required|trim');			
		$this->form_validation->set_rules('c_faxno', 'Fax', 'trim');			
		$this->form_validation->set_rules('c_country', 'Country', 'trim');			
		$this->form_validation->set_rules('c_city', 'City', 'trim');			
		$this->form_validation->set_rules('c_county', 'County', 'trim');			
		$this->form_validation->set_rules('c_email', 'Email', 'trim|valid_email');			
		$this->form_validation->set_rules('c_website', 'Website', 'trim');			
		$this->form_validation->set_rules('c_street_address', 'Street Address', 'trim');			
		$this->form_validation->set_rules('c_postal_address', 'Postal Address', 'trim');
                
                $this->form_validation->set_error_delimiters('<br /><span class="red">', '</span>');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
		
		$this->load->view('manage_agents',$data);
		}
		else
		{
                        
			
                        $trimmedphoneno = str_replace (" ", "", $this->input->post('c_mobileno'));
                    $phoneno = $trimmedphoneno;
                    if (substr($trimmedphoneno, 0,2)=="07")
                    {
                        $phoneno = (int)"254".substr($trimmedphoneno, -9);
                    }
                    if (substr($trimmedphoneno, 0,4)=="+254")
                    {
                        $phoneno = (int)substr($trimmedphoneno, -12);
                    }
                        $insdata = array(
					       	'Agent_First_Name' => set_value('c_fname'),
					       	'Agent_Last_Name' => set_value('c_lname'),
					       	'Agent_Company' => set_value('c_company'),
					       	'Agent_Mobile_No' => $phoneno,
					       	'Agent_Fax_No' => set_value('c_faxno'),
					       	'Agent_Country' => set_value('c_country'),
					       	'Agent_City' => set_value('c_city'),
					       	'Agent_E_mail' => set_value('c_email'),
					       	'Agent_Website' => set_value('c_website'),
					       	'Agent_Street_Address' => set_value('c_street_address'),
					       	'Agent_Postal_Address' => set_value('c_postal_address')
						);
					// run insert model to write data to db
                        $tablename = 'agent';
                        $pkname = 'Agent_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_agents','refresh');
                        $data['url'] = 'admins/crud_agents';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function create_customers()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		
		 $this->form_validation->set_rules('c_fname', 'First Name', 'required|trim|max_length[20]');			
		$this->form_validation->set_rules('c_lname', 'Last Name', 'max_length[20]');			
		$this->form_validation->set_rules('c_company', 'Company', 'trim|max_length[30]');			
		$this->form_validation->set_rules('c_mobileno', 'Mobile', 'required|trim');			
		$this->form_validation->set_rules('c_faxno', 'Fax', 'trim');			
		$this->form_validation->set_rules('c_country', 'Country', 'trim');			
		$this->form_validation->set_rules('c_city', 'City', 'trim');			
		$this->form_validation->set_rules('c_county', 'County', 'trim');			
		$this->form_validation->set_rules('c_email', 'Email', 'trim|valid_email');			
		$this->form_validation->set_rules('c_website', 'Website', 'trim');			
		$this->form_validation->set_rules('c_street_address', 'Street Address', 'trim');			
		$this->form_validation->set_rules('c_postal_address', 'Postal Address', 'trim');			
		$this->form_validation->set_rules('acctype', 'Account Type', 'trim');
                
                $this->form_validation->set_error_delimiters('<br /><span class="red">', '</span>');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['countries']=$this->bus_model->getcountries();
			
                        $data['customers']=$this->bus_model->getplainresults('customer');
		
                        $this->load->view('manage_corp_customers',$data);
		}
		else
		{
                        
			
                        $trimmedphoneno = str_replace (" ", "", $this->input->post('c_mobileno'));
                    $phoneno = $trimmedphoneno;
                    if (substr($trimmedphoneno, 0,2)=="07")
                    {
                        $phoneno = (int)"254".substr($trimmedphoneno, -9);
                    }
                    if (substr($trimmedphoneno, 0,4)=="+254")
                    {
                        $phoneno = (int)substr($trimmedphoneno, -12);
                    }
                        $insdata = array(
					       	'Cust_First_Name' => set_value('c_fname'),
					       	'Cust_Last_Name' => set_value('c_lname'),
					       	'Cust_Company' => set_value('c_company'),
					       	'Cust_Mobile_No' => $phoneno,
					       	'Cust_Fax_No' => set_value('c_faxno'),
					       	'Cust_Country' => set_value('c_country'),
					       	'Cust_City' => set_value('c_city'),
					       	'Cust_E_mail' => set_value('c_email'),
					       	'Cust_Website' => set_value('c_website'),
					       	'Cust_Street_Address' => set_value('c_street_address'),
					       	'Cust_Postal_Address' => set_value('c_postal_address'),
					       	'cust_type' => set_value('acctype')
						);
					// run insert model to write data to db
                        $tablename = 'customer';
                        $pkname = 'Cust_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_customers','refresh');
                        $data['url'] = 'admins/crud_customers';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function update_customer()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$custid = $this->input->post('custid');
		 $this->form_validation->set_rules('c_fname', 'First Name', 'required|trim|max_length[20]');			
		$this->form_validation->set_rules('c_lname', 'Last Name', 'max_length[20]');			
		$this->form_validation->set_rules('c_company', 'Company', 'trim|max_length[30]');			
		$this->form_validation->set_rules('c_mobileno', 'Mobile', 'required|trim');			
		$this->form_validation->set_rules('c_faxno', 'Fax', 'trim');			
		$this->form_validation->set_rules('c_country', 'Country', 'trim');			
		$this->form_validation->set_rules('c_city', 'City', 'trim');			
		$this->form_validation->set_rules('c_county', 'County', 'trim');			
		$this->form_validation->set_rules('c_email', 'Email', 'trim|valid_email');			
		$this->form_validation->set_rules('c_website', 'Website', '');			
		$this->form_validation->set_rules('c_street_address', 'Street Address', 'trim');			
		$this->form_validation->set_rules('c_postal_address', 'Postal Address', 'trim');			
		$this->form_validation->set_rules('acctype', 'Account Type', 'trim');
                
                $this->form_validation->set_error_delimiters('<br /><span class="red">', '</span>');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['countries']=$this->bus_model->getcountries();
			
                        $data['customers']=$this->bus_model->getplainresults('customer');
                        $data['custid'] = $custid;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('customer', 'Cust_id', $custid);
		$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $custid);
		
                        $this->load->view('manage_corp_customers',$data);
		}
		else
		{
                        
			
                        $trimmedphoneno = str_replace (" ", "", $this->input->post('c_mobileno'));
                    $phoneno = $trimmedphoneno;
                    if (substr($trimmedphoneno, 0,2)=="07")
                    {
                        $phoneno = (int)"254".substr($trimmedphoneno, -9);
                    }
                    if (substr($trimmedphoneno, 0,4)=="+254")
                    {
                        $phoneno = (int)substr($trimmedphoneno, -12);
                    }
                        $updatedata = array(
					       	'Cust_First_Name' => set_value('c_fname'),
					       	'Cust_Last_Name' => set_value('c_lname'),
					       	'Cust_Company' => set_value('c_company'),
					       	'Cust_Mobile_No' => $phoneno,
					       	'Cust_Fax_No' => set_value('c_faxno'),
					       	'Cust_Country' => set_value('c_country'),
					       	'Cust_City' => set_value('c_city'),
					       	'Cust_E_mail' => set_value('c_email'),
					       	'Cust_Website' => $this->input->post('c_website'),
					       	'Cust_Street_Address' => set_value('c_street_address'),
					       	'Cust_Postal_Address' => set_value('c_postal_address'),
					       	'cust_type' => set_value('acctype')
						);
					// run insert model to write data to db
                        $tablename = 'customer';
                        $pkname = 'Cust_id';
                        $pkvalue = $custid;
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_customers','refresh');
                        $data['url'] = 'admins/crud_customers';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function update_agent()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$aid = $this->input->post('aid');
		 $this->form_validation->set_rules('c_fname', 'First Name', 'required|trim|max_length[20]');			
		$this->form_validation->set_rules('c_lname', 'Last Name', 'max_length[20]');			
		$this->form_validation->set_rules('c_company', 'Company', 'trim|max_length[30]');			
		$this->form_validation->set_rules('c_mobileno', 'Mobile', 'required|trim');			
		$this->form_validation->set_rules('c_faxno', 'Fax', 'trim');			
		$this->form_validation->set_rules('c_country', 'Country', 'trim');			
		$this->form_validation->set_rules('c_city', 'City', 'trim');			
		$this->form_validation->set_rules('c_county', 'County', 'trim');			
		$this->form_validation->set_rules('c_email', 'Email', 'trim|valid_email');			
		$this->form_validation->set_rules('c_website', 'Website', '');			
		$this->form_validation->set_rules('c_street_address', 'Street Address', 'trim');			
		$this->form_validation->set_rules('c_postal_address', 'Postal Address', 'trim');
                
                $this->form_validation->set_error_delimiters('<br /><span class="red">', '</span>');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['countries']=$this->bus_model->getcountries();
			
		$data['agents']=$this->bus_model->getplainresults('agent');
                $data['deposits'] = NULL;
			
		$data['custdetails']=$this->bus_model->getonerowfromonetbl('agent', 'Agent_id', $aid);
		//$data['cusers']=$this->bus_model->getresultfromonetbl('cust_users', 'cust_id', $aid);
                        $data['aid'] = $aid;
		
		$this->load->view('manage_agents',$data);
		}
		else
		{
                        
			
                        $trimmedphoneno = str_replace (" ", "", $this->input->post('c_mobileno'));
                    $phoneno = $trimmedphoneno;
                    if (substr($trimmedphoneno, 0,2)=="07")
                    {
                        $phoneno = (int)"254".substr($trimmedphoneno, -9);
                    }
                    if (substr($trimmedphoneno, 0,4)=="+254")
                    {
                        $phoneno = (int)substr($trimmedphoneno, -12);
                    }
                        $updatedata = array(
					       	'Agent_First_Name' => set_value('c_fname'),
					       	'Agent_Last_Name' => set_value('c_lname'),
					       	'Agent_Company' => set_value('c_company'),
					       	'Agent_Mobile_No' => $phoneno,
					       	'Agent_Fax_No' => set_value('c_faxno'),
					       	'Agent_Country' => set_value('c_country'),
					       	'Agent_City' => set_value('c_city'),
					       	'Agent_E_mail' => set_value('c_email'),
					       	'Agent_Website' => $this->input->post('c_website'),
					       	'Agent_Street_Address' => set_value('c_street_address'),
					       	'Agent_Postal_Address' => set_value('c_postal_address')
						);
					// run insert model to write data to db
                        $tablename = 'agent';
                        $pkname = 'Agent_id';
                        $pkvalue = $aid;
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_agents','refresh');
                        $data['url'] = 'admins/crud_agents';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function update_bus_hire()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('bus', 'Bus', 'trim|required');
		
		$this->form_validation->set_rules('datefrom', 'Date From', 'trim|required');
		
		$this->form_validation->set_rules('dateto', 'Date To', 'trim|required');
		
		$this->form_validation->set_rules('name', 'Hirer name', 'trim|required');
		
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		

		
		if($this->form_validation->run() == FALSE)
		{
		$data['buses']=$this->bus_model->getplainresults('buses');		
		$data['bushire']=$this->bus_model->getBusHire();
		$this->load->view('manage_bus_hire',$data);
		}
		else
		{	
			$updatedata = array(
			'HirerName' => $this->input->post('name'),
			'HirerPhone' => $this->input->post('phone'),
			'HirerAddress' => $this->input->post('address'),
			'HirerEmail' => $this->input->post('email')
		);
                        $tablename = 'hirers';
                        $pkname = 'HirerId';
                        $pkvalue = $this->input->post('hirerid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			
			$updatedata = array(
			'BusId' => $this->input->post('bus'),
			'DateFrom' => $this->input->post('datefrom'),
			'DateTo' => $this->input->post('dateto'),
		);
                        $tablename = 'bus_hire';
                        $pkname = 'BusHireId';
                        $pkvalue = $this->input->post('bushireid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
		$data['buses']=$this->bus_model->getplainresults('buses');		
		$data['bushire']=$this->bus_model->getBusHire();
		//redirect('admins/crud_hiring','refresh');
                        $data['url'] = 'admins/crud_hiring';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
	
			function create_bus_hire()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('bus', 'Bus', 'trim|required');
		
		$this->form_validation->set_rules('datefrom', 'Date From', 'trim|required');
		
		$this->form_validation->set_rules('dateto', 'Date To', 'trim|required');
		
		$this->form_validation->set_rules('name', 'Hirer name', 'trim|required');
		
		$this->form_validation->set_rules('phone', '"Phone', 'trim|required');
		
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		

		
		if($this->form_validation->run() == FALSE)
		{
		$data['buses']=$this->bus_model->getplainresults('buses');		
		$data['bushire']=$this->bus_model->getBusHire();
			$this->load->view('manage_bus_hire',$data);
		}
		else
		{	
			$insdata = array(
			'HirerName' => $this->input->post('name'),
			'HirerPhone' => $this->input->post('phone'),
			'HirerAddress' => $this->input->post('address'),
			'HirerEmail' => $this->input->post('email')
		);
		                $tablename = 'hirers';
                        $pkname = 'HirerId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 1;
                      $hirer = $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
					  $hirerid = $hirer->HirerId;
			
			$insdata = array(
			'BusId' => $this->input->post('bus'),
			'HirerId' => $hirerid,
			'DateFrom' => $this->input->post('datefrom'),
			'DateTo' => $this->input->post('dateto'),
		);
					// run insert model to write data to db
                        $tablename = 'bus_hire';
                        $pkname = 'BusHireId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
		$data['buses']=$this->bus_model->getplainresults('buses');		
		$data['bushire']=$this->bus_model->getBusHire();
			//redirect('admins/crud_bus','refresh');
                        $data['url'] = 'admins/crud_bus';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

	
		function create_bus_terminus()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['termini']=$this->bus_model->getBusTermini();
		
		$this->form_validation->set_rules('region', 'Region', 'trim|required');
		
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		
		$this->form_validation->set_rules('terminusname', 'Terminus Name', 'trim|required');
		
		$this->form_validation->set_rules('terminusphone', 'Terminus Ext Phone', 'trim');
		
		$this->form_validation->set_rules('terminuscellphone', 'Terminus Cell Phone', 'trim');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('manage_bus_termini');
		}
		else
		{	
			$insdata = array(
			'RegionLkp' => $this->input->post('region'),
			'TownLkp' => $this->input->post('town'),
			'TerminusName' => $this->input->post('terminusname'),
			'TerminusPhone' => $this->input->post('terminusphone'),
			'TerminusCellPhone' => $this->input->post('terminuscellphone'),
			'TerminusEmail' => $this->input->post('email'),
			'AddedDate' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);	

					// run insert model to write data to db
                        $tablename = 'bus_termini';
                        $pkname = 'TerminusId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_termini','refresh');
                        $data['url'] = 'admins/crud_termini';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function update_bus_terminus()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['termini']=$this->bus_model->getBusTermini();
		
		$this->form_validation->set_rules('region', 'Region', 'trim|required');
		
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		
		$this->form_validation->set_rules('terminusname', 'Terminus Name', 'trim|required');
		
		$this->form_validation->set_rules('terminusphone', 'Terminus Ext Phone', 'trim|required');
		
		$this->form_validation->set_rules('terminuscellphone', 'Terminus Cell Phone', 'trim');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('manage_bus_termini');
		}
		else
		{	
			$updatedata = array(
			'RegionLkp' => $this->input->post('region'),
			'TownLkp' => $this->input->post('town'),
			'TerminusName' => $this->input->post('terminusname'),
			'TerminusPhone' => $this->input->post('terminusphone'),
			'TerminusCellPhone' => $this->input->post('terminuscellphone'),
			'TerminusEmail' => $this->input->post('email'),
			'AddedDate' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);	

                        $tablename = 'bus_termini';
                        $pkname = 'TerminusId';
                        $pkvalue = $this->input->post('terminusid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_termini','refresh');
                        $data['url'] = 'admins/crud_termini';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

			function create_route_price()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['prices']=$this->bus_model->getBuses();
		
		
		$this->form_validation->set_rules('from', 'Route From', 'trim|required');
		
		$this->form_validation->set_rules('to', 'Route To', 'trim|required');
		
		$this->form_validation->set_rules('bustype', 'Bus Type', 'trim|required');
		
		$this->form_validation->set_rules('seat', 'Seat Type', 'trim|required');
		
		$this->form_validation->set_rules('keamount', 'Ke Amount', 'trim|required');
		
		$this->form_validation->set_rules('ugamount', 'Ug Amount', 'trim');
		
		$this->form_validation->set_rules('tzamount', 'Tz Amount', 'trim');
		
		//$this->form_validation->set_rules('modellkp', 'Model', 'trim|required');
		//$this->form_validation->set_rules('seats', 'Seats', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
				
     	$this->load->view('manage_route_price',$data);
		}
		else
		{
                    
			$insdata = array(
			'RouteFrom' => $this->input->post('from'),
			'RouteTo' => $this->input->post('to'),
			'BusTypeLkp' => $this->input->post('bustype'),
			'SeatLkp' => $this->input->post('seat'),
			'KeAmount' => $this->input->post('keamount'),
			'UgAmount' => $this->input->post('ugamount'),
			'TzAmount' => $this->input->post('tzamount'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),	
			'ApprovedStatus' => 0,						
		);
					// run insert model to write data to db
                        $tablename = 'route_prices';
                        $pkname = 'PriceId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                        $rp = $this->bus_model->ifrouteseatpriceadded($this->input->post('bustype'),$this->input->post('seat'),$this->input->post('from'),$this->input->post('to'));
                        
                        if ($rp==NULL)
                        {
                     $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                        }
                        else {
                            $pkvalue = $rp->PriceId;
                            $this->bus_model->dbupdate($tablename, $insdata, $pkname, $pkvalue);
                        }
                      
                       
                         $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk','uacc_email', 'ugrp_name');
       $thisuser = $this->flexi_auth->get_user_id();
	    //$sql_where = 'uacc_group_fk = "3" and uacc_id != "'.$thisuser.'"';
            $sql_where = 'uacc_group_fk = "3"';
            
		
		$users = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                //$this->flexi_auth->get_users($sql_select, $sql_where)->result();
						 //$dids = $_POST['rec'];
                        // $tot = count($_POST['rec']);
						$useremails = "";
						 foreach ($users as $u)
						 {
							 $useremails .= $u->uacc_email.",";
						 }
						 //echo $useremails;
			
			
				$msg = 'Some route prices have been added that require your approval. Please login and approve. Thank you.';
                               
				
                                    $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
    
    $useremail = $this->user[0]->uacc_email;
					
					 $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '26';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;

                        $this->email->initialize($config);
                        $this->email->from($useremail, $this->user[0]->upro_first_name." ".$this->user[0]->upro_last_name);
                        //$this->email->to(trim($useremails,','));
                        $this->email->bcc('a.marcel77@gmail.com');
                        $this->email->subject('Route Price Approval');
                        $this->email->message($msg);

                        $this->email->send();
                      
			
		
                //redirect('admins/crud_prices','refresh');
                        $data['url'] = 'admins/crud_prices';
                        $this->load->view('data_success',$data);
                                }
		}else {
                 redirect('auth','refresh');
            }
		
	
        }
        
        function create_route_price2()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		 $bt = $this->bus_model->getonerowfromonetbl('bustype_lkp', 'BusTypeId', $this->input->post('bustypeid'));
		if ($bt->ifalltowns==1)
                {
		$towns = $this->bus_model->getTowns();
		
		//$data['townstot']=$this->bus_model->getTownstot();
                }
                else {
                   $towns = $this->bus_model->getwojoinedtbresultswithwhereclause('bus_type_towns', 'towns_lkp', 'TownId', 'BusTypeId', $this->input->post('bustypeid'));
                   //$data['townstot'] = $this->bus_model->getwojoinedtbtotrowswithwhereclause('bus_type_towns', 'towns_lkp', 'TownId', 'BusTypeId', $this->input->post('bustype'));
                }
		$similartofros = "";
		foreach ($towns as $t)
                {
                    foreach ($towns as $r)
                    {
                        //echo $this->input->post('keamount'.$t->TownId.'_'.$r->TownId)."<br /> hey";
                        //echo $this->input->post('ugamount'.$t->TownId.'_'.$r->TownId)."<br />";
                        //echo $this->input->post('tzamount'.$t->TownId.'_'.$r->TownId)."<br />";
                        
                        if ($t->TownId!==$r->TownId)
                        {
                        $pos = strpos($similartofros, $t->TownId.".".$r->TownId);
                        if ($pos==FALSE)
                        {
                          $pricecheck =  $this->bus_model->checkaddedrouteprices($this->input->post('bustypeid'),$this->input->post('seattypeid'),$t->TownId,$r->TownId);
                          
                        $similartofros .= $t->TownId.".".$r->TownId.","; 
                        $insdata = array(
			'RouteFrom' => $t->TownId,
			'RouteTo' => $r->TownId,
			'BusTypeLkp' => $this->input->post('bustypeid'),
			'SeatLkp' => $this->input->post('seattypeid'),
			'KeAmount' => $this->input->post('keamount'.$t->TownId.'_'.$r->TownId),
			'UgAmount' => $this->input->post('ugamount'.$t->TownId.'_'.$r->TownId),
			'TzAmount' => $this->input->post('tzamount'.$t->TownId.'_'.$r->TownId),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
					// run insert model to write data to db
                        $tablename = 'route_prices';
                        $pkname = 'PriceId';
                        $campuscode = ""; 
                        $iflastid = 0;
                        if ($pricecheck==NULL)
                          {
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                          }
                          else {
                              $pkvalue =$pricecheck;
                              $this->bus_model->dbupdate($tablename, $insdata, $pkname, $pkvalue);
                          }
                       
                      //$this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                        }
                        
                        else {
                            $ke = $this->input->post('keamount'.$t->TownId.'_'.$r->TownId);
                            $ug = $this->input->post('ugamount'.$t->TownId.'_'.$r->TownId);
                            $tz = $this->input->post('tzamount'.$t->TownId.'_'.$r->TownId);
                            if ($ke==0 and $ug==0 and $tz==0){
                                $ke = $this->input->post('keamount'.$r->TownId.'_'.$t->TownId);
                            $ug = $this->input->post('ugamount'.$r->TownId.'_'.$t->TownId);
                            $tz = $this->input->post('tzamount'.$r->TownId.'_'.$t->TownId);
                            
                            $similartofros .= $t->TownId.".".$r->TownId.","; 
                             $pricecheck =  $this->bus_model->checkaddedrouteprices($this->input->post('bustypeid'),$this->input->post('seattypeid'),$t->TownId,$r->TownId);
                            $insdata = array(
                            'RouteFrom' => $r->TownId,
                            'RouteTo' => $t->TownId,
                            'BusTypeLkp' => $this->input->post('bustypeid'),
                            'SeatLkp' => $this->input->post('seattypeid'),
                            'KeAmount' => $ke,
                            'UgAmount' => $ug,
                            'TzAmount' => $tz,
                            'DateAdded' => $this->dtime,
                            'AddedBy' => $this->flexi_auth->get_user_id()							
                    );
                                            // run insert model to write data to db
                            $tablename = 'route_prices';
                            $pkname = 'PriceId';
                            $campuscode = "";
                            //$pkvalue = $campuscode."_1";//do function to generate pkvalue
                            $iflastid = 0;if ($pricecheck==NULL)
                          {
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                          }
                          else {
                              $pkvalue =$pricecheck;
                              $this->bus_model->dbupdate($tablename, $insdata, $pkname, $pkvalue);
                          }
                            
                            }
                            else {
                                
                            
                            $similartofros .= $t->TownId.".".$r->TownId.","; 
                            $pricecheck =  $this->bus_model->checkaddedrouteprices($this->input->post('bustypeid'),$this->input->post('seattypeid'),$t->TownId,$r->TownId);
                            $insdata = array(
                            'RouteFrom' => $r->TownId,
                            'RouteTo' => $t->TownId,
                            'BusTypeLkp' => $this->input->post('bustypeid'),
                            'SeatLkp' => $this->input->post('seattypeid'),
                            'KeAmount' => $ke,
                            'UgAmount' => $ug,
                            'TzAmount' => $tz,
                            'DateAdded' => $this->dtime,
                            'AddedBy' => $this->flexi_auth->get_user_id()							
                    );
                                            // run insert model to write data to db
                            $tablename = 'route_prices';
                            $pkname = 'PriceId';
                            $campuscode = "";
                            //$pkvalue = $campuscode."_1";//do function to generate pkvalue
                            $iflastid = 0;
                          if ($pricecheck==NULL)
                          {
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                          }
                          else {
                              $pkvalue =$pricecheck;
                              $this->bus_model->dbupdate($tablename, $insdata, $pkname, $pkvalue);
                          }
                            
                            }
                        }
                    }
                    
                    }
                }
			
			
			//redirect('admins/crud_prices2','refresh');
                        $data['url'] = 'admins/crud_prices2';
                        $this->load->view('data_success',$data);
		
		}
                else {
                 redirect('auth','refresh');
            }
		
	}

			function create_season()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['prices']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('seasonname', 'Season Name', 'trim|required');
		
		$this->form_validation->set_rules('sdescription', 'Description', 'trim|required');
		
		$this->form_validation->set_rules('pricefrom', 'Price From', 'trim|required');
		
		$this->form_validation->set_rules('priceto', 'Price To', 'trim|required');
		
		$this->form_validation->set_rules('pamountke', 'KE Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('pamountug', 'UG Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('pamounttz', 'TZ Amount', 'trim|required|is_numeric');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
				
     	$this->load->view('manage_route_price',$data);
		}
		else
		{	if ($this->input->post('moreorless')=='+')
                            {
                                $pamountke = abs($this->input->post('pamountke'));
                                $pamountug = abs($this->input->post('pamountug'));
                                $pamounttz = abs($this->input->post('pamounttz'));
                            }
                            else {
                                $pamountke = '-'.abs($this->input->post('pamountke'));
                                $pamountug = '-'.abs($this->input->post('pamountug'));
                                $pamounttz = '-'.abs($this->input->post('pamounttz'));
                            }
			$insdata = array(
			'SeasonName' => $this->input->post('seasonname'),
			'Description' => $this->input->post('sdescription'),
			'PriceFrom' => $this->input->post('pricefrom'),
			'PriceTo' => $this->input->post('priceto'),
			'PAmountke' => $pamountke,			
			'PAmountug' => $pamountug,			
			'PAmounttz' => $pamounttz,			
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
					// run insert model to write data to db
                        $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_prices2#tab3','refresh');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function create_bus_schedule()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['schedules']=$this->bus_model->getBusSchedules2();
		
		$this->form_validation->set_rules('route', 'Route', 'trim|required');
		
		$this->form_validation->set_rules('bustypelkp', 'Bus Type', 'trim|required');
				
		$this->form_validation->set_rules('departuredatetime', 'Departure DateTime From', 'trim|required|callback_date_check');
				
		$this->form_validation->set_rules('departuredatetime2', 'Departure DateTime To', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
			$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
				
     	$this->load->view('manage_bus_schedule',$data);
		}
		else
		{	
                    $check_date = strtotime(date('Y-m-d',strtotime($this->input->post('departuredatetime')))); // '2010-02-27';
                    $end_date = strtotime(date('Y-m-d',strtotime($this->input->post('departuredatetime2')))); // '2010-03-24';
                    $bs = $this->bus_model->getonerowfromonetbl('bus_routes','RouteId',$this->input->post('route'));
                    //while (strtotime($date) <= strtotime($end_date))
                    while($check_date <= $end_date){
                        //echo date("Ymd", $check_date) . '<br>'; 
                        //$check_date += 86400;
                        if (isset($_POST['isactive']))
                        {
                            $ispecial = "1";
                        }
                        else {
                            $ispecial = "0";
                        }
                        //echo $ispecial; exit;
                        if ($this->bus_model->ifschedulealreadyadded($this->input->post('route'),$this->input->post('bustypelkp'),date('Y-m-d', $check_date),$ispecial)==FALSE)
                        {
                            if (!in_array(date('l', $check_date),$_POST['skipdays']))
                            {
			$insdata = array(
			'RouteLkp' => $this->input->post('route'),
			'BusTypeLkp' => $this->input->post('bustypelkp'),			
			'DepartureDateTime' => date('Y-m-d', $check_date),
			'Busno' => $bs->Busno,
                        'IsSpecial'=> $ispecial,
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
					// run insert model to write data to db
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                            }
                        }
                      //$check_date += 86400;
                        //echo date('Y-m-d',$check_date).'<br />';
                      $check_date = strtotime(date ("Y-m-d", strtotime("+1 day", $check_date)));
                      //echo date('Y-m-d',$check_date).'<br />';
                        
                    }
			//redirect('admins/crud_schedules','refresh');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function update_bus_schedule()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['schedules']=$this->bus_model->getBusSchedules2();
		
		$this->form_validation->set_rules('route', 'Route', 'trim|required');
		
		$this->form_validation->set_rules('bustypelkp', 'Bus Type', 'trim|required');
				
		$this->form_validation->set_rules('departuredatetime', 'Departure DateTime From', 'trim|required');
				
		//$this->form_validation->set_rules('departuredatetime2', 'Departure DateTime To', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
                    $data['prices']=$this->bus_model->getRoutePrices();
		
                    $data['departurearrival']=$this->bus_model->getReportDepartTimes();

                    $data['buses']=$this->bus_model->getBuses();

                    $data['terminals']=$this->bus_model->getBusTermini();

                    $data['availablebuses']=$this->bus_model->getAvailableBuses();

                    $data['routes']=$this->bus_model->getBusRoutes();

                    $data['route']=$this->bus_model->getplainresults('bus_routes');

                    $data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);

                    $data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');

                    $data['schedules']=$this->bus_model->getBusSchedules2();
			$this->load->view('manage_bus_schedule',$data);
		}
		else
		{	
                    
                    $bs = $this->bus_model->getonerowfromonetbl('bus_routes','RouteId',$this->input->post('route'));
			$updatedata = array(
			'RouteLkp' => $this->input->post('route'),
			'BusTypeLkp' => $this->input->post('bustypelkp'),
			'DepartureDateTime' => $this->input->post('departuredatetime'),
			'Busno' => $bs->Busno,
			'IsSpecial' => $this->input->post('isactive')							
		);
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $this->input->post('scheduleid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_schedules','refresh');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
			function update_schedule_time_update()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['schedules']=$this->bus_model->getBusSchedules2();
				
		$this->form_validation->set_rules('departuredatetime3', 'New Time', 'trim|required');
				
		//$this->form_validation->set_rules('departuredatetime2', 'Departure DateTime To', 'trim|required');
		
	
		if($this->form_validation->run() == FALSE)
		{
                    $data['prices']=$this->bus_model->getRoutePrices();
		
		$data['departurearrival']=$this->bus_model->getReportDepartTimes();
		
		$data['buses']=$this->bus_model->getBuses();
		
		$data['terminals']=$this->bus_model->getBusTermini();
		
		$data['availablebuses']=$this->bus_model->getAvailableBuses();
                
                $data['sms'] = NULL;
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['route']=$this->bus_model->getplainresults('bus_routes');
		
		$data['drivers']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',1);
		
		$data['conductors']=$this->bus_model->getresultfromonetbl('bus_staff','StaffJobLkp',2);
		
		$data['bustypes']=$this->bus_model->getBusTypes();
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
		
		$data['schedules']=$this->bus_model->getBusSchedules2();
                
                $data['schedid'] = $this->input->post('scheduleid');
                
                $data['sched'] = $this->bus_model->getBusSchedulesbySchedId($this->input->post('scheduleid'));
                $data['rep'] = NULL;
				
     	$this->load->view('manage_bus_schedule',$data);
		}
		else
		{	
			$updatedata = array(
			'NewDepartureTime' => $this->input->post('departuredatetime3')			
		);
                        $tablename = 'bus_schedule';
                        $pkname = 'ScheduleId';
                        $pkvalue = $this->input->post('scheduleid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_schedules','refresh');
                        $data['url'] = 'admins/crud_schedules';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function create_bus_maintenance()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['maintenance']=$this->bus_model->getBusMaintenance();
		
		$this->form_validation->set_rules('buslkp', 'Bus', 'trim|required');
		
		$this->form_validation->set_rules('admitted', 'Date Admitted', 'trim|required');
		
		$this->form_validation->set_rules('maintenancelkp', 'Maintenance Type', 'trim|required');
		
		$this->form_validation->set_rules('description', 'Notes', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('manage_bus_maintenance');
		}
		else
		{		
			$insdata = array(
			'BusLkp' => $this->input->post('buslkp'),
			'DateAdmitted' => $this->input->post('admitted'),
			'DateReleased' => $this->input->post('released'),
			'MaintenanceLkp' => $this->input->post('maintenancelkp'),
			'Description' => $this->input->post('description'),
			'IsSorted' => $this->input->post('issorted'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
					// run insert model to write data to db
                        $tablename = 'bus_maintenance';
                        $pkname = 'MaintenanceId';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
			//redirect('admins/crud_maintenance','refresh');
                        $data['url'] = 'admins/crud_maintenance';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}
		function update_bus_maintenance()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['maintenance']=$this->bus_model->getBusMaintenance();
		
		$this->form_validation->set_rules('buslkp', 'Bus', 'trim|required');
		
		$this->form_validation->set_rules('admitted', 'Date Admitted', 'trim|required');
		
		$this->form_validation->set_rules('maintenancelkp', 'Maintenance Type', 'trim|required');
		
		$this->form_validation->set_rules('description', 'Notes', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('manage_bus_maintenance');
		}
		else
		{		
			$updatedata = array(
			'BusLkp' => $this->input->post('buslkp'),
			'DateAdmitted' => $this->input->post('admitted'),
			'DateReleased' => $this->input->post('released'),
			'MaintenanceLkp' => $this->input->post('maintenancelkp'),
			'Description' => $this->input->post('description'),
			'IsSorted' => $this->input->post('issorted'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id()							
		);
                        $tablename = 'bus_maintenance';
                        $pkname = 'MaintenanceId';
                        $pkvalue = $this->input->post('maintenanceid');//do function to generate pkvalue
                        $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_maintenance','refresh');
                        $data['url'] = 'admins/crud_maintenance';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

				function update_route_price()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['prices']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('from', 'Route From', 'trim|required');
		
		$this->form_validation->set_rules('to', 'Route To', 'trim|required');
		
		$this->form_validation->set_rules('bustype', 'Bus Type', 'trim|required');
		
		$this->form_validation->set_rules('seat', 'Seat Type', 'trim|required');
		
		$this->form_validation->set_rules('keamount', 'Ke Amount', 'trim|required');
		
		$this->form_validation->set_rules('ugamount', 'Ug Amount', 'trim');
		
		$this->form_validation->set_rules('tzamount', 'Tz Amount', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
				
     	$this->load->view('manage_route_price',$data);
		}
		else
		{		
			$updatedata = array(
			'RouteFrom' => $this->input->post('from'),
			'RouteTo' => $this->input->post('to'),
			'BusTypeLkp' => $this->input->post('bustype'),
			'SeatLkp' => $this->input->post('seat'),
			'KeAmount' => $this->input->post('keamount'),
			'UgAmount' => $this->input->post('ugamount'),
			'TzAmount' => $this->input->post('tzamount'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),
			'ApprovedStatus' => 0							
		);
                        $tablename = 'route_prices';
                        $pkname = 'PriceId';
                        $pkvalue = $this->input->post('priceid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      
                      
                         $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk','uacc_email', 'ugrp_name');
       $thisuser = $this->flexi_auth->get_user_id();
	    //$sql_where = 'uacc_group_fk = "3" and uacc_id != "'.$thisuser.'"';
            $sql_where = 'uacc_group_fk = "3"';
            
		
		$users = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                
						$useremails = "";
						 foreach ($users as $u)
						 {
							 $useremails .= $u->uacc_email.",";
						 }
						
			
			
				$msg = 'Some route prices have been edited that require your approval. Please login and approve. Thank you.';
                               
				
                                    $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
    
    $useremail = $this->user[0]->uacc_email;
					
					 $this->load->library('email');

                        $config['protocol'] = 'smtp';
                        $config['mailpath'] = '/usr/sbin/sendmail';
                        $config['mailtype'] = 'html';
                        $config['charset'] = 'iso-8859-1';
                        $config['smtp_host'] = 'mail.mshop.co.ke';
                        $config['smtp_user'] = 'info@mshop.co.ke';
                        $config['smtp_pass'] = 'admin2011';
                        $config['smtp_port'] = '26';
                        $config['crlf'] = '\n';

                        $config['wordwrap'] = TRUE;

                        $this->email->initialize($config);
                        $this->email->from($useremail, $this->user[0]->upro_first_name." ".$this->user[0]->upro_last_name);
                        //$this->email->to(trim($useremails,','));
                        $this->email->bcc('a.marcel77@gmail.com');
                        $this->email->subject('Route Price Approval');
                        $this->email->message($msg);

                        $this->email->send();
                      
			//redirect('admins/crud_prices','refresh');
                        $data['url'] = 'admins/crud_prices';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

				function update_season()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
                    $sid = $this->input->post('sid');
		$data['prices']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('seasonname', 'Season Name', 'trim|required');
		
		$this->form_validation->set_rules('sdescription', 'Description', 'trim|required');
		
		$this->form_validation->set_rules('pricefrom', 'Price From', 'trim|required');
		
		$this->form_validation->set_rules('priceto', 'Price To', 'trim|required');
		
		$this->form_validation->set_rules('pamountke', 'KE Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('pamountug', 'UG Amount', 'trim|required|is_numeric');
		
		$this->form_validation->set_rules('pamounttz', 'TZ Amount', 'trim|required|is_numeric');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['prices']=$this->bus_model->getRoutePrices();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['seattypes']=$this->bus_model->getSeatTypes();
		
		$data['route'] = $this->bus_model->getplainresults('bus_routes');
		
		$data['bustypes']=$this->bus_model->getBusTypes();
		
		$data['seasons']=$this->bus_model->getSeasons();
				
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['towns']=$this->bus_model->getTowns();
                
                $data['seasondetails'] = $this->bus_model->getonerowfromonetbl('season_lkp', 'SeasonId', $sid);
                $data['sid'] = $sid;
				
     	$this->load->view('manage_route_price',$data);
		}
		else
		{		if ($this->input->post('moreorless')=='+')
                            {
                                $pamountke = abs($this->input->post('pamountke'));
                                $pamountug = abs($this->input->post('pamountug'));
                                $pamounttz = abs($this->input->post('pamounttz'));
                            }
                            else {
                                $pamountke = '-'.abs($this->input->post('pamountke'));
                                $pamountug = '-'.abs($this->input->post('pamountug'));
                                $pamounttz = '-'.abs($this->input->post('pamounttz'));
                            }
			$updatedata = array(
			'SeasonName' => $this->input->post('seasonname'),
			'Description' => $this->input->post('sdescription'),
			'PriceFrom' => $this->input->post('pricefrom'),
			'PriceTo' => $this->input->post('priceto'),
			'PAmountke' => $pamountke,			
			'PAmountug' => $pamountug,			
			'PAmounttz' => $pamounttz						
		);
                        $tablename = 'season_lkp';
                        $pkname = 'SeasonId';
                        $pkvalue = $sid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
			//redirect('admins/crud_prices2#tab3','refresh');
                        $data['url'] = 'admins/crud_prices2#tab3';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		function update_bus()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		$data['buses']=$this->bus_model->getBuses();
		
		$this->form_validation->set_rules('bustypelkp', 'Bus Type', 'trim|required');
		
		$this->form_validation->set_rules('ExternalColour', 'External Colour', 'trim|required');
		
		$this->form_validation->set_rules('InternalColour', 'Internal Colour', 'trim|required');
		
		$this->form_validation->set_rules('regno', 'Registration Number', 'trim|required');
		
		$this->form_validation->set_rules('makelkp', 'Make', 'trim|required');
		
		$this->form_validation->set_rules('insexpiry', 'Insurance Expiry Date', 'trim|required');
		
		//$this->form_validation->set_rules('modellkp', 'Model', 'trim|required');
		//$this->form_validation->set_rules('seats', 'Seats', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
			$data['bustypes']=$this->bus_model->getBusTypes();
		
                        $data['makes']=$this->bus_model->getMakes();

                        $data['models']=$this->bus_model->getModels();

                        $data['positions']=$this->bus_model->getplainresults('seat_position_lkp');

                        $data['buses']=$this->bus_model->getBuses();

                        $this->load->view('manage_bus',$data);
		}
		else
		{		
                    $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('inscopy');
                        $data3 =  $this->upload->data();
                        $inscopy = $data3['file_name'];
            
                        $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('inscomesacopy');
                        $data4 =  $this->upload->data();
                        $comesacopy = $data4['file_name'];
                        
                        $config['upload_path'] = './filecopies/';
                        $config['allowed_types'] = '*';

                        $this->load->library('upload', $config);

                       $this->upload->do_upload('logbookcopy');
                        $data5 =  $this->upload->data();
                        $logbookcopy = $data5['file_name'];
                        
			$updatedata = array(
			'BusTypeLkp' => $this->input->post('bustypelkp'),
			'ExternalColour' => $this->input->post('ExternalColour'),
			'InternalColour' => $this->input->post('InternalColour'),
			'RegistrationNumber' => $this->input->post('regno'),
			'MakeLkp' => $this->input->post('makelkp'),
			'DateBought' => $this->input->post('datebought'),
			'BusName' =>  $this->input->post('busname'),
			'BusNumber' =>  $this->input->post('busnumber'),
			'IsActive' =>  $this->input->post('isactive'),
			'DateAdded' => $this->dtime,
			'AddedBy' => $this->flexi_auth->get_user_id(),	
			'InsuranceExpiry' => $this->input->post('insexpiry'),
			'CopyOfInsurance' =>  $inscopy,
			'CopyOfComesa' =>  $comesacopy,
			'CopyOfLogbook' =>  $logbookcopy									
		);
		                $tablename = 'buses';
                        $pkname = 'BusId';
                        $pkvalue = $this->input->post('busid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);

			//redirect('admins/crud_bus','refresh');
                        $data['url'] = 'admins/crud_bus';
                        $this->load->view('data_success',$data);
		}
		}else {
                 redirect('auth','refresh');
            }
		
	}

		 function bus_unique($str)
   		 {
 		return $this->bus_model->checkBus($str);
    	}
		function create_bus_routes()	
		{
		$data['terminals']=$this->bus_model->getTowns();

		//$this->form_validation->set_rules('routecode', 'Route Code', 'trim|required');
		
		//$this->form_validation->set_rules('routename', 'Route Name', 'trim|required');
		
		$this->form_validation->set_rules('from', 'From', 'trim|required');
		
		$this->form_validation->set_rules('to', 'To', 'trim|required');
		
		$this->form_validation->set_rules('distance', 'Distance', 'trim|required');
		
		$this->form_validation->set_rules('buzno', 'Bus No', 'trim|required');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$this->load->view('manage_bus_routes',$data);
		}
		else
		{		
		//create logs	
                     $tf = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                    $tt = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('to'));
                    $array=$_POST['departure'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $atime = "";
		for($i=0; $i<$count; $i++) {
		if($_POST['townid'][$i]!=NULL and $_POST['departure'][$i]!=NULL and isset($_POST['checkthis'][$i])):
                    $atime = $this->bus_model->checkarrivaltimeinserted($_POST['departure'][$i]);
                if($atime<>""):
                    break;
                endif;
                endif;
                }
                $tm = $this->bus_model->getonerowfromonetbl('reporting_departure_times_lkp', 'TimeId', $atime);
                    $rtcode = $tf->TownSCode."-".$tt->TownSCode.date('g:iA',strtotime($tm->ArrivalDeparture))."(".$this->input->post('buzno').")";
                    
			if($this->bus_model->createBusRoute($rtcode,$this->flexi_auth->get_user_id(),$this->dtime))
			{
				$data['url'] = 'admins/crud_routes';
                        $this->load->view('data_success',$data);
			}
			else
			{
				$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$this->load->view('manage_bus_routes',$data);		
			}
		}
		
	}
			function update_bus_route()	
	{	
		if ($this->flexi_auth->is_logged_in())
            {	
		//$this->form_validation->set_rules('routecode', 'Route Code', 'trim|required');
		
		//$this->form_validation->set_rules('routename', 'Route Name', 'trim|required');
		
		$this->form_validation->set_rules('from', 'From', 'trim|required');
		
		$this->form_validation->set_rules('to', 'To', 'trim|required');
		
		$this->form_validation->set_rules('distance', 'Distance', 'trim|required');
		
		$this->form_validation->set_rules('buzno', 'Bus No', 'trim|required');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$this->load->view('manage_bus_routes',$data);
		}
		else
		{		
				
			if($this->bus_model->updateBusRoute2($this->flexi_auth->get_user_id(),$this->dtime))
			{
				$data['url'] = 'admins/crud_routes';
                        $this->load->view('data_success',$data);
			}
			else
			{
				$data['regions']=$this->bus_model->getRegions();
		
		$data['routecodes']=$this->bus_model->getRouteCodes();
		
		$data['terminals']=$this->bus_model->getTowns();
		
		$data['routes']=$this->bus_model->getBusRoutes();
		
		$data['times']=$this->bus_model->getReportDepartTimes();
		
		$data['towns']=$this->bus_model->getTowns();
		
		$this->load->view('manage_bus_routes',$data);		
			}
		}
		}else {
                 redirect('auth','refresh');
            }

}

function remail()
	{
		$mb = imap_open("{mail.mtl.co.ke:143/imap}","info@mtl.co.ke", "admin2011" );

                $messageCount = imap_num_msg($mb);
                $cnt = 1;
                for( $MID = 1; $MID <= 2; $MID++ )
                {
                   $EmailHeaders = imap_headerinfo( $mb, $MID );
                   $Body = imap_fetchbody( $mb, $MID, 1 );
                   echo $cnt." Subject: ".$EmailHeaders." | Body: ".$Body;
                }
                echo "------------------------------------------<br>";
                echo "total messages unread: ".$messageCount;
	}
        
        function CountUnreadMail()
        {
            $mbox = imap_open("{mail.mtl.co.ke:993/imap/ssl/novalidate-cert}INBOX", "info@mtl.co.ke", "admin2011");
            $count = 0;
            if (!$mbox) {
                echo "Error";
            } else {
                $headers = imap_headers($mbox);
                foreach ($headers as $mail) {
                    $flags = substr($mail, 0, 4);
                    $isunr = (strpos($flags, "U") !== false);
                    if ($isunr)
                    {
                    $count++;
                    echo $mail."<br>";
                    $user = strstr($mail, '315', true);
                    if ($user)
                    {
                        imap_clearflag_full($mbox, '315', "//Seen");
                    }
                    }
                    
                }
            }

            imap_close($mbox);
            echo $count;
            
        }
        
        function notify_customer_reserved_seat()
        {
           $reservedseats = $this->bus_model->getreservedseats();
           if ($reservedseats<>NULL)
           {
               if ($this->bus_model->check_if_reserved_seat_notification_has_not_been_sent($reservedseats->TicketId)==TRUE)
               {
                   $strimmedphoneno = str_replace (" ", "", $reservedseats->mobileno);
                    $sphoneno = $strimmedphoneno;
                    if (substr($strimmedphoneno, 0,2)=="07")
                    {
                        $sphoneno = "254".substr($strimmedphoneno, -9);
                    }
                    else if (substr($strimmedphoneno, 0,4)=="+254" || substr($strimmedphoneno, 0,4)=="+255" || substr($strimmedphoneno, 0,4)=="+256")
                    {
                        $sphoneno = (int)substr($strimmedphoneno, -12);
                    }
					else {
						$sphoneno = $strimmedphoneno;
					}
                   $this->_sendsms($sphoneno);
               }
           }
           echo "No reserved ticket found.";
        }
	
	function _sendsms($phoneno)
	{
            //insert sms in db
            $content = "Hello. Please complete your ticket purchase in the next 30 minutes, otherwise it will be cancelled. Thank you.";
            $smsfunction = "Corporate Ticket Notification";
            $smslength = strlen($content);
            if ($smslength<=160)
            {
                $noofsms = 1;
            }
            else {
                    $noofsms = ceil($smslength/160);
                
            }
	// Infobip's POST URL
		$postUrl = "http://api2.infobip.com/api/sendsms/xml";
		// XML-formatted data
		$xmlString =
		"<SMS>
		<authentification>
		<username>Modernltd</username>
		<password>moderncoast</password>
		</authentification>
		<message>
		<sender>ModernCoast</sender>
		<text>$content</text>
		</message>
		<recipients>
		<gsm>$phoneno</gsm>
		</recipients>
		</SMS>";
		// previously formatted XML data becomes value of "XML" POST variable
		$fields = "XML=" .urlencode($xmlString);
		// in this example, POST request was made using PHP's CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);
		// write out the response
		//return $response;
                
            $this->bus_model->insert_sms($smslength,$noofsms,$phoneno,$smsfunction);
	}
	
	function _sendsmsticket($ticketno,$townfrom,$townto,$seatno,$phoneno)
	{
            //insert sms in db
            $content = "Booking details: ".$ticketno.", route: ".$townfrom."-".$townto." Seat No: ".$seatno.". Have a safe journey";
            $smsfunction = "Corporate Ticket Notification";
            $smslength = strlen($content);
            if ($smslength<=160)
            {
                $noofsms = 1;
            }
            else {
                    $noofsms = ceil($smslength/160);
                
            }
	// Infobip's POST URL
		$postUrl = "http://api2.infobip.com/api/sendsms/xml";
		// XML-formatted data
		$xmlString =
		"<SMS>
		<authentification>
		<username>Modernltd</username>
		<password>moderncoast</password>
		</authentification>
		<message>
		<sender>ModernCoast</sender>
		<text>$content</text>
		</message>
		<recipients>
		<gsm>$phoneno</gsm>
		</recipients>
		</SMS>";
		// previously formatted XML data becomes value of "XML" POST variable
		$fields = "XML=" .urlencode($xmlString);
		// in this example, POST request was made using PHP's CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);
		// write out the response
		//return $response;
                
            $this->bus_model->insert_sms($smslength,$noofsms,$phoneno,$smsfunction);
	}
        
  function crud_corp_sales()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $corp = 0;
                $terminus = 0;
                $clerk = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['corps'] = $this->bus_model->getplainresults('customer');
                //corp
                $data['corp'] = 'All';
                $data['rep'] = NULL;
                //total ticket sales
                $tts = $this->bus_model->get_total_corp_ticket_sales($corp,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                
               
                
                //date title
                $data['datettitle'] = "Today";
  
  $this->load->view('manage_corp_sales',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_corp_sales_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $corp = 0;
                $terminus = 0;
                $clerk = 0;
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                if (isset($_POST['corp']))
                 {
                 $array=$_POST['corp'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $custids = "";
                 for($i=0; $i<$count; $i++) 
                 {
                     $custids .=$_POST['corp'][$i].",";
                 }
                 $data['custids'] = trim($custids,',');
                 }
                 else {
                      $custids = "";
                 }
                 //echo $custids."2";
                $data['corps'] = $this->bus_model->getplainresults('customer');//all customers
                $data['corpz'] = $this->bus_model->get_customers_selected($custids);///selected customers
                //corp
                $data['corp'] = 'All';
                $data['rep'] = 1;
                //total ticket sales
                $tts = $this->bus_model->get_total_corp_ticket_sales($clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                
               
                
                //date title
                //$data['datettitle'] = "Today";
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('manage_corp_sales',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_bus_revenues()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $bus = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['buses'] = $this->bus_model->getplainresults('buses');
                //corp
                $data['bus'] = 'All';
                $data['rep'] = NULL;
                //total ticket sales
                $tts = $this->bus_model->get_total_bus_ticket_sales($bus,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                
               
                
                //date title
                $data['datettitle'] = "Today";
  
  $this->load->view('reports/manage_bus_revenues',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_bus_revenues_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $bus = 0;
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                if (isset($_POST['bus']))
                 {
                 $array=$_POST['bus'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $busids = "";
                 for($i=0; $i<$count; $i++) 
                 {
                     $busids .=$_POST['bus'][$i].",";
                 }
                 }
                 else {
                      $busids = "";
                 }
                 //echo $busids;
                 $data['busids'] = trim($busids,',');
                 //echo $custids."2";
                $data['buses'] = $this->bus_model->getplainresults('buses');//all customers
                $data['busesz'] = $this->bus_model->get_buses_selected(trim($busids,','));//selected customers
                //var_dump($data['busesz']);
                //corp
                $data['bus'] = 'All';
                $data['rep'] = 1;
                //total ticket sales
                $tts = $this->bus_model->get_total_bus_ticket_sales($bus,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                
               
                
                //date title
                //$data['datettitle'] = "Today";
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('reports/manage_bus_revenues',$data);
  }else {
                 redirect('auth','refresh');
            }
 }
        
  function crud_daily_expenses()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = date('Y-m-d');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                //total ticket sales
                $petts = $this->bus_model->get_total_petty_cash_daily($datef);
                
                $data['petts'] = $petts;
                
                //date title
                $data['datettitle'] = "Daily Expense Today";
                
                //graph data
                $data['graphdata'] = 1;
                $typeid = 2;
                $html = "";
                $xml = "";
                $ptitle = "Daily Expense";
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Daily Expense Summary' subcaption='For the date $datef' xAxisName='Towns' yAxisMinValue='0' yAxisName='Expense' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($petts<>NULL)
                {
                    foreach ($petts as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TownName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Expense' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
  
  $this->load->view('reports/manage_daily_expenses',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_daily_expenses_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = $this->input->post('datef');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                //total ticket sales
                $petts = $this->bus_model->get_total_petty_cash_daily($datef);
                
                $data['petts'] = $petts;
                //graph data
                $data['graphdata'] = 1;
                $xml = "";
                $ptitle = "Daily Expense";
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Daily Expense Summary' subcaption='For the date $datef' xAxisName='Towns' yAxisMinValue='0' yAxisName='Expense' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($petts<>NULL)
                {
                    foreach ($petts as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TownName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Expense' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
               
                
                //date title
                $data['datettitle'] = "Daily Expense on ".date('d/m/Y',strtotime($datef));
  
  $this->load->view('reports/manage_daily_expenses',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_banking_report()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                //total ticket sales
                $bankeds = $this->bus_model->get_total_banked($datef,$datet);
                $data['bankeds'] = $bankeds;
                //graph data
                $data['graphdata'] = 1;
                $xml = "";
                $ptitle = "Banking Report";
                $datefd = date('d/m/Y',strtotime($datef));
                $datetd = date('d/m/Y',strtotime($datet));
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Banking Report Summary' subcaption='From $datefd to $datetd' xAxisName='Termini' yAxisMinValue='0' yAxisName='Banked Amount' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($bankeds<>NULL)
                {
                    foreach ($bankeds as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TerminusName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Banked Amount' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
               
                
                //date title
                $data['datettitle'] = "Banking Report Today";
  
  $this->load->view('reports/manage_banking_report',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_banking_report_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                //total ticket sales
                
                $bankeds = $this->bus_model->get_total_banked($datef,$datet);
                $data['bankeds'] = $bankeds;
                //graph data
                $data['graphdata'] = 1;
                $xml = "";
                $ptitle = "Banking Report";
                $datefd = date('d/m/Y',strtotime($datef));
                $datetd = date('d/m/Y',strtotime($datet));
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Banking Report Summary' subcaption='From $datefd to $datetd' xAxisName='Termini' yAxisMinValue='0' yAxisName='Banked Amount' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($bankeds<>NULL)
                {
                    foreach ($bankeds as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TerminusName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Banked Amount' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
                
               
                
                //date title
                $data['datettitle'] = "Banking Report from ".date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
  
  $this->load->view('reports/manage_banking_report',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_voucher_report()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                //total ticket sales
                $vs = $this->bus_model->get_total_voucher_daily($datef,$datet);
                $data['vs'] = $vs;
                //graph data
                $data['graphdata'] = 1;
                $xml = "";
                $ptitle = "Voucher Report";
                $datefd = date('d/m/Y',strtotime($datef));
                $datetd = date('d/m/Y',strtotime($datet));
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Voucher Report Summary' subcaption='From $datefd to $datetd' xAxisName='Towns' yAxisMinValue='0' yAxisName='Voucher Amount' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($vs<>NULL)
                {
                    foreach ($vs as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TownName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Voucher Amount' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
               
                
                //date title
                $data['datettitle'] = "Voucher Report Today";
  
  $this->load->view('reports/manage_voucher_report',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_voucher_report_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
            $this->load->helper('array');
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['rep'] = 1;
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                //total ticket sales
                $vs = $this->bus_model->get_total_voucher_daily($datef,$datet);
                $data['vs'] = $vs;
                //graph data
                $data['graphdata'] = 1;
                $xml = "";
                $ptitle = "Voucher Report";
                $datefd = date('d/m/Y',strtotime($datef));
                $datetd = date('d/m/Y',strtotime($datet));
                $colorcodes = array("AFD8F8","F6BD0F","8BBA00","FF8E46","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE");
                $xml .= "<graph caption='Voucher Report Summary' subcaption='From $datefd to $datetd' xAxisName='Towns' yAxisMinValue='0' yAxisName='Voucher Amount' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
                if ($vs<>NULL)
                {
                    foreach ($vs as $c)
                    {
                        $cc = random_element($colorcodes);
                        $town = $c->TownName;
                        $value = $c->amountke;
                        $xml .= "<set name='$town' value='$value' hoverText='Voucher Amount' color='$cc'/>";
                    }
                }
                     $xml .= "</graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
                
               
                
                //date title
                $data['datettitle'] = "Voucher Report from ".date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
  
  $this->load->view('reports/manage_voucher_report',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_bus_clocked_hours()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $bus = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['buses'] = $this->bus_model->getplainresults('buses');
                //corp
                $data['bus'] = 'All';
                $data['rep'] = NULL;
                
               
                
                //date title
                $data['datettitle'] = "Today";
  
  $this->load->view('reports/manage_bus_clocked_hours',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_bus_clocked_hours_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $bus = 0;
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                if (isset($_POST['bus']))
                 {
                 $array=$_POST['bus'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $busids = "";
                 for($i=0; $i<$count; $i++) 
                 {
                     $busids .=$_POST['bus'][$i].",";
                 }
                 }
                 else {
                      $busids = "";
                 }
                 //echo $busids;
                 $data['busids'] = trim($busids,',');
                 //echo $custids."2";
                $data['buses'] = $this->bus_model->getplainresults('buses');//all customers
                $data['busesz'] = $this->bus_model->get_buses_selected(trim($busids,','));//selected customers
                //var_dump($data['busids']);
                //corp
                $data['bus'] = 'All';
                $data['rep'] = 1;
                
               
                
                //date title
                //$data['datettitle'] = "Today";
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('reports/manage_bus_clocked_hours',$data);
  }else {
                 redirect('auth','refresh');
            }
 }
        
  function crud_cash_recon()
 { 
        if ($this->flexi_auth->is_logged_in()){
               $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk','ugrp_name');
       
	    $sql_where = 'uacc_group_fk = "1" or uacc_group_fk = "2" or uacc_group_fk = "3"';
		
		$data['clerks'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
                //$data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['datettitle'] = "Today";
                $clerk = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                if ($clerk[0]->uacc_group_fk==1)
                {
                    redirect('auth');
                }
  
  $this->load->view('manage_cash_recon',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_cash_recon_special()
 { 
        if ($this->flexi_auth->is_logged_in()){
               $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk','ugrp_name');
       
	    $sql_where = 'uacc_group_fk = "1" or uacc_group_fk = "2" or uacc_group_fk = "3"';
		
		$data['clerks'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
                //$data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['datettitle'] = "Today";
                $clerk = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                if ($clerk[0]->uacc_group_fk==1)
                {
                    redirect('auth');
                }
  
  $this->load->view('manage_cash_recon_special',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $townf = 0;
                $terminus = 0;
                $clerk = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['townz'] = $this->bus_model->getplainresults('towns_lkp');
                //branch
                $data['branch'] = 'All';
                //clerk
                $data['clerk'] = 'All';
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;
                
                //date title
                $data['datettitle'] = "Today";
  
  $this->load->view('manage_ticket_sales',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $townf = $this->input->post('town');
                $terminus = $this->input->post('terminus');
                $clerk = $this->input->post('clerk');
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $data['datef'] = $datef;
                $data['datet'] = $datet;
                
                if (isset($_POST['town']))
                 {
                 $array=$_POST['town'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $townids = "";
                 for($i=0; $i<$count; $i++) 
                 {
                     $townids .=$_POST['town'][$i].",";
                 }
                 $data['townids'] = trim($townids,',');
                 }
                 else {
                      $townids = "";
                 }
                 //echo $townids;
                 $townidsarray = explode(",",trim($townids,','));
                //$data['corps'] = $this->bus_model->getplainresults('customer');//all customers
                $data['townz'] = $this->bus_model->get_towns_selected($townidsarray);///selected customers
                //var_dump($data['townz']);
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                
               
                
                //date title
                $data['datettitle'] = "Today";
  
  $this->load->view('manage_ticket_sales',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales_branch_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $terminus = $this->input->post('terminus');
                $clerk = $this->input->post('clerk');
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$townf= $this->user[0]->t_station;
                $data['termini'] = $this->bus_model->getresultfromonetbl('bus_termini', 'TownLkp', $townf);
                //branch
                if ($townf==0):
                    $data['branch'] = 'All';
                else:
                    $br = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
                    $data['branch'] = $br->TownName;
                    $data['townid'] = $townf;
                endif;
                
                
                
                //clerk
                if ($clerk==0):
                    $data['clerk'] = 'All';
                else:
                    $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
                    $data['clerk']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                endif;
                
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;
                
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('manage_ticket_sales_branch',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales_clerk()
 { 
        if ($this->flexi_auth->is_logged_in()){
                //$townf = 0;
                $terminus = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$townf= $this->user[0]->t_station;
                $clerk = $this->user[0]->uacc_id;
                //branch
                $br = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
                    $data['branch'] = $br->TownName;
                    $data['townid'] = $townf;
                //clerk
                $data['clerk'] = 'All';
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;
                
                //date title
                $data['datettitle'] = "Today";
                $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                $data['clerk'] = $user[0]->upro_first_name." ".$user[0]->upro_last_name;
                //echo $user[0]->t_termini; exit;
                $data['station'] = $this->bus_model->getonerowfromonetbl('bus_termini', 'TerminusId', $user[0]->t_termini)->TerminusName;
  
  $this->load->view('manage_ticket_sales_clerk',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales_clerk_refine()
 { 
        if ($this->flexi_auth->is_logged_in()){
                $terminus = 0;
                $datef = $this->input->post('datef');
                $datet = $this->input->post('datet');
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$townf= $this->user[0]->t_station;
                $clerk = $this->user[0]->uacc_id;
                //branch
                if ($townf==0):
                    $data['branch'] = 'All';
                else:
                    $br = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
                    $data['branch'] = $br->TownName;
                    $data['townid'] = $townf;
                endif;
                
                
                
                //clerk
                if ($clerk==0):
                    $data['clerk'] = 'All';
                else:
                    $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
                    $data['clerk']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                endif;
                
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;
                
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('manage_ticket_sales_clerk',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_reports()
 { 
        if ($this->flexi_auth->is_logged_in()){
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
                $data['channels'] = $this->bus_model->getplainresults('channels');
                $data['paymodes'] = $this->bus_model->getplainresults('paymodes');
                $data['rep'] = NULL;
                
                $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk','ugrp_name');
       
	    $sql_where = 'uacc_group_fk = "1"';
		
		$data['clerks'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                $this->flexi_auth->get_users($sql_select, $sql_where)->result();
  
  $this->load->view('manage_reports',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_graphical_reports()
 { 
        if ($this->flexi_auth->is_logged_in()){
		        $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
                $data['channels'] = $this->bus_model->getplainresults('channels');
                $data['paymodes'] = $this->bus_model->getplainresults('paymodes');
                $data['rep'] = NULL;
                
                $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk', 'ugrp_name');
       
	    $sql_where = 'uacc_group_fk = "1"';
		
		$data['clerks'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                $this->flexi_auth->get_users($sql_select, $sql_where)->result();
                 $doc = new DOMDocument( );
    $ele = $doc->createElement( 'Root' );
    $ele->nodeValue = 'Hello XML World';
    $doc->appendChild( $ele );
    $doc->save('MyXmlFile.xml');
    
    
  
  $this->load->view('manage_graphical_reports',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_vouchers()
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $data['vouchers']=$this->bus_model->getplainresults('vouchers');
  
  $this->load->view('manage_vouchers',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function create_vouchers() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  
  $this->form_validation->set_rules('vname', 'Name', 'trim|required');
  
  $this->form_validation->set_rules('description', 'Description', 'trim|required');
  
  $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|is_numeric');
  
  $this->form_validation->set_rules('vamount', 'Amount', 'trim|required|is_numeric');
  
  $this->form_validation->set_rules('vdatefrom', 'Date From', 'trim|required');
  
  $this->form_validation->set_rules('vdateto', 'Date To', 'trim|required');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['vouchers']=$this->bus_model->getplainresults('vouchers');
   $data['customers']=$this->bus_model->getplainresults('card_details');
                        $this->load->view('manage_vouchers',$data);
  }
  else
  {  
   $insdata = array(
   'vname' => $this->input->post('vname'),
   'description' => $this->input->post('description'),
   'quantity' => $this->input->post('quantity'),
   'vamount' => $this->input->post('vamount'),
   'vdatefrom' => $this->input->post('vdatefrom'),
   'vdateto' => $this->input->post('vdateto')       
  );
     // run insert model to write data to db
                        $tablename = 'vouchers';
                        $pkname = 'vouchers_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      
   $data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
                        $this->load->view('manage_vouchers',$data);
  }
  }else {
                 redirect('auth','refresh');
            }
  
 }
  function crud_voucher_items($voucherid)
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $data['vouchers']=$this->bus_model->getplainresults('vouchers');
  //$data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
  $data['voucherid'] = $voucherid;
  $this->load->view('manage_vouchers',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function view_voucher_items($voucherid)
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $data['voucheritems']=$this->bus_model->getcustomervouchers($voucherid);
  $data['vouchers']=$this->bus_model->getplainresults('vouchers');
  $data['voucherid'] = $voucherid;
  $this->load->view('manage_vouchers',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function create_voucher_items() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  $data['voucherid'] = $this->input->post('voucherid');
  
  $this->form_validation->set_rules('CustomerId', 'Customer', 'required');
  
  $this->form_validation->set_rules('voucher_code', 'Description', 'trim');
  
  $this->form_validation->set_rules('dateawarded', 'Date Awarded', 'trim');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
   $data['customers']=$this->bus_model->getplainresults('card_details');
   $this->load->view('manage_voucher_items',$data);
  }
  else
  { 
   $vouchercode = $this->bus_model->_generateVoucherCode();
   $insdata = array(
   'CustomerId' => $this->input->post('CustomerId'),
   'vouchers_id' => $this->input->post('voucherid'),
   'voucher_code' => $vouchercode,
   'dateawarded' => date('Y-m-d')      
  );
     // run insert model to write data to db
                        $tablename = 'voucher_items';
                        $pkname = 'voucher_items_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      
   $data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
                        $this->load->view('manage_vouchers',$data);
  }
  }else {
                 redirect('auth','refresh');
            }
  
 }
function edit_voucher($voucherid)
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['row']=$this->bus_model->getonerowfromonetbl('vouchers','vouchers_id',$voucherid);
  $data['voucherid'] = $voucherid;
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $this->load->view('manage_vouchers',$data);
  }else {
                 redirect('auth','refresh');
            }
 }
   function edit_voucher_update() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  $voucherid = $this->input->post('voucherid');
  
  $this->form_validation->set_rules('vname', 'Name', 'trim|required');
  
  $this->form_validation->set_rules('description', 'Description', 'trim|required');
  
  $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|is_numeric');
  
  $this->form_validation->set_rules('vamount', 'Amount', 'trim|required|is_numeric');
  
  $this->form_validation->set_rules('vdatefrom', 'Date From', 'trim|required');
  
  $this->form_validation->set_rules('vdateto', 'Date To', 'trim|required');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['voucher']=$this->bus_model->getonerowfromonetbl('vouchers','vouchers_id',$voucherid);
   $data['customers']=$this->bus_model->getplainresults('card_details');
  
  $this->load->view('manage_vouchers',$data);
  }
  else
  {  
   $updatedata = array(
   'vname' => $this->input->post('vname'),
   'description' => $this->input->post('description'),
   'quantity' => $this->input->post('quantity'),
   'vamount' => $this->input->post('vamount'),
   'vdatefrom' => $this->input->post('vdatefrom'),
   'vdateto' => $this->input->post('vdateto')       
  );
     // run insert model to write data to db
                        $tablename = 'vouchers';
                        $pkname = 'vouchers_id';
                        $pkvalue = $voucherid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename,$updatedata,$pkname,$pkvalue);
                      
   $data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
                        $this->load->view('manage_vouchers',$data);
  }
  }else {
                 redirect('auth','refresh');
            }
  
 }
  function edit_voucher_items($voucheritemid)
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['voucheritem']=$this->bus_model->getonerowfromonetbl('voucher_items','voucher_items_id',$voucheritemid);
  $data['voucheritemid'] = $voucheritemid;
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $this->load->view('manage_voucher_items_edit',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function edit_voucher_items_update() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  $voucheritemid = $this->input->post('voucheritemid');
  
  $this->form_validation->set_rules('CustomerId', 'Customer', 'required');
  
  $this->form_validation->set_rules('voucher_code', 'Description', 'trim|required');
  
  $this->form_validation->set_rules('dateawarded', 'Date Awarded', 'trim|required');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['voucheritem']=$this->bus_model->getonerowfromonetbl('voucher_items','voucher_items_id',$voucheritemid);
  $data['voucheritemid'] = $voucheritemid;
  $data['customers']=$this->bus_model->getplainresults('card_details');
  $this->load->view('manage_voucher_items_edit',$data);
  }
  else
  { 
                    //$vouchercode = $this->bus_model->_generateVoucherCode();
   $updatedata = array(
   'CustomerId' => $this->input->post('CustomerId'),
   'vouchers_id' => $this->input->post('voucherid'),
   'dateawarded' => date('Y-m-d')      
  );
     // run insert model to write data to db
                        $tablename = 'voucher_items';
                        $pkname = 'voucher_items_id';
                        $pkvalue = $voucheritemid;//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename,$updatedata,$pkname,$pkvalue);
                      
   $data['vouchers']=$this->bus_model->getwojoinedtbresults('vouchers','voucher_items','vouchers_id');
                        $this->load->view('manage_voucher_items',$data);
  }

  }else {
                 redirect('auth','refresh');
            }
  
 }
	function logout()
	{
		$this->session->sess_destroy();
		
		$this->load->view('home');
	}	
function crud_return_discounts()
 { 
        if ($this->flexi_auth->is_logged_in()){
  $data['rdiscs']=$this->bus_model->getplainresults('return_discount');
  $this->load->view('manage_return_discounts',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function create_return_discounts() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  
  $this->form_validation->set_rules('percent', 'Percent', 'required');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['rdiscs']=$this->bus_model->getplainresults('return_discount');
                        $this->load->view('manage_return_discounts',$data);
  }
  else
  { 
   $insdata = array(
   'percent' => set_value('percent'),
   'user_id' => $this->flexi_auth->get_user_id()   
  );
     // run insert model to write data to db
                        $tablename = 'return_discount';
                        $pkname = 'return_discount_id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      
   $data['rdiscs']=$this->bus_model->getplainresults('return_discount');
                        $this->load->view('manage_return_discounts',$data);
  }
  }else {
                 redirect('auth','refresh');
            }
  
 }
 			  function getseatprice($bustypeseattownftownt)
	{	
            $sb = explode(".", $bustypeseattownftownt);
            $bustypeid= $sb[0];
            $seat = $sb[1];
            if ($bustypeid==1){
                if ($seat==1 || $seat==4)
                {
                    $seatclass = 1;
                }
                else if ($seat==2 || $seat==3 || $seat==5 || $seat==6)
                {
                    $seatclass = 2;
                }
                else {
                    $seatclass = 3;
                }
            }
            if ($bustypeid==2){
                if ($seat==1 || $seat==2 || $seat==5 || $seat==6)
                {
                    $seatclass = 1;
                }
                else if ($seat==3 || $seat==4 || $seat==7 || $seat==8)
                {
                    $seatclass = 2;
                }
                else {
                    $seatclass = 3;
                }
            }
            if ($bustypeid==3){
                if ($seat==1 || $seat==2 || $seat==3 || $seat==4)
                {
                    $seatclass = 1;
                }
                else {
                    $seatclass = 3;
                }
            }
            if ($bustypeid==4){
                if ($seat==1 || $seat==2)
                {
                    $seatclass = 1;
                }
                else if ($seat==3 || $seat==4 || $seat==5 || $seat==6)
                {
                    $seatclass = 2;
                }
                else {
                    $seatclass = 3;
                }
            }
            $routefrom = $sb[2];
            $routeto = $sb[3];
                //$details = $this->bus_model->routesingleseatprice($scheduleid,$seat);
                $details = $this->bus_model->routeseatprice($bustypeid,$seatclass,$routefrom,$routeto);
                
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        if ($this->flexi_auth->get_user_id())
                        {
                            $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        }
                        else {
                            $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $routefrom);
                        }
                        $cur = $tcur->Currency;
                        $returnamount = $this->bus_model->getreturndiscount($cur);
                $amount = $details - $returnamount;
                echo $amount;

}
  function discgetseats($seatclassbustype)
        {
            $sb = explode(".", $seatclassbustype);
            $seatclass = $sb[0];
            $bustype = $sb[1];
            $html ="Seats: <input name=\"seat0\" type=\"checkbox\" value=\"0\" /> All <br>";
            $seats = $this->bus_model->getBusSeatsdics($seatclass,$bustype);
			//print_r( $seats);
            $seatstot = $this->bus_model->getBusSeatsdicstotrows($seatclass,$bustype);
            $cnt = 1;
			if($seats<>NULL):
            foreach ($seats as $s)
            {
            $html.="<input name=\"seat".$cnt."\" type=\"checkbox\" value=\"".$s->seat_no."\" /> ".$s->seat_no." | ";
            $cnt++;
            }
			echo $html;
			else: echo 'No seats for this bus type added yet! Contact Admin.';
			endif;
        }
         function discgettowns()
        {
            $html ="Branches: <input name=\"branch0\" type=\"checkbox\" value=\"0\" /> All <br>";
            
            $branches = $this->bus_model->getbranchesdics();
           // $seatstot = $this->bus_model->getBusSeatsdicstotrows($seatclass);
            $cnt = 1;
            foreach ($branches as $s)
            {
            $html.="<input name=\"branch".$cnt."\" type=\"checkbox\" value=\"".$s->TownId."\" /> ".$s->TownName." | ";
            $cnt++;
            }
			return $html;
        }
         function gettimeshtml()
        {
            $html ="";
            
            $times = $this->bus_model->gettimes();
           // $seatstot = $this->bus_model->getBusSeatsdicstotrows($seatclass);
            $cnt = 1;
            $j = 1;
            $br = "";
            
            $timedisc = $this->bus_model->gettimediscounts();
            foreach ($timedisc as $item):
            $timed = trim($item->time_values,',');
			 
                      endforeach;
            foreach ($times as $s)
            { 
                $tz = explode(",",$timed);
                if (in_array($s->TimeId, $tz)):
                    $checked = "checked";
                    else:
                        $checked = "";
                endif;
            $html.="<input name=\"timez[]".$cnt."\" type=\"checkbox\" value=\"".$s->TimeId."\" ".$checked." /> ".date('g:i A',strtotime($s->ArrivalDeparture))."  ".$br;
            $cnt++; 
            if ($j > 7): $j=0; $br = "<br />"; else: $br="";  endif;
           $j++;
            }
			return trim($html," | ");
        }
 function crud_discounts()
 { 
  if ($this->flexi_auth->is_logged_in()){
  $data['discount']=$this->bus_model->getnormaldiscounts();
  $data['discountd']=$this->bus_model->getdaydiscounts();
  $data['discountt']=$this->bus_model->gettimediscounts();
  $data['discountr']=$this->bus_model->getreturndiscounts();
  $data['timez']= $this->gettimeshtml();
  $data['branches'] = $this->discgettowns();
  $data['branchtot'] = $this->bus_model->getbranchtotal();
  $data['bustypes']=$this->bus_model->getBusTypes();
  $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
  $data['routes']=$this->bus_model->getBusRoutes();
  $this->load->view('manage_discounts',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
   function create_discounts() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  
  $this->form_validation->set_rules('Percent_or_Amount', 'Discount Type', 'required');
  $this->form_validation->set_rules('disc_name', 'Name', 'max_length[30]|required');   
  $this->form_validation->set_rules('disc_amountke', 'Discount Amount KE', 'required|numeric'); 
  $this->form_validation->set_rules('disc_amountug', 'Discount Amount UG', 'required|numeric'); 
  $this->form_validation->set_rules('disc_amounttz', 'Discount Amount TZ', 'required|numeric');   
  $this->form_validation->set_rules('Disc_Desc', 'Description', 'required');   
  $this->form_validation->set_rules('Disc_From_Date', 'From Date', 'required');   
  $this->form_validation->set_rules('Disc_To_Date', 'To Date', 'required');
  
  if($this->form_validation->run() == FALSE)
  {
   $data['discount']=$this->bus_model->getplainresults('discounts');
  $data['branches'] = $this->discgettowns();
  $data['bustypes']=$this->bus_model->getBusTypes();
   $data['branchtot'] = $this->bus_model->getbranchtotal();
  $data['seatclass'] = $this->bus_model->getplainresults('seats_lkp');
  $this->load->view('manage_discounts',$data);
  }
  else
  { 
     $totbranches = $_POST['branchtot'];
     $discvalues = "";
	 if (isset($_POST['branch0'])):
	  $discvalues .=$_POST['branch0'];
	 else:
     for($j=1; $j<=$totbranches; $j++)
      {
       if (isset($_POST['branch'.$j]))
         {
       $discvalues .=$_POST['branch'.$j].",";
         }
      }
        endif;                           
     $totseats = 47;
     $seatvalues = "";
	 if (isset($_POST['seat0'])):
	 $seatvalues .= $_POST['seat0'];
	 else:
     for($i=1; $i<=$totseats; $i++)
      {
       if (isset($_POST['seat'.$i]))
         {
       $seatvalues .=$_POST['seat'.$i].",";
         }
      }
      endif;              
   $insdata = array(
   'Disc_Name' => set_value('disc_name'),
                        'Percent_or_Amount' => $this->input->post('Percent_or_Amount'),
                        'Disc_Amountke' => set_value('disc_amountke'),
                        'Disc_Amountug' => set_value('disc_amountug'),
                        'Disc_Amounttz' => set_value('disc_amounttz'),
                        'Disc_Desc' => set_value('Disc_Desc'),
                        'Disc_From_Date' => set_value('Disc_From_Date'),
                        'Disc_To_Date' => set_value('Disc_To_Date'),
                        'User' => $this->flexi_auth->get_user_id(),
                        'disc_type_id' => 1,
                        'branch_values' => $discvalues,
                        'seat_values' => $seatvalues ,
                        'BusTypeId' => $this->input->post('bustypelkp')
  );
     // run insert model to write data to db
                        $tablename = 'discounts';
                        $pkname = 'Disc_Id';
                        $campuscode = "";
                        $pkvalue = $campuscode."_1";//do function to generate pkvalue
                        $iflastid = 0;
                      $this->bus_model->dbinsert($tablename, $insdata, $pkname, $pkvalue, $iflastid);
                      
  //redirect('admins/crud_discounts','refresh');
                        $data['url'] = 'admins/crud_discounts';
                        $this->load->view('data_success',$data);
  }
  }else {
                 redirect('auth','refresh');
            }
  
 }
   function update_day_discounts() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
      $count = sizeof($this->input->post('days', TRUE));
      $days = $this->input->post('days', TRUE);
      $dayvalues = "";
      for ($i=0;$i<$count;$i++) {
          $dayvalues .= $days[$i].",";
      }
  
   $updatedata = array(
                        'Percent_or_Amount' => $this->input->post('Percent_or_Amountd'),
                        'Disc_Amountke' => $this->input->post('disc_amountke'),
                        'Disc_Amountug' => $this->input->post('disc_amountug'),
                        'Disc_Amounttz' => $this->input->post('disc_amounttz'),
                        'day_values' => $dayvalues,
                        'If_Other_Discount_Active' => $this->input->post('discountactive'),
                        'If_Other_Season_Active' => $this->input->post('seasonactive'),
                        'RouteId' => $this->input->post('route'),
                        'BusTypeId' => $this->input->post('bustypelkp')
  );
     // run insert model to write data to db
                        $tablename = 'discounts';
                        $pkname = 'Disc_Id';
                        $pkvalue = $this->input->post('discid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      
                     
                        $data['url'] = 'admins/crud_discounts';
                        $this->load->view('data_success',$data);
  }
  else {
                 redirect('auth','refresh');
            }
  
 }
   function update_time_discounts() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
      $count = sizeof($this->input->post('timez', TRUE));
      $timez = $this->input->post('timez', TRUE);
      $timevalues = "";
      for ($i=0;$i<$count;$i++) {
          $timevalues .= $timez[$i].",";
      }
  
   $updatedata = array(
                        'Percent_or_Amount' => $this->input->post('Percent_or_Amountt'),
                        'Disc_Amountke' => $this->input->post('disc_amountke'),
                        'Disc_Amountug' => $this->input->post('disc_amountug'),
                        'Disc_Amounttz' => $this->input->post('disc_amounttz'),
                        'time_values' => $timevalues,
                        'If_Other_Discount_Active' => $this->input->post('discountactive'),
                        'If_Other_Season_Active' => $this->input->post('seasonactive'),
                        'RouteId' => $this->input->post('route'),
                        'BusTypeId' => $this->input->post('bustypelkp')
  );
     // run insert model to write data to db
                        $tablename = 'discounts';
                        $pkname = 'Disc_Id';
                        $pkvalue = $this->input->post('discid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      
                     
                        $data['url'] = 'admins/crud_discounts';
                        $this->load->view('data_success',$data);
  }
  else {
                 redirect('auth','refresh');
            }
  
 }
   function update_return_discounts() 
 { 
  if ($this->flexi_auth->is_logged_in())
            { 
  
   $updatedata = array(
                        'Percent_or_Amount' => $this->input->post('Percent_or_Amountt'),
                        'Disc_Amountke' => $this->input->post('disc_amountke'),
                        'Disc_Amountug' => $this->input->post('disc_amountug'),
                        'Disc_Amounttz' => $this->input->post('disc_amounttz'),
                        'activate' => $this->input->post('discountactivate'),
                        'If_Other_Discount_Active' => $this->input->post('discountactive'),
                        'If_Other_Season_Active' => $this->input->post('seasonactive')
  );
     // run insert model to write data to db
                        $tablename = 'discounts';
                        $pkname = 'Disc_Id';
                        $pkvalue = $this->input->post('discid');//do function to generate pkvalue
                      $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
                      
                     
                        $data['url'] = 'admins/crud_discounts';
                        $this->load->view('data_success',$data);
  }
  else {
                 redirect('auth','refresh');
            }
  
 }
 
 function displaytermini($townid)
 {
     
     $termini = $this->bus_model->getresultfromonetbl('bus_termini','TownLkp',$townid);
     $html = "";
     $html .="<label for=\"t_termini\">Termini:</label><br> <select name=\"t_termini\">";
                 if ($termini<>NULL){
                 foreach($termini as $t){
                     $html .=" <option value=\"".$t->TerminusId."\">".$t->TerminusName."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No termini set for this station</option>";
                 }
               $html .="</select><br />";
               
               echo $html;
 }
 
 function displayterminiclerks($townid)
 {
     //$clerks = $this->bus_model->getresultfromonetbl('user_accounts','t_station',$townid);
     $sql_select = array('uacc_id','upro_first_name','upro_last_name','t_station');
       
	    $sql_where = 'uacc_group_fk = "1" and t_station = '.$townid;
            //$sql_where = 'uacc_group_fk = "1"';
		
		$clerks = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                //$this->flexi_auth->get_users($sql_select, $sql_where)->result();
     $termini = $this->bus_model->getresultfromonetbl('bus_termini','TownLkp',$townid);
     $html = "";
     $html .="Termini <br /> <select name=\"terminus\">
                <option selected=\"selected\" value=\"0\">All</option>";
                 if ($termini<>NULL){
                 foreach($termini as $t){
                     $html .=" <option value=\"".$t->TerminusId."\">".$t->TerminusName."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No termini set for this station</option>";
                 }
               $html .="</select><br />";
               
               $html .="Clerk <br /> <select name=\"clerk\">
                <option selected=\"selected\" value=\"0\">All</option>";
                 if ($termini<>NULL){
                 foreach($clerks as $c){
                     $html .=" <option value=\"".$c->uacc_id."\">".$c->upro_first_name." ".$c->upro_last_name."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No clerks added to this station</option>";
                 }
               $html .="</select><br />";
               
               echo $html;
 }
 
 function displayterminiclerkspettycash($townid)
 {
     //$clerks = $this->bus_model->getresultfromonetbl('user_accounts','t_station',$townid);
     $sql_select = array('uacc_id','upro_first_name','upro_last_name','t_station');
       
	    $sql_where = 'uacc_group_fk = "1" and t_station = '.$townid;
            //$sql_where = 'uacc_group_fk = "1"';
		
		$clerks = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
                
                 $sql_select2 = array('uacc_id','upro_first_name','upro_last_name','t_station');
       
	    $sql_where2 = 'uacc_group_fk = "2" and t_station = '.$townid;
            $branchmanagers = $this->flexi_auth->get_users($sql_select2,$sql_where2)->result();

                //$this->flexi_auth->get_users($sql_select, $sql_where)->result();
     $termini = $this->bus_model->getresultfromonetbl('bus_termini','TownLkp',$townid);
     $html = "";
     $html .="Termini <br /> <select name=\"pickuppoint\">";
                 if ($termini<>NULL){
                 foreach($termini as $t){
                     $html .=" <option value=\"".$t->TerminusId."\">".$t->TerminusName."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No termini set for this station</option>";
                 }
               $html .="</select><br />";
               
               $html .="Clerk <br /> <select name=\"clerk\">";
                 if ($termini<>NULL){
                 foreach($clerks as $c){
                     $html .=" <option value=\"".$c->uacc_id."\">".$c->upro_first_name." ".$c->upro_last_name."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No clerks added to this station</option>";
                 }
               $html .="</select><br />";
               
               $html .="Authorized By <br /> <select name=\"authorizedby\">";
                 if ($termini <> NULL){
                 foreach($branchmanagers as $b){
                     $html .="<option value=\"".$b->uacc_id."\">".$b->upro_first_name." ".$b->upro_last_name."</option>";
                 }
                 }
                 else {
                     $html .=" <option value=\"0\">No branch managers added to this station</option>";
                 }
               $html .="</select><br />";
               
               echo $html;
 }
 
 function displaygraphtypes()
 {
     
     $typeid = $this->input->post('typeid');
     $html = "";
     if ($typeid==1)
     {
         $ptitle = "Per Hour Graph";
         $html .= "Select Date <br /><input type=\"text\" name=\"datef\"  id=\"Disc_From_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datef')."\"/>";
     }
     if ($typeid==2 or $typeid==3)
     {
         if ($typeid==2)
         {
             $ptitle = "Per Day Graph";
         }
         if ($typeid==3)
         {
             $ptitle = "Per Week Graph";
         }
         $html .= "Select Date From <br /><input type=\"text\" name=\"datef\"  id=\"Disc_From_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datef')."\"/><br />Select Date To <br /><input type=\"text\" name=\"datet\"  id=\"Disc_To_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datet')."\"/>";
     }
     if ($typeid==4)
     {
         $ptitle = "Per Month Graph";
         $html .="Select Year<br /><select name=\"yeart\">";
         $endy = date('Y') + 2;
         for ($j=2012;$j<=$endy;$j++)
         {
             if ($j==date('Y')){
                 $sely = "selected";
             }
             else {
                 $sely = "";
             }
             $html .= "<option ".$sely." value=\"".$j."\">".$j."</option>"; 
         }
         $html .="</select>";
         
     }

               
               $data['html'] = $html;
               $data['ptitle'] = $ptitle;
               $data['typeid'] = $typeid;
               
               
                if ($this->flexi_auth->is_logged_in()){
                $data['rep'] = 1;
                
                $xml = "<graph caption='Monthly Sales Summary' subcaption='For the year 2004' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='$' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
   <set name='Jan' value='17400' hoverText='January'/>
   <set name='Feb' value='19800' hoverText='February'/>
   <set name='Mar' value='21800' hoverText='March'/>
   <set name='Apr' value='23800' hoverText='April'/>
   <set name='May' value='29600' hoverText='May'/>
   <set name='Jun' value='27600' hoverText='June'/>
   <set name='Jul' value='31800' hoverText='July'/>
   <set name='Aug' value='39700' hoverText='August'/>
   <set name='Sep' value='37800' hoverText='September'/>
   <set name='Oct' value='21900' hoverText='October'/>
   <set name='Nov' value='32900' hoverText='November' />
   <set name='Dec' value='39800' hoverText='December' />
</graph>";
                $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
                
                $data['graphdata'] = 0;
               
  
  $this->load->view('manage_graphical_reports',$data);
  }else {
                 redirect('auth','refresh');
            }
                
                
     
 }
 
 function gen_report()
 {
     $townf = $_POST['townf'];
     $townt = $_POST['townt'];
     $channel = $_POST['channel'];
     $terminus = $_POST['terminus'];
     $clerk = $_POST['clerk'];
     $datef = $_POST['datef'];
     $datet = $_POST['datet'];
     $bustypelkp = $_POST['bustypelkp'];
     $tickettype = $_POST['tickettype'];
     $paymode = $_POST['paymode'];
     $outputformat = $_POST['outputype'];
     if (isset($_POST['tickdet']))
     {
         $tickdet = $_POST['tickdet'];
     }
     else {
         $tickdet = 1;
     }
     
     $data['tickets'] = $this->bus_model->gen_report($townf,$townt,$channel,$terminus,$clerk,$bustypelkp,$tickettype,$paymode,$datef,$datet);
     //$data['kesamount'] = $this->bus_model->gen_report_sum_kes($townf,$townt,$channel,$terminus,$clerk,$bustypelkp,$paymode,$datef,$datet);
      $data['termini'] = $this->bus_model->getplainresults('bus_termini');
                $data['towns'] = $this->bus_model->getplainresults('towns_lkp');
                $data['bustypes'] = $this->bus_model->getplainresults('bustype_lkp');
                $data['channels'] = $this->bus_model->getplainresults('channels');
                $data['paymodes'] = $this->bus_model->getplainresults('paymodes');
                $data['rep'] = 1;
                $data['tickdet'] = $tickdet;
                $sql_select = array('uacc_id','upro_first_name','upro_last_name','uacc_group_fk', 'ugrp_name');
       
	    $sql_where = 'uacc_group_fk = "1"';
		
		$data['clerks'] = $this->flexi_auth->get_users($sql_select,$sql_where)->result();

                $this->flexi_auth->get_users($sql_select, $sql_where)->result();
                
                $title = $tickettype." Ticket Sales ";
                    if ($tickdet==2):
                        $title .=" without ticket details ";
                    endif;
                if (isset($townf) and $townf<>0):
                    $town = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId',$townf);
                $title .="from ".$town->TownName." ";
                    endif;
                    if (isset($townt) and $townt<>0):
                    $townt = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId',$townt);
                $title .="to ".$townt->TownName." ";
                    endif;
                    if (isset($terminus) and $terminus<>0):
                        $termi = $this->bus_model->getonerowfromonetbl('bus_termini','TerminusId',$terminus);
                $title .="at ".$termi->TerminusName." ";
                    endif;
                    if (isset($channel) and $channel<>0):
                        $ch = $this->bus_model->getonerowfromonetbl('channels','channels_id',$channel);
                        $title .="through ".$ch->channel." channel ";
                    endif;
                    if (isset($bustypelkp) and $bustypelkp<>0):
                        $bust = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId',$bustypelkp);
                        $title .="for ".$bust->BusType." ";
                    endif;
                    if (isset($paymode) and $paymode<>0):
                        $py = $this->bus_model->getonerowfromonetbl('paymodes','paymodes_id',$paymode);
                        $title .="paid by ".$py->paymodes." ";
                    endif;
                    if (isset($datef) and $datef<>0):
                        $title .="from ".$datef." ";
                    endif;
                    if (isset($datet) and $datet<>0):
                        $title .="to ".$datet." ";
                    endif;
                      if ((!isset($datef) and !isset($datet)) || ($datef=="" and $datet=="")):
                         $title .="on ".date('Y-m-d')." ";
                     endif;
                    if (isset($clerk) and $clerk<>0):
                        $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                //$userid = $this->user[0]->uacc_id;
                        $title .="booked by ".$username;
                    endif;
                    $data['reporttitle'] = $title;
  if($outputformat=="desktop"):
  $this->load->view('manage_reports',$data);
  endif;
  if($outputformat=="printr"):
  $this->load->view('manage_reports_print',$data);
  endif;
  if($outputformat=="excel"):
  $this->load->view('manage_reports_excel',$data);
  endif;
 }
 
 function clerk_report()
 {
         //$tickdet = 1;
         //echo $this->flexi_auth->get_user_id(); exit;
     if ($this->flexi_auth->is_logged_in()){
     $data['tickets'] = $this->bus_model->clerk_report();
                $data['rep'] = 1;
                $data['tickdet'] = 1;
                
                $title = " Ticket Sales ";
                    
                    $data['reporttitle'] = $title;
            $this->load->view('clerk_report',$data);
            }
  else {
                 redirect('auth','refresh');
            }
 }
 
 function gen_graphical_report()
 {
     if ($this->flexi_auth->is_logged_in()){
     $data['graphdata'] = 1;
     $typeid = $this->input->post('typeid1');
     $html = "";
     $xml = "";
     if ($typeid==1)
     {
         $ptitle = "Per Hour Graph";
         $date = $this->input->post('datef');
         $datedd = date("F d,Y",strtotime($date));
         $html .= "Select Date <br /><input type=\"text\" name=\"datef\"  id=\"Disc_From_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datef')."\"/>";
         $xml .= "<graph caption='Hourly Sales Summary' subcaption='For the dates $datedd' xAxisName='Hour' yAxisMinValue='0' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
                        <set name='7am' value='17400' hoverText='Sales'/>
                        <set name='8am' value='19800' hoverText='Sales'/>
                        <set name='9am' value='21800' hoverText='Sales'/>
                        <set name='10am' value='23800' hoverText='Sales'/>
                        <set name='11am' value='29600' hoverText='Sales'/>
                        <set name='12pm' value='27600' hoverText='Sales'/>
                        <set name='1pm' value='31800' hoverText='Sales'/>
                        <set name='2pm' value='39700' hoverText='Sales'/>
                        <set name='3pm' value='37800' hoverText='Sales'/>
                        <set name='4pm' value='21900' hoverText='Sales'/>
                        <set name='5pm' value='32900' hoverText='Sales' />
                        <set name='6pm' value='39800' hoverText='Sales' />
                        <set name='7pm' value='31800' hoverText='Sales'/>
                        <set name='8pm' value='39700' hoverText='Sales'/>
                        <set name='9pm' value='37800' hoverText='Sales'/>
                        <set name='10pm' value='21900' hoverText='Sales'/>
                        <set name='11pm' value='32900' hoverText='Sales' />
                     </graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
     }
     if ($typeid==2 or $typeid==3)
     {
         if ($typeid==2)
         {
             $ptitle = "Per Day Graph";
         }
         if ($typeid==3)
         {
             $ptitle = "Per Week Graph";
         }
         $datef = $this->input->post('datef');
         $datet = $this->input->post('datet');
         if ($datef==$datet)
         {
         $datedd = date("F d,Y",strtotime($datef));
         }
         else {
             $datedd = date("F d,Y",strtotime($datef))." to ".date("F d,Y",strtotime($datet));
         }
         $html .= "Select Date From <br /><input type=\"text\" name=\"datef\"  id=\"Disc_From_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datef')."\"/><br />Select Date To <br /><input type=\"text\" name=\"datet\"  id=\"Disc_To_Date\"  readonly=\"readonly\" class=\"long_input\" value=\"".$this->input->post('datet')."\"/>";
          $xml .= "<graph caption='Daily Sales Summary' subcaption='For the date $datedd' xAxisName='Days' yAxisMinValue='0' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
                        <set name='April 01' value='17400' hoverText='Sales' color='AFD8F8'/>
                        <set name='April 02' value='19800' hoverText='Sales' color='F6BD0F'/>
                        <set name='April 03' value='21800' hoverText='Sales' color='8BBA00'/>
                        <set name='April 04' value='23800' hoverText='Sales' color='FF8E46'/>
                        <set name='April 05' value='29600' hoverText='Sales' color='008E8E'/>
                        <set name='April 06' value='27600' hoverText='Sales' color='D64646'/>
                        <set name='April 07' value='31800' hoverText='Sales' color='8E468E'/>
                        <set name='April 08' value='39700' hoverText='Sales' color='588526'/>
                        <set name='April 09' value='37800' hoverText='Sales' color='B3AA00'/>
                        <set name='April 10' value='21900' hoverText='Sales' color='008ED6'/>
                        <set name='April 11' value='32900' hoverText='Sales' color='9D080D'/>
                        <set name='April 12' value='39800' hoverText='Sales' color='A186BE'/>
                     </graph>";
         $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
     }
     if ($typeid==4)
     {
         $ptitle = "Per Month Graph";
         $html .="Select Year<br /><select name=\"yeart\">";
         $endy = date('Y') + 2;
         for ($j=2012;$j<=$endy;$j++)
         {
             if ($j==date('Y')){
                 $sely = "selected";
             }
             else {
                 $sely = "";
             }
             $html .= "<option ".$sely." value=\"".$j."\">".$j."</option>"; 
         }
         $html .="</select>";
               $year = $this->input->post('yeart');
          $xml .= "<graph caption='Monthly Sales Summary' subcaption='For the year $year' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='Ksh ' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >
                        <set name='Jan' value='17400' hoverText='January'/>
                        <set name='Feb' value='19800' hoverText='February'/>
                        <set name='Mar' value='21800' hoverText='March'/>
                        <set name='Apr' value='23800' hoverText='April'/>
                        <set name='May' value='29600' hoverText='May'/>
                        <set name='Jun' value='27600' hoverText='June'/>
                        <set name='Jul' value='31800' hoverText='July'/>
                        <set name='Aug' value='39700' hoverText='August'/>
                        <set name='Sep' value='37800' hoverText='September'/>
                        <set name='Oct' value='21900' hoverText='October'/>
                        <set name='Nov' value='32900' hoverText='November' />
                        <set name='Dec' value='39800' hoverText='December' />
                     </graph>";
    $myFile = "testFile.xml";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $xml;
                fwrite($fh, $stringData);
                fclose($fh);
         
     }

               
               $data['html'] = $html;
               $data['ptitle'] = $ptitle;
               
               $data['typeid'] = $typeid;
                       
               
                
                    
                   
                    
		       
                $data['rep'] = 1;
                $this->load->view('manage_graphical_reports',$data);
  }
  else {
                 redirect('auth','refresh');
            }
                
                
     
 }
 
 function displaydriverdetails($did)
 {
     $html = "";
     if ($did==1)
     {
         $html .= "<script type=\"text/javascript\">  
$(function() {  
    $('#Disc_From_Date5').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     }); });</script>";
         $html .= "<table class=\"table1\"><tr><td>Licence No:</td><td><input type=\"text\" name=\"licenceno\" id=\"long_input\" value=\"".$this->input->post('licenceno')."\"></td></tr>";
         $html .= "<tr><td width=\"167\">Expiry Date:</td><td width=\"521\"><input type=\"text\" name=\"expirydate\" id=\"Disc_From_Date5\" class=\"long_input\" ></td></tr>";
         $html .= "<tr><td>Copy of Licence:</td><td><input type=\"file\" name=\"licencecopy\" id=\"long_input\"></td></tr></table>";
     }
     echo $html;
 }
 
 function displayroutetowns($rid)
 {
     $html = "";
     if ($rid!=0):
     
         $rtowns = $this->bus_model->getwojoinedtbresultswithwhereclause('bus_routes', 'route_arrival_departure', 'RouteId', 'RouteId', $rid);
         if ($rtowns<>NULL):
             foreach ($rtowns as $rt):
                $times = $this->bus_model->getonerowfromonetbl('reporting_departure_times_lkp', 'TimeId', $rt->DepartureTime);
                $town = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $rt->TerminusId);
                $html .= $town->TownName." (".date('g:i A',strtotime($times->ArrivalDeparture))." ) ---> ";
             endforeach;
             
         endif;
         
 endif;
 $htmlr = trim($html," ---> ");
        
     echo $htmlr;
	
}
 
 function displaytownroutes($townid)
 {
     $html = "";
     if ($townid!=0):
     
         $routes = $this->bus_model->get_town_routes($townid);
         if ($routes<>NULL):
             $html .="<select name=\"route\" id=\"route\" onChange=\"displayroutetowns(this.value);\" >";
         $html .="<option disabled=\"disabled\" selected=\"selected\">---select route--</option>";
             foreach ($routes as $rt):
                //$towns = trim($rt->towns,',');
             //$rtowns = explode(",",$towns);
             //$tottowns = count($rtowns);
                 
                  $html .="<option value=\"".$rt->RouteId."\">".$rt->RouteCode."</option>";
             endforeach;
             $html .="</select>";
             else:
                $html .="No Bus Route with the above town!"; 
         endif;
         
 endif;
 //$htmlr = trim($html," ---> ");
        
     echo $html;
	
}
 
 function displayvouchermount($ticketvoucherno)
 {
     $tv = explode(":",$ticketvoucherno);
     $ticketno = $tv[0];
     $voucherno = $tv[1];
     $html = "";
         $vn = $this->bus_model->get_voucher_ticket($ticketno,$voucherno);
         if ($vn<>NULL):
             $ndate = $vn->open_date;
     $newdate = date('Y-m-d',strtotime($ndate.' + 30 day'));
     $today = date('Y-m-d');
     if ($today<=$newdate):
             $html .= "Voucher amount is <strong>".round($vn->amount)."</strong><input type=\"hidden\" name=\"vouchertickid\" value=\"".$vn->TicketId."\" /><input type=\"hidden\" name=\"voucheramnt\" value=\"".round($vn->amount)."\" /> | Add some more<input type=\"text\" name=\"addedvoucheramnt\" value=\"0\" />";
     
     else:
         $html .= "This voucher code has expired!<input type=\"hidden\" name=\"vouchertickid\" value=\"0\" /><input type=\"hidden\" name=\"voucheramnt\" value=\"0\" /><input type=\"hidden\" name=\"addedvoucheramnt\" value=\"0\" />";
     endif;
         else:
            $html .= "Voucher number not valid!<input type=\"hidden\" name=\"vouchertickid\" value=\"0\" /><input type=\"hidden\" name=\"voucheramnt\" value=\"0\" /><input type=\"hidden\" name=\"addedvoucheramnt\" value=\"0\" />"; 
         
 endif;
        
     echo $html;
	
}

function displaypaymodes($paymode)
{
    $html = "";
    if ($paymode==1)
    {
        $html .= "Amount <input type=\"text\" name=\"depamount\" id=\"long_input\" />";
    }
    if ($paymode==2)
    {
        $html .= "Ref No <input type=\"text\" name=\"chequerefno\" id=\"long_input\" /><br />Amount <input type=\"text\" name=\"cheqamount\" id=\"long_input\" />";
    }
    if ($paymode==3)
    {
        $html .= "<p>Please pay the amount you wish to deposit via mpesa paybill. Instructions below:<br />
          <ul>
          <li>Go to your phone and select mpesa/ or SIM toolkit in some phones</li>
          <li>Under mpesa, select \"Payment Services\"</li>
          <li>Select \"Pay Bill\"</li>
          <li>Enter Business Number as <strong>525600</strong></li>
          <li>Leave account number blank</li>
          <li>Enter PIN and complete</li>
          <li>You will find the transaction number in the message sent to you by mpesa, enter it below and click Deposit</li>
          </ul>
          </p>
          Mpesa Transaction Number: <input type=\"text\" name=\"mpesatransno\" id=\"long_input\" />";
    }
    
    echo $html;
}

function print_ticket_print($tids)
{
                 $data['tids'] = $tids;
     	 $this->load->view('print_ticket_print',$data);
}
function numbertest($number)
{
    echo $this->bus_model->convert_number_to_words($number);
}

function tryfor()
{
    $townsa = explode(",","1,6,2,3,4");
    for ($i=0;$i<count($townsa);$i++)
    {
        if ($i+1!=count($townsa))
        {
        echo $townsa[$i]." - ".$townsa[$i+1]."<br />";
        }
    }
}
	
	function _sendsms_schedules($message, $rphoneno)
	{
            //insert sms in db
            $content = $message;
            $smsfunction = "Schedule Customer Info";
            $smslength = strlen($content);
            if ($smslength<=160)
            {
                $noofsms = 1;
            }
            else {
                    $noofsms = ceil($smslength/160);
            }
	// Infobip's POST URL
		$postUrl = "http://api2.infobip.com/api/sendsms/xml";
		// XML-formatted data
		$xmlString =
		"<SMS>
		<authentification>
		<username>Modernltd</username>
		<password>moderncoast</password>
		</authentification>
		<message>
		<sender>ModernCoast</sender>
		<text>$content</text>
		</message>
		<recipients>
		<gsm>$rphoneno</gsm>
		</recipients>
		</SMS>";
		// previously formatted XML data becomes value of "XML" POST variable
		$fields = "XML=" . urlencode($xmlString);
		// in this example, POST request was made using PHP's CURL
                $senditems1 = "http://developer.mtl.co.ke:8080/smsAPI/smsout?username=modern&password=modernmarcel&msisdn=$rphoneno&sender=ModernCoast&text=$content";
                $senditems = str_replace(" ", "%20", $senditems1);
/*
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $senditems);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);
		// write out the response
		//return $response;
                 */
                 
                //$senditems = "http://developer.mtl.co.ke:8080/smsAPI/smsout?username=modern&password=modernmarcel&msisdn=$rphoneno&sender=Modern&text=$content";
                $sent = file_get_contents($senditems);
                //echo $senditems;
                //echo "<br />".$rphoneno;
            //$this->main_model->insert_sms($smslength,$noofsms,$rphoneno,$smsfunction);
	}
        
        function trymatch()
        {
            $t = $this->bus_model->getlastinsertedticketfromtown("MO","NORMAL");
            echo $t->newticketno;
        }
        
        function tryday()
        {
            
            //echo date('l', strtotime(date('Y-m-d').' + 1 day'));
            $check_date = strtotime(date('Y-m-d',strtotime($this->input->post('departuredatetime')))); // '2010-02-27';
                    $end_date = strtotime(date('Y-m-d',strtotime($this->input->post('departuredatetime2')))); // '2010-03-24';
                    $bs = $this->bus_model->getonerowfromonetbl('bus_routes','RouteId',$this->input->post('route'));
                    //var_dump($_POST['skipdays']);
                    //while (strtotime($date) <= strtotime($end_date))
                    while($check_date <= $end_date){
                        echo date('l', $check_date)."<br />";
                        $check_date = strtotime(date ("Y-m-d", strtotime("+1 day", $check_date)));
                    }
        }

}


