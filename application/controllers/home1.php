<div id="container">
<!--topbar area-->
<div id="topbar">
<img src="<?=base_url()?>images/logo.png" align="left" />

<div id="loggerdetails">
<?php include("application/views/includes/loggerinfo.php"); ?>
</div>

</div>
<!--end topbar area start container-->

 <!--menu area -->
<div id="menucontainer">

    <?php
    include 'application/views/includes/menu.php';
    ?>

</div>
<!--end of menu area -->
<div id="containerseparator">

</div>

<div id="contentscontainer">

    <div class="column" id="column1">
		<div class="dragbox" id="item1" >
			<h2>Invoice Reminder</h2>
			<div class="dragbox-content" >
                <?php
				if ($invoices==NULL)
				{
					echo "No invoice generated recently";	
				}
				else {
				foreach ($invoices as $invoice)
					{
						echo "Invoice No: <strong>".$invoice->Inv_No."</strong> sent to <strong>".$invoice->Cust_Company."</strong> on <strong>".$invoice->Inv_Date."</strong><br />";
					}
				}
				?>
			</div>
		</div>
		<div class="dragbox" id="item2" >
			<h2><span class="configure" ><a href="#" >Configure</a></span>Orders</h2>
			<div class="dragbox-content" >
            		 <?php
				if ($orders==NULL)
				{
					echo "No pending orders.";	
				}
				else {
				foreach ($orders as $order)
					{
						if ($order->Cust_Company=="")
						{
						$ordercreator = $order->Cust_First_Name." ".$order->Cust_Last_Name;
						}
						else {
							$ordercreator = $order->Cust_Company;
						}
						echo anchor('packages/orders/index/'.$order->Order_id,'Order No:')."<strong>".$order->Order_id."</strong> created by <strong>".$ordercreator."</strong> on <strong>".$order->Order_Date."</strong>. Contacts: ".$order->Cust_Mobile_No.nbs(2).$order->Order_Address."<br /><hr />";
					}
				}
				?>
				
			</div>
		</div>
	</div>
	<div class="column" id="column2" >
		<div class="dragbox" id="item4" >
			<h2>Messages</h2>
			<div class="dragbox-content" >
				<?php
				if ($messages==NULL)
				{
					echo "No new messages";	
				}
				else {
				foreach ($messages as $message)
					{
						echo anchor('messages/manage_messages/read/'.$message->m_id,$message->m_header)."<br />";
					}
				}
				?>
                
			</div>
		</div>
		<div class="dragbox" id="item5" >
			<h2>Notifications</h2>
			<div class="dragbox-content" >
				No new notifications
			</div>
		</div>
	</div>

</div>

</div>