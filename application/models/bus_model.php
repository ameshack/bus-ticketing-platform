<?php class bus_model extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->helper('date');
    }
	
	function _generateVoucherCode()
	  {
	    $length = 8;
		$vouchercode = "";
	
		// define possible characters - any character in this string can be
		// picked for use in the password, so if you want to put vowels back in
		// or add special characters such as exclamation marks, this is where
		// you should do it
		$possible = "1234567890";
	
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
	  
		// check for length overflow and truncate if necessary
		if ($length > $maxlength) {
		  $length = $maxlength;
		}
		
		// set up a counter for how many characters are in the password so far
		$i = 0; 
		
		// add random characters to $password until $length is reached
		while ($i < $length) { 
	
		  // pick a random character from the possible ones
		  $char = substr($possible, mt_rand(0, $maxlength-1), 1);
			
		  // have we already used this character in $password?
		  if (!strstr($vouchercode, $char)) { 
			// no, so it's OK to add it onto the end of whatever we've already got...
			$vouchercode .= $char;
			// ... and increase the counter by one
			$i++;
		  }
	
		}
	
		// done!
		return $vouchercode;
	
	  }
	
	function _generatePettyCashNo()
	  {
	    $length = 9;
		$pettycode = "";
	
		// define possible characters - any character in this string can be
		// picked for use in the password, so if you want to put vowels back in
		// or add special characters such as exclamation marks, this is where
		// you should do it
		$possible = "1234567890";
	
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
	  
		// check for length overflow and truncate if necessary
		if ($length > $maxlength) {
		  $length = $maxlength;
		}
		
		// set up a counter for how many characters are in the password so far
		$i = 0; 
		
		// add random characters to $password until $length is reached
		while ($i < $length) { 
	
		  // pick a random character from the possible ones
		  $char = substr($possible, mt_rand(0, $maxlength-1), 1);
			
		  // have we already used this character in $password?
		  if (!strstr($pettycode, $char)) { 
			// no, so it's OK to add it onto the end of whatever we've already got...
			$pettycode .= $char;
			// ... and increase the counter by one
			$i++;
		  }
	
		}
	
		// done!
		return $pettycode;
	
	  }

          function dbinsert($tablename,$insdata,$pkname,$pkvalue,$iflastid)
          {
              //insert into the table 
              $this->db->insert($tablename, $insdata);///if
              
                    $query = $this->db->query('SELECT LAST_INSERT_ID() AS '.$pkname.' From '.$tablename.' ORDER BY '.$pkname.' ');
                if ($query->num_rows() > 0)
		$lastid = $query->last_row();
		

              //insert into the transaction log 
            // $lastid = $query->last_row();
              $transdata = array('transaction_log_name'=>'insert',
                  'table_name'=>$tablename,
                  'pkName'=>$pkname,
                  'pkValue_id'=>$lastid->$pkname,
                  'user_id'=>  $this->flexi_auth->get_user_id(),//$this->session->userdata('userid'),
                  'description'=>'insert to '.$tablename.' table');
		
              $this->db->insert('transaction_log', $transdata);
			                  if ($iflastid==1)
              {
                    return $lastid;
              }
              
          }
          
          function dbupdate($tablename,$updatedata,$pkname,$pkvalue)
          {
              //update the table 
              $this->db->where($pkname,$pkvalue);
              $this->db->update($tablename, $updatedata);
              //insert into the transaction log 
              $transdata = array('transaction_log_name'=>'update',
                  'table_name'=>$tablename,
                  'pkName'=>$pkname,
                  'pkValue_id'=>$pkvalue,
                  'user_id'=>  $this->flexi_auth->get_user_id(),//$this->session->userdata('user_id'),
                  'description'=>'update row described in '.$tablename.' table');
		
              $this->db->insert('transaction_log', $transdata);
              
          }
		  
          //general
          function getplainresults($tablename)
          {
              
                $query = $this->db->select('*')->from($tablename)->where('status !=',0)->get();
                if ($query->num_rows() > 0)
                {return $query->result();}
                else 
                {return NULL;}
          }        
          //general
          function getplainresultstotrows($tablename)
          {
              
                $query = $this->db->select('*')->from($tablename)->where('status !=',0)->get();
                return $query->num_rows();
          }          
          //general
          function getonerowfromonetbl($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->where('status !=',0)->get();
                if ($query->num_rows() == 1)
                {return $query->row();}
                else 
                {return NULL;}
          }                   
          //general
          function getonerowfromonetblgreaterthanzero($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->where('status !=',0)->get();
                if ($query->num_rows() > 0)
                {return $query->row();}
                else 
                {return NULL;}
          }             
          //general
          function getonerowfromonetbl_status($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->get();
                if ($query->num_rows() == 1)
                {return $query->row();}
                else 
                {return NULL;}
          }        
          function getonerowfromonetbl_array($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->where('status !=',0)->get();
                if ($query->num_rows() == 1)
                {return $query->row_array();}
                else 
                {return NULL;}
          }        
          //general
          function getresultfromonetbl($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->where('status !=',0)->get();
                if ($query->num_rows() > 0)
                {return $query->result();}
                else 
                {return NULL;}
          }
           //general
          function gettotrowsresultfromonetbl($tablename,$cmpfield,$cmpfieldval)
          {
              
                $query = $this->db->select('*')->from($tablename)->where($cmpfield,$cmpfieldval)->where('status !=',0)->get();
                
                return $query->num_rows();
          }
		  //general
        function getfieldvaluefromrow($table,$columnname,$uniquefield,$fieldvalue){
   $this->db->select(''.$columnname.'');
   $this->db->where(''.$uniquefield.'',$fieldvalue); 
   $query = $this->db->get(''.$table.''); 
   if ($query->num_rows() > 0){
   $row = $query->row_array(); 
   return $row[''.$columnname.'']; 
   }
} 
		  //general
        function getfieldvaluefromrowtwofields($table,$columnname,$uniquefield,$fieldvalue,$uniquefield1,$fieldvalue1){
   $this->db->select(''.$columnname.'');
   $this->db->where(''.$uniquefield.'',$fieldvalue); 
   $this->db->where(''.$uniquefield1.'',$fieldvalue1); 
   $query = $this->db->get(''.$table.''); 
   if ($query->num_rows() > 0){
   $row = $query->row_array(); 
   return $row[''.$columnname.'']; 
   }
} 
         
          //get result from two tables joined by a similar primary key
          function getwojoinedtbresults($table1,$table2,$joinerfield)
          {
              
                $query = $this->db->select('*')->from($table1)->join($table2, $table1.'.'.$joinerfield.' = '.$table2.'.'.$joinerfield)->where($table1.'.status !=',0)->get();
                if ($query->num_rows() > 0)
                {return $query->result();}
                else 
                {return NULL;}
          }
          
          //get result from two tables joined by a similar primary key with a field for where clause
          function getwojoinedtbresultswithwhereclause($table1,$table2,$joinerfield,$wherefield,$wherefieldvalue)
          {
              
                $query = $this->db->select('*')->from($table1)->join($table2, $table1.'.'.$joinerfield.' = '.$table2.'.'.$joinerfield)->where($table1.'.status !=',0)->where($table2.'.status !=',0)->where($table1.".".$wherefield,$wherefieldvalue)->get();
                if ($query->num_rows() > 0)
                {return $query->result();}
                else 
                {return NULL;}
          }

	function createBus($adminid,$date)
	{
			$new_bus_insert_data = array(
			'BusTypeLkp' => $this->input->post('bustypelkp'),
			'RegistrationNumber' => $this->input->post('regno'),
			'MakeLkp' => $this->input->post('makelkp'),
			'DateBought' => $this->input->post('datebought'),
			'BusName' =>  $this->input->post('busname'),
			'BusNumber' =>  $this->input->post('busnumber'),
			'IsActive' =>  $this->input->post('isactive'),
			'DateAdded' => $date,
			'AddedBy' => $adminid							
		);
		$insert = $this->db->insert('buses', $new_bus_insert_data);
		$insert = $this->db->insert_id();
		return $insert;
	}
	function getThisBus($id){
  $data = array();
  $this->db->where('BusId',$id);
  $this->db->limit('1');
  $query = $this->db->get('buses');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
     return $data;

  }
  	function getThisRoute($id){
  $data = array();
  $this->db->where('RouteId',$id);
  $this->db->limit('1');
  $query = $this->db->get('bus_routes');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
  return $data;

  }
    function getThisMaintenance($id){
  $data = array();
  $this->db->where('MaintenanceId',$id);
  $this->db->limit('1');
  $query = $this->db->get('bus_maintenance');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
  return $data;
  }
    	function getThisTerminus($id){
  $data = array();
  $this->db->where('TerminusId',$id);
  $this->db->limit('1');
  $query = $this->db->get('bus_termini');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
  return $data;
  }
      	function getThisRoutePrice($id){
  $data = array();
  $this->db->where('PriceId',$id);
  $this->db->limit('1');
  $query = $this->db->get('route_prices');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
  return $data;
  }
        function getThisBusSchedule($id){
  $data = array();
  $this->db->where('ScheduleId',$id);
  $this->db->limit('1');
  $query = $this->db->get('bus_schedule');
  if($query->num_rows > 0){
  $data = $query->result();
  }
  $query -> free_result();
  return $data;
  }
		  function updateBus($id){
  	$this->db->where('BusId',$id);
	 $data =  array(
			'BusTypeLkp' => $this->input->post('bustypelkp'),
			'RegistrationNumber' => $this->input->post('regno'),
			'MakeLkp' => $this->input->post('makelkp'),
			'DateBought' => $this->input->post('datebought'),
			'BusName' =>  $this->input->post('busname'),
			'BusNumber' =>  $this->input->post('busnumber'),
			'IsActive' =>  $this->input->post('isactive'),
			);
	$update=$this->db->update('buses',$data);
	return $update;
  }
  		function createBusTerminus($adminid,$date)
	{
			$new_bus_terminus_insert_data = array(
			'RegionLkp' => $this->input->post('region'),
			'TownLkp' => $this->input->post('town'),
			'TerminusName' => $this->input->post('terminusname'),
			'TerminusPhone' => $this->input->post('terminusphone'),
			'TerminusEmail' => $this->input->post('email'),
			'AddedDate' => $date,
			'AddedBy' => $adminid							
		);
		$insert = $this->db->insert('bus_termini', $new_bus_terminus_insert_data);
		$insert = $this->db->insert_id();
		return $insert;
	}
	function countArray($array)
{
$count=0;
foreach ($array as $k => $v)
{
    if (!empty($v))
    {
        $count++;
    }
}
return $count;

}
		function createBusRoute($rtcode,$adminid,$date)
	{
                    $tf = $this->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                    $tt = $this->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('to'));
			$new_bus_route_insert_data = array(
			'RouteCode' => $rtcode,
			'RouteName' => $tf->TownName."-".$tt->TownName,
			'From' => $this->input->post('from'),
			'To' => $this->input->post('to'),
			'Distance' =>  $this->input->post('distance'),
			'DateAdded' => $date,
			'AddedBy' => $adminid,	
			'Busno' => $this->input->post('buzno')							
		);
		$this->db->insert('bus_routes', $new_bus_route_insert_data);
		$insert = $this->db->insert_id();
		//remove null arrays
		$array=$_POST['departure'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
		//echo $count;
                
                //
                $array=$_POST['departure'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $atime = "";
		for($i=0; $i<$count; $i++) {
		if($_POST['townid'][$i]!=NULL and $_POST['departure'][$i]!=NULL and isset($_POST['checkthis'][$i])):
                    $atime = $this->bus_model->checkarrivaltimeinserted($_POST['departure'][$i]);
                if($atime<>""):
                    break;
                endif;
                endif;
                }
                //
                
                $towns = "";
		for($i=0; $i<$count; $i++) {
		if($_POST['townid'][$i]!=NULL and $_POST['departure'][$i]!=NULL and isset($_POST['checkthis'][$i])):
                    //$atime = $this->checkarrivaltimeinserted($_POST['arrival'][$i]);
                    $rtime = date('H:i',strtotime($_POST['departure'][$i].' - 30 minute'));
                    $atime = $this->checkarrivaltimeinserted($rtime);
                $dtime = $this->checkarrivaltimeinserted($_POST['departure'][$i]);
                if (isset($_POST['checkthis'][$i]))
                {
                $towns .= $_POST['townid'][$i].",";
                }
                //$towns .= $_POST['townid'][$i].",";
		$data = array(
    'RouteId'      => $insert,
    'TerminusId'      => $_POST['townid'][$i],
    'ArrivalTime'  => $atime,
	'DepartureTime'  => $dtime,
  );
  $this->db->insert('route_arrival_departure', $data);
  $routeid = $insert;
  if ($this->checkiftownardepinserted($routeid,$_POST['townid'][$i])==TRUE)
                {
                    $this->db->where('RouteId',  $routeid);
                    $this->db->where('TerminusId',  $_POST['townid'][$i]);
  $this->db->update('route_arrival_departure', $data);
                }
                else {
                    $this->db->insert('route_arrival_departure', $data);
                }
  
  
	endif;
        	}
                $data2 = array('towns'=>$towns);
  $this->db->where('RouteId',$insert);
  $this->db->update('bus_routes',$data2);
        
        return TRUE;
		
	}
		function updateBusRoute2($adminid,$date)
	{
                    
                    $routeid = $this->input->post('routeid');
                    $tf = $this->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('from'));
                    $tt = $this->getonerowfromonetbl('towns_lkp', 'TownId', $this->input->post('to'));
                      $array=$_POST['departure'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);	 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
                $atime = "";
		for($i=0; $i<$count; $i++) {
		if($_POST['townid'][$i]!=NULL and $_POST['departure'][$i]!=NULL and isset($_POST['checkthis'][$i])):
                    $atime = $this->bus_model->checkarrivaltimeinserted($_POST['departure'][$i]);
                if($atime<>""):
                    break;
                endif;
                endif;
                }
                $tm = $this->bus_model->getonerowfromonetbl('reporting_departure_times_lkp', 'TimeId', $atime);
                    $rtcode = $tf->TownSCode."-".$tt->TownSCode.date('g:iA',strtotime($tm->ArrivalDeparture))."(".$this->input->post('buzno').")";
			$new_bus_route_insert_data = array(
			'RouteCode' => $rtcode,
			'RouteName' => $tf->TownName."-".$tt->TownName,
			//'RouteName' => $this->input->post('routename'),
			//'From' => $this->input->post('from'),
			//'To' => $this->input->post('to'),
			'Distance' =>  $this->input->post('distance'),
			'DateAdded' => $date,
			'AddedBy' => $adminid,
			//'Busno' => $this->input->post('buzno')								
		);
                $this->db->where('RouteId',  $routeid);
		$this->db->update('bus_routes', $new_bus_route_insert_data);
		
               
                
		//remove null arrays
		$array=$_POST['departure'];
		foreach($array as $key => $value)
		 {
  			if($value == "") {
  			  unset($array[$key]);
  							 }
		}

		$new_array = array_values($array); 
		$count =sizeof($new_array);
		echo $count;
                $towns = "";
                 //$this->db->where('RouteId',$routeid);
  $this->db->delete('route_arrival_departure',array('RouteId' => $routeid));
		for($i=0; $i<$count; $i++) {
		if($_POST['townid'][$i]!=NULL and $_POST['departure'][$i]!=NULL and isset($_POST['checkthis'][$i])):
                    //$atime = $this->checkarrivaltimeinserted($_POST['arrival'][$i]);
                    $rtime = date('H:i',strtotime($_POST['departure'][$i].' - 30 minute'));
                    $atime = $this->checkarrivaltimeinserted($rtime);
                $dtime = $this->checkarrivaltimeinserted($_POST['departure'][$i]);
                if (isset($_POST['checkthis'][$i]))
                {
                $towns .= $_POST['townid'][$i].",";
                }
                
		$data = array(
    'RouteId'      => $routeid,
    'TerminusId'      => $_POST['townid'][$i],
    'ArrivalTime'  => $atime,
	'DepartureTime'  => $dtime,
  );
                if ($this->checkiftownardepinserted($routeid,$_POST['townid'][$i])==TRUE)
                {
                    $this->db->where('RouteId',  $routeid);
                    $this->db->where('TerminusId',  $_POST['townid'][$i]);
  $this->db->update('route_arrival_departure', $data);
                }
                else {
                    $this->db->insert('route_arrival_departure', $data);
                }
	endif;	}
        
         $data2 = array('towns'=>$towns);
  $this->db->where('RouteId',$routeid);
  $this->db->update('bus_routes',$data2);
        
        return TRUE;
		
	}
        
        function checkarrivaltimeinserted($atime)
        {
           $query = $this->db->select('*')->from('reporting_departure_times_lkp')->where('ArrivalDeparture',$atime.":00")->get();
           if ($query->num_rows()>0)
           {
               $time = $query->row();
               return $time->TimeId;
               //return TRUE;
           }
           else {
               
               $data = array(
    'ArrivalDeparture' => $atime,
    'AddedBy' => $this->flexi_auth->get_user_id()
  );
  $this->db->insert('reporting_departure_times_lkp', $data);
  return $this->db->insert_id();
               
           }
        }
        
        function checkiftownardepinserted($routeid,$townid)
        {
           $query = $this->db->select('*')->from('route_arrival_departure')->where('RouteId',$routeid)->where('TerminusId',$townid)->get();
           if ($query->num_rows()>0)
           {
               return TRUE;
           }
           else {
               return FALSE;
           }
        }


        function updateBusTerminus($id){
  	$this->db->where('TerminusId',$id);
	$data =  array(
			'RegionLkp' => $this->input->post('region'),
			'TownLkp' => $this->input->post('town'),
			'TerminusName' => $this->input->post('terminusname'),
			'TerminusPhone' => $this->input->post('terminusphone'),
			'TerminusEmail' => $this->input->post('email')
			);
	$update=$this->db->update('bus_termini',$data);
	return $update;
  }
  		function createBusMaintenance($adminid,$date)
	{
			$new_bus_checkup_insert_data = array(
			'BusLkp' => $this->input->post('buslkp'),
			'DateAdmitted' => $this->input->post('admitted'),
			'DateReleased' => $this->input->post('released'),
			'MaintenanceLkp' => $this->input->post('maintenancelkp'),
			'Description' => $this->input->post('description'),
			'IsSorted' => $this->input->post('issorted'),
			'DateAdded' => $date,
			'AddedBy' => $adminid							
		);
		$insert = $this->db->insert('bus_maintenance', $new_bus_checkup_insert_data);
		$insert = $this->db->insert_id();
		return $insert;
	}
	  function createRoutePrice($adminid,$date)
	{
			$new_route_price_insert_data = array(
			'FareCode' => $this->input->post('farecode'),
			'RouteFrom' => $this->input->post('from'),
			'RouteTo' => $this->input->post('to'),
			'BusTypeLkp' => $this->input->post('bustype'),
			'SeatLkp' => $this->input->post('seat'),
			'SeasonLkp' => $this->input->post('season'),
			'KeAmount' => $this->input->post('keamount'),
			'UgAmount' => $this->input->post('ugamount'),
			'TzAmount' => $this->input->post('tzamount'),
			'DateAdded' => $date,
			'AddedBy' => $adminid							
		);
		$insert = $this->db->insert('route_prices', $new_route_price_insert_data);
		$insert = $this->db->insert_id();
		return $insert;
	}
		  function createBusSchedule($adminid,$date)   
	{
			$new_bus_schedule_insert_data = array(
			'BusLkp' => $this->input->post('bus'),
			'RouteLkp' => $this->input->post('route'),
			'PassengerArrivalTimeLkp' => $this->input->post('arrival'),
			'TerminalFrom' => $this->input->post('terminalfrom'),
			'TerminalTo' => $this->input->post('terminalto'),
			'ArrivalDateTime' => $this->input->post('arrivaldatetime'),
			'DepartureDateTime' => $this->input->post('departuredatetime'),
			'IsActive' => $this->input->post('isactive'),
			'DateAdded' => $date,
			'AddedBy' => $adminid							
		);
		$insert = $this->db->insert('bus_schedule', $new_bus_schedule_insert_data);
		$insert = $this->db->insert_id();
		return $insert;
	}
		  function updateBusSchedule($id){
  	$this->db->where('ScheduleId',$id);
	$data =  array(
			'BusLkp' => $this->input->post('bus'),
			'RouteLkp' => $this->input->post('route'),
			'PassengerArrivalTimeLkp' => $this->input->post('arrival'),
			'TerminalFrom' => $this->input->post('terminalfrom'),
			'TerminalTo' => $this->input->post('terminalto'),
			'ArrivalDateTime' => $this->input->post('arrivaldatetime'),
			'DepartureDateTime' => $this->input->post('departuredatetime'),
			'IsActive' => $this->input->post('isactive'),
			);
	$update=$this->db->update('bus_schedule',$data);
	return $update;
  }
	  	function updateRoutePrice($id){
  	$this->db->where('PriceId',$id);
	$data =  array(
			'RouteLkp' => $this->input->post('route'),
			'BusTypeLkp' => $this->input->post('bustype'),
			'SeatLkp' => $this->input->post('seat'),
			'SeasonLkp' => $this->input->post('season'),
			'KeAmount' => $this->input->post('keamount'),
			'UgAmount' => $this->input->post('ugamount'),
			'TzAmount' => $this->input->post('tzamount')
			);
	$update=$this->db->update('route_prices',$data);
	return $update;
  }
  	function updateBusMaintenance($id){
  	$this->db->where('MaintenanceId',$id);
	$data =  array(
			'BusLkp' => $this->input->post('buslkp'),
			'DateAdmitted' => $this->input->post('admitted'),
			'DateReleased' => $this->input->post('released'),
			'MaintenanceLkp' => $this->input->post('maintenancelkp'),
			'Description' => $this->input->post('description'),
			'IsSorted' => $this->input->post('issorted')
			);
	$update=$this->db->update('bus_maintenance',$data);
	return $update;
  }
	  function updateBusRoute($id){
  	$this->db->where('RouteId',$id);
	 $data =  array(
			'RouteCodeLkp' => $this->input->post('routecode'),
			'From' => $this->input->post('from'),
			'To' => $this->input->post('to'),
			'Distance' =>  $this->input->post('distance'),
			'RegionLkp' =>  $this->input->post('region'),
			);
	$update=$this->db->update('bus_routes',$data);
	return $update;
  }
  	function removeBusTerminus($id) {
$this->db->delete('bus_termini', array('TerminusId' => $id));
}
	function removeBusMaintenance($id) {
$this->db->delete('bus_maintenance', array('MaintenanceId' => $id));
}
	function removeBusRoute($id) {
$this->db->delete('bus_routes', array('RouteId' => $id));
}
	function removeBus($id) {
$this->db->delete('buses', array('BusId' => $id));
}
	function removeRoutePrice($id) {
$this->db->delete('route_prices', array('PriceId' => $id));
}
	function removeBusSchedule($id) {
$this->db->delete('bus_schedule', array('ScheduleId' => $id));
}
  	function checkBus($str){
	        $result = $this->db->get_where('buses', array('RegistrationNumber' => $str));

        // Is there a row with this email?
        if ($result->num_rows > 0)
        {
            // Let's return false for the validation and set a custom message for this function
            $this->form_validation->set_message('bus_unique', 'That bus already exist.');
            return FALSE;
        }
        else
        {
            // Everything is good, don't return an error.
            return TRUE;
        }

	}
			function getModels()
{
	  $query = $this->db->get('models_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getRouteCodes()
{     $this->db->select('*');
      $this->db->where('routecodes_lkp.status !=',0);
	  $query = $this->db->get('routecodes_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getMakes()
{
      $this->db->select('*');
      $this->db->where('makes_lkp.status !=',0);
	  $query = $this->db->get('makes_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
		function get_town_by_id($id){
        $this->db->select('TownId, TownName');
		$this->db->where('TownId', $id);
        $query = $this->db->get('towns_lkp');
        $cities = array();

      if($query->result()){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
        } else {
            return FALSE;
        }
    }
		function get_name_by_code ($id){
        $this->db->select('RouteCodeId, RouteName');
		$this->db->where('RouteCodeId', $id);
        $query = $this->db->get('routecodes_lkp');
        $cities = array();

      if($query->result()){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
        } else {
            return FALSE;
        }
    }  
		function getMaintenanceTypes()
{
	  $query = $this->db->get('maintenance_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
		function getTowns()
{	    $this->db->select('*');
      $this->db->where('towns_lkp.status !=',0);
	  $query = $this->db->get('towns_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getTownsByTownids($tidsr)
{	   
                    $query = $this->db->select('*')->from('towns_lkp')->where('towns_lkp.status !=',0)->where_in('TownId',$tidsr)->get();
      
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getTownsOutOfTownids($tidsr)
{	   
                    $query = $this->db->select('*')->from('towns_lkp')->where('towns_lkp.status !=',0)->where_not_in('TownId',$tidsr)->get();
      
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getTownstot()
{	    $this->db->select('*');
      $this->db->where('towns_lkp.status !=',0);
	  $query = $this->db->get('towns_lkp');
 	  
  	  $data = $query->num_rows();
	  $query -> free_result();
      return $data;
}
		function getBusTermini()
{
 		$this->db->select("bus_termini.*", FALSE);
		$this->db->select("regions_lkp.*", FALSE);
		$this->db->select("towns_lkp.*", FALSE);
 		$this->db->from("bus_termini");
		$this->db->join("regions_lkp", "regions_lkp.RegionId = bus_termini.RegionLkp",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = bus_termini.TownLkp",'inner');
		$this->db->where('towns_lkp.status !=',0);
		$this->db->where('bus_termini.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getBusRoutes()
{
		$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
 		$this->db->from("bus_routes");
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->join("towns_lkp", "towns_lkp.TownId = bus_routes.From",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = bus_routes.To",'inner');
		$this->db->where('bus_routes.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }

}
		function getRoutePrices()
{
 		$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("seats_lkp.*", FALSE);
		$this->db->select("bustype_lkp.*", FALSE);
		$this->db->select("season_lkp.*", FALSE);
		$this->db->select("route_prices.*", FALSE);
		$this->db->select("route_prices.ApprovedStatus as aps", FALSE);
 		$this->db->from("route_prices");
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = route_prices.RouteTo",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId = route_prices.SeatLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = route_prices.BusTypeLkp",'inner');
		$this->db->join("season_lkp", "season_lkp.SeasonId = route_prices.SeasonLkp",'left outer');
		$this->db->where('route_prices.status !=',0);
		$this->db->order_by('route_prices.ApprovedStatus','asc');
		$this->db->order_by('bustype_lkp.BusTypeId','asc');
		//$this->db->order_by('bustype_lkp.BusTypeId','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function checkaddedrouteprices($bustype,$seatetype,$townf,$townt)
{
 		$this->db->select("*");
 		$this->db->from("route_prices");
		$this->db->where('route_prices.status !=',0);
		$this->db->where('route_prices.BusTypeLkp',$bustype);
		$this->db->where('route_prices.SeatLkp',$seatetype);
		$this->db->where('route_prices.RouteFrom',$townf);
		$this->db->where('route_prices.RouteTo',$townt);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data1 = $query->row();
                $data = $data1->PriceId;
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     $this->db->select("*");
                    $this->db->from("route_prices");
                    $this->db->where('route_prices.status !=',0);
		$this->db->where('route_prices.BusTypeLkp',$bustype);
		$this->db->where('route_prices.SeatLkp',$seatetype);
                    $this->db->where('route_prices.RouteFrom',$townt);
                    $this->db->where('route_prices.RouteTo',$townf);
                    $query = $this->db->get();
                    if($query->num_rows > 0){
                    $data1 = $query->row();
                    $data = $data1->PriceId;
                    $query -> free_result();
                    return $data;
                     }
                     else {
                         return NULL;
                     }
                     
                 }
}
		function getspecRoutePrices($bustype,$seatype,$routefrom,$routeto)
{
 		//$this->db->select("towns_lkp.*",FALSE);
		///$this->db->select("seats_lkp.*", FALSE);
		//$this->db->select("bustype_lkp.*", FALSE);
		//$this->db->select("season_lkp.*", FALSE);
		//$this->db->select("route_prices.*", FALSE);
                    $this->db->select("*");
 		$this->db->from("route_prices");
		$this->db->where('route_prices.status !=',0);
		$this->db->where('route_prices.BusTypeLkp',$bustype);
		$this->db->where('route_prices.SeatLkp',$seatype);
		$this->db->where('route_prices.RouteFrom',$routefrom);
                $this->db->where('route_prices.RouteTo',$routeto);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getpettyCash()
{
 		$this->db->select("*, petty_cash.Currency as Cur");
		$this->db->from("petty_cash");
		//$this->db->join("admins", "admins.AdminId = petty_cash.CashierId",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = petty_cash.StationId",'inner');
		$this->db->join("bus_termini", "bus_termini.TerminusId = petty_cash.SubstationId",'inner');
		//$this->db->join("account_codes", "account_codes.AccountCodeId = petty_cash.AccountCodeId",'inner');
		$this->db->where('petty_cash.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getpettyCashByClerkToday($clerk)
{
 		$this->db->select("*, petty_cash.Currency as Cur");
		$this->db->from("petty_cash");
		//$this->db->join("admins", "admins.AdminId = petty_cash.CashierId",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = petty_cash.StationId",'inner');
		$this->db->join("bus_termini", "bus_termini.TerminusId = petty_cash.SubstationId",'inner');
		//$this->db->join("account_codes", "account_codes.AccountCodeId = petty_cash.AccountCodeId",'inner');
		$this->db->where('petty_cash.status !=',0);
		$this->db->where('petty_cash.CashierId',$clerk);
		$this->db->where('petty_cash.CashDate',date('Y-m-d'));
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getBuses()
{
 		$this->db->select("bustype_lkp.*", FALSE);
		$this->db->select("makes_lkp.*", FALSE);
		$this->db->select("buses.*", FALSE);
		$this->db->select("towns_lkp.TownName", FALSE);
 		$this->db->from("buses");
		$this->db->join("makes_lkp", "makes_lkp.MakeId = buses.MakeLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = buses.BusTypeLkp",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = buses.station_at",'inner');
		$this->db->where('buses.status !=',0);
		$this->db->order_by('buses.RegistrationNumber','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getAvailableBuses()
{
 		$this->db->select("bustype_lkp.*", FALSE);
		$this->db->select("makes_lkp.*", FALSE);
		$this->db->select("buses.*", FALSE);
 		$this->db->from("buses");
		$this->db->join("makes_lkp", "makes_lkp.MakeId = buses.MakeLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = buses.BusTypeLkp",'inner');
		$this->db->where('IsActive', 1);
		$this->db->where('buses.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getwouldbeAvailableBuses($townf,$bustype,$depdate,$deptime)
{
 		$this->db->select("bustype_lkp.*", FALSE);
		$this->db->select("makes_lkp.*", FALSE);
		$this->db->select("buses.*", FALSE);
 		$this->db->from("buses");
		$this->db->join("makes_lkp", "makes_lkp.MakeId = buses.MakeLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = buses.BusTypeLkp",'inner');
		$this->db->where('buses.station_at',$townf);
		$this->db->where('buses.BusTypeLkp',$bustype);
		$this->db->where('buses.status !=',0);
		$this->db->where('IsActive', 0);
		$this->db->where('buses.date_available_from <', $depdate);
		$this->db->or_where('buses.date_available_from', $depdate);
		$this->db->where('buses.time_available_from <=', $deptime);
		$this->db->where('buses.station_at',$townf);
		$this->db->where('buses.BusTypeLkp',$bustype);
		$this->db->where('buses.status !=',0);
		$this->db->where('IsActive', 0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
}

		function getwouldbeAvailableBusesActive($townf,$bustype)
{
 		$this->db->select("bustype_lkp.*", FALSE);
		$this->db->select("makes_lkp.*", FALSE);
		$this->db->select("buses.*", FALSE);
 		$this->db->from("buses");
		$this->db->join("makes_lkp", "makes_lkp.MakeId = buses.MakeLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = buses.BusTypeLkp",'inner');
		$this->db->where('buses.station_at', $townf);
		$this->db->where('buses.BusTypeLkp', $bustype);
		$this->db->where('buses.IsActive', 1);
		$this->db->where('buses.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
}

        function getwouldbeAvailableDriversActive($townf)
        {
            $this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',1);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',1);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
        }

        function getwouldbeAvailableDrivers($townf,$depdate,$deptime)
        {
            $this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',1);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',0);
		$this->db->where('bus_staff.date_available_from <=', $depdate);
		$this->db->or_where('bus_staff.date_available_from', $depdate);
		$this->db->where('bus_staff.time_available_from <=', $deptime);
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',1);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
        }

        function getwouldbeAvailableConductorsActive($townf)
        {
            $this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',2);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',1);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
        }

        function getwouldbeAvailableConductors($townf,$depdate,$deptime)
        {
            $this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',2);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',0);
		$this->db->where('bus_staff.date_available_from <=', $depdate);
		$this->db->or_where('bus_staff.date_available_from', $depdate);
		$this->db->where('bus_staff.time_available_from <=', $deptime);
		$this->db->where('bus_staff.status',1);
		$this->db->where('bus_staff.StaffJobLkp',2);
		$this->db->where('bus_staff.station_at', $townf);
		$this->db->where('bus_staff.IsActive',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                 return NULL;
                 }
        }
        
		function getBusMaintenance()
{
 		$this->db->select("buses.*", FALSE);
		$this->db->select("bus_maintenance.*", FALSE);
		$this->db->select("maintenance_lkp.*", FALSE);
 		$this->db->from("bus_maintenance");
		$this->db->join("buses", "buses.BusId = bus_maintenance.BusLkp",'inner');
		$this->db->join("maintenance_lkp", "maintenance_lkp.TypeId = bus_maintenance.MaintenanceLkp",'inner');
		$this->db->where('bus_maintenance.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getBusSchedules() 
{
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_staff", "bus_staff.StaffId = bus_schedule.Driver",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'left outer');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive !=',0);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                //$this->db->where('Pack_sending_date > DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                $this->db->order_by('bus_schedule.DepartureDateTime','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getDriversFromDriverSchedules($schedid) 
{
 		$this->db->select("*");
 		$this->db->from("driver_schedules");
		$this->db->join("bus_staff", "bus_staff.StaffId = driver_schedules.driver_id",'inner');
		$this->db->where('driver_schedules.status !=',0);
		$this->db->where('bus_staff.status !=',0);
		$this->db->where('driver_schedules.ScheduleId',$schedid);
                $this->db->group_by('bus_staff.StaffId');
                $this->db->order_by('bus_staff.StaffName','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getBusSchedules2() 
{
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_staff", "bus_staff.StaffId = bus_schedule.Driver",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'left outer');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive !=',0);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime < DATE_ADD(CURDATE(), INTERVAL 7 DAY)');
                $this->db->order_by('bus_schedule.DepartureDateTime','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getBusSchedules_refine($branchf,$brancht,$datef,$datet) 
{
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_staff", "bus_staff.StaffId = bus_schedule.Driver",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'left outer');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive !=',0);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                if ($branchf<>0)
                {
                $this->db->where('bus_routes.From',$branchf);
                }
                if ($brancht<>0)
                {
                $this->db->where('bus_routes.To',$brancht);
                }
                $this->db->where('bus_schedule.DepartureDateTime >=',$datef);
                $this->db->where('bus_schedule.DepartureDateTime <=',$datet);
                $this->db->order_by('bus_schedule.DepartureDateTime','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getSpecBusSchedule($id) 
{
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.ScheduleId',$id);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query->free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getBusSchedulesnotification() 
{
                    $null = NULL;
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_staff", "bus_staff.StaffId = bus_schedule.Driver",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'left outer');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive !=',0);
		$this->db->where('bus_schedule.DepartureDateTime',date('Y-m-d'));
		$this->db->where('bus_schedule.BusLkp IS NULL');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

function getBusScheduleswithtickets($routecode,$traveldate,$from,$to,$seat,$bustype,$schedule) 
{
 		$this->db->select("*,bus_schedule.IsActive as astatus");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_staff", "bus_staff.StaffId = bus_schedule.Driver",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'left outer');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_routes.RouteCode',$routecode);
		$this->db->where('bus_schedule.DepartureDateTime',$traveldate);
		$this->db->where('ticketing.fromtown',$from);
		$this->db->where('ticketing.totown',$to);
		$this->db->where('ticketing.seatno !=',$seat);
		$this->db->where('bus_schedule.BusTypeLkp',$bustype);
		$this->db->where('ticketing.schedule_id !=',$schedule);
		$this->db->group_by('bus_schedule.ScheduleId');
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

function checkifseatalreadybooked($seat,$from,$to,$schedule) 
{
 		$this->db->select("*");
 		$this->db->from("ticketing");
		$this->db->where('ticketing.seatno',$seat);
		$this->db->where('ticketing.fromtown',$from);
		$this->db->where('ticketing.totown',$to);
		$this->db->where('ticketing.schedule_id',$schedule);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('ticketing.seatno',$seat);
		$this->db->where('ticketing.fromtown',$from);
		$this->db->where('ticketing.totown',$to);
		$this->db->where('ticketing.schedule_id',$schedule);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = FALSE;
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return TRUE;
                 }
}

	function apply_seat_discount($busid,$seattype) {
$count = sizeof($this->input->post('seatid', TRUE));
$seatid = $this->input->post('seatid', TRUE);
$disctype = $this->input->post('DiscountType', TRUE);
$discamount = $this->input->post('DiscountAmount', TRUE);
$discfrom = $this->input->post('DiscountFrom', TRUE);
$discto = $this->input->post('DiscountTo', TRUE);
for ($i=0;$i<$count;$i++) {
    $data = array(
    'discount_factor' => $disctype ,
    'discount_value' => $discamount ,
    'discount_from' => $discfrom ,
    'discount_to' => $discto ,
    'seat_id' => $seatid[$i]
  );
$this->db->where('seat_id', $seatid[$i]);
$this->db->from('seat_discounts');
$count = $this->db->count_all_results();
if($count == 0):
$insert=$this->db->insert('seat_discounts', $data);
		$updatedata =  array('IsDiscounted' => 1);
                       $tablename = 'seats';
                        $pkname = 'seat_id';
                        $pkvalue = $seatid[$i];
            $this->bus_model->dbupdate($tablename, $updatedata, $pkname, $pkvalue);
else:
return false;
endif;
}
  }
		function getBusSchedulesbyTown($townid) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('m/d/Y H:i'));
                $this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime <=',date('Y-m-d'));
                //$this->db->where('bus_schedule.DepartureDateTime',date('m/d/Y H:i'));
		$this->db->where('route_arrival_departure.TerminusId',$townid);
		//$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulesbySchedIdandTown($schedid,$townid) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('m/d/Y H:i'));
		$this->db->where('bus_schedule.ScheduleId',$schedid);
		$this->db->where('route_arrival_departure.TerminusId',$townid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulesbySchedId($schedid) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		//$this->db->where('bus_schedule.DepartureDateTime >=',date('m/d/Y H:i'));
		$this->db->where('bus_schedule.ScheduleId',$schedid);
		//$this->db->where('route_arrival_departure.TerminusId',$townid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulesbysearch($datet,$townfid,$towntid) 
{
               $start_date = date('Y-m-d',strtotime($datet)); 
               $end_date = strtotime(date('Y-m-d',strtotime($datet. ' + 1 day'))); // '2010-03-24';

 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
                $this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime',$start_date);
                $this->db->where('bus_schedule.lockstatus',0);
                //$this->db->where('bus_schedule.DepartureDateTime <',$end_date);
                $this->db->where('FIND_IN_SET('.$townfid.', `towns`) ');
                $this->db->where('FIND_IN_SET('.$towntid.', `towns`) ');
		$this->db->group_by('bus_schedule.ScheduleId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulesbysearchcorp($datet,$townfid,$towntid) 
{
               $start_date = date('Y-m-d',strtotime($datet)); 
               $end_date = strtotime(date('Y-m-d',strtotime($datet. ' + 1 day'))); // '2010-03-24';

 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('bus_schedule.IsSpecial',0);
                $this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime',$start_date);
                //$this->db->where('bus_schedule.DepartureDateTime <',$end_date);
                $this->db->where('FIND_IN_SET('.$townfid.', `towns`) ');
                $this->db->where('FIND_IN_SET('.$towntid.', `towns`) ');
		$this->db->group_by('bus_schedule.ScheduleId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulestotseats($schedid,$townfid,$towntid) 
{
 		$this->db->select("*");
 		$this->db->from("ticketing");
		$this->db->join("bus_schedule", "bus_schedule.ScheduleId = ticketing.schedule_id ",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.schedule_id',$schedid);
		$this->db->where('ticketing.fromtown',$townfid);
		$this->db->where('ticketing.totown',$towntid);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.schedule_id',$schedid);
		$this->db->where('ticketing.fromtown',$townfid);
		$this->db->where('ticketing.totown',$towntid);
		//$this->db->group_by('bus_schedule.ScheduleId');
		$query = $this->db->get();
 	  	return $query->num_rows;
 		 
}
		function getBusSchedulesbyTownFTownToDate($townfid,$towntid,$tdate) 
{
 		//$this->db->select("*");
 		//$this->db->from("bus_schedule");
		//$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		//$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		//$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		//$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
                //$this->db->where('DATE(bus_schedule.DepartureDateTime) >=',$tdate);
		//$this->db->where('route_arrival_departure.TerminusId',$townid);
                //$tdate= 
                $query = $this->db->query("SELECT *
FROM route_arrival_departure t1, route_arrival_departure t2 JOIN bus_schedule ON t2.RouteId=bus_schedule.RouteLkp
WHERE (t1.RouteId + 1)=t2.RouteId
AND t1.TerminusId=$townfid
AND t2.TerminusId=$towntid
"               
                        );
		//$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBusSchedulesbyTownDate($townid,$date) 
{		$date=date('Y-m-d',strtotime($date));
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('bus_schedule.DepartureDateTime =',$date);
		$this->db->where('bus_routes.From',1);
		//$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getBusSchedulesbyTownFTDate($townfid,$towntid,$date1) 
{		$date=date('Y-m-d',strtotime($date1));
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		//$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('bus_routes.From',$townfid);
		$this->db->where('bus_routes.To',$towntid);
		$this->db->where('bus_schedule.DepartureDateTime >=',$date);
		$this->db->where('bus_schedule.DepartureDateTime <=',$date);
		$this->db->group_by('bus_schedule.ScheduleId');
		//$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getseattype($seatlkp,$booked)
{
		$this->db->select("*,seats.seat_no as seats");
 		$this->db->from("seats");
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId = seats.SeatTypeId",'inner');
		$this->db->where('seats.status !=',0);
		$this->db->where('seats.SeatTypeId',$seatlkp);
		$this->db->where_not_in('seats.seat_no',$booked);
		$this->db->order_by('seats.seat_no','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$row = $query->result();
		$query -> free_result();
         return $row; 
 		 }
}

		function getseattyperr($seatlkp,$booked)
{
		$this->db->select("*,seats.seat_no as seats");
 		$this->db->from("seats");
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId = seats.SeatTypeId",'inner');
		$this->db->where('seats.status !=',0);
		$this->db->where('seats.SeatTypeId',$seatlkp);
		$this->db->where_not_in('seats.seat_no',$booked);
		$this->db->order_by('seats.seat_no','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$row = $query->result_array();
		//$query -> free_result();
         return $row; 
 		 }
}
function getBusSeatsdics($seattype,$bustype)
{
  $this->db->select("*");
   $this->db->from("seats");
  $this->db->where('seats.status !=',0);
                if ($seattype<>0):
  $this->db->where('seats.SeatTypeId',$seattype);
                endif;
                 if ($bustype<>0):
  $this->db->where('seats.BusTypeId',$bustype);
                endif;
$this->db->group_by('seats.seat_no');
  $this->db->order_by("seat_no","asc");
  $query = $this->db->get();
     if($query->num_rows > 0){
    $data = $query->result();
  $query -> free_result();
       return $data;
    }
}
function getbranchesdics()
{
  $this->db->select("*");
   $this->db->from("towns_lkp");
  $this->db->where('towns_lkp.status !=',0);
  //$this->db->order_by('seats.seat_no','asc');
  $query = $this->db->get();
     if($query->num_rows > 0){
    $data = $query->result();
  $query -> free_result();
       return $data;
    }
}
function getbranchtotal()
{
  $this->db->select("*");
   $this->db->from("towns_lkp");
  $this->db->where('towns_lkp.status !=',0);
  //$this->db->order_by('seats.seat_no','asc');
   $query = $this->db->get();
   $data = $query->num_rows;
   $query -> free_result();
   return $data;
}             
function getBusSeatsdicstotrows($seattype,$bustype)
{
  $this->db->select("*");
   $this->db->from("seats");
  $this->db->join("seats_lkp", "seats_lkp.SeatTypeId = seats.SeatTypeId",'inner');
  $this->db->join("buses", "seats.BusId = buses.BusId",'inner');
  $this->db->join("bustype_lkp", "buses.BusTypeLkp = bustype_lkp.BusTypeId",'inner');
  $this->db->where('seats.status !=',0);
  if ($seattype<>0):
  $this->db->where('seats.SeatTypeId',$seattype);
                endif;
                 if ($bustype<>0):
  $this->db->where('bustype_lkp.BusTypeId',$bustype);
                endif;
  //$this->db->order_by('seats.seat_no','asc');
  $query = $this->db->get();
     
    $data = $query->num_rows();
  $query -> free_result();
       return $data;
    
}
		function getseatcost($seatlkp,$from,$to,$bustype)
{
		$this->db->select("*");
 		$this->db->from("route_prices");
		$this->db->where('route_prices.status !=',0);
		$this->db->where('route_prices.SeatLkp',$seatlkp);
		$this->db->where('route_prices.RouteFrom',$from);
		$this->db->where('route_prices.RouteTo',$to);
		$this->db->where('route_prices.BusTypeLkp',$bustype);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getaddedseats($busid)
{

		$this->db->select("*,GROUP_CONCAT(seats.seat_no) as seats");
 		$this->db->from("seats");
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId = seats.SeatTypeId",'inner');
		$this->db->join("seat_position_lkp", "seat_position_lkp.SeatPositionLkp = seats.SeatPositionLkp",'inner');
		$this->db->where('seats.status !=',0);
		$this->db->where('seats.BusId',$busid);
		$this->db->group_by('seats.SeatTypeId');
		$this->db->order_by('seats.seat_no','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}

		function getBusScheduledetails($scheduleid) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('route_arrival_departure.TerminusId',$townid);
		$this->db->group_by('bus_schedule.ScheduleId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result_row();
		$query -> free_result();
      	return $data;
 		 }
}
		function getBusSchedulesbyStation($datet,$townfid,$towntid) 
{
 		$start_date = date('Y-m-d',strtotime($datet)); 
               $end_date = strtotime(date('Y-m-d',strtotime($datet. ' + 1 day'))); // '2010-03-24';

 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
                //$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime',$start_date);
                //$this->db->where('bus_schedule.DepartureDateTime <',$end_date);
                $this->db->where('FIND_IN_SET('.$townfid.', `towns`) ');
                $this->db->where('FIND_IN_SET('.$towntid.', `towns`) ');
		$this->db->group_by('bus_schedule.ScheduleId');
                $query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		//$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
}
}

		function getBusSchedulesbyStationrr($datet,$townfid,$towntid) 
{
 		$start_date = date('Y-m-d',strtotime($datet)); 
               //$end_date = strtotime(date('Y-m-d',strtotime($datet. ' + 1 day'))); // '2010-03-24';

 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
                //$this->db->where('bus_schedule.DepartureDateTime >=',date('Y-m-d'));
                $this->db->where('bus_schedule.DepartureDateTime',$start_date);
                //$this->db->where('bus_schedule.DepartureDateTime <',$end_date);
                $this->db->where('FIND_IN_SET('.$townfid.', `towns`) ');
                $this->db->where('FIND_IN_SET('.$towntid.', `towns`) ');
		$this->db->group_by('bus_schedule.ScheduleId');
                $query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result_array();
		//$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
}
}
		function getcustomerVoucher($vouchercode,$customerid) 
{
 		$this->db->select("*");
 		$this->db->from("vouchers");
		$this->db->join("voucher_items", "voucher_items.vouchers_id = vouchers.vouchers_id",'inner');
		$this->db->join("card_details", "card_details.card_details_id= voucher_items.CustomerId",'inner');
		$this->db->where('voucher_items.status !=',0);
		$this->db->where('vouchers.status !=',0);
		$this->db->where('voucher_items.voucher_code =',''.$vouchercode.'');
		$this->db->where('card_details.card_no =',$customerid);
		$this->db->where('voucher_items.redeemed =',0);
		$this->db->where('vouchers.vdateto >=',date('Y-m-d'));
		$query = $this->db->get();
 	  	if($query->num_rows ==1){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
		 else{
		 return NULL;
		 }
}
		function validatecustomermpesa($mobile,$transid,$amount)
{
 		$this->db->select("*");
 		$this->db->from("mpesa_trans");
		$this->db->where('mpesa_trans.trans_status =',0);
		$this->db->where('mpesa_trans.sender_phone =',''.$mobile.'');
		$this->db->where('mpesa_trans.empTras_id =',$transid);
		$this->db->where('mpesa_trans.empesa_amount =',$amount);
		$query = $this->db->get();
 	  	if($query->num_rows ==1){
  		$data = $query->row();
		$query -> free_result();
      	return TRUE;
 		 }
		 else{
		 return FALSE;
		 }
}
		function getcustomervouchers($voucherid) 
{
 		$this->db->select("*");
 		$this->db->from("vouchers");
		$this->db->join("voucher_items", "voucher_items.vouchers_id = vouchers.vouchers_id",'inner');
		$this->db->join("card_details", "card_details.card_details_id= voucher_items.CustomerId",'inner');
		$this->db->where('voucher_items.status !=',0);
		$this->db->where('vouchers.status !=',0);
		$this->db->where('vouchers.vouchers_id =',$voucherid);
		$query = $this->db->get();
 	  	if($query->num_rows >=1){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}

function ifrouteseatpriceadded($bustypeid,$seatclassid,$routefrom,$routeto) 
{
 		$this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routefrom);
                        $this->db->where('route_prices.RouteTo',$routeto);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        if ($query->num_rows()>0)
                        {
                            return $query->row();
                        }
                        else {
                            return NULL;
                        }
                        
}

function unapprovedrouteprices() 
{
 		$this->db->select("*");
                        $this->db->from("route_prices");
                        $this->db->where('route_prices.status !=',0);
                        $this->db->where('route_prices.ApprovedStatus',0);
                        $query = $this->db->get();
                        if ($query->num_rows()>0)
                        {
                            return TRUE;
                        }
                        else {
                            return FALSE;
                        }
                        
}
		function routeseatprice($bustypeid,$seatclassid,$routefrom,$routeto) 
{
 		$this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routefrom);
                        $this->db->where('route_prices.RouteTo',$routeto);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
                        if($query->num_rows >0){
                        $data = $query->row();
                        $query -> free_result();
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->KeAmount + $activeseason->PAmountke;
                            }
                            else {
                                return $data->KeAmount;
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->UgAmount + $activeseason->PAmountug;
                            }
                            else {
                                return $data->UgAmount;
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->TzAmount + $activeseason->PAmounttz;
                            }
                            else {
                                return $data->TzAmount;
                            }
                        }
 		 }
                 else {
                     $this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        //$this->db->where('bus_routes.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routeto);
                        $this->db->where('route_prices.RouteTo',$routefrom);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        if($query->num_rows >0){
                        $data = $query->row();
                        $query -> free_result();
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->KeAmount + $activeseason->PAmountke;
                            }
                            else {
                                return $data->KeAmount;
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->UgAmount + $activeseason->PAmountug;
                            }
                            else {
                                return $data->UgAmount;
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->TzAmount + $activeseason->PAmounttz;
                            }
                            else {
                                return $data->TzAmount;
                            }
                        }
                 }
                 else {
                     return "0";
                 }
                 }
}
		function routeseatpricearray($bustypeid,$seatclassid,$routefrom,$routeto) 
{
 		$this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routefrom);
                        $this->db->where('route_prices.RouteTo',$routeto);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        //$this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        //$tid = $this->user[0]->t_station;
                        $tcur = $this->getonerowfromonetbl_array('towns_lkp','TownId', $routefrom);
                        $cur = $tcur['Currency'];
                        if($query->num_rows >0){
                        $data = $query->row_array();
                        
                        $activeseason = $this->getonerowfromonetbl_array('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['KeAmount'] + $activeseason['PAmountke'];
                            }
                            else {
                                return $data['KeAmount'];
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['UgAmount'] + $activeseason['PAmountug'];
                            }
                            else {
                                return $data['UgAmount'];
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['TzAmount'] + $activeseason['PAmounttz'];
                            }
                            else {
                                return $data['TzAmount'];
                            }
                        }
 		 }
                 else {
                     $this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        //$this->db->where('bus_routes.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routeto);
                        $this->db->where('route_prices.RouteTo',$routefrom);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        if($query->num_rows >0){
                        $data = $query->row_array();
                        //$query -> free_result();
                        $activeseason = $this->getonerowfromonetbl_array('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['KeAmount'] + $activeseason['PAmountke'];
                            }
                            else {
                                return $data['KeAmount'];
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['UgAmount'] + $activeseason['PAmountug'];
                            }
                            else {
                                return $data['UgAmount'];
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data['TzAmount'] + $activeseason['PAmounttz'];
                            }
                            else {
                                return $data['TzAmount'];
                            }
                        }
                 }
                 else {
                     return "0";
                 }
                 }
}
		function routeseatpricewithcurrency($bustypeid,$seatclassid,$routefrom,$routeto,$cur) 
{
 		$this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routefrom);
                        $this->db->where('route_prices.RouteTo',$routeto);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        
                        if($query->num_rows >0){
                        $data = $query->row();
                        $query -> free_result();
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->KeAmount + $activeseason->PAmountke;
                            }
                            else {
                                return $data->KeAmount;
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->UgAmount + $activeseason->PAmountug;
                            }
                            else {
                                return $data->UgAmount;
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->TzAmount + $activeseason->PAmounttz;
                            }
                            else {
                                return $data->TzAmount;
                            }
                        }
 		 }
                 else {
                     $this->db->select("*");
                        //$this->db->from("bus_schedule");
                        $this->db->from("route_prices");
                        $this->db->join("towns_lkp", "towns_lkp.TownId = route_prices.RouteFrom",'inner');
                        //$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
                        //$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                        //$this->db->where('bus_schedule.status !=',0);
                        //$this->db->where('bus_routes.status !=',0);
                        $this->db->where('towns_lkp.status !=',0);
                        $this->db->where('route_prices.status !=',0);
                        //$this->db->where('route_prices.FareCode =',$routecode);
                        $this->db->where('route_prices.BusTypeLkp',$bustypeid);
                        $this->db->where('route_prices.SeatLkp',$seatclassid);
                        $this->db->where('route_prices.RouteFrom',$routeto);
                        $this->db->where('route_prices.RouteTo',$routefrom);
                        //$this->db->group_by('route_prices.FareCode');
                        //$this->db->group_by('route_prices.SeatLkp');
                        //$this->db->group_by('route_arrival_departure.TerminusId');
                        $query = $this->db->get();
                        if($query->num_rows >0){
                        $data = $query->row();
                        $query -> free_result();
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        if ($cur==1)
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->KeAmount + $activeseason->PAmountke;
                            }
                            else {
                                return $data->KeAmount;
                            }
                        
                        }
                        if ($cur==2) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->UgAmount + $activeseason->PAmountug;
                            }
                            else {
                                return $data->UgAmount;
                            }
                        }
                        if ($cur==3) 
                        {
                            if ($activeseason<>NULL)
                            {
                                return $data->TzAmount + $activeseason->PAmounttz;
                            }
                            else {
                                return $data->TzAmount;
                            }
                        }
                 }
                 else {
                     return "0";
                 }
                 }
}
		function routesingleseatprice($scheduleid,$seat) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_prices", "route_prices.FareCode= bus_routes.RouteCode",'inner');
                $this->db->join("seats", "seats.SeatTypeId= route_prices.SeatLkp",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.ScheduleId =',$scheduleid);
		$this->db->where('seats.seat_no',$seat);
		//$this->db->group_by('route_prices.SeatLkp');
		$query = $this->db->get();
 	  	if($query->num_rows >0){
  		$data = $query->row();
		$query -> free_result();
      	return $data->KeAmount;
 		 }
}
		function getreturndiscount($cur) 
{
 		$this->db->select("*");
 		$this->db->from("discounts");
		//$this->db->where('discounts.status !=',0);
		$this->db->where('discounts.disc_type_id',4);
		$query = $this->db->get();
 	  	if($query->num_rows >0){
                    $data = $query->row();
                    //$query -> free_result();
                    if ($data->activate==1)
                    {
                        if ($cur==1)
                        {
                            return $data->Disc_Amountke;
                        }
                        if ($cur==2)
                        {
                            return $data->Disc_Amountug;
                        }
                        if ($cur==3)
                        {
                            return $data->Disc_Amounttz;
                        }
                    }
                    else {
                        return 0;
                    }
                
 		 }
                 else {
                      return 0;
                 }
}
		function getBookedSeats($scheduleid) 
{
 		$this->db->select("*,GROUP_CONCAT(seats.seat_no) as booked");
 		$this->db->from("bus_schedule");
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		//$this->db->join("seatlkp", "buses.BusId = seatlkp.BusLkp",'left outer');
		$this->db->join("seats", "seats.BusTypeId= bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("ticketing", "ticketing.seatno = seats.seat_no ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("ticketing as tick", "bus_routes.RouteCode = tick.routecode",'inner');
		//$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		//$this->db->where('ticketing.fromtown',5);
		//$this->db->where('ticketing.totown',2);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Reserved');
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBookedSeats_t_f($scheduleid,$townf,$townt) 
{
 		$this->db->select("*,GROUP_CONCAT(ticketing.seatno) as booked");
 		$this->db->from("bus_schedule");
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		//$this->db->join("seatlkp", "buses.BusId = seatlkp.BusLkp",'left outer');
		//$this->db->join("seats", "seats.BusTypeId= bus_schedule.BusTypeLkp ",'inner');
		//$this->db->join("ticketing", "ticketing.seatno = seats.seat_no ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		//$this->db->join("ticketing as tick", "bus_routes.RouteCode = tick.routecode",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		//$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->where('ticketing.fromtown',$townf);
		$this->db->where('ticketing.totown',$townt);
		//$this->db->where('ticketing.fromtown',5);
		//$this->db->where('ticketing.totown',2);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Reserved');
                $this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->where('ticketing.fromtown',$townf);
		$this->db->where('ticketing.totown',$townt);
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getBookedSeatsrr($scheduleid) 
{
 		$this->db->select("*,GROUP_CONCAT(seats.seat_no) as booked");
 		$this->db->from("bus_schedule");
		$this->db->join("seats", "seats.BusTypeId= bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("ticketing", "ticketing.seatno = seats.seat_no ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("ticketing as tick", "bus_routes.RouteCode = tick.routecode",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Reserved');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row_array();
		//$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function getcorptickets($custid,$datef,$datet) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId ",'inner');
		$this->db->join("corp_transactions", "ticketing.TicketId = corp_transactions.ticket_id ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = bus_routes.To",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getBusbooking($scheduleid) 
{
                    /*
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = bus_routes.To",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('bus_schedule.ScheduleId',$scheduleid);
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
                 */
                 $this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->or_where('ticketing.ticketstatus','Reserved');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('ticketing.schedule_id',$scheduleid);
                $this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getbuscheduleclockedtimes($busscheduleid) 
{
                
                 $this->db->select("*");
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		//$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.ScheduleId',$busscheduleid);
                //$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function get_bus_and_schedules($busid,$datef,$datet) 
{
                
                 $this->db->select(" GROUP_CONCAT(bus_schedule.ScheduleId) AS buscheduleids");
                $this->db->from("buses");
		$this->db->join("bus_schedule", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->where('buses.status',1);
		$this->db->where('buses.BusId',$busid);
		$this->db->where('bus_schedule.DepartureDateTime >=',$datef);
		$this->db->where('bus_schedule.DepartureDateTime <=',$datet);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function search_tickets() 
{
                    
                    
 		$this->db->select("*");
                $this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
 		$this->db->from("bus_schedule");
		//$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
                if ($this->input->post('searchtype')=="ticket")
                {
		$this->db->where('ticketing.newticketno',$this->input->post('searchvalue'));
                }
                if ($this->input->post('searchtype')=="name")
                {
		$this->db->like('ticketing.clientname',$this->input->post('searchvalue'));
                }
                if ($this->input->post('searchtype')=="idno")
                {
		$this->db->where('ticketing.t_idno',$this->input->post('searchvalue'));
                }
                if ($this->input->post('searchtype')=="phone")
                {
		$this->db->where('ticketing.mobileno',$this->input->post('searchvalue'));
                }
                if ($this->input->post('searchtype')=="issuedby")
                {
		$this->db->like('ticketing.t_username',$this->input->post('searchvalue'));
                }
                if ($this->input->post('searchtype')=="voucher")
                {
		$this->db->where('ticketing.voucherno',$this->input->post('searchvalue'));
                }
		//$this->db->where('ticketing.ticketstatus','Active');
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getscheduletimes($routeid,$townid) 
{
 		$this->db->select("*");
 		$this->db->from("route_arrival_departure");
		$this->db->join("reporting_departure_times_lkp", "route_arrival_departure.DepartureTime = reporting_departure_times_lkp.TimeId ",'inner');
		$this->db->where('route_arrival_departure.status !=',0);
		$this->db->where('reporting_departure_times_lkp.status !=',0);
		$this->db->where('route_arrival_departure.RouteId',$routeid);
		$this->db->where('route_arrival_departure.TerminusId',$townid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getBusSchedule($scheduleid) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'left outer');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('bus_schedule.ScheduleId',$scheduleid);
     	$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
}
		function getScheduledbus($busid,$depdate,$deptime) 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.ScheduleId',$scheduleid);
		$this->db->where('buses.BusId',$busid);
		$this->db->where('bus_schedule.DepartureDateTime <',$depdate);
		$this->db->where('reporting_departure_times_lkp.ArrivalDeparture <',$deptime);
     	$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getStaff() 
{
 		$this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->join("job_title_lkp", "job_title_lkp.JobTitleId = bus_staff.StaffJobLkp",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = bus_staff.station_at",'inner');
		$this->db->where('bus_staff.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getdriversnotification() 
{
                $newdate = date('Y-m-d',strtotime(' + 15 day'));
 		$this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->join("job_title_lkp", "job_title_lkp.JobTitleId = bus_staff.StaffJobLkp",'inner');
		$this->db->where('bus_staff.status !=',0);
		$this->db->where('bus_staff.StaffJobLkp',1);
		$this->db->where('bus_staff.LicenceExpiry >=',date('Y-m-d'));
		$this->db->where('bus_staff.LicenceExpiry <=',$newdate);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getdriversnotificationexpired() 
{
 		$this->db->select("*");
 		$this->db->from("bus_staff");
		$this->db->join("job_title_lkp", "job_title_lkp.JobTitleId = bus_staff.StaffJobLkp",'inner');
		$this->db->where('bus_staff.status !=',0);
		$this->db->where('bus_staff.StaffJobLkp',1);
		$this->db->where('bus_staff.LicenceExpiry <',date('Y-m-d'));
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getcustomerpoints($CustomerId) 
{
 		$this->db->select("SUM(Points) as Point");
 		$this->db->from("customer_loyalty_points");
		//$this->db->join("job_title_lkp", "job_title_lkp.JobTitleId = bus_staff.StaffJobLkp",'inner');
		$this->db->where('customer_loyalty_points.status !=',0);
		$this->db->where('customer_loyalty_points.CustomerLkp',$CustomerId);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
		 else{return NULL;}
}
		function getpointsrates() 
{
 		$this->db->select("*");
 		$this->db->from("points_burn");
		$this->db->where('points_burn.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
		 else{return NULL;}
}

			function getBusTypes()
{
	  $query = $this->db->get('bustype_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getSeatTypes()
{
	  $query = $this->db->get('seats_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getSeasons()
{
      $this->db->where('season_lkp.status !=',0);
	  $query = $this->db->get('season_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getRegions()
{
	  $query = $this->db->get('regions_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
			function getReportDepartTimes()
{
	  $query = $this->db->get('reporting_departure_times_lkp');
 	  if($query->num_rows > 0){
  	  $data = $query->result();
	  $query -> free_result();
      return $data;
 		 }
}
          //get result from two tables joined by a similar primary key with a field for where clause
          function getwojoinedtbrowswithwhereclause($table1,$table2,$joinerfield,$wherefield,$wherefieldvalue)
          {
              
                $query = $this->db->select('*')->from($table1)->join($table2, $table1.'.'.$joinerfield.' = '.$table2.'.'.$joinerfield)->where($table1.'.status !=',0)->where($table2.'.status !=',0)->where($table1.".".$wherefield,$wherefieldvalue)->get();
                if ($query->num_rows() ==1)
                {return $query->row();}
                else 
                {return NULL;}
          }   //get result from two tables joined by a similar primary key with a field for where clause
          function getwojoinedtbtotrowswithwhereclause($table1,$table2,$joinerfield,$wherefield,$wherefieldvalue)
          {
              
                $query = $this->db->select('*')->from($table1)->join($table2, $table1.'.'.$joinerfield.' = '.$table2.'.'.$joinerfield)->where($table1.'.status !=',0)->where($table2.'.status !=',0)->where($table1.".".$wherefield,$wherefieldvalue)->get();
               
                return $query->num_rows();
          }
		function getBusHire()
{

		$this->db->select("*");
 		$this->db->from("bus_hire");
		$this->db->join("buses", "buses.BusId = bus_hire.BusId",'inner');
		$this->db->join("hirers", "hirers.HirerId = bus_hire.HirerId",'inner');
		$this->db->where('buses.status !=',0);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getmessages()
{

		$this->db->select("*");
 		$this->db->from("messages");
		$this->db->join("message_recipients", "messages.m_id = message_recipients.MR_MID",'inner');
		//$this->db->join("hirers", "hirers.HirerId = bus_hire.HirerId",'inner');
                $this->db->where('message_recipients.MR_RID',  $this->flexi_auth->get_user_id());
                $this->db->order_by('m_date','desc');
                $this->db->order_by('m_time','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
 else { return NULL;}
}
		function getmessagesnotice()
{

		$this->db->select("*");
 		$this->db->from("messages");
		$this->db->join("message_recipients", "messages.m_id = message_recipients.MR_MID",'inner');
		//$this->db->join("hirers", "hirers.HirerId = bus_hire.HirerId",'inner');
                $this->db->where('message_recipients.MR_RID',  $this->flexi_auth->get_user_id());
                $this->db->where('messages.m_isread',0);
                $this->db->order_by('m_date','desc');
                $this->db->order_by('m_time','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
 else { return NULL;}
}
		function getmessagessent()
{

		$this->db->select("*");
 		$this->db->from("messages");
		//$this->db->join("buses", "buses.BusId = bus_hire.BusId",'inner');
		//$this->db->join("hirers", "hirers.HirerId = bus_hire.HirerId",'inner');
                $this->db->where('m_senderid',  $this->flexi_auth->get_user_id());
                $this->db->order_by('m_date','desc');
                $this->db->order_by('m_time','desc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else { return NULL;}
}
		function getspecmessage($mid)
{

		$this->db->select("*");
 		$this->db->from("messages");
		//$this->db->join("buses", "buses.BusId = bus_hire.BusId",'inner');
		//$this->db->join("hirers", "hirers.HirerId = bus_hire.HirerId",'inner');
                //$this->db->where('m_recipientid',  $this->flexi_auth->get_user_id());
                $this->db->where('m_id',$mid);
		$query = $this->db->get();
 	  	if($query->num_rows == 1){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
		 else{return NULL;}
}
	
	function message_is_read($mid)
	{
	    $data = array(
		       'm_isread' => "1"
            );
		$this->db->where('m_id',$mid);	
        $this->db->update('messages', $data);
   }	
   
        
        function add_message_SaveForm($message_form_data)
	{
		$this->db->insert('messages', $message_form_data);
             $query = $this->db->query('SELECT LAST_INSERT_ID() AS m_id From messages ORDER BY m_id ');
                if ($query->num_rows() > 0)
		{return $query->last_row();}
		else 
		{return NULL;}
		
	}	
        function add_messagerec_SaveForm($messagerec_form_data)
	{
		$this->db->insert('message_recipients', $messagerec_form_data);
	}	
	
	function savebusseats($busid) {
$count = sizeof($this->input->post('seatposition', TRUE));
$seatnumber = $this->input->post('seatnumber', TRUE);
$seatposition = $this->input->post('seatposition', TRUE);
$seatclass = $this->input->post('seatclass', TRUE);
//print_r($seatposition);
for ($i=0;$i<$count;$i++) {
  $data = array(
    'BusId'      => $busid,
    'seat_no'      => $seatnumber[$i],
    'SeatPositionLkp'  => $seatposition[$i],
	'SeatTypeId'  => $seatclass[$i]
  );
$this->db->where('BusId', $busid);
$this->db->where('seat_no', $seatnumber[$i]);
$this->db->from('seats');
$count = $this->db->count_all_results();
if($count == 0):
$insert=$this->db->insert('seats', $data);
else:
return false;
endif;
  }
  return $insert;
}
	function savetowns() {
$count = sizeof($this->input->post('town', TRUE));
$town = $this->input->post('town', TRUE);
$towncode = $this->input->post('towncode', TRUE);
$stowncode = $this->input->post('stowncode', TRUE);
$currency = $this->input->post('currency', TRUE);
//print_r($seatposition);
for ($i=0;$i<$count;$i++) {
  $data = array(
    'TownName'=> $town[$i],
    'TownCode'=> strtoupper($towncode[$i]),
    'TownSCode'=> strtoupper($stowncode[$i]),
    'Currency'=> strtoupper($currency[$i]),
  );
$this->db->insert('towns_lkp', $data);
  }
  //return $insert;
}

		function getBusSeats($busid,$seattype)
{
		$this->db->select("*");
 		$this->db->from("seats");
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId = seats.SeatTypeId",'inner');
		$this->db->join("seat_position_lkp", "seat_position_lkp.SeatPositionLkp = seats.SeatPositionLkp",'inner');
		$this->db->where('seats.status !=',0);
		$this->db->where('seats.BusId',$busid);
		$this->db->where('seats.SeatTypeId',$seattype);
		//$this->db->order_by('seats.seat_no','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}

		function gettickettoprint($ticketid) 
{
 		$this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		//$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.TicketId',$ticketid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

		function gettickettoprint_device($ticketno) 
{
 		$this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.newticketno',$ticketno);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

		function getticketstochart($from,$to,$scheduleid) 
{
 		$this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('ticketing.fromtown',$from);
		$this->db->where('ticketing.totown',$to);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function gettickettochart($seat,$from,$to,$scheduleid) 
{
 		$this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('ticketing.seatno',$seat);
		$this->db->where('ticketing.fromtown',$from);
		$this->db->where('ticketing.totown',$to);
		$this->db->where('ticketing.schedule_id',$scheduleid);
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

function getticketsbyschedule($schid) 
{
 		$this->db->select("*");
		//$this->db->select("towns_lkp.*",FALSE);
		//$this->db->select("bus_schedule.*",FALSE);
		//$this->db->select("bus_routes.*",FALSE);
		//$this->db->select("ticketing.*",FALSE);
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_schedule");
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("ticketing", "ticketing.routecode = bus_routes.RouteCode",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$schid);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
                $this->db->where('bus_schedule.status !=',0);
		//$this->db->where('buses.status !=',0);
		//$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.schedule_id',$schid);
		$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

		function gettownsinroutes($routeid,$townid) 
{
		$this->db->select("*, reporting_departure_times_lkp.ArrivalDeparture as departuretime");
		//$this->db->select("route_arrival_departure.*",FALSE);
		//$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		//$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_routes");
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		//$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('route_arrival_departure.RouteId',$routeid);
		$this->db->where('route_arrival_departure.TerminusId',$townid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}

		function gettownsinroutes_first($routeid) 
{
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select('da.ArrivalDeparture as departuretime', FALSE);
                $this->db->from("bus_routes");
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->join("reporting_departure_times_lkp da", "da.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('route_arrival_departure.RouteId',$routeid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->first_row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }
}
		function getbusesforscheduling($ticketid) 
{
 		$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		$this->db->select("buses.*",FALSE);
		$this->db->from("bus_schedule");
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("ticketing", "ticketing.reg_no = buses.RegistrationNumber ",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('buses.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.TicketId',$ticketid);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
//CAST(ticketing.transdate AS DATE)
		function gettotalcollectionperstation($stationid,$date) 
{
 		$this->db->select("*,SUM(ticketing.amount) AS total",FALSE);
		$this->db->from("ticketing");
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->group_by('ticketing.t_station');
		if ($date<>'') {
		 $this->db->where('date(ticketing.transdate)',$date);
		 }
		 if ($stationid<>'') {
		 $this->db->where('ticketing.t_station',$stationid);
		 }
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data->total;
 		 }
}
		function gettotalpettyperstation($stationid,$date) 
{
 		$this->db->select("*,SUM(petty_cash.KeAmount) AS total",FALSE);
		$this->db->from("petty_cash");
		$this->db->where('petty_cash.status !=',0);
		$this->db->group_by('petty_cash.SubstationId');
		if ($date<>'') {
		 $this->db->where('date(petty_cash.Cashdate)',$date);
		 }
		 if ($stationid<>'') {
		 $this->db->where('petty_cash.SubstationId',$stationid);
		 }
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data->total;
 		 }
}
		function get_daily_expense($stationid,$date) 
{
 		$this->db->select("*,SUM(petty_cash.KeAmount) AS total",FALSE);
		$this->db->from("petty_cash");
		$this->db->where('petty_cash.status !=',0);
		$this->db->group_by('petty_cash.SubstationId');
		if ($date<>'') {
		 $this->db->where('date(petty_cash.Cashdate)',$date);
		 }
		 if ($stationid<>'') {
		 $this->db->where('petty_cash.SubstationId',$stationid);
		 }
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data->total;
 		 }
}
		function getallpayments($busid,$date) 
{
 		$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		$this->db->select("buses.*",FALSE);
		$this->db->select("seats.*",FALSE);
		$this->db->select("seats_lkp.*",FALSE);
		$this->db->from("ticketing");
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		$this->db->join("bus_schedule", "bus_schedule.BusLkp= buses.BusId",'inner');
		$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->group_by('ticketing.TicketId');
		if ($date<>'') {
		 $this->db->where('date(ticketing.transdate)',$date);
		 }
		 if ($busid<>'') {
		 $this->db->where('ticketing.reg_no',$busid);
		 }
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
}
		function getreservedseats() 
{
 		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->join("buses", "buses.BusId = bus_schedule.BusLkp",'inner');
		$this->db->join("seats", "seats.BusId= buses.BusId ",'inner');
		$this->db->join("ticketing", "ticketing.seatno = seats.seat_no ",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "bus_routes.RouteId = route_arrival_departure.RouteId",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('buses.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('bus_schedule.IsActive',1);
		$this->db->where('ticketing.ticketstatus','Reserved');
                $this->db->where('DATE_FORMAT(bus_schedule.DepartureDateTime, "%Y-%m-%d")',date('Y-m-d'));
                $this->db->where('hour(bus_schedule.DepartureDateTime) - 2',date('H'));
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                      return NULL;
                 }
}
		function check_if_reserved_seat_notification_has_not_been_sent($ticketid) 
{
 		$this->db->select("*");
 		$this->db->from("ticketing");
		$this->db->where('ticketing.TicketId',$ticketid);
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		
		$query -> free_result();
      	return FALSE;
 		 }
                 else {
                     return TRUE;
                 }
   }
   
   function insert_sms($smslength,$noofsms,$phoneno,$smsfunction)
	{
	  $dateday = date("Y-m-d");
	    $data = array(
               'SMS_Length' => $smslength,
               'SMS_NO_Sent' => $noofsms,
               'SMS_Recipient' => $phoneno,
               'SMS_Function' => $smsfunction,
               'SMS_Date' => $dateday
            );
        $this->db->insert('sms', $data);
   }
   
   function getdiscountedseats($branchid,$ndate)
   {
       $day = date('l',strtotime($ndate));
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        
   $this->db->select("*");
 		$this->db->from("discounts");
		$this->db->where('discounts.status !=',0);
		$this->db->where('discounts.disc_type_id !=',2);
		$this->db->where('discounts.disc_type_id !=',3);
		$this->db->where('discounts.If_Other_Discount_Active',1);
                
                if ($activeseason<>NULL):
		$this->db->where('discounts.If_Other_Season_Active',1);
                endif;
                
		$this->db->where('discounts.Disc_From_Date <=',$ndate);
		$this->db->where('discounts.Disc_To_Date >=',$ndate);
		//$this->db->like('discounts.branch_values ',$branchid);
                $this->db->where('FIND_IN_SET('.$branchid.', `branch_values`) ');
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
		}
                else {
                    return NULL;
                }
   		   
   }
   
   
   function getdaydiscountedseats($branchid,$ndate)
   {
       $day = date('l',strtotime($ndate));
                        $activeseason = $this->getonerowfromonetbl('season_lkp', 'IsActive', 1);
                        
   $this->db->select("*");
 		$this->db->from("discounts");
		$this->db->where('discounts.status !=',0);
		$this->db->where('discounts.disc_type_id !=',2);
		$this->db->where('discounts.disc_type_id !=',3);
		$this->db->where('discounts.If_Other_Discount_Active',1);
                
                if ($activeseason<>NULL):
		$this->db->where('discounts.If_Other_Season_Active',1);
                endif;
                
		$this->db->where('discounts.Disc_From_Date <=',$ndate);
		$this->db->where('discounts.Disc_To_Date >=',$ndate);
		$this->db->like('discounts.branch_values ',$branchid);
                $this->db->where('FIND_IN_SET('.$branchid.', `branch_values`) ');
//		$this->db->group_by('route_arrival_departure.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
		}
                else {
                    return NULL;
                }
   		   
   }
   
   function check_manifest_created($scheduleid,$townf,$townt)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("*");
		$this->db->from("manifest");
		$this->db->where('manifest.status !=',0);
		$this->db->where('manifest.ScheduleId',$scheduleid);
		$this->db->where('manifest.townf',$townf);
		$this->db->where('manifest.townt',$townt);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		return $query->row();
		//$query -> free_result();
      	
   }
        else {
            return NULL;
        }
   }
   
   function check_manifest_created_without_scheduleid($townf,$townt)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("*");
		$this->db->from("manifest");
		$this->db->where('manifest.status !=',0);
		$this->db->where('manifest.townf',$townf);
		$this->db->where('manifest.townt',$townt);
		$this->db->order_by('manifest.manifest_id','desc');
		$this->db->limit(1);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function getlastmanifestno($scheduleid,$townf,$townt)
   {
       $mancheck = $this->check_manifest_created($scheduleid,$townf,$townt);
       if ($mancheck==NULL)
       {
       $tf = $this->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
       $tt = $this->getonerowfromonetbl('towns_lkp', 'TownId', $townt);
       $oldnum = $this->check_manifest_created_without_scheduleid($townf,$townt);
       if ($oldnum<>NULL)
       {
           $newnum = $oldnum->mnum + 1;
       }
       else {
           $newnum = 1;
       }
       
       $manifestno = $tf->TownSCode."-".$tt->TownSCode."-".$newnum;
       $insdata = array('manifest_no'=>$manifestno,'ScheduleId'=>$scheduleid,'townf'=>$townf,'townt'=>$townt,'created_by'=>$this->flexi_auth->get_user_id(),'mnum'=>$newnum);
       $this->db->insert('manifest',$insdata);
       $data = $this->db->insert_id();
       //$data = $manifestno;
       }
       else {
           $data = $mancheck->manifest_id;
       }
       return $data;
       
   }
   
   function gen_manifest_tickets($scheduleid)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		$this->db->select("seats.*",FALSE);
		$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
               
		$this->db->where('ticketing.schedule_id',$scheduleid);
               
		$this->db->group_by('ticketing.TicketId');
                
		$this->db->order_by('ticketing.seatno','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function gen_clerk_tickets_today($clerk)
   {
       $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		$this->db->select("seats.*",FALSE);
		$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
               
		$this->db->where('ticketing.t_userid',$clerk);
               
		$this->db->where('ticketing.bookdate',date('Y-m-d'));
		$this->db->where('ticketing.specialstatus','NORMAL');
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Open');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.specialstatus','NORMAL');
               
		$this->db->where('ticketing.cancelled_by',$username);
               
		$this->db->where('ticketing.cancel_date',date('Y-m-d'));
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.specialstatus','NORMAL');
               
		$this->db->where('ticketing.voucheredeemedby',$clerk);
               
		$this->db->where('DATE(ticketing.voucheredeemedate)',date('Y-m-d'));
                
               
		$this->db->group_by('ticketing.TicketId');
                
		$this->db->order_by('ticketing.seatno','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function gen_clerk_tickets_today_special($clerk)
   {
       $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		$this->db->select("seats.*",FALSE);
		$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
               
		$this->db->where('ticketing.t_userid',$clerk);
               
		//$this->db->where('ticketing.t_userid',$clerk);
               
		$this->db->where('ticketing.bookdate',date('Y-m-d'));
		$this->db->where('ticketing.specialstatus','SPECIAL');
		$this->db->where('ticketing.ticketstatus','Active');
		$this->db->or_where('ticketing.ticketstatus','Open');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.specialstatus','SPECIAL');
               
		$this->db->where('ticketing.cancelled_by',$username);
               
		$this->db->where('ticketing.cancel_date',date('Y-m-d'));
		$this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.specialstatus','SPECIAL');
               
		$this->db->where('ticketing.voucheredeemedby',$clerk);
               
		$this->db->where('DATE(ticketing.voucheredeemedate)',date('Y-m-d'));
               
		$this->db->group_by('ticketing.TicketId');
                
		$this->db->order_by('ticketing.seatno','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function gen_clerk_open_tickets_today($clerk)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		$this->db->select("seats.*",FALSE);
		$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
               
		$this->db->where('ticketing.t_userid',$clerk);
               
		$this->db->where('ticketing.bookdate',date('Y-m-d'));
               
		$this->db->group_by('ticketing.TicketId');
                
		$this->db->order_by('ticketing.seatno','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_corp_ticket_sales($clerk,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
       //$clerk=0;
		$this->db->select("SUM(amount) as amountke, SUM(amountug) as amountug, SUM(amounttz) as amounttz, SUM(amountusd) as amountusd, SUM(amounteuro) as amounteuro");
		$this->db->from("ticketing");
		$this->db->join("user_accounts", "user_accounts.uacc_id = ticketing.t_userid",'inner');
		$this->db->join("cust_users", "cust_users.usermail = user_accounts.uacc_email",'inner');
		$this->db->join("customer", "customer.Cust_id = cust_users.cust_id",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
                
                if (isset($clerk) and $clerk<>0):
		$this->db->where('customer.Cust_id',$clerk);
                endif;
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		//$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_bus_ticket_sales($bus,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
       //$clerk=0;
		$this->db->select("SUM(amount) as amountke, SUM(amountug) as amountug, SUM(amounttz) as amounttz, SUM(amountusd) as amountusd, SUM(amounteuro) as amounteuro");
		$this->db->from("ticketing");
		$this->db->join("bus_schedule", "bus_schedule.ScheduleId = ticketing.schedule_id",'inner');
		$this->db->join("buses", "bus_schedule.BusLkp = buses.BusId",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
                
                if (isset($bus) and $bus<>0):
		$this->db->where('buses.BusId',$bus);
                endif;
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		//$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(amount) as amountke, SUM(amountug) as amountug, SUM(amounttz) as amounttz, SUM(amountusd) as amountusd, SUM(amounteuro) as amounteuro");
		$this->db->from("ticketing");
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
                if (isset($townf) and $townf<>0):
		$this->db->where('ticketing.fromtown',$townf);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('ticketing.pick_point',$terminus);
                endif;
                if (isset($clerk) and $clerk<>0):
		$this->db->where('ticketing.t_userid',$clerk);
                endif;
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		//$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       return NULL;
   }
   }
   
   function get_total_ticket_sales_clerk($townf,$terminus,$clerk,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(amount) as amountke, SUM(amountug) as amountug, SUM(amounttz) as amounttz, SUM(amountusd) as amountusd, SUM(amounteuro) as amounteuro");
		$this->db->from("ticketing");
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
                if (isset($townf) and $townf<>0):
		$this->db->where('ticketing.fromtown',$townf);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('ticketing.pick_point',$terminus);
                endif;
                if (isset($clerk) and $clerk<>0):
		$this->db->where('ticketing.t_userid',$clerk);
                endif;
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		//$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       return NULL;
   }
   }
   
   function get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(KeAmount) as amountke, SUM(UgAmount) as amountug, SUM(TzAmount) as amounttz, SUM(UsdAmount) as amountusd, SUM(EurAmount) as amounteuro");
		$this->db->from("petty_cash");
		$this->db->where('petty_cash.status !=',0);
                if (isset($townf) and $townf<>0):
		$this->db->where('petty_cash.StationId',$townf);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('petty_cash.SubstationId',$terminus);
                endif;
                if (isset($clerk) and $clerk<>0):
		$this->db->where('petty_cash.CashierId',$clerk);
                endif;
		$this->db->where('petty_cash.CashDate >=',$datef);
		$this->db->where('petty_cash.CashDate <=',$datet);
		//$this->db->group_by('ticketing.TicketId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_petty_cash_daily($datef)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(KeAmount) as amountke, SUM(UgAmount) as amountug, SUM(TzAmount) as amounttz, SUM(UsdAmount) as amountusd, SUM(EurAmount) as amounteuro, TownName");
		$this->db->from("towns_lkp");
		$this->db->join("petty_cash", "petty_cash.StationId = towns_lkp.TownId",'left outer');
		$this->db->where('petty_cash.status !=',0);
		$this->db->where('petty_cash.CashDate',$datef);
		$this->db->group_by('petty_cash.StationId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_voucher_daily($datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(amount) as amountke, SUM(amountug) as amountug, SUM(amounttz) as amounttz, SUM(amountusd) as amountusd, SUM(amounteuro) as amounteuro, TownName");
		$this->db->from("towns_lkp");
		$this->db->join("ticketing", "ticketing.fromtown = towns_lkp.TownId",'left outer');
		$this->db->where('ticketing.status !=',0);
                $this->db->where('ticketing.ticketstatus','Open');
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
                $this->db->or_where('ticketing.ticketstatus','Redeemed');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.bookdate >=',$datef);
		$this->db->where('ticketing.bookdate <=',$datet);
		$this->db->group_by('towns_lkp.TownId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_banked($datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("SUM(TotalAmount) as amountke, TerminusName");
		$this->db->from("banking");
		$this->db->join("bus_termini", "banking.TerminusId = bus_termini.TerminusId",'left outer');
		$this->db->where('banking.status !=',0);
		$this->db->where('banking.BankingDate >=',$datef.' 00:00:00');
		$this->db->where('banking.BankingDate <=',$datet.' 23:59:59');
		$this->db->group_by('banking.TerminusId');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function get_total_banked_amount($townf,$terminus,$datef,$datet,$cur)
   {
		$this->db->select("SUM(TotalAmount) as amountcur");
		$this->db->from("banking");
		$this->db->join("bus_termini", "banking.TerminusId = bus_termini.TerminusId",'inner');
		$this->db->where('banking.status !=',0);
		$this->db->where('bus_termini.status !=',0);
		$this->db->where('banking.BCurrency',$cur);
                if (isset($townf) and $townf<>0):
		$this->db->where('bus_termini.TownLkp',$townf);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('banking.TerminusId',$terminus);
                endif;
		$this->db->where('DATE(banking.BankingDate) >=',$datef);
		$this->db->where('DATE(banking.BankingDate) <=',$datet);
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function gen_report($townf,$townt,$channel,$terminus,$clerk,$bustypelkp,$tickettype,$paymode,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		//$this->db->select("seats.*",FALSE);
		//$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		//$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		//$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.specialstatus',"NORMAL");
                if ($tickettype<>"All"):
		$this->db->where('ticketing.ticketstatus',$tickettype);
                endif;
                if (isset($townf) and $townf<>0):
		$this->db->where('ticketing.fromtown',$townf);
                endif;
                if (isset($townt) and $townt<>0):
		$this->db->where('ticketing.totown',$townt);
                endif;
                if (isset($channel) and $channel<>0):
		$this->db->where('ticketing.channel',$channel);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('ticketing.pick_point',$terminus);
                endif;
                if (isset($clerk) and $clerk<>0):
		$this->db->where('ticketing.t_userid',$clerk);
                endif;
                if (isset($bustypelkp) and $bustypelkp<>0):
		$this->db->where('ticketing.bus_type',$bustypelkp);
                endif;
                if (isset($paymode) and $paymode<>0):
		$this->db->where('ticketing.paymode',$paymode);
                endif;
                //if (isset($datef) and $datef<>0):
		$this->db->where('ticketing.bookdate >=',$datef);
                //endif;
                //if (isset($datet) and $datet<>0):
		$this->db->where('ticketing.bookdate <=',$datet);
                //endif;
                //if ((!isset($datef) and !isset($datet)) || ($datef=="" and $datet=="") || (empty($datef) and empty($datet))):
                //$this->db->where('ticketing.transdate >=',date('Y-m-d H:i'));
                //$this->db->where('ticketing.transdate <=',date('Y-m-d H:i'));
                //endif;
		$this->db->group_by('ticketing.TicketId');
                if (isset($_POST['seatsdesc'])):
		$this->db->order_by('ticketing.seatno','desc');
                endif;
                if (isset($_POST['seatsasc'])):
		$this->db->order_by('ticketing.seatno','asc');
                endif;
                if (isset($_POST['datedesc'])):
		$this->db->order_by('ticketing.bookdate','desc');
                endif;
                if (isset($_POST['dateasc'])):
		$this->db->order_by('ticketing.bookdate','asc');
                endif;
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function clerk_report()
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("CAST(ticketing.transdate AS DATE) AS dateonly",FALSE);
		$this->db->select("bus_schedule.*",FALSE);
		$this->db->select("bus_routes.*",FALSE);
		$this->db->select("ticketing.*",FALSE);
		$this->db->select("route_arrival_departure.*",FALSE);
		$this->db->select("reporting_departure_times_lkp.*",FALSE);
		//$this->db->select("buses.*",FALSE);
		$this->db->select("bustype_lkp.*",FALSE);
		//$this->db->select("seats.*",FALSE);
		//$this->db->select("seats_lkp.*",FALSE);
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->select("paymodes.*",FALSE);
		$this->db->from("ticketing");
		//$this->db->join("buses", "buses.RegistrationNumber = ticketing.reg_no",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_schedule", "bus_schedule.ScheduleId = ticketing.schedule_id",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		//$this->db->where('ticketing.specialstatus',"NORMAL");
                
		//$this->db->where('ticketing.ticketstatus',$tickettype);
		$this->db->where('ticketing.t_userid',$this->flexi_auth->get_user_id());
		$this->db->where('ticketing.bookdate',date('Y-m-d'));
		$this->db->group_by('ticketing.TicketId');
		$this->db->order_by('ticketing.seatno','asc');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->result();
		//$query -> free_result();
      	return $data;
   }
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function gen_report_sum_kes($townf,$townt,$channel,$terminus,$clerk,$bustypelkp,$paymode,$datef,$datet)
   {
       //$this->db->select("towns_lkp.*",FALSE);
		$this->db->select("*, SUM(amount) as kesamount");
		$this->db->select('fto.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
		$this->db->select('ra.ArrivalDeparture as reportingtime', FALSE);
		$this->db->from("ticketing");
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = ticketing.bus_type",'inner');
		$this->db->join("paymodes", "paymodes.paymodes_id = ticketing.paymode",'inner');
		$this->db->join("seats", "seats.seat_no= ticketing.seatno",'inner');
		$this->db->join("seats_lkp", "seats_lkp.SeatTypeId= seats.SeatTypeId",'inner');
		//$this->db->join("towns_lkp", "towns_lkp.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp fto", "fto.TownId = ticketing.fromtown",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = ticketing.totown",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteCode = ticketing.routecode",'inner');
		$this->db->join("bus_schedule", "bus_schedule.RouteLkp = bus_routes.RouteId",'inner');
		$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
		$this->db->join("reporting_departure_times_lkp", "reporting_departure_times_lkp.TimeId = route_arrival_departure.DepartureTime",'inner');
		$this->db->join("reporting_departure_times_lkp ra", "ra.TimeId = route_arrival_departure.ArrivalTime",'inner');
		$this->db->where('ticketing.status !=',0);
		$this->db->where('ticketing.ticketstatus','Active');
                if (isset($townf) and $townf<>0):
		$this->db->where('ticketing.fromtown',$townf);
                endif;
                if (isset($townt) and $townt<>0):
		$this->db->where('ticketing.totown',$townt);
                endif;
                if (isset($channel) and $channel<>0):
		$this->db->where('ticketing.channel',$channel);
                endif;
                if (isset($terminus) and $terminus<>0):
		$this->db->where('ticketing.pick_point',$terminus);
                endif;
                if (isset($clerk) and $clerk<>0):
		$this->db->where('ticketing.t_userid',$clerk);
                endif;
                if (isset($bustypelkp) and $bustypelkp<>0):
		$this->db->where('ticketing.bus_type',$bustypelkp);
                endif;
                if (isset($paymode) and $paymode<>0):
		$this->db->where('ticketing.paymode',$paymode);
                endif;
                if (isset($datef) and $datef<>0):
		$this->db->where('ticketing.transdate >=',$datef);
                endif;
                if (isset($datet) and $datet<>0):
		$this->db->where('ticketing.transdate <=',$datet);
                endif;
                //if ((!isset($datef) and !isset($datet)) || ($datef=="" and $datet=="") || (empty($datef) and empty($datet))):
                //$this->db->where('ticketing.transdate >=',date('Y-m-d H:i'));
                //$this->db->where('ticketing.transdate <=',date('Y-m-d H:i'));
                //endif;
                $this->db->where('ticketing.t_currency',1);
		$this->db->group_by('ticketing.TicketId');
                if (isset($_POST['seatsdesc'])):
		$this->db->order_by('ticketing.seatno','desc');
                endif;
                if (isset($_POST['seatsasc'])):
		$this->db->order_by('ticketing.seatno','asc');
                endif;
                if (isset($_POST['datedesc'])):
		$this->db->order_by('ticketing.transdate','desc');
                endif;
                if (isset($_POST['dateasc'])):
		$this->db->order_by('ticketing.transdate','asc');
                endif;
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data1 = $query->row();
                $kesamount = $data1->kesamount;
		$query -> free_result();
      	return $kesamount;
   }
   
   else {
       $data=NULL;
       return $data;
   }
   }
   
   function getlastinsertedticketfromtown($ftowncode,$specialstat)
   {
       $this->db->select("*");
		$this->db->from("ticketing");
                //$this->db->where('ticketing.fromtown',$ftownid);
                $this->db->where('ticketing.specialstatus',$specialstat);
                //$this->db->where('MATCH (ticketing.newticketno) AGAINST ("value")', NULL, FALSE);
                $this->db->like('ticketing.newticketno',$ftowncode);
                $this->db->order_by('ticketing.TicketId','asc');
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->last_row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getlastinsertedticketfromtown_original($ftownid,$specialstat)
   {
       $this->db->select("*");
		$this->db->from("ticketing");
                $this->db->where('ticketing.fromtown',$ftownid);
                $this->db->where('ticketing.specialstatus',$specialstat);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->last_row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getcountries()
   {
        $this->db->select("*");
		$this->db->from("countrylist");
                $this->db->where('countrylist.status',1);
                $this->db->order_by('countrylist.ID','asc');
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getnormaldiscounts()
   {
        $this->db->select("*");
		$this->db->from("discounts");
                $this->db->where('discounts.status',1);
                $this->db->where('discounts.disc_type_id',1);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getdaydiscounts()
   {
        $this->db->select("*");
		$this->db->from("discounts");
                $this->db->where('discounts.status',1);
                $this->db->where('discounts.disc_type_id',2);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function gettimediscounts()
   {
        $this->db->select("*");
		$this->db->from("discounts");
                $this->db->where('discounts.status',1);
                $this->db->where('discounts.disc_type_id',3);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getreturndiscounts()
   {
        $this->db->select("*");
		$this->db->from("discounts");
                $this->db->where('discounts.status',1);
                $this->db->where('discounts.disc_type_id',4);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function gettimes()
   {
        $this->db->select("*");
		$this->db->from("reporting_departure_times_lkp");
                $this->db->where('reporting_departure_times_lkp.status',1);
                $this->db->group_by('reporting_departure_times_lkp.ArrivalDeparture');
                $this->db->order_by('reporting_departure_times_lkp.ArrivalDeparture','asc');
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getdeptimebytown($routeid,$townid)
   {
        $this->db->select("*");
		$this->db->from("reporting_departure_times_lkp");
		$this->db->join("route_arrival_departure", "route_arrival_departure.DepartureTime = reporting_departure_times_lkp.TimeId",'inner');
                $this->db->where('reporting_departure_times_lkp.status',1);
                $this->db->where('route_arrival_departure.RouteId',$routeid);
                $this->db->where('route_arrival_departure.TerminusId',$townid);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function getdeptime($routeid)
   {
        $this->db->select("*");
		$this->db->from("reporting_departure_times_lkp");
		$this->db->join("route_arrival_departure", "route_arrival_departure.DepartureTime = reporting_departure_times_lkp.TimeId",'inner');
                $this->db->where('reporting_departure_times_lkp.status',1);
                $this->db->where('route_arrival_departure.RouteId',$routeid);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->first_row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_customer_total_deposit($custid)
   {
        $this->db->select("SUM(camount) as camount");
		$this->db->from("corp_transactions");
                $this->db->where('corp_transactions.status',1);
                $this->db->where('corp_transactions.cust_id',$custid);
                $this->db->where('corp_transactions.approvedstatus',1);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_customer_total_debited($custid)
   {
        $this->db->select("SUM(camount) as camountdeb");
		$this->db->from("corp_transactions");
                $this->db->where('corp_transactions.status',1);
                $this->db->where('corp_transactions.cust_id',$custid);
                $this->db->where('corp_transactions.approvedstatus',1);
                $this->db->where('corp_transactions.camount >=',0);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_customer_total_credited($custid)
   {
        $this->db->select("SUM(camount) as camountcred");
		$this->db->from("corp_transactions");
                $this->db->where('corp_transactions.status',1);
                $this->db->where('corp_transactions.cust_id',$custid);
                $this->db->where('corp_transactions.camount <',0);
                $this->db->where('corp_transactions.approvedstatus',1);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_customer_credited($custid,$datef,$datet)
   {
       $ndatef = $datef.' 00:00:00';
       $ndatet = $datet.' 23:59:59';
        $this->db->select("*");
		$this->db->from("corp_transactions");
                $this->db->where('corp_transactions.status',1);
                $this->db->where('corp_transactions.cust_id',$custid);
                $this->db->where('corp_transactions.camount <',0);
                $this->db->where('corp_transactions.dateadded >=',$ndatef);
                $this->db->where('corp_transactions.dateadded <=',$ndatet);
                //$this->db->where('corp_transactions.approvedstatus',1);
                $this->db->order_by('corp_transactions.dateadded','desc');
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_mpesa_trans_det($mpesatransno)
   {
        $this->db->select("*");
		$this->db->from("mpesa_trans");
                $this->db->where('mpesa_trans.status',1);
                $this->db->where('mpesa_trans.empTras_id',$mpesatransno);
                $this->db->where('mpesa_trans.trans_status',0);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function is_mpesatransno_valid($mpesatransno)
   {
        $this->db->select("*");
		$this->db->from("mpesa_trans");
                $this->db->where('mpesa_trans.status',1);
                $this->db->where('mpesa_trans.empTras_id',$mpesatransno);
                $this->db->where('mpesa_trans.trans_status',0);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = TRUE;
		$query -> free_result();
      	return $data1;
                }
                else {
                    return FALSE;
                }
   }
   
   function is_pettycash_available($currency,$amount)
   {
                if ($currency==1)
                {
                    $this->db->select("SUM(amount) totamount");
                }
                if ($currency==2)
                {
                    $this->db->select("SUM(amountug) totamount");
                }
                if ($currency==3)
                {
                    $this->db->select("SUM(amounttz) totamount");
                }
		$this->db->from("ticketing");
                $this->db->where('ticketing.status',1);
                $this->db->where('ticketing.t_userid',$this->flexi_auth->get_user_id());
                $this->db->where('ticketing.ticketstatus','Active');
                $this->db->where('ticketing.bookdate',date('Y-m-d'));
                $query = $this->db->get();
                if($query->num_rows > 0){
                    //$amntin = $query->row()->totamount;
                    
                    if ($currency==1)
                    {
                        $this->db->select("SUM(KeAmount) ptotamount");
                    }
                    if ($currency==2)
                    {
                        $this->db->select("SUM(UgAmount) ptotamount");
                    }
                    if ($currency==3)
                    {
                        $this->db->select("SUM(TzAmount) ptotamount");
                    }
                    $this->db->from("petty_cash");
                    $this->db->where('petty_cash.status',1);
                    $this->db->where('petty_cash.CashierId',$this->flexi_auth->get_user_id());
                    $this->db->where('petty_cash.CashDate',date('Y-m-d'));
                    $query2 = $this->db->get();
                    if ($query2->num_rows()==0)
                    {
                        $petycashgiven = 0;
                    }
                    else {
                        $petycashgiven = $query2->row()->ptotamount;
                    }
                    $amntin = $query->row()->totamount - $petycashgiven;
                    if ($amntin>=$amount)
                    {
                        return TRUE;
                    }
                    else {
                        return FALSE;
                    }
  		
                }
                else {
                    return FALSE;
                }
   }
   
   function get_town_routes($townid)
   {
        $this->db->select("*");
		$this->db->from("bus_routes");
		//$this->db->join("route_arrival_departure", "route_arrival_departure.RouteId = bus_routes.RouteId",'inner');
                $this->db->where('bus_routes.status',1);
                //$this->db->where('route_arrival_departure.status',1);
                $this->db->where('bus_routes.From',$townid);
                $this->db->group_by('bus_routes.RouteId');
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->result();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
   function get_voucher_ticket($ticketno,$voucherno)
   {
       
        $this->db->select("*");
		$this->db->from("ticketing");
                $this->db->where('ticketing.status',1);
                $this->db->where('ticketing.ticketstatus','Open');
                $this->db->where('ticketing.newticketno',$ticketno);
                $this->db->where('ticketing.voucherno',$voucherno);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
      	return $data1;
                }
                else {
                    return NULL;
                }
   }
   
function notifications()
{
    $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
    $usergrp = $user[0]->uacc_group_fk;
    //notify on schedules without buses
    $html = "<ul>";
    $scheds = $this->bus_model->getBusSchedulesnotification();
    $rps = $this->bus_model->unapprovedrouteprices();
    if ($usergrp==3)
    {
    if ($rps==TRUE)
    {
        $html .= "<li>".anchor('admins/crud_prices2','There are route prices requiring your approval.')."</li>";
    }
    }
    if ($usergrp==2 || $usergrp==3)
    {
    if ($scheds<>NULL)
    {
         $html .= "<li>".anchor('admins/crud_schedules','There are bus schedules without assigned buses and drivers today.')."</li>";
    }
    }
    //activated seasons
     if ($usergrp==1 || $usergrp==2 || $usergrp==3)
    {
    $activeseason = $this->bus_model->getonerowfromonetbl('season_lkp', 'IsActive', 1);
    if ($activeseason==NULL)
    {
        $html .= "<li>No active price season set. Default prices apply</li>";
    }
    if ($activeseason<>NULL)
    {
        $html .= "<li>".$activeseason->SeasonName." price season has been set by admin. Default prices have been subsequently adjusted!</li>";
    }
    }
    //activated discounts
    //notify near full schedules
    //notify on drivers whose licences about to expire
    if ($usergrp==3)
    {
    $abttoexpire = $this->bus_model->getdriversnotification();
    $expired = $this->bus_model->getdriversnotificationexpired();
    if ($abttoexpire<>NULL)
    {
        foreach ($abttoexpire as $ab)
        {
            $date = date('Y-m-d');
            $now = new DateTime($date);
            $ref = new DateTime($ab->LicenceExpiry);
            $diff = $now->diff($ref);
            if ($diff->d==0)
            {
                $diffdays = "today";
            }
            else {
                $diffdays = "in ".$diff->d." days";
            }
            $html .= "<li>".$ab->StaffName."'s driver's licence will expire ".$diffdays." (".date('m/d/Y',strtotime($ab->LicenceExpiry)).")</li>";
        }
    }
    if ($expired<>NULL)
    {
        foreach ($expired as $ex)
        {
            $html .= "<li>".$ex->StaffName."'s driver's licence has expired</li>";
        }
    }
    }
    if ($usergrp==5)
    {
        $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    $custdet = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
                    if ($custdet->discountamount==0)
                    {
                        $html .= "<li>The more you use our platform, the more goodies from us, like great ticket discounts!</li>";
                    }
                    else {
                        $html .= "<li>You are currently enjoying Ksh ".round($custdet->discountamount)." discount. The more you use our platform, the more goodies from us.</li>";
                    }
                    $custamountcre=$this->bus_model->get_customer_total_credited($cust->cust_id);
			$totcredits = abs($custamountcre->camountcred);
                         $custamountdeb=$this->bus_model->get_customer_total_debited($cust->cust_id);
			$totdebits = $custamountdeb->camountdeb;
                        $balance = $totcredits - $totdebits;
                        if ($balance <= 10000)
                        {
                            $html .= "<li>Your account is running low. You might consider ".anchor('admins/crud_acc_statements','topping up')."</li>";
                        }
    }
    //notify branch manager to confirm bus arrival and comment on the schedule
    $html .= "</ul>";
    
    return $html;
    
}

function get_discount_for_corporate()
{
    $user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
    $useremail = $this->user[0]->uacc_email;
                    $cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
                    $custdet = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
                    if ($custdet->discountamount==0)
                    {
                        $html = NULL;
                    }
                    else {
                        $html = "Ksh ".round($custdet->discountamount);
                    }
                    return $html;
    
}

    function get_total_points_gained_per_customer($custcardno)
    {
          $this->db->select_sum("Points","pointsgained");
		$this->db->from("customer_loyalty_points");
                $this->db->where('customer_loyalty_points.status',1);
                $this->db->where('customer_loyalty_points.CustomerLkp',$custcardno);
                $this->db->where('customer_loyalty_points.Points >=',0);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
            return $data1;
                }
                else {
                    return NULL;
                }
    }

    function get_total_points_burned_per_customer($custcardno)
    {
          $this->db->select_sum("Points","pointsburned");
		$this->db->from("customer_loyalty_points");
                $this->db->where('customer_loyalty_points.status',1);
                $this->db->where('customer_loyalty_points.CustomerLkp',$custcardno);
                $this->db->where('customer_loyalty_points.Points <',0);
                $query = $this->db->get();
                if($query->num_rows > 0){
  		$data1 = $query->row();
		$query -> free_result();
            return $data1;
                }
                else {
                    return NULL;
                }
    }

    function get_points_history_per_customer_daterangw($custcardno,$datef,$datet)
    {
          $this->db->select("*");
		$this->db->from("customer_loyalty_points");
                $this->db->where('customer_loyalty_points.status',1);
                $this->db->where('customer_loyalty_points.CustomerLkp',$custcardno);
                $this->db->where('customer_loyalty_points.PointsDate >=',$datef);
                $this->db->where('customer_loyalty_points.PointsDate <=',$datet);
                $this->db->order_by('customer_loyalty_points.PointsDate','desc');
                $query = $this->db->get();
                if($query->num_rows() > 0){
  		$data1 = $query->result();
		$query -> free_result();
            return $data1;
                }
                else {
                    return NULL;
                }
    }

    function get_customers_selected($custids)
    {
        $custidsr = explode(",",trim($custids,','));
          $this->db->select("*");
		$this->db->from("customer");
                $this->db->where('customer.status',1);
                $this->db->where_in('customer.Cust_id',$custidsr);
                $query = $this->db->get();
                if($query->num_rows() > 0){
  		$data1 = $query->result();
		$query -> free_result();
            return $data1;
                }
                else {
                    return NULL;
                }
    }

    function get_buses_selected($busids)
    {
         $busidsr = explode(",",trim($busids,','));
          $this->db->select('*');
		$this->db->from('buses');
                $this->db->where('buses.status',1);
                $this->db->where_in('buses.BusId',$busidsr);
                $query = $this->db->get();
                if($query->num_rows() > 0){
  		$data1 = $query->result();
		$query -> free_result();
            return $data1;
                }
                else {
                    return NULL;
                }
    }

    function get_towns_selected($townids)
    {
        //$townidsr = explode(",",trim($townids,','));
          $query = $this->db->select("*")->from("towns_lkp")->where('status',1)->where_in('TownId',$townids)->get();
          
                if($query->num_rows() > 0)
                    {
  		return $query->result();
		//$query -> free_result();
            //return $data1;
                }
                else {
                    return NULL;
                }
    }
		function ifschedulealreadyadded($route,$bustype,$dateoftravel,$ispecial)
{
		$this->db->select("*");
 		$this->db->from("bus_schedule");
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('bus_schedule.RouteLkp',$route);
		$this->db->where('bus_schedule.BusTypeLkp',$bustype);
		$this->db->where('bus_schedule.DepartureDateTime',$dateoftravel);
		$this->db->where('bus_schedule.IsSpecial',$ispecial);
		$query = $this->db->get();
 	  	if($query->num_rows() > 0){
  		//$data = $query->result();
		$query -> free_result();
      	return TRUE;
                }
        else {
            return FALSE;
        }

}
		function getbusmanifest($manifestno)
{
		$this->db->select("*");
                $this->db->select('tfro.TownName as TownFrom', FALSE);
		$this->db->select('tto.TownName as TownTo', FALSE);
                $this->db->from("manifest");
		$this->db->join("bus_schedule", "manifest.ScheduleId = bus_schedule.ScheduleId",'inner');
                $this->db->join("towns_lkp tfro", "tfro.TownId = manifest.townf",'inner');
		$this->db->join("towns_lkp tto", "tto.TownId = manifest.townt",'inner');
		$this->db->join("bus_routes", "bus_routes.RouteId = bus_schedule.RouteLkp",'inner');
		//$this->db->join("ticketing", "ticketing.schedule_id = bus_schedule.ScheduleId",'inner');
		$this->db->join("bustype_lkp", "bustype_lkp.BusTypeId = bus_schedule.BusTypeLkp",'inner');
		$this->db->where('bus_schedule.status !=',0);
		$this->db->where('manifest.status !=',0);
		$this->db->where('bus_routes.status !=',0);
		$this->db->where('manifest.manifest_no',$manifestno);
		$this->db->group_by('manifest.manifest_id');
		$query = $this->db->get();
 	  	if($query->num_rows > 0){
  		$data = $query->row();
		$query -> free_result();
      	return $data;
 		 }
                 else {
                     return NULL;
                 }

}


function convert_number_to_words($number) {

    $hyphen      = ' ';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . $this->convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . $this->convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= $this->convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function min_to_hours($Minutes)
{
   if ($Minutes < 0)
   {
       $Min = Abs($Minutes);
   }
   else
   {
       $Min = $Minutes;
   }
   $iHours = Floor($Min / 60);
   $Minutes = ($Min - ($iHours * 60)) / 100;
   $tHours = $iHours + $Minutes;
   if ($Minutes < 0)
   {
       $tHours = $tHours * (-1);
   }
   $aHours = explode(".", $tHours);
   $iHours = $aHours[0];
   if (empty($aHours[1]))
   {
       $aHours[1] = "00";
   }
   $Minutes = $aHours[1];
   if (strlen($Minutes) < 2)
   {
       $Minutes = $Minutes ."0";
   }
   $tHours = $iHours ." hours ". $Minutes.' minutes';
   return $tHours;
}
                 


}