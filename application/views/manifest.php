<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bus Manifest</title>
<style type="text/css">

body, html, h1,ul, h2, h3, p,span,td,tr,form,table,thead,tbody {
	margin: 0px;
	padding: 0px;
	/*font-size: 1em;*/
	font-weight: normal;
	background-position: left top;
	vertical-align: top;
} 
@page {
  size: A4 portrait;
}

body {
    margin:0;
    width: auto;
    font-family:Verdana, Geneva, sans-serif; 
	size: A4 portrait;
	padding-left:5px;
	
	
}
</style>
<script>
function newDoc()
  {
  window.location.assign("<?=site_url('admins/crud_schedules')?>")
  }
</script>
</head>

<body onLoad="window.print(); newDoc(); window.close();">
<table width="787" align="center">
  <tr>
    <th colspan="5" scope="col" align="center"><h1>MODERN COAST EXPRESS LTD - BUS MANIFEST
        <hr noshade /></h1></th>
  </tr>
  <tr>
    <td width="170" align="right"><strong>Station:</strong></td>
    <td width="138"><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	echo $sc->TownName; ?></td>
    <td width="111">&nbsp;</td>
    <td width="129" align="right"><strong>Sub Station:</strong></td>
    <td width="164"><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	echo $sc->TownName; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Manifest No:</strong></td>
    <td><?php echo $manifestno; ?></td>
    <td>&nbsp;</td>
    <td align="right"><strong>Reg. No:</strong></td>
    <td><?php 
	echo $sched->RegistrationNumber; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Departure Date:</strong></td>
    <td><?php echo $sched->DepartureDateTime; ?></td>
    <td>&nbsp;</td>
    <td align="right"><strong>Route:</strong></td>
    <td><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	$st = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->To);
	echo $sc->TownName." - ".$st->TownName; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Departure Time:</strong></td>
    <td>
    <?php
	$dp = $this->bus_model->getdeptimebytown($sched->RouteLkp,$sched->From);
	echo date('g:i A',strtotime($dp->ArrivalDeparture));
	?>
    </td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><strong>Driver:</strong></td>
    <td>
    <?php  
	//$dr = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $sched->Driver);
	//if ($dr<>NULL):
	//echo $dr->StaffName; endif;
	
	$drvs = $this->bus_model->getresultfromonetbl('driver_schedules','ScheduleId',$schedid);
	if ($drvs<>NULL)
	{
		foreach($drvs as $drid)
		{
			$dr = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $drid->driver_id);
			echo $dr->StaffName."<br />";
		}
	}
	 ?>
    </td>
    <td>&nbsp;</td>
    <td align="right"><strong>Conductor:</strong></td>
    <td> <?php  
	$cn = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $sched->Conductor);
	if ($cn<>NULL):
	echo $cn->StaffName;endif; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>BUS:</strong></td>
    <td><?php echo $sched->Busno; ?></td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" class="table1" border="1">
   
    <thead id="pageFooter">
  <tr>
    <th scope="col">#</th>
    <th scope="col">Traveller Name</th>
    <th scope="col">ID No</th>
    <th scope="col">Mobile No</th>
    <th scope="col">Seat No</th>
    <th scope="col">Ticket No</th>
    <th scope="col">Destination</th>
    <th scope="col">Nationality</th>
    <th scope="col">Cost</th>
  </tr>
  </thead>
  <?php
  if ($tickets==NULL)
  {
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No transactions generated</td></tr>";
  }
  else {
	  $totkes = 0;
	  $totugx = 0;
	  $tottzs = 0;
	  $totusd = 0;
	  $cnt=1;
	  foreach ($tickets as $t)
	  {
		  
  ?>
  <tr>
    <td><?php echo $cnt; ?></td>
    <td><?php echo $t->clientname; ?></td>
    <td><?php echo $t->t_idno; ?></td>
    <td><?php echo $t->mobileno; ?></td>
    <td><?php echo $t->seatno; ?></td>
    <td><?php echo $t->newticketno; ?></td>
    <td><?php 
	
           $tw = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $t->totown);
	echo $tw->TownName; ?></td>
    <td><?php 
	
           //$nt = $this->bus_model->getonerowfromonetbl('countrylist', 'ID', $t->nationality);
	echo  $t->nationality;
	 ?></td>
    <td><?php 
	if ($t->t_currency==1)
	{
		$totkes +=$t->amount;
		echo "Kshs ".number_format($t->amount);
	}
	if ($t->t_currency==2)
	{
		$totugx +=$t->amountug;
		echo "Ushs ".number_format($t->amountug);
	}
	if ($t->t_currency==3)
	{
		$tottzs +=$t->amounttz;
		echo "Tshs ".number_format($t->amounttz);
	}
	if ($t->t_currency==4)
	{
		$totusd +=$t->amountusd;
		echo "$".number_format($t->amountusd);
	}
	 ?></td>
  </tr>
  <? $cnt++; } }
  
  if ($tickets<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>Total</strong></td>
    <td><strong><? 
	if ($totkes<>0)
	{
	echo "Kshs ".number_format($totkes)."<br />";
	}
	if ($totugx<>0)
	{
	echo "Ushs ".number_format($totugx)."<br />";
	}
	if ($tottzs<>0)
	{
	echo "Tshs ".number_format($tottzs)."<br />";
	}
	if ($totusd<>0)
	{
	echo "$".number_format($totusd)."<br />";
	}
	?></strong></td>
  </tr>
  <? } ?>
</table>

</body>
</html>