<?php
$data = array(
'title'=>'Manage Staff',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage staff',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#PriceFrom').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
		changeMonth: true,
		changeYear: true,
		yearRange: '1910:2016',
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     });    
	    $('#PriceTo').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '1910:2016',
        time24h: true  
     });     
	 $('#PriceFrom1').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '1910:2016',
        time24h: true  
     });    
	    $('#PriceTo1').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '1910:2016', 
        time24h: true  
     }); 
 
 
 
});  
</script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Staff</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">

    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Staff</a></li>
      <li><a href="#create" name="#tab2">Add Staff</a></li>
      <li><a href="#edit" name="#tab3">Edit Staff</a></li>
      <li><a href="#delete" name="#tab4">Delete Staff</a></li>
      <li><a href="#update" name="#tab5">Update Location</a></li>
    </ul>
      <div id="content">
    <div id="tab1">
    	 <?php if(!empty($staff)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0"  class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Name</th>
			  <th width="169">Title</th>
			  <th width="125">Added Date</th>
			  <th width="125">Current/Next Location</th>
			  <th width="125">DL ExpiryDate</th>
			  <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($staff as $item): ?>
			<tr>
				<td align="left"><?php echo $item->StaffName;?></td>
			  <td align="left"><?php echo $item->JobTitle; ?></td>
              <td align="left"><?php echo $item->StaffAdded; ?></td>
              <td align="left"><?php echo $item->TownName;
			  echo nbs(2);
			  			   $art = array('title'=>'Update Location','class'=>'confirm');

			  echo anchor('admins/update_staff_location/'.$item->StaffId.'#tab5', img(array('src'=>base_url().'images/bus_terminal_selected.png','border'=>'0','align'=>'absmiddle', 'width'=>'14', 'height'=>'21', 'alt'=>'Update Location')),$art);
			  
			   ?></td>
              <td align="left"><?php 
			  if ($item->StaffJobLkp==1)
			  {
			  echo date('d/m/Y',strtotime($item->LicenceExpiry));
			  }
		 
			 else {
				 echo "-";
			 } ?></td>
              <td align="left"><?php echo anchor('admins/edit_staff/'.$item->StaffId.'/#tab3','Edit');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no staff added yet!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open_multipart('admins/create_staff');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Job Title:</td>
          <td width="519"><span class="red"><?php echo form_error('title'); ?></span>
            <select name="title" id="title">
              <option disabled="disabled" selected="selected">---select title--</option>
              <?php foreach($titles as $t){?>
              <option value="<?php echo $t->JobTitleId;?>"><?php echo $t->JobTitle;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td width="169" class="labels">Full Name:</td>
          <td width="519"><span class="red"><?php echo form_error('name'); ?>
            <input type="text" name="name" id="long_input" value="<?php echo $this->input->post('name');?>" />
          </span></td>
        </tr>
        <tr>
          <td class="labels">Id Number:</td>
          <td><span class="red"><?php echo form_error('idnumber'); ?></span>
          <input type="text" name="idnumber" class="long_input" value="<?php echo $this->input->post('idnumber');?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Staff Number:</td>
          <td><span class="red"><?php echo form_error('staffnumber'); ?></span>
            <input type="text" name="staffnumber" id="long_input" value="<?php echo $this->input->post('staffnumber');?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Photo:</td>
          <td><span class="red"><?php echo form_error('sphoto'); ?></span>
           <input name="sphoto" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" colspan="2" class="labels">
          <h2>For Drivers Only (leave blank in case of conductors)</h2>
          <div id="driverdetails">
          <?php
           $html = "";
     
         $html .= "<script type=\"text/javascript\">  
$(function() {  
    $('#Disc_From_Date5').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     }); });</script>";
         $html .= "<table class=\"table1\"><tr><td>Licence No:</td><td><input type=\"text\" name=\"licenceno\" id=\"long_input\" value=\"".$this->input->post('licenceno')."\"></td></tr>";
         $html .= "<tr><td width=\"167\">Expiry Date:</td><td width=\"521\"><input type=\"text\" name=\"expirydate\" id=\"Disc_From_Date5\" class=\"long_input\" readonly ></td></tr>";
		 $html .= "<tr><td>PSV No:</td><td><input type=\"text\" name=\"psvno\" id=\"long_input\" value=\"".$this->input->post('psvno')."\"></td></tr>";
		 $html .= "<tr><td>Copy of PSV:</td><td><input type=\"file\" name=\"psvcopy\" id=\"long_input\"></td></tr>";
         $html .= "<tr><td>Copy of Licence:</td><td><input type=\"file\" name=\"licencecopy\" id=\"long_input\"></td></tr></table>";
     
     echo $html;
        ?>
        </div></td>
        </tr>
       
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Staff" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($row)){?>
	<?php echo form_open_multipart('admins/update_staff');?>
    <?php // foreach($staffdetails as $row):?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="279" class="labels">Job Title:</td>
          <td width="851"><span class="red"><?php echo form_error('region'); ?></span>
          <select name="title" id="title">
              <?php foreach($titles as $t){?>
              <option value="<?php echo $t->JobTitleId;?>"<?php if(!(strcmp($t->JobTitleId, $row->StaffJobLkp))) :echo "SELECTED";endif;?>><?php echo $t->JobTitle;?></option>
              <?php }?>
          </select></td>
        </tr>
                  
        <tr>
          <td width="169" class="labels">Full Name:</td>
          <td width="519"><span class="red"><?php echo form_error('name'); ?><input type="text" name="name" id="long_input" value="<?php echo $row->StaffName;?>" />
            
          </span></td>
        </tr>
        <tr>
          <td class="labels">Id Number:</td>
          <td><span class="red"><?php echo form_error('idnumber'); ?></span>
          <input type="text" name="idnumber" id="long_input" value="<?php echo $row->StaffIdNumber;?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Staff Number:</td>
          <td><span class="red"><?php echo form_error('staffnumber'); ?></span>
            <input type="text" name="staffnumber" id="long_input" value="<?php echo $row->StaffNumber;?>"></td>
        </tr>
            
        <tr>
          <td height="24" class="labels">Photo:</td>
          <td><span class="red"><?php echo form_error('sphoto'); ?></span>
          <?php
		  if ($row->SPhoto<>NULL)
		 {
		
		  ?>
           <img src="<?php echo base_url();?>staffphotos/<?php echo $row->SPhoto;?>" width="100" height="100" />
           <? } ?><input name="sphoto" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" colspan="2" class="labels">
          <h2>For Drivers Only (leave blank in case of conductors)</h2>
          <div id="driverdetails">
          <?php
           $html = "";
     
         $html .= "<script type=\"text/javascript\">  
$(function() {  
    $('#Disc_From_Date6').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     }); });</script>";
         $html .= "<table class=\"table1\"><tr><td>Licence No:</td><td><input type=\"text\" name=\"licenceno\" id=\"long_input\" value=\"".$row->LicenceNo."\"></td></tr>";
         $html .= "<tr><td width=\"167\">Expiry Date:</td><td width=\"521\"><input type=\"text\" name=\"expirydate\" id=\"Disc_From_Date6\" class=\"long_input\" value=\"".$row->LicenceExpiry."\" readonly ></td></tr>";
		 
		 if ($row->LicenceCopyFile<>NULL)
		 {
			 $anchor = anchor(base_url('staffphotos').'/'.$row->LicenceCopyFile,$row->LicenceCopyFile);
		 }
		 else {
			 $anchor = "";
		 }
		 $html .= "<tr><td>PSV No:</td><td><input type=\"text\" name=\"psvno\" id=\"long_input\" value=\"".$row->PSVNo."\"></td></tr>";
		 if ($row->PSVCopy<>NULL)
		 {
			 $anchor2 = anchor(base_url('staffphotos').'/'.$row->PSVCopy,$row->PSVCopy);
		 }
		 else {
			 $anchor2 = "";
		 }
		 $html .= "<tr><td>Copy of PSV:</td><td>".$anchor2."<input type=\"file\" name=\"psvcopy\" id=\"long_input\"></td></tr>";
         $html .= "<tr><td>Copy of Licence:</td><td>".$anchor."<input type=\"file\" name=\"licencecopy\" id=\"long_input\"></td></tr></table>";
     
     echo $html;
        ?>
        </div></td>
        </tr>    
        <tr>
          <td><input type="hidden" name="staffid" value="<?php echo $row->StaffId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Staff" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php // endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Staff\' tab!</p>';}; ?>
</div>
    <div id="tab4">
	<?php if(!empty($staff)){?>
   	  <table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Name</th>
			  <th width="169">Title</th>
			  <th width="125">Added Date</th>
			  <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($staff as $item): ?>
			<tr>
				<td align="left"><?php echo $item->StaffName;?></td>
			  <td align="left"><?php echo $item->JobTitle; ?></td>
              <td align="left"><?php echo $item->StaffAdded; ?></td>
              <td align="left"><?php echo anchor('admins/delete_staff/'.$item->StaffId.'/#delete','X',array('class' => 'delete','title'=>''.$item->StaffName.''));?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus staff added yet!</p>';}; ?>
</div>

<div id="tab5">
<?php if(!empty($staffloc)){?>
<?php echo form_open('admins/update_staff_location_update');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
          <tr>
          <td width="199" class="labels">Town:</td>
          <td width="931"><span class="red"><?php echo form_error('town'); ?></span>
            <select name="town" id="town">
              <option disabled="disabled" selected="selected">---select town--</option>
              <?php foreach($towns as $town){?>
              <option <?php if ($town->TownId==$staffloc->station_at) { echo "selected=\"selected\"";} ?> value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select><input name="staffid" type="hidden" value="<?php echo $staffid;?>" /></td>
          </tr>
                <tr>
          <td class="labels">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Schedule Time" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
      </table>
      <?php echo form_close(); } else{ echo '<p class="red">Please select staff from \'view staff\' tab!</p>';}?>
    </div>

  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
