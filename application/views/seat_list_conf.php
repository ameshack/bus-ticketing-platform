<html>
<head>
<title><?php echo $title;?></title>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-1.8.2.js"></script>
<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
<?php

if ((current_url()==base_url()."admins/crud_ticketing_voucher/".$this->uri->segment('3')))
{
	?>
<script type="text/javascript">
           $(document).ready(function() {
               $('.clicklink:checkbox').change(function() {
                   var chkd = $(this).is(':checked')
                    $('.clicklink:checkbox:not(:checked)').attr("disabled", chkd);
               });
           }); 
       </script>
       <? } ?>
  <script type="text/javascript">
  $(document).ready(function(){
  $("#submit").click(function() {// This event fires when a button is clicked
  var query = window.location.pathname.split('/');
  //var val=$("input[type='checkbox']").val();
  //var busid = query[4];
  var busid = <?=$id;?>
 // var rcode =$("#rcode").val();
  var val = $("input[name=seat]:checked").map(function () {return this.value;}).get().join(",");
  var id = $(this).attr("id");
  var isChecked = $('#seats').attr('checked')?true:false;
  var totalchecked=$("input:checkbox:checked").length;
  //alert(val);
  $.ajax({ // ajax call starts
  url: '<?php echo base_url()?>admins/getseatclass', // JQuery loads serverside.php
  type: "GET",  
  success: function(data) // Variable data contains the data we get from serverside
  { 
   	  //window.opener.document.ticket.seatclass<?=$quan?>.value= seatclass;
	  var array=val.split(",");
	  window.opener.document.ticket.seatarray<?=$quan?>.value= array;
	//window.opener.document.ticket.busstype.value= busstype;
	//window.opener.document.ticket.seatprice<?=$quan?>.value= seatprice;
	  window.opener.document.ticket.quantity.value= totalchecked;
	  window.opener.document.ticket.quantity.focus();
	  //window.opener.calc();
      window.close();  
  }
});
      return false; // keeps the page from not refreshing 
  });
});

</script>
<script src="<script type="text/javascript" src="<?php echo base_url('');?>js/jquery.hoverIntent.js" type="text/javascript"> </script> 
<script src="<script type="text/javascript" src="<?php echo base_url('');?>js/jquery.cluetip.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('a.tips').cluetip();

  $('#tip').cluetip({
    splitTitle: '|', // use the invoking element's title attribute to populate the clueTip...
                     // ...and split the contents into separate divs where there is a "|"
    showTitle: false // hide the clueTip's heading
  });
});
</script>
</head>
<body>
<div id="basi">
<?php
//echo $discountedseats;
//$booked=array();
$booked=explode(',',$bookedseats);
$discounted=explode(',',$discountedseats);

function check($seat,$seat_array,$class,$discounted,$discountedamount){
if (in_array($seat, $discounted)) {
$starr = '<a id="tip" title="Discount: '.$discountedamount.'"><span style="color:#000; font-size:16px;">*</span></a>';
}
else {
$starr ='';
}
if (!in_array($seat, $seat_array)) {
    echo  " <div  class='notbooked' ><input type='checkbox' name='seat' id=".$seat." class=clicklink  value=".$seat.'-'.$class.">".$seat.$starr."</div>";
}
else{
echo  "<div class='booked' >$seat</div>";
}

}
?>
<?php if($bustype==2){?>
<form id="normal">
<table width="400" height="400" class="bustable" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td colspan="2" align="center"><h2>VIP</h2></td>
        <td rowspan="4">&nbsp;</td>
        <td colspan="2" align="center"><div id="a">
          <h2>DRIVER</h2>
        </div></td>
    </tr>
      <tr>
        <td colspan="2" align="center"><?php  check('1',$booked,1,$discounted,$discountedamount); ?></td>
        <td colspan="2" align="center"><h2>FIRST CLASS</h2></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><?php  check('2',$booked,1,$discounted,$discountedamount); ?></td>
        <td width="21%" align="center"><?php  check('3',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('4',$booked,2,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td colspan="2" align="left"><h2>DOOR</h2></td>
        <td colspan="2" align="center"><h2>VIP</h2></td>
       </tr>
      <tr>
        <td colspan="2" align="center"><h2>FIRST CLASS</h2></td>
        <td>&nbsp;</td>
        <td colspan="2" align="center"><?php  check('5',$booked,1,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td align="center"><?php  check('7',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('8',$booked,2,$discounted,$discountedamount); ?></td>
        <td>&nbsp;</td>
        <td colspan="2" align="center"><?php  check('6',$booked,1,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2>BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="center" ><?php  check('9',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center" ><?php  check('10',$booked,3,$discounted,$discountedamount); ?></td>
        <td rowspan="8" align="center">&nbsp;</td>
        <td align="center"><?php  check('11',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="23%" align="center"><?php  check('12',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('13',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('14',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('15',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('16',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('17',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('18',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('19',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('20',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('21',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('22',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('23',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('24',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('25',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('26',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('27',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('28',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('29',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('30',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('31',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('32',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('33',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('34',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('35',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('36',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('37',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('38',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('39',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('40',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      
      <tr>
        <td width="18%" align="center"><?php  check('41',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="19%" align="center"><?php  check('42',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="19%" align="center"><?php  check('43',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('44',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('45',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><font color="#0033FF">Please seat(s) then click Submit button</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><input type="submit" name="submit" id="submit" class="submit" value="Submit" /></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </form>
  <?php } elseif($bustype==1){ ?>
 
  <form id="oxygen">
 <table width="400" height="400" class="bustable" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td colspan="2" align="left"><h2>DOOR</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="left"><h2>CONDUCTOR</h2></td>
        <td colspan="2" align="center"><h2>DRIVER</h2></td>
      </tr>
      
      <tr>
        <td colspan="2" align="center"><h2>FIRST CLASS</h2></td>
        <td colspan="2" align="center"><h2>VIP</h2></td>
       </tr>
      <tr>
        <td align="center"><?php  check('3',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('2',$booked,2,$discounted,$discountedamount); ?></td>
        <td>&nbsp;</td>
        <td colspan="2" align="center"><?php  check('1',$booked,1,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td align="center"><?php  check('6',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('5',$booked,2,$discounted,$discountedamount); ?></td>
        <td>&nbsp;</td>
        <td colspan="2" align="center"><?php  check('4',$booked,1,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2>BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="center" ><?php  check('10',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center" ><?php  check('9',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="15%" rowspan="9" align="center">&nbsp;</td>
        <td width="17%" align="center"><?php  check('8',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="19%" align="center"><?php  check('7',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('14',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('13',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('12',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('11',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('18',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('17',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('16',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('15',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('22',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('21',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('20',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('19',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('26',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('25',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('24',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('23',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('30',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('29',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('28',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('27',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('34',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('33',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('32',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('31',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('38',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('37',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('36',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('35',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      
      <tr>
        <td width="26%" align="center"><?php  check('42',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="23%" align="center"><?php  check('41',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('40',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('39',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('47',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('46',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('45',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('44',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('43',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><font color="#0033FF">Please seat(s) then  click Submit button</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><input type="hidden" name="rcode" id="rcode" value="<?=$rcode;?>"/><input type="submit" id="submit" name="submit" value="Submit" /></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </form>
   <?php } elseif($bustype==3){ ?>
 
  <form id="iriza">
 <table width="400" height="400" class="bustable" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td colspan="2" align="left"><h2>DOOR</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="left"><h2>CONDUCTOR</h2></td>
        <td colspan="2" align="center"><h2>DRIVER</h2></td>
      </tr>
      
      <tr>
        <td colspan="2" align="center">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2>BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="center" ><?php  check('4',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center" ><?php  check('3',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="20%" rowspan="11" align="center">&nbsp;</td>
        <td width="14%" align="center"><?php  check('2',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="20%" align="center"><?php  check('1',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('8',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('7',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('6',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('5',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('12',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('11',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('10',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('9',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('16',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('15',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('14',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('13',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('20',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('19',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('18',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('17',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('24',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('23',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('22',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('21',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('28',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('27',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('26',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('25',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('32',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('31',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('30',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('29',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      
      <tr>
        <td align="center"><?php  check('36',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('35',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('34',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('33',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('40',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('39',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('38',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('37',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      
      <tr>
        <td align="center"><?php  check('44',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('43',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('42',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('41',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('49',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('48',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('47',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('46',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('45',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><font color="#0033FF">Please seat(s) then  click Submit button</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><input type="hidden" name="rcode" id="rcode" value="<?=$rcode;?>"/><input type="submit" id="submit" name="submit" value="Submit"/></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </form>
  <?php } elseif($bustype==4){ ?>
 
  <form id="mini">
 <table width="400" height="400" class="bustable" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td colspan="2" align="left"><h2>&nbsp;</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center"><h2>VIP </h2></td>
        <td colspan="2" align="center"><h2>DRIVER</h2></td>
      </tr>
      
      <tr>
        <td colspan="2" align="center"><h2>
          <?php  check('1',$booked,1,$discounted,$discountedamount); ?>
        </h2></td>
        <td colspan="2" align="center"><h2>VIP</h2></td>
       </tr>
      <tr>
        <td colspan="2" align="center"><?php  check('2',$booked,1,$discounted,$discountedamount); ?></td>
        <td>&nbsp;</td>
        <td align="center"><?php  check('3',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('4',$booked,2,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td colspan="2" align="center"><h2>DOOR</h2></td>
        <td>&nbsp;</td>
        <td align="center"><?php  check('5',$booked,2,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('6',$booked,2,$discounted,$discountedamount); ?></td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2>BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="center"><h2>&nbsp;</h2></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><?php  check('7',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('8',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('9',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('10',$booked,3,$discounted,$discountedamount); ?></td>
        <td width="20%" rowspan="5" align="center">&nbsp;</td>
        <td align="center"><?php  check('11',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('12',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('13',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('14',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('15',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('16',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('17',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('18',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('19',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('20',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('21',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('22',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('23',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('24',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('25',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('26',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('27',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('28',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td align="center"><?php  check('29',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('30',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('31',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('32',$booked,3,$discounted,$discountedamount); ?></td>
        <td align="center"><?php  check('33',$booked,3,$discounted,$discountedamount); ?></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><font color="#0033FF">Please seat(s) then  click Submit button</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><input type="hidden" name="rcode" id="rcode" value="<?=$rcode;?>"/><input type="submit" id="submit" name="submit" value="Submit"/></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </form>
  <?php }?>
</div>


</body></html>