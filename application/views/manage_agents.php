<?php
$data = array(
'title'=>'Manage Agents',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage agents',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Corporate Customers</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Agents</a></li>
      <li><a href="#create" name="#tab2">Add Agent</a></li>
      <li><a href="#edit" name="#tab3">Edit Agent</a></li>
      <li><a href="#delete" name="#tab4">Delete Agent</a></li>
      <li><a href="#user" name="#tab5">Add Agent Clerks</a></li>
      <li><a href="#disc" name="#tab6">Agent Discount</a></li>
      <li><a href="#custdedt" name="#tab7">Agent Details</a></li>
      <li><a href="#custdep" name="#tab8">Agent Float</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($agents)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead>
  <tr>
    <th>No</th>
    <th>Name</th>
    <th>Type</th>
    <th>Address</th>
    <th>Email</th>
    <th>Action</th>
    </tr>
</thead>
  <?php 
$cnt=1;
foreach ($agents as $customer)
{
	$acronym = "<b>Country:</b> ".$customer->Agent_Country."<br>".
  "<b>City:</b> +".$customer->Agent_City."<br>".
  "<b>Phone:</b> ".$customer->Agent_Mobile_No."<br>".
  "<b>Website:</b> ".$customer->Agent_Website."<br>";
  
?>
  <tr>
    <td valign="top"><?php echo $cnt;?></td>
    <td valign="top"><acronym class="acronym" title=""><span onmouseover="fadeBox.showTooltip(event,'<?=$acronym?>')"><?php 
	if ($customer->Agent_Company!="")
	{
		echo anchor('admins/view_agent/'.$customer->Agent_id.'#tab7',$customer->Agent_Company);
	}
	else {
		echo anchor('admins/view_agent/'.$customer->Agent_id.'#tab7',$customer->Agent_First_Name." ".$customer->Agent_Last_Name);
	}?></span></acronym></td>
    <td valign="top"><?php 
	$cs = TRUE;
	if ($cs==TRUE)
	{
		echo "Corporate";
	}
	if ($cs==FALSE)
	{
		echo "Normal";
	}?></td>
    <td valign="top"><?php echo $customer->Agent_Street_Address."<br>".$customer->Agent_Postal_Address;
	
	if ($customer->Agent_Street_Address=="" && $customer->Agent_Postal_Address=="")
	{
		echo "None";
	}?></td>
    <td valign="top"><?php 
	
	if ($customer->Agent_E_mail=="")
	{
		echo "None";
	}
	else {
	echo safe_mailto($customer->Agent_E_mail,$customer->Agent_E_mail) ;
	}?></td>
    <td valign="top">
    <?php
	echo anchor('admins/edit_agent/'.$customer->Agent_id.'#tab3','Edit | ');
	echo anchor('admins/manage_customer_users/'.$customer->Agent_id.'#tab5','Add Users | ');
	echo anchor('admins/manage_customer_discount/'.$customer->Agent_id.'#tab6','Edit Discount | ');
	echo anchor('admins/make_deposit/'.$customer->Agent_id.'#tab8','Make Deposit');
	?>
    </td>
    </tr>
  <?php
  $cnt++;
}
  ?>
  
</table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no customers currently!</p>';}; ?>


    </div>
    <div id="tab2">
<?php echo form_open_multipart('admins/create_agents');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td width="22%" valign="top">Company</td>
    <td width="78%" valign="top"><input id="long_input" type="text" name="c_company" value="<?php echo set_value('c_company'); ?>" /><?php echo form_error('c_company'); ?></td>
  </tr>
  <tr>
    <td valign="top">Country</td>
    <td valign="top"><select name="c_country" value="<?php echo set_value('c_country'); ?>" id="c_country">
      <option selected="selected" value="">--Select--</option>
      <? 
					  if ($countries<>NULL)
					  {
                      foreach ($countries as $country)
                      {
                      ?>
      <option value="<?=$country->Name?>" <? echo set_select('c_country', $country->Name); ?>
                      >
        <?=$country->Name?>
        </option>
      <? }} ?>
      </select>
      <?php echo form_error('c_country'); ?></td>
    </tr>
  <tr>
    <td valign="top">First Name <span class="red">*</span></td>
    <td valign="top"><input id="long_input" type="text" name="c_fname" value="<?php echo set_value('c_fname'); ?>"  />
      <?php echo form_error('c_fname'); ?></td>
    </tr>
  <tr>
    <td valign="top">Last Name <span class="red">*</span></td>
    <td valign="top"><input id="long_input" type="text" name="c_lname" value="<?php echo set_value('c_lname'); ?>" />
      <?php echo form_error('c_lname'); ?></td>
    </tr>
  <tr>
    <td valign="top"><label for="user_nat_id_no">Mobile No <span class="red">*</span></label></td>
    <td valign="top"><input id="long_input" type="text" name="c_mobileno"  value="<?php echo set_value('c_mobileno'); ?>"  />
      <?php echo form_error('c_mobileno'); ?></td>
    </tr>
  <tr>
    <td valign="top">Fax</td>
    <td valign="top"><input id="long_input" type="text" name="c_faxno"  value="<?php echo set_value('c_faxno'); ?>"  />
      <?php echo form_error('c_faxno'); ?></td>
    </tr>
  <tr>
    <td valign="top">City</td>
    <td valign="top"><input id="long_input" type="text" name="c_city"  value="<?php echo set_value('c_city'); ?>"  />
      <?php echo form_error('c_city'); ?></td>
    </tr>
  <tr>
    <td valign="top">Street Address</td>
    <td valign="top"><?php echo form_textarea( array( 'name' => 'c_street_address', 'rows' => '1', 'class' => 'animated', 'id' => 'long_input', 'value' => set_value('c_street_address') ) )?><?php echo form_error('c_street_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">Email </td>
    <td valign="top"><input id="long_input" type="text" name="c_email"  value="<?php echo set_value('c_email'); ?>"   />
      <?php echo form_error('c_email'); ?></td>
    </tr>
  <tr>
    <td valign="top">Website</td>
    <td valign="top"><input id="long_input" type="text" name="c_website"  value="<?php echo set_value('c_website'); ?>"  />
      <?php echo form_error('c_website'); ?></td>
    </tr>
  <tr>
    <td valign="top">Postal Address</td>
    <td valign="top"><textarea name="c_postal_address" class='animated' id="long_input"><?php echo set_value('c_postal_address'); ?></textarea>
      <?php echo form_error('c_postal_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  type="submit" name="submit2" value="Add Agent" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
    </tr>
</table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($custdetails)){?>
	<?php echo form_open_multipart('admins/update_agent');?>
   
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td width="22%" valign="top">Company</td>
    <td width="78%" valign="top"><input id="long_input" type="text" name="c_company" value="<?php echo $custdetails->Agent_Company; ?>" /><?php echo form_error('c_company'); ?></td>
  </tr>
  <tr>
    <td valign="top">Country</td>
    <td valign="top"><select name="c_country" value="<?php echo set_value('c_country'); ?>" id="c_country">
      <? 
					  if ($countries<>NULL)
					  {
                      foreach ($countries as $country)
                      {
                      ?>
      <option <? if ($custdetails->Agent_Country==$country->Name) { echo 'selected'; } ?> value="<?=$country->Name?>" <? echo set_select('c_country', $country->Name); ?>
                      >
        <?=$country->Name?>
        </option>
      <? }} ?>
      </select>
      <?php echo form_error('c_country'); ?></td>
    </tr>
  <tr>
    <td valign="top">First Name <span class="red">*</span></td>
    <td valign="top"><input id="long_input" type="text" name="c_fname" value="<?php echo $custdetails->Agent_First_Name; ?>"  />
      <?php echo form_error('c_fname'); ?></td>
    </tr>
  <tr>
    <td valign="top">Last Name <span class="red">*</span></td>
    <td valign="top"><input id="long_input" type="text" name="c_lname" value="<?php echo $custdetails->Agent_Last_Name; ?>" />
      <?php echo form_error('c_lname'); ?></td>
    </tr>
  <tr>
    <td valign="top"><label for="user_nat_id_no">Mobile No <span class="red">*</span></label></td>
    <td valign="top"><input id="long_input" type="text" name="c_mobileno"  value="<?php echo $custdetails->Agent_Mobile_No; ?>"  />
      <?php echo form_error('c_mobileno'); ?></td>
    </tr>
  <tr>
    <td valign="top">Fax</td>
    <td valign="top"><input id="long_input" type="text" name="c_faxno"  value="<?php echo $custdetails->Agent_Fax_No; ?>"  />
      <?php echo form_error('c_faxno'); ?></td>
    </tr>
  <tr>
    <td valign="top">City</td>
    <td valign="top"><input id="long_input" type="text" name="c_city"  value="<?php echo $custdetails->Agent_City; ?>"  />
      <?php echo form_error('c_city'); ?></td>
    </tr>
  <tr>
    <td valign="top">Street Address</td>
    <td valign="top"><?php echo form_textarea( array( 'name' => 'c_street_address', 'rows' => '1', 'class' => 'animated', 'id' => 'long_input', 'value' => $custdetails->Agent_Street_Address ) )?><?php echo form_error('c_street_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">Email </td>
    <td valign="top"><input id="long_input" type="text" name="c_email"  value="<?php echo $custdetails->Agent_E_mail; ?>"   />
      <?php echo form_error('c_email'); ?></td>
    </tr>
  <tr>
    <td valign="top">Website</td>
    <td valign="top"><input id="long_input" type="text" name="c_website"  value="<?php echo $custdetails->Agent_Website; ?>"  />
      <?php echo form_error('c_website'); ?></td>
    </tr>
  <tr>
    <td valign="top">Postal Address</td>
    <td valign="top"><textarea name="c_postal_address" class='animated' id="long_input"><?php echo $custdetails->Agent_Postal_Address; ?></textarea>
      <?php echo form_error('c_postal_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input name="custid" type="hidden" value="<?=$aid?>" />&nbsp;</td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  type="submit" name="submit2" value="Edit Customer" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
    </tr>
</table>
     
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select agent from \'View Agents\' tab!</p>';}; ?>
</div>
    <div id="tab4">    	 <?php if(!empty($agents)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead>
  <tr>
    <th width="71">No</th>
    <th width="298">Name</th>
    <th width="193">Type</th>
    <th width="194">Address</th>
    <th width="249">Email</th>
    <th width="249">Action</th>
    </tr>
</thead>
  <?php 
$cnt=1;
foreach ($agents as $customer)
{
	$acronym = "<b>Country:</b> ".$customer->Agent_Country."<br>".
  "<b>City:</b> +".$customer->Agent_City."<br>".
  "<b>Phone:</b> ".$customer->Agent_Mobile_No."<br>".
  "<b>Website:</b> ".$customer->Agent_Website."<br>";
  
?>
  <tr>
    <td valign="top"><?php echo $cnt;?></td>
    <td valign="top"><acronym class="acronym" title=""><span onmouseover="fadeBox.showTooltip(event,'<?=$acronym?>')"><?php 
	if ($customer->Agent_Company!="")
	{
		echo $customer->Agent_Company;
	}
	else {
		echo $customer->Agent_First_Name." ".$customer->Agent_Last_Name;
	}?></span></acronym></td>
    <td valign="top"><?php 
	$cs = TRUE;
	if ($cs==TRUE)
	{
		echo "Corporate";
	}
	if ($cs==FALSE)
	{
		echo "Normal";
	}?></td>
    <td valign="top"><?php echo $customer->Agent_Street_Address."<br>".$customer->Agent_Postal_Address;
	
	if ($customer->Agent_Street_Address=="" && $customer->Agent_Postal_Address=="")
	{
		echo "None";
	}?></td>
    <td valign="top"><?php 
	
	if ($customer->Agent_E_mail=="")
	{
		echo "None";
	}
	else {
	echo safe_mailto($customer->Agent_E_mail,$customer->Agent_E_mail) ;
	}?></td>
    <td valign="top">
    <?php echo anchor('admins/delete_agent/'.$customer->Agent_id.'','X',array('class' => 'delete','title'=>''.$customer->Agent_Company.'')); ?>
    </td>
    </tr>
  <?php
  $cnt++;
}
  ?>
  
</table>	
    <?php // endif; ?>
    <?php }else{ echo '<p class="red">Sorry, no buses currently!</p>';}; ?></div>
    <div id="tab5">  
  <?php 
    if(!empty($seats)){?>
	
	<table width="100%" class="table1">
    <thead id="pageFooter">
			<tr align="left">
			  <th width="153">Seat Number</th>
          </tr>
		</thead>
        <?php foreach($seats as $s){?>
            <tr id="input1" class="clonedInput">
            <td><?php echo $s->SeatType ;?> :<?php $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/apply_discount/'.$s->BusId.'/'.$s->SeatTypeId, ''.$s->seats.'', $atts);?> </td>
            </tr><?php }?>
 </table>
	<?php } ?>
    <?php if($this->uri->segment(3)!=''){?>
<?php echo form_open('admins/add_cust_users/'.$this->uri->segment(3).'');?>
            <table class="table1">
            <thead id="pageFooter">
            <? if (!empty($cusers) || !is_null($cusers))
			{
			 ?>
            <tr>
            <th colspan="5" style="text-align:center;">
            <strong>Current Added Users</strong><hr />
            <?
			foreach ($cusers as $c) {
				echo "Name: ".$c->uname." | Email: ".$c->usermail."<br />";
			}
			?>
            </th>
            </tr>
            <? } ?>
			<tr align="left">
			  <th width="153">Name</th>
	          <th width="96">Username</th>
	          <th width="96">Password</th>
	          <th width="96">Email</th>
		    </tr>
		</thead>
            <tr id="input1" class="clonedInput1">
            <td>
  
 
<div id="input_ln">
                        <input name="name[]" type="text" id="seatnumber" tabindex="3">
                    </div><!-- end #input_ln -->                </td>
 
                <td><div id="select_class">
                  <input name="username[]" type="text" id="seatnumber4" tabindex="3" />
                </div></td>
                <td>
                    <div id="select_cat">
                      <input name="passwordu[]" type="password" id="seatnumber3" tabindex="3" />
                    </div><!-- end #select_cat -->                </td>
                <td><input name="useremail[]" type="text" id="seatnumber2" tabindex="3" /><input name="t_station" type="hidden" value="1" /></td>
            </tr>
            <!-- end #input1 -->
 </table>
            <div id="addDelButtons">
                <input type="button" id="btnAdd1" value="add section"> <input type="button" id="btnDel1" value="remove section above">
      </div>
 
  
                <input type="submit" value="Submit" tabindex="11">
            
      <?php echo form_close();?>
    <?php }else{ echo '<p class="red">Please select customer from \'Customers\' tab!</p>';}; ?>
    </div>
    <div id="tab6">


    <?php if($this->uri->segment(3)!=''){?>
<?php echo form_open('admins/update_customer_discount/'.$this->uri->segment(3).'');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td colspan="2" class="labels">This is the discount awarded to an. This discount can reviewed by admin depending on agent's account activity!</td>
        </tr>
        <tr>
          <td width="169" class="labels">Discount Amount:</td>
          <td width="519"><span class="red"><?php echo form_error('discamount'); ?></span>
          <input type="text" name="discamount" id="long_input" value="<?php echo $custdetails->discountamount;?>" /><input name="cust_id" type="hidden" value="<?=$this->uri->segment(3)?>" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Discount" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" class="confirm" title="Update Discount"  ></td>
        </tr>
      </table>
      <?php echo form_close(); }else{ echo '<p class="red">Please select customer from \'Customers\' tab!</p>';}; ?>
    </div>
    <div id="tab7">
	<?php if(!empty($custdetails)){?>
	
   
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
     
  
  <tr>
    <td width="22%" valign="top">Company</td>
    <td width="78%" valign="top"><input name="c_company" type="text" disabled="disabled" id="long_input" value="<?php echo $custdetails->Agent_Company; ?>" /><?php echo form_error('c_company'); ?></td>
    </tr>
  <tr>
    <td valign="top">Country</td>
    <td valign="top"><select name="c_country" value="<?php echo set_value('c_country'); ?>" id="c_country" disabled="disabled">
      <? 
					  if ($countries<>NULL)
					  {
                      foreach ($countries as $country)
                      {
                      ?>
      <option <? if ($custdetails->Agent_Country==$country->Name) { echo 'selected'; } ?> value="<?=$country->Name?>" <? echo set_select('c_country', $country->Name); ?>
                      >
        <?=$country->Name?>
        </option>
      <? }} ?>
      </select>
      <?php echo form_error('c_country'); ?></td>
    </tr>
  <tr>
    <td valign="top">First Name <span class="red">*</span></td>
    <td valign="top"><input name="c_fname" type="text" disabled="disabled" id="long_input" value="<?php echo $custdetails->Agent_First_Name; ?>"  />
      <?php echo form_error('c_fname'); ?></td>
    </tr>
  <tr>
    <td valign="top">Last Name <span class="red">*</span></td>
    <td valign="top"><input name="c_lname" type="text" disabled="disabled" id="long_input" value="<?php echo $custdetails->Agent_Last_Name; ?>" />
      <?php echo form_error('c_lname'); ?></td>
    </tr>
  <tr>
    <td valign="top"><label for="user_nat_id_no">Mobile No <span class="red">*</span></label></td>
    <td valign="top"><input name="c_mobileno" type="text" disabled="disabled" id="long_input"  value="<?php echo $custdetails->Agent_Mobile_No; ?>"  />
      <?php echo form_error('c_mobileno'); ?></td>
    </tr>
  <tr>
    <td valign="top">Fax</td>
    <td valign="top"><input name="c_faxno" type="text" disabled="disabled" id="long_input"  value="<?php echo $custdetails->Agent_Fax_No; ?>"  />
      <?php echo form_error('c_faxno'); ?></td>
    </tr>
  <tr>
    <td valign="top">City</td>
    <td valign="top"><input name="c_city" type="text" disabled="disabled" id="long_input"  value="<?php echo $custdetails->Agent_City; ?>"  />
      <?php echo form_error('c_city'); ?></td>
    </tr>
  <tr>
    <td valign="top">Street Address</td>
    <td valign="top"><?php echo form_textarea( array( 'name' => 'c_street_address', 'rows' => '1', 'id' => 'long_input', 'value' => $custdetails->Agent_Street_Address, 'disabled'=>'disabled' ) )?><?php echo form_error('c_street_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">Email </td>
    <td valign="top"><input name="c_email" type="text" disabled="disabled" id="long_input"  value="<?php echo $custdetails->Agent_E_mail; ?>"   />
      <?php echo form_error('c_email'); ?></td>
    </tr>
  <tr>
    <td valign="top">Website</td>
    <td valign="top"><input name="c_website" type="text" disabled="disabled" id="long_input"  value="<?php echo $custdetails->Agent_Website; ?>"  />
      <?php echo form_error('c_website'); ?></td>
    </tr>
  <tr>
    <td valign="top">Postal Address</td>
    <td valign="top"><textarea name="c_postal_address" disabled="disabled" id="long_input"><?php echo $custdetails->Agent_Postal_Address; ?></textarea>
      <?php echo form_error('c_postal_address'); ?></td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input name="custid" type="hidden" value="<?=$aid?>" />&nbsp;</td>
    </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><? echo anchor('admins/edit_agent/'.$aid.'#tab3','Edit'); ?></td>
    </tr>
</table>
     
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select agent from \'View Agents\' tab!</p>';}; ?>
</div>
<div id="tab8">
<?php if(!empty($custdetails)){?>
<?php echo form_open('admins/corp_deposit_payment/#tab8');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
     <tr>
    <td width="22%" valign="top">Payment Mode</td>
    <td width="78%" valign="top">
    <select name="paymode" onchange="displaypaymodes(this.value);">
    <option selected="selected" disabled="disabled"></option>
    <option value="1">Cash</option>
    <option value="2">Cheque</option>
    <option value="3">MPesa</option>
    </select>
    </td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><div id="paymodes"></div></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input name="custid" type="hidden" value="<?=$aid?>" /></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  type="submit" name="submit2" value="Make Deposit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
    </tr>
</table>
      <?php echo form_close(); ?>
      <?php echo form_open('admins/corp_get_tickets_head/#tab8'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Deposits</th>
		  </tr>
		</thead>
        <tr>
        <td>Date From
          <input name="datef" type="text" class="long_input" id="insdate" value="<?php echo $this->input->post('datef'); ?>"  />
           Date To <input name="datet" type="text" class="long_input" id="insdateedit" value="<?php echo $this->input->post('datet'); ?>" /><input name="custid" type="hidden" value="<?=$aid?>" />
        <input  type="submit" name="submit" value="Search Deposits" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"> 
        </td>
        </tr>
        </table>
        <?php echo form_close(); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Date</th>
			  <th>Amount</th>
			  <th>Approval Status</th>
	          <th>Deposited By</th>
	      </tr>
		</thead>
        <?php
		if ($deposits<>NULL) {
		 foreach ($deposits as $item2): ?>
			<tr>
			  <td align="left"><?php echo date('m/d/Y g:i A',strtotime($item2->dateadded)); ?></td>
			  <td align="left"><?php echo "Ksh ".abs($item2->camount);?></td>
			  <td align="left"><?php 
			  if ($item2->approvedstatus==0)
			  {
				  $attr = array('class'=>'confirm','title'=>'Approve cheque');
				  echo "Pending | ".anchor('admins/approve_cust_cheque','Approve',$attr);
			  }
			  else {
				  echo "Approved";
			  }
			  
			  ?></td>
			  <td align="left"><?php 
			   $this->user=$this->flexi_auth->get_user_by_id($item2->user)->result();
						
                    echo $this->user[0]->upro_first_name." ".$this->user[0]->upro_last_name;
			  ?></td>
	    </tr>
		<?php endforeach; } ?>
	 </table>
     <? } ?>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
