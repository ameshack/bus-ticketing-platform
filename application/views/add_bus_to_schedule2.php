<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add bus to schedule</title>

<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
</head>

<body style="background:#D3ECFA;">
<?php if($message==NULL){?>
<?php echo form_open('admins/add_bus_to_schedule_add');
$sched = $this->bus_model->getBusSchedule($scheduleid);
$towns = trim($sched->towns,',');
$rtowns = explode(",",$towns);
$townstot = count($rtowns);
$townf = $rtowns[0];
$townt =  $rtowns[$townstot-1];
$timeid = $this->bus_model->getdeptimebytown($sched->RouteId,$townf);
$timeidto = $this->bus_model->getdeptimebytown($sched->RouteId,$townt);
$deptime = $timeid->ArrivalDeparture;
$arrtime = $timeidto->ArrivalDeparture;

$depdate = $sched->DepartureDateTime;
$arrdate = date('Y-m-d', strtotime($sched->DepartureDateTime.' + 1 day'));
$wudbeavalbusesactive = $this->bus_model->getwouldbeAvailableBusesActive($townf,$sched->BusTypeId);
$wudbeavalbuses = $this->bus_model->getwouldbeAvailableBuses($townf,$sched->BusTypeId,$depdate,$deptime);
$wudbeavaldriversactive = $this->bus_model->getwouldbeAvailableDriversActive($townf);
$wudbeavaldrivers = $this->bus_model->getwouldbeAvailableDrivers($townf,$depdate,$deptime);
$wudbeavalconductorsactive = $this->bus_model->getwouldbeAvailableConductorsActive($townf);
$wudbeavalconductors = $this->bus_model->getwouldbeAvailableConductors($townf,$depdate,$deptime);

?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
              <tr>
          <td width="269" class="labels">Bus:</td>
          <td width="931">
          <input name="station_at" type="hidden" value="<?php echo $townt; ?>" />
          <input name="date_aval_frm" type="hidden" value="<?php echo $arrdate; ?>" />
          <input name="time_aval_frm" type="hidden" value="<?php echo $arrtime; ?>" />
          <span class="red"><?php echo form_error('bus'); ?></span>
            <select name="bus" id="bus">
             
              <?php 
			  if ($wudbeavalbusesactive<>NULL)
			  {
			  foreach($wudbeavalbusesactive as $busactive){
				 
				  ?>
              <option value="<?php echo $busactive->BusId;?>"><?php echo $busactive->RegistrationNumber;?></option>
              <?php }}
			   if ($wudbeavalbuses<>NULL)
			  {
			  foreach($wudbeavalbuses as $bus){
			  ?>
              <option value="<?php echo $bus->BusId;?>"><?php echo $bus->RegistrationNumber;?></option>
              <?php }}
			  if ($wudbeavalbuses==NULL && $wudbeavalbusesactive==NULL)
			  {
			   ?>
               <option value="0">No Bus Available!</option>
               <? } ?>
          </select></td>
        </tr>
                
                        <tr>
          <td width="269" class="labels">Driver:</td>
          <td width="931"><span class="red"><?php echo form_error('driver'); ?></span>
            <select name="driver" id="driver">
              
              <?php 
			  if ($wudbeavaldriversactive<>NULL)
			  {
			  foreach($wudbeavaldriversactive as $drivera){?>
              <option value="<?php echo $drivera->StaffId;?>"><?php echo $drivera->StaffName;?></option>
              <?php }}
			  if ($wudbeavaldrivers<>NULL)
			  {
			  foreach($wudbeavaldrivers as $driver){
			  ?>
               <option value="<?php echo $drivera->StaffId;?>"><?php echo $drivera->StaffName;?></option>
               <? }}
			    if ($wudbeavaldrivers==NULL && $wudbeavaldriversactive==NULL)
				{
			   ?>
               <option value="0">No Driver Available!</option>
               <? } ?>
          </select></td>
        </tr>
        <tr>
          <td width="269" class="labels">Conductor:</td>
          <td width="931"><span class="red"><?php echo form_error('conductor'); ?></span>
            <select name="conductor" id="conductor">
              <?php 
			  if ($wudbeavalconductorsactive<>NULL)
			  {
			  foreach($wudbeavalconductorsactive as $conda){?>
              <option value="<?php echo $conda->StaffId;?>"><?php echo $conda->StaffName;?></option>
              <?php }}
			  if ($wudbeavalconductors<>NULL)
			  {
			  foreach($wudbeavalconductors as $cond){
			  ?>
               <option value="<?php echo $cond->StaffId;?>"><?php echo $cond->StaffName;?></option>
               <? }}
			    if ($wudbeavalconductors==NULL && $wudbeavalconductorsactive==NULL)
				{
			   ?>
               <option value="0">No Conductor Available!</option>
               <? } ?>
            </select>
            <input type="hidden" name="scheduleid" value="<?php echo $scheduleid;?>" /></td>
        </tr>


        <tr>
          <td></td>
          <td width="931"><input  type="submit" name="submit" value="Submit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
<?php echo form_close();?>
     <?php } else { echo "<p>".$message."</p>";?>
<table width="100%">
  <tr>
    <td width="266">&nbsp;</td>
    <td width="879">      <form action='javascript:void(0);' onclick="window.close(); window.opener.location.reload();">
        <p>
          <input name="Button"  type='submit'  id='close' value="Close Window" />
          &nbsp;&nbsp; </p>
      </form></td>
  </tr>
</table>
<?php }?>
</body>
</html>