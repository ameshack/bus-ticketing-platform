<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TMS: Ticket Cancel Reason</title>

<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
</head>

<body style="background:#D3ECFA;">
<table width="100%">
  <tr>
    <td>Cancel Reason</td>
    <td><? echo $row->cancel_reason;?></td>
  </tr>
  <tr>
    <td>Cancelled By</td>
    <td><? echo $row->cancelled_by;?></td>
  </tr>
  <tr>
    <td>Cancelled Date</td>
    <td><? echo $row->cancel_date;?></td>
  </tr>
  <tr>
    <td width="593">&nbsp;</td>
    <td width="786">      <form action='javascript:void(0);' onClick="window.close();">
        <p>
          <input name="Button"  type='submit'  id='close' value="Close Window" />
          &nbsp;&nbsp; </p>
      </form></td>
  </tr>
</table>
</body>
</html>