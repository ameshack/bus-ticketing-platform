<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Petty Cash</title>
<script>
function newDoc()
  {
  window.location.assign("<?=site_url('admins/crud_petty_cash')?>")
  }
</script>
</head>
<body onLoad="window.print(); newDoc(); window.close();">

<table border="1" cellpadding="0" cellspacing="0" width="600">
  <tr>
    <td width="380" rowspan="3" align="left"><h4>MODERN COAST EXPRESS LTD<br />
    </h4></td>
    <td colspan="2" align="left">NO: <? echo $pettno;?></td>
  </tr>
  <tr>
    <td colspan="2" align="left">PETTY CASH VOUCHER</td>
  </tr>
  <tr>
    <td colspan="2" align="left">Date: <? echo date('d/m/Y'); ?></td>
  </tr>
  <tr>
    <td align="left"><span style="float:left;">PAYEE:  <? echo $payee;?></span> <span style="float:right;">PARTICULARS:  <? echo $particulars;?></span></td>
    <td width="106" align="center">SHS</td>
    <td width="106" align="center">CTS</td>
  </tr>
  <tr>
    <td rowspan="2" align="left">A/C&nbsp;  <u><? echo $ac;?></u>
      <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>AMOUNT IN WORDS:  <u><? echo $shortcurrency." ".$amountinwords." only";?></u></p></td>
    <td width="106" height="222" align="right" valign="top"><? echo $amount;?></td>
    <td width="106" align="right" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><? echo $amount;?></td>
    <td width="106" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><table width="100%" border="0">
      <tr>
        <td width="30%">Prepared By: <? echo $preparedby;?></td>
        <td width="35%">Authorised By:</td>
        <td width="35%">Received By:</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>