<?php
$data = array(
'title'=>'My Sales',
'keywords'=>' ',
'description'=>' ',
'selected'=>'my sales',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Ticket Sales</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Sales Summary</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/crud_ticket_sales_clerk_refine/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23" colspan="2">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
        <td width="50%" valign="top">Date From<br />
          <input name="datef" type="text" class="long_input" disabled="disabled" id="insdate" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  /></td>
        <td width="50%" valign="top"><span style="vertical-align:top;">Date To<br />
            <input name="datet" type="text" class="long_input" disabled="disabled" id="insdateedit" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d');} else { echo $this->input->post('datet');} ?>" />
        </span></td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:center;">
          <!--<input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" />--></td>
        </tr>
        </table>
        <?php echo form_close(); ?>
         <div id="printarea">
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th height="23"><?php //echo $datettitle;?> Clerk: <?php echo $clerk; ?></th>
 <th>Station: <?php echo $station; ?></th>
	          <th>Date: <?php echo date('d/m/Y'); ?></th>
</tr>
			<tr align="left">
			  <th height="23">Total Ticket Sales</th>
	          <th>Petty Cash</th>
	          <th>Final Amount</th>
          </tr>
		</thead>
        
			<tr>
			  <td align="left" style="vertical-align:top;"><?php echo $tts;?></td>
			  <td align="left" style="vertical-align:top;"><?php echo $pc;?></td>
			  <td align="left" style="vertical-align:top;"><?php echo $fa;?></td>
        </tr>
		
	 </table>
    </div>
    
  
<table width="596" border="0" align="center" cellspacing="0" style="text-align:center;">
  <tr>
    <td align="center" valign="top" style="text-align:center;">
    <form>
      <input type="button" value="Print" onClick="javascript:void(printSpecial())" />
    </form>    </td>
    </tr>
</table>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
