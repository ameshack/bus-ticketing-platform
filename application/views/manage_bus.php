<?php
$data = array(
'title'=>'Manage Bus',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Buses</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Buses</a></li>
      <li><a href="#create" name="#tab2">Add Bus</a></li>
      <li><a href="#edit" name="#tab3">Edit Bus</a></li>
      <li><a href="#delete" name="#tab4">Delete Bus</a></li>
       <li><a href="#addseat" name="#tab5">Add Bus Seats</a></li>
      <li><a href="#update" name="#tab6">Update Location</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($buses)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th width="38">No</th>
			  <th width="235">Registration Number</th>
			  <th width="192">Bus Type</th>
			  <th width="180">Make</th>
			  <th width="140">Seats</th>
	          <th width="153">Status</th>
	          <th width="153">Current/Next Location</th>
	          <th width="226">Options</th>
		  </tr>
		</thead>
        <?php $cnt = 1; foreach ($buses as $item): if($item->IsActive == 1): $status = "Available";else:$status = "Not Available"; endif; ?>
			<tr>
			  <td align="left"><? echo $cnt; ?></td>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->MakeName; ?></td>
              <td align="left"><?php echo $item->TotalSeats; ?></td>
			  <td align="left"><?php echo $status; ?></td>
			  <td align="left"><?php echo $item->TownName;
			   echo nbs(2);
			  			   $art = array('title'=>'Update Location','class'=>'confirm');

			  echo anchor('admins/update_bus_location/'.$item->BusId.'#tab6', img(array('src'=>base_url().'images/bus_terminal_selected.png','border'=>'0','align'=>'absmiddle', 'width'=>'14', 'height'=>'21', 'alt'=>'Update Location')),$art);
			  
			   ?></td>
			  <td align="left"><?php echo anchor('admins/edit_bus/'.$item->BusId.'/#tab3','Edit |');?> <?php echo anchor('admins/manage_bus_seats/'.$item->BusId.'/#tab5','Add Seats');?></td>
			</tr>
		<?php $cnt++; endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no buses currently!</p>';}; ?>


    </div>
    <div id="tab2">
<?php echo form_open_multipart('admins/create_bus');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus Type:</td>
          <td width="519"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="bustypelkp" id="bustypelkp">
              <option disabled="disabled" selected="selected">---select bus type---</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td height="24" class="labels">Exterior Colour:</td>
          <td><?php echo form_error('ExternalColour'); ?></span>
            <input type="text" name="ExternalColour" id="long_input" value="<?php echo $this->input->post('ExternalColour');?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Interior Colour:</td>
          <td><?php echo form_error('InternalColour'); ?></span>
            <input type="text" name="InternalColour" id="long_input" value="<?php echo $this->input->post('InternalColour');?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Registration Number:</td>
          <td><span class="red"><?php echo form_error('regno'); ?></span>
            <input type="text" name="regno" id="long_input" value="<?php echo $this->input->post('regno');?>" ></td>
        </tr>
        <tr>
          <td class="labels">Make:</td>
          <td><span class="red"><?php echo form_error('makelkp'); ?></span>
            <select name="makelkp" id="makelkp">
              <option disabled="disabled" selected="selected">---select make type---</option>
              <?php foreach($makes as $make){?>
              <option value="<?php echo $make->MakeId;?>"><?php echo $make->MakeName;?></option>
              <?php }?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Date Bought:</td>
          <td><span class="red"><?php echo form_error('datebought'); ?></span>
            <input type="text" name="datebought"  id="datebought"  class="long_input" value="<?php echo $this->input->post('datebought');?>"/></td>
        </tr>
        <tr>
          <td class="labels">Bus Name</td>
          <td><span class="red"><?php echo form_error('busname'); ?></span>
            <input type="text" name="busname" id="long_input" value="<?php echo $this->input->post('busname');?>"></td>
        </tr>
        <tr>
          <td class="labels">Bus Number:</td>
          <td><span class="red"><?php echo form_error('busnumber'); ?></span>
            <input type="text" name="busnumber" id="long_input" value="<?php echo $this->input->post('busnumber');?>"></td>
        </tr>
        <tr>
          <td class="labels">Insurance Expiry Date:</td>
          <td><span class="red"><?php echo form_error('insexpiry'); ?></span>
            <input type="text" name="insexpiry" id="insdate" readonly="readonly" class="long_input" value="<?php echo $this->input->post('insexpiry');?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of Insurance:</td>
          <td><span class="red"><?php echo form_error('inscopy'); ?></span>
           <input name="inscopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of COMESA:</td>
          <td><span class="red"><?php echo form_error('inscomesacopy'); ?></span>
           <input name="inscomesacopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of Logbook:</td>
          <td><span class="red"><?php echo form_error('logbookcopy'); ?></span>
           <input name="logbookcopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td class="labels">Is Active</td>
          <td><input name="isactive" type="checkbox" checked="checked" id="isactive" value="1" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Bus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($busdetails)){?>
	<?php echo form_open_multipart('admins/update_bus');?>
    <?php foreach($busdetails as $row): if($row->IsActive == 1): $set_checked = "CHECKED";else:$set_checked = ""; endif;?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Bus Type:</td>
          <td width="511"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="bustypelkp" id="bustypelkp">
              <option disabled="disabled">---select bus type---</option>
              <?php foreach($bustypes as $bustype):?>
<option value="<?php echo $bustype->BusTypeId;?>" <?php if(!(strcmp($bustype->BusTypeId, $row->BusTypeLkp))) :echo "SELECTED";endif;?>><?php echo $bustype->BusType;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Exterior Colour:</td>
          <td><?php echo form_error('ExternalColour'); ?></span>
          <input type="text" name="ExternalColour" id="long_input" value="<?php echo $row->ExternalColour;?>" ></td>
        </tr>
        <tr>
          <td class="labels">Interior Colour:</td>
          <td><?php echo form_error('InternalColour'); ?></span>
            <input type="text" name="InternalColour" id="long_input" value="<?php echo $row->InternalColour;?>" ></td>
        </tr>
        <tr>
          <td class="labels">Registration Number:</td>
          <td><span class="red"><?php echo form_error('regno'); ?></span>
            <input type="text" name="regno" id="long_input" value="<?php echo $row->RegistrationNumber;?>" ></td>
        </tr>
        <tr>
          <td class="labels">Make:</td>
          <td><span class="red"><?php echo form_error('makelkp'); ?></span>
            <select name="makelkp" id="makelkp">
              <option disabled="disabled">---select make type---</option>
              <?php foreach($makes as $make):?>
 <option value="<?php echo $make->MakeId;?>" <?php if(!(strcmp($make->MakeId, $row->MakeLkp))) :echo "SELECTED";endif;?>><?php echo $make->MakeName;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Date Bought:</td>
          <td><span class="red"><?php echo form_error('datebought'); ?></span>
           <input type="text" name="datebought" id="dateboughtedit"  class="long_input"  value="<?php echo $row->DateBought;?>"/></td>
        </tr></td>
        </tr>
        <tr>
          <td class="labels">Bus Name</td>
          <td><span class="red"><?php echo form_error('busname'); ?></span>
            <input type="text" name="busname" id="long_input" value="<?php echo $row->BusName;?>"></td>
        </tr>
        <tr>
          <td class="labels">Bus Number:</td>
          <td><span class="red"><?php echo form_error('busnumber'); ?></span>
            <input type="text" name="busnumber" id="long_input" value="<?php echo $row->BusNumber;?>"></td>
        </tr>
        <tr>
          <td class="labels">Insurance Expiry Date:</td>
          <td><span class="red"><?php echo form_error('insexpiry'); ?></span>
            <input type="text" name="insexpiry" id="insdateedit" readonly="readonly" class="long_input" value="<?php echo  $row->InsuranceExpiry;?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of Insurance:</td>
          <td><span class="red"><?php echo form_error('inscopy'); ?></span>
          <?php
		  if ($row->CopyOfInsurance<>NULL || !empty($row->CopyOfInsurance))
		 {
		    echo anchor(base_url('filecopies').'/'.$row->CopyOfInsurance,$row->CopyOfInsurance);
		 }
		  ?>
           <input name="inscopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of COMESA:</td>
          <td><span class="red"><?php echo form_error('inscomesacopy'); ?></span>
           <?php
		  if ($row->CopyOfComesa<>NULL || !empty($row->CopyOfComesa))
		 {
		    echo anchor(base_url('filecopies').'/'.$row->CopyOfComesa,$row->CopyOfComesa);
		 }
		  ?>
           <input name="inscomesacopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td height="24" class="labels">Copy of Logbook:</td>
          <td><span class="red"><?php echo form_error('logbookcopy'); ?></span>
          <?php
		  if ($row->CopyOfLogbook<>NULL || !empty($row->CopyOfLogbook))
		 {
		    echo anchor(base_url('filecopies').'/'.$row->CopyOfLogbook,$row->CopyOfLogbook);
		 }
		  ?>
           <input name="logbookcopy" type="file" id="long_input" />
           </td>
        </tr>
        <tr>
          <td class="labels">Is Active</td>
          <td><input name="isactive" type="checkbox" id="isactive" value="1" <?php echo $set_checked;?> /></td>
        </tr>
        <tr>
          <td><input type="hidden" name="busid" value="<?php echo $row->BusId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Bus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select bus from \'View Buses\' tab!</p>';}; ?>
</div>
    <div id="tab4">    	 <?php if(!empty($buses)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="218">Registration Number</th>
			  <th width="110">Bus Type</th>
			  <th width="115">Make</th>
			  <th width="138">Status</th>
			  <th width="95">Delete</th>
	  </tr>
		</thead>
        <?php foreach ($buses as $item):if($item->IsActive == 1): $status = "Available";else:$status = "Not Available"; endif; ?>
			<tr>
				<td><?php echo $item->RegistrationNumber; ?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->MakeName; ?></td>
              <td align="left"><?php echo $status; ?></td>
              <td align="left"><?php echo anchor('admins/delete_bus/'.$item->BusId.'','X',array('class' => 'delete','title'=>''.$item->RegistrationNumber.'')); ?></td>
			</tr><?php endforeach; ?>
	 </table>	
    <?php // endif; ?>
    <?php }else{ echo '<p class="red">Sorry, no buses currently!</p>';}; ?></div>
    <div id="tab5">  
  <?php 
    if(!empty($seats)){?>
	
	<table width="100%" class="table1">
    <thead id="pageFooter">
			<tr align="left">
			  <th width="153">Seat Number</th>
          </tr>
		</thead>
        <?php foreach($seats as $s){?>
            <tr id="input1" class="clonedInput">
            <td><?php echo $s->SeatType ;?> :<?php $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/apply_discount/'.$s->BusId.'/'.$s->SeatTypeId, ''.$s->seats.'', $atts);?> </td>
            </tr><?php }?>
 </table>
	<?php } ?>
    <?php if($this->uri->segment(3)!=''){?>
<?php echo form_open('admins/add_bus_seats/'.$this->uri->segment(3).'');?>
            <table class="table1">
            <thead id="pageFooter">
			<tr align="left">
			  <th width="153">Seat Number</th>
	          <th width="96">Seat Class</th>
	          <th width="96">Seat Position</th>
		  </tr>
		</thead>
            <tr id="input1" class="clonedInput">
            <td>
  
 
<div id="input_ln">
                        <input name="seatnumber[]" type="text" id="seatnumber" tabindex="3">
                    </div><!-- end #input_ln -->                </td>
 
                <td><div id="select_class"><select name="seatclass[]" id="seatclass" tabindex="4">
                  <option value="" selected="selected" disabled="disabled">Select seat class</option>
                  <option value="1">VIP</option>
                  <option value="2">First Class</option>
                  <option value="3">Business</option>
                  </select></div></td>
                <td>
                    <div id="select_cat">
                        <select name="seatposition[]" id="seatposition" tabindex="5">
                            <option value="" selected="selected" disabled="disabled">Select seat position</option> 
                            <option value="1">Left Window</option>
                            <option value="2">Left Isle </option>
                            <option value="3">Right window</option>
                            <option value="4">Right Isle</option>
                            <option value="5">Isle</option>
                        </select>
                    </div><!-- end #select_cat -->                </td>
            </tr><!-- end #input1 -->
 </table>
            <div id="addDelButtons">
                <input type="button" id="btnAdd" value="add section"> <input type="button" id="btnDel" value="remove section above">
            </div>
 
  
                <input type="submit" value="Submit" tabindex="11">
            
      <?php echo form_close();?>
    <?php }else{ echo '<p class="red">Please select bus from \'buses\' tab!</p>';}; ?>
    </div>
    
    
    <div id="tab6">
<?php if(!empty($bus)){?>
<?php echo form_open('admins/update_bus_location_update');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
          <tr>
          <td width="199" class="labels">Town:</td>
          <td width="931"><span class="red"><?php echo form_error('town'); ?></span>
            <select name="town" id="town">
              <option disabled="disabled" selected="selected">---select town--</option>
              <?php foreach($towns as $town){?>
              <option <?php if ($town->TownId==$bus->station_at) { echo "selected=\"selected\"";} ?> value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select><input name="busid" type="hidden" value="<?php echo $busid;?>" /></td>
          </tr>
                <tr>
          <td class="labels">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Schedule Time" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
      </table>
      <?php echo form_close(); } else{ echo '<p class="red">Please select bus from \'view buses\' tab!</p>';}?>
    </div>
    
    
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
