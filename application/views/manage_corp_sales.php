<?php
$data = array(
'title'=>'Manage Corporate Sales',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage ticket sales',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Ticket Sales</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Sales Summary</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/crud_corp_sales_refine/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23" colspan="2">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
        <td width="50%" rowspan="2" valign="top">
        Corporate(s) <br /> <select name="corp[]" id="corp" multiple="multiple">
              <?php if ($corps<>NULL) {foreach($corps as $t){?>
              <option value="<?php echo $t->Cust_id;?>"><?php echo $t->Cust_Company;?></option>
              <?php } }?>
          </select></td>
        <td width="50%" valign="top">Date From<br />
          <input name="datef" type="text" class="long_input" id="insdate" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  /></td>
        </tr>
        <tr>
          <td valign="top" style="vertical-align:top;">Date To<br />
          <input name="datet" type="text" class="long_input" id="insdateedit" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d');} else { echo $this->input->post('datet');} ?>" /></td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:center;"><input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        <? if ($rep<>NULL)
		{
			?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="7"><?php echo $datettitle;?></th>
</tr>
<? if ($corpz<>NULL)
{
	$cnt = 1;
	
	foreach ($corpz as $c)
	{
	?>
			<tr align="left">
			  <th>No</th>
			  <th height="23">Corporate</th>
			  <th>Total Ticket Sales</th>
          </tr>
		</thead>
        
			<tr>
			  <td align="left" style="vertical-align:top;"><? echo $cnt; ?></td>
			  <td align="left" style="vertical-align:top;"><?php echo $c->Cust_Company; ?></td>
			  <td align="left" style="vertical-align:top;"><?php 
			  $tts = $this->bus_model->get_total_corp_ticket_sales($c->Cust_id,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                   // $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    //$ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     //$ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     //$ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     //$ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                echo $ttshtml;
			  //echo $tts;?></td>
        </tr>
		<? $cnt++; }} ?>
	 </table>
    <? } ?>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
