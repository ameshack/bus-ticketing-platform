<?php
$data = array(
'title'=>'Search Tickets',
'keywords'=>' ',
'description'=>' ',
'selected'=>'Search tickets',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>

<script type="text/javascript">  
$(function() { 
//var maxDate = getDateYymmdd($(this).data("val-rangedate-max")); 
	    $('#traveldatezz1').datepicker({ 
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,   
         minDate: 0,
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 	    $('#traveldate').datepicker({ 
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	     $('#traveltime1').timepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     });  
});  
</script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Tickets</p>

<div id="rightcontentwide">
 
    <ul id="tabs">
      <li><a href="#create" name="#tab1">Search Ticket</a></li>
    </ul>
     <div id="content">
        
<div id="tab1">
            	
                 <?php echo form_open('admins/search_tickets_r/'); ?>
                 <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Ticket</th>
		  </tr>
		</thead>
        <tr>
        <td>Search By <select name="searchtype">
        <option value="ticket">Ticket No</option>
        <option value="name">Name</option>
        <option value="idno">Id No</option>
        <option value="phone">Phone No</option>
        <option value="voucher">Voucher No</option>
        <option value="issuedby">Issued By</option>
        </select><input name="searchvalue" type="text" id="long_input" />
        <input  type="submit" name="submit" value="Search Ticket" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px">
        </td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Date</th>
			  <th> Time</th>
			  <th>Traveller Name</th>
			  <th>Id No</th>
			  <th>Seat</th>
	          <th>Ticket No</th>
	          <th>Status</th>
	          <th>Amount</th>
	          <th>Route</th>
		      <th>Options</th>
		  </tr>
		</thead>
        <?php 
		if ($tickets==NULL)
		{
			echo "<tr><td colspan=\"11\">No data to display</td></tr>";
		}
		else {
		foreach ($tickets as $item): ?>
			<tr>
			  <td align="left"><?php echo date('d/m/Y',strtotime($item->DepartureDateTime)); ?></td>
			  <td align="left"><?php echo $item->reporttime;?></td>
			  <td align="left"><?php echo $item->clientname; ?></td>
              <td align="left"><?php echo $item->t_idno; ?></td>
              <td align="left"><?php echo $item->seatno;?></td>
			  <td align="left"><?php echo $item->newticketno; ?></td>
			  <td align="left"><?php echo $item->ticketstatus;?></td>
			  <td align="left"><?php 
			  if ($item->t_currency==1)
			  {
				  echo "KSH ".number_format($item->amount,2);
			  }
			  if ($item->t_currency==2)
			  {
				  echo "UGX ".number_format($item->amountug,2);
			  }
			  if ($item->t_currency==3)
			  {
				  echo "TZS ".number_format($item->amounttz,2);
			  }
			  if ($item->t_currency==4)
			  {
				  echo "USD ".number_format($item->amountusd,2);
			  }
			  if ($item->t_currency==5)
			  {
				  echo "EUR ".number_format($item->amounteuro,2);
			  }
			  
			  ?></td>
			  <td align="left"><?php echo $item->TownFrom." - ".$item->TownTo;?></td>
			  <td align="left">
			  <?php
$user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$usergrp = $user[0]->uacc_group_fk;
		
			  $now = date('Y-m-d H:i:s');
			$nztime = $this->bus_model->getdeptime($item->RouteId);
			  
			  $depdatetimez = $item->DepartureDateTime.' '.$nztime->ArrivalDeparture;
?>
<?php
$rp = array('target'=>'_blank');
 $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
if ($usergrp==2 || $usergrp==3){
?>
			  <?php if($item->ticketstatus=='Reserved' || $item->ticketstatus=='Active'){
			 
			  if (strtotime($depdatetimez) > strtotime($now))
			  {
			echo anchor_popup('admins/cancel_ticket/'.$item->TicketId, 'CANCEL | ', $atts); } } elseif($item->ticketstatus=='Inactive') {
							echo "<font color=\"#FF0000\">Cancelled |  </font>"; $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/ticket_cancel_reason/'.$item->TicketId, 'Reason', $atts);
						}?>&nbsp;
		      <?php 
			  $atr = array('Confirm Ticket', 
			  'class'=>'confirm');
			   $atr1 = array('width'      => '650',
              'height'     => '800',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30',
			  'title'=>'Open Ticket');
			  
			  //$nztime = $this->bus_model->getdeptime($item->RouteId);
			  
			  //$depdatetimez = $item->DepartureDateTime.' '.$nztime->ArrivalDeparture;
			  if (strtotime($depdatetimez) > strtotime($now))
			  {
			  if($item->ticketstatus=='Active'){ echo anchor_popup('admins/open_ticket/'.$item->TicketId, 'Open Ticket',$atr1);
			  }
			  }
			  if ($item->ticketstatus!='Open'){
			  echo anchor('admins/print_ticket/'.$item->TicketId, ' | Reprint',$rp);}} 
			  
			  if($item->ticketstatus=='Reserved') { 
			  
			  echo anchor_popup('admins/cancel_ticket/'.$item->TicketId, 'CANCEL ', $atts); 
			  
			   $atr = array('Confirm Ticket', 
			  'class'=>'confirm');
			   $atr1 = array('width'      => '650',
              'height'     => '800',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30',
			  'title'=>'Open Ticket');
			  if (strtotime($depdatetimez) > strtotime($now))
			  {
			  echo anchor('admins/crud_ticketing_conf/'.$item->schedule_id.":".$item->fromtown.":".$item->totown.":".$item->TicketId, '| Confirm Booking',$atr);}
			  
			  
			  }
			  
			  if ($item->ticketstatus=='Redeemed' || $item->ticketstatus=='Active'){
				  
				  echo anchor('admins/print_ticket/'.$item->TicketId, ' | Reprint',$rp);
			  }
			  
			  //else {
				  //echo anchor('admins/print_ticket/'.$item->TicketId, ' | Reprint',$rp);
			  //}
			  ?></td>
		</tr>
		<?php endforeach; } ?>
	 </table>	
<?php // endif; ?>


    </div>
</div>
<script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

<?php  $this->load->view('extras/lower'); ?>
