<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add bus to schedule</title>

<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/add_seats.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/add_users.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-templ.js"></script>


  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">


<style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    /* support: IE7 */
    *height: 1.7em;
    *top: 0.1em;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 0.3em;
  }
  </style>
  <script>
  (function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );
 
  $(function() {
    $( "#combobox" ).combobox();
    $( "#toggle" ).click(function() {
      $( "#combobox" ).toggle();
    });
  });
  </script>

</head>

<body style="background:#D3ECFA;">
<?php if($message==NULL){?>
<?php echo form_open('admins/add_bus_to_schedule_add');
//$d = ("1,2,3,4,5");
//$dr = explode(",",$d);
//echo count($dr); exit;
//$timef = "23:00:00";
//$timet = date('H:i:s');
//$hrs = strtotime($timet) - strtotime($timef); 
//echo date('H',$hrs);
//$date = date('H:i:s');
//$now = new DateTime($date);
//$ref = new DateTime($timef);
//$diff = $now->diff($ref);
//echo $diff->h;
//exit;

$sched = $this->bus_model->getBusSchedule($scheduleid);
$towns = trim($sched->towns,',');
$rtowns = explode(",",$towns);
$townstot = count($rtowns);
$townf = $rtowns[0];
$townt =  $rtowns[$townstot-1];
$timeid = $this->bus_model->getdeptimebytown($sched->RouteId,$townf);
$timeidto = $this->bus_model->getdeptimebytown($sched->RouteId,$townt);
$deptime = $timeid->ArrivalDeparture;
$arrtime = $timeidto->ArrivalDeparture;

$depdate = $sched->DepartureDateTime;
if (strtotime($deptime) >= strtotime($arrtime))
{
	$arrdate = date('Y-m-d', strtotime($sched->DepartureDateTime.' + 1 day'));
}
else {
	$arrdate = date('Y-m-d', strtotime($sched->DepartureDateTime));
}
//echo $depdate." to ".$arrdate; exit;
$wudbeavalbusesactive = $this->bus_model->getwouldbeAvailableBusesActive($townf,$sched->BusTypeId);
$wudbeavalbuses = $this->bus_model->getwouldbeAvailableBuses($townf,$sched->BusTypeId,$depdate,$deptime);
$wudbeavaldriversactive = $this->bus_model->getwouldbeAvailableDriversActive($townf);
$wudbeavaldrivers = $this->bus_model->getwouldbeAvailableDrivers($townf,$depdate,$deptime);
$wudbeavalconductorsactive = $this->bus_model->getwouldbeAvailableConductorsActive($townf);
$wudbeavalconductors = $this->bus_model->getwouldbeAvailableConductors($townf,$depdate,$deptime);

?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
              <tr>
          <td width="269" class="labels">Bus:</td>
          <td width="931">
          <input name="station_at" type="hidden" value="<?php echo $townt; ?>" />
          <input name="date_aval_frm_initial" type="hidden" value="<?php echo $depdate; ?>" />
          <input name="time_aval_frm_initial" type="hidden" value="<?php echo $deptime; ?>" />
          <input name="date_aval_frm" type="hidden" value="<?php echo $arrdate; ?>" />
          <input name="time_aval_frm" type="hidden" value="<?php echo $arrtime; ?>" />
          <span class="red"><?php echo form_error('bus'); ?></span>
            <select name="bus" id="bus">
             
              <?php 
			  if ($wudbeavalbusesactive<>NULL)
			  {
			  foreach($wudbeavalbusesactive as $busactive){
				 
				  ?>
              <option value="<?php echo $busactive->BusId;?>"><?php echo $busactive->RegistrationNumber;?></option>
              <?php }}
			   if ($wudbeavalbuses<>NULL)
			  {
			  foreach($wudbeavalbuses as $bus){
			  ?>
              <option value="<?php echo $bus->BusId;?>"><?php echo $bus->RegistrationNumber;?></option>
              <?php }}
			  if ($wudbeavalbuses==NULL && $wudbeavalbusesactive==NULL)
			  {
			   ?>
               <option value="">No Bus Available!</option>
               <? } ?>
          </select>
          <?php
		  if ($sched->BusLkp<>NULL)
		  {
			  echo "<strong>Already Assigned Bus: ".$sched->RegistrationNumber."</strong>";
		  }
		  ?>
          </td>
        </tr>
        <tr>
          <td width="269" class="labels">Conductor:</td>
          <td width="931"><span class="red"><?php echo form_error('conductor'); ?></span>
            <select name="conductor" id="conductor">
              <?php 
			  if ($wudbeavalconductorsactive<>NULL)
			  {
			  foreach($wudbeavalconductorsactive as $conda){
				  $date = $depdate.' '.$deptime;
					$now = new DateTime($date);
					$ref = new DateTime($conda->date_available_from.' '.$conda->time_available_from);
					$diff = $now->diff($ref);
					$days = $diff->d;
					$hrs = $diff->h;
					if ($days==0)
					{
						$timerested = " (should have rested ".$hrs."hrs)";
					}
					else {
						$timerested = " (should have rested ".$days." days and ".$hrs."hrs)";
					}
				  ?>
              <option value="<?php echo $conda->StaffId;?>"><?php echo $conda->StaffName.$timerested;?></option>
              <?php }}
			  if ($wudbeavalconductors<>NULL)
			  {
			  foreach($wudbeavalconductors as $cond){
				   $date = $depdate.' '.$deptime;
					$now = new DateTime($date);
					$ref = new DateTime($cond->date_available_from.' '.$cond->time_available_from);
					$diff = $now->diff($ref);
					$days = $diff->d;
					$hrs = $diff->h;
					if ($days==0)
					{
						$timerested = " (will have rested ".$hrs."hrs)";
					}
					else {
						$timerested = " (will have rested ".$days." days and ".$hrs."hrs)";
					}
			  ?>
               <option value="<?php echo $cond->StaffId;?>"><?php echo $cond->StaffName.$timerested;?></option>
               <? }}
			    if ($wudbeavalconductors==NULL && $wudbeavalconductorsactive==NULL)
				{
			   ?>
               <option value="">No Conductor Available!</option>
               <? } ?>
            </select>
			<?php
		  if ($sched->Conductor<>NULL)
		  {
			  $cn = $this->bus_model->getonerowfromonetbl('bus_staff','StaffId',$sched->Conductor);
			  echo "<strong>Already Assigned Conductor: ".$cn->StaffName."</strong>";
		  }
		  ?>
            <input type="hidden" name="scheduleid" value="<?php echo $scheduleid;?>" /></td>
        </tr>
        <tr>
          <td colspan="2" class="labels">
          <?php
		  if ($sched->Conductor<>NULL)
		  {
			  $drv = $this->bus_model->getDriversFromDriverSchedules($scheduleid);
			  if ($drv<>NULL)
			  {
				  echo "<u><strong>Already Assigned Drivers: </strong></u><br />";
				  foreach ($drv as $d)
				  {
					  echo $d->StaffName."<br />";
				  }
			  }
		  }
		  ?>
           <table class="table1">
            <thead id="pageFooter">
			<tr align="left">
			  <th width="221">Driver</th>
			  <th width="121">Town From</th>
			  <th width="136">Town To</th>
			  </tr>
		</thead>
            <tr id="input1" class="clonedInput">
            <td>
  
 
<div id="input_ln" class="ui-widget">
<?php echo form_error('driver'); ?>
<select name="driver[]" id="combobox">
             <option value=""></option> 
              <?php 
			  if ($wudbeavaldriversactive<>NULL)
			  {
			  foreach($wudbeavaldriversactive as $drivera){
				  $date = $depdate.' '.$deptime;
					$now = new DateTime($date);
					$ref = new DateTime($drivera->date_available_from.' '.$drivera->time_available_from);
					$diff = $now->diff($ref);
					$days = $diff->d;
					$hrs = $diff->h;
					if ($days==0)
					{
						$timerested = " (should have rested ".$hrs."hrs)";
					}
					else {
						$timerested = " (should have rested ".$days." days and ".$hrs."hrs)";
					}
				  ?>
              <option value="<?php echo $drivera->StaffId;?>"><?php echo $drivera->StaffName.$timerested;?></option>
              <?php }}
			  if ($wudbeavaldrivers<>NULL)
			  {
			  foreach($wudbeavaldrivers as $driver){
				   $date = $depdate.' '.$deptime;
					$now = new DateTime($date);
					$ref = new DateTime($driver->date_available_from.' '.$driver->time_available_from);
					$diff = $now->diff($ref);
					$days = $diff->d;
					$hrs = $diff->h;
					if ($days==0)
					{
						$timerested = " (will have rested ".$hrs."hrs)";
					}
					else {
						$timerested = " (will have rested ".$days." days and ".$hrs."hrs)";
					}
			  ?>
               <option value="<?php echo $driver->StaffId;?>"><?php echo $driver->StaffName.$timerested;?></option>
               <? }}
			    if ($wudbeavaldrivers==NULL && $wudbeavaldriversactive==NULL)
				{
			   ?>
               <option value="">No Driver Available!</option>
               <? } ?>
          </select></div><!-- end #input_ln -->
                        
                        
                                    </td>
                                    <td><div id="towncodef">
                                    <select name="towncodef[]" id="towncodef" class="smallselect">
              <?php 
			  $townsdb = $this->bus_model->getTownsByTownids($rtowns);
			  if($townsdb<>NULL) {
			  foreach($townsdb as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }}?>
          </select>
                                    </div>
                                    </td>
                                    <td> <select name="towncodet[]" id="towncodet" class="smallselect">
              <?php 
			  $townsdb = $this->bus_model->getTownsByTownids($rtowns);
			  if($townsdb<>NULL) {
			  foreach($townsdb as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }}?>
          </select></td>
             </tr><!-- end #input1 -->
 </table>
            <div id="addDelButtons">
                <input type="button" id="btnAdd" value="add section"> <input type="button" id="btnDel" value="remove section above">
            </div>
          </td>
        </tr>


        <tr>
          <td></td>
          <td width="931"><input  type="submit" name="submit" value="Update Schedule" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
<?php echo form_close();?>
     <?php } else { echo "<p>".$message."</p>";?>
<table width="100%">
  <tr>
    <td width="266">&nbsp;</td>
    <td width="879">      <form action='javascript:void(0);' onclick="window.close(); window.opener.location.reload();">
        <p>
          <input name="Button"  type='submit'  id='close' value="Close Window" />
          &nbsp;&nbsp; </p>
      </form></td>
  </tr>
</table>
<?php }?>
</body>
</html>