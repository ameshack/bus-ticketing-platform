<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Bus Ticket</title>
<link href="<?=base_url()?>css/style.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">

body, html, h1,ul, h2, h3, p,span,td,tr,form,table,thead,tbody {
	margin: 0px;
	padding: 0px;
	/*font-size: 1em;*/
	font-weight: normal;
	background-position: left top;
	vertical-align: top;
} 
@page {
  size: A6 landscape;
}

body {
    margin:0;
    width: auto;
    font-family:Verdana, Geneva, sans-serif; 
	size: A6 landscape;
	padding-left:5px;
	
	
}
#pager {
	page-break-after:always;
}
#printwrapper {
	width:788px;
	font-size:12px;
	display:block;
	margin:0;
	border:1px solid #000;
	float: left;
}
#topmost {
	width:788px;
	height:46px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
	
}
#thirdtopmost {
	width:788px;
	height:36px;
	font-size:10px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
}
#secondtop {
	width:788px;
	height:27px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
	
}
#bottommost {
	width:788px;
	height:40px;
	font-size:12px;
	margin:0;
	float:left;
	
}
#nleftpanestop {
	width:200px;
	height:27px;
	padding:0px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nleftpanes3top {
	width:250px;
	height:36px;
	padding:0px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nleftpanes4top {
	width:200px;
	height:36px;
	padding:0px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nrightpanes3top {
	width:286px;
	height:34px;
	padding:0px;
	float:left;
	text-align: left;
}
#nrightpanes5top {
	width:788px;
	height:40px;
	padding:0px;
	float:left;
	text-align:left;
}
#nrightpanes4top {
	width:587px;
	height:34px;
	padding:0px;
	float:left;
	text-align: left;
}
#nrightpanestop {
	width:386px;
	height:26px;
	padding:0px;
	float:left;
	text-align: left;
}
#nleftpane {
	width:260px;
	height:45px;
	padding:0px;
	float:left;
	text-align: left;
}
#nrighttoppane {
	width:528px;
	height:45px;
	padding:0px;
	float:left;
	text-align: right;
}
#nleftpane1 {
	width:180px;
	height:205px;
	padding:0px;
	float:left;
	border:1px solid #000;
}
#nleftpane2 {
	width:280px;
	height:45px;
	padding:0px;
	float:left;
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-right:1px solid #000;
}

#nleftpane3 {
	width:240px;
	height:105px;
	padding:5px;
	float:left;
	
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-bottom:1px solid #000;
}

#nleftpane5 {
	width:520px;
	height:105px;
	padding:5px;
	float:left;
	border:1px solid #000;
}

#nleftpane4 {
	width:432px;
	height:60px;
	padding:5px;
	float:left;
	border:1px solid #000;
}
.t90
{

/* Rotate div */

transform:rotate(-90deg);
-ms-transform:rotate(-90deg);  /*Internet Explorer */
-moz-transform:rotate(-90deg); /*Firefox */
-webkit-transform:rotate(-90deg);  /*Safari and Chrome*/
-o-transform:rotate(-90deg); /* Opera */
}
.style1 {font-weight: bold}
</style>
</head>
<body onLoad="window.print(); window.close();">
<?
$tid = explode(",",$tids);
$tot = count($tid);
for($i=0;$i<$tot;$i++)
{
	$row = $this->bus_model->gettickettoprint($tid[$i]);
?>
<div id="pager">
<br>
<center>
<br>
<p>&nbsp;</p><p>&nbsp;</p><br>
<div id="printwrapper">
<div id="topmost">
<div id="nleftpane">
<p>Ticket No. <strong>1223311</strong></p>
<p>NAME <strong>Marcel Auja</strong></p>
<p>ID <strong>25973919</strong></p>


    </div>

<div id="nrighttoppane">
<p>BUS <strong>KBP 001E</strong></p>
<p>MOBILE. 0710228024</strong></p>
</div>
</div>
<div id="secondtop">
<div id="nleftpanestop">
   <p>FROM <br>
     <strong>Nairobi</strong></p>
    </div>
    <div id="nleftpanestop">
    <p>TO<br>
      <strong>Mombasa</strong></p>
    </div>
    <div id="nrightpanestop"><p>SEAT <br>
        <strong>1</strong></p>
     </div>

</div>
<div id="thirdtopmost">
<div id="nleftpanes3top">
    <p>DATE OF ISSUE<br>
      <strong><?php echo date('d/m/Y');?></strong></p>
    </div>
    <div id="nleftpanes3top">
     <p>DATE OF TRAVEL<br>
       <strong><?php echo date('d/m/Y');?></strong></p>
    </div>
    <div id="nrightpanes3top">
    <p>FARE PAID<br>
      <strong><?php echo number_format(2200);?></strong></p>
    </div>

</div>
<div id="thirdtopmost">
<div id="nleftpanes4top">
     <p>REPORTING TIME<br>
       <strong><?php echo  date('g:i A',strtotime('21:30:00'));?></strong></p>
</div>
    <div id="nleftpanes3top">
     <p>DEPARTURE TIME<br>
       <strong><?php echo date('g:i A',strtotime('22:00:00'));?></strong></p>
        </div>
    <div id="nrightpanes3top">
 <p><strong>DRIVER NOT ALLOWED TO PICK ANY PASSENGER ALONG THE WAY. INCASE THIS HAPPENS, PLEASE CALL 0738126849</strong></p>
    </div>
</div>
<div id="bottommost">
  <div id="nrightpanes5top">
     <p>SERVED BY<br>
       <span class="style1">
       
	   mwazito
	
       </span></p>
  </div>

</div>
</div><br clear="all">

<p style="text-align:left; font-size:10px;">The management will not accept responsibility for any LOSS,DAMAGE or DELAY.Ticket Money will not be<br>
refunded.<br>
No refund will be made to passengers left behind. Passengers must take care of the their luggage.<br>
15 kilos of luggage free, any additional weight will be charged.<br>
Spirits, alcohol and smoking NOT ALLOWED.</p>
</center>
</div>
<? } ?>


</body>
</html>