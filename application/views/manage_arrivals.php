<?php
$data = array(
'title'=>'Manage Arrivals',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage arrivals',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Cash Reconciliation</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Arrivals</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/search_manifest/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Manifest</th>
		  </tr>
		</thead>
        <tr>
          <td valign="top">
          Manifest No <br /> <input name="manifestno" id="long_input" type="text" /></td>
        </tr>
        <tr>
          <td style="text-align:center;"><input  type="submit" name="submit" value="Search" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
        </table>
      <?php echo form_close(); ?>
      <? 
	  if ($manifestlist<>NULL)
	  {
	  if ($manifests<>NULL) { ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th colspan="6" align="center">&nbsp;</th>
		  </tr>
			<tr align="left">
			  <th width="107">No</th>
			  <th width="284">Route</th>
			  <th width="161">Bus </th>
			  <th width="243">Driver(s)</th>
			  <th width="226">Conductor</th>
	          <th width="160">Options</th>
		  </tr>
		</thead>
        <?php 
		$item = $manifests;
		$cnt = 1;
		//foreach ($manifests as $item):  ?>
			<tr>
			  <td align="left"><? echo $cnt; ?></td>
				<td align="left"><?php echo $item->TownFrom." - ".$item->TownTo;?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php //echo $item->MakeName; ?></td>
              <td align="left"><?php //echo $item->TotalSeats; ?></td>
			  <td align="left"><?php 
			  if ($item->arrivalstatus==0)
			  {
				  $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
				  echo anchor_popup('admins/confirm_arrival/'.$item->manifest_id.'/#tab3','Confirm',$atts);
			  }
			  else {
				  echo "Arrival Confirmed";
			  }
			  ;?> </td>
			</tr>
		<?php  ?>
	 </table>
     <? } else {
		 echo "No manifest with that number. Try again.";
	 }
	 }?>
      </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
