<?php
$data = array(
'title'=>'Manage Vouchers',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage voucher',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#vdatefrom').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#vdateto').datepicker({  
        duration: '',  
        showTime: false, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });  
    $('#vdatefrome').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#vdatetoe').datepicker({  
        duration: '',  
        showTime: false, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
});  
</script> 

<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Vouchers</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Vouchers</a></li>
      <li><a href="#create" name="#tab2">Add Voucher</a></li>
      <li><a href="#edit" name="#tab3">Edit Voucher</a></li>
      <li><a href="#voucher" name="#tab4">Add Voucher Items</a></li>
      <li><a href="#voucheritems" name="#tab5">View Voucher Items</a></li>
      <li><a href="#delete" name="#tab6">Delete Voucher</a></li>
    </ul>
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($vouchers)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="160">Voucher</th>
			  <th width="120">Amount</th>
			  <th width="147">Date From</th>
			  <th width="124">Date To</th>
	          <th width="119">Quantity</th>
	          <th width="310">Options</th>
		  </tr>
		</thead>
        <?php foreach ($vouchers as $item):?>
			<tr>
				<td align="left"><?php echo anchor('admins/view_voucher_items/'.$item->vouchers_id.'/#tab5',$item->vname);?></td>
			  <td align="left"><?php echo $item->vamount; ?></td>
              <td align="left"><?php echo $item->vdatefrom; ?></td>
              <td align="left"><?php echo $item->vdateto; ?></td>
			  <td align="left"><?php echo $item->quantity; ?></td>
			  <td align="left"><?php echo anchor('admins/edit_voucher/'.$item->vouchers_id.'/#tab3','Edit');?> | <?php echo anchor('admins/crud_voucher_items/'.$item->vouchers_id.'/#tab4','Create Voucher Items');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no vouchers created yet!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_vouchers');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Voucher Name:</td>
          <td width="519"><span class="red"><?php echo form_error('vname'); ?></span>
            <input type="text" name="vname"  id="vname" class="long_input" value="<?php echo $this->input->post('vname');?>"/></td>
        </tr>
        
        <tr>
          <td class="labels">Quantity:</td>
          <td><span class="red"><?php echo form_error('quantity'); ?>
            <input type="text" name="quantity"  id="quantity" class="long_input" value="<?php echo $this->input->post('quantity');?>"/>
          </span></td>
        </tr>
        <tr>
          <td class="labels">Date From:</td>
          <td><span class="red"><?php echo form_error('vdatefrom'); ?></span>
            <input type="text" name="vdatefrom"  id="vdatefrom" class="long_input" value="<?php echo $this->input->post('vdatefrom');?>"/></td>
        </tr>
 <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('vdateto'); ?></span>
            <input type="text" name="vdateto"  id="vdateto" class="long_input" value="<?php echo $this->input->post('vdateto');?>"/></td>
        </tr>    
 <tr>
          <td class="labels">Amount:</td>
          <td><span class="red"><?php echo form_error('vamount'); ?></span>
            <input type="text" name="vamount"  id="vamount" class="long_input" value="<?php echo $this->input->post('vamount');?>"/></td>
        </tr>           
                
        <tr>
          <td class="labels">Description:</td>
          <td><span class="red"><?php echo form_error('description'); ?></span>
          <textarea name="description"  class='animated' id="long_input"><?php echo $this->input->post('description');?></textarea></td>
        </tr>
        
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Voucher" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($row)){?>
	<?php echo form_open('admins/edit_voucher_update');?>
          <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        
<tr>
          <td width="177" class="labels">Voucher Name:</td>
          <td width="519"><span class="red"><?php echo form_error('vname'); ?></span>
            <input type="text" name="vname"  id="vname" class="long_input" value="<?php echo $row->vname;?>"/></td>
        </tr>
          <tr>
          <td class="labels">Quantity:</td>
          <td><span class="red"><?php echo form_error('quantity'); ?>
            <input type="text" name="quantity"  id="quantity" class="long_input" value="<?php echo $row->quantity;?>"/>
          </span></td>
        </tr>
          <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('vdatefrom'); ?></span>
            <input type="text" name="vdatefrom"  id="vdatefrome" class="long_input" value="<?php echo $row->vdatefrom;?>"/></td>
          </tr>
          <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('vdateto'); ?></span>
            <input type="text" name="vdateto"  id="vdatetoe" class="long_input" value="<?php echo $row->vdateto;?>"/></td>
        </tr> 
                
                <tr>
          <td class="labels">Amount:</td>
          <td><span class="red"><?php echo form_error('vamount'); ?></span>
            <input type="text" name="vamount"  id="vamount" class="long_input" value="<?php echo $row->vamount;?>"/></td>
        </tr>
                <tr>
          <td class="labels">Description:</td>
          <td><span class="red"><?php echo form_error('description'); ?></span>
          <textarea name="description"  class='animated' id="long_input"><?php echo $row->description;?></textarea></td>
        </tr>
   
        <tr>
          <td><input type="hidden" name="voucherid" value="<?php echo $voucherid;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Voucher" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Vouchers\' tab!</p>';}; ?>
</div>
<div id="tab4">
	<?php if(!empty($voucherid)){?>
	<?php echo form_open('admins/create_voucher_items');?>
    <table width="100%">
  <tr>
    <td width="169">Customer:</td>
    <td width="519"><?php echo form_error('CustomerId'); ?></span>
    <?php if($customers<>NULL){?>
            <select name="CustomerId" id="CustomerId">
              <option disabled="disabled" selected="selected">---select customer---</option>
              <?php foreach($customers as $c){?>
              <option value="<?php echo $c->card_details_id;?>"><?php echo $c->f_name.' '.$c->f_name.' ['.$c->idno_or_passportno.']';?></option>
              <?php }?>
          </select><?php } else{ echo'Sorry, no customers added yet.';}?></td>
  </tr>
</table>

      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">First Name:</td>
          <td width="519"><span class="red"><?php echo form_error('f_name'); ?></span>
            <input type="text" name="f_name"  id="f_name" class="long_input" value="<?php echo $this->input->post('f_name');?>"/></td>
        </tr>
        
        <tr>
          <td class="labels">Last Name:</td>
          <td><span class="red"><?php echo form_error('l_name'); ?>
            <input type="text" name="l_name"  id="l_name" class="long_input" value="<?php echo $this->input->post('l_name');?>"/>
          </span></td>
        </tr>
        <tr>
          <td class="labels">Identity:</td>
          <td><span class="red"><?php echo form_error('idno_or_passportno'); ?></span>
            <input type="text" name="idno_or_passportno"  id="idno_or_passportno" class="long_input" value="<?php echo $this->input->post('idno_or_passportno');?>"/></td>
        </tr>
 <tr>
          <td class="labels">Phone Number:</td>
          <td><span class="red"><?php echo form_error('phoneno'); ?></span>
            <input type="text" name="phoneno"  id="phoneno" class="long_input" value="<?php echo $this->input->post('phoneno');?>"/></td>
        </tr>    
 <tr>
          <td class="labels">Email:</td>
          <td><span class="red"><?php echo form_error('email'); ?></span>
            <input type="text" name="email"  id="email" class="long_input" value="<?php echo $this->input->post('email');?>"/></td>
        </tr>           
                

        <tr>
          <td><input type="hidden" name="voucherid" value="<?php echo $voucherid;?>" /></td>
          <td><input  type="submit" name="submit" value="Create Voucher Item" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
            <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Vouchers\' tab!</p>';}; ?>
</div>
<div id="tab5">
    	 <?php if(!empty($voucheritems)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="306">Customer</th>
			  <th width="122">Voucher</th>
			  <th width="92">Amount</th>
			  <th width="111">Date From</th>
			  <th width="116">Date To</th>
	          <th width="112">Redeemed</th>
	          <th width="121">Voucher Code</th>
		  </tr>
		</thead>
        <?php foreach ($voucheritems as $item): if($item->redeemed==0): $redeemed= 'No' ;else: $redeemed='Yes'; endif;?>
			<tr>
			  <td align="left"><?php echo $item->f_name.' '.$item->f_name.'['.$item->idno_or_passportno.']';?></td>
				<td align="left"><?php echo $item->vname;?></td>
			  <td align="left"><?php echo $item->vamount; ?></td>
              <td align="left"><?php echo $item->vdatefrom; ?></td>
              <td align="left"><?php echo $item->vdateto; ?></td>
			  <td align="left"><?php echo $redeemed; ?></td>
			  <td align="left"><?php echo $item->voucher_code; ?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no vouchers items for selected voucher created yet!</p>';}; ?>
    </div>
    <div id="tab6">
	<?php if(!empty($vouchers)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Voucher </th>
			  <th width="169">Amount</th>
			  <th width="125">Date From</th>
			  <th width="147">Date To</th>
	          <th width="107">Quantity</th>
	          <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($vouchers as $item):?>
			<tr>
				<td align="left"><?php echo $item->vname;?></td>
			  <td align="left"><?php echo $item->vamount; ?></td>
              <td align="left"><?php echo $item->vdatefrom; ?></td>
              <td align="left"><?php echo $item->vdateto; ?></td>
			  <td align="left"><?php echo $item->quantity; ?></td>
			  <td align="left"><?php echo anchor('admins/delete_voucher/'.$item->vouchers_id.'','X',array('class' => 'delete','title'=>''.$item->vouchers_id.'')); ?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no vouchers created yet!</p>';}; ?>
</div>
</div>
  <!-- end tabs -->
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
