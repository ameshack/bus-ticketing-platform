<?
$data = array(
'title'=>'Add User Account',
'keywords'=>' ',
'description'=>' ',
'selected'=>'User Account Update',
'img'=>'',
'load_side_menu'=>false
);
 $this->load->view('extras/upper',$data);?>
 <div id="formy">
	
	<!-- Intro Content -->
	<!-- Main Content -->
<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Add User</h2>

			    <?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url()); ?>  	
					<fieldset>
					<legend>Personal Details</legend>
                   <p class="info_req">
        <label for="employee_details_id">Station:</label><br>

        <?php echo form_error('t_station'); ?>
            <select name="t_station" id="t_station" class="smallselect" onChange="displaytermini(this.value)">
              <option disabled="disabled" selected="selected">---select station---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select>
</p><div class="info_req" id="termini">
        <label for="t_termini">Termini:</label><br>

        <?php echo form_error('t_termini'); ?>
            <select name="t_termini" id="t_termini" class="smallselect">
              <option disabled="disabled" selected="selected">---select termini---</option>
              <?php foreach($termini as $tm){?>
              <option value="<?php echo $tm->TerminusId;?>"><?php echo $tm->TerminusName;?></option>
              <?php }?>
          </select>
</div>
							<p class="info_req">
								<label for="first_name">First Name:</label><br>

								<input type="text" id="first_name" name="register_first_name" value="<?php echo $this->input->post('first_name');?>"/>
							</p>
							<p class="info_req">
								<label for="last_name">Last Name:</label><br>

								<input type="text" id="last_name" name="register_last_name" value="<?php echo set_value('register_last_name');?>"/>
							</p>
						
					</fieldset>
					
					<fieldset>
					<legend>Contact Details</legend>
						
							<p class="info_req">
								<label for="phone_number">Phone Number:</label><br>

								<input type="text" id="phone_number" name="register_phone_number" value="<?php echo set_value('register_phone_number');?>"/>
							</p>

					</fieldset>
					
					<fieldset>
						<legend>Login Details</legend>
						<p class="info_req">
								<label for="email_address">Email Address:</label><br>

								<input type="text" id="email_address" name="register_email_address" value="<?php echo set_value('register_email_address');?>" class="tooltip_trigger"
									title="This demo requires that upon registration, you will need to activate your account via clicking a link that is sent to your email address."
								/>
					</p>
							<p class="info_req">
								<label for="username">Username:</label><br>

								<input type="text" id="username" name="register_username" value="<?php echo set_value('register_username');?>" class="tooltip_trigger"
									title="Set a username that can be used to login with."
								/>
							</p>
							<p>							
								<small>
									Password length must be more than <?php echo $this->flexi_auth->min_password_length(); ?> characters in length.<br/>Only alpha-numeric, dashes, underscores, periods and comma characters are allowed.
								</small>
							</p>
							<p class="info_req">
								<label for="password">Password:</label>
								<br>
								<input type="password" id="password" name="register_password" value="<?php echo set_value('register_password');?>"/>
							</p>
							<p class="info_req">
								<label for="confirm_password">Confirm Password:</label><br>

								<input type="password" id="confirm_password" name="register_confirm_password" value="<?php echo set_value('register_confirm_password');?>"/>
							</p>
                        							<p>
								
								<input type="submit" name="register_user" id="submit" value="Submit" class="link_button large"/>
							</p>
					</fieldset>
					
					<?php echo form_close();?> </div>
		</div>
   </div>	
	

</div>
<? $this->load->view('extras/lower') ?>