<?php
$data = array(
'title'=>'Change Email',
'keywords'=>' ',
'description'=>' ',
'selected'=>'change email',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<p class="title">Modern Coast Booking <span class="red">>> </span>Change Email</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
<div class="flexihome">
				<h2>Change Email via Email Verification</h2>
				<a href="<?php echo $base_url;?>auth_public/update_account">Update Account Details</a>
<div class="content clearfix">
			<div class="col100">
			  <p>The user submits their new email address, an email is sent to the user, if they do not click the verification link in that email, their account is not updated.</p>
			</div>		
		</div>
			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());	?>  	
						<ul>
							<li class="info_req">
								<label for="email_address">New Email Address:</label>
								<input type="text" id="email_address" name="email_address" value="<?php echo set_value('email_address');?>"/>
							</li>
							<li>
								<label for="submit"></label>
								<input type="submit" name="update_email" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
				<?php echo form_close();?>
			</div>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
