<?php
$data = array(
'title'=>'Resend Activation Token',
'keywords'=>' ',
'description'=>' ',
'selected'=>'resend activation token',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<p class="title">Modern Coast Booking <span class="red">>> </span>Resend Activation Token</p>

<div class="flexihome">

			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());?>  	
					<div class="w100 frame">
						<ul>
							<li class="info_req">
								<label for="identity">Email or Username:</label>
								<input type="text" id="identity" name="activation_token_identity" value="" class="tooltip_trigger"
									title="Please enter either your email address or username defined during registration."
								/>
							</li>
							<li>
								<label for="submit"></label>
								<input type="submit" name="send_activation_token" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
					</div>	
				<?php echo form_close();?>
			</div>
</div>
<?php $this->load->view('extras/scripts'); ?> 
<?php  $this->load->view('extras/lower'); ?>