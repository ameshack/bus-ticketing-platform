<?php
$data = array(
'title'=>'Manage Loyalty Points',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage loyalty points',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Loyalty</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Gains & Burns</a></li>
      <li><a href="#edit" name="#tab2">Points History</a></li>
    </ul>  
  <div id="content">
    <div id="tab1">
<?php echo form_open('admins/gains_burns');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Customer Card No:</td>
          <td width="519"><span class="red"><?php echo form_error('custcardno'); ?></span>
            <input name="custcardno" type="text" autofocus="autofocus" />
            </td>
        </tr>
        

        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Search" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <?php if ($rep<>NULL) { ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th height="23" colspan="3" style="text-align:center;">Customer Name: <?php echo $custname; ?></th>
</tr>
			<tr align="left">
			  <th height="23">Points Gained</th>
			  <th>Points Burned</th>
	          <th>Points Balance</th>
	      </tr>
		</thead>
        
			<tr>
			  <td align="left"><?php echo $pointsgained;?></td>
			  <td align="left"><?php echo $pointsburned;?></td>
			  <td align="left"><?php echo $balpoints;?></td>
	    </tr>
	
	 </table>
      <?php } ?> 
    </div>
    <div id="tab2">
<?php echo form_open('admins/points_history/#tab2');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td class="labels">Customer Card No: <span class="red"><?php echo form_error('custcardno1'); ?></span>
            <input name="custcardno1" type="text" value="<?php echo $this->input->post('custcardno1');?>" /></td>
          <td>Date From: <span class="red"><?php echo form_error('datef'); ?></span>
            <input name="datef" type="text" id="Disc_From_Date" value="<?php echo $this->input->post('datef');?>" />
            </td>
            <td>Date To: <span class="red"><?php echo form_error('datet'); ?></span>
            <input name="datet" type="text" id="Disc_To_Date" value="<?php echo $this->input->post('datet');?>" />
            </td>
        </tr>
        

        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Search Points History" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
          <td></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <?php if ($rep1<>NULL) { ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th height="23" colspan="3" style="text-align:center;">Customer Name: <?php echo $custname; ?></th>
</tr>
			<tr align="left">
			  <th height="23">Date</th>
			  <th>Type</th>
	          <th>Points</th>
          </tr>
		</thead>
        <?php
		if ($phs==NULL)
		{
			echo "<tr><td colspan=\"4\" style=\"text-align:center;\">No recorded points gained!</td></tr>";
		}
		else {
			$totpoints = 0;
			foreach ($phs as $s)
			{
		?>
			<tr>
			  <td align="left"><?php echo date('d/m/Y g:i A',strtotime($s->PointsDate));?></td>
			  <td align="left"><?php 
			  if ($s->Points>=0)
			  {
				  echo "Gain";
			  }
			  else {
				  echo "Burn";
			  }
			  ?></td>
			  <td align="left"><?php
			  $totpoints += $s->Points;
			   echo abs($s->Points);?></td>
        </tr>
	 <?php }} ?> 
     <thead id="pageFooter">
     <tr>
<th height="23" colspan="3" style="text-align:right;">Points Balance: <?php
$kes = $pb->kes *$totpoints ;
$ugx = $pb->ugx *$totpoints ;
$tzs = $pb->tzs *$totpoints ;
 echo $totpoints." <br />
<strong><u>Worth</u> </strong> <br />Ksh ".number_format($kes)."<br />Ush ".number_format($ugx)."<br />Tsh ".number_format($tzs);
 ?></th>
</tr></thead>
	 </table>
      <?php } ?> 
    </div>
    </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
