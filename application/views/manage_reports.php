<?php
$data = array(
'title'=>'Manage Reports',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage reports',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true 
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true
     });  
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Reports</p>

<div id="rightcontentwide">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Generate Report</a></li>
      <li><a href="#create" name="#tab2">Web Report</a></li>
      
       
    </ul>  
  <div id="content">
   
    <div id="tab1">
	<?php 
	$nt = array('target'=>'_blank');
	echo form_open('admins/gen_report/#tab2',$nt);?>
    	 <h2>Use fields below to generate desired report.</h2>
         <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td valign="top" style="vertical-align:top;">Booking Channel <br /> 
    <select name="channel">
    <option selected="selected" value="0">All</option>
    <?php foreach($channels as $ch){?>
    <option value="<?php echo $ch->channels_id;?>"><?php echo $ch->channel;?></option>
    <? }?>
    </select></td>
    <td valign="top" style="vertical-align:top;">Date From <br />
      <input type="text" name="datef"  id="Disc_From_Date"  readonly="readonly" class="long_input" value="<?php if (isset($_POST['datef'])) {echo $_POST['datef']; } else { echo date('Y-m-d')." 00:00"; }?>"/></td>
    <td valign="top" style="vertical-align:top;">Date To <br /> 
      <input type="text" name="datet"  id="Disc_To_Date"  readonly="readonly" class="long_input" value="<?php if (isset($_POST['datet'])) {echo $_POST['datet']; } else { echo date('Y-m-d H:i'); }?>"/></td>
  </tr>
  <tr>
    <td rowspan="3" valign="top" style="vertical-align:top;">    Town From <br /><select name="townf" onchange="displayterminiclerks(this.value)">
              <option selected="selected" value="0">All</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select>
            <br />
              Town To <br /><select name="townt">
                <option selected="selected" value="0">All</option>
                <?php foreach($towns as $town){?>
                <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
                <?php }?>
                </select><br />
                <div id="terminiclerk">
              Terminal <br /> <select name="terminus">
                <option selected="selected" value="0">All</option>
                <?php foreach($termini as $t){?>
                <option value="<?php echo $t->TerminusId;?>"><?php echo $t->TerminusName;?></option>
                <?php }?>
                </select><br />
              Clerk <br />
              <select name="clerk">
                <option selected="selected" value="0">All</option>
                <?php 
			  if ($clerks<>NULL)
			  {
			  foreach($clerks as $c){?>
                <option value="<?php echo $c->uacc_id;?>"><?php echo $c->upro_first_name." ".$c->upro_last_name;?></option>
                <?php }}?>
                </select>
          </div></td>
    <td valign="top" style="vertical-align:top;"> Bus Type <br /><select name="bustypelkp" id="bustypelkp" >
    <option selected="selected" value="0">All</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
    <td valign="top" style="vertical-align:top;">Paymode <br /> 
    <select name="paymode">
    <option selected="selected" value="0">All</option>
    <?php foreach($paymodes as $py){?>
    <option value="<?php echo $py->paymodes_id;?>"><?php echo $py->paymodes;?></option>
    <? } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td valign="top" style="vertical-align:top;">Ticket Type <br />
      <select name="tickettype" id="tickettype" >
        <option value="All">All</option>
        <option selected="selected" value="Active">Active</option>
        <option value="Reserved">Reserved</option>
        <option value="Open">Open</option>
        <option value="Redeemed">Redeemed</option>
        <option value="Cancelled">Cancelled</option>
      </select></td>
    <td valign="top" style="vertical-align:top;">
    With ticket details? <br />
    <input name="tickdet" type="radio" value="1" checked="checked" /> Yes <br />
    <input name="tickdet" type="radio" value="2" /> No <br />
    </td>
  </tr>
  <tr>
    <td valign="top" style="vertical-align:top;">Order By:<br />
      <input name="seatsasc" type="checkbox" value="1" checked="checked" />Seats Ascending <br />
      <input name="seatsdesc" type="checkbox" value="2" />Seats Descending <br />
      <input name="dateasc" type="checkbox" value="3" />Date Ascending <br />
      <input name="datedesc" type="checkbox" value="4" />Date Descending <br />
    </td>
    <td valign="top" style="vertical-align:top;">
    Output Format: <br />
    <input name="outputype" type="radio" value="desktop" checked="checked" /> Desktop <br />
    <input name="outputype" type="radio" value="printr" /> Print <br />
    <input name="outputype" type="radio" value="excel" /> Excel <br />
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center" valign="top" style="text-align:center;"><input  type="submit" name="submit" value="Generate Report" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
    </tr>
      </table>

          <?php echo form_close();?>
    </div>
   
    <div id="tab2">
    <? if ($rep==NULL){
		echo '<p class="red">Please generate a desktop report in the \'Generate Report\' tab!</p>';
	}
	else {
		?>

    <table width="100%" cellpadding="0" cellspacing="0" class="table1">
    <thead id="pageFooter">
    <tr><th colspan="10" style="text-align:center;"><strong><?php echo $reporttitle;?></strong></th></tr>
    </thead>
    <thead id="pageFooter">
  <tr>
    <th scope="col">Ticket No</th>
    <th scope="col">Customer</th>
    <th scope="col">Route</th>
    <th scope="col">Paymode</th>
    <th scope="col">Seat No</th>
    <th scope="col">Bus</th>
    <th scope="col">TravelDate</th>
    <th scope="col">Booked By</th>
    <th scope="col">Amount</th>
  </tr>
  </thead>
  <?php
  if ($tickets==NULL)
  {
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No transactions generated</td></tr>";
  }
  else {
	  $totkes = 0;
	  $totugx = 0;
	  $tottzs = 0;
	  $totusd = 0;
	   if ($tickdet==2)
		  {
	  foreach ($tickets as $t)
	  {
		 
			if ($t->t_currency==1)
			{
				$totkes +=$t->amount;
			}
			if ($t->t_currency==2)
			{
				$totugx +=$t->amountug;
			}
			if ($t->t_currency==3)
			{
				$tottzs +=$t->amounttz;
			}
			if ($t->t_currency==4)
			{
				$totusd +=$t->amountusd;
			}
		  }
		  }
		  else {
			foreach ($tickets as $t)
	  {  
		  
  ?>
  <tr>
    <td><?php echo $t->newticketno; ?></td>
    <td><?php echo $t->clientname; ?></td>
    <td><?php echo $t->TownFrom."-".$t->TownTo; ?></td>
    <td><?php echo $t->paymodes; ?></td>
    <td><?php echo $t->seatno; ?></td>
    <td><?php echo $t->bus_number; ?></td>
    <td><?php echo $t->dateonly; ?></td>
    <td><?php echo $t->t_username; ?></td>
    <td><?php 
	if ($t->t_currency==1)
	{
		$totkes +=$t->amount;
		echo "Kshs ".number_format($t->amount);
	}
	if ($t->t_currency==2)
	{
		$totugx +=$t->amountug;
		echo "Ushs ".number_format($t->amountug);
	}
	if ($t->t_currency==3)
	{
		$tottzs +=$t->amounttz;
		echo "Tshs ".number_format($t->amounttz);
	}
	if ($t->t_currency==4)
	{
		$totusd +=$t->amountusd;
		echo "$".number_format($t->amountusd);
	}
	 ?></td>
  </tr>
  <? } } }
  
  if ($tickets<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>Total</strong></td>
    <td><strong><? 
	if ($totkes<>0)
	{
	echo "Kshs ".number_format($totkes)."<br />";
	}
	if ($totugx<>0)
	{
	echo "Ushs ".number_format($totugx)."<br />";
	}
	if ($tottzs<>0)
	{
	echo "Tshs ".number_format($tottzs)."<br />";
	}
	if ($totusd<>0)
	{
	echo "$".number_format($totusd)."<br />";
	}
	?></strong></td>
  </tr>
  <? } ?>
</table>
<? } ?>
    </div>
    </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
