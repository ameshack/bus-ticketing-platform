<html>
<head>
<title>Bus Chart</title>
<style type="text/css">

body, html, h1,ul, h2, h3, p,span,td,tr,form,table,thead,tbody {
	margin: 0px;
	padding: 0px;
	/*font-size: 1em;*/
	font-weight: normal;
	background-position: left top;
	vertical-align: top;
} 
@page {
  size: A4 portrait;
}

body {
    margin:0;
    width: auto;
    font-family:Verdana, Geneva, sans-serif; 
	size: A4 portrait;
	padding-left:5px;
	
	
}
.chart {
font-size:12px;	
}
.chart td{ height: 54px };

h2 { background-color:#777; }

</style>
<script>
function newDoc()
  {
  window.location.assign("<?=site_url('admins/crud_schedules')?>")
  }
</script>
</head>
<body onLoad="window.print(); newDoc(); window.close();">
<table align="center">
  <tr>
    <th colspan="5" scope="col" align="center"><h1>MODERN COAST EXPRESS LTD - BUS CHART<hr noshade /></h1></th>
  </tr>
  <tr>
    <td width="170" align="right"><strong>Station:</strong></td>
    <td width="138"><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	echo $sc->TownName; ?></td>
    <td width="111">&nbsp;</td>
    <td width="129" align="right"><strong>Sub Station:</strong></td>
    <td width="138"><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	echo $sc->TownName; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Manifest No:</strong></td>
    <td><?php echo $manifestno; ?></td>
    <td>&nbsp;</td>
    <td align="right"><strong>Reg. No:</strong></td>
    <td><?php 
	echo $sched->RegistrationNumber; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Departure Date:</strong></td>
    <td><?php echo $sched->DepartureDateTime; ?></td>
    <td>&nbsp;</td>
    <td align="right"><strong>Route:</strong></td>
    <td><?php  
	$sc = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->From);
	$st = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $sched->To);
	echo $sc->TownName." - ".$st->TownName; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>Departure Time:</strong></td>
    <td>
    <?php
	$dp = $this->bus_model->getdeptimebytown($sched->RouteLkp,$sched->From);
	echo date('g:i A',strtotime($dp->ArrivalDeparture));
	?>
    </td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><strong>Driver(s):</strong></td>
    <td>
    <?php  
	$drvs = $this->bus_model->getresultfromonetbl('driver_schedules','ScheduleId',$schedid);
	if ($drvs<>NULL)
	{
		foreach($drvs as $drid)
		{
			$dr = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $drid->driver_id);
			echo $dr->StaffName."<br />";
		}
	}
	 ?>
    </td>
    <td>&nbsp;</td>
    <td align="right"><strong>Conductor:</strong></td>
    <td> <?php  
	$cn = $this->bus_model->getonerowfromonetbl('bus_staff', 'StaffId', $sched->Conductor);
	if ($cn<>NULL):
	echo $cn->StaffName;endif; ?></td>
  </tr>
  <tr>
    <td align="right"><strong>BUS:</strong></td>
    <td><?php echo $sched->Busno; ?></td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div id="basi">

<?php
//echo $discountedseats;
//$booked=array();
//$bsid = $this->bus_model->getonerowfromonetbl('bus_schedule', 'ScheduleId', $schedid);
//$bustype = $bsid->BusTypeLkp;
$bustype = $sched->BusTypeId;
//$schedid = $sched->ScheduleId;
//echo $schedid;
$ntickets = $this->bus_model->getticketstochart($from,$to,$schedid);
//var_dump($ntickets); exit;
function check($schedid,$seat,$from,$to){
//function check($sched->ScheduleId,$seat){
$seatsbo = "";
//$this = &getintance;
//$from = 1;
//$to = 2;
$CI = get_instance();
$ntickets = $CI->bus_model->getticketstochart($from,$to,$schedid);
//var_dump($ntickets); exit;
if ($ntickets==NULL)
{
	$seatsbo .= "";
}
else {
	foreach ($ntickets as $tc)
	{
		$seatsbo .= $tc->seatno.",";
	}
	
}
$nseatbo = trim($seatsbo,",");
$seat_array = explode(",",$nseatbo);
if (!in_array($seat, $seat_array)) {
    echo "</strong>".$seat."</strong> ";
}
else{
	//for($y=0;$y<count($seat_array
	$tck = $CI->bus_model->gettickettochart($seat,$from,$to,$schedid);
	$html = "";
	$html .= "</strong>".$seat."</strong> ".$tck->newticketno."<br />";
	$html .= $tck->clientname."<br />";
	$html .= $tck->mobileno."<br />";
	$html .= $tck->TownFrom." - ".$tck->TownTo;
echo  $html; //echo "marcel";
}

}
?>
<?php if($bustype=="2"){?>
<form id="normal">
<table width="800" height="800" class="bustable chart" border="1" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2" align="center" class="th"><h2 class="th">VIP</h2></td>
        <td rowspan="4">&nbsp;</td>
        <td colspan="2" align="center"><div id="a">
          <h2 class="th">DRIVER</h2>
        </div></td>
    </tr>
      <tr>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'1',$from,$to); ?></td>
        <td colspan="2" align="center"><h2 class="th">FIRST CLASS</h2></td>
      </tr>
      <tr>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'2',$from,$to); ?></td>
        <td width="21%" align="left"><?php  check($sched->ScheduleId,'3',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'4',$from,$to); ?></td>
       </tr>
      <tr>
        <td colspan="2" align="left"><h2 class="th">DOOR</h2></td>
        <td colspan="2" align="center"><h class="th"2>VIP</h2></td>
       </tr>
      <tr>
        <td colspan="2" align="center"><h2 class="th">FIRST CLASS</h2></td>
        <td>&nbsp;</td>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'5',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'7',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'8',$from,$to); ?></td>
        <td align="left">&nbsp;</td>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'6',$from,$to); ?></td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2 class="th">BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="left" ><?php  check($sched->ScheduleId,'9',$from,$to); ?></td>
        <td align="left" ><?php  check($sched->ScheduleId,'10',$from,$to); ?></td>
        <td rowspan="8" align="left">&nbsp;</td>
        <td align="left"><?php  check($sched->ScheduleId,'11',$from,$to); ?></td>
        <td width="23%" align="left"><?php  check($sched->ScheduleId,'12',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'13',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'14',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'15',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'16',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'17',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'18',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'19',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'20',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'21',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'22',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'23',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'24',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'25',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'26',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'27',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'28',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'29',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'30',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'31',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'32',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'33',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'34',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'35',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'36',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'37',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'38',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'39',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'40',$from,$to); ?></td>
      </tr>
      
      <tr>
        <td width="18%" align="left"><?php  check($sched->ScheduleId,'41',$from,$to); ?></td>
        <td width="19%" align="left"><?php  check($sched->ScheduleId,'42',$from,$to); ?></td>
        <td width="19%" align="left"><?php  check($sched->ScheduleId,'43',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'44',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'45',$from,$to); ?></td>
      </tr>
    </table>
  </form>
  <?php } elseif($bustype=="1"){ ?>
 
  <form id="oxygen">
 <table width="800" height="800" class="bustable chart" border="1" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2" align="left"><h2 class="th">DOOR</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="left"><h2 class="th">CONDUCTOR</h2></td>
        <td colspan="2" align="center"><h2 class="th">DRIVER</h2></td>
      </tr>
      
      <tr>
        <td colspan="2" align="center"><h2 class="th">FIRST CLASS</h2></td>
        <td colspan="2" align="center"><h2 class="th">VIP</h2></td>
       </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'3',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'2',$from,$to); ?></td>
        <td align="left">&nbsp;</td>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'1',$from,$to); ?></td>
       </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'6',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'5',$from,$to); ?></td>
        <td align="left">&nbsp;</td>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'4',$from,$to); ?></td>
       </tr>
      <tr>
        <td colspan="5" align="center"><h2 class="th">BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="left" ><?php  check($sched->ScheduleId,'10',$from,$to); ?></td>
        <td align="left" ><?php  check($sched->ScheduleId,'9',$from,$to); ?></td>
        <td width="15%" rowspan="9" align="left">&nbsp;</td>
        <td width="17%" align="left"><?php  check($sched->ScheduleId,'8',$from,$to); ?></td>
        <td width="19%" align="left"><?php  check($sched->ScheduleId,'7',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'14',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'13',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'12',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'11',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'18',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'17',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'16',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'15',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'22',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'21',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'20',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'19',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'26',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'25',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'24',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'23',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'30',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'29',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'28',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'27',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'34',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'33',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'32',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'31',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'38',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'37',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'36',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'35',$from,$to); ?></td>
      </tr>
      
      <tr>
        <td width="26%" align="left"><?php  check($sched->ScheduleId,'42',$from,$to); ?></td>
        <td width="23%" align="left"><?php  check($sched->ScheduleId,'41',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'40',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'39',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'47',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'46',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'45',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'44',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'43',$from,$to); ?></td>
      </tr>
    </table>
  </form>
   <?php } elseif($bustype=="3"){ ?>
 
  <form id="iriza">
 <table width="800" height="800" class="bustable chart" border="1" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2" align="left"><h2 class="th">DOOR</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="left"><h2 class="th">CONDUCTOR</h2></td>
        <td colspan="2" align="center"><h2 class="th">DRIVER</h2></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><h2 class="th">BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="left" ><?php  check($sched->ScheduleId,'4',$from,$to); ?></td>
        <td align="left" ><?php  check($sched->ScheduleId,'3',$from,$to); ?></td>
        <td width="20%" rowspan="11" align="left">&nbsp;</td>
        <td width="14%" align="left"><?php  check($sched->ScheduleId,'2',$from,$to); ?></td>
        <td width="20%" align="left"><?php  check($sched->ScheduleId,'1',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'8',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'7',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'6',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'5',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'12',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'11',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'10',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'9',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'16',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'15',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'14',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'13',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'20',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'19',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'18',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'17',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'24',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'23',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'22',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'21',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'28',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'27',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'26',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'25',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'32',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'31',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'30',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'29',$from,$to); ?></td>
      </tr>
      
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'36',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'35',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'34',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'33',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'40',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'39',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'38',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'37',$from,$to); ?></td>
      </tr>
      
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'44',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'43',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'42',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'41',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'49',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'48',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'47',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'46',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'45',$from,$to); ?></td>
      </tr>
    </table>
  </form>
  <?php } elseif($bustype=="4"){ ?>
 
  <form id="mini">
 <table width="800" height="800" class="bustable chart" border="1" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td colspan="2" align="left"><h2>&nbsp;</h2></td>
        <td rowspan="3">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center"><h2 class="th">VIP </h2></td>
        <td colspan="2" align="center"><h2 class="th">DRIVER</h2></td>
      </tr>
      
      <tr>
        <td colspan="2" align="left"><h2>
          <?php  check($sched->ScheduleId,'1',$from,$to); ?>
        </h2></td>
        <td colspan="2" align="center"><h2>VIP</h2></td>
      </tr>
      <tr>
        <td colspan="2" align="left"><?php  check($sched->ScheduleId,'2',$from,$to); ?></td>
        <td>&nbsp;</td>
        <td align="left"><?php  check($sched->ScheduleId,'3',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'4',$from,$to); ?></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><h2 class="th">DOOR</h2></td>
        <td>&nbsp;</td>
        <td align="left"><?php  check($sched->ScheduleId,'5',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'6',$from,$to); ?></td>
      </tr>
      <tr>
        <td colspan="5" align="center"><h2 class="th">BUSINESS CLASS</h2></td>
      </tr>
      <tr>
        <td align="left"><h2>&nbsp;</h2></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left"><?php  check($sched->ScheduleId,'7',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'8',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'9',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'10',$from,$to); ?></td>
        <td width="20%" rowspan="5" align="left">&nbsp;</td>
        <td align="left"><?php  check($sched->ScheduleId,'11',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'12',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'13',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'14',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'15',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'16',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'17',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'18',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'19',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'20',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'21',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'22',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'23',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'24',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'25',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'26',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'27',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'28',$from,$to); ?></td>
      </tr>
      <tr>
        <td align="left"><?php  check($sched->ScheduleId,'29',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'30',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'31',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'32',$from,$to); ?></td>
        <td align="left"><?php  check($sched->ScheduleId,'33',$from,$to); ?></td>
      </tr>
    </table>
  </form>
  <?php }?>
</div>


</body></html>