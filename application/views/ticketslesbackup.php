 
                
                //branch
                if ($townf==0):
                    $data['branch'] = 'All';
                else:
                    $br = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
                    $data['branch'] = $br->TownName;
                endif;
                
                
                
                //clerk
                if ($clerk==0):
                    $data['clerk'] = 'All';
                else:
                    $this->user=$this->flexi_auth->get_user_by_id($clerk)->result();
                    $data['clerk']= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
                endif;
                
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;

                
                //date title
                if ($datef==$datet):
                    if ($datef==date('Y-m-d')):
                        $title = "Today";
                    else:
                        $title = date('d/m/Y',strtotime($datef));
                    endif;
                    elseif($datef<$datet):
                        $title = date('d/m/Y',strtotime($datef))." to ".date('d/m/Y',strtotime($datet));
                    else:
                        $title = "";
                endif;
                $data['datettitle'] = $title;
  
  $this->load->view('manage_ticket_sales',$data);
  }else {
                 redirect('auth','refresh');
            }
 } 
        
  function crud_ticket_sales_branch()
 { 
        if ($this->flexi_auth->is_logged_in()){
                //$townf = 0;
                $terminus = 0;
                $clerk = 0;
                $datef = date('Y-m-d');
                $datet = date('Y-m-d');
                $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$townf= $this->user[0]->t_station;
                
                $data['termini'] = $this->bus_model->getresultfromonetbl('bus_termini', 'TownLkp', $townf);
                //branch
                $br = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townf);
                    $data['branch'] = $br->TownName;
                    $data['townid'] = $townf;
                //clerk
                $data['clerk'] = 'All';
                //total ticket sales
                $tts = $this->bus_model->get_total_ticket_sales($townf,$terminus,$clerk,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                $data['tts'] = $ttshtml;
                //petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($townf,$terminus,$clerk,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                 
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
                $data['pc'] = $pchtml;
                //final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                $data['fa'] = $fa;
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($townf, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                $data['ba'] = $ba;