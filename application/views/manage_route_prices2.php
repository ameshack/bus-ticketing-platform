<?php
$data = array(
'title'=>'Manage Route Prices',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage route prices',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
     <script type="text/javascript">  
$(function() {
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdate2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });    
	 $('#insdateedit2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Reports</p>

<div id="rightcontentwide">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Filter Prices</a></li>
      <li><a href="#create" name="#tab2">View and Edit Prices</a></li>
      <li><a href="#edit" name="#tab3">Season</a></li>
      <li><a href="#delete" name="#tab4">Edit Seasons</a></li>
      
       
    </ul>  
  <div id="content">
   
    <div id="tab1">
	<?php echo form_open('admins/crud_prices3/#tab2');?>
    	 <h2>Use fields below to generate desired report.</h2>
         <table width="100%" cellpadding="0" cellspacing="0" class="table1">
                <tr>
          <td width="228" class="labels">Bus Type:</td>
          <td width="902"><span class="red"><?php echo form_error('bustype'); ?></span>
            <select name="bustype" id="bustype">
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
        </tr>
        
                <tr>
          <td width="228" class="labels">Class:</td>
          <td width="902"><span class="red"><?php echo form_error('seat'); ?></span>
            <select name="seat" id="seat">
              <?php foreach($seattypes as $seattype){?>
              <option value="<?php echo $seattype->SeatTypeId;?>"><?php echo $seattype->SeatType;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="View Route Prices" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>

          <?php echo form_close();?>
    </div>
   
    <div id="tab2">
    <? if ($rep==NULL){
		echo '<p class="red">Please filter in the \'Filter Prices\' tab!</p>';
	}
	else {
		echo form_open('admins/create_route_price2/#tab2');
		?>

    <table width="100%" cellpadding="0" cellspacing="0" class="table1">
    <thead id="pageFooter">
      <tr><th colspan="50" style="text-align:center;"><strong><?php echo $reporttitle;?></strong>
      <input name="bustypeid" type="hidden" value="<?php echo $bustypeid;?>" />
      <input name="seattypeid" type="hidden" value="<?php echo $seattypeid;?>" />
      </th></tr>
    </thead>
    <?php
  if ($towns==NULL)
  {
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No towns added yet!</td></tr>";
  }
  else {
	  
		  
  ?>
  <tr>
  <td style="text-align:center; vertical-align:middle;">
  <strong>Towns From<br />v<br />v<br /></strong><strong>Towns To >></strong></td>
  <?php
  foreach ($towns as $t)
	  {
		  ?>
    <td><strong><?php echo $t->TownName; ?></strong></td>
    <? } ?>
  </tr>
 <?php
 foreach ($towns as $t)
	  {
		  ?>
          <tr>
          <td><strong><?php echo $t->TownName; ?></strong></td>
          <?php
          foreach ($towns as $r)
	  {
		  
		  $prices = $this->bus_model->getspecRoutePrices($bustypeid,$seattypeid,$t->TownId,$r->TownId);
		  if ($prices==NULL)
		  {
			  $prices2 = $this->bus_model->getspecRoutePrices($bustypeid,$seattypeid,$r->TownId,$t->TownId);
			  if ($prices2==NULL)
			  {
				  $kes = 0;
				  $ugx = 0;
				  $tzs = 0;
			  }
			  else {
				  $kes = $prices2->KeAmount;
				  $ugx = $prices2->UgAmount;
				  $tzs = $prices2->TzAmount;
			  }
			  
		  }
		  else {
			  $kes = $prices->KeAmount;
			  $ugx = $prices->UgAmount;
			  $tzs = $prices->TzAmount;
		  }
		  ?>
          <td>
          
          Ksh<span class="red"><?php echo form_error('keamount'); ?></span>
          <input name="keamount<?=$t->TownId."_".$r->TownId?>" id="keamount<?=$t->TownId."_".$r->TownId?>" type="text" value="<?php echo $kes;?>" size="2" onchange="copyprices(this.name);"><br />
          Ugx<span class="red"><?php echo form_error('ugamount'); ?></span>
            <input type="text" name="ugamount<?=$t->TownId."_".$r->TownId?>" value="<?php echo $ugx;?>" size="2" onchange="copyprices(this.name);"><br />
          Tzs<span class="red"><?php echo form_error('tzamount'); ?></span>
            <input type="text" name="tzamount<?=$t->TownId."_".$r->TownId?>" value="<?php echo $tzs;?>" size="2" onchange="copyprices(this.name);">
          </td>
          <? }?>
          </tr>
          
          <? } }?><tr>
            <td colspan="100" style="text-align:center;"><input  type="submit" name="submit" class="confirm" value="Edit Route Prices" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" title="Edit Prices"  ></td>
          </tr>
</table>
<?php echo form_close();?>
<? } ?>
    </div>
    
<div id="tab3">
<?php echo form_open('admins/create_season');?>
<table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
                  <td width="228" class="labels">Season Name:</td>
                  <td width="902"><span class="red"><?php echo form_error('seasonname'); ?></span>
                  <input type="text" name="seasonname" id="long_input" value="<?php echo $this->input->post('seasonname');?>"></td>
        </tr>
                <tr>
                  <td class="labels">Description:</td>
                  <td><span class="red"><?php echo form_error('sdescription'); ?></span>
                  <input type="text" name="sdescription"  id="sdescription" class="long_input" value="<?php echo $this->input->post('sdescription');?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price From Date:</td>
                  <td><span class="red"><?php echo form_error('pricefrom'); ?></span>
                  <input type="text" name="pricefrom"  id="insdate" class="long_input" readonly="readonly" value="<?php echo $this->input->post('pricefrom');?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price To Date:</td>
                  <td><span class="red"><?php echo form_error('priceto'); ?></span>
                  <input type="text" name="priceto"  id="insdate2" class="long_input" readonly="readonly" value="<?php echo $this->input->post('priceto');?>"></td>
                </tr>
                <tr>
                  <td class="labels">KE Amount</td>
                  <td><span class="red"><?php echo form_error('pamountke'); ?></span>
                  <input type="text" name="pamountke"  id="pamountke" class="long_input" value="<?php echo $this->input->post('pamountke');?>"></td>
                </tr>
                <tr>
                  <td class="labels">UG Amount</td>
                  <td><span class="red"><?php echo form_error('pamountug'); ?></span>
                  <input type="text" name="pamountug"  id="pamountug" class="long_input" value="<?php echo $this->input->post('pamountug');?>"></td>
                </tr>
                <tr>
                  <td class="labels">TZ Amount</td>
                  <td><span class="red"><?php echo form_error('pamounttz'); ?></span>
                  <input type="text" name="pamounttz"  id="pamounttz" class="long_input" value="<?php echo $this->input->post('pamounttz');?>"></td>
                </tr>
                <tr>
                  <td class="labels">More or Less?</td>
                  <td><span class="red"><?php echo form_error('moreorless'); ?></span>
                  <select name="moreorless">
                  <option value="+">+</option>
                  <option value="-">-</option>
                  </select>
                  </td>
                </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Season" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
	<?php if(!empty($seasons)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="120">Season Name</th>
			  <th width="201">Description</th>
			  <th width="115">Date From</th>
			  <th width="126">Date To</th>
			  <th width="147">Amount</th>
			  <th width="151">Status</th>
	          <th width="110">Options</th>
		  </tr>
		</thead>
        <?php foreach ($seasons as $s): ?>
			<tr>
				<td align="left"><?php echo anchor('admins/edit_season/'.$s->SeasonId.'/#tab4',$s->SeasonName,array('class' => 'edit','title'=>$s->SeasonName));?></td>
			  <td align="left"><?php echo $s->Description; ?></td>
			  <td align="left"><?php echo $s->PriceFrom; ?></td>
			  <td align="left"><?php echo $s->PriceTo; ?></td>
			  <td align="left">
			 <?php echo "Ksh ".$s->PAmountke."<br />";
			 echo "Ush ".$s->PAmountug."<br />";
			 echo "Tsh ".$s->PAmounttz."<br />"; ?></td>
              <td align="left"><?php if($s->IsActive==1): echo "Active | ";
			  echo anchor('admins/deactivate_season/'.$s->SeasonId.'/#tab3','Deactivate',array('class' => 'confirm','title'=>'Deactivate'));
			    else:
			  echo anchor('admins/activate_season/'.$s->SeasonId.'/#tab3','Activate',array('class' => 'confirm','title'=>'Activate')); endif;
			   ?></td>
			  <td align="left">
			  
			  <?php echo anchor('admins/delete_season/'.$s->SeasonId.'/#tab3','X',array('class' => 'delete','title'=>$s->SeasonName));?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no route prices currently!</p>';}; ?>
</div>
<div id="tab4">
<?php if(!empty($seasondetails)){?>
<?php echo form_open('admins/update_season/#tab3');?>

<table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
                  <td width="228" class="labels">Season Name:</td>
                  <td width="902"><span class="red"><?php echo form_error('seasonname'); ?></span>
                  <input type="text" name="seasonname" id="long_input" value="<?php echo $seasondetails->SeasonName;?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price From Date:</td>
                  <td><span class="red"><?php echo form_error('pricefrom'); ?></span>
                  <input type="text" name="pricefrom"  id="insdateedit" class="long_input" readonly="readonly" value="<?php echo $seasondetails->PriceFrom;?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price To Date:</td>
                  <td><span class="red"><?php echo form_error('priceto'); ?></span>
                  <input type="text" name="priceto"  id="insdateedit2" class="long_input" readonly="readonly" value="<?php echo $seasondetails->PriceTo;?>"></td>
                </tr>
                <tr>
                  <td class="labels">KE Amount</td>
                  <td><span class="red"><?php echo form_error('pamountke'); ?></span>
                  <input type="text" name="pamountke"  id="pamountke" class="long_input" value="<?php echo $seasondetails->PAmountke;?>"></td>
                </tr>
                <tr>
                  <td class="labels">UG Amount</td>
                  <td><span class="red"><?php echo form_error('pamountug'); ?></span>
                  <input type="text" name="pamountug"  id="pamountug" class="long_input" value="<?php echo $seasondetails->PAmountug;?>"></td>
                </tr>
                <tr>
                  <td class="labels">TZ Amount</td>
                  <td><span class="red"><?php echo form_error('pamounttz'); ?></span>
                  <input type="text" name="pamounttz"  id="pamounttz" class="long_input" value="<?php echo $seasondetails->PAmounttz;?>"></td>
                </tr>
                <tr>
                  <td class="labels">More or Less?</td>
                  <td><span class="red"><?php echo form_error('moreorless'); ?></span>
                  <select name="moreorless">
                  <option <?php if ($seasondetails->PAmountke>=0): echo 'selected'; endif;?> value="+">+</option>
                  <option <?php if ($seasondetails->PAmountke<0): echo 'selected'; endif;?> value="-">-</option>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td class="labels">Description:</td>
                  <td><span class="red"><?php echo form_error('sdescription'); ?></span>
                  <input type="text" name="sdescription"  id="sdescription" class="long_input" value="<?php echo $seasondetails->Description;?>"><input name="sid" type="hidden" value="<? echo $sid;?>" /></td>
                </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Season" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Select Season to edit From Season</p>';}; ?>
</div>
    </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
