
<ul class="paddingleftright">
<div style="top:10px; margin-top:40px;">
<?php
$user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$usergrp = $user[0]->uacc_group_fk;
?>
<?php
if ($usergrp==1 || $usergrp==2 || $usergrp==3):
?>
<li class="ticketing"><?php echo anchor('admins/ticket_station/','Ticketing');?></li>
<li class="payment"><?php echo anchor('admins/search_tickets_r/','Search Tickets');?></li>
<?php endif; ?>
<?php
if ($usergrp==1):
?>
<li class="payment"><?php echo anchor('admins/crud_ticket_sales_clerk/','My Sales');?></li>
<li class="payment"><?php echo anchor('admins/clerk_report/','Ticket Details');?></li>
<?php endif; ?>
<?php
if ($usergrp==2):
?>
<li class="payment"><?php echo anchor('admins/crud_ticket_sales_branch/','Ticket Sales');?></li>
<?php endif; ?>
<?php
if ($usergrp==3):
?>
<li class="payment"><?php echo anchor('admins/crud_ticket_sales/','Ticket Sales');?></li>
<?php endif; ?><?php
if ($usergrp==3):
?>
<?php endif; ?><?php

if ($usergrp==3):
?>
    <li class="payment"><?php echo anchor('admins/crud_reports/','Reports');?></li>
    <li class="payment"><?php echo anchor('admins/crud_report_center/','Reports Center');?></li>
    <li class="payment"><?php echo anchor('admins/crud_cash_recon/','Cash Reconciliation');?></li>
<?php
endif;
if ($usergrp==1):
?>
<li class="bus"><?php echo anchor('admins/crud_cash_recon_clerk/','Cash Reconciliation');?></li>
<?php
endif;
if ($usergrp==3):
?>
<li class="bus"><?php echo anchor('admins/crud_bus/','Bus');?></li>
<?php endif; ?>
<?php

if ($usergrp==3):
?>
<li class="maintenance"><?php echo anchor('admins/crud_maintenance/','Bus Maintenance');?></li>
<?php
endif;
if ($usergrp==3):
?>
<li class="routes"><?php echo anchor('admins/crud_routes/','Bus Routes');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="terminal"><?php echo anchor('admins/crud_termini/','Bus Terminal');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2):
?>
    <li class="schedule"><?php echo anchor('admins/crud_schedules/','Bus Schedule');?></li>
    <li class="payment"><?php echo anchor('admins/crud_arrivals/','Arrivals');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="prices"><?php echo anchor('admins/crud_prices2/','Route Prices');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2 || $usergrp==1):
?>
    <li class="loyalty"><?php echo anchor('admins/crud_loyalty/','Loyalty Points');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2):
?>
    <li class="banking"><?php echo anchor('admins/crud_banking/','Banking');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2):
?>
    <li class="petty_cash"><?php echo anchor('admins/crud_petty_cash/','Petty Cash');?></li>
<?php
endif;
if ($usergrp==1):
?>
    <li class="petty_cash"><?php echo anchor('admins/crud_petty_cash_clerk/','Petty Cash');?></li>
<?php
endif;
if ($usergrp==3):
?>
<li class="hr"><?php echo anchor('admins/crud_customers/','Corporate Customers');?></li>
<li class="payment"><?php echo anchor('admins/crud_corp_sales/','Corporate Reports');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="hr"><?php echo anchor('admins/crud_staff/','Bus Staff');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2):
?>
    <li class="bus_hire"><?php echo anchor('admins/crud_hiring/','Bus Hire');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="mobilepayment"><?php echo anchor('admins/crud_vouchers/','Vouchers');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="mobilepayment"><?php echo anchor('admins/crud_discounts/','Discounts');?></li>
<?php
endif;
if ($usergrp==3 || $usergrp==2 || $usergrp==1):
?>
    <!--<li class="mobilepayment" id="borderbottom">Notifications</li>-->
    <li class="messages"><?php echo anchor('admins/crud_messages/','Messages');?></li> 
<?php
endif;
if ($usergrp==3):
?>
    <li class="payment"><?php echo anchor('admins/crud_agents','Agents Manager');?></li>

<?php
endif;
if ($usergrp==3):
?>
    <li class="maintenance"><?php echo anchor('admins/crud_settings/','Settings');?></li>
<?php
endif;
if ($usergrp==3):
?>
    <li class="payment"><?php echo anchor('admins/crud_graphical_reports/','Graphical Reports');?></li>
<?php
endif;
?>
<?php
if ($usergrp==3 || $usergrp==2):
?>
    <li><?php echo anchor('admins/crud_cash_recon_special/',nbs(50));?></li>
<?php
endif;
?>

<?php
if ($usergrp==5):
?>
<li class="ticketing"><?php echo anchor('admins/ticket_station_corp/','Ticketing');?></li>
<li class="payment"><?php echo anchor('admins/crud_acc_statements/','Account Statements');?></li>
<?php endif; ?>
    </div>
  </ul>