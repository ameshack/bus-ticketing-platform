<?php
$data = array(
'title'=>'Manage Banking',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage banking',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#bankingdate').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });
	    $('#datebankededit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Banking</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Banking</a></li>
      <li><a href="#create" name="#tab2">Add Banking</a></li>
      <li><a href="#edit" name="#tab3">Edit Banking</a></li>
      <li><a href="#delete" name="#tab4">Delete Banking</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($banking)){ ?>
         <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="304">Terminus</th>
			  <th width="153"> Amount</th>
			  <th width="160">Date</th>
			  <th width="167">Reference #</th>
			  <th width="215">Options</th>
	  </tr>
		</thead>
        <?php foreach ($banking as $item):?>
			<tr>
				<td><?php echo $item->TerminusName; ?></td>
			  <td align="left"><?php 
			  if ($item->BCurrency==1)
			  {
				  $curr="Ksh ";
			  }
			  else if($item->BCurrency==2) {
				  $curr="Ush ";
			  }
			  else if($item->BCurrency==3) {
				  $curr="Tsh ";
			  }
			  else if($item->BCurrency==4) {
				  $curr="USD ";
			  }
			  else if($item->BCurrency==5) {
				  $curr="EURO ";
			  }
			  else {
				  $curr="";
			  }
			  echo $curr.$item->TotalAmount;?></td>
              <td align="left"><?php echo date('m/d/Y',strtotime($item->BankingDate)); ?></td>
              <td align="left"><?php echo $item->refno;?></td>
              <td align="left"><?php echo anchor('admins/edit_banking/'.$item->BankingId.'/#tab3','Edit');?> | <?php if($item->bankslip==NULL):
			  $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/upload_bank_slip/'.$item->BankingId, 'Upload Slip', $atts);
			else :
			echo anchor('admins/download_bank_slip/'.$item->BankingId, 'Download Slip');
			 endif;?></td>
			</tr><?php endforeach; ?>
	 </table>
      <?php // endif; ?>
      <?php }else{ echo '<p class="red">Sorry, no banking yet!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_banking/#tab2');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Terminus:</td>
          <td width="519"><span class="red"><?php echo form_error('terminusid'); ?></span>
            <select name="terminusid" id="terminusid">
              <option disabled="disabled" selected="selected">---select terminus--</option>
              <?php foreach($termini as $t){?>
              <option value="<?php echo $t->TerminusId;?>"><?php echo $t->TerminusName;?></option>
              <?php }?>
            </select></td>
        </tr>
        <tr>
                          <td class="labels">Currency:</td>
                          <td><span class="red"><?php echo form_error('currency'); ?></span>
              <select name="currency" id="currency">
              <option value="1" selected="selected">Kshs</option>
              <option value="2">Ushs</option>
              <option value="3">Tshs</option> 
              <option value="4">USD</option> 
              <option value="5">EUR</option>              
            </select></td>
                        </tr>
        <tr>
          <td height="24" class="labels">Total Amount:</td>
          <td><span class="red"><?php echo form_error('totalamount'); ?></span>
            <input type="text" name="totalamount" id="datebanked" class="long_input" value="<?php echo $this->input->post('totalamount');?>" ></td>
        </tr>
        
        <tr>
          <td class="labels">Date Banked:</td>
          <td><span class="red"><?php echo form_error('bankingdate'); ?></span>
            <input type="text" name="bankingdate"  id="bankingdate"  class="long_input" value="<?php echo $this->input->post('bankingdate');?>"/></td>
        </tr>
        <tr>
          <td class="labels">Reference Number</td>
          <td><span class="red"><?php echo form_error('refno'); ?></span>
            <input type="text" name="refno" id="long_input" value="<?php echo $this->input->post('refno');?>"></td>
        </tr>
        
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Banking" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($row)){?>
	<?php echo form_open('admins/update_banking');?>
    <?php // foreach($bankingdetails as $row): if($row->IsActive == 1): $set_checked = "CHECKED";else:$set_checked = ""; endif;?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Terminus:</td>
          <td width="511"><span class="red"><?php echo form_error('terminusid'); ?></span>
            <select name="terminusid" id="terminusid">
              <option disabled="disabled" selected="selected">---select terminus--</option>
              <?php foreach($termini as $t){?>
              <option value="<?php echo $t->TerminusId;?>" <?php if(!(strcmp($t->TerminusId, $row->TerminusId))) :echo "SELECTED";endif;?>><?php echo $t->TerminusName;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
                          <td class="labels">Currency:</td>
                          <td><span class="red"><?php echo form_error('currency'); ?></span>
              <select name="currency" id="currency">
              <option value="1" <?php if ($row->BCurrency==1){ echo "selected"; } ?>>Kshs</option>
              <option value="2" <?php if ($row->BCurrency==2){ echo "selected"; } ?>>Ushs</option>
              <option value="3" <?php if ($row->BCurrency==3){ echo "selected"; } ?>>Tshs</option> 
              <option value="4" <?php if ($row->BCurrency==4){ echo "selected"; } ?>>USD</option> 
              <option value="5" <?php if ($row->BCurrency==5){ echo "selected"; } ?>>EUR</option>              
            </select></td>
                        </tr>
        <tr>
          <td class="labels">Total Amount:</td>
          <td><span class="red"><?php echo form_error('totalamount'); ?></span>
            <input type="text" name="totalamount" id="long_input" value="<?php echo $row->TotalAmount;?>" ></td>
        </tr>
        
        <tr>
          <td class="labels">Date Banked:</td>
          <td><span class="red"><?php echo form_error('bankingdate'); ?></span>
           <input type="text" name="bankingdate" id="datebankededit"  class="long_input"  value="<?php echo $row->BankingDate;?>"/></td>
        </tr></td>
        </tr>
        <tr>
          <td class="labels">Reference Number</td>
          <td><span class="red"><?php echo form_error('refno'); ?></span>
            <input type="text" name="refno" id="long_input" value="<?php echo $row->refno;?>"></td>
        </tr>
        
        <tr>
          <td><input type="hidden" name="bankingid" value="<?php echo $row->BankingId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Banking" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php //endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select banking from \'View Banking\' tab!</p>';}; ?>
</div>
    <div id="tab4">    	 <?php if(!empty($banking)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="218">Terminus</th>
			  <th width="110"> Amount</th>
			  <th width="115">Date</th>
			  <th width="138">Reference Number</th>
			  <th width="95">Delete</th>
	  </tr>
		</thead>
        <?php foreach ($banking as $item):?>
			<tr>
				<td><?php echo $item->TerminusName; ?></td>
			  <td align="left"><?php echo $item->TotalAmount; ?></td>
              <td align="left"><?php echo date('m/d/Y',strtotime($item->BankingDate)); ?></td>
              <td align="left"><?php echo $item->refno;?></td>
              <td align="left"><?php echo anchor('admins/delete_banking/'.$item->BankingId.'','X',array('class' => 'delete','title'=>''.$item->refno.'')); ?></td>
			</tr><?php endforeach; ?>
	 </table>	
    <?php // endif; ?>
    <?php }else{ echo '<p class="red">Sorry, no banking yet!</p>';}; ?></div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
