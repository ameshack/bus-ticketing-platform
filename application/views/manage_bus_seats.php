<?php
$data = array(
'title'=>'Manage Bus Seats',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus seats',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript" language="javascript1.5">
var gblvar = 1;
//This function will add extra row to provide another file
function AddRow() {
	var prevrow = document.getElementById("uplimg");
	var newrowiddd = parseInt(prevrow.rows.length) + 2;
	var tmpvar = gblvar;
	var newrow = prevrow.insertRow(prevrow.rows.length);
	newrow.id = newrow.uniqueID; // give row its own ID
    var newcell; // an inserted row has no cells, so insert the cells
	newcell = newrow.insertCell(0);
	newcell.id = newcell.uniqueID;
	newcell.innerHTML = "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr height='25'><td  align='center' width='6%' >&nbsp;"+ newrowiddd +"</td><td  align='center' width='24%'><input name='particulars[]' type='text' size='25' /></td><td  align='center' width='20%'><input name='percentage_ratio[]' type='text' size='15' /></td><td  align='center' width='20%'><input name='percentage_term_one[]' type='text' size='12' /></td><td  align='center' width='20%'><input name='percentage_term_two[]' type='text' size='12' /></td><td  align='center' width='20%'><input name='percentage_term_three[]' type='text' size='12' /></td><td  align='center' width='15%'>&nbsp; <a href='javascript:AddRow()' title='Add'><img src='images/add_16.png' border='0'  /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Delete' href='javascript:DelRow()'><img src='images/b_drop.png' border='0' /></a></td></tr></table>";
  
  gblvar = tmpvar + 1;
  }

function DelRow() //This function will delete the last row
{
	if(gblvar == 1)
		return;
	var prevrow = document.getElementById("uplimg");
	prevrow.deleteRow(prevrow.rows.length-1);
	gblvar = gblvar - 1;
}

</script>

<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Seats</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Buses</a></li>
      <li><a href="#create" name="#tab2">Add Bus</a></li>
      <li><a href="#edit" name="#tab3">Edit Bus</a></li>
      <li><a href="#delete" name="#tab4">Delete Bus</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($buses)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="153">Registration Number</th>
			  <th width="93">Bus Type</th>
			  <th width="107">Make</th>
			  <th width="118">Seats</th>
	          <th width="105">Status</th>
	          <th width="96">Options</th>
		  </tr>
		</thead>
        <?php foreach ($buses as $item): if($item->IsActive == 1): $status = "Available";else:$status = "Not Available"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->MakeName; ?></td>
              <td align="left"><?php echo $item->TotalSeats; ?></td>
			  <td align="left"><?php echo $status; ?></td>
			  <td align="left"><?php echo anchor('admins/edit_bus/'.$item->BusId.'/#tab3','Edit |');?> <?php echo anchor('admins/add_seats/'.$item->BusId.'/#tab3','Add Seats');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no buses currently!</p>';}; ?>


    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus Type:</td>
          <td width="519"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="bustypelkp" id="bustypelkp">
              <option disabled="disabled" selected="selected">---select bus type---</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td height="24" class="labels">Registration Number:</td>
          <td><span class="red"><?php echo form_error('regno'); ?></span>
            <input type="text" name="regno" id="long_input" value="<?php echo $this->input->post('regno');?>" ></td>
        </tr>
        <tr>
          <td class="labels">Make:</td>
          <td><span class="red"><?php echo form_error('makelkp'); ?></span>
            <select name="makelkp" id="makelkp">
              <option disabled="disabled" selected="selected">---select make type---</option>
              <?php foreach($makes as $make){?>
              <option value="<?php echo $make->MakeId;?>"><?php echo $make->MakeName;?></option>
              <?php }?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Date Bought:</td>
          <td><span class="red"><?php echo form_error('datebought'); ?></span>
            <input type="text" name="datebought"  id="datebought"  class="long_input" value="<?php echo $this->input->post('datebought');?>"/></td>
        </tr>
        <tr>
          <td class="labels">Bus Name</td>
          <td><span class="red"><?php echo form_error('busname'); ?></span>
            <input type="text" name="busname" id="long_input" value="<?php echo $this->input->post('busname');?>"></td>
        </tr>
        <tr>
          <td class="labels">Bus Number:</td>
          <td><span class="red"><?php echo form_error('busnumber'); ?></span>
            <input type="text" name="busnumber" id="long_input" value="<?php echo $this->input->post('busnumber');?>"></td>
        </tr>
        <tr>
          <td class="labels">Is Active</td>
          <td><input name="isactive" type="checkbox" id="isactive" value="1" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Bus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($busdetails)){?>
	<?php echo form_open('admins/update_bus');?>
    <?php foreach($busdetails as $row): if($row->IsActive == 1): $set_checked = "CHECKED";else:$set_checked = ""; endif;?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Bus Type:</td>
          <td width="511"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="bustypelkp" id="bustypelkp">
              <option disabled="disabled">---select bus type---</option>
              <?php foreach($bustypes as $bustype):?>
<option value="<?php echo $bustype->BusTypeId;?>" <?php if(!(strcmp($bustype->BusTypeId, $row->BusTypeLkp))) :echo "SELECTED";endif;?>><?php echo $bustype->BusType;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Registration Number:</td>
          <td><span class="red"><?php echo form_error('regno'); ?></span>
            <input type="text" name="regno" id="long_input" value="<?php echo $row->RegistrationNumber;?>" ></td>
        </tr>
        <tr>
          <td class="labels">Make:</td>
          <td><span class="red"><?php echo form_error('makelkp'); ?></span>
            <select name="makelkp" id="makelkp">
              <option disabled="disabled">---select make type---</option>
              <?php foreach($makes as $make):?>
 <option value="<?php echo $make->MakeId;?>" <?php if(!(strcmp($make->MakeId, $row->MakeLkp))) :echo "SELECTED";endif;?>><?php echo $make->MakeName;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
        <tr>
          <td class="labels">Date Bought:</td>
          <td><span class="red"><?php echo form_error('datebought'); ?></span>
           <input type="text" name="datebought" id="dateboughtedit"  class="long_input"  value="<?php echo $row->DateBought;?>"/></td>
        </tr></td>
        </tr>
        <tr>
          <td class="labels">Bus Name</td>
          <td><span class="red"><?php echo form_error('busname'); ?></span>
            <input type="text" name="busname" id="long_input" value="<?php echo $row->BusName;?>"></td>
        </tr>
        <tr>
          <td class="labels">Bus Number:</td>
          <td><span class="red"><?php echo form_error('busnumber'); ?></span>
            <input type="text" name="busnumber" id="long_input" value="<?php echo $row->BusNumber;?>"></td>
        </tr>
        <tr>
          <td class="labels">Is Active</td>
          <td><input name="isactive" type="checkbox" id="isactive" value="1" <?php echo $set_checked;?> /></td>
        </tr>
        <tr>
          <td><input type="hidden" name="busid" value="<?php echo $row->BusId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Bus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select bus from \'View Buses\' tab!</p>';}; ?>
</div>
    <div id="tab4">    	 <?php if(!empty($buses)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="218">Registration Number</th>
			  <th width="110">Bus Type</th>
			  <th width="115">Make</th>
			  <th width="138">Status</th>
			  <th width="95">Delete</th>
	  </tr>
		</thead>
        <?php foreach ($buses as $item):if($item->IsActive == 1): $status = "Available";else:$status = "Not Available"; endif; ?>
			<tr>
				<td><?php echo $item->RegistrationNumber; ?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->MakeName; ?></td>
              <td align="left"><?php echo $status; ?></td>
              <td align="left"><?php echo anchor('admins/delete_bus/'.$item->BusId.'','X',array('class' => 'delete','title'=>''.$item->RegistrationNumber.'')); ?></td>
			</tr><?php endforeach; ?>
	 </table>	
    <?php // endif; ?>
    <?php }else{ echo '<p class="red">Sorry, no buses currently!</p>';}; ?></div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
