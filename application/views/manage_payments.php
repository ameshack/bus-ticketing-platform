<?php
$data = array(
'title'=>'Manage Payments',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage payments',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#collecteddate').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#datebankededit').datetimepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Loyalty</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Payment Reports</a></li>
    </ul>  
  <div id="content">
    <div id="tab1">
<?php echo form_open('admins/filter_payments_by_bus');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus:</td>
          <td width="519"><span class="red"><?php echo form_error('busid'); ?></span><br />

            <select name="busid" id="busid">
              <option disabled="disabled" selected="selected">---select Bus--</option>
              <?php foreach($buses as $t){?>
              <option value="<?php echo $t->RegistrationNumber;?>"><?php echo $t->RegistrationNumber;?></option>
              <?php }?>
            </select>
            Date:
            <input name="collecteddate" id="collecteddate" type="text" value="<?php echo set_value('collecteddate');?>"  readonly="readonly" size="25"/>
            </td>
        </tr>
        

        <tr>
          <td><?php echo $bus;?></td>
          <td><input  type="submit" name="submit" value="Submit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <p>&nbsp;</p>
          	 <?php if(!empty($payments)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="269">Customer</th>
			  <th width="157">Bus</th>
			  <th width="114">Seat</th>
			  <th width="218">Seat Type</th>
			  <th width="226">Travel Date</th>
	          <th width="148">Amount</th>
          </tr>
		</thead>
        <?php foreach ($payments as $item):?>
			<tr>
				<td align="left"><?php echo $item->clientname; ?></td>
			  <td align="left"><?php echo $item->RegistrationNumber; ?></td>
              <td align="left"><?php echo $item->seatno; ?></td>
              <td align="left"><?php echo $item->SeatType;?></td>
              <td align="left"><?php echo date('m/d/Y',strtotime($item->DepartureDateTime)); ?></td>
			  <td align="left"><?php echo number_format($item->amount,2);?></td>
		    </tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no payments yet!</p>';}; ?>

    </div>
    </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
