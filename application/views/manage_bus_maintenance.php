<?php
$data = array(
'title'=>'Manage Bus Maintenance',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus maintenance',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#admittedadd').datetimepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#admitted').datetimepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	     $('#released').datetimepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
});  
</script> 

<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Maintenance</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Maintenance History</a></li>
      <li><a href="#create" name="#tab2">Add Maintenance</a></li>
      <li><a href="#edit" name="#tab3">Edit Maintenance</a></li>
      <li><a href="#delete" name="#tab4">Delete Maintenance</a></li>
    </ul>
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($maintenance)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Bus</th>
			  <th width="169">Maintenance Type</th>
			  <th width="125">Date Admitted</th>
			  <th width="147">Date Released</th>
	          <th width="107">Status</th>
	          <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($maintenance as $item):  if($item->IsSorted == 1): $status = "Available";else:$status = "Not Available"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->MaintenanceType; ?></td>
              <td align="left"><?php echo $item->DateAdmitted; ?></td>
              <td align="left"><?php echo $item->DateReleased; ?></td>
			  <td align="left"><?php echo $status; ?></td>
			  <td align="left"><?php echo anchor('admins/edit_bus_maintenance/'.$item->MaintenanceId.'/#tab3','Edit');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus maintenance history currently!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus_maintenance');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus:</td>
          <td width="519"><span class="red"><?php echo form_error('buslkp'); ?></span>
            <select name="buslkp" id="buslkp">
              <option disabled="disabled" selected="selected">---select bus---</option>
              <?php foreach($buses as $bus){?>
              <option value="<?php echo $bus->BusId;?>"><?php echo $bus->RegistrationNumber;?></option>
              <?php }?>
          </select></td>
        </tr>
        
        <tr>
          <td class="labels">Date Admitted:</td>
          <td><span class="red"><?php echo form_error('admitted'); ?></span>
            <input type="text" name="admitted"  id="admittedadd" class="long_input" value="<?php echo $this->input->post('admitted');?>"/></td>
        </tr>
                
                <tr>
          <td width="169" class="labels">Maintenance Type:</td>
          <td width="519"><span class="red"><?php echo form_error('maintenancelkp'); ?></span>
            <select name="maintenancelkp" id="maintenancelkp">
              <option disabled="disabled" selected="selected">---select maintenance type---</option>
              <?php foreach($maintenancetypes as $type){?>
              <option value="<?php echo $type->TypeId;?>"><?php echo $type->MaintenanceType;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td class="labels">Notes:</td>
          <td><span class="red"><?php echo form_error('description'); ?></span>
          <textarea name="description" cols="40" rows="6" id="description"><?php echo $this->input->post('description');?></textarea></td>
        </tr>
        
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Maintenance" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($maintenancedetails)){?>
	<?php echo form_open('admins/update_bus_maintenance');?>
    <?php foreach($maintenancedetails as $row): if($row->IsSorted == 1): $set_checked = "CHECKED";else:$set_checked = ""; endif;?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Bus:</td>
          <td width="519"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="buslkp" id="buslkp">
              <option disabled="disabled">---select bus---</option>
              <?php foreach($buses as $bus):?>
<option value="<?php echo $bus->BusId;?>" <?php if(!(strcmp($bus->BusId, $row->BusLkp))) :echo "SELECTED";endif;?>><?php echo $bus->RegistrationNumber;?></option>
              <?php endforeach;?>
            </select></td>
          </tr>
          <tr>
          <td class="labels">Date Admitted:</td>
          <td><span class="red"><?php echo form_error('admitted'); ?></span>
            <input type="text" name="admitted"  id="admitted" class="long_input" value="<?php echo $row->DateAdmitted;?>"/></td>
          </tr>
          <tr>
          <td class="labels">Date Released:</td>
          <td><span class="red"><?php echo form_error('released'); ?></span>
            <input type="text" name="released"  id="released" class="long_input" value="<?php echo $row->DateReleased;?>"/></td>
        </tr> 
                <tr>
          <td width="177" class="labels">Maintenance Type:</td>
          <td width="519"><span class="red"><?php echo form_error('maintenancelkp'); ?></span>
            <select name="maintenancelkp" id="maintenancelkp">
              <option disabled="disabled">---select maintenance type---</option>
              <?php foreach($maintenancetypes as $type):?>
				<option value="<?php echo $type->TypeId;?>" <?php if(!(strcmp($type->TypeId, $row->MaintenanceLkp))) :echo "SELECTED";endif;?>><?php echo $type->MaintenanceType;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
                <tr>
          <td class="labels">Notes:</td>
          <td><span class="red"><?php echo form_error('description'); ?></span>
          <textarea name="description"cols="40" rows="6" id="description"><?php echo $row->Description;?></textarea></td>
        </tr>
   
        <tr>
          <td class="labels">Is Sorted</td>
          <td><input name="issorted" type="checkbox" id="issorted" value="1" <?php echo $set_checked;?> /></td>
        </tr>
        <tr>
          <td><input type="hidden" name="maintenanceid" value="<?php echo $row->MaintenanceId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Bus Maintenance" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Maintenance History\' tab!</p>';}; ?>
</div>
    <div id="tab4">
	<?php if(!empty($maintenance)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Bus</th>
			  <th width="169">Maintenance Type</th>
			  <th width="125">Date Admitted</th>
			  <th width="147">Date Released</th>
	          <th width="107">Status</th>
	          <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($maintenance as $item): if($item->IsSorted == 1): $status = "Sorted";else:$status = "Not Sorted"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->MaintenanceType; ?></td>
              <td align="left"><?php echo $item->DateAdmitted; ?></td>
              <td align="left"><?php echo $item->DateReleased; ?></td>
			  <td align="left"><?php echo $status; ?></td>
			  <td align="left"><?php echo anchor('admins/delete_bus_maintenance/'.$item->BusId.'','X',array('class' => 'delete','title'=>''.$item->MaintenanceId.'')); ?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus maintenance history currently!</p>';}; ?>
</div>
</div>
  <!-- end tabs -->
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
