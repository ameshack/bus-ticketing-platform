<?php
$data = array(
'title'=>'Manage Ticket Sales',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage ticket sales',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Ticket Sales</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Sales Summary</a></li>
      <li><a href="#create" name="#tab2">Petty Cash</a></li>
      <li><a href="#edit" name="#tab3">Banking</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/crud_ticket_sales_refine/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23" colspan="2">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
        <td width="50%" rowspan="2" valign="top">
        Branch <br /> <select name="town[]" size="5" onchange="displayterminiclerks(this.value)" multiple="multiple" style="height:100px;">
             
              <?php foreach($towns as $t){?>
              <option selected="selected"  value="<?php echo $t->TownId;?>"><?php echo $t->TownName;?></option>
              <?php }?>
          </select></td>
        <td width="50%" valign="top">Date From<br />
          <input name="datef" type="text" class="long_input" id="insdate" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  /></td>
        </tr>
        <tr>
          <td valign="top" style="vertical-align:top;">Date To<br />
          <input name="datet" type="text" class="long_input" id="insdateedit" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d');} else { echo $this->input->post('datet');} ?>" /></td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:center;"><input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        <div id="printarea">
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="6"><?php echo $datettitle;?></th>
</tr>
			<tr align="left">
			  <th height="23">Branch</th>
			  <th>Clerk</th>
			  <th>Total Ticket Sales</th>
	          <th>Petty Cash</th>
	          <th>Final Amount</th>
	          <th>Banked Amount</th>
          </tr>
		</thead>
        <?php
		if ($townz<>NULL)
		{
		foreach ($townz as $t)
		{
		?>
			<tr>
			  <td align="left" style="vertical-align:top;"><?php echo $t->TownName; ?></td>
			  <td align="left" style="vertical-align:top;"><?php 
			  $sql_select = array('uacc_id','upro_first_name','upro_last_name','t_station');
       
	    $sql_where = '(uacc_group_fk = 1 OR uacc_group_fk = 3) AND t_station = '.$t->TownId;
            //$sql_where = 'uacc_group_fk = "1"';
		
		$clerks = $this->flexi_auth->get_users($sql_select,$sql_where)->result();
		if ($clerks==NULL)
		{
			$totc = 1;
			$cnt = 1;
			echo "<td colspan=\"4\"></td>";
		}
		if ($clerks<>NULL)
		{
			$totc = count($clerks);
			$cnt = 1;
		foreach ($clerks as $c)
		{
			$terminus = 0;
			echo $c->upro_first_name." ".$c->upro_last_name;
			
			echo "</td><td align=\"left\" style=\"vertical-align:top;\">";
			 $tts = $this->bus_model->get_total_ticket_sales($t->TownId,$terminus,$c->uacc_id,$datef,$datet);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                    $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    $ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     $ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     $ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     $ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                echo $ttshtml."<br />";
				echo "</td><td align=\"left\" style=\"vertical-align:top;\">";
				//petty cash issued
                $pc = $this->bus_model->get_total_petty_cash($t->TownId,$terminus,$c->uacc_id,$datef,$datet);
                $pchtml = '';
                $fapcke = 0;
                 $fapcug = 0;
                 $fapctz = 0;
                 $fapcusd = 0;
                 $fapceuro = 0;
                if ($pc==NULL):
                    $pchtml = 0;
                else:
                    if ($pc->amountke > 0):
                        $pchtml.="Ksh ".round($pc->amountke,2)."<br />";
                    $fapcke = $pc->amountke;
                    endif;
                    if ($pc->amountug > 0):
                        $pchtml.="Ush ".round($pc->amountug,2)."<br />";
                    $fapcug = $pc->amountug;
                    endif;
                    if ($pc->amounttz > 0):
                        $pchtml.="Tsh ".round($pc->amounttz,2)."<br />";
                    $fapctz = $pc->amounttz;
                    endif;
                    if ($pc->amountusd > 0):
                        $pchtml.="USD ".round($pc->amountusd,2)."<br />";
                    $fapcusd = $pc->amountusd;
                    endif;
                    if ($pc->amounteuro > 0):
                        $pchtml.="EUR ".round($pc->amounteuro,2)."<br />";
                    $fapceuro = $pc->amounteuro;
                    endif;
                    
                endif;
             echo $pchtml."<br />";
				echo "</td><td align=\"left\" style=\"vertical-align:top;\">";
				//final amount
                $fa = '';
                 if($ttspcke <>0 and $fapcke<>0):
                     if ($fapcke==0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                     else:
                         $fa .= "Ksh ".round($ttspcke - $fapcke)."<br />";
                     endif;
                     else:
                         if ($ttspcke<>0):
                         $fa .= "Ksh ".round($ttspcke)."<br />";
                         endif;
                 endif;
                 if($ttspcug <>0 and $fapcug<>0):
                     if ($fapcug==0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                     else:
                         $fa .= "Ush ".round($ttspcug - $fapcug)."<br />";
                     endif;
                     else:
                         if ($ttspcug<>0):
                         $fa .= "Ush ".round($ttspcug)."<br />";
                         endif;
                 endif;
                 if($ttspctz <>0 and $fapctz<>0):
                     if ($fapctz==0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                     else:
                         $fa .= "Tsh ".round($ttspctz - $fapctz)."<br />";
                     endif;
                     else:
                         if ($ttspctz<>0):
                         $fa .= "Tsh ".round($ttspctz)."<br />";
                         endif;
                 endif;
                 if($ttspcusd <>0 and $fapcusd<>0):
                     if ($fapcusd==0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                     else:
                         $fa .= "USD ".round($ttspcusd - $fapcusd)."<br />";
                     endif;
                     else:
                          if ($ttspcusd<>0):
                         $fa .= "USD ".round($ttspcusd)."<br />";
                          endif;
                 endif;
                 if($ttspceuro <>0 and $fapceuro<>0):
                     if ($fapceuro==0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                     else:
                         $fa .= "EUR ".round($ttspceuro - $fapceuro)."<br />";
                     endif;
                     else:
                         if ($ttspceuro<>0):
                         $fa .= "EUR ".round($ttspceuro)."<br />";
                         endif;
                 endif;
                echo $fa."<br />";
				echo "</td>";
				if ($totc==$cnt)
				{
				echo "<td align=\"left\" style=\"vertical-align:top;\">";
				
                //banked amount
                $ba = '';
                $baamntske = $this->bus_model->get_total_banked_amount($t->TownId, $terminus, $datef, $datet,1);
                if ($baamntske<>NULL):
                    if ($baamntske->amountcur<>"" or $baamntske->amountcur<>NULL):
                    $ba .="Ksh ".$baamntske->amountcur."<br />";
                    endif;
                endif;
                $baamntsug = $this->bus_model->get_total_banked_amount($t->TownId, $terminus, $datef, $datet,2);
                if ($baamntsug<>NULL):
                    if ($baamntsug->amountcur<>"" or $baamntsug->amountcur<>NULL):
                    $ba .="Ush ".$baamntsug->amountcur."<br />";
                    endif;
                endif;
                $baamntstz = $this->bus_model->get_total_banked_amount($t->TownId, $terminus, $datef, $datet,3);
                if ($baamntstz<>NULL):
                    if ($baamntstz->amountcur<>"" or $baamntstz->amountcur<>NULL):
                    $ba .="Tsh ".$baamntstz->amountcur."<br />";
                    endif;
                endif;
                $baamntsusd = $this->bus_model->get_total_banked_amount($t->TownId, $terminus, $datef, $datet,4);
                if ($baamntsusd<>NULL):
                    if ($baamntsusd->amountcur<>"" or $baamntsusd->amountcur<>NULL):
                    $ba .="USD ".$baamntsusd->amountcur."<br />";
                    endif;
                endif;
                $baamntseuro = $this->bus_model->get_total_banked_amount($t->TownId, $terminus, $datef, $datet,5);
                if ($baamntseuro<>NULL):
                    if ($baamntseuro->amountcur<>"" or $baamntseuro->amountcur<>NULL):
                    $ba .="EUR ".$baamntseuro->amountcur."<br />";
                    endif;
                endif;
                
                echo $ba;
				echo "</td></tr>";
				}
				else { 
				echo "<td align=\"left\" style=\"vertical-align:top; border-bottom:none;\">&nbsp;</td></tr><tr>
			  <td align=\"left\" style=\"vertical-align:top;\">"; echo "</td>
			  <td align=\"left\" style=\"vertical-align:top;\">";
				}
		$cnt++;		
		}
		}
			  ?></td>
        </tr>
		<? }} ?>
	 </table>
    </div>
    
<table width="596" border="0" align="center" cellspacing="0">
  <tr>
    <td align="center" valign="top">
    <form>
      <input type="button" value="Print" onClick="javascript:void(printSpecial())" />
    </form>    </td>
    </tr>
</table>

    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
