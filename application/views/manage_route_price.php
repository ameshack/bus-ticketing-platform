<?php
$data = array(
'title'=>'Manage Route Price',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage route price',
'img'=>'',
'load_side_menu'=>false

);

$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#PriceFrom').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });    
	    $('#PriceTo').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });     
	 $('#PriceFrom1').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });    
	    $('#PriceTo1').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
 
 
 
});  
</script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Route Price</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
 
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Route Prices</a></li>
      <li><a href="#create" name="#tab2">Add Route Price</a></li>
      <li><a href="#edit" name="#tab3">Edit Route Price</a></li>
      <li><a href="#delete" name="#tab4">Delete Price</a></li>
      <li><a href="#edit" name="#tab5">Season</a></li>
      <li><a href="#delete" name="#tab6">Edit Seasons</a></li>
    </ul> 
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($prices)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th width="42">No</th>
			  <th width="147">Bus Type</th>
			  <th width="158">Class</th>
			  <th width="140">From</th>
			  <th width="120">To</th>
			  <th width="114">Ke Price</th>
	          <th width="112">Ug Amount</th>
	          <th width="121">Tz Amount</th>
	          <th width="210">Options</th>
		  </tr>
		</thead>
        <?php $cnt = 1; foreach ($prices as $item): ?>
			<tr>
				<td align="left"><?php echo $cnt;?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->SeatType; ?></td>
              <td align="left"><?php echo $item->TownName; ?></td>
              <td align="left"><?php echo $item->TownTo; ?></td>
              <td align="left"><?php echo $item->KeAmount; ?></td>
			  <td align="left"><?php echo $item->UgAmount; ?></td>
			  <td align="left"><?php echo $item->TzAmount; ?></td>
			  <td align="left"><?php 
			  
				  echo anchor('admins/edit_route_price/'.$item->PriceId.'/#tab3','Edit');
			
			 if ($item->aps==0)
			 {
				  echo " | ".anchor('admins/approve_price/'.$item->PriceId,'Approve',array('title'=>'Approve','class'=>'confirm'));
			  }
			  ?></td>
			</tr>
		<?php $cnt++; endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no route prices currently!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_route_price');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
                <tr>
          <td width="228" class="labels">Bus Type:</td>
          <td width="902"><span class="red"><?php echo form_error('bustype'); ?></span>
            <select name="bustype" id="bustype">
              <option disabled="disabled" selected="selected">---select bus type--</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
        </tr>
                <tr>
          <td class="labels">From:</td>
          <td><span class="red"><?php echo form_error('from'); ?></span>
            <select name="from" id="from">
              <option disabled="disabled" selected="selected">---select town from---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select></td>
        </tr>
                <tr>
          <td class="labels">To:</td>
          <td><span class="red"><?php echo form_error('to'); ?></span>
            <select name="to" id="to">
              <option disabled="disabled" selected="selected">---select town to---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select></td>
        </tr>
        
                <tr>
          <td width="228" class="labels">Class:</td>
          <td width="902"><span class="red"><?php echo form_error('seat'); ?></span>
            <select name="seat" id="seat">
              <option disabled="disabled" selected="selected">---select class--</option>
              <?php foreach($seattypes as $seattype){?>
              <option value="<?php echo $seattype->SeatTypeId;?>"><?php echo $seattype->SeatType;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td class="labels">Ke Amount:</td>
          <td><span class="red"><?php echo form_error('keamount'); ?></span>
          <input type="text" name="keamount" id="long_input" value="<?php echo $this->input->post('keamount');?>" onkeypress='validate(event)'></td>
        </tr>
        <tr>
          <td height="25" class="labels">Ug Amount:</td>
          <td><span class="red"><?php echo form_error('ugamount'); ?></span>
            <input type="text" name="ugamount" id="long_input" value="<?php echo $this->input->post('ugamount');?>" onkeypress='validate(event)'></td>
        </tr>
        <tr>
          <td class="labels">Tz Amount:</td>
          <td><span class="red"><?php echo form_error('tzamount'); ?></span>
            <input type="text" name="tzamount" id="long_input" value="<?php echo $this->input->post('tzamount');?>" onkeypress='validate(event)'></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Route Price" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($pricedetails)){?>
	<?php echo form_open('admins/update_route_price');?>
    <?php foreach($pricedetails as $row):?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="231" class="labels">Bus Type:</td>
          <td width="899"><span class="red"><?php echo form_error('bustype'); ?></span>
            <select name="bustype" id="bustype">
              <option disabled="disabled">---select bus type---</option>
              <?php foreach($bustypes as $bustype):?>
<option value="<?php echo $bustype->BusTypeId;?>" <?php if(!(strcmp($bustype->BusTypeId, $row->BusTypeLkp))) :echo "SELECTED";endif;?>><?php echo $bustype->BusType;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
        <tr>
          <td width="231" class="labels">From:</td>
          <td width="899"><span class="red"><?php echo form_error('from'); ?></span>
            <select name="from" id="from">
              <option disabled="disabled">---select route from---</option>
              <?php foreach($towns as $town):?>
<option value="<?php echo $town->TownId;?>" <?php if(!(strcmp($town->TownId, $row->RouteFrom))) :echo "SELECTED";endif;?>><?php echo $town->TownName;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
                         <tr>
          <td width="231" class="labels">To:</td>
          <td width="899"><span class="red"><?php echo form_error('to'); ?></span>
            <select name="to" id="to">
              <option disabled="disabled">---select route to---</option>
              <?php foreach($towns as $town):?>
<option value="<?php echo $town->TownId;?>" <?php if(!(strcmp($town->TownId, $row->RouteTo))) :echo "SELECTED";endif;?>><?php echo $town->TownName;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>

                            <tr>
          <td width="231" class="labels">Class:</td>
          <td width="899"><span class="red"><?php echo form_error('seat'); ?></span>
            <select name="seat" id="seat">
              <option disabled="disabled">---select class---</option>
              <?php foreach($seattypes as $seattype):?>
<option value="<?php echo $seattype->SeatTypeId;?>" <?php if(!(strcmp($seattype->SeatTypeId, $row->SeatLkp))) :echo "SELECTED";endif;?>><?php echo $seattype->SeatType;?></option>
              <?php endforeach;?>
            </select></td>
        </tr>
          <tr>
          <td class="labels">Ke Amount:</td>
          <td><span class="red"><?php echo form_error('keamount'); ?></span>
          <input type="text" name="keamount" id="long_input" value="<?php echo $row->KeAmount;?>" onkeypress='validate(event)'></td>
        </tr>
          <tr>
          <td class="labels">Ug Amount:</td>
          <td><span class="red"><?php echo form_error('ugamount'); ?></span>
          <input type="text" name="ugamount" id="long_input" value="<?php echo $row->UgAmount;?>" onkeypress='validate(event)'></td>
        </tr>
          <tr>
          <td class="labels">Tz Amount:</td>
          <td><span class="red"><?php echo form_error('tzamount'); ?></span>
          <input type="text" name="tzamount" id="long_input" value="<?php echo $row->TzAmount;?>" onkeypress='validate(event)'></td>
        </tr>
                
        <tr>
          <td><input type="hidden" name="priceid" value="<?php echo $row->PriceId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Route Price" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Route Prices\' tab!</p>';}; ?>
</div>
    <div id="tab4">
	<?php if(!empty($prices)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="64">Code</th>
			  <th width="144">Bus Type</th>
			  <th width="94">Class</th>
			  <th width="82">From</th>
			  <th width="82">To</th>
			  <th width="83">Ke Price</th>
	          <th width="88">Ug Amount</th>
	          <th width="84">Tz Amount</th>
	          <th width="93">Options</th>
		  </tr>
		</thead>
        <?php foreach ($prices as $item): ?>
			<tr>
				<td align="left"><?php echo $item->FareCode;?></td>
			  <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->SeatType; ?></td>
              <td align="left"><?php echo $item->TownName; ?></td>
              <td align="left"><?php echo $item->TownTo; ?></td>
              <td align="left"><?php echo $item->KeAmount; ?></td>
			  <td align="left"><?php echo $item->UgAmount; ?></td>
			  <td align="left"><?php echo $item->TzAmount; ?></td>
			  <td align="left"><?php echo anchor('admins/delete_route_price/'.$item->PriceId.'/#delete','X',array('class' => 'delete','title'=>''));?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no route prices currently!</p>';}; ?>
</div>
<div id="tab5">
<?php echo form_open('admins/create_season');?>
<table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
                  <td width="228" class="labels">Season Name:</td>
                  <td width="902"><span class="red"><?php echo form_error('seasonname'); ?></span>
                  <input type="text" name="seasonname" id="long_input" value="<?php echo $this->input->post('seasonname');?>"></td>
        </tr>
                <tr>
                  <td class="labels">Description:</td>
                  <td><span class="red"><?php echo form_error('sdescription'); ?></span>
                  <input type="text" name="sdescription"  id="sdescription" class="long_input" value="<?php echo $this->input->post('sdescription');?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price From Date:</td>
                  <td><span class="red"><?php echo form_error('pricefrom'); ?></span>
                  <input type="text" name="pricefrom"  id="PriceFrom" class="long_input" readonly="readonly" value="<?php echo $this->input->post('pricefrom');?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price To Date:</td>
                  <td><span class="red"><?php echo form_error('priceto'); ?></span>
                  <input type="text" name="priceto"  id="PriceTo" class="long_input" readonly="readonly" value="<?php echo $this->input->post('priceto');?>"></td>
                </tr>
                <tr>
                  <td class="labels">KE Amount</td>
                  <td><span class="red"><?php echo form_error('pamountke'); ?></span>
                  <input type="text" name="pamountke"  id="pamountke" class="long_input" value="<?php echo $this->input->post('pamountke');?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">UG Amount</td>
                  <td><span class="red"><?php echo form_error('pamountug'); ?></span>
                  <input type="text" name="pamountug"  id="pamountug" class="long_input" value="<?php echo $this->input->post('pamountug');?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">TZ Amount</td>
                  <td><span class="red"><?php echo form_error('pamounttz'); ?></span>
                  <input type="text" name="pamounttz"  id="pamounttz" class="long_input" value="<?php echo $this->input->post('pamounttz');?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">More or Less?</td>
                  <td><span class="red"><?php echo form_error('moreorless'); ?></span>
                  <select name="moreorless">
                  <option value="+">+</option>
                  <option value="-">-</option>
                  </select>
                  </td>
                </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Season" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
	<?php if(!empty($seasons)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="120">Season Name</th>
			  <th width="201">Description</th>
			  <th width="115">Date From</th>
			  <th width="126">Date To</th>
			  <th width="147">Amount</th>
			  <th width="151">Status</th>
	          <th width="110">Options</th>
		  </tr>
		</thead>
        <?php foreach ($seasons as $s): ?>
			<tr>
				<td align="left"><?php echo anchor('admins/edit_season/'.$s->SeasonId.'/#tab6',$s->SeasonName,array('class' => 'edit','title'=>$s->SeasonName));?></td>
			  <td align="left"><?php echo $s->Description; ?></td>
			  <td align="left"><?php echo $s->PriceFrom; ?></td>
			  <td align="left"><?php echo $s->PriceTo; ?></td>
			  <td align="left">
			 <?php echo "Ksh ".$s->PAmountke."<br />";
			 echo "Ush ".$s->PAmountug."<br />";
			 echo "Tsh ".$s->PAmounttz."<br />"; ?></td>
              <td align="left"><?php if($s->IsActive==1): echo "Active | ";
			  echo anchor('admins/deactivate_season/'.$s->SeasonId.'/#tab5','Deactivate',array('class' => 'confirm','title'=>'Deactivate'));
			    else:
			  echo anchor('admins/activate_season/'.$s->SeasonId.'/#tab5','Activate',array('class' => 'confirm','title'=>'Activate')); endif;
			   ?></td>
			  <td align="left">
			  
			  <?php echo anchor('admins/delete_season/'.$s->SeasonId.'/#tab6','X',array('class' => 'delete','title'=>$s->SeasonName));?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no route prices currently!</p>';}; ?>
</div>
<div id="tab6">
<?php if(!empty($seasondetails)){?>
<?php echo form_open('admins/update_season/#tab5');?>

<table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
                  <td width="228" class="labels">Season Name:</td>
                  <td width="902"><span class="red"><?php echo form_error('seasonname'); ?></span>
                  <input type="text" name="seasonname" id="long_input" value="<?php echo $seasondetails->SeasonName;?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price From Date:</td>
                  <td><span class="red"><?php echo form_error('pricefrom'); ?></span>
                  <input type="text" name="pricefrom"  id="PriceFrom1" class="long_input" readonly="readonly" value="<?php echo $seasondetails->PriceFrom;?>"></td>
                </tr>
                <tr>
                  <td class="labels">Price To Date:</td>
                  <td><span class="red"><?php echo form_error('priceto'); ?></span>
                  <input type="text" name="priceto"  id="PriceTo1" class="long_input" readonly="readonly" value="<?php echo $seasondetails->PriceTo;?>"></td>
                </tr>
                <tr>
                  <td class="labels">KE Amount</td>
                  <td><span class="red"><?php echo form_error('pamountke'); ?></span>
                  <input type="text" name="pamountke"  id="pamountke" class="long_input" value="<?php echo $seasondetails->PAmountke;?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">UG Amount</td>
                  <td><span class="red"><?php echo form_error('pamountug'); ?></span>
                  <input type="text" name="pamountug"  id="pamountug" class="long_input" value="<?php echo $seasondetails->PAmountug;?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">TZ Amount</td>
                  <td><span class="red"><?php echo form_error('pamounttz'); ?></span>
                  <input type="text" name="pamounttz"  id="pamounttz" class="long_input" value="<?php echo $seasondetails->PAmounttz;?>" onkeypress='validate(event)'></td>
                </tr>
                <tr>
                  <td class="labels">More or Less?</td>
                  <td><span class="red"><?php echo form_error('moreorless'); ?></span>
                  <select name="moreorless">
                  <option <?php if ($seasondetails->PAmountke>=0): echo 'selected'; endif;?> value="+">+</option>
                  <option <?php if ($seasondetails->PAmountke<0): echo 'selected'; endif;?> value="-">-</option>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td class="labels">Description:</td>
                  <td><span class="red"><?php echo form_error('sdescription'); ?></span>
                  <input type="text" name="sdescription"  id="sdescription" class="long_input" value="<?php echo $seasondetails->Description;?>"><input name="sid" type="hidden" value="<? echo $sid;?>" /></td>
                </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Season" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Select Season to edit From Season</p>';}; ?>
</div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
