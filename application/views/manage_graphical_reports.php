<?php
$data = array(
'title'=>'Manage Graphical Reports',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage graphical reports',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Reports</p>

<div id="rightcontentwide">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Generate Report</a></li>
      <li><a href="#create" name="#tab2">Graphical Report</a></li>
      
       
    </ul>  
  <div id="content">
   
    <div id="tab1">
	<?php echo form_open('admins/displaygraphtypes/#tab2');?>
   	  <h2>Use fields below to generate desired report.</h2>
         <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td valign="top" style="vertical-align:top;">Graph Duration<br /> 
      <select name="typeid">
        <option value="1">Per Hour</option>
        <option value="2">Per Day</option>
        <option value="3">Per Week</option>
        <option value="4">Per Month</option>
      </select>
      
      </td>
    </tr>
  <tr>
    <td valign="top" style="vertical-align:top;"> <br /></td>
    </tr>
  <tr>
    <td valign="top" style="vertical-align:top;"><span style="text-align:center;">
      <input  type="submit" name="submit" value="Proceed Graph Report" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" />
    </span></td>
    </tr>
      </table>

          <?php echo form_close();?>
    </div>
   
    <div id="tab2">
    <? if ($rep==NULL){
		echo '<p class="red">Please generate a graph report in the \'Generate Report\' tab!</p>';
	}
	else {
		echo form_open('admins/gen_graphical_report/#tab2');
		?>
        <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td valign="top" style="vertical-align:top;">
    <h2><?php echo $ptitle; ?></h2>
    <input name="typeid1" type="hidden" value="<?php echo $typeid;?>" />
      <?php echo $html; ?>
      </td>
    </tr>
  <tr>
    <td valign="top" style="vertical-align:top;"> <br /></td>
    </tr>
  <tr>
    <td valign="top" style="vertical-align:top;">
    <span style="text-align:center;">
      <input  type="submit" name="submit" value="Generate Graph Report" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" />
    </span></td>
    </tr>
      </table>
      
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
  <tr>
    <td>
    <?php
	if ($graphdata==1)
	{
	//This page demonstrates the ease of generating charts using FusionCharts.
	//For this chart, we've used a pre-defined Data.xml (contained in /Data/ folder)
	//Ideally, you would NOT use a physical data file. Instead you'll have 
	//your own PHP scripts virtually relay the XML data document. Such examples are also present.
	//For a head-start, we've kept this example very simple.
	if ($typeid==1)
	{
		$graphfile = "FCF_Line.swf";
	}
	else {
		$graphfile = "FCF_Column3D.swf";
	}
	
	//Create the chart - Column 3D Chart with data from Data/Data.xml
	echo renderChartHTML(base_url()."/Charts/".$graphfile."", base_url()."testFile.xml", "", "myFirst", 840, 400);
	}
	else {
		echo '<p class="red">Use date filters above!</p>';
	}
	
?>
    </td>
  </tr>
</table>

        
    <? } ?>
    </div>
    </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
