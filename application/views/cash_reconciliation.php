<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cash Reconciliation</title>
<script>
function newDoc()
  {
  window.location.assign("<?=site_url('admins/crud_cash_recon')?>")
  }
</script>
</head>

<body onLoad="window.print(); ">
<div style="float:left;">
<table width="787" align="center">
  <tr>
    <th scope="col" align="center"><h1>MODERN COAST EXPRESS LTD 
    <p>CASH RECONCILIATION SUMMARY</p>
        <hr noshade /></h1></th>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
  </tr>
</table>

<table style="width:350px; float:left; margin-right:20px;" cellpadding="0" cellspacing="0" class="table1" border="0">
   
    <thead id="pageFooter">
  <tr>
    <th style="border-bottom:2px solid #000;" scope="col">Transdate</th>
    <th style="border-bottom:2px solid #000;" scope="col">Ticket No</th>
    <th style="border-bottom:2px solid #000; border-right:1px solid #000;" scope="col">Cost</th>
    </tr>
  </thead>
  <?php
  $totkes = 0;
	  $totugx = 0;
	  $tottzs = 0;
	  $totusd = 0;
  $totopenkes = 0;
	  $totopenugx = 0;
	  $totopentzs = 0;
	  $totopenusd = 0;
  if ($tickets==NULL)
  {
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No transactions generated</td></tr>";
  }
  else {
	  
	  $cnt=1;
	  foreach ($tickets as $t)
	  {
		//if ($t->ticketstatus<>"Open")
			//{
  ?>
  <tr>
    <td><?php echo date('d/m/Y'); ?></td>
    <td><?php 
	if ($t->ticketstatus=="Redeemed")
			{
	echo $t->newticketno;
			}
			elseif($t->ticketstatus=="Open")
			{
				echo $t->newticketno ;
			}
			elseif($t->ticketidreturn<>NULL)
			{
				echo $t->newticketno."R";
			}
			else {
	echo $t->newticketno; }?></td>
    <td style=" border-right:1px solid #000;"><?php 
	if ($t->t_currency==1)
	{
			if ($t->ticketstatus<>"Redeemed")
			{
				$totkes +=$t->amount;
				$totopenkes += $t->openticket_charge;
			
				echo "Kshs ".number_format($t->amount);
			}
			else {
				$totkes +=$t->voucheramount;
				//$totopenkes += $t->openticket_charge;
				echo "Kshs ".number_format($t->voucheramount);
			}
	}
	if ($t->t_currency==2)
	{
		if ($t->ticketstatus<>"Redeemed")
			{
		$totugx +=$t->amountug;
			$totopenugx += $t->openticket_chargeug;
		
		echo "Ushs ".number_format($t->amountug);
			}
			else {
				$totugx +=$t->voucheramount;
				echo "Ushs ".number_format($t->voucheramount);
			}
	}
	if ($t->t_currency==3)
	{
		if ($t->ticketstatus<>"Redeemed")
			{
		$tottzs +=$t->amounttz;
			$totopentzs +=$t->openticket_chargetz;
		echo "Tshs ".number_format($t->amounttz);
			}
			else {
			$tottzs +=$t->voucheramount;
		echo "Tshs ".number_format($t->voucheramount);
			}
	}
	if ($t->t_currency==4)
	{
		if ($t->ticketstatus<>"Redeemed")
			{
		$totusd +=$t->amountusd;
			$totopenusd +=$t->openticket_chargeusd;

		echo "$".number_format($t->amountusd);
			}
			else {
				$totusd +=$t->voucheramount;
				echo "$".number_format($t->voucheramount);
			}
	}
	 ?></td>
  </tr>
  <? //$cnt++; 
  if ($t->ticketstatus=="Redeemed");
  {
  $opent = $this->bus_model->gettickettoprint($t->TicketId);
  if ($opent->cancel_date==date('Y-m-d'))
  {
	?>  
  
  
  
  
  
  
  
  <tr>
    <td><?php echo date('d/m/Y'); ?></td>
    <td><?php 
	
				echo $opent->newticketno;?></td>
    <td style=" border-right:1px solid #000;"><?php 
	if ($t->t_currency==1)
	{
			
				$totkes +=$t->amount;
				$totopenkes += $t->openticket_charge;
			
				echo "Kshs ".number_format($t->amount);
			
	}
	if ($t->t_currency==2)
	{
		$totugx +=$t->amountug;
			$totopenugx += $t->openticket_chargeug;
		
		echo "Ushs ".number_format($t->amountug);
			
	}
	if ($t->t_currency==3)
	{
		
		$tottzs +=$t->amounttz;
			$totopentzs +=$t->openticket_chargetz;
		echo "Tshs ".number_format($t->amounttz);
			
	}
	if ($t->t_currency==4)
	{
		
		$totusd +=$opent->amountusd;
			$totopenusd +=$opent->openticket_chargeusd;

		echo "$".number_format($opent->amountusd);
			
	}
	 ?></td>
  </tr>
  
  
  
  
  
  
  
  
  
  
  
 <?  }
 $cnt++;}
  
  } }
  
  if ($tickets<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Sales</strong></td>
    <td style=" border-right:1px solid #000; border-bottom:double #000;"><strong>
      <? 
	if ($totkes<>0)
	{
	echo "Kshs ".number_format($totkes)."<br />";
	}
	if ($totugx<>0)
	{
	echo "Ushs ".number_format($totugx)."<br />";
	}
	if ($tottzs<>0)
	{
	echo "Tshs ".number_format($tottzs)."<br />";
	}
	if ($totusd<>0)
	{
	echo "$".number_format($totusd)."<br />";
	}
	?>
    </strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Open Ticket Sales</strong></td>
    <td style=" border-right:1px solid #000;">
    <strong>
      <? 
	if ($totopenkes<>0)
	{
	echo "Kshs ".number_format($totopenkes)."<br />";
	}
	if ($totopenugx<>0)
	{
	echo "Ushs ".number_format($totopenugx)."<br />";
	}
	if ($totopentzs<>0)
	{
	echo "Tshs ".number_format($totopentzs)."<br />";
	}
	if ($totopenusd<>0)
	{
	echo "$".number_format($totopenusd)."<br />";
	}
	?>
    </strong>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Sales</strong></td>
    <td style=" border-right:1px solid #000; border-bottom:double #000;">
    
     <strong>
      <? 
	  $totkesales = $totkes + $totopenkes;
	  $totugsales = $totugx + $totopenugx;
	  $tottzsales = $tottzs + $totopentzs;
	  $totusdsales = $totusd + $totopenusd;
	if ($totkesales<>0)
	{
	echo "Kshs ".number_format($totkesales)."<br />";
	}
	if ($totugsales<>0)
	{
	echo "Ushs ".number_format($totugsales)."<br />";
	}
	if ($tottzsales<>0)
	{
	echo "Tshs ".number_format($tottzsales)."<br />";
	}
	if ($totusdsales<>0)
	{
	echo "$".number_format($totusdsales)."<br />";
	}
	?>
    </strong>
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td style=" border-right:1px solid #000;">&nbsp;</td>
  </tr>
  <? } ?>
</table>

<table style="width:350px; float:left;" cellpadding="0" cellspacing="0" class="table1" border="0">
   
    <thead id="pageFooter">
  <tr>
    <th style="border-bottom:2px solid #000;" scope="col">Date</th>
    <th style="border-bottom:2px solid #000;" scope="col">Description</th>
    <th style="border-bottom:2px solid #000;" scope="col">Amount</th>
    </tr>
  </thead>
  <?php 
  $totpkes = 0;
	  $toptugx = 0;
	  $totptzs = 0;
	  $totpusd = 0;
  if ($pcs==NULL)
  {//echo $totkes;
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No transactions generated</td></tr>";
  }
  else {
	 
	  $cntz=1;
	  foreach ($pcs as $p)
	  {
		  
  ?>
  <tr>
    <td><?php echo date('d/m/Y'); ?></td>
    <td><?php 
	echo $p->Description; ?></td>
    <td><?php 
	
        
	echo $p->KeAmount;
	$totpkes += $p->KeAmount;
	 ?></td>
  </tr>
  <? $cntz++; } }
  //echo $totkes;
  if ($pcs<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total</strong></td>
    <td><? echo $totpkes; ?></td>
  </tr>
  <?  } $cb = $totkesales-$totpkes;?>
</table><p style="border-bottom:1px solid #000; font-weight:bold; float:left; width:100%; margin:20px;">
<span style="float:left;">Cashier: <? echo $username; ?></span><span style="text-align:center; margin-left:45px;">Printed By: <? echo $printeruser." on ".date('d/m/Y g:i A'); ?></span> <?php echo nbs(3); ?><span ">Closing Balance: <? 
echo "Kshs ".number_format($cb).", Ushs ".number_format(($totugsales-(0)))."<br />";
?>
</div>

</body>
</html>