<?php
$data = array(
'title'=>'Manage Bus Hire',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus hire',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datefrom').datepicker({  
        duration: '',  
        showTime: false, 
		dateFormat:'yy-mm-dd', 
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	     $('#dateto').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd',
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	     $('#datefromedit').datetimepicker({  
        duration: '',  
        showTime: false,  
		dateFormat:'yy-mm-dd', 
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#datetoedit').datetimepicker({  
        duration: '',  
        showTime: false,  
		dateFormat:'yy-mm-dd', 
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Hire</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Bus Hire</a></li>
      <li><a href="#create" name="#tab2">Add Bus Hire</a></li>
      <li><a href="#edit" name="#tab3">Edit Bus Hire</a></li>
      <li><a href="#delete" name="#tab4">Delete Bus Hire</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($bushire)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="147">Bus</th>
			  <th width="197">Hirer</th>
			  <th width="171">Hired From</th>
			  <th width="155">Hired To</th>
	          <th width="147">Status</th>
	          <th width="323">Options</th>
		  </tr>
		</thead>
        <?php foreach ($bushire as $item): ?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->HirerName; ?></td>
              <td align="left"><?php echo $item->DateFrom; ?></td>
              <td align="left"><?php echo $item->DateTo; ?></td>
			  <td align="left"><?php echo $item->AuthorizedStatus; ?></td>
			  <td align="left"><?php if($item->AuthorizedStatus!='returned'):echo anchor('admins/edit_bus_hire/'.$item->BusHireId.'/#tab3','Edit'); endif;?>  <?php if($item->AuthorizedStatus=='pending'):?><?php
						
						 
						 $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/authorize_bus_hire/'.$item->BusHireId.'/'.$item->BusId, '| Authorize', $atts);endif
						?> <?php if($item->AuthorizedStatus=='authorized'):?><?php
						
						 
						 $atts = array(
              'width'      => '550',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/bus_return/'.$item->BusHireId.'/'.$item->BusId, '| Confirm Return', $atts); endif;?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus hired yet!</p>';}; ?>


    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus_hire');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus:</td>
          <td width="519"><span class="red"><?php echo form_error('bustypelkp'); ?></span>
            <select name="bus" id="bus">
              <option disabled="disabled" selected="selected">---select bus---</option>
              <?php foreach($buses as $bus){?>
              <option value="<?php echo $bus->BusId;?>"><?php echo $bus->RegistrationNumber;?></option>
              <?php }?>
          </select></td>
        </tr>
        
                <tr>
          <td class="labels">Date From:</td>
          <td><span class="red"><?php echo form_error('datefrom'); ?></span>
            <input type="text" name="datefrom"  id="datefrom"  class="long_input" value="<?php echo $this->input->post('datefrom');?>" readonly="readonly"/></td>
        </tr>
                   <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('dateto'); ?></span>
            <input type="text" name="dateto"  id="dateto"  class="long_input" value="<?php echo $this->input->post('dateto');?>" readonly="readonly"/></td>
        </tr>
        <tr>
          <td colspan="2" class="labels">HIRER DETAILS</td>
        </tr>
        <tr>
          <td class="labels"> Name:</td>
          <td><span class="red"><?php echo form_error('name'); ?></span>
            <input type="text" name="name"  id="name"  class="long_input" value="<?php echo $this->input->post('name');?>"/></td>
        </tr>
        <tr>
          <td class="labels">Phone:</td>
          <td><span class="red"><?php echo form_error('phone'); ?></span>
            <input type="text" name="phone" id="long_input" value="<?php echo $this->input->post('phone');?>"></td>
        </tr>
        <tr>
          <td class="labels">Address:</td>
          <td><span class="red"><?php echo form_error('address'); ?></span>
            <textarea name="address" id="long_input"><?php echo $this->input->post('address');?></textarea></td>
        </tr>
        <tr>
          <td class="labels">Email:</td>
          <td><?php echo form_error('email'); ?></span>
            <input type="text" name="email" id="long_input" value="<?php echo $this->input->post('email');?>"></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Bus Hire" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if($row<>NULL){?>
	<?php echo form_open('admins/update_bus_hire');?>
    <?php //foreach($bushiredetails as $row):?>
    <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Bus:</td>
          <td width="519"><span class="red"><?php echo form_error('bus'); ?></span>
            <select name="bus" id="bus">
              <option disabled="disabled" selected="selected">---select bus---</option>
              <?php foreach($buses as $bus){?>
              <option value="<?php echo $bus->BusId;?>" <?php if(!(strcmp($bus->BusId, $row->BusId))) :echo "SELECTED";endif;?>><?php echo $bus->RegistrationNumber;?></option>
              <?php }?>
          </select></td>
        </tr>
        
                <tr>
          <td class="labels">Date From:</td>
          <td><span class="red"><?php echo form_error('datefrom'); ?></span>
            <input type="text" name="datefrom"  id="datefromedit"  class="long_input" value="<?php echo $row->DateFrom;?>" readonly="readonly"/></td>
        </tr>
                   <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('dateto'); ?></span>
            <input type="text" name="dateto"  id="datetoedit"  class="long_input" value="<?php echo $row->DateTo;?>" readonly="readonly"/></td>
        </tr>
        <tr>
          <td colspan="2" class="labels">HIRER DETAILS</td>
        </tr>
        <tr>
          <td class="labels"> Name:</td>
          <td><span class="red"><?php echo form_error('name'); ?></span>
            <input type="text" name="name"  id="name"  class="long_input" value="<?php echo $row->HirerName;?>"/></td>
        </tr>
        <tr>
          <td class="labels">Phone:</td>
          <td><span class="red"><?php echo form_error('phone'); ?></span>
            <input type="text" name="phone" id="long_input" value="<?php echo $row->HirerPhone;?>"></td>
        </tr>
        <tr>
          <td class="labels">Address:</td>
          <td><span class="red"><?php echo form_error('address'); ?></span>
            <textarea name="address" id="long_input"><?php echo $row->HirerAddress;?></textarea></td>
        </tr>
        <tr>
          <td class="labels">Email:</td>
          <td><?php echo form_error('email'); ?></span>
            <input type="text" name="email" id="long_input" value="<?php echo $row->HirerEmail;?>"><input type="hidden" name="bushireid" value="<?php echo $row->BusHireId;?>" /><input type="hidden" name="hirerid" value="<?php echo $row->HirerId;?>" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Edit Bus Hire" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php //endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select bus hire from \'Bus Hire\' tab!</p>';}; ?>
    </div>
    <div id="tab4">    	 <?php if(!empty($bushire)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="218">Bus</th>
			  <th width="198">Hirer</th>
			  <th width="181">Hired From</th>
			  <th width="200">Hired To</th>
	          <th width="178">Status</th>
	          <th width="165">Options</th>
		  </tr>
		</thead>
        <?php foreach ($bushire as $item): ?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber;?></td>
			  <td align="left"><?php echo $item->HirerName; ?></td>
              <td align="left"><?php echo $item->DateFrom; ?></td>
              <td align="left"><?php echo $item->DateTo; ?></td>
			  <td align="left"><?php echo $item->AuthorizedStatus; ?></td>
			  <td align="left"><?php echo anchor('admins/delete_bus_hire/'.$item->BusHireId.'','X',array('class' => 'delete','title'=>''.$item->RegistrationNumber.'')); ?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
    <?php // endif; ?>
    <?php }else{ echo '<p class="red">Sorry, no bus hired yet!</p>';}; ?></div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
