<?php
$data = array(
'title'=>'Clerk Report',
'keywords'=>' ',
'description'=>' ',
'selected'=>'clerk report',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true 
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true
     });  
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Clerk Report</p>

<div id="rightcontentwide">
    
  <div id="content">
   
    
   
    <div id="tab2">

    <table width="100%" cellpadding="0" cellspacing="0" class="table1">
    <thead id="pageFooter">
    <tr><th colspan="11" style="text-align:center;"><strong><?php echo $reporttitle;?></strong></th></tr>
    </thead>
    <thead id="pageFooter">
  <tr>
    <th scope="col">#</th>
    <th scope="col">Ticket No</th>
    <th scope="col">Customer</th>
    <th scope="col">Route</th>
    <th scope="col">Paymode</th>
    <th scope="col">Seat No</th>
    <th scope="col">Bus</th>
    <th scope="col">TravelDate</th>
    <th scope="col">Booked By</th>
    <th scope="col">Amount</th>
  </tr>
  </thead>
  <?php
  $cnt = 1;
  if ($tickets==NULL)
  {
	  echo "<tr><td colspan=\"10\" style=\"text-align:center;\">No transactions yet today</td></tr>";
  }
  else {
	  $totkes = 0;
	  $totugx = 0;
	  $tottzs = 0;
	  $totusd = 0;
	   if ($tickdet==2)
		  {
			  
	  foreach ($tickets as $t)
	  {
		 
			if ($t->t_currency==1)
			{
				$totkes +=$t->amount;
			}
			if ($t->t_currency==2)
			{
				$totugx +=$t->amountug;
			}
			if ($t->t_currency==3)
			{
				$tottzs +=$t->amounttz;
			}
			if ($t->t_currency==4)
			{
				$totusd +=$t->amountusd;
			}
		  }
		  }
		  else {
			foreach ($tickets as $t)
	  {  
		  
  ?>
  <tr>
    <td><?php echo $cnt; ?></td>
    <td><?php echo $t->newticketno; ?></td>
    <td><?php echo $t->clientname; ?></td>
    <td><?php echo $t->TownFrom."-".$t->TownTo; ?></td>
    <td><?php echo $t->paymodes; ?></td>
    <td><?php echo $t->seatno; ?></td>
    <td><?php echo $t->bus_number; ?></td>
    <td><?php echo $t->dateonly; ?></td>
    <td><?php echo $t->t_username; ?></td>
    <td><?php 
	if ($t->t_currency==1)
	{
		$totkes +=$t->amount;
		echo "Kshs ".number_format($t->amount);
	}
	if ($t->t_currency==2)
	{
		$totugx +=$t->amountug;
		echo "Ushs ".number_format($t->amountug);
	}
	if ($t->t_currency==3)
	{
		$tottzs +=$t->amounttz;
		echo "Tshs ".number_format($t->amounttz);
	}
	if ($t->t_currency==4)
	{
		$totusd +=$t->amountusd;
		echo "$".number_format($t->amountusd);
	}
	 ?></td>
  </tr>
  <? $cnt++; } } }
  
  if ($tickets<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>Total</strong></td>
    <td><strong><? 
	if ($totkes<>0)
	{
	echo "Kshs ".number_format($totkes)."<br />";
	}
	if ($totugx<>0)
	{
	echo "Ushs ".number_format($totugx)."<br />";
	}
	if ($tottzs<>0)
	{
	echo "Tshs ".number_format($tottzs)."<br />";
	}
	if ($totusd<>0)
	{
	echo "$".number_format($totusd)."<br />";
	}
	?></strong></td>
  </tr>
  <?  } ?>
</table>
    </div>
    </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
