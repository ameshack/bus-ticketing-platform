<?php
$data = array(
'title'=>'Manage Discount',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage discount',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datepicker({  
        duration: '',  
        showTime: false, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Discounts</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Discounts</a></li>
      <li><a href="#add" name="#tab2">Add Discount</a></li>
      <li><a href="#day" name="#tab3">Day Based Discounts</a></li>
      <li><a href="#time" name="#tab4">Time Based Discounts</a></li>
      <li><a href="#return" name="#tab5">Return Discounts</a></li>
      
       
    </ul>  
  <div id="content">
    <div id="tab1">
    	 <?php if(!empty($discount)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="153">Discount Name</th>
			  <th width="93">Discount From</th>
			  <th width="107">Discount To</th>
			  <th width="107">Amount/percentage</th>
			  <th width="118">Description</th>
	          <th width="105">Discount Type</th>
          </tr>
		</thead>
        <?php foreach ($discount as $item): if($item->Percent_or_Amount == 1): $type = "Amount";else:$type = "Percentage"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->Disc_Name;?></td>
			  <td align="left"><?php echo $item->Disc_From_Date; ?></td>
              <td align="left"><?php echo $item->Disc_To_Date; ?></td>
              <td align="left"><?php echo "Ksh ".number_format($item->Disc_Amountke)."<br />"; 
			  echo "Ush ".number_format($item->Disc_Amountug)."<br />";
			  echo "Tsh ".number_format($item->Disc_Amounttz);
			  ?></td>
              <td align="left"><?php echo $item->Disc_Desc; ?></td>
			  <td align="left"><?php echo $type; ?></td>
		    </tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no discount has been added yet!</p>';}; ?>


    </div>
    
    <div id="tab2">
<?php $attr=array('name'=>'discount'); echo form_open('admins/create_discounts',$attr);?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
              <tr>
          <td height="24" class="labels">Discount Name:</td>
          <td><span class="red"><?php echo form_error('disc_name'); ?></span>
            <input type="text" name="disc_name" id="long_input" value="<?php echo $this->input->post('disc_name');?>" >
            <input type="hidden" name="branchtot" id="long_input" value="<?php echo $branchtot;?>" ></td>
        </tr>
        <tr>
          <td width="169" class="labels">Amount or percentage:</td>
          <td width="519"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="Percent_or_Amount" value="1" id="Percent_or_Amount_0" />
                Amount</label>
              <br />
              <label>
                <input type="radio" name="Percent_or_Amount" value="2" id="Percent_or_Amount_1" />
                Percentage</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td height="24" class="labels">KE Amount:</td>
          <td><?php echo form_error('disc_amountke'); ?></span>
            <input type="text" name="disc_amountke" id="long_input" value="<?php echo $this->input->post('disc_amountke');?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Ug Amount:</td>
          <td><?php echo form_error('disc_amountug'); ?></span>
            <input type="text" name="disc_amountug" id="long_input" value="<?php echo $this->input->post('disc_amountug');?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Tz Amount:</td>
          <td><?php echo form_error('disc_amounttz'); ?></span>
            <input type="text" name="disc_amounttz" id="long_input" value="<?php echo $this->input->post('disc_amounttz');?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Description:</td>
          <td><?php echo form_error('Disc_Desc'); ?></span>
            <textarea name="Disc_Desc"  class='animated' id="long_input" ><?php echo $this->input->post('Disc_Desc');?></textarea></td>
        </tr>

        
        <tr>
          <td class="labels">Date From:</td>
          <td><span class="red"><?php echo form_error('Disc_From_Date'); ?></span>
            <input type="text" name="Disc_From_Date"  id="Disc_From_Date"  readonly="readonly" class="long_input" value="<?php echo $this->input->post('Disc_From_Date');?>"/></td>
        </tr>
         <tr>
          <td class="labels">Date To:</td>
          <td><span class="red"><?php echo form_error('Disc_To_Date'); ?></span>
            <input type="text" name="Disc_To_Date"  id="Disc_To_Date"  readonly="readonly" class="long_input" value="<?php echo $this->input->post('Disc_To_Date');?>"/></td>
        </tr>
         <tr>
           <td></td>
           <td>Select Branches</td>
         </tr>
         <tr>
           <td colspan="2"><?=$branches;?></td>
         </tr>
         <tr>
           <td>Select Bus Type</td>
           <td><select name="bustypelkp" id="bustypelkp" >
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
         </tr>
         <tr>
           <td>Select Seat Class</td>
           <td><select name="seatclass" id="seatclass" onChange="loadSeats(this.value);">
                  <option value="" selected="selected" disabled="disabled">Select seat class</option>
                  <option value="1">VIP</option>
                  <option value="2">First Class</option>
                  <option value="3">Business</option>
                  </select></td>
         </tr>
         <tr>
           <td colspan="2"><div id="seats"></div></td>
         </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Discount" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
    <?php if(!empty($discountd)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="5" style="text-align:center;"><strong><h3>This is discount that is applied to specific days in a week</h3></strong></th>
</tr>
			<tr align="left">
			  <th width="153">Discount Name</th>
			  <th>Days</th>
			  <th width="107">Amount/percentage</th>
			  <th width="118">Description</th>
	          <th width="105">Discount Type</th>
          </tr>
		</thead>
        <?php foreach ($discountd as $item): if($item->Percent_or_Amount == 1): $type = "Amount";else:$type = "Percentage"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->Disc_Name;?></td>
			  <td align="left"><?php echo trim($item->day_values,','); ?></td>
              <td align="left"><?php echo "Ksh ".number_format($item->Disc_Amountke)."<br />"; 
			  echo "Ush ".number_format($item->Disc_Amountug)."<br />";
			  echo "Tsh ".number_format($item->Disc_Amounttz);
			  ?></td>
              <td align="left"><?php echo $item->Disc_Desc; ?></td>
			  <td align="left"><?php echo $type; ?></td>
		    </tr>
		<?php 
		$days2 = trim($item->day_values,',');
		$ddiscid = $item->Disc_Id;  endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no discount has been added yet!</p>';} ?>
            
          
<?php $attr=array('name'=>'discount'); echo form_open('admins/update_day_discounts',$attr);
$ddisc = $this->bus_model->getonerowfromonetbl('discounts','Disc_Id', $ddiscid);
?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="199" class="labels">Amount or percentage:</td>
          <td width="931"><span class="red"><?php echo form_error('Percent_or_Amountd'); ?></span>
            <p>
              <label>
                <input type="radio" name="Percent_or_Amountd" value="1" checked="checked" id="Percent_or_Amount_0" />
                Amount</label>
              <br />
              <label>
                <input type="radio" name="Percent_or_Amountd" value="2" id="Percent_or_Amount_1" disabled="disabled" />
                Percentage</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td height="24" class="labels">KE Amount:</td>
          <td><?php echo form_error('disc_amountke'); ?></span>
            <input type="text" name="disc_amountke" id="long_input" value="<?php echo $ddisc->Disc_Amountke;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Ug Amount:</td>
          <td><?php echo form_error('disc_amountug'); ?></span>
            <input type="text" name="disc_amountug" id="long_input" value="<?php echo $ddisc->Disc_Amountug;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Tz Amount:</td>
          <td><?php echo form_error('disc_amounttz'); ?></span>
            <input type="text" name="disc_amounttz" id="long_input" value="<?php echo $ddisc->Disc_Amounttz;?>" ></td>
        </tr>
        <tr>
          <td width="199" class="labels">Apply if other discounts are active?</td>
          <td width="931"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="discountactive" value="1" <?php if ($ddisc->If_Other_Discount_Active==1):echo 'checked'; endif; ?> id="discountactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="discountactive" value="0" <?php if ($ddisc->If_Other_Discount_Active==0):echo 'checked'; endif; ?>  id="discountactive_1" />
                No</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td width="199" class="labels">Apply if a season is active?</td>
          <td width="931"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="seasonactive" value="1" <?php if ($ddisc->If_Other_Season_Active==1):echo 'checked'; endif; ?> id="seasonactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="seasonactive" value="0" <?php if ($ddisc->If_Other_Season_Active==0):echo 'checked'; endif; ?> id="seasonactive_1" />
                No</label>
              <br />
            </p>        <input name="discid" type="hidden" value="<?php echo $ddiscid;?>" /> </td>
        </tr>
        <tr>
          <td width="199" class="labels">Route:</td>
          <td width="931"><span class="red"><?php echo form_error('route'); ?></span>
            <select name="route" id="route" onChange="displayroutetowns(this.value);">
              <option disabled="disabled" selected="selected">---select route--</option>
           <option value="0">All</option>
              <?php foreach($routes as $route){?>
              <option value="<?php echo $route->RouteId;?>"><?php echo $route->RouteName;?></option>
              <?php }?>
          </select>
          <div id="routetowns"></div>
          </td>
        </tr>
         <tr>
           <td>Select Bus Type</td>
           <td><select name="bustypelkp" id="bustypelkp" >
           <option selected="selected" value="0">All</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
         </tr>
         <tr>
           <td></td>
           <td>Select Days</td>
         </tr>
         <tr>
           <td colspan="2">
           <?php 
		   $daysz = explode(",",$days2);
		   ?>
           <input name="days[]" <?php if (in_array("Monday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Monday" />Monday | 
           <input name="days[]" <?php if (in_array("Tuesday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Tuesday" />Tuesday | 
           <input name="days[]" <?php if (in_array("Wednesday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Wednesday" />Wednesday | 
           <input name="days[]" <?php if (in_array("Thursday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Thursday" />Thursday | 
           <input name="days[]" <?php if (in_array("Friday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Friday" />Friday | 
           <input name="days[]" <?php if (in_array("Saturday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Saturday" />Saturday | 
           <input name="days[]" <?php if (in_array("Sunday", $daysz)): echo 'checked'; endif; ?> type="checkbox" value="Sunday" />Sunday | 
           </td>
         </tr>
         <tr>
           <td colspan="2"></td>
         </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Edit Day Based Discount" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>  
    </div>
    <div id="tab4">
    <?php if(!empty($discountt)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="5" style="text-align:center;"><strong>
<h3>This is discount that is applied to specific departure or arrival times(especially for highway routes)</h3></strong></th>
</tr>
			<tr align="left">
			  <th width="153">Discount Name</th>
			  <th>Times</th>
			  <th width="107">Amount/percentage</th>
			  <th width="118">Description</th>
	          <th width="105">Discount Type</th>
          </tr>
		</thead>
        <?php foreach ($discountt as $item): if($item->Percent_or_Amount == 1): $type = "Amount";else:$type = "Percentage"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->Disc_Name;?></td>
			  <td align="left"><?php $times = trim($item->time_values,',');
			  $tz = explode(",",$times);
			  $tztot = count($tz);
			  $timesz = "";
			  for($i=0;$i<$tztot;$i++):
			  $time = $this->bus_model->getonerowfromonetbl('reporting_departure_times_lkp','TimeId', $tz[$i]);
			  $timesz .= date('g:i A',strtotime($time->ArrivalDeparture)).",";
			  endfor;
			  echo trim($timesz,",");
			   ?></td>
              <td align="left"><?php echo "Ksh ".number_format($item->Disc_Amountke)."<br />"; 
			  echo "Ush ".number_format($item->Disc_Amountug)."<br />";
			  echo "Tsh ".number_format($item->Disc_Amounttz);
			  ?></td>
              <td align="left"><?php echo $item->Disc_Desc; ?></td>
			  <td align="left"><?php echo $type; ?></td>
		    </tr>
		<?php $tdiscid = $item->Disc_Id; endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no discount has been added yet!</p>';} ?>
            
          
<?php $attr=array('name'=>'discount'); echo form_open('admins/update_time_discounts',$attr);
$tdisc = $this->bus_model->getonerowfromonetbl('discounts','Disc_Id', $tdiscid);

?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Amount or percentage:</td>
          <td width="519"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="Percent_or_Amountt" value="1" checked="checked" id="Percent_or_Amount_0" />
                Amount</label>
              <br />
              <label>
                <input type="radio" name="Percent_or_Amountt" value="2" id="Percent_or_Amount_1" disabled="disabled" />
                Percentage</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td height="24" class="labels">KE Amount:</td>
          <td><?php echo form_error('disc_amountke'); ?></span>
            <input type="text" name="disc_amountke" id="long_input" value="<?php echo $tdisc->Disc_Amountke;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Ug Amount:</td>
          <td><?php echo form_error('disc_amountug'); ?></span>
            <input type="text" name="disc_amountug" id="long_input" value="<?php echo $tdisc->Disc_Amountug;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Tz Amount:</td>
          <td><?php echo form_error('disc_amounttz'); ?></span>
            <input type="text" name="disc_amounttz" id="long_input" value="<?php echo $tdisc->Disc_Amounttz;?>" ></td>
        </tr>
        <tr>
          <td width="169" class="labels">Apply if other discounts are active?</td>
          <td width="519"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="discountactive" value="1" <?php if ($tdisc->If_Other_Discount_Active==1):echo 'checked'; endif; ?> id="discountactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="discountactive" value="0" <?php if ($tdisc->If_Other_Discount_Active==0):echo 'checked'; endif; ?>  id="discountactive_1" />
                No</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td width="169" class="labels">Apply if a season is active?</td>
          <td width="519"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="seasonactive" value="1" <?php if ($tdisc->If_Other_Season_Active==1):echo 'checked'; endif; ?> id="seasonactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="seasonactive" value="0" <?php if ($tdisc->If_Other_Season_Active==0):echo 'checked'; endif; ?> id="seasonactive_1" />
                No</label>
              <br />
            </p>          <input name="discid" type="hidden" value="<?php echo $tdiscid;?>" /> </td>
        </tr>
        <tr>
          <td width="199" class="labels">Route:</td>
          <td width="931"><span class="red"><?php echo form_error('route'); ?></span>
            <select name="route" id="route" onChange="displayroutetowns1(this.value);">
              <option disabled="disabled" selected="selected">---select route--</option>
           <option value="0">All</option>
              <?php foreach($routes as $route){?>
              <option value="<?php echo $route->RouteId;?>"><?php echo $route->RouteName;?></option>
              <?php }?>
          </select> <div id="routetowns1"></div></td>
        </tr>
         <tr>
           <td>Select Bus Type</td>
           <td><select name="bustypelkp" id="bustypelkp" >
           <option selected="selected" value="0">All</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select></td>
         </tr>
         <tr>
           <td></td>
           <td>Select Times</td>
         </tr>
         <tr>
           <td colspan="2"><?php echo $timez;?></td>
         </tr>
         <tr>
           <td colspan="2"></td>
         </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Edit Time Based Discount" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>  
    </div>
    
    <div id="tab5">
    <?php if(!empty($discountr)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="5" style="text-align:center;"><strong>
<h3>This is discount that is applied when one books a return ticket</h3></strong></th>
</tr>
			<tr align="left">
			  <th width="153">Discount Name</th>
			  <th width="107">Amount/percentage</th>
			  <th width="118">Description</th>
	          <th width="105">Discount Type</th>
          </tr>
		</thead>
        <?php foreach ($discountr as $item): if($item->Percent_or_Amount == 1): $type = "Amount";else:$type = "Percentage"; endif; ?>
			<tr>
				<td align="left"><?php echo $item->Disc_Name;?></td>
              <td align="left"><?php echo "Ksh ".number_format($item->Disc_Amountke)."<br />"; 
			  echo "Ush ".number_format($item->Disc_Amountug)."<br />";
			  echo "Tsh ".number_format($item->Disc_Amounttz);
			  ?></td>
              <td align="left"><?php echo $item->Disc_Desc; ?></td>
			  <td align="left"><?php echo $type; ?></td>
		    </tr>
		<?php $tdiscid = $item->Disc_Id; endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no discount has been added yet!</p>';} ?>
            
          
<?php $attr=array('name'=>'discount'); echo form_open('admins/update_return_discounts',$attr);
$tdisc = $this->bus_model->getonerowfromonetbl('discounts','Disc_Id', $tdiscid);

?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="199" class="labels">Amount or percentage:</td>
          <td width="931"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="Percent_or_Amountt" value="1" checked="checked" id="Percent_or_Amount_0" />
                Amount</label>
              <br />
              <label>
                <input type="radio" name="Percent_or_Amountt" value="2" id="Percent_or_Amount_1" disabled="disabled" />
                Percentage</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td height="24" class="labels">KE Amount:</td>
          <td><?php echo form_error('disc_amountke'); ?></span>
            <input type="text" name="disc_amountke" id="long_input" value="<?php echo $tdisc->Disc_Amountke;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Ug Amount:</td>
          <td><?php echo form_error('disc_amountug'); ?></span>
            <input type="text" name="disc_amountug" id="long_input" value="<?php echo $tdisc->Disc_Amountug;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">Tz Amount:</td>
          <td><?php echo form_error('disc_amounttz'); ?></span>
            <input type="text" name="disc_amounttz" id="long_input" value="<?php echo $tdisc->Disc_Amounttz;?>" ></td>
        </tr>
        <tr>
          <td class="labels">Activate?</td>
          <td>
          
          <span class="red"><?php echo form_error('discountactivate'); ?></span>
            <p>
              <label>
                <input type="radio" name="discountactivate" value="1" <?php if ($tdisc->activate==1):echo 'checked'; endif; ?> id="discountactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="discountactivate" value="0" <?php if ($tdisc->activate==0):echo 'checked'; endif; ?>  id="discountactive_1" />
                No</label></td>
        </tr>
        <tr>
          <td width="199" class="labels">Apply if other discounts are active?</td>
          <td width="931"><span class="red"><?php echo form_error('discountactive'); ?></span>
            <p>
              <label>
                <input type="radio" name="discountactive" value="1" <?php if ($tdisc->If_Other_Discount_Active==1):echo 'checked'; endif; ?> id="discountactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="discountactive" value="0" <?php if ($tdisc->If_Other_Discount_Active==0):echo 'checked'; endif; ?>  id="discountactive_1" />
                No</label>
              <br />
            </p>          </td>
        </tr>
        <tr>
          <td width="199" class="labels">Apply if a season is active?</td>
          <td width="931"><span class="red"><?php echo form_error('Percent_or_Amount'); ?></span>
            <p>
              <label>
                <input type="radio" name="seasonactive" value="1" <?php if ($tdisc->If_Other_Season_Active==1):echo 'checked'; endif; ?> id="seasonactive_0" />
                Yes</label>
              <br />
              <label>
                <input type="radio" name="seasonactive" value="0" <?php if ($tdisc->If_Other_Season_Active==0):echo 'checked'; endif; ?> id="seasonactive_1" />
                No</label>
              <br />
            </p>          <input name="discid" type="hidden" value="<?php echo $tdiscid;?>" /> </td>
        </tr>
         <tr>
           <td colspan="2"></td>
         </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Edit Return Discount" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>  
    </div>
    
  </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
