<?php
$data = array(
'title'=>'Manage Tickets',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage tickets',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>

<script type="text/javascript">  
$(function() { 
//var maxDate = getDateYymmdd($(this).data("val-rangedate-max")); 
	    $('#traveldatezz1').datepicker({ 
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,   
         minDate: 0,
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 	    $('#traveldate').datepicker({ 
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	     $('#traveltime1').timepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     });  
});  
</script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Tickets</p>

<div id="rightcontentwide">
 
    <ul id="tabs">
      <li><a href="#create" name="#tab1">Add Ticket</a></li>
      <li><a href="#read" name="#tab2">Booked and Reserved</a></li>
    </ul>
     <div id="content">
        <div id="tab1">
<?php 
$attr = array('name'=>'ticketing');
echo form_open('admins/get_schedules',$attr);?>
<table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th colspan="6">View Open Bookings</th>
		  </tr>
		</thead>
  <tr>
    <td width="141">Station</td>
    <td width="150"><?php echo form_error('t_station'); ?></span>
    <?php
	$this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$tstation= $this->user[0]->t_station;
	?>
            <select name="t_station" id="t_station" class="smallselect" onChange="getBusSchedules5(this.value)">
          
              <?php foreach($towns as $town){?>
              <option <?php if ($town->TownId==$townf) { echo "selected";} ?> value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select></td>
    <td width="174">Town To</td>
    <td width="187"><?php echo form_error('traveldate'); ?>
    <select name="t_stationto" id="t_stationto" class="smallselect" onChange="getBusSchedules5(this.value)">
          
              <?php foreach($towns as $town){
				  //if ($town->TownId!=$tstation){
				  ?>
              <option <?php if ($town->TownId==$townt) { echo "selected";} ?>  value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select>
      </td>
    <td width="194">Travel Date</td>
    <td width="125"><?php echo form_error('traveldate'); ?></span>
      <input type="text" name="traveldate" id="traveldatezz1" class="short_input"  readonly="readonly"  value="<?php if ($this->input->post('traveldate')): echo $this->input->post('traveldate'); else: echo date('Y-m-d'); endif;?>" onchange="getBusSchedules5(this.value)"></td>
  </tr>
  <tr>
    <td colspan="6" align="center" style="text-align:center;"><noscript><input  type="submit" name="submit" value="Search Schedules" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></noscript></td>
    </tr>
</table>
<?php echo form_close();?>

<table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="141">Route Code</th>
			  <th width="150">Route</th>
			  <th width="174">Travel Date</th>
			  <th width="194">Bus Type</th>
          </tr>
		</thead>
        <tr><table width="100%" id="schedules" class="table1">

</table>

       </tr>
</table>

</div>
<div id="tab2">
            	 <?php if(!empty($tickets)){?>
                 <?php echo form_open('admins/search_tickets/#tab2'); ?>
                 <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Ticket</th>
		  </tr>
		</thead>
        <tr>
        <td>Search By <select name="searchtype">
        <option value="ticket">Ticket No</option>
        <option value="name">Name</option>
        <option value="idno">Id No</option>
        <option value="phone">Phone No</option>
        <option value="voucher">Voucher No</option>
        <option value="issuedby">Issued By</option>
        </select><input name="searchvalue" type="text" id="long_input" />
        <input  type="submit" name="submit" value="Search Ticket" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px">
        </td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Date</th>
			  <th> Time</th>
			  <th>Traveller Name</th>
			  <th>Id No</th>
			  <th>Seat</th>
	          <th>Ticket No</th>
	          <th>Status</th>
	          <th>Amount</th>
	          <th>Route</th>
		      <th>Options</th>
		  </tr>
		</thead>
        <?php foreach ($tickets as $item): ?>
			<tr>
			  <td align="left"><?php echo date('d/m/Y',strtotime($item->DepartureDateTime)); ?></td>
			  <td align="left"><?php echo $item->reporttime;?></td>
			  <td align="left"><?php echo $item->clientname; ?></td>
              <td align="left"><?php echo $item->t_idno; ?></td>
              <td align="left"><?php echo $item->seatno;?></td>
			  <td align="left"><?php echo $item->newticketno; ?></td>
			  <td align="left"><?php echo $item->ticketstatus;?></td>
			  <td align="left"><?php 
			  if ($item->t_currency==1)
			  {
				  echo "KSH ".number_format($item->amount,2);
			  }
			  if ($item->t_currency==2)
			  {
				  echo "UGX ".number_format($item->amountug,2);
			  }
			  if ($item->t_currency==3)
			  {
				  echo "TZS ".number_format($item->amounttz,2);
			  }
			  if ($item->t_currency==4)
			  {
				  echo "USD ".number_format($item->amountusd,2);
			  }
			  if ($item->t_currency==5)
			  {
				  echo "EUR ".number_format($item->amounteuro,2);
			  }
			  
			  ?></td>
			  <td align="left"><?php echo $item->TownFrom." - ".$item->TownTo;?></td>
			  <td align="left">
			  <?php
$user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		$usergrp = $user[0]->uacc_group_fk;
?>
<?php
if ($usergrp==2 || $usergrp==3){
?>
			  <?php if($item->ticketstatus=='Reserved' || $item->ticketstatus=='Active'){
			  $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/cancel_ticket/'.$item->TicketId, 'CANCEL | ', $atts); } elseif($item->ticketstatus=='Inactive') {
							echo "<font color=\"#FF0000\">Cancelled |  </font>"; $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/ticket_cancel_reason/'.$item->TicketId, 'Reason', $atts);
						}?>&nbsp;
		      <?php 
			  $atr = array('Confirm Ticket', 
			  'class'=>'confirm');
			   $atr1 = array('width'      => '650',
              'height'     => '800',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30',
			  'title'=>'Open Ticket');
			  if($item->ticketstatus=='Active'){ echo anchor_popup('admins/open_ticket/'.$item->TicketId, 'Open Ticket',$atr1);
			  }
			  
			  echo anchor('admins/print_ticket/'.$item->TicketId, ' | Reprint');} elseif($item->ticketstatus=='Reserved') { 
			   $atr = array('Confirm Ticket', 
			  'class'=>'confirm');
			   $atr1 = array('width'      => '650',
              'height'     => '800',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30',
			  'title'=>'Open Ticket');
			  echo anchor('admins/confirm_booking/'.$item->TicketId, '| Confirm Booking',$atr);}?></td>
		</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Please select schedule from \'add ticket\' tab!</p>';}; ?>


    </div>
</div>
<script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

<?php  $this->load->view('extras/lower'); ?>
