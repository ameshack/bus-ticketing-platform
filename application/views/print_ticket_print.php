<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Bus Ticket</title>
<link href="<?=base_url()?>css/style.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">

body, html, h1,ul, h2, h3, p,span,td,tr,form,table,thead,tbody {
	margin: 0px;
	padding: 0px;
	/*font-size: 1em;*/
	font-weight: normal;
	background-position: left top;
	vertical-align: top;
} 
@page {
  size: A6 landscape;
}

body {
    margin:0;
    width: auto;
    font-family:Verdana, Geneva, sans-serif; 
	size: A6 landscape;
	padding-left:5px;
	
	
}
#pager {
	page-break-after:always;
}
#printwrapper {
	width:719px;
	font-size:12px;
	margin-right:5px;
	display:block;
	border:1px solid #000;
	float: left;
}
#topmost {
	width:718px;
	height:50px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
	
}
#thirdtopmost {
	width:718px;
	height:56px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
}
#thirdtopmost2 {
	width:718px;
	height:58px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
}
#secondtop {
	width:718px;
	height:42px;
	font-size:12px;
	margin:0;
	border-bottom:1px solid #000;
	float:left;
	
}
#bottommost {
	width:718px;
	height:40px;
	font-size:12px;
	margin:0;
	float:left;
	
}
#nleftpanestop {
	width:180px;
	height:40px;
	padding-bottom:2px;
	padding-top:2px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nleftpanes3top {
	width:190px;
	height:56px;
	padding-bottom:2px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nleftpanes4top {
	width:180px;
	height:56px;
	padding-bottom:2px;
	float:left;
	border-right:1px solid #000;
	text-align: left;
}
#nrightpanes3top {
	width:300px;
	height:40px;
	padding:0px;
	float:left;
	text-align: left;
}
#nrightpanes5top {
	width:718px;
	height:36px;
	padding-bottom:2px;
	float:left;
	text-align:left;
}
#nrightpanes4top {
	width:587px;
	height:34px;
	padding-bottom:2px;
	float:left;
	text-align: left;
}
#nrightpanestop {
	width:auto;
	height:40px;
	padding-bottom:2px;
	padding-top:2px;
	float:left;
	text-align: left;
}
#nleftpane {
	width:260px;
	height:49px;
	padding-bottom:2px;
	float:left;
	text-align: left;
}
#nrighttoppane {
	width:auto;
	height:49px;
	padding-bottom:2px;
	float:left;
	text-align: left;
}
#nleftpane1 {
	width:180px;
	height:205px;
	padding-bottom:2px;
	float:left;
	border:1px solid #000;
}
#nleftpane2 {
	width:280px;
	height:45px;
	padding-bottom:2px;
	float:left;
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-right:1px solid #000;
}

#nleftpane3 {
	width:240px;
	height:105px;
	padding:5px;
	float:left;
	
	border-left:1px solid #000;
	border-top:1px solid #000;
	border-bottom:1px solid #000;
}

#nleftpane5 {
	width:520px;
	height:105px;
	padding:5px;
	float:left;
	border:1px solid #000;
}

#nleftpane4 {
	width:432px;
	height:60px;
	padding:5px;
	float:left;
	border:1px solid #000;
}
.t90
{

/* Rotate div */

transform:rotate(-90deg);
-ms-transform:rotate(-90deg);  /*Internet Explorer */
-moz-transform:rotate(-90deg); /*Firefox */
-webkit-transform:rotate(-90deg);  /*Safari and Chrome*/
-o-transform:rotate(-90deg); /* Opera */
}
.style1 {font-weight: bold}
</style>
<script>
function newDoc()
  {
  window.location.assign("<?=site_url('admins/ticket_station')?>")
  }
</script>
</head>
<body onLoad="window.print(); newDoc(); window.close();">
<?
$tids = trim($tids, ",");
$tid = explode(",",$tids);
$tot = count($tid);
for($i=0;$i<$tot;$i++)
{
	$row = $this->bus_model->gettickettoprint($tid[$i]);
?>
<div id="pager">
<br>


<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<div id="printwrapper">
<div id="topmost">
<div id="nleftpane">
<p>Ticket No. <strong><?php echo $row->newticketno;?></strong></p>
<p>NAME <strong><?php echo $row->clientname;?></strong></p>
<p>ID <strong><?php echo $row->t_idno;?></strong></p>


    </div>

<div id="nrighttoppane">
<p>BUS <strong><?php echo "<span style=\"font-size:24px;\">".$row->bus_number."</span>(".$row->BusType.")";?></strong></p>
<p>MOBILE. <strong><?php echo $row->mobileno;?></strong></p>
</div>
</div>
<div id="secondtop">
<div id="nleftpanestop">
   <p>FROM </p>
    <p> <strong><?php echo $row->TownFrom;?></strong></p>
    </div>
    <div id="nleftpanestop">
    <p>TO</p>
      <p> <strong><?php echo $row->TownTo;?></strong></p>
    </div>
    <div id="nrightpanestop"><p style="vertical-align:top;">SEAT  <?php echo "Have a safe journey..."; ?></p>
        <p> <strong><?php echo $row->seatno;?></strong>
        
        </p>
     </div>

</div>
<div id="thirdtopmost">
<div id="nleftpanes3top">
    <p>DATE OF ISSUE<br>
      <strong><?php echo date('d/m/Y',strtotime(date('Y-m-d')));?></strong></p>
    </div>
    <div id="nleftpanes3top">
     <p>DATE OF TRAVEL<br>
       <strong><?php echo date('d/m/Y',strtotime($row->DepartureDateTime));?></strong></p>
    </div>
    <div id="nrightpanes3top">
    <p>FARE PAID<br>
      <strong><?php 
	  if($row->t_currency==1):
	  echo "Ksh ".number_format($row->amount,2);
	  endif;
	  if($row->t_currency==2):
	  echo "Ush ".number_format($row->amountug,2);
	  endif;
	  if($row->t_currency==3):
	  echo "Tsh ".number_format($row->amounttz,2);
	  endif;
	  if($row->t_currency==4):
	  echo "USD ".number_format($row->amountusd,2);
	  endif;
	  ?></strong></p>
    </div>

</div>
<div id="thirdtopmost2">
<div id="nleftpanes4top">
     <p>REPORTING TIME<br>
       <strong><?php echo date('g:i A',strtotime($row->reportingtime));?></strong></p>
</div>
    <div id="nleftpanes3top">
     <p>DEPARTURE TIME<br>
       <strong><?php echo date('g:i A',strtotime($row->departuretime));?></strong></p>
        </div>
    <div id="nrightpanes3top">
 <p><strong>DRIVER NOT ALLOWED TO PICK ANY PASSENGER ALONG THE WAY. INCASE THIS HAPPENS, PLEASE CALL 0738126849</strong></p>
    </div>
</div>
<div id="bottommost">
  <div id="nrightpanes5top">
     <p>SERVED BY<br>
       <span class="style1">
       <?php 
	   $this->user=$this->flexi_auth->get_user_by_id($row->t_userid)->result();
		$username= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
	   echo $username;
	   ?>
       </span></p>
  </div>

</div>
</div><br clear="all">
<p style="text-align:left; font-size:10px;">The management will not accept responsibility for any LOSS,DAMAGE or DELAY.Ticket Money will not be refunded.<br>
No refund will be made to passengers left behind. Passengers must take care of the their luggage.<br>
15 kilos of luggage free, any additional weight will be charged.<br>
Spirits, alcohol and smoking NOT ALLOWED.</p>
</div>
<? } ?>
</body>
</html>