<?php
$data = array(
'title'=>'Manage Messages',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage messages',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Messages</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Inbox</a></li>
      <li><a href="#create" name="#tab2">New Message</a></li>
      <li><a href="#edit" name="#tab3">Read Message</a></li>
      <li><a href="#delete" name="#tab4">Sent</a></li>
    </ul>
    <div id="content">
    <div id="tab1">
<?php 
if ($messages==NULL)
{
	echo "No messages added yet!";
}
if ($messages<>NULL)
{
?>
<div class="sorttable">
<div id="pagewrap">
 <div id="search">
        <label for="filter">Filter</label> <input type="text" name="filter" value="" id="filter" />
      </div>

<table width="100%" border="0" align="center" id="resultTable">
<thead>
  <tr>
    <th width="52">No</th>
    <th width="206">From</th>
    <th width="431">Message </th>
    <th width="122">Date</th>
    <th width="118">Time</th>
    </tr></thead>
  <?php 
$cnt=1;
foreach ($messages as $message)
{
	$sender = $this->flexi_auth->get_user_by_id($message->m_senderid)->result();
  if ($message->m_isread==0)
  {
	$bs = "<strong>";
	$bst= "</strong>";  
  }
  if ($message->m_isread==1)
  {
	$bs = "";
	$bst= "";  
  }
?>
  <tr>
    <td><?php echo $bs.$cnt.$bst;?></td>
    <td><?php echo  $bs.$sender[0]->upro_first_name.$bst; ?></td>
    <td><?php echo anchor('admins/read_message/'.$message->m_id.'/#tab3',$message->m_header);?></td>
    <td><?php echo $bs.date('d/m/Y',strtotime($message->m_date)).$bst; ?></td>
    <td><?php echo $bs.date('g:i A',strtotime($message->m_time)).$bst ; ?></td>
    </tr>
  <?php
  $cnt++;
}
  ?>
</table>
</div></div><?php } ?>

    </div>
    <div id="tab2">
<?php // Change the css classes to suit your needs    

$attributes = array('class' => '', 'id' => '');
echo form_open('admins/send_message', $attributes); ?>
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="table1">
      <tr>
        <td align="right" valign="top"><strong>To:</strong></td>
        <td width="" align="left" valign="top">
        
       
        <select name="rec[]" value="<?php echo set_value('rec'); ?>" class="multiselect" multiple="multiple">
          
          <? 
					  if ($users<>NULL)
					  {
                      foreach ($users as $user)
                      {
                      ?>
          <option value="<?=$user->uacc_id?>" <? echo set_select('rec', $user->uacc_id); ?>
                      >
            <?=$user->upro_first_name." ".$user->upro_last_name?>
            </option>
          <? }} ?>
        </select>
        
          <?php echo form_error('rec'); ?></td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td width="80" align="right" valign="top"><strong>Subject:</strong></td>
        <td align="left" valign="top"><input name="subject" type="text" id="textfield" size="63" value="<?php echo set_value('subject'); ?>" />
          <?php echo form_error('subject'); ?></td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="top"><strong>Message</strong>:</td>
        <td valign="bottom"><textarea id="elm1" name="message" rows="15" cols="45" class="textarea"><?php echo set_value('message');?></textarea>
          <?php echo form_error('message'); ?></td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="top"><label>
          <input type="checkbox" name="sendemail" id="sendemail" />
        </label><?=nbs(1)?></td>
        <td valign="bottom"> <?=nbs(1)?>Send to email(s)</td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="bottom"><input type="submit" name="submit" value="Send" class="loginsubmitbutton" />
          <input type="reset" name="reset" value="Reset" class="loginsubmitbutton" /></td>
      </tr>
    </table>
<?php echo form_close(); ?>    </div>
    <div id="tab3">
    <?php if($messagedet==NULL){echo '<p class=red>Please select message to read from Inbox or Sent tabs!</p>'; } else{?>
<table width="100%" border="0" align="center">

  <tr>
    <td width="171" valign="top"><strong>
      <?php 
	$sender = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
	$this->name= $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
	echo $this->name?>
    </strong></td>
    <td width="360" align="center" valign="top"><strong><?php echo $messagedet->m_header; ?></strong></td>
    <td width="153" align="right" valign="top"><strong><?php echo $messagedet->m_date." at ".$messagedet->m_time; ?></strong></td>
    </tr>
  <tr>
    <td colspan="3" valign="top"><hr noshade="noshade" /><?=html_entity_decode($messagedet->m_message)?></td>
    </tr>
</table>
<?php }?>
</div>
    <div id="tab4">
<?php 
if ($sentmessages==NULL)
{
	echo "No messages added yet!";
}
if ($sentmessages<>NULL)
{
?>
<div class="sorttable">
<div id="pagewrap">
 <div id="search">
        <label for="filter">Filter</label> <input type="text" name="filter" value="" id="filter" />
      </div>

<table width="100%" border="0" align="center" id="resultTable">
<thead>
  <tr>
    <th width="52">No</th>
    <th width="206">To</th>
    <th width="431">Message </th>
    <th width="122">Date</th>
    <th width="118">Time</th>
    </tr></thead>
  <?php 
$cnt=1;
foreach ($sentmessages as $message)
{
	$sender = $this->flexi_auth->get_user_by_id($message->m_senderid)->result();
  if ($message->m_isread==0)
  {
	$bs = "<strong>";
	$bst= "</strong>";  
  }
  if ($message->m_isread==1)
  {
	$bs = "";
	$bst= "";  
  }
?>
  <tr>
    <td><?php echo $bs.$cnt.$bst;?></td>
    <td><?php echo  $bs.$sender[0]->upro_first_name. " ".$sender[0]->upro_last_name.$bst; ?></td>
    <td><?php echo $bs.anchor('admins/read_message/'.$message->m_id.'/#tab3',$message->m_header).$bst; ?></td>
    <td><?php echo $bs.$message->m_date.$bst; ?></td>
    <td><?php echo $bs.$message->m_time.$bst ; ?></td>
    </tr>
  <?php
  $cnt++;
}
  ?>
</table>
</div></div><?php } ?>

</div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
