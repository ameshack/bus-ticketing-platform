<?php
$data = array(
'title'=>'Corporate Tickets',
'keywords'=>' ',
'description'=>' ',
'selected'=>'corporate tickets',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>

<?php
		 $townz = $this->uri->segment(3);
		 $ft = explode(":",$townz);
		 $scheduleidz = $ft[0];
		 $ftownz = $ft[1];
		 $ttownz = $ft[2];
		 $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                        $tid = $this->user[0]->t_station;
                        $tcur = $this->bus_model->getonerowfromonetbl('towns_lkp','TownId', $tid);
                        $cur = $tcur->Currency;
						
						$useremail = $this->user[0]->uacc_email;
						$cust = $this->bus_model->getonerowfromonetbl('cust_users','usermail', $useremail);
						$custamount = $this->bus_model->get_customer_total_deposit($cust->cust_id);
						
						$remaininamount = abs($custamount->camount);
						
                
                        $tsch = $this->bus_model->getonerowfromonetbl('bus_schedule','ScheduleId', $scheduleidz);
				$twns = $this->bus_model->getscheduletimes($tsch->RouteLkp,$ftownz);
				$deptime = date('g:i A',strtotime($twns->ArrivalDeparture));
				$reptime = date('g:i A',strtotime($twns->ArrivalDeparture. ' - 30 minute'));
				
		 ?>

<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Tickets</p>

<div id="rightcontentwide">
 
    <ul id="tabs">
      <li><a href="#create" name="#tab1">Add Ticket</a></li>
      <li><a href="#read" name="#tab2">Booked</a></li>
    </ul>
     <div id="content">
        <div id="tab1">
       
<?php $attributes = array('name' => 'ticket', 'id' => 'ticket');?>
            <?php echo form_open('admins/create_ticket_corp',$attributes);?>
            
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="197" class="labels">Route Code:</td>
          <td width="225"><span class="red"><?php echo form_error('routecode'); ?></span>
            <?php echo form_error('routecode'); ?>
          <input type="text" name="routecode" id="routecode" value="<?php echo $schedules->RouteCode;?>" readonly="readonly"></td>
          <td width="168">Travel Date:</td>
          <td colspan="3"><span class="red"><?php echo form_error('traveldate'); ?></span><input type="text" name="traveldate1"  class="short_input" value="<?php echo $schedules->DepartureDateTime;?> " readonly="readonly"></td>
        </tr>
        
               
        <tr>
          <td class="labels">Bus Type:</td>
          <td><span class="red"><?php echo form_error('bustype'); ?></span>
          <input name="bustype" type="hidden" value="<?php echo $schedules->BusTypeId;?>" />
           
          <?php 
		  $btz = $this->bus_model->getonerowfromonetbl('bustype_lkp','BusTypeId', $schedules->BusTypeId);
                        echo $btz->BusType;
		  ?>
          </td>
          <td>Bus Number:</td>
          <td colspan="3"><span class="red"><?php echo form_error('busno'); ?></span><input type="text" name="busno" id="busno" readonly="readonly" value="<?php  echo $schedules->Busno; ?>"></td>
        </tr> 
                <tr>
                  <td class="labels">Report Time:</td>
          <td><span class="red"><?php echo form_error('reporttime'); ?></span>
          <input name="reporttime" type="hidden" value="<?php echo $reptime; ?>" />
           <?php echo $reptime; ?></td>
                              <td>Departure Time:</td>
                  <td colspan="3"> <span class="red"><?php echo form_error('departuretime'); ?></span><?php echo $deptime; ?></td>
          </tr>
                <tr>
                  <td class="labels">Route Name:</td>
                  <td><span class="red"><?php echo form_error('routename'); ?></span><input type="text" name="routename" id="routename" value="<?php echo $schedules->RouteName;?>" readonly="readonly"><input type="hidden" name="scheduleid" id="scheduleid" value="<?php echo $scheduleidz;?>"></td>
                  <td></td>
                  <td></td>
                </tr>    </table>
         
       <div id="advanced" style="display:block;"> 
       <table width="100%" cellpadding="0" cellspacing="0" class="table1">
       <tr>
          <td>&nbsp;</td>
          <td>Number of tickets</td>
          <td><input type="text" name="quantity" id="quantity" readonly="readonly" class="inputfields" onfocus="cloneRegioncorp(this.value)" value="1" /><input type="hidden" name="seatarray1" id="seatarray1"  value="<?php print_r($this->input->post('seatarray'));?>"></td>
          <td >&nbsp;</td>
        </tr>
        </table>
      <div id="quaneffect">
      
         </div></div>
         <table width="100%" cellpadding="0" cellspacing="0" class="table1">
         
        <tr>
          <td>From:</td>
          <td><span class="red"><?php echo form_error('from'); ?></span>
            <select name="from" id="from" class="smallselect" onfocus="this.defaultIndex=this.selectedIndex;" onchange="this.selectedIndex=this.defaultIndex;">
              <option disabled="disabled" selected="selected">---select town from---</option>
              <?php 
			  
			  foreach($towns as $town){?>
              <option <?php if ($town->TownId==$ftownz) { echo "selected";} ?> value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select></td>
          <td>Pick-Up-Point:</td>
          <td colspan="3"><span class="red"><?php echo form_error('pickuppoint'); ?></span>
          <div id="termini">
            <select name="pickuppoint" id="pickuppoint" class="smallselect">
              <?php 
			  $terminiz = $this->bus_model->getresultfromonetbl('bus_termini','TownLkp',$ftownz);
			  if ($terminiz<>NULL)
			  {
			  foreach($terminiz as $t){?>
              <option value="<?php echo $t->TerminusId;?>"><?php echo $t->TerminusName;?></option>
              <?php }}
			  else {
				 echo "<option value=\"0\">No termini for this town</option>"; 
			  }?>
            </select></div></td>
        </tr>
        <tr>
          <td>Destination:</td>
          <td><span class="red"><?php echo form_error('to'); ?></span>
            <select name="to" id="to" class="smallselect" onfocus="this.defaultIndex=this.selectedIndex;" onchange="this.selectedIndex=this.defaultIndex;">
              <option disabled="disabled" selected="selected">---select town to---</option>
              <?php foreach($towns as $town){?>
              <option <? if ($town->TownId==$ttownz) { echo "selected";} ?> value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select></td>
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td>Account Details:</td>
          <td valign="top"><span class="red"><?php echo form_error('bookingmode'); ?><?php echo form_error('voucher'); ?><?php echo form_error('mpesaref'); ?></span>
            <label>
            <?php 
			
						$custdet = $this->bus_model->getonerowfromonetbl('customer','Cust_id', $cust->cust_id);
			if ($custdet->cust_type==1)
			{
             echo  "Currently you have <strong><u>Ksh ".number_format($remaininamount)."</u></strong> balance in your account<input name=\"ctype\" type=\"hidden\" value=\"1\" /><input name=\"camount\" type=\"hidden\" value=\"".$remaininamount."\" />";
			}
			else {
				echo "Please proceed to get ticket!<input name=\"ctype\" type=\"hidden\" value=\"2\" /><input name=\"camount\" type=\"hidden\" value=\"99999999999999999999\" />";
			}
			 ?>
              
              
              </label>
            <br />         
           <p class="debitcredit" style="display:none">Debit/Credit card #                             
  <input type="text"  size="25" name="debitcreditcardnumber"/><br />
Debit/Credit Ref No   
  <input type="text"  size="25" name="debitcreditcardref"/></p>
             <p class="voucher" style="display:none">Voucher Number                              
  <input type="text"  size="25" name="vouchernumber"/></p>
             <p class="textBox" style="display:none">Mpesa Ref                              
  <input type="text"  size="25" name="mpesaref"/></p> </td>
          <td valign="top">&nbsp;</td>
          <td colspan="3" valign="top">
            <input name="schedfrto" type="hidden" value="<?php echo $this->uri->segment(3);?>" />
            <input name="currency" type="hidden" value="<?php echo $cur;?>" />
            <input name="usercur" type="hidden" value="<?php echo $cur;?>" />
            <input name="custid" type="hidden" value="<?php echo $cust->cust_id;?>" />
            <input name="usermail" type="hidden" value="<?php echo $useremail;?>" />
            </td>
        </tr>
        
        <tr>
          <td>Amount </td>
          <td colspan="5"><input name="amount" type="text" readonly="readonly" id="amount"  value="<?php echo $this->input->post('amount');?>" /><input name="calcbutton" type="button" value="Click to calculate total" onClick="calc();"  style="height:40px; font-size:16px; color:#900; font-weight:bold;"  >
          <br />
          </td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Issue Ticket"  onKeyPress="return keyPressed(event)" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"></td>
          <td></td>
          <td colspan="3">&nbsp;</td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab2">
            	 <?php if(!empty($tickets)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="5%" height="23">Date</th>
			  <th width="6%"> Time</th>
			  <th width="16%">Traveller Name</th>
			  <th width="6%">Id No</th>
			  <th width="6%">Seat</th>
	          <th width="9%">Ticket No</th>
	          <th width="9%">Status</th>
	          <th width="6%">KSHS</th>
	          <th width="6%">USHS</th>
	          <th width="6%">TSHS</th>
	          <th width="11%">Destination</th>
		      <th width="14%">Options</th>
		  </tr>
		</thead>
        <?php foreach ($tickets as $item): ?>
			<tr>
			  <td align="left"><?php echo date('m/d/Y',strtotime($item->DepartureDateTime)); ?></td>
			  <td align="left"><?php echo date('H:i',strtotime($item->DepartureDateTime));?></td>
			  <td align="left"><?php echo $item->clientname; ?></td>
              <td align="left"><?php echo $item->t_idno; ?></td>
              <td align="left"><?php echo $item->seatno;?></td>
			  <td align="left"><?php echo $item->newticketno; ?></td>
			  <td align="left"><?php echo $item->ticketstatus;?></td>
			  <td align="left"><?php echo number_format($item->amount,2);?></td>
			  <td align="left"><?php echo number_format($item->amountug,2);?></td>
			  <td align="left"><?php echo number_format($item->amounttz);?></td>
			  <td align="left"><?php echo $item->TownName;?></td>
			  <td align="left"><?php if($item->ticketstatus=='Reserved'){
			  $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/cancel_ticket/'.$item->TicketId, 'CANCEL', $atts); } elseif($item->ticketstatus=='Inactive') {
							echo "<font color=\"#FF0000\">Cancelled |  </font>"; $atts = array(
              'width'      => '350',
              'height'     => '350',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/ticket_cancel_reason/'.$item->TicketId, 'Reason', $atts);
						}?>&nbsp;
		      <?php if($item->ticketstatus=='Active'){ echo anchor('admins/print_ticket/'.$item->TicketId, '| Reprint');} elseif($item->ticketstatus=='Reserved') { echo anchor('admins/confirm_booking/'.$item->TicketId, '| Confirm Booking');}?></td>
		</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Please select schedule from \'add ticket\' tab!</p>';}; ?>


    </div>
     </div>
     <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
