<?php
$data = array(
'title'=>'Manage Petty Cash',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage petty cash',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
	    $('#issueddate').datepicker({ 
		dateFormat: 'yy-mm-dd',  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
});  
</script> 
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Petty Cash</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
  
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Issued Petty Cash</a></li>
      <li><a href="#create" name="#tab2">Petty Cash Issue</a></li>
    </ul>
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($pettycash)){ //print_r($pettycash);?>
         
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="112">Receipt #</th>
			  <th width="134">Date</th>
			  <th width="199">Cashier</th>
			  <th width="127">Amount</th>
			  <th width="127"> Option</th>
          </tr>
		</thead>
        <?php foreach ($pettycash as $item): ?>
			<tr>
				<td align="left"><?php echo $item->pettycashno;?></td>
			  <td align="left"><?php echo date('m/d/Y',strtotime($item->CashDate)); ?></td>
              <td align="left"><?php 
			  $this->user = $this->flexi_auth->get_user_by_id($item->CashierId)->result();
			  echo $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
			   ?></td>
              <td align="left"><?php 
			  if($item->Cur==1){echo  "KES ".$item->KeAmount;} elseif($item->Cur==2){echo  "UGX ".$item->UgAmount;}elseif($item->Cur==3){ echo  "TZS ".$item->TzAmount;} elseif($item->Cur==4){ echo  "USD ".$item->UsdAmount;} elseif($item->Cur==5){ echo  "Eur ".$item->EurAmount;} else{ echo '--';}
			   ?></td>
              <td align="left"><?php echo anchor('admins/print_pettycash/'.$item->PettyCashId, 'Print', array('title'=>'print pettycash','class'=>'confirm'));?></td>
		    </tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no petty cash issued yet!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_petty_cash/#tab2',array('target'=>'_blank'));?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
                  <td class="labels">Town:</td>
                  <td><span class="red"><?php echo form_error('town'); ?></span>
                  <select name="town" id="town" onchange="displayterminiclerkspettycash(this.value)">
              <option disabled="disabled" selected="selected">---select town--</option>
              <?php foreach($towns as $t){?>
              <option value="<?php echo $t->TownId;?>"><?php echo $t->TownName;?></option>
              <?php }?>
          </select></td>
                </tr>
                <tr>
          <td width="283" class="labels">Terminus:<br />Clerk:<br />Manager:<br /></td>
          <td width="847">
          <div id="terminiclerk">select town above
          </div></td>
        </tr>
                <tr>
                  <td class="labels">Date:</td>
                  <td><?php echo form_error('issueddate'); ?></span>
                  <input type="text" name="issueddate"  id="issueddate" class="long_input" readonly="readonly" value="<?php if ($this->input->post('issueddate')) {echo $this->input->post('issueddate');} else { echo date('Y-m-d');}?>"/></td>
        </tr>
<!--                <tr>
          <td class="labels">Cashier:</td>
          <td><span class="red"><?php //echo form_error('cashier'); ?></span>
            <select name="cashier" id="cashier">
              <option disabled="disabled" selected="selected">---select Payee--</option>
              <?php //foreach($users as $user){?>
              <option value="<?php //echo $user->AdminId;?>"><?php //echo $user->AdminName;?></option>
              <?php //}?>
          </select></td>
        </tr>-->
                <tr>
          <td class="labels">Account Code:</td>
          <td><span class="red"><?php echo form_error('accountcode'); ?></span>
          <input type="hidden" name="cashier" value="<?php echo $this->flexi_auth->get_user_id();?>" />
            <select name="accountcode" id="accountcode">
              <option disabled="disabled" selected="selected">---select account code---</option>
              <?php foreach($codes as $code){?>
              <option value="<?php echo $code->AccountCodeId;?>"><?php echo $code->AccountCode;?></option>
              <?php }?>
            </select></td>
        </tr>
                <tr>
          <td width="283" class="labels">Issued to:</td>
          <td width="847"><span class="red"><?php echo form_error('issuedto'); ?></span>
           <input name="issuedto" type="text" id="long_input" /></td>
        </tr>
                        <tr>
                          <td class="labels">Particulars:</td>
                          <td><span class="red"><?php echo form_error('particulars'); ?></span>
          <textarea name="particulars" class='animated' id="long_input"><?php echo $this->input->post('particulars');?></textarea></td>
                        </tr>
                        <tr>
                          <td class="labels">Currency:</td>
                          <td><span class="red"><?php echo form_error('currency'); ?></span>
              <select name="currency" id="currency" class="smallselect">
              <option value="1" selected="selected">Kshs</option>
              <option value="2">Ushs</option>
              <option value="3">Tshs</option> 
              <option value="4">USD</option> 
              <option value="5">EUR</option>              
            </select></td>
                        </tr>
        <tr>
          <td class="labels"> Amount:</td>
          <td><span class="red"><?php echo form_error('keamount'); ?></span>
          <input type='text' name='keamount' id='numfield' value='<?php echo $this->input->post('keamount');?>'  onkeypress='validate(event)' /></td>
        </tr>
        
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add New" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
