<?php
$data = array(
'title'=>'Manage Bus Termini',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus termini',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Termini</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Termini</a></li>
      <li><a href="#create" name="#tab2">Add Terminus</a></li>
      <li><a href="#edit" name="#tab3">Edit Terminus</a></li>
      <li><a href="#delete" name="#tab4">Delete Terminus</a></li>
    </ul>
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($termini)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Terminus</th>
			  <th width="169">Region</th>
			  <th width="125">Town</th>
			  <th width="147">Phone</th>
	          <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($termini as $item): ?>
			<tr>
				<td align="left"><?php echo $item->TerminusName;?></td>
			  <td align="left"><?php echo $item->RegionName; ?></td>
              <td align="left"><?php echo $item->TownName; ?></td>
              <td align="left"><?php echo $item->TerminusPhone; ?></td>
			  <td align="left"><?php echo anchor('admins/edit_bus_terminus/'.$item->TerminusId.'/#tab3','Edit');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus termini currently!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus_terminus');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" class="labels">Region:</td>
          <td width="519"><span class="red"><?php echo form_error('region'); ?></span>
            <select name="region" id="region">
              <option disabled="disabled" selected="selected">---select region--</option>
              <?php foreach($regions as $region){?>
              <option value="<?php echo $region->RegionId;?>"><?php echo $region->RegionName;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td width="169" height="33" class="labels">Town:</td>
          <td width="519"><span class="red"><?php echo form_error('town'); ?></span>
            <select name="town" id="town">
              <option disabled="disabled" selected="selected">---select town--</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td class="labels">Terminus Name:</td>
          <td><span class="red"><?php echo form_error('terminusname'); ?></span>
          <input type="text" name="terminusname" id="long_input" value="<?php echo $this->input->post('terminusname');?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Terminus Ext Phone:</td>
          <td><span class="red"><?php echo form_error('terminusphone'); ?></span>
            <input type="text" name="terminusphone" id="long_input" value="<?php echo $this->input->post('terminusphone');?>"></td>
        </tr>
        <tr>
          <td height="24" class="labels">Terminus Cell Phone:</td>
          <td><span class="red"><?php echo form_error('terminuscellphone'); ?></span>
            <input type="text" name="terminuscellphone" id="long_input" value="<?php echo $this->input->post('terminusphone');?>"></td>
        </tr>
        <tr>
          <td class="labels">Terminus Email:</td>
          <td><span class="red"><?php echo form_error('email'); ?></span>
            <input type="text" name="email" id="long_input" value="<?php echo $this->input->post('email');?>"></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Terminus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
	<?php if(!empty($terminusdetails)){?>
	<?php echo form_open('admins/update_bus_terminus');?>
    <?php foreach($terminusdetails as $row):?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Region:</td>
          <td width="519"><span class="red"><?php echo form_error('region'); ?></span>
            <select name="region" id="region">
              <option disabled="disabled">---select region---</option>
              <?php foreach($regions as $region):?>
<option value="<?php echo $region->RegionId;?>" <?php if(!(strcmp($region->RegionId, $row->RegionLkp))) :echo "SELECTED";endif;?>><?php echo $region->RegionName;?></option>
              <?php endforeach;?>
            </select></td>
          </tr>
                  <tr>
          <td width="177" class="labels">Town:</td>
          <td width="519"><span class="red"><?php echo form_error('town'); ?></span>
            <select name="town" id="town">
              <option disabled="disabled">---select town---</option>
              <?php foreach($towns as $town):?>
<option value="<?php echo $town->TownId;?>" <?php if(!(strcmp($town->TownId, $row->TownLkp))) :echo "SELECTED";endif;?>><?php echo $town->TownName;?></option>
              <?php endforeach;?>
            </select></td>
          </tr>
          <tr>
          <td class="labels">Terminus Name:</td>
          <td><span class="red"><?php echo form_error('terminusname'); ?></span>
          <input type="text" name="terminusname" id="long_input" value="<?php echo $row->TerminusName;?>"></td>
        </tr>
          <tr>
          <td class="labels">Terminus Ext Phone:</td>
          <td><span class="red"><?php echo form_error('terminusphone'); ?></span>
          <input type="text" name="terminusphone" id="long_input" value="<?php echo $row->TerminusPhone;?>"></td>
        </tr>
          <tr>
          <td class="labels">Terminus Cell Phone:</td>
          <td><span class="red"><?php echo form_error('terminuscellphone'); ?></span>
          <input type="text" name="terminuscellphone" id="long_input" value="<?php echo $row->TerminusCellPhone;?>"></td>
        </tr>
          <tr>
          <td class="labels">Terminus Email:</td>
          <td><span class="red"><?php echo form_error('email'); ?></span>
          <input type="text" name="email" id="long_input" value="<?php echo $row->TerminusEmail;?>"></td>
        </tr>
                
        <tr>
          <td><input type="hidden" name="terminusid" value="<?php echo $row->TerminusId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Bus Terminus" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
      <?php }else{ echo '<p class="red">Please select record from \'View Termini\' tab!</p>';}; ?>
</div>
    <div id="tab4">
	<?php if(!empty($termini)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="128">Terminus</th>
			  <th width="169">Region</th>
			  <th width="125">Town</th>
			  <th width="147">Phone</th>
	          <th width="107">Options</th>
		  </tr>
		</thead>
        <?php foreach ($termini as $item): ?>
			<tr>
				<td align="left"><?php echo $item->TerminusName;?></td>
			  <td align="left"><?php echo $item->RegionName; ?></td>
              <td align="left"><?php echo $item->TownName; ?></td>
              <td align="left"><?php echo $item->TerminusPhone; ?></td>
			  <td align="left"><?php echo anchor('admins/delete_bus_terminus/'.$item->TerminusId.'/#delete','X',array('class' => 'delete','title'=>''.$item->TerminusName.''));?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus termini currently!</p>';}; ?>
</div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
