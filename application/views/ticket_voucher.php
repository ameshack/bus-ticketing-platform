<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ticket Voucher</title>
</head>

<body onLoad="window.print(); window.opener.location.reload();">

<table border="1" cellpadding="0" cellspacing="0" width="600">
  <tr>
    <td align="center"><h4>MODERN COAST EXPRESS LTD<br />
MODERN COAST EXPRESS TRAVEL VOUCHER<br />
BRANCH: <?php echo $row->TownFrom;?>
</h4></td>
  </tr>
</table>

<table border="1" cellpadding="0" cellspacing="0" width="600">
  <tr>
    <td style="padding:5px;"><strong>Voucher No. <?php echo $voucherno; ?>  <div style="float:right;">Ticket No: <?php echo $row->newticketno;?></div></strong></td>
  </tr>
  <tr>
    <td style="padding:5px;">Traveller's Name: <strong><?php echo $row->clientname;?></strong>  <div style="float:right;">ID No: <strong><?php echo $row->t_idno;?></strong></div></td>
  </tr>
  <tr>
    <td style="padding:5px;">Ticket Amount: <strong><?php 
	  if($row->t_currency==1):
	  echo "Ksh ".number_format($row->amount,2);
	  endif;
	  if($row->t_currency==2):
	  echo "Ush ".number_format($row->amountug,2);
	  endif;
	  if($row->t_currency==3):
	  echo "Tsh ".number_format($row->amounttz,2);
	  endif;
	  if($row->t_currency==4):
	  echo "USD ".number_format($row->amountusd,2);
	  endif;
	  ?></strong> 
      <div style="float:right;">Open Ticket Amount Charged: <strong><?php echo $openchargeamnt;?></strong></div>
      </td>
  </tr>
  <tr>
    <td style="padding:5px;">Date of Issue: <?php echo date('d/m/Y');?>
    <div style="float:right;">Expiry Date: <strong><?php echo date('d/m/Y',strtotime(' + 30 day'));?></strong></div></td>
  </tr>
  <tr>
    <td style="padding:5px;">Description: <?php echo $reason; ?>
    </td>
  </tr>
  <tr>
    <td style="padding:5px;">Issued By:  <?php $this->user=$this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
		echo $this->user[0]->upro_first_name.' '.$this->user[0]->upro_last_name;
		?></td>
  </tr>
  <tr>
    <td style="padding:5px;"><strong>Valid for 30 days from date of issue. <br />This is NOT a valid travel document</strong>
    <div style="float:right;">Printed on: <strong><?php echo date('Y-m-d H:i:s A')?></strong></div>
    </td>
  </tr>
</table>


</body>
</html>