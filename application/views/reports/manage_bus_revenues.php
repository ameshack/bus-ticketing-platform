<?php
$data = array(
'title'=>'Manage Bus Revenues',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus revenues',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Ticket Sales</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Bus Revenues</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/crud_bus_revenues_refine/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23" colspan="2">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
        <td width="50%" rowspan="2" valign="top">
        Buses(Use Control key to select multiple) <br /> <select name="bus[]" id="bus" style="height:100px" multiple="multiple">
              <?php foreach($buses as $t){?>
              <option value="<?php echo $t->BusId;?>"><?php echo $t->RegistrationNumber;?></option>
              <?php }?>
          </select></td>
        <td width="50%" valign="top">Date From<br />
          <input name="datef" type="text" class="long_input" id="insdate" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  /></td>
        </tr>
        <tr>
          <td valign="top" style="vertical-align:top;">Date To<br />
          <input name="datet" type="text" class="long_input" id="insdateedit" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d');} else { echo $this->input->post('datet');} ?>" /></td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:center;"><input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        <? if ($rep<>NULL)
		{
			?>
            
 <div id="printarea">
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="7"><?php echo $datettitle;?></th>
</tr>
			<tr align="left">
			  <th>No</th>
			  <th height="23">Bus</th>
			  <th>Total Ticket Sales</th>
          </tr>
		</thead>
        
<? if ($busesz<>NULL)
{
	$cnt = 1;
	//var_dump($busesz);
	$nbs = explode(",",$busids);
	for ($i=0;$i<count($nbs);$i++)
	{
	//foreach ($nbs as $nb)
	//{
		
	//foreach ($busesz as $c)
	//{
		$c = $this->bus_model->getonerowfromonetbl('buses','BusId',$nbs[$i]);
	?>
			<tr>
			  <td align="left" style="vertical-align:top;"><? echo $cnt; ?></td>
			  <td align="left" style="vertical-align:top;"><?php echo $c->RegistrationNumber; ?></td>
			  <td align="left" style="vertical-align:top;"><?php  //echo $c->BusId;
			  $tts = $this->bus_model->get_total_bus_ticket_sales($nbs[$i],$datef,$datet);
			  //var_dump($tts);
                $ttshtml = '';
                $ttspcke = 0;
                 $ttspcug = 0;
                 $ttspctz = 0;
                 $ttspcusd = 0;
                 $ttspceuro = 0;
                if ($tts==NULL):
                    $ttshtml = 0;
                else:
                    if ($tts->amountke > 0):
                        $ttshtml.="Ksh ".round($tts->amountke,2)."<br />";
                   // $ttspcke = $tts->amountke;
                    endif;
                    if ($tts->amountug > 0):
                        $ttshtml.="Ush ".round($tts->amountug,2)."<br />";
                    //$ttspcug = $tts->amountug;
                    endif;
                    if ($tts->amounttz > 0):
                        $ttshtml.="Tsh ".round($tts->amounttz,2)."<br />";
                     //$ttspctz = $tts->amounttz;
                    endif;
                    if ($tts->amountusd > 0):
                        $ttshtml.="USD ".round($tts->amountusd,2)."<br />";
                     //$ttspcusd = $tts->amountusd;
                    endif;
                    if ($tts->amounteuro > 0):
                        $ttshtml.="EUR ".round($tts->amounteuro,2)."<br />";
                     //$ttspceuro = $tts->amounteuro;
                    endif;
                    
                endif;
                echo $ttshtml;
			  //echo $tts;?></td>
        </tr>
		<? $cnt++; }} ?>
	 </table>
     </div>
    <? } ?>
    </div>
  </div>
  
<table width="596" border="0" align="center" cellspacing="0" style="text-align:center;">
  <tr>
    <td align="center" valign="top" style="text-align:center;">
    <form>
      <input type="button" value="Print" onClick="javascript:void(printSpecial())" />
    </form>    </td>
    </tr>
</table>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
