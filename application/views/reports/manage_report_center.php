<?php
$data = array(
'title'=>'Reports Center',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage reports',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
     <style>
	 #table1 td {
		vertical-align:top; 
	 }
	 </style>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Reports</p>

<div id="rightcontentwide">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Report Center</a></li>
      
       
    </ul>  
  <div id="content">
   
    <div id="tab1">
    	 <h2>Below are reports for various modules.</h2>
         <table width="100%" border="0" align="left" id="table1">
  <tr id="row_content">
    <td valign="top">
    <h3>Master Report</h3>
    <p><?php echo anchor('admins/crud_reports/','Master Report');?><br />
Generates various ticket reports, numerous filters</p></td>
    <td valign="top">
    <?php
	/*
    echo "<h3>Bus Schedules</h3><p>";
    echo anchor('admins/crud_reports/','Timely Report')."<br />";
    echo anchor('admins/crud_reports/','Closed Schedules')."<br />";
    echo anchor('admins/crud_reports/','Lost Potential Revenue')."<br />";
    echo "Generates bus scheduls related reports</p>";*/
?></td>
    <td valign="top">
    <?php
	/*
    echo "<h3>Arrivals</h3><p>";
	echo anchor('admins/crud_reports/','Timely Arrivals Report')."<br />";
    echo anchor('admins/crud_reports/','Unconfirmed Arrivals')."<br />";
    echo anchor('admins/crud_reports/','Changeovers')."<br />";
    echo "Generates bus arrivals related reports</p>"; */?></td>
  </tr>
  <tr  id="row_content">
    <td valign="top"><h3>Bus</h3>
    <p><?php echo anchor('admins/crud_bus_revenues/','Bus Revenues');?><br />
    <?php echo anchor('admins/crud_bus_clocked_hours/','Clocked Hours');?><br />
    <?php echo anchor('admins/crud_bus/','Bus Current Locations');?><br />
Generates bus related reports</p></td>
    <td valign="top"><h3>Bus Staff</h3>
    <?php echo anchor('admins/crud_staff/','Document Expiries');?><br />
Generates bus staff related reports</p></td>
    <td valign="top"><h3>Vouchers</h3>
    <?php echo anchor('admins/crud_voucher_report/','Vouchers Report');?><br />
Generates voucher related reports</p></td>
  </tr>
  <tr  id="row_content">
    <td valign="top"><h3>Pettycash/Expense</h3>
    <p><?php echo anchor('admins/crud_daily_expenses/','Daily Expense Report');?><br />
Generates Pettycash/Expense related reports</p></td>
    <td valign="top"><h3>Banking</h3>
    <p><?php echo anchor('admins/crud_banking_report/','Banking Report');?><br />
Generates banking related reports</p></td>
    <td valign="top"><h3>Loyalty</h3>
    <p><?php echo anchor('admins/crud_loyalty/','Summary Report');?><br />
    <?php echo anchor('admins/crud_loyalty/#tab2','Redeemed Points Report');?><br />
Generates loyalty related reports</p></td>
  </tr>
  <tr  id="row_content">
    <td valign="top">
    <h3>Corporate Reports</h3>
    <p><?php //echo anchor('admins/crud_corp_sales/','Summary Sales');?><br />
    <?php //echo anchor('admins/crud_reports/','Monthly Sales');?><br />
    <?php //echo anchor('admins/crud_reports/','Corporate Deposits');?><br />
Generates corporate customer related reports</p></td>
    <td valign="top"><h3>Agent Reports</h3>
    <p><?php //echo anchor('admins/crud_reports/','Summary Sales');?><br />
    <?php //echo anchor('admins/crud_reports/','Timely Sales');?><br />
Generates agent related reports</p></td>
    <td valign="top">&nbsp;</td>
  </tr>
</table>

         
    </div>
  </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
