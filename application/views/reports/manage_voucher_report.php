<?php
$data = array(
'title'=>'Manage Voucher Report',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage voucher	report',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Voucher Report</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Voucher Report</a></li>
    </ul>  
    <div id="content">
<div id="tab1">

      <?php echo form_open('admins/crud_voucher_report_refine/#tab1'); ?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
          <td valign="top" style="text-align:center;">Date From 
          <input name="datef" type="text" class="long_input" id="insdate" readonly="readonly" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  />
          Date To
          <input name="datet" type="text" class="long_input" id="insdateedit" readonly="readonly" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d');} else { echo $this->input->post('datet');} ?>"  />
                    <input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /><br /></td>
        </table>
        <?php echo form_close(); ?>
        <? if ($rep<>NULL)
		{
			?>
            
 <div id="printarea">
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
<tr>
<th colspan="9"><?php echo $datettitle;?></th>
</tr>
			<tr align="left">
			  <th width="9%">No</th>
			  <th width="30%" height="23">Town</th>
			  <th width="32%" style="text-align:right;">Total Voucher  Amount </th>
          </tr>
		</thead>
        
<? if ($vs<>NULL)
{
	$cnt = 1;
	//var_dump($busesz);
	foreach ($vs as $c)
	{
	?>
			<tr>
			  <td align="left" style="vertical-align:top;"><? echo $cnt; ?></td>
			  <td align="left" style="vertical-align:top;"><?php echo $c->TownName; ?></td>
			  <td style="text-align:right;vertical-align:top;"><?php  
                
                    if ($c->amountke<>0)
					{
						echo "Ksh ".number_format($c->amountke)."<br />";
					}
                    if ($c->amountug<>0)
					{
						echo "Ush ".number_format($c->amountug)."<br />";
					}
                    if ($c->amounttz<>0)
					{
						echo "Tsh ".number_format($c->amounttz)."<br />";
					}
                    if ($c->amountusd<>0)
					{
						echo "USD ".number_format($c->amountusd)."<br />";
					}
                    if ($c->amounteuro<>0)
					{
						echo "EUR ".number_format($c->amounteuro)."<br />";
					}
			  
			  ?></td>
        </tr>
		<? $cnt++; }?>
        <tr>
        <td colspan="4">
        
    <?php
	
	if ($graphdata==1)
	{
		$graphfile = "FCF_Column3D.swf";

	
	//Create the chart - Column 3D Chart with data from Data/Data.xml
	echo renderChartHTML(base_url()."Charts/FCF_Column3D.swf", base_url()."testFile.xml", "", "myFirst", 840, 400);
	}
	else {
		echo '<p class="red">Use date filters above!</p>';
	}
	
?>
        </td>
        </tr>
		
		<? } ?>
	 </table>
     </div>
    <? } ?>
    </div>
  </div>
  
<table width="596" border="0" align="center" cellspacing="0" style="text-align:center;">
  <tr>
    <td align="center" valign="top" style="text-align:center;">
    <form>
      <input type="button" value="Print" onClick="javascript:void(printSpecial())" />
    </form>    </td>
    </tr>
</table>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
