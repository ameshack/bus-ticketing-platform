<?php
$data = array(
'title'=>'Manage Reports',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage reports',
'img'=>'',
'load_side_menu'=>false

);
////download to excel
header('Content-Type: application/force-download');
header('Content-disposition: attachment; filename=tickets_report_'.date('YmdHis').'.xls');
header("Pragma: ");
header("Cache-Control: ");
 ?>
 <!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Report</title>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/add_seats.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-templ.js"></script>
<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('');?>styles/styles.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('');?>styles/ui.multiselect.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('');?>styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>includes/css/global.css?v=1.0">
<link rel="stylesheet" href="<?php echo base_url(); ?>includes/css/structure.css?v=1.0">
  
</head>
 <body style="width:100%;">
<div id="wrapper">

<div id="maincontent">
 
<script type="text/javascript">  
$(function() {  
    $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	    $('#Disc_To_Date').datetimepicker({  
        duration: '',  
        showTime: true, 
		dateFormat:'yy-mm-dd',  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 }); 
     </script>
<p class="title">&nbsp;</p>

<div id="rightcontentwide">
<div id="content">

      <? if ($rep==NULL){
		echo '<p class="red">Please generate a desktop report in the \'Generate Report\' tab!</p>';
	}
	else {
		?>
      
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
    <thead id="pageFooter">
    <tr><th colspan="10" style="text-align:center;"><strong><?php echo $reporttitle;?></strong></th></tr>
    </thead>
    <thead id="pageFooter">
  <tr>
    <th scope="col">Ticket No</th>
    <th scope="col">Customer</th>
    <th scope="col">Route</th>
    <th scope="col">Paymode</th>
    <th scope="col">Seat No</th>
    <th scope="col">Bus</th>
    <th scope="col">TravelDate</th>
    <th scope="col">Booked By</th>
    <th scope="col">Amount</th>
  </tr>
  </thead>
  <?php
  if ($tickets==NULL)
  {
	  echo "<tr><td colspan=\"9\" style=\"text-align:center;\">No transactions generated</td></tr>";
  }
  else {
	  $totkes = 0;
	  $totugx = 0;
	  $tottzs = 0;
	  $totusd = 0;
	  foreach ($tickets as $t)
	  {
		  
  ?>
  <tr>
    <td><?php echo $t->newticketno; ?></td>
    <td><?php echo $t->clientname; ?></td>
    <td><?php echo $t->TownFrom."-".$t->TownTo; ?></td>
    <td><?php echo $t->paymodes; ?></td>
    <td><?php echo $t->seatno; ?></td>
    <td><?php echo $t->bus_number; ?></td>
    <td><?php echo $t->dateonly; ?></td>
    <td><?php echo $t->t_username; ?></td>
    <td><?php 
	if ($t->t_currency==1)
	{
		$totkes +=$t->amount;
		echo "Kshs ".number_format($t->amount);
	}
	if ($t->t_currency==2)
	{
		$totugx +=$t->amountug;
		echo "Ushs ".number_format($t->amountug);
	}
	if ($t->t_currency==3)
	{
		$tottzs +=$t->amounttz;
		echo "Tshs ".number_format($t->amounttz);
	}
	if ($t->t_currency==4)
	{
		$totusd +=$t->amountusd;
		echo "$".number_format($t->amountusd);
	}
	 ?></td>
  </tr>
  <? } }
  
  if ($tickets<>NULL)
  { ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>Total</strong></td>
    <td><strong><? 
	if ($totkes<>0)
	{
	echo "Kshs ".number_format($totkes)."<br />";
	}
	if ($totugx<>0)
	{
	echo "Ushs ".number_format($totugx)."<br />";
	}
	if ($tottzs<>0)
	{
	echo "Tshs ".number_format($tottzs)."<br />";
	}
	if ($totusd<>0)
	{
	echo "$".number_format($totusd)."<br />";
	}
	?></strong></td>
  </tr>
  <? } ?>
</table>
<? } ?>
  
    </div>
  <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php
exit();
?>