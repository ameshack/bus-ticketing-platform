<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="REFRESH" content="3;url=<?=base_url()."admins/crud_ticketing/".$uri?>">
<title>Failed Ticket</title>
</head>

<body>
<div style="color:#F00; font-size:18px;">
<strong>Ooops! Looks like there was a problem with your seat booking. Please wait while you are redirected back to ticketing page!</strong>
</div>
</body>
</html>