<?php
$data = array(
'title'=>'Account Statements',
'keywords'=>' ',
'description'=>' ',
'selected'=>'account statements',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdate2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Corporate Accounts</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">Account Summary</a></li>
      <li><a href="#create" name="#tab2">Deposit Money</a></li>
      <li><a href="#create" name="#tab3">Tickets History</a></li>
      <li><a href="#create" name="#tab4">Deposits History</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr>
			  <th colspan="3" style="text-align:center;"> Account Type: <strong><?php echo $acctype; ?></strong></th>
		  </tr>
			<tr align="left">
			  <th>Total Credits</th>
			  <th>Total Debits</th>
			  <th>Balance</th>
		  </tr>
		</thead>
			<tr>
				<td align="left"><?php echo anchor('admins/corp_get_deposits/#tab4',"ksh ".number_format($totcredits));?></td>
			  <td align="left"><?php echo anchor('admins/corp_get_tickets/#tab3',"ksh ".number_format($totdebits)); ?></td>
              <td align="left"><?php echo "ksh ".($totcredits - $totdebits); ?></td>
            </tr>
		
	 </table>	
<?php // endif; ?>
            


    </div>
    <div id="tab2">
<?php echo form_open('admins/corp_depost_cash/#tab2');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td colspan="2" class="labels"><p>Please pay the amount you wish to deposit via mpesa paybill. Instructions below:<br />
          <ul>
          <li>Go to your phone and select mpesa/ or SIM toolkit in some phones</li>
          <li>Under mpesa, select "Payment Services"</li>
          <li>Select "Pay Bill"</li>
          <li>Enter Business Number as <strong>525600</strong></li>
          <li>Leave account number blank</li>
          <li>Enter PIN and complete</li>
          <li>You will find the transaction number in the message sent to you by mpesa, enter it below and click Deposit</li>
          </ul>
          </p></td>
        </tr>
        <tr>
          <td width="169" class="labels">Mpesa Transaction Number:</td>
          <td width="519"><span class="red"><?php echo form_error('mpesatransno'); ?></span>
          <input type="text" name="mpesatransno" id="long_input" value="<?php echo $this->input->post('mpesatransno');?>" /><input name="cust_id" type="hidden" value="<?=$cust_id?>" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Deposit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab3">
            	 
                 <?php echo form_open('admins/corp_get_tickets/#tab3'); ?>
                 <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Tickets</th>
		  </tr>
		</thead>
        <tr>
        <td>Date From
          <input name="datef" type="text" class="long_input" id="insdate" value="<?php echo $this->input->post('datef'); ?>"  />
           Date To <input name="datet" type="text" class="long_input" id="insdateedit" value="<?php echo $this->input->post('datet'); ?>" />
        <input  type="submit" name="submit" value="Search Tickets" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"> 
        </td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Date</th>
			  <th>Traveller Name</th>
			  <th>Id No</th>
			  <th>Seat</th>
	          <th>Status</th>
	          <th>Amount</th>
	          <th>Destination</th>
	      </tr>
		</thead>
        <?php
		if ($tickets<>NULL) {
		 foreach ($tickets as $item): ?>
			<tr>
			  <td align="left"><?php echo date('m/d/Y',strtotime($item->DepartureDateTime)); ?></td>
			  <td align="left"><?php echo $item->clientname; ?></td>
              <td align="left"><?php echo $item->t_idno; ?></td>
              <td align="left"><?php echo $item->seatno;?></td>
			  <td align="left"><?php echo $item->ticketstatus;?></td>
			  <td align="left"><?php echo number_format($item->amount,2);?></td>
			  <td align="left"><?php echo $item->TownName;?></td>
	    </tr>
		<?php endforeach; } ?>
	 </table>	
<?php // endif; ?>
          


    </div>
    <div id="tab4">
            	 
                 <?php echo form_open('admins/corp_get_deposits/#tab4'); ?>
                 <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Search Deposits</th>
		  </tr>
		</thead>
        <tr>
        <td>Date From
          <input name="datef" type="text" class="long_input" id="insdate2" value="<?php echo $this->input->post('datef'); ?>"  />
           Date To <input name="datet" type="text" class="long_input" id="insdateedit2" value="<?php echo $this->input->post('datet'); ?>" />
        <input  type="submit" name="submit" value="Search Deposits" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"> 
        </td>
        </tr>
        </table>
        <?php echo form_close(); ?>
        
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Date</th>
			  <th>Amount</th>
			  <th>Approval Status</th>
	          <th>Deposited By</th>
	      </tr>
		</thead>
        <?php
		if ($deposits<>NULL) {
		 foreach ($deposits as $item2): ?>
			<tr>
			  <td align="left"><?php echo date('m/d/Y g:i A',strtotime($item2->dateadded)); ?></td>
			  <td align="left"><?php echo "Ksh ".abs($item2->camount);?></td>
			  <td align="left"><?php 
			  if ($item2->approvedstatus==0)
			  {
				  $attr = array('class'=>'confirm','title'=>'Approve cheque');
				  echo "Pending Approval";
			  }
			  else {
				  echo "Approved";
			  }
			  
			  ?></td>
			  <td align="left"><?php 
			   $this->user=$this->flexi_auth->get_user_by_id($item2->user)->result();
						
                    echo $this->user[0]->upro_first_name." ".$this->user[0]->upro_last_name;
			  ?></td>
	    </tr>
		<?php endforeach; } ?>
	 </table>	
<?php // endif; ?>
          


    </div>
    
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
