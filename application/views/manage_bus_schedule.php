<?php
$data = array(
'title'=>'Manage Bus Schedule',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus schedule',
'img'=>'',
'load_side_menu'=>false
);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
	    $('#arrivaldatetime').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	     $('#departuredatetime').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,   
         minDate: 0, 
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	     $('#departuredatetime2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,   
         minDate: 0, 
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
  
	     $('#departuredatetimeedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
  
	     $('#departuredatetimeedit2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '',
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030', 
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 $('#Disc_From_Date').datetimepicker({  
        duration: '', 
		dateFormat:'yy-mm-dd', 
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true 
     });    
});  
</script> 
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Schedule</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
 
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Bus Schedules</a></li>
      <li><a href="#create" name="#tab2">Add Bus Schedule</a></li>
      <li><a href="#delete" name="#tab4">Delete Schedule</a></li>
      <li><a href="#close" name="#tab5">Close Schedule</a></li>
      <li><a href="#sms" name="#tab6">SMS</a></li>
      <li><a href="#update" name="#tab3">Update Time</a></li>
    </ul>
     <div id="content">
    <div id="tab1">
    
      <?php echo form_open('admins/refine_schedules/#tab1'); ?>
      <table cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23" colspan="2">Refine Summary</th>
		  </tr>
		</thead>
        <tr>
          <td valign="top">
            Branch From <br /> <select name="townf" >
              <option value="0">All</option>
              <?php foreach($towns as $t){?>
              <option <?php if ($this->input->post('townf')==$t->TownId) { echo 'selected'; } ?> value="<?php echo $t->TownId;?>"><?php echo $t->TownName;?></option>
              <?php }?>
          </select></td>
          <td valign="top">Branch To <br />
            <select name="townt" >
              <option value="0">All</option>
              <?php foreach($towns as $t){?>
              <option <?php if ($this->input->post('townt')==$t->TownId) { echo 'selected'; } ?> value="<?php echo $t->TownId;?>"><?php echo $t->TownName;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td valign="top">Date From<br />
          <input name="datef" type="text" class="long_input" id="insdate" readonly="readonly" value="<?php if (!$this->input->post('datef')) { echo date('Y-m-d');} else { echo $this->input->post('datef');} ?>"  /></td>
          <td valign="top">Date To<br />
          <input name="datet" type="text" class="long_input" id="insdateedit" readonly="readonly" value="<?php if (!$this->input->post('datet')) { echo date('Y-m-d',strtotime(' + 6 day'));} else { echo $this->input->post('datet');} ?>" /></td>
        </tr>
        <tr>
          <td colspan="4" style="text-align:center;"><input  type="submit" name="submit" value="Refine Summary" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
        </table>
        <?php echo form_close(); ?>
    	 <?php if(!empty($schedules)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1 display" id="example">
<thead id="pageFooter">
			<tr align="left">
			  <th>No</th>
			  <th>Departure Date</th>
			  <th>Bus Type(Seats Booked)</th>
			  <th>Route</th>
			  <th>Departure Time (New Time)</th>
			  <th>Bus</th>
	          <th>Status</th>
	          <th>Options</th>
		  </tr>
		</thead>
        <?php $cnt = 1; foreach ($schedules as $item): if($item->astatus == '1'): $status = "Open";else:$status = "Closed"; endif;?>
			<tr>
			  <td align="left"><?php echo $cnt; ?></td>
			  <td align="left"><?php echo date('d/m/Y',strtotime($item->DepartureDateTime)); ?></td>
              <td align="left"><?php echo $item->BusType."(".$this->bus_model->gettotrowsresultfromonetbl('ticketing','schedule_id',$item->ScheduleId).")"; ?></td>
              <td align="left"><?php echo $item->RouteName;?></td>
              <td align="left"><?php 
			  $townsz = explode(",",$item->towns);
			  $timeid = $this->bus_model->getdeptimebytown($item->RouteId,$townsz[0]);
			  
			  echo date('g:i A',strtotime($timeid->ArrivalDeparture));
			   $art = array('title'=>'Update Schedule Time','class'=>'confirm');
			  if ($item->NewDepartureTime<>NULL)
			  {
				 
				  echo anchor('admins/update_schedule_time/'.$item->ScheduleId.'#tab3',"(".date('g:i A',strtotime($item->NewDepartureTime)).")",$art);
			  }
			  else {
			   echo "| ";
			   echo anchor('admins/update_schedule_time/'.$item->ScheduleId.'#tab3', img(array('src'=>base_url().'images/schedule_selected.png','border'=>'0','align'=>'absmiddle', 'width'=>'16', 'height'=>'16', 'alt'=>'Update Time')),$art);
			  }
			  ?>
			  </td>
              <td align="left"><?php 
				if ($item->RegistrationNumber=="")
				{
					echo "None";
				}
				else {
					echo $item->RegistrationNumber;
				}
				 ?></td>
			  <td align="left"><?php
			   
			  if ($item->lockstatus==0)
			  {
				   echo $status;
			$atts1 = array(
              'class'      => 'confirm',
			  'title' => 'Lock Schedule'
            );
			  echo anchor('admins/lock_schedule/'.$item->ScheduleId.'/#tab5',img(array('src'=>base_url().'images/lock-icon.png','border'=>'0','align'=>'absmiddle', 'width'=>'16', 'height'=>'16', 'alt'=>'Lock Schedule')),$atts1); 
			  }
			   else
			  {
				   echo "Locked";
			$atts1 = array(
              'class'      => 'confirm',
			  'title' => 'Unlock Schedule'
            );
			  echo anchor('admins/unlock_schedule/'.$item->ScheduleId.'/#tab5',img(array('src'=>base_url().'images/unlock-icon.png','border'=>'0','align'=>'absmiddle', 'width'=>'16', 'height'=>'16', 'alt'=>'Unlock Schedule')),$atts1); 
			  }
			   ?></td>
			  <td align="left"><?php
			   $atts1 = array(
              'class'      => 'confirm',
			  'title' => 'Close Schedule'
            );
			  echo anchor('admins/close_schedule/'.$item->ScheduleId.'/#tab5','Close Schedule',$atts1);
			  ?>  <?php if($item->BusLkp==NULL||$item->BusLkp==0):
			  $atts = array(
              'width'      => '650',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/add_bus_to_schedules/'.$item->ScheduleId, ' | Add Bus', $atts); 
			else:
			 $atts = array(
              'width'      => '650',
              'height'     => '650',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '300',
              'screeny'    => '30'
            );
			echo anchor_popup('admins/add_bus_to_schedules/'.$item->ScheduleId, ' | Edit Bus', $atts); 
			
			endif;
			
			echo anchor('admins/crud_manifest/'.$item->ScheduleId, ' | Manifest', array('title'=>'print manifest','class'=>'confirm'));
			echo anchor('admins/crud_schedules_sms/'.$item->ScheduleId.'#tab6', ' | SMS', array('title'=>'SMS','class'=>'confirm'));
			?></td>
			</tr>		<?php $cnt++; endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no schedules currently!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus_schedule');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
       <tr>
          <td width="199" class="labels">Station:</td>
          <td width="931"><span class="red"><?php echo form_error('route'); ?></span>
             <select name="from" id="from" class="smallselect" onchange="displayroutesfromtown(this.value);">
              <option disabled="disabled" selected="selected">---select town from---</option>
              <?php 
			  
			  foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select></td>
        </tr>
        <tr>
          <td width="199" class="labels">Route:</td>
          <td width="931"><span class="red"><?php echo form_error('route'); ?></span>
          <div id="townroutes">
            <select name="route" id="route" onChange="displayroutetowns(this.value);">
              <option disabled="disabled" selected="selected">---select route--</option>
              <?php foreach($route as $route){?>
              <option value="<?php echo $route->RouteId;?>"><?php echo $route->RouteCode;?></option>
              <?php }?>
          </select></div><div id="routetowns"></div></td>
        </tr>
          <tr>
            <td class="labels">BusType:</td>
            <td><span class="red"><?php echo form_error('bustypelkp'); ?>
              <select name="bustypelkp" id="bustypelkp">
              <option disabled="disabled" selected="selected">---select bus type---</option>
              <?php foreach($bustypes as $bustype){?>
              <option value="<?php echo $bustype->BusTypeId;?>"><?php echo $bustype->BusType;?></option>
              <?php }?>
          </select>
            </span></td>
        </tr>
          <tr>
          <td class="labels"> From:</td>
          <td><span class="red"><?php echo form_error('departuredatetime'); ?></span>
            <input type="text" name="departuredatetime"  id="departuredatetime" class="long_input" value="<?php if (isset($_POST['departuredatetime'])) { echo $this->input->post('departuredatetime');} else { echo date('Y-m-d');}?>"/></td>
          </tr>
          <tr>
          <td class="labels">To:</td>
          <td><span class="red"><?php echo form_error('departuredatetime'); ?></span>
            <input type="text" name="departuredatetime2"  id="departuredatetime2" class="long_input" value="<?php if (isset($_POST['departuredatetime2'])) { echo $this->input->post('departuredatetime2');} else { echo date('Y-m-d');}?>" /></td>
          </tr>
                <tr>
          <td class="labels">Is Special</td>
          <td><input name="isactive" type="checkbox" id="isactive" value="1" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Bus Schedule" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
      </table>
      <?php echo form_close();?>
    </div>
    <div id="tab4">
      <?php if(!empty($schedules)){?>
      <? echo form_open('admins/delete_bus_schedule_multiple'); ?>
      <table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="129">Bus</th>
			  <th width="152">Departure Date</th>
			  <th width="183">Bus Type</th>
			  <th width="223"> Route</th>
			  <th width="171">Departure Time (New Time)</th>
	          <th width="121">Status</th>
	          <th width="131">Options</th>
		  </tr>
		</thead>
        <?php foreach ($schedules as $item):  if($item->IsActive == 1): $status = "Open";else:$status = "Closed"; endif;?>
			<tr>
				<td align="left"><?php echo $item->RegistrationNumber; ?></td>
			  <td align="left"><?php echo date('m/d/Y',strtotime($item->DepartureDateTime)); ?></td>
              <td align="left"><?php echo $item->BusType; ?></td>
              <td align="left"><?php echo $item->RouteName;?></td>
              <td align="left"><?php echo date('g:i A',strtotime($timeid->ArrivalDeparture));
			  if ($item->NewDepartureTime<>NULL)
			  {
				  echo "(".date('g:i A',strtotime($item->NewDepartureTime)).")";
			  }
			  ?></td>
			  <td align="left"><?php echo $status ;?></td>
			  <td align="left"><?php 
			  $ar = array('class'=>'delete','title'=>'Schedule');
			  echo anchor('admins/delete_bus_schedule/'.$item->ScheduleId.'/#delete','X',$ar);?>
              <input name="delsched[]" type="checkbox" value="<? echo $item->ScheduleId; ?>" />
              </td>
			</tr>
            <?php endforeach; ?>
			<tr>
			  <td colspan="7" align="center"><input  type="submit" name="submit" value="Delete Selected Schedules" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px; text-align:center;"  ></td>
	    </tr>
		
	 </table>	
<?php echo form_close();// endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no bus schedules currently!</p>';}; ?>
</div>
<div id="tab5">
<?php if($rep<>NULL) { ?>
<?php echo $closedata;
if ($tickets<>NULL)
{
	
?>
<table width="100%" class="table1">
<thead id="pageFooter">

  <tr>
    <th scope="col">Tickets</th>
    <th scope="col">Available Schedules</th>
  </tr>
  
  </thead><?php

	foreach ($tickets as $t)
	{
		
?>
  <tr>
    <td><?php echo "Name: ".$t->clientname." | Seat No: ".$t->seatno." | Ticket No: ".$t->newticketno."<br />"; ?></td>
    <td>
    <?php 
	
			   $atr1 = array('width'      => '950',
              'height'     => '800',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '30',
              'screeny'    => '30',
			  'title'=>'Open Ticket');
	echo anchor('admins/refund_ticket/'.$t->TicketId,'Refund',array('title'=>'Refund Ticket','class'=>'confirm'));
	echo anchor('admins/open_ticket/'.$t->TicketId,' | Open Ticket');
	echo anchor_popup('admins/ticket_station_voucher/'.$t->fromtown.":".$t->totown.":".$t->TicketId,' | Book',$atr1);
	$atr = array('title'=>'Move ticket', 'class'=>'confirm', 'target'=>'_blank');
	$scheds = $this->bus_model->getBusScheduleswithtickets($t->routecode,$t->DepartureDateTime,$t->fromtown,$t->totown,$t->seatno,$t->BusTypeLkp,$t->schedule_id);
	if ($scheds==NULL):
	//echo "No perfectly matching bus schedules found!";
	else:
	$cnt=1;
	foreach ($scheds as $sc):
	echo anchor('admins/move_ticket_to_new_schedule/'.$sc->schedule_id.':'.$t->TicketId.':'.$t->schedule_id,'Move to',$atr)." this schedule ".$cnt."<br/>";
	$cnt++;
	endforeach;
	endif;
	?>
    </td>
  </tr><?php } ?>
  <?php echo form_open('admins/close_bus_schedule');?>
  <tr>
    <td>Reason</td>
    <td><label for="textarea"></label>
      <textarea name="textarea" id="long_input" class='animated' cols="20" rows="2"></textarea><input name="scheduleid" type="hidden" value="<?php echo $this->uri->segment(3);?>" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input  type="submit" name="submit" value="Close Schedule" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
  </tr>
  <?php echo form_close();?>
</table>

            <?php } }else{ echo '<p class="red">Please select a schedule to close in view schedules tab!</p>';}; ?>
</div>
<div id="tab6">
<?php if(!empty($sms)){?>
<?php // Change the css classes to suit your needs    

$attributes = array('class' => '', 'id' => '');
echo form_open('admins/send_message_sms', $attributes); ?>
<table width="100%" align="center" cellpadding="0" cellspacing="0" class="table1">
      <tr>
        <td width="80" align="right" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="top"><strong>Message</strong>:</td>
        <td valign="bottom"><textarea id="elm1" name="message" class='animated' rows="2" cols="20"><?php echo set_value('message');?></textarea>
          <?php echo form_error('message'); ?><input name="schedid" type="hidden" value="<?php echo $this->uri->segment(3); ?>" /></td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="top">&nbsp;</td>
        <td valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="bottom"><input type="submit" name="submit" value="Send" class="loginsubmitbutton" />
          <input type="reset" name="reset" value="Reset" class="loginsubmitbutton" /></td>
      </tr><?php echo form_close(); ?> 
    </table>
<?php echo form_close(); } else{ echo '<p class="red">Please select schedule from \'view bus schedules\' tab!</p>';}  ?>    </div>
<div id="tab3">
<?php if(!empty($sched)){?>
<?php echo form_open('admins/update_schedule_time_update');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
          <tr>
          <td width="199" class="labels">To:</td>
          <td width="931"><span class="red"><?php echo form_error('departuredatetime3'); ?></span>
            <input type="text" name="departuredatetime3"  id="Disc_From_Date" class="long_input" value="<?php if ($sched->NewDepartureTime<>NULL) { echo $sched->NewDepartureTime; } else { echo date('Y-m-d H:i:s'); } ?>" /><input name="scheduleid" type="hidden" value="<?php echo $this->uri->segment(3);?>" /></td>
          </tr>
                <tr>
          <td class="labels">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Update Schedule Time" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
      </table>
      <?php echo form_close(); } else{ echo '<p class="red">Please select schedule from \'view bus schedules\' tab!</p>';}?>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
