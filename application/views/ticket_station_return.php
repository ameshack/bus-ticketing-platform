<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TMS: Return Ticket</title>

<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
</head>

<body style="background:#D3ECFA;">

<script type="text/javascript">  
$(function() {  
	    $('#traveldate1').datepicker({ 
		dateFormat: 'yy-mm-dd', 
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	 	    $('#traveldate').datepicker({ 
		dateFormat: 'yy-mm-dd', 
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	     $('#traveltime1').timepicker({  
        duration: '',  
        showTime: true,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
        time24h: true  
     });  
});  
</script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Return Tickets</p>

<div id="rightcontentwide">
 
    <ul id="tabs">
      <li><a href="#create" name="#tab1">Add Ticket</a></li>
      <li><a href="#read" name="#tab2">Booked</a></li>
    </ul>
     <div id="content">
        <div id="tab1">
<?php echo form_open('admins/get_schedules');?>
<table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th colspan="6">View Open Bookings</th>
		  </tr>
		</thead>
  <tr>
    <td width="141">Station</td>
    <td width="150"><?php echo form_error('t_station'); ?></span>
            <select name="t_station" id="t_station" class="smallselect" onChange="getBusSchedules(this.value)">
              <option disabled="disabled" selected="selected">---select station---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
          </select></td>
    <td width="174">Travel Date</td>
    <td width="187"><?php echo form_error('traveldate'); ?></span>
      <input type="text" name="traveldate" id="traveldate" class="short_input"  readonly="readonly"  value="<?php echo $this->input->post('traveldate');?>"></td>
    <td width="194">Travel Time</td>
    <td width="125"><?php echo form_error('traveltime'); ?></span>
      <input type="text" name="traveltime1" id="traveltime1" class="short_input"  readonly="readonly" value="<?php echo $this->input->post('traveltime');?>"></td>
  </tr>
</table>
<?php echo form_close();?>

<table width="100%" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="141">Route Code</th>
			  <th width="150">Route</th>
			  <th width="174">Travel Date</th>
			  <th width="187">Departure  Time</th>
			  <th width="194">Bus Type</th>
          </tr>
		</thead>
        <tr><table width="100%" id="schedules" class="table1"><?php echo $html;?>

</table>

       </tr>
</table>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
