<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ticket Voucher</title>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-1.8.2.js"></script>
<style>

			textarea {
				border:2px solid #ccc;
				padding: 10px;
				vertical-align: top;
				width: 25%;
			}

			.animated {
				-webkit-transition: height 0.2s;
				-moz-transition: height 0.2s;
				transition: height 0.2s;
			}

		</style>
<script src='<?=base_url()?>js/jquery.autosize.js'></script>
		<script>
			$(function(){
				$('.normal').autosize();
				$('.animated').autosize({append: "\n"});
			});
		</script>
</head>

<body>

<table border="1" cellpadding="0" cellspacing="0" width="600">
  <tr>
    <td align="center"><h4>MODERN COAST EXPRESS LTD<br />
MODERN COAST EXPRESS TRAVEL VOUCHER<br />
BRANCH: <?php echo $row->TownFrom;?><br />
    Ticket No:<strong> <?php echo $row->newticketno;?></strong></h4></td>
  </tr>
</table>
<?php echo form_open('admins/open_ticket2'); ?>
<table border="1" cellpadding="0" cellspacing="0" width="600">
  <tr>
    <td style="padding:5px;"><strong>Voucher Charge
    <input name="ticketid" type="hidden" value="<? echo $tids; ?>">  
      <select name="currency" id="currency" class="smallselect">
              <option <?php if ($row->t_currency==1): echo "selected"; endif;?> value="1">Kshs</option>
              <option <?php if ($row->t_currency==2): echo "selected"; endif;?> value="2">Ushs</option>
              <option <?php if ($row->t_currency==3): echo "selected"; endif;?> value="3">Tshs</option>   
              <option <?php if ($row->t_currency==4): echo "selected"; endif;?> value="4">USD</option>              
      </select>
      <!--<input name="vouchercharge" type="text" id="vouchercharge" value="0"  onblur="if(this.value=='') this.value='0';" onfocus="if(this.value=='0') this.value='';">-->
      <input name="vouchercharge" type="text" readonly id="vouchercharge" value="<?php echo $vouchercharge; ?>" style="border:1px dotted #000;"> 
      </strong></td>
  </tr>
  <tr>
    <td style="padding:5px;"><strong>Reason
       <span class="red"><?php echo form_error('reason'); ?></span>
                    <textarea name="reason" id="long_input" class="animated" rows="2" style="width:250px;" cols="40"></textarea>
    </strong></td>
  </tr>
  <tr>
    <td style="padding:5px;"><input type="submit" name="button" id="button" value="Create Voucher"></td>
  </tr>
</table>
<?php echo form_close();?>

</body>
</html>