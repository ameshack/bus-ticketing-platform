<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply Discount to Seats</title>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('');?>js/jquery-templ.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" href="<?php echo base_url('');?>styles/ui-lightness/jquery-ui-1.9.1.custom.min.css" type="text/css" />
<script type="text/javascript">  
$(function() {  
    $('#DiscountFrom').datepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });    
	    $('#DiscountTo').datepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });     
 
});  
</script>
<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
</head>

<body style="background:#D3ECFA;">
<?php if($message==NULL){?>
<?php echo form_open('admins/apply_discount_to_seats');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
      <tr>
        <td colspan="2" align="center"><h1>Bus Number: 
          <?=$busname;?>
        </h1></td>
        </tr>
      <tr>
        <td width="227"><span class="red"><?php echo form_error('DiscountType'); ?></span><p>Discount Type<br />
          <label>
            <input type="radio" name="DiscountType" value="1" id="DiscountType_0" />
            Monetary</label>
          <br />
          <label>
            <input type="radio" name="DiscountType" value="2" id="DiscountType_1" />
            Percentage</label>
          <br />
        </p></td>
        <td width="930"><span class="red"><?php echo form_error('DiscountAmount'); ?></span><br />
Discount Amount
        <input type="text" name="DiscountAmount" id="DiscountAmount" /><input type="hidden" name="busid" value="<?php echo $busid;?>" />
        <input type="hidden" name="seattype" value="<?php echo $seattype;?>" /></td>
      </tr>
      <tr>
        <td>Discount From</td>
        <td><span class="red"><?php echo form_error('DiscountFrom'); ?></span>
                  <input type="text" name="DiscountFrom"  id="DiscountFrom" class="long_input" readonly="readonly" value="<?php echo $this->input->post('DiscountFrom');?>"></td>
      </tr>
      <tr>
        <td>Discount To</td>
        <td><span class="red"><?php echo form_error('DiscountTo'); ?></span>
                  <input type="text" name="DiscountTo"  id="DiscountTo" class="long_input" readonly="readonly" value="<?php echo $this->input->post('DiscountTo');?>"></td>
      </tr>
      <tr>
  <td colspan="2">Select Seat</td>
</tr>
<?php foreach($seats as $s){?>

<tr>
                  <td colspan="2"><span class="red"><?php echo form_error('seatno'); ?></span>
                <?php echo $s->seat_no;?> <input type="checkbox" name="seatid[]" id="long_input" value="<?php $s->seat_id;?>">                                   </td>
                </tr><?php }?>

        <tr>
          <td colspan="2"><input  type="submit" name="submit" value="Submit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
<?php echo form_close();?>
     <?php } else { echo "<p>".$message."</p>";?>
<table width="100%">
  <tr>
    <td width="266">&nbsp;</td>
    <td width="879">      <form action='javascript:void(0);' onclick="window.close(); window.opener.location.reload();">
        <p>
          <input name="Button"  type='submit'  id='close' value="Close Window" />
          &nbsp;&nbsp; </p>
      </form></td>
  </tr>
</table>
<?php }?>
</body>
</html>