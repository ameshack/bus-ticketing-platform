<?php
$data = array(
'title'=>'Manage Bus Routes',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus routes',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>

<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Routes</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
 
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Routes</a></li>
      <li><a href="#create" name="#tab2">Add Route</a></li>
      <li><a href="#edit" name="#tab3">Edit Route</a></li>
      <li><a href="#delete" name="#tab4">Delete Route</a></li>
      <li><a href="#addtown" name="#tab5">Add Towns</a></li>
    </ul>
     <div id="content">
    <div id="tab1">
    	 <?php if(!empty($routes)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th height="23">Route Code</th>
			  <th>From</th>
			  <th>To</th>
			  <th>Route</th>
	          <th>Options</th>
		  </tr>
		</thead>
        <?php foreach ($routes as $item): ?>
			<tr>
			  <td align="left"><?php echo $item->RouteCode;?></td>
			  <td align="left"><?php echo $item->TownName; ?></td>
              <td align="left"><?php echo $item->TownTo; ?></td>
              <td align="left"><?php echo $item->RouteName;?></td>
			  <td align="left"><?php echo anchor('admins/edit_bus_route/'.$item->RouteId.'/#tab3','Edit');?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no routes currently!</p>';}; ?>
    </div>
    <div id="tab2">
<?php echo form_open('admins/create_bus_routes');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="102" class="labels">Route Code:</td>
          <td width="217"><span class="red"><?php echo form_error('routecode'); ?></span>
            <?php echo form_error('routecode'); ?><input type="text" name="routecode" id="long_input" value="<?php echo $this->input->post('routecode');?>"></td>
          <td width="202">&nbsp;</td>
          <td width="601">&nbsp;</td>
        </tr>
        
                <tr>
                  <td class="labels">Route Name:</td>
                  <td><input type="text" name="routename" id="long_input" value="<?php echo $this->input->post('routename');?>"></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
          <td class="labels">From</td>
          <td><span class="red"><?php echo form_error('from'); ?></span>
            <select name="from" id="from">
              <option disabled="disabled" selected="selected">---select town from---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
                <tr>
          <td class="labels">To:</td>
          <td><span class="red"><?php echo form_error('to'); ?></span>
            <select name="to" id="to">
              <option disabled="disabled" selected="selected">---select town to---</option>
              <?php foreach($towns as $town){?>
              <option value="<?php echo $town->TownId;?>"><?php echo $town->TownName;?></option>
              <?php }?>
            </select></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="labels">Distance:</td>
          <td><span class="red"><?php echo form_error('distance'); ?></span>
            <input type="text" name="distance" id="long_input" value="<?php echo $this->input->post('distance');?>"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>        <tr>
          <td colspan="4"><span class="red">Only checked ones will be saved!</span></td>
          </tr
><? if ($terminals<>NULL):foreach($terminals as $t):?>
>
        <tr>
          <td><?=$t->TownName; ?><input type="hidden" name="townid[]" value="<?=$t->TownId;?>"></td><td><select name="arrival[]">
              <option disabled="disabled" selected="selected">---select arrival time---</option>
              <?php foreach($times as $x){?>
              <option value="<?php echo $x->TimeId;?>"><?php echo $x->ArrivalDeparture;?></option>
              <?php }?>
            </select></td>
          <td><select name="departure[]">
              <option disabled="disabled" selected="selected">---select departure time---</option>
              <?php foreach($times as $y){?>
              <option value="<?php echo $y->TimeId;?>"><?php echo $y->ArrivalDeparture;?></option>
              <?php }?>
            </select></td>
          <td><input type=checkbox name=checkthis[] value='1'></td>
        </tr>
        <? endforeach; endif;?>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Add Bus Route" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <?php echo form_close();?>
      <script language=Javascript>
function Inint_AJAX() {
try { return new ActiveXObject("Msxml2.XMLHTTP");  } catch(e) {} //IE
try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch(e) {} //IE
try { return new XMLHttpRequest();          } catch(e) {} //Native Javascript
alert("XMLHttpRequest not supported");
return null;
};

function dochange(src, val) {
var req = Inint_AJAX();
req.onreadystatechange = function () {
 if (req.readyState==4) {
      if (req.status==200) {
           document.getElementById(src).innerHTML=req.responseText; //retuen value
      }
 }
};
req.open("GET", "http://localhost/tms/admins/get_name/data="+src+"&val="+val); //make connection
req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=iso-8859-1"); // set Header
req.send(null); //send value
}

window.onLoad=dochange('routecode', -1);         // value in first dropdown
</script>


    </div>
    <div id="tab3"><?php if(!empty($routedetails)){?> 
    <?php echo form_open('admins/update_bus_route');?>
     <?php foreach($routedetails as $row):?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="177" class="labels">Route Code:</td>
          <td width="527"><span class="red"><?php echo form_error('routecode'); ?></span>
            <input type="text" name="routecode" id="long_input" value="<?php echo $row->RouteCode;?>"></td>
        </tr>
        <tr>
          <td class="labels">Route Name:</td>
          <td><span class="red"><?php echo form_error('routename'); ?></span>
            <input type="text" name="routename" id="long_input" value="<?php echo $row->RouteName;?>" ></td>
        </tr>
        
       <tr>
          <td width="177" class="labels">From:</td>
          <td width="527"><span class="red"><?php echo form_error('from'); ?></span>
            <select name="from" id="from">
              <option disabled="disabled">---select route from---</option>
              <?php foreach($towns as $town):?>
<option value="<?php echo $town->TownId;?>" <?php if(!(strcmp($town->TownId, $row->From))) :echo "SELECTED";endif;?>><?php echo $town->TownName;?></option>
              <?php endforeach;?>
            </select></td>
          </tr>
                         <tr>
          <td width="177" class="labels">To:</td>
          <td width="527"><span class="red"><?php echo form_error('to'); ?></span>
            <select name="to" id="to">
              <option disabled="disabled">---select route to---</option>
              <?php foreach($towns as $town):?>
<option value="<?php echo $town->TownId;?>" <?php if(!(strcmp($town->TownId, $row->To))) :echo "SELECTED";endif;?>><?php echo $town->TownName;?></option>
              <?php endforeach;?>
            </select></td>
          </tr>
        <tr>
          <td class="labels">Distance:</td>
          <td><span class="red"><?php echo form_error('distance'); ?></span>
            <input type="text" name="distance" id="long_input" value="<?php echo $row->Distance;?>"></td>
        </tr>
        
        <tr>
          <td><input type="hidden" name="routeid" value="<?php echo $row->RouteId;?>" /></td>
          <td><input  type="submit" name="submit" value="Edit Bus Route" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php endforeach; ?>
      <?php echo form_close();?>
     <?php }else{ echo '<p class="red">Please select route from \'View Routes\' tab!</p>';}; ?></div>
    <div id="tab4">    	 <?php if(!empty($routes)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="center">
			  <th>Route Code</th>
			  <th>From</th>
			  <th>To</th>
			  <th>Delete</th>
	  </tr>
		</thead>
        <?php foreach ($routes as $item): ?>
			<tr>
				<td align="center"><?php echo $item->RouteCode; ?></td>
			  <td align="center"><?php echo $item->From; ?></td>
              <td align="center"><?php echo $item->To; ?></td>
              <td align="center"><?php echo anchor('admins/delete_bus_route/'.$item->RouteId.'','X',array('class' => 'delete','title'=>''.$item->RouteName.'')); ?></td>
			</tr>
		<?php endforeach; ?>
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no routes currently!</p>';}; ?>
</div>
<div id="tab5">  
  <?php 
    if(!empty($towns)){?>
	
	<table width="100%" class="table1">
    <thead id="pageFooter">
			<tr align="left">
			  <th width="153">Added towns</th>
          </tr>
		</thead>
        <?php foreach($towns as $s){?>
            <tr id="input1" class="clonedInput">
            <td><?php echo $s->TownName ;?></td>
            </tr><?php }?>
 </table>
	<?php } ?>
  <?php echo form_open('admins/add_towns');?>
            <table class="table1">
            <thead id="pageFooter">
			<tr align="left">
			  <th width="153">Town</th>
		  </tr>
		</thead>
            <tr id="input1" class="clonedInput">
            <td>
  
 
<div id="input_ln">
                        <input name="town[]" type="text" id="town" tabindex="3">
                    </div><!-- end #input_ln -->                </td>

            </tr><!-- end #input1 -->
 </table>
            <div id="addDelButtons">
                <input type="button" id="btnAdd" value="add section"> <input type="button" id="btnDel" value="remove section above">
            </div>
 
  
                <input type="submit" value="Submit" tabindex="11">
            
      <?php echo form_close();?>
        </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>
</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
