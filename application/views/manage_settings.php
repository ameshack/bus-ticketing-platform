<?php
$data = array(
'title'=>'Manage Settings',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage settings',
'img'=>'',
'load_side_menu'=>false

);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
    $('#datebought').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });
	    $('#dateboughtedit').datetimepicker({  
        duration: '',  
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     }); 
	 $('#insdate').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 $('#insdateedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	 }); 
     </script>
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Settings</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
    <ul id="tabs">
      <li><a href="#read" name="#tab1">View Exchange Rate</a></li>
      <li><a href="#create" name="#tab2">Edit Exchange Rate</a></li>
    </ul>  
    <div id="content">
    <div id="tab1">
    	 <?php if(!empty($item)){?>
   	  <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<thead id="pageFooter">
			<tr align="left">
			  <th width="153">KES</th>
			  <th width="93">UGX</th>
			  <th width="107">TZS</th>
			  <th width="118">USD</th>
	          <th width="105">EURO</th>
          </tr>
		</thead>
      
		<tr>
				<td align="left"><?php echo $item->kes;?></td>
			  <td align="left"><?php echo $item->ugx; ?></td>
              <td align="left"><?php echo $item->tzs; ?></td>
              <td align="left"><?php echo $item->usd; ?></td>
			  <td align="left"><?php echo $item->euro; ?></td>
		    </tr>
		
	 </table>	
<?php // endif; ?>
            <?php }else{ echo '<p class="red">Sorry, no exchange rates set currently!</p>';}; ?>


    </div>
    <div id="tab2">
<?php echo form_open('admins/update_exchange_rate');

if (!empty($item)) {?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="169" height="24" class="labels">KES:</td>
          <td width="519"><?php echo form_error('kes'); ?></span>
            <input type="text" name="kes" id="long_input" value="<?php echo $item->kes;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">UGX:</td>
          <td><?php echo form_error('ugx'); ?></span>
            <input type="text" name="ugx" id="long_input" value="<?php echo $item->ugx;?>" ></td>
        </tr>
        <tr>
          <td height="24" class="labels">TZS:</td>
          <td><span class="red"><?php echo form_error('tzs'); ?></span>
            <input type="text" name="tzs" id="long_input" value="<?php echo $item->tzs;?>" ></td>
        </tr>
        <tr>
          <td class="labels">USD:</td>
          <td><span class="red"><?php echo form_error('usd'); ?></span>
            <input type="text" name="usd"  class="long_input" value="<?php echo $item->usd;?>"/></td>
        </tr>
        <tr>
          <td class="labels">EURO:</td>
          <td><span class="red"><?php echo form_error('euro'); ?></span>
            <input type="text" name="euro" id="long_input" value="<?php echo $item->euro;?>"></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" class="confirm" title="Edit Exchange Rates" value="Edit Exchange Rates" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
      <?php echo form_close();
}
	  ?>
    </div>
  </div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
