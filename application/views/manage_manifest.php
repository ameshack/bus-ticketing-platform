<?php
$data = array(
'title'=>'Manage Bus Schedule',
'keywords'=>' ',
'description'=>' ',
'selected'=>'manage bus schedule',
'img'=>'',
'load_side_menu'=>false
);
$this->load->view('extras/upper',$data); ?>
<script type="text/javascript">  
$(function() {  
	    $('#arrivaldatetime').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
	     $('#departuredatetime').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });   
	     $('#departuredatetime2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
  
	     $('#departuredatetimeedit').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
  
	     $('#departuredatetimeedit2').datepicker({  
        duration: '',  
		dateFormat:'yy-mm-dd', 
        showTime: false,  
        constrainInput: false,  
        stepMinutes: 1,  
        stepHours: 1,  
        altTimeField: '', 
		changeMonth: true,
		changeYear: true,
		yearRange: '2005:2030',
        time24h: true  
     });  
});  
</script> 
<p class="title">Modern Coast Booking <span class="red">>> </span>Manage Bus Schedule</p>
<div id="leftcontent">
<?php $this->load->view('extras/left_links'); ?>
</div>
<div id="rightcontent">
 
    <ul id="tabs">
      <li><a href="#mani" name="#tab1">Manifest/Chart</a></li>
    </ul>
     <div id="content">
<div id="tab1">
<?php echo form_open('admins/manifest_chart'); ?>

<table width="100%" cellpadding="0" cellspacing="0" class="table1">
        <tr>
          <td width="199" class="labels">Route:</td>
          <td width="931"><span class="red"><?php echo form_error('route'); ?></span>
         
            <select name="route" id="route" onChange="displayroutetowns(this.value);">
              <?php
			  $townsa = explode(",",trim($manifest->towns,','));
    for ($i=0;$i<count($townsa);$i++)
    {
		for ($j=0;$j<count($townsa);$j++)
		{
        if ($i<$j)
			{
			$ftown = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townsa[$i]);
			$stown = $this->bus_model->getonerowfromonetbl('towns_lkp', 'TownId', $townsa[$j]);
			
        	$townids = $townsa[$i]."-".$townsa[$j];
			echo "<option value=\"".$townids."\">".$ftown->TownName." - ".$stown->TownName."</option>";
			}
        }
		
		
    }
			   ?>
          </select></td>
        </tr>
                <tr>
          <td class="labels">Choose</td>
          <td><input type="radio" name="maniorchart" id="maniorchart" value="1" checked="checked" />Manifest <input type="radio" name="maniorchart" id="maniorchart" value="2" />Chart 
            <label for="radio"></label><input name="schedid" type="hidden" value="<?=$schedid?>" /></td>
        </tr>
        <tr>
          <td></td>
          <td><input  type="submit" name="submit" value="Print" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px" /></td>
        </tr>
      </table>
   
    <?php echo form_close();?>

</div></div>
 <script>
    function resetTabs(){
        $("#content > div").hide(); //Hide all content
        $("#tabs a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content > div").hide(); // Initially hide all content
        $("#tabs li:first a").attr("id","current"); // Activate first tab
        $("#content > div:first").fadeIn(); // Show first tab content
        
        $("#tabs a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })()
  </script>

</div>
</div>
<?php  $this->load->view('extras/lower'); ?>
