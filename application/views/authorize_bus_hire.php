<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Authorize Bus Hire</title>

<link rel="stylesheet" href="<?php echo base_url('');?>styles/tms.css" type="text/css" />
</head>

<body style="background:#D3ECFA;">
<?php if($message==NULL){?>
<?php echo form_open('admins/confirm_authorization');?>
      <table width="100%" cellpadding="0" cellspacing="0" class="table1">
<tr>
  <td class="labels">Authorizing Person:</td>
  <td><?php echo form_error('user'); ?>
  <select name="user" id="user">
        <option disabled selected>---please select user---</option>
        <? if ($users<>NULL) {  foreach ($users as $r){ ?>
        <option value="<? echo $r->uacc_id;?>"> <? echo $r->upro_first_name.' '.$r->upro_first_name;?></option>
        <? }} ?>
      </select></td>
</tr>
<tr>
                  <td width="269" class="labels">Password:</td>
                  <td><span class="red"><?php echo form_error('password'); ?></span>
                  <input type="password" name="password" id="long_input">
                  <input type="hidden" name="busid" value="<?php echo $busid;?>" />
                   <input type="hidden" name="hireid" value="<?php echo $hireid;?>" />                  </td>
                </tr>

        <tr>
          <td></td>
          <td width="886"><input  type="submit" name="submit" value="Submit" style="padding-left:10px; padding-right:10px; padding-top:5px; padding-bottom:5px"  ></td>
        </tr>
      </table>
     <?php echo form_close();?>
     <?php } else { echo "<p>".$message."</p>";?>
      <table width="100%">
  <tr>
    <td width="266">&nbsp;</td>
    <td width="879">      <form action='javascript:void(0);' onclick="window.close(); window.opener.location.reload();">
        <p>
          <input name="Button"  type='submit'  id='close' value="Close Window" />
          &nbsp;&nbsp; </p>
      </form></td>
  </tr>
</table>
<?php }?>
</body>
</html>