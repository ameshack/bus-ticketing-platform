$(function() {
    $("#routecode").change(function(){
        // getting the value that user selected
        var searchString    = $("#routecode").val();
        // forming the queryString
        var data = 'routeid='+ searchString;
        // if searchString is not empty
        if(searchString) {
            // ajax call
            $.ajax({
                type: "POST",
                url: "http://localhost/tms/admins/getRoute/",
                data: data,
                beforeSend: function(html) { // this happens before actual call
                    $("#results").html('');
                    $("#searchresults").show();
               },
               success: function(html){ // this happens after we get results
			   var obj = jQuery.parseJSON(html);
			   var routename = '';
			   $.each(obj, function() {
      			  routename += this['RouteName'];
                    $("#results").show();
                    $("#results").append(routename);
    				});
              }
            });   
        }
        return false;
    });
});