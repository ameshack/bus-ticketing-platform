$(function () {
    $('#btnAdd').click(function () {
        var num     = $('.clonedInput').length, // how many "duplicatable" input fields we currently have
            newNum  = new Number(num + 1),      // the numeric ID of the new input field being added
            newElem = $('#input' + num).clone().attr('id', 'input' + newNum).fadeIn('slow'); // create the new element via clone(), and manipulate it's ID using newNum value
    // manipulate the name/id values of the input inside the new element
        newElem.find('#label_fn label').attr('for', 'seatnumber' + newNum);
        newElem.find('#input_fn input').attr('id', 'seatnumber' + newNum).attr('name', 'seatnumber[]' + newNum).val('');
	    newElem.find('#label_class label').attr('for', 'seatclass' + newNum);
        newElem.find('#select_class select').attr('id', 'seatclass' + newNum).attr('name', 'seatclass[]' + newNum).val('');
        newElem.find('#label_cat label').attr('for', 'seatposition' + newNum);
        newElem.find('#select_cat select').attr('id', 'seatposition' + newNum).attr('name', 'seatposition[]' + newNum).val('');
    // insert the new element after the last "duplicatable" input field
        $('#input' + num).after(newElem);
    // enable the "remove" button
        $('#btnDel').attr('disabled', false);
 
    // right now you can only add 5 sections. change '5' below to the max number of times the form can be duplicated
        if (newNum == 47)
        $('#btnAdd').attr('disabled', true).prop('value', "You've reached the limit");
    });
 
    $('#btnDel').click(function () {
    // confirmation
        if (confirm("Are you sure you wish to remove this section? This cannot be undone."))
            {
                var num = $('.clonedInput').length;
                // how many "duplicatable" input fields we currently have
                $('#input' + num).slideUp('slow', function () {$(this).remove();
                // if only one element remains, disable the "remove" button
                    if (num -1 === 1)
                $('#btnDel').attr('disabled', true);
                // enable the "add" button
                $('#btnAdd').attr('disabled', false).prop('value', "add section");});
            }
        return false;
             // remove the last element
 
    // enable the "add" button
        $('#btnAdd').attr('disabled', false);
    });
 
    $('#btnDel').attr('disabled', true);
});