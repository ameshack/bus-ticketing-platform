$('#routename, #routename').hide();
$('#routecode').change(function(){
    var routeid = $('#routecode').val();
    if (routeid != ""){
        var post_url = "/tms/admins/get_name/" + routeid;
        $.ajax({
            type: "POST",
             url: post_url,
             success: function(routes) //we're calling the response json array 'cities'
              {
                $('#routecode').empty();
                $('#routecode').show();
                   $.each(routes,function(RouteCodeId,RouteName) 
                   {
                    var opt = $('<option />'); // here we're creating a new select option for each group
                      opt.val(RouteCodeId);
                      opt.text(RouteName);
                      $('#routename').append(opt); 
                });
               } //end success
         }); //end AJAX
    } else {
        $('#routename').empty();
        $('#routename').hide();
    }//end if
}); //end change 
