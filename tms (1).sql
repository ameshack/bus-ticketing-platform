-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2013 at 07:37 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_codes`
--

CREATE TABLE IF NOT EXISTS `account_codes` (
  `AccountCodeId` int(11) NOT NULL AUTO_INCREMENT,
  `AccountCode` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`AccountCodeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account_codes`
--

INSERT INTO `account_codes` (`AccountCodeId`, `AccountCode`, `status`) VALUES
(1, 'ACC 1', 1),
(2, 'ACC 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `AdminId` int(10) NOT NULL AUTO_INCREMENT,
  `AdminName` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `IsActive` enum('1','0') DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`AdminId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`AdminId`, `AdminName`, `Email`, `Username`, `Password`, `IsActive`, `status`) VALUES
(2, 'Wesley Kosgey', 'wesleygk@gmail.com', 'wesley', 'e10adc3949ba59abbe56e057f20f883e', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banking`
--

CREATE TABLE IF NOT EXISTS `banking` (
  `BankingId` int(50) NOT NULL AUTO_INCREMENT,
  `TerminusId` smallint(3) NOT NULL,
  `TotalAmount` double(10,2) NOT NULL,
  `BankingDate` datetime NOT NULL,
  `AddedBy` int(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `refno` varchar(100) NOT NULL,
  `bankslip` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`BankingId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `banking`
--

INSERT INTO `banking` (`BankingId`, `TerminusId`, `TotalAmount`, `BankingDate`, `AddedBy`, `status`, `refno`, `bankslip`) VALUES
(1, 5, 55000.00, '2013-01-21 18:06:00', 2, 0, 'NO67272', NULL),
(2, 2, 54000.00, '2012-01-21 00:00:00', 1, 1, 'QWERTY123', 'CORD-TV-EDITED.jpg'),
(3, 2, 10000.00, '2013-04-14 00:00:00', 3, 1, 'QW3434YU5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE IF NOT EXISTS `buses` (
  `BusId` int(11) NOT NULL AUTO_INCREMENT,
  `BusTypeLkp` smallint(3) NOT NULL,
  `ExternalColour` varchar(40) NOT NULL,
  `InternalColour` varchar(40) NOT NULL,
  `RegistrationNumber` varchar(20) NOT NULL,
  `MakeLkp` smallint(3) NOT NULL,
  `ModelLkp` smallint(3) DEFAULT NULL,
  `DateBought` varchar(100) DEFAULT NULL,
  `DateAdded` datetime NOT NULL,
  `BusName` varchar(100) DEFAULT NULL,
  `BusNumber` varchar(40) DEFAULT NULL,
  `IsActive` enum('1','0') NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `InsuranceExpiry` date NOT NULL,
  `CopyOfInsurance` varchar(200) NOT NULL,
  `CopyOfComesa` varchar(200) NOT NULL,
  `CopyOfLogbook` varchar(200) NOT NULL,
  PRIMARY KEY (`BusId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`BusId`, `BusTypeLkp`, `ExternalColour`, `InternalColour`, `RegistrationNumber`, `MakeLkp`, `ModelLkp`, `DateBought`, `DateAdded`, `BusName`, `BusNumber`, `IsActive`, `AddedBy`, `status`, `InsuranceExpiry`, `CopyOfInsurance`, `CopyOfComesa`, `CopyOfLogbook`) VALUES
(1, 2, 'White', 'Blue', 'KBC 796S', 1, NULL, '05/07/2013 00:00', '2013-05-07 16:42:31', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'Copy_of_KBC_796S.jpg'),
(2, 2, 'White', 'Blue', 'KBC 797S', 1, NULL, '05/07/2013 00:00', '2013-05-07 16:44:36', 'Scania', 'B', '1', 1, 1, '2013-08-31', '', '', 'Copy_of_KBC_797S.jpg'),
(3, 2, 'White', 'Blue', 'KBF 634H', 1, NULL, '05/07/2013 00:00', '2013-05-07 16:45:43', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'Copy_of_KBF_634H.jpg'),
(4, 2, 'White', 'Blue', 'KBK 036J', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:03:57', 'Scania', 'A', '1', 1, 1, '2013-09-30', '', '', 'Copy_of_KBK_036J.jpg'),
(5, 1, 'Gold', 'Blue', 'KBT 001S', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:13:56', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'KBT_001S.jpg'),
(6, 1, 'White', 'Blue', 'KBT 007S', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:17:45', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'KBT_007S.jpg'),
(7, 1, 'Black', 'Blue', 'KBP 001E', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:18:48', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'LOG_BOOK_-_KBP_001E.jpg'),
(8, 1, 'Green', 'Blue', 'KBM 377V', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:19:48', 'Scania', '1', '1', 1, 1, '2013-08-31', '', '', 'Copy_of_KBM_377V.jpg'),
(9, 1, 'Purple', 'Blue', 'KBH 681Q', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:21:35', 'Scania', 'A', '1', 1, 1, '2013-09-30', '', '', 'KBH_681Q.jpg'),
(10, 1, 'White', 'Blue', 'KBH 190C', 1, NULL, '05/07/2013 00:00', '2013-05-07 17:22:17', 'Scania', 'A', '1', 1, 1, '2013-08-31', '', '', 'KBH_190C.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bustype_lkp`
--

CREATE TABLE IF NOT EXISTS `bustype_lkp` (
  `BusTypeId` smallint(2) NOT NULL AUTO_INCREMENT,
  `BusType` varchar(50) NOT NULL,
  `TotalSeats` smallint(3) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `ifalltowns` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BusTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bustype_lkp`
--

INSERT INTO `bustype_lkp` (`BusTypeId`, `BusType`, `TotalSeats`, `AddedBy`, `ifalltowns`, `status`) VALUES
(1, 'Oxygen', 47, NULL, 0, 1),
(2, 'Normal', 45, 1, 1, 1),
(3, 'Irizar', 47, 1, 0, 1),
(4, 'Minibus', 35, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bus_hire`
--

CREATE TABLE IF NOT EXISTS `bus_hire` (
  `BusHireId` int(20) NOT NULL AUTO_INCREMENT,
  `BusId` int(11) NOT NULL,
  `HirerId` varchar(255) NOT NULL,
  `DateFrom` datetime NOT NULL,
  `DateTo` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `DateReturned` datetime DEFAULT NULL,
  `AuthorizedBy` int(11) DEFAULT NULL,
  `ReturnedBy` int(11) DEFAULT NULL,
  `AuthorizedStatus` varchar(30) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`BusHireId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bus_hire`
--

INSERT INTO `bus_hire` (`BusHireId`, `BusId`, `HirerId`, `DateFrom`, `DateTo`, `status`, `description`, `DateReturned`, `AuthorizedBy`, `ReturnedBy`, `AuthorizedStatus`) VALUES
(1, 3, '1', '2013-01-19 17:00:00', '2013-01-26 00:00:00', 1, '', NULL, NULL, NULL, 'returned'),
(2, 2, '2', '2013-01-18 07:00:00', '2013-01-23 00:00:00', 1, '', NULL, 1, 2, 'returned');

-- --------------------------------------------------------

--
-- Table structure for table `bus_maintenance`
--

CREATE TABLE IF NOT EXISTS `bus_maintenance` (
  `MaintenanceId` int(100) NOT NULL AUTO_INCREMENT,
  `BusLkp` int(100) NOT NULL,
  `DateAdmitted` varchar(30) NOT NULL,
  `DateReleased` varchar(30) DEFAULT NULL,
  `MaintenanceLkp` int(100) NOT NULL,
  `Description` text NOT NULL,
  `IsSorted` enum('1','0') DEFAULT '0',
  `DateAdded` datetime NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`MaintenanceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `bus_maintenance`
--

INSERT INTO `bus_maintenance` (`MaintenanceId`, `BusLkp`, `DateAdmitted`, `DateReleased`, `MaintenanceLkp`, `Description`, `IsSorted`, `DateAdded`, `AddedBy`, `status`) VALUES
(5, 2, '11/06/2012 03:00', '11/07/2012 03:00', 1, 'Regular maintenance clinic - changing of oil filters, engine cleaning and fixing the air con.', '1', '2012-11-07 12:45:07', 2, 1),
(6, 1, '11/07/2012 16:04', '11/07/2012 18:03', 1, 'Regular checkups', '1', '2012-11-07 16:04:47', 2, 1),
(7, 4, '01/17/2013 12:47', '001/18/2013 12:47', 1, 'Now in good condition', '1', '2013-01-21 15:31:39', 0, 1),
(8, 1, '01/18/2013 17:26', '01/21/2013 17:26', 1, 'Testing new tabs', '1', '2013-01-21 15:27:28', 0, 1),
(9, 2, '2013-01-31', '0', 1, 'maintain', '', '2013-01-31 00:57:04', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bus_routes`
--

CREATE TABLE IF NOT EXISTS `bus_routes` (
  `RouteId` int(100) NOT NULL AUTO_INCREMENT,
  `RouteCode` varchar(40) NOT NULL,
  `From` int(11) NOT NULL,
  `To` int(11) NOT NULL,
  `Distance` varchar(50) NOT NULL,
  `RouteName` varchar(100) NOT NULL,
  `RegionLkp` int(100) DEFAULT NULL,
  `DateAdded` datetime NOT NULL,
  `AddedBy` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `towns` varchar(250) NOT NULL,
  PRIMARY KEY (`RouteId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `bus_routes`
--

INSERT INTO `bus_routes` (`RouteId`, `RouteCode`, `From`, `To`, `Distance`, `RouteName`, `RegionLkp`, `DateAdded`, `AddedBy`, `status`, `towns`) VALUES
(43, 'NRB-MSA11:00PM', 2, 1, '450', 'Nairobi-Mombasa', NULL, '2013-01-21 15:42:15', 1, 1, '2,6,1,'),
(44, 'MSA-NRB11:00PM', 1, 2, '500KM', 'Mombasa-Nairobi', NULL, '2013-04-14 15:02:47', 1, 1, '1,6,2,'),
(45, 'MSA-KAM10:00AM', 1, 5, '1500', 'Mombasa-Kampala', NULL, '2013-05-03 19:44:14', 1, 1, '1,6,2,3,5,'),
(47, 'NRB-KAM6:00PMA', 2, 5, '1100', 'Nairobi-Kampala', NULL, '2013-05-04 17:04:37', 1, 1, '2,3,5,'),
(49, 'MSA-NRB10:00AMA', 1, 2, '500', 'Mombasa-Nairobi', NULL, '2013-05-04 16:43:25', 1, 1, '1,6,2,'),
(50, 'KAM-NRB10:00AMA', 5, 2, '1100', 'Kampala-Nairobi', NULL, '2013-05-07 01:29:49', 1, 1, '5,4,3,2,');

-- --------------------------------------------------------

--
-- Table structure for table `bus_schedule`
--

CREATE TABLE IF NOT EXISTS `bus_schedule` (
  `ScheduleId` int(100) NOT NULL AUTO_INCREMENT,
  `BusTypeLkp` int(11) DEFAULT NULL,
  `BusLkp` int(100) DEFAULT NULL,
  `RouteLkp` int(100) NOT NULL,
  `Driver` int(11) DEFAULT NULL,
  `Conductor` int(11) DEFAULT NULL,
  `DepartureDateTime` date NOT NULL,
  `Busno` varchar(10) NOT NULL,
  `IsActive` enum('1','0') DEFAULT '1',
  `IsSpecial` int(1) NOT NULL DEFAULT '0',
  `DateAdded` datetime NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ScheduleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `bus_schedule`
--

INSERT INTO `bus_schedule` (`ScheduleId`, `BusTypeLkp`, `BusLkp`, `RouteLkp`, `Driver`, `Conductor`, `DepartureDateTime`, `Busno`, `IsActive`, `IsSpecial`, `DateAdded`, `AddedBy`, `status`) VALUES
(3, 1, 3, 43, 1, 2, '0000-00-00', 'A', '1', 0, '2013-03-19 12:55:47', 1, 1),
(4, 1, 2, 43, 1, 2, '0000-00-00', 'A', '1', 0, '2013-02-02 15:22:42', 1, 1),
(5, 1, NULL, 44, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-04-14 15:06:10', 1, 1),
(15, 2, 1, 44, 1, 2, '0000-00-00', 'A', '1', 1, '2013-05-01 14:44:45', 1, 1),
(16, 1, NULL, 44, NULL, NULL, '0000-00-00', 'A', '1', 1, '2013-04-23 05:13:32', 1, 1),
(17, 2, NULL, 44, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-04-23 05:13:32', 1, 1),
(18, 2, NULL, 44, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-04-23 05:13:32', 1, 1),
(19, 2, NULL, 44, NULL, NULL, '0000-00-00', 'B', '1', 0, '2013-04-23 16:24:22', 1, 1),
(20, 2, NULL, 44, NULL, NULL, '2013-05-10', 'B', '1', 0, '2013-04-23 16:24:22', 1, 1),
(21, 2, NULL, 44, NULL, NULL, '2013-05-10', 'B', '1', 0, '2013-04-23 16:24:22', 1, 1),
(22, 1, NULL, 44, NULL, NULL, '2013-05-10', 'B', '1', 1, '2013-04-23 16:24:22', 1, 1),
(23, 1, NULL, 44, NULL, NULL, '2013-05-10', 'B', '1', 1, '2013-04-23 16:24:22', 1, 1),
(24, 1, NULL, 43, NULL, NULL, '2013-05-11', 'A', '1', 1, '2013-04-23 16:24:22', 1, 1),
(25, 1, NULL, 44, NULL, NULL, '2013-05-11', 'B', '1', 1, '2013-04-23 16:24:22', 1, 1),
(26, 1, NULL, 47, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-05-05 12:30:10', 1, 1),
(27, 2, NULL, 47, NULL, NULL, '2013-05-10', 'B', '1', 1, '2013-05-05 12:30:10', 1, 1),
(28, 2, NULL, 50, NULL, NULL, '2013-05-10', 'B', '1', 0, '2013-05-07 01:30:27', 1, 1),
(29, 2, NULL, 47, NULL, NULL, '2013-05-10', 'B', '1', 0, '2013-05-09 20:05:19', 1, 1),
(30, 1, NULL, 43, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-05-11 19:36:37', 1, 1),
(31, 1, NULL, 43, NULL, NULL, '0000-00-00', 'A', '1', 0, '2013-05-11 19:36:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bus_staff`
--

CREATE TABLE IF NOT EXISTS `bus_staff` (
  `StaffId` int(100) NOT NULL AUTO_INCREMENT,
  `StaffName` varchar(100) NOT NULL,
  `StaffIdNumber` varchar(15) DEFAULT NULL,
  `StaffNumber` varchar(30) NOT NULL,
  `StaffJobLkp` smallint(3) NOT NULL,
  `StaffEmail` varchar(70) DEFAULT NULL,
  `StaffAdded` datetime DEFAULT NULL,
  `AddedBy` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `SPhoto` varchar(200) DEFAULT NULL,
  `LicenceNo` varchar(100) DEFAULT NULL,
  `LicenceCopyFile` varchar(200) DEFAULT NULL,
  `LicenceExpiry` date DEFAULT NULL,
  `PSVNo` varchar(100) DEFAULT NULL,
  `PSVCopy` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`StaffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bus_staff`
--

INSERT INTO `bus_staff` (`StaffId`, `StaffName`, `StaffIdNumber`, `StaffNumber`, `StaffJobLkp`, `StaffEmail`, `StaffAdded`, `AddedBy`, `status`, `SPhoto`, `LicenceNo`, `LicenceCopyFile`, `LicenceExpiry`, `PSVNo`, `PSVCopy`) VALUES
(1, 'Fahmi Mohammed', '22411922', 'NO989', 1, '', '2013-05-07 13:12:19', 1, 1, 'sofa.png', 'RD75411', 'ScaleBase-Technical-WhitePaper_V2_February-2013.pdf', '2013-08-31', NULL, NULL),
(2, 'Juma Kilungi', '454545454', '34', 2, '', '2013-01-18 10:00:00', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Jackson Siele', '23232323', '543', 2, NULL, '2013-01-18 12:07:10', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Katama Mkangi', '23325431', '12', 1, NULL, '2013-05-07 12:22:38', 1, 1, 'Bill_Gates.jpg', 'RD75436', 'output.pdf', '2013-09-30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bus_termini`
--

CREATE TABLE IF NOT EXISTS `bus_termini` (
  `TerminusId` int(100) NOT NULL AUTO_INCREMENT,
  `RegionLkp` smallint(3) NOT NULL,
  `TownLkp` smallint(3) NOT NULL,
  `TerminusName` varchar(100) NOT NULL,
  `TerminusPhone` varchar(20) DEFAULT NULL,
  `TerminusCellPhone` varchar(20) DEFAULT NULL,
  `TerminusEmail` varchar(70) DEFAULT NULL,
  `AddedDate` datetime NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TerminusId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bus_termini`
--

INSERT INTO `bus_termini` (`TerminusId`, `RegionLkp`, `TownLkp`, `TerminusName`, `TerminusPhone`, `TerminusCellPhone`, `TerminusEmail`, `AddedDate`, `AddedBy`, `status`) VALUES
(2, 1, 2, 'Total Mombasa Road', '0721333333', '0721333334', 'totalmsard@moderncoast.com', '2013-04-26 17:09:22', 1, 1),
(3, 1, 2, 'Total Ngong Road', '0721222222', NULL, 'totalngongrd@moderncoast.com', '2012-11-07 15:58:56', 2, 1),
(4, 1, 1, 'Total Moi Avenue', '0334949494', NULL, 'totalmoiav@moderncoast.com', '2012-11-09 18:16:36', 2, 1),
(5, 1, 2, 'Uchumi Ngong Rd', '0728454389', NULL, 'kosgeywes@yahoo.com', '2013-01-17 12:38:48', 2, 1),
(6, 2, 5, 'Kampala', '0721333333', '0721333334', 'totalmsard@moderncoast.com', '2013-05-07 01:58:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bus_type_towns`
--

CREATE TABLE IF NOT EXISTS `bus_type_towns` (
  `bus_type_towns_id` int(11) NOT NULL AUTO_INCREMENT,
  `BusTypeId` int(11) NOT NULL,
  `TownId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bus_type_towns_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `bus_type_towns`
--

INSERT INTO `bus_type_towns` (`bus_type_towns_id`, `BusTypeId`, `TownId`, `status`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 3, 1, 1),
(4, 3, 2, 1),
(5, 4, 1, 1),
(6, 3, 10, 1),
(7, 4, 10, 1),
(8, 4, 2, 1),
(9, 1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card_details`
--

CREATE TABLE IF NOT EXISTS `card_details` (
  `card_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `card_no` varchar(150) NOT NULL,
  `f_name` varchar(100) NOT NULL,
  `m_name` varchar(100) NOT NULL,
  `l_name` varchar(100) NOT NULL,
  `idno_or_passportno` varchar(50) NOT NULL,
  `phoneno` varchar(20) NOT NULL,
  `nationality` varchar(150) NOT NULL,
  `address` text,
  `email` varchar(150) NOT NULL,
  `dob` date NOT NULL,
  `county` varchar(100) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`card_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `card_details`
--

INSERT INTO `card_details` (`card_details_id`, `card_no`, `f_name`, `m_name`, `l_name`, `idno_or_passportno`, `phoneno`, `nationality`, `address`, `email`, `dob`, `county`, `gender`, `status`) VALUES
(1, '234567', 'Marcel', 'Auja', 'Ogweno', '11237486', '0703212345', 'Kenya', NULL, 'marcel@mtl.co.ke', '2012-09-04', 'Siaya', 'Male', 1),
(2, '123456', 'Haroon', 'O2', 'Butt', '5554535353', '0719110786', 'Kenya', NULL, 'haroon.butt@mcbc.co.ke', '1986-08-06', 'Mombasa', 'Male', 1);

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `channels_id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`channels_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`channels_id`, `channel`, `status`) VALUES
(1, 'Desktop', 1),
(2, 'Device(Agents)', 1),
(3, 'Online', 1),
(4, 'M-Shop', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countrylist`
--

CREATE TABLE IF NOT EXISTS `countrylist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=248 ;

--
-- Dumping data for table `countrylist`
--

INSERT INTO `countrylist` (`ID`, `Name`, `status`) VALUES
(1, 'Kenya', 1),
(243, 'Canada', 1),
(244, 'Mexico', 1),
(245, 'Afghanistan', 1),
(246, 'Albania', 1),
(6, 'South Sudan', 1),
(7, 'Andorra', 1),
(8, 'Angola', 1),
(9, 'Anguilla', 1),
(10, 'Antarctica', 1),
(11, 'Antigua and Barbuda', 1),
(12, 'Argentina', 1),
(13, 'Armenia', 1),
(14, 'Aruba', 1),
(15, 'Ascension Island', 1),
(16, 'Australia', 1),
(17, 'Austria', 1),
(18, 'Azerbaijan', 1),
(19, 'Bahamas', 1),
(20, 'Bahrain', 1),
(21, 'Bangladesh', 1),
(22, 'Barbados', 1),
(23, 'Belarus', 1),
(24, 'Belgium', 1),
(25, 'Belize', 1),
(26, 'Benin', 1),
(27, 'Bermuda', 1),
(28, 'Bhutan', 1),
(29, 'Bolivia', 1),
(30, 'Bophuthatswana', 1),
(31, 'Bosnia-Herzegovina', 1),
(32, 'Botswana', 1),
(33, 'Bouvet Island', 1),
(34, 'Brazil', 1),
(35, 'British Indian Ocean', 1),
(36, 'British Virgin Islands', 1),
(37, 'Brunei Darussalam', 1),
(38, 'Bulgaria', 1),
(39, 'Burkina Faso', 1),
(5, 'Burundi', 1),
(41, 'Cambodia', 1),
(42, 'Cameroon', 1),
(44, 'Cape Verde Island', 1),
(45, 'Cayman Islands', 1),
(46, 'Central Africa', 1),
(47, 'Chad', 1),
(48, 'Channel Islands', 1),
(49, 'Chile', 1),
(50, 'China, Peoples Republic', 1),
(51, 'Christmas Island', 1),
(52, 'Cocos (Keeling) Islands', 1),
(53, 'Colombia', 1),
(54, 'Comoros Islands', 1),
(55, 'Congo', 1),
(56, 'Cook Islands', 1),
(57, 'Costa Rica', 1),
(58, 'Croatia', 1),
(59, 'Cuba', 1),
(60, 'Cyprus', 1),
(61, 'Czech Republic', 1),
(62, 'Denmark', 1),
(63, 'Djibouti', 1),
(64, 'Dominica', 1),
(65, 'Dominican Republic', 1),
(66, 'Easter Island', 1),
(67, 'Ecuador', 1),
(68, 'Egypt', 1),
(69, 'El Salvador', 1),
(70, 'England', 1),
(71, 'Equatorial Guinea', 1),
(72, 'Estonia', 1),
(73, 'Ethiopia', 1),
(74, 'Falkland Islands', 1),
(75, 'Faeroe Islands', 1),
(76, 'Fiji', 1),
(77, 'Finland', 1),
(78, 'France', 1),
(79, 'French Guyana', 1),
(80, 'French Polynesia', 1),
(81, 'Gabon', 1),
(82, 'Gambia', 1),
(83, 'Georgia Republic', 1),
(84, 'Germany', 1),
(85, 'Gibraltar', 1),
(86, 'Greece', 1),
(87, 'Greenland', 1),
(88, 'Grenada', 1),
(89, 'Guadeloupe (French)', 1),
(90, 'Guatemala', 1),
(91, 'Guernsey Island', 1),
(92, 'Guinea Bissau', 1),
(93, 'Guinea', 1),
(94, 'Guyana', 1),
(95, 'Haiti', 1),
(96, 'Heard and McDonald Isls', 1),
(97, 'Honduras', 1),
(98, 'Hong Kong', 1),
(99, 'Hungary', 1),
(100, 'Iceland', 1),
(101, 'India', 1),
(102, 'Iran', 1),
(103, 'Iraq', 1),
(104, 'Ireland', 1),
(105, 'Isle of Man', 1),
(106, 'Israel', 1),
(107, 'Italy', 1),
(108, 'Ivory Coast', 1),
(109, 'Jamaica', 1),
(110, 'Japan', 1),
(111, 'Jersey Island', 1),
(112, 'Jordan', 1),
(113, 'Kazakhstan', 1),
(242, 'United States', 1),
(115, 'Kiribati', 1),
(116, 'Kuwait', 1),
(117, 'Laos', 1),
(118, 'Latvia', 1),
(119, 'Lebanon', 1),
(120, 'Lesotho', 1),
(121, 'Liberia', 1),
(122, 'Libya', 1),
(123, 'Liechtenstein', 1),
(124, 'Lithuania', 1),
(125, 'Luxembourg', 1),
(126, 'Macao', 1),
(127, 'Macedonia', 1),
(128, 'Madagascar', 1),
(129, 'Malawi', 1),
(130, 'Malaysia', 1),
(131, 'Maldives', 1),
(132, 'Mali', 1),
(133, 'Malta', 1),
(134, 'Martinique (French)', 1),
(135, 'Mauritania', 1),
(136, 'Mauritius', 1),
(137, 'Mayotte', 1),
(139, 'Micronesia', 1),
(140, 'Moldavia', 1),
(141, 'Monaco', 1),
(142, 'Mongolia', 1),
(143, 'Montenegro', 1),
(144, 'Montserrat', 1),
(145, 'Morocco', 1),
(146, 'Mozambique', 1),
(147, 'Myanmar', 1),
(148, 'Namibia', 1),
(149, 'Nauru', 1),
(150, 'Nepal', 1),
(151, 'Netherlands Antilles', 1),
(152, 'Netherlands', 1),
(153, 'New Caledonia (French)', 1),
(154, 'New Zealand', 1),
(155, 'Nicaragua', 1),
(156, 'Niger', 1),
(157, 'Niue', 1),
(158, 'Norfolk Island', 1),
(159, 'North Korea', 1),
(160, 'Northern Ireland', 1),
(161, 'Norway', 1),
(162, 'Oman', 1),
(163, 'Pakistan', 1),
(164, 'Panama', 1),
(165, 'Papua New Guinea', 1),
(166, 'Paraguay', 1),
(167, 'Peru', 1),
(168, 'Philippines', 1),
(169, 'Pitcairn Island', 1),
(170, 'Poland', 1),
(171, 'Polynesia (French)', 1),
(172, 'Portugal', 1),
(173, 'Qatar', 1),
(174, 'Reunion Island', 1),
(175, 'Romania', 1),
(176, 'Russia', 1),
(4, 'Rwanda', 1),
(178, 'S.Georgia Sandwich Isls', 1),
(179, 'Sao Tome, Principe', 1),
(180, 'San Marino', 1),
(181, 'Saudi Arabia', 1),
(182, 'Scotland', 1),
(183, 'Senegal', 1),
(184, 'Serbia', 1),
(185, 'Seychelles', 1),
(186, 'Shetland', 1),
(187, 'Sierra Leone', 1),
(188, 'Singapore', 1),
(189, 'Slovak Republic', 1),
(190, 'Slovenia', 1),
(191, 'Solomon Islands', 1),
(192, 'Somalia', 1),
(193, 'South Africa', 1),
(194, 'South Korea', 1),
(195, 'Spain', 1),
(196, 'Sri Lanka', 1),
(197, 'St. Helena', 1),
(198, 'St. Lucia', 1),
(199, 'St. Pierre Miquelon', 1),
(200, 'St. Martins', 1),
(201, 'St. Kitts Nevis Anguilla', 1),
(202, 'St. Vincent Grenadines', 1),
(203, 'Sudan', 1),
(204, 'Suriname', 1),
(205, 'Svalbard Jan Mayen', 1),
(206, 'Swaziland', 1),
(207, 'Sweden', 1),
(208, 'Switzerland', 1),
(209, 'Syria', 1),
(210, 'Tajikistan', 1),
(211, 'Taiwan', 1),
(212, 'Tahiti', 1),
(3, 'Tanzania', 1),
(214, 'Thailand', 1),
(215, 'Togo', 1),
(216, 'Tokelau', 1),
(217, 'Tonga', 1),
(218, 'Trinidad and Tobago', 1),
(219, 'Tunisia', 1),
(220, 'Turkmenistan', 1),
(221, 'Turks and Caicos Isls', 1),
(222, 'Tuvalu', 1),
(2, 'Uganda', 1),
(224, 'Ukraine', 1),
(225, 'United Arab Emirates', 1),
(226, 'Uruguay', 1),
(227, 'Uzbekistan', 1),
(228, 'Vanuatu', 1),
(229, 'Vatican City State', 1),
(230, 'Venezuela', 1),
(231, 'Vietnam', 1),
(232, 'Virgin Islands (Brit)', 1),
(233, 'Wales', 1),
(234, 'Wallis Futuna Islands', 1),
(235, 'Western Sahara', 1),
(236, 'Western Samoa', 1),
(237, 'Yemen', 1),
(238, 'Yugoslavia', 1),
(239, 'Zaire', 1),
(240, 'Zambia', 1),
(241, 'Zimbabwe', 1),
(247, 'Algeria', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency_lkp`
--

CREATE TABLE IF NOT EXISTS `currency_lkp` (
  `CurrencyId` smallint(5) NOT NULL AUTO_INCREMENT,
  `CurrencyName` varchar(100) NOT NULL,
  `CurrencySign` varchar(5) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`CurrencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `CustomerId` int(100) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(30) NOT NULL,
  `LastName` varchar(30) NOT NULL,
  `IdNumber` varchar(30) NOT NULL,
  `NationalityLkp` int(10) NOT NULL,
  `RegionLkp` int(10) DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL,
  `Dob` date DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `Phone` varchar(20) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `IsActive` enum('1','0') NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`CustomerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_loyalty_points`
--

CREATE TABLE IF NOT EXISTS `customer_loyalty_points` (
  `LoyaltyId` int(100) NOT NULL AUTO_INCREMENT,
  `CustomerLkp` int(100) NOT NULL,
  `Points` int(100) NOT NULL DEFAULT '0',
  `PointsDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`LoyaltyId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `customer_loyalty_points`
--

INSERT INTO `customer_loyalty_points` (`LoyaltyId`, `CustomerLkp`, `Points`, `PointsDate`, `status`) VALUES
(1, 234567, 18, '2013-02-13 02:44:22', 1),
(2, 0, 40, '2013-04-17 17:44:47', 1),
(3, 0, 40, '2013-04-17 17:44:47', 1),
(4, 0, 40, '2013-04-17 17:57:53', 1),
(5, 0, 40, '2013-04-17 17:57:53', 1),
(6, 0, 40, '2013-04-17 18:01:56', 1),
(7, 0, 40, '2013-04-17 18:01:56', 1),
(8, 0, 40, '2013-04-17 18:03:05', 1),
(9, 0, 40, '2013-04-17 18:03:05', 1),
(10, 0, 40, '2013-04-17 18:04:46', 1),
(11, 0, 40, '2013-04-17 18:04:46', 1),
(12, 0, 40, '2013-04-17 18:08:30', 1),
(13, 0, 40, '2013-04-17 18:08:30', 1),
(16, 0, 36, '2013-04-23 11:46:57', 1),
(17, 0, 36, '2013-04-23 11:46:57', 1),
(18, 0, 36, '2013-04-23 11:46:57', 1),
(19, 0, 12, '2013-04-23 12:17:09', 1),
(20, 0, 12, '2013-04-23 12:19:01', 1),
(21, 0, 12, '2013-04-23 12:19:41', 1),
(22, 0, 12, '2013-04-23 12:20:35', 1),
(23, 0, 12, '2013-04-23 12:21:23', 1),
(24, 0, 12, '2013-04-23 12:22:44', 1),
(25, 0, 12, '2013-04-23 12:23:41', 1),
(26, 0, 28, '2013-04-23 13:07:49', 1),
(27, 0, 28, '2013-04-23 13:07:49', 1),
(28, 0, 24, '2013-04-23 13:12:23', 1),
(29, 0, 24, '2013-04-23 13:12:23', 1),
(30, 0, 12, '2013-04-23 13:17:58', 1),
(31, 0, 36, '2013-04-23 14:41:43', 1),
(32, 0, 36, '2013-04-23 14:41:43', 1),
(33, 0, 36, '2013-04-23 14:41:43', 1),
(34, 0, 12, '2013-05-01 13:38:09', 1),
(35, 0, 12, '2013-05-01 13:40:58', 1),
(36, 0, 36, '2013-05-02 15:09:59', 1),
(37, 0, 36, '2013-05-02 15:09:59', 1),
(38, 0, 36, '2013-05-02 15:09:59', 1),
(39, 0, 2, '2013-05-06 22:58:58', 1),
(40, 0, 17, '2013-05-07 23:45:12', 1),
(41, 0, 17, '2013-05-11 14:42:35', 1),
(42, 0, 17, '2013-05-11 14:43:14', 1),
(43, 0, 17, '2013-05-11 14:43:45', 1),
(44, 0, 17, '2013-05-11 14:50:47', 1),
(45, 0, 17, '2013-05-11 15:07:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `demo_user_address`
--

CREATE TABLE IF NOT EXISTS `demo_user_address` (
  `uadd_id` int(11) NOT NULL AUTO_INCREMENT,
  `uadd_uacc_fk` int(11) NOT NULL,
  `uadd_alias` varchar(50) NOT NULL,
  `uadd_recipient` varchar(100) NOT NULL,
  `uadd_phone` varchar(25) NOT NULL,
  `uadd_company` varchar(75) NOT NULL,
  `uadd_address_01` varchar(100) NOT NULL,
  `uadd_address_02` varchar(100) NOT NULL,
  `uadd_city` varchar(50) NOT NULL,
  `uadd_county` varchar(50) NOT NULL,
  `uadd_post_code` varchar(25) NOT NULL,
  `uadd_country` varchar(50) NOT NULL,
  PRIMARY KEY (`uadd_id`),
  UNIQUE KEY `uadd_id` (`uadd_id`),
  KEY `uadd_uacc_fk` (`uadd_uacc_fk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `demo_user_address`
--

INSERT INTO `demo_user_address` (`uadd_id`, `uadd_uacc_fk`, `uadd_alias`, `uadd_recipient`, `uadd_phone`, `uadd_company`, `uadd_address_01`, `uadd_address_02`, `uadd_city`, `uadd_county`, `uadd_post_code`, `uadd_country`) VALUES
(1, 4, 'Home', 'Joe Public', '0123456789', '', '123', '', 'My City', 'My County', 'My Post Code', 'My Country'),
(2, 4, 'Work', 'Joe Public', '0123456789', 'Flexi', '321', '', 'My Work City', 'My Work County', 'My Work Post Code', 'My Work Country');

-- --------------------------------------------------------

--
-- Table structure for table `demo_user_profiles`
--

CREATE TABLE IF NOT EXISTS `demo_user_profiles` (
  `upro_id` int(11) NOT NULL AUTO_INCREMENT,
  `upro_uacc_fk` int(11) NOT NULL,
  `upro_company` varchar(50) NOT NULL,
  `upro_first_name` varchar(50) NOT NULL,
  `upro_last_name` varchar(50) NOT NULL,
  `upro_phone` varchar(25) NOT NULL,
  `upro_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`upro_id`),
  UNIQUE KEY `upro_id` (`upro_id`),
  KEY `upro_uacc_fk` (`upro_uacc_fk`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `demo_user_profiles`
--

INSERT INTO `demo_user_profiles` (`upro_id`, `upro_uacc_fk`, `upro_company`, `upro_first_name`, `upro_last_name`, `upro_phone`, `upro_newsletter`) VALUES
(1, 1, '', 'John', 'Admin', '07235345454', 0),
(2, 2, '', 'Jim', 'Moderator', '0123465798', 0),
(3, 3, '', 'Joe', 'Public', '0123456789', 0),
(4, 4, '', 'Marcel', 'Auja', '0710228024', 0),
(6, 6, '', 'Meshack', 'Alloys', '0202678128', 0);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `Disc_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Disc_Name` varchar(30) NOT NULL,
  `Percent_or_Amount` int(1) NOT NULL COMMENT '1 for amount, 2 for percent',
  `Disc_Amountke` double(10,2) NOT NULL,
  `Disc_Amountug` double(10,2) NOT NULL DEFAULT '0.00',
  `Disc_Amounttz` double(10,2) NOT NULL DEFAULT '0.00',
  `Disc_Desc` varchar(300) NOT NULL,
  `Disc_From_Date` date DEFAULT NULL,
  `Disc_To_Date` date DEFAULT NULL,
  `User` int(11) NOT NULL,
  `disc_type_id` int(3) DEFAULT NULL,
  `branch_values` text,
  `seat_values` text,
  `day_values` text,
  `time_values` text,
  `disc_threshold` double DEFAULT NULL,
  `If_Other_Discount_Active` int(1) NOT NULL DEFAULT '1',
  `If_Other_Season_Active` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Disc_Id`),
  KEY `Disc_Id` (`Disc_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`Disc_Id`, `Disc_Name`, `Percent_or_Amount`, `Disc_Amountke`, `Disc_Amountug`, `Disc_Amounttz`, `Disc_Desc`, `Disc_From_Date`, `Disc_To_Date`, `User`, `disc_type_id`, `branch_values`, `seat_values`, `day_values`, `time_values`, `disc_threshold`, `If_Other_Discount_Active`, `If_Other_Season_Active`, `status`) VALUES
(2, 'Easter', 1, 200.00, 0.00, 0.00, 'easter', '2013-04-01', '2013-06-30', 1, 1, '1,2,3,4,5,', '1,2,3,5,6,', NULL, NULL, NULL, 1, 1, 1),
(3, 'Labor Day Special', 1, 100.00, 0.00, 0.00, 'labour day special', '2013-04-23', '2013-04-30', 4, 1, '1,2,3,4,5,', '37,38,39,40,41,', NULL, NULL, NULL, 1, 1, 1),
(4, 'Day Based Discount', 1, 400.00, 0.00, 1000.00, 'Day Based Discount', '2013-05-10', '2013-05-31', 1, 2, NULL, NULL, 'Tuesday,Saturday,', NULL, NULL, 1, 1, 1),
(5, 'Time Based Discount', 1, 300.00, 0.00, 1000.00, 'Time Based Discount', '0000-00-00', '0000-00-00', 1, 3, NULL, NULL, NULL, '13,14,15,', NULL, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hirers`
--

CREATE TABLE IF NOT EXISTS `hirers` (
  `HirerId` bigint(20) NOT NULL AUTO_INCREMENT,
  `HirerName` varchar(100) NOT NULL,
  `HirerPhone` varchar(20) NOT NULL,
  `HirerAddress` text NOT NULL,
  `HirerEmail` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`HirerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `hirers`
--

INSERT INTO `hirers` (`HirerId`, `HirerName`, `HirerPhone`, `HirerAddress`, `HirerEmail`, `status`) VALUES
(1, 'Marcel', '03444444444', '444 Msa', 'mmrr@mmm.com', 1),
(2, 'Jackson Siele', '04455555', '75 Bomet', 'kosgeywes@yahoo.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_title_lkp`
--

CREATE TABLE IF NOT EXISTS `job_title_lkp` (
  `JobTitleId` int(10) NOT NULL AUTO_INCREMENT,
  `JobTitle` varchar(100) NOT NULL,
  `JobDescription` text,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`JobTitleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `job_title_lkp`
--

INSERT INTO `job_title_lkp` (`JobTitleId`, `JobTitle`, `JobDescription`, `status`) VALUES
(1, 'Driver', NULL, 1),
(2, 'Conductor', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `loyalty_points`
--

CREATE TABLE IF NOT EXISTS `loyalty_points` (
  `PointsId` int(20) NOT NULL AUTO_INCREMENT,
  `TicketId` bigint(20) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `Point` int(11) NOT NULL DEFAULT '0',
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PointsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_lkp`
--

CREATE TABLE IF NOT EXISTS `maintenance_lkp` (
  `TypeId` int(2) NOT NULL AUTO_INCREMENT,
  `MaintenanceType` varchar(100) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `maintenance_lkp`
--

INSERT INTO `maintenance_lkp` (`TypeId`, `MaintenanceType`, `AddedBy`, `status`) VALUES
(1, 'Regular', NULL, 1),
(2, 'Breaddown', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `makes_lkp`
--

CREATE TABLE IF NOT EXISTS `makes_lkp` (
  `MakeId` smallint(3) NOT NULL AUTO_INCREMENT,
  `MakeName` varchar(50) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`MakeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `makes_lkp`
--

INSERT INTO `makes_lkp` (`MakeId`, `MakeName`, `AddedBy`, `status`) VALUES
(1, 'Scania', NULL, 1),
(2, 'Nissan Diesel', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `m_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `m_header` varchar(300) NOT NULL,
  `m_message` longtext NOT NULL,
  `m_date` date NOT NULL,
  `m_time` time NOT NULL,
  `m_senderid` int(11) NOT NULL,
  `m_recipientid` int(11) NOT NULL,
  `m_isread` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `m_header`, `m_message`, `m_date`, `m_time`, `m_senderid`, `m_recipientid`, `m_isread`) VALUES
(1, 'testing messages', 'testing messages testing messages testing messages testing messages', '2013-01-21', '10:00:00', 1, 1, 1),
(4, 'hey again', '&lt;p&gt;&lt;strong&gt;wazzzzup&lt;/strong&gt;&lt;/p&gt;', '2013-01-21', '14:30:10', 2, 1, 1),
(3, 'hey', '&lt;p&gt;hallo&lt;/p&gt;', '2013-01-21', '14:22:53', 2, 1, 1),
(5, 'hey again', '&lt;p&gt;&lt;strong&gt;wazzzzup&lt;/strong&gt;&lt;/p&gt;', '2013-01-21', '14:31:52', 2, 1, 1),
(6, 'hey again', '&lt;p&gt;&lt;strong&gt;wazzzzup&lt;/strong&gt;&lt;/p&gt;', '2013-01-21', '14:34:30', 2, 1, 1),
(7, 'hi', '&lt;p&gt;trying new message&lt;/p&gt;', '2013-05-02', '11:39:58', 1, 4, 1),
(8, 'MEmo', '&lt;p&gt;tyr memo&lt;/p&gt;', '2013-05-05', '19:42:52', 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `message_recipients`
--

CREATE TABLE IF NOT EXISTS `message_recipients` (
  `MR_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MR_MID` bigint(20) NOT NULL,
  `MR_RID` bigint(20) NOT NULL,
  PRIMARY KEY (`MR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `message_recipients`
--

INSERT INTO `message_recipients` (`MR_ID`, `MR_MID`, `MR_RID`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 4, 1),
(4, 4, 3),
(5, 5, 1),
(6, 5, 3),
(7, 6, 1),
(8, 6, 3),
(9, 7, 4),
(10, 8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `models_lkp`
--

CREATE TABLE IF NOT EXISTS `models_lkp` (
  `Modeld` smallint(3) NOT NULL AUTO_INCREMENT,
  `ModelName` varchar(50) NOT NULL,
  `ModelDescription` varchar(50) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Modeld`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_trans`
--

CREATE TABLE IF NOT EXISTS `mpesa_trans` (
  `mpesa_id` int(11) NOT NULL AUTO_INCREMENT,
  `empTras_id` varchar(15) NOT NULL,
  `empesa_account_no` varchar(30) NOT NULL,
  `empesa_amount` double NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `empesa_date` date NOT NULL,
  `empesa_time` varchar(20) NOT NULL,
  `sender_phone` varchar(20) NOT NULL,
  `mpesa_balance` double NOT NULL,
  `date_received` date NOT NULL,
  `time_received` time NOT NULL,
  `trans_status` int(4) NOT NULL DEFAULT '0' COMMENT '0=received, 1=processed, 2=reversed,',
  `payment_method` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`mpesa_id`),
  UNIQUE KEY `empTras_id` (`empTras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `mpesa_trans`
--

INSERT INTO `mpesa_trans` (`mpesa_id`, `empTras_id`, `empesa_account_no`, `empesa_amount`, `sender_name`, `empesa_date`, `empesa_time`, `sender_phone`, `mpesa_balance`, `date_received`, `time_received`, `trans_status`, `payment_method`, `status`) VALUES
(128, 'qwerty', '123445', 1200, 'wesley', '2013-04-16', '13:00', '0728401698', 0, '2013-04-16', '13:00:00', 0, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities_lkp`
--

CREATE TABLE IF NOT EXISTS `nationalities_lkp` (
  `NationalityId` smallint(3) NOT NULL AUTO_INCREMENT,
  `Nationality` varchar(50) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`NationalityId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `nationalities_lkp`
--

INSERT INTO `nationalities_lkp` (`NationalityId`, `Nationality`, `AddedBy`, `status`) VALUES
(1, 'Kenya', 1, 1),
(2, 'Uganda', 1, 1),
(3, 'Tanzania', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notificationsid` bigint(20) NOT NULL AUTO_INCREMENT,
  `notificationstitle` varchar(30) NOT NULL,
  `notificationsURL` text NOT NULL,
  `notificationsdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usergroupid` int(11) NOT NULL,
  `isread` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `readeruserid` int(11) DEFAULT NULL,
  PRIMARY KEY (`notificationsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `PaymentId` int(50) NOT NULL AUTO_INCREMENT,
  `CustomerLkp` int(100) NOT NULL,
  `Amount` double(10,2) NOT NULL,
  `PaidDate` datetime NOT NULL,
  `PaymentLkp` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PaymentId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments_type_lkp`
--

CREATE TABLE IF NOT EXISTS `payments_type_lkp` (
  `PaymentTypeId` smallint(3) NOT NULL AUTO_INCREMENT,
  `PaymentType` varchar(50) NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PaymentTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paymodes`
--

CREATE TABLE IF NOT EXISTS `paymodes` (
  `paymodes_id` int(11) NOT NULL AUTO_INCREMENT,
  `paymodes` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`paymodes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `paymodes`
--

INSERT INTO `paymodes` (`paymodes_id`, `paymodes`, `status`) VALUES
(1, 'Cash', 1),
(2, 'Loyalty', 1),
(3, 'Voucher', 1),
(4, 'Mpesa', 1),
(5, 'Credit Card', 1);

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE IF NOT EXISTS `petty_cash` (
  `PettyCashId` bigint(20) NOT NULL AUTO_INCREMENT,
  `StationId` int(11) NOT NULL,
  `SubstationId` int(11) NOT NULL,
  `CashDate` varchar(20) NOT NULL,
  `CashierId` int(11) NOT NULL,
  `AccountCodeId` int(11) NOT NULL,
  `Description` text,
  `IssuedTo` int(11) NOT NULL,
  `Particulars` text NOT NULL,
  `AuthorizedBy` bigint(11) NOT NULL,
  `Currency` int(5) DEFAULT NULL,
  `KeAmount` double NOT NULL,
  `UgAmount` double DEFAULT NULL,
  `TzAmount` double DEFAULT NULL,
  `ReceiptNumber` varchar(40) DEFAULT 'pending',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PettyCashId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `petty_cash`
--

INSERT INTO `petty_cash` (`PettyCashId`, `StationId`, `SubstationId`, `CashDate`, `CashierId`, `AccountCodeId`, `Description`, `IssuedTo`, `Particulars`, `AuthorizedBy`, `Currency`, `KeAmount`, `UgAmount`, `TzAmount`, `ReceiptNumber`, `status`) VALUES
(4, 2, 3, '2013-04-15', 2, 1, 'Painting', 2, 'Painting', 2, NULL, 1200, 0, 0, 'pending', 1),
(5, 2, 3, '2013-04-15', 2, 2, 'Tea', 2, 'Tea', 1, 1, 500, NULL, NULL, 'pending', 1);

-- --------------------------------------------------------

--
-- Table structure for table `points_burn`
--

CREATE TABLE IF NOT EXISTS `points_burn` (
  `points_burn_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kes` double(20,2) NOT NULL,
  `ugx` double(20,2) NOT NULL,
  `tzs` double(20,2) NOT NULL,
  `points` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`points_burn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `points_burn`
--

INSERT INTO `points_burn` (`points_burn_id`, `kes`, `ugx`, `tzs`, `points`, `user_id`, `status`, `dateadded`) VALUES
(1, 2.00, 35.00, 25.00, 1, 1, 1, '2013-02-13 03:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `point_gain`
--

CREATE TABLE IF NOT EXISTS `point_gain` (
  `point_gain_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kes` double(20,2) NOT NULL DEFAULT '0.00',
  `ugx` double(20,2) NOT NULL DEFAULT '0.00',
  `tzs` double(20,2) NOT NULL DEFAULT '0.00',
  `points` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`point_gain_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `point_gain`
--

INSERT INTO `point_gain` (`point_gain_id`, `kes`, `ugx`, `tzs`, `points`, `user_id`, `status`, `dateadded`) VALUES
(1, 100.00, 20000.00, 18000.00, 1, 1, 1, '2013-02-13 02:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `regions_lkp`
--

CREATE TABLE IF NOT EXISTS `regions_lkp` (
  `RegionId` int(100) NOT NULL AUTO_INCREMENT,
  `RegionName` varchar(100) NOT NULL,
  `RegionCode` varchar(20) DEFAULT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`RegionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `regions_lkp`
--

INSERT INTO `regions_lkp` (`RegionId`, `RegionName`, `RegionCode`, `AddedBy`, `status`) VALUES
(1, 'Kenya', 'Ke', 0, 1),
(2, 'Uganda', 'Ug', 0, 1),
(3, 'Tanzania', 'Tz', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reporting_departure_times_lkp`
--

CREATE TABLE IF NOT EXISTS `reporting_departure_times_lkp` (
  `TimeId` smallint(3) NOT NULL AUTO_INCREMENT,
  `ArrivalDeparture` time NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TimeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `reporting_departure_times_lkp`
--

INSERT INTO `reporting_departure_times_lkp` (`TimeId`, `ArrivalDeparture`, `AddedBy`, `status`) VALUES
(1, '01:00:00', 2, 1),
(2, '01:15:00', 2, 1),
(3, '01:20:00', 2, 1),
(4, '01:25:00', 2, 1),
(5, '01:30:00', 2, 1),
(6, '01:35:00', 2, 1),
(7, '01:40:00', 2, 1),
(8, '01:45:00', 2, 1),
(9, '01:50:00', 0, 1),
(10, '01:55:00', 0, 1),
(11, '02:00:00', 2, 1),
(12, '02:05:00', 2, 1),
(13, '18:00:00', 1, 1),
(14, '21:00:00', 1, 1),
(15, '21:30:00', 1, 1),
(16, '16:00:00', 1, 1),
(17, '18:00:00', 1, 1),
(18, '18:00:00', 1, 1),
(19, '21:00:00', 1, 1),
(20, '21:30:00', 1, 1),
(21, '01:00:00', 1, 1),
(22, '01:30:00', 1, 1),
(23, '14:00:00', 1, 1),
(24, '14:30:00', 1, 1),
(25, '18:00:00', 1, 1),
(26, '18:00:00', 1, 1),
(27, '21:00:00', 1, 1),
(28, '21:30:00', 1, 1),
(29, '01:00:00', 1, 1),
(30, '01:30:00', 1, 1),
(31, '14:00:00', 1, 1),
(32, '14:30:00', 1, 1),
(33, '06:00:00', 1, 1),
(34, '10:00:00', 1, 1),
(35, '13:00:00', 1, 1),
(36, '13:15:00', 1, 1),
(37, '19:00:00', 1, 1),
(38, '18:00:00', 1, 1),
(39, '18:00:00', 1, 1),
(40, '21:00:00', 1, 1),
(41, '21:30:00', 1, 1),
(42, '01:00:00', 1, 1),
(43, '01:30:00', 1, 1),
(44, '14:00:00', 1, 1),
(45, '14:30:00', 1, 1),
(46, '16:30:00', 1, 1),
(47, '18:15:00', 1, 1),
(48, '11:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `ReservationId` int(100) NOT NULL AUTO_INCREMENT,
  `CustomerLkp` int(100) NOT NULL,
  `BusLkp` int(20) NOT NULL,
  `SourceLkp` int(20) NOT NULL,
  `DestinationLkp` int(20) NOT NULL,
  `SeatLkp` int(10) NOT NULL,
  `SeatNumber` int(11) NOT NULL,
  `ReservationDate` datetime NOT NULL,
  `TravelDate` date NOT NULL,
  `TravelTime` time NOT NULL,
  `ReservationStatus` varchar(50) NOT NULL DEFAULT 'Pending',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ReservationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `return_discount`
--

CREATE TABLE IF NOT EXISTS `return_discount` (
  `return_discount_id` bigint(20) NOT NULL,
  `percent` double(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `routecodes_lkp`
--

CREATE TABLE IF NOT EXISTS `routecodes_lkp` (
  `RouteCodeId` smallint(5) NOT NULL AUTO_INCREMENT,
  `RouteCode` varchar(100) NOT NULL,
  `RouteName` varchar(100) NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`RouteCodeId`),
  KEY `RouteCode` (`RouteCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `routecodes_lkp`
--

INSERT INTO `routecodes_lkp` (`RouteCodeId`, `RouteCode`, `RouteName`, `AddedBy`, `status`) VALUES
(1, 'MSA-KSM', 'Mombasa-Kisumu', 0, 1),
(2, 'NRB-KPL', 'Nairobi-Kampala', 0, 1),
(3, 'NRB-MSA', 'Nairobi-Mombasa', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `route_arrival_departure`
--

CREATE TABLE IF NOT EXISTS `route_arrival_departure` (
  `RouteId` bigint(20) NOT NULL,
  `TerminusId` int(11) NOT NULL,
  `ArrivalTime` int(11) NOT NULL,
  `DepartureTime` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `route_arrival_departure`
--

INSERT INTO `route_arrival_departure` (`RouteId`, `TerminusId`, `ArrivalTime`, `DepartureTime`, `status`) VALUES
(40, 1, 1, 2, 1),
(40, 2, 3, 4, 1),
(40, 3, 5, 6, 1),
(41, 1, 2, 3, 1),
(41, 2, 4, 5, 1),
(41, 3, 6, 7, 1),
(41, 4, 8, 9, 1),
(42, 1, 2, 3, 1),
(42, 2, 4, 5, 1),
(42, 3, 9, 11, 1),
(43, 2, 1, 3, 1),
(44, 1, 2, 3, 1),
(45, 1, 1, 1, 1),
(45, 6, 2, 3, 1),
(45, 2, 4, 5, 1),
(45, 3, 9, 9, 1),
(47, 2, 38, 39, 1),
(47, 3, 40, 41, 1),
(47, 4, 42, 43, 1),
(47, 5, 16, 16, 1),
(49, 1, 34, 34, 1),
(49, 6, 35, 36, 1),
(49, 2, 37, 37, 1),
(50, 5, 34, 34, 1),
(50, 4, 16, 46, 1),
(50, 3, 13, 47, 1),
(50, 2, 48, 48, 1);

-- --------------------------------------------------------

--
-- Table structure for table `route_prices`
--

CREATE TABLE IF NOT EXISTS `route_prices` (
  `PriceId` int(100) NOT NULL AUTO_INCREMENT,
  `FareCode` varchar(50) DEFAULT NULL,
  `RouteFrom` int(11) NOT NULL,
  `RouteTo` int(11) NOT NULL,
  `BusTypeLkp` smallint(3) NOT NULL,
  `SeatLkp` int(20) NOT NULL,
  `SeasonLkp` int(20) DEFAULT NULL,
  `KeAmount` double(10,2) NOT NULL,
  `UgAmount` double(10,2) NOT NULL,
  `TzAmount` double(10,2) DEFAULT NULL,
  `DateAdded` datetime NOT NULL,
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PriceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=162 ;

--
-- Dumping data for table `route_prices`
--

INSERT INTO `route_prices` (`PriceId`, `FareCode`, `RouteFrom`, `RouteTo`, `BusTypeLkp`, `SeatLkp`, `SeasonLkp`, `KeAmount`, `UgAmount`, `TzAmount`, `DateAdded`, `AddedBy`, `status`) VALUES
(4, 'NRB-MSA11:00PM', 0, 0, 1, 1, 1, 0.00, 0.00, 0.00, '2013-05-06 13:26:56', 1, 1),
(5, 'NRB-MSA11:00PM', 0, 0, 1, 1, 1, 0.00, 0.00, 0.00, '2013-05-06 13:27:33', 1, 1),
(6, 'NRB-MSA11:00PM', 2, 1, 1, 1, 1, 2300.00, 0.00, 44000.00, '2013-05-09 19:21:54', 1, 1),
(7, 'MSA-NRB11:00PM', 0, 0, 1, 1, 1, 0.00, 0.00, 0.00, '2013-05-06 13:26:56', 1, 1),
(8, 'MSA-NRB11:00PM', 0, 0, 1, 1, 1, 0.00, 0.00, 0.00, '2013-05-06 13:27:33', 1, 1),
(9, 'MSA-NRB11:00PM', 1, 2, 1, 1, 1, 2300.00, 0.00, 44000.00, '2013-05-09 19:21:54', 1, 1),
(10, 'NRB-KAM', 5, 2, 2, 1, 1, 3000.00, 60000.00, 45000.00, '2013-05-07 15:18:14', 1, 1),
(11, 'NRB-KAMF', 2, 5, 1, 2, 1, 1800.00, 56000.00, 42000.00, '2013-05-05 12:32:42', 1, 1),
(12, 'NRB-KAMB', 2, 5, 1, 3, 1, 1500.00, 50000.00, 38000.00, '2013-05-05 12:33:07', 1, 1),
(13, NULL, 2, 1, 3, 1, NULL, 1600.00, 0.00, 31000.00, '2013-05-07 15:36:15', 1, 1),
(14, NULL, 2, 1, 1, 2, NULL, 2100.00, 0.00, 40000.00, '2013-05-09 19:23:15', 1, 1),
(15, NULL, 2, 1, 1, 3, NULL, 1700.00, 0.00, 32000.00, '2013-05-07 15:15:17', 1, 1),
(16, NULL, 2, 1, 2, 1, NULL, 1600.00, 0.00, 31000.00, '2013-05-07 15:18:14', 1, 1),
(17, NULL, 3, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(18, NULL, 4, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(19, NULL, 5, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(20, NULL, 6, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(21, NULL, 7, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(22, NULL, 8, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(23, NULL, 3, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(24, NULL, 4, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(25, NULL, 6, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(26, NULL, 7, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(27, NULL, 8, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(28, NULL, 4, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(29, NULL, 5, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(30, NULL, 6, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(31, NULL, 7, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(32, NULL, 8, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(33, NULL, 5, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(34, NULL, 6, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(35, NULL, 7, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(36, NULL, 8, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(37, NULL, 6, 5, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(38, NULL, 7, 5, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(39, NULL, 8, 5, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(40, NULL, 7, 6, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(41, NULL, 8, 6, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(42, NULL, 8, 7, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(43, NULL, 2, 1, 2, 3, NULL, 1300.00, 0.00, 25000.00, '2013-05-07 15:19:41', 1, 1),
(44, NULL, 3, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(45, NULL, 4, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(46, NULL, 5, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(47, NULL, 6, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(48, NULL, 7, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(49, NULL, 8, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(50, NULL, 3, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(51, NULL, 4, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(52, NULL, 5, 2, 2, 3, NULL, 2500.00, 42000.00, 36000.00, '2013-05-07 15:19:41', 1, 1),
(53, NULL, 6, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(54, NULL, 7, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(55, NULL, 8, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(56, NULL, 4, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(57, NULL, 5, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(58, NULL, 6, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(59, NULL, 7, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(60, NULL, 8, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(61, NULL, 5, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(62, NULL, 6, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(63, NULL, 7, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(64, NULL, 8, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(65, NULL, 6, 5, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(66, NULL, 7, 5, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(67, NULL, 8, 5, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(68, NULL, 7, 6, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(69, NULL, 8, 6, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(70, NULL, 8, 7, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(71, NULL, 2, 1, 2, 2, NULL, 1500.00, 0.00, 30000.00, '2013-05-09 20:10:53', 1, 1),
(72, NULL, 3, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(73, NULL, 4, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(74, NULL, 5, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(75, NULL, 6, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(76, NULL, 7, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(77, NULL, 8, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(78, NULL, 3, 2, 2, 2, NULL, 500.00, 0.00, 3000.00, '2013-05-09 20:10:53', 1, 1),
(79, NULL, 4, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(80, NULL, 5, 2, 2, 2, NULL, 2700.00, 45000.00, 38000.00, '2013-05-09 20:10:53', 1, 1),
(81, NULL, 6, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(82, NULL, 7, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(83, NULL, 8, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(84, NULL, 4, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(85, NULL, 5, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(86, NULL, 6, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(87, NULL, 7, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(88, NULL, 8, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(89, NULL, 5, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(90, NULL, 6, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(91, NULL, 7, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(92, NULL, 8, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(93, NULL, 6, 5, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(94, NULL, 7, 5, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(95, NULL, 8, 5, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(96, NULL, 7, 6, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(97, NULL, 8, 6, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(98, NULL, 8, 7, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(99, NULL, 9, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(100, NULL, 10, 1, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(101, NULL, 9, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(102, NULL, 10, 2, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(103, NULL, 9, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(104, NULL, 10, 3, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(105, NULL, 9, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(106, NULL, 10, 4, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(107, NULL, 9, 5, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(108, NULL, 10, 5, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(109, NULL, 9, 6, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(110, NULL, 10, 6, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(111, NULL, 9, 7, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(112, NULL, 10, 7, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(113, NULL, 9, 8, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(114, NULL, 10, 8, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(115, NULL, 10, 9, 2, 1, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:18:14', 1, 1),
(116, NULL, 9, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(117, NULL, 10, 1, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(118, NULL, 9, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(119, NULL, 10, 2, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(120, NULL, 9, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(121, NULL, 10, 3, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(122, NULL, 9, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(123, NULL, 10, 4, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(124, NULL, 9, 5, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(125, NULL, 10, 5, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(126, NULL, 9, 6, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(127, NULL, 10, 6, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(128, NULL, 9, 7, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(129, NULL, 10, 7, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(130, NULL, 9, 8, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(131, NULL, 10, 8, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(132, NULL, 10, 9, 2, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 20:10:53', 1, 1),
(133, NULL, 9, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(134, NULL, 10, 1, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(135, NULL, 9, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(136, NULL, 10, 2, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(137, NULL, 9, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(138, NULL, 10, 3, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(139, NULL, 9, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(140, NULL, 10, 4, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(141, NULL, 9, 5, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(142, NULL, 10, 5, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(143, NULL, 9, 6, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(144, NULL, 10, 6, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(145, NULL, 9, 7, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(146, NULL, 10, 7, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(147, NULL, 9, 8, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(148, NULL, 10, 8, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(149, NULL, 10, 9, 2, 3, NULL, 0.00, 0.00, 0.00, '2013-05-07 15:19:41', 1, 1),
(150, NULL, 10, 1, 3, 1, NULL, 1700.00, 0.00, 32000.00, '2013-05-07 15:36:15', 1, 1),
(151, NULL, 10, 2, 3, 1, NULL, 3300.00, 0.00, 64000.00, '2013-05-07 15:36:15', 1, 1),
(152, NULL, 2, 1, 3, 2, NULL, 1500.00, 0.00, 30000.00, '2013-05-07 15:50:26', 1, 1),
(153, NULL, 10, 1, 3, 2, NULL, 1500.00, 0.00, 30000.00, '2013-05-07 15:50:26', 1, 1),
(154, NULL, 10, 2, 3, 2, NULL, 3000.00, 0.00, 60000.00, '2013-05-07 15:50:26', 1, 1),
(155, NULL, 2, 1, 3, 3, NULL, 1300.00, 0.00, 25000.00, '2013-05-07 15:53:44', 1, 1),
(156, NULL, 10, 1, 3, 3, NULL, 1300.00, 0.00, 25000.00, '2013-05-07 15:53:44', 1, 1),
(157, NULL, 10, 2, 3, 3, NULL, 2600.00, 0.00, 49000.00, '2013-05-07 15:53:44', 1, 1),
(158, NULL, 6, 1, 1, 1, NULL, 1800.00, 0.00, 33000.00, '2013-05-09 19:21:54', 1, 1),
(159, NULL, 6, 2, 1, 1, NULL, 0.00, 0.00, 0.00, '2013-05-09 19:21:54', 1, 1),
(160, NULL, 6, 1, 1, 2, NULL, 1600.00, 0.00, 31000.00, '2013-05-09 19:23:15', 1, 1),
(161, NULL, 6, 2, 1, 2, NULL, 0.00, 0.00, 0.00, '2013-05-09 19:23:15', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `season_lkp`
--

CREATE TABLE IF NOT EXISTS `season_lkp` (
  `SeasonId` smallint(5) NOT NULL AUTO_INCREMENT,
  `SeasonName` varchar(100) NOT NULL,
  `Description` text,
  `PriceFrom` date NOT NULL,
  `PriceTo` date NOT NULL,
  `PAmountke` double NOT NULL,
  `PAmountug` double NOT NULL,
  `PAmounttz` double NOT NULL,
  `IsActive` int(1) NOT NULL DEFAULT '0',
  `IsDefault` int(1) NOT NULL DEFAULT '0',
  `AddedBy` smallint(3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SeasonId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `season_lkp`
--

INSERT INTO `season_lkp` (`SeasonId`, `SeasonName`, `Description`, `PriceFrom`, `PriceTo`, `PAmountke`, `PAmountug`, `PAmounttz`, `IsActive`, `IsDefault`, `AddedBy`, `status`) VALUES
(1, 'Easter', 'Easter holidays', '2013-03-20', '2013-06-30', -100, -10000, -7000, 0, 1, 1, 1),
(2, 'December', 'December Holidays Season', '2013-11-01', '2013-12-31', 200, 0, 0, 0, 0, 1, 1),
(3, 'Valentine Prices', 'Valentine Period prices', '2013-02-01', '2013-02-28', 0, 0, 0, 0, 0, 1, 1),
(4, 'sw', 'qwwq', '2013-05-01', '2013-05-01', 0, 0, 0, 2, 0, 1, 0),
(5, 'Madaraka Day Special', 'Madaraka Day Special', '2013-05-31', '2013-06-02', -200, 0, 0, 0, 0, 1, 0),
(6, 'Jamhuri Day Peaks', 'Jamhuri Day Peaks', '2013-10-10', '2013-05-31', 500, 10000, 6000, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE IF NOT EXISTS `seats` (
  `seat_id` int(11) NOT NULL AUTO_INCREMENT,
  `BusId` int(11) NOT NULL,
  `BusTypeId` int(3) NOT NULL,
  `seat_no` varchar(20) NOT NULL,
  `SeatPositionLkp` int(11) NOT NULL,
  `SeatTypeId` int(3) NOT NULL,
  `IsDiscounted` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`seat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`seat_id`, `BusId`, `BusTypeId`, `seat_no`, `SeatPositionLkp`, `SeatTypeId`, `IsDiscounted`, `status`) VALUES
(118, 3, 1, '8', 2, 3, 0, 1),
(117, 3, 1, '7', 1, 3, 0, 1),
(116, 3, 1, '6', 3, 2, 0, 1),
(115, 3, 1, '5', 3, 2, 0, 1),
(114, 3, 1, '4', 3, 1, 0, 1),
(113, 3, 1, '3', 4, 2, 0, 1),
(112, 3, 1, '2', 1, 2, 0, 1),
(157, 3, 1, '1', 1, 1, 0, 1),
(119, 3, 1, '9', 1, 3, 0, 1),
(120, 3, 1, '10', 2, 3, 0, 1),
(121, 3, 1, '11', 4, 3, 0, 1),
(122, 3, 1, '12', 3, 3, 0, 1),
(123, 3, 1, '13', 1, 3, 0, 1),
(124, 3, 1, '14', 2, 3, 0, 1),
(125, 3, 1, '15', 4, 3, 0, 1),
(126, 3, 1, '16', 3, 3, 0, 1),
(127, 3, 1, '17', 1, 3, 0, 1),
(128, 3, 1, '18', 2, 3, 0, 1),
(129, 3, 1, '19', 4, 3, 0, 1),
(130, 3, 1, '20', 3, 3, 0, 1),
(131, 3, 1, '21', 1, 3, 0, 1),
(132, 3, 1, '22', 2, 3, 0, 1),
(133, 3, 1, '23', 4, 3, 0, 1),
(134, 3, 1, '24', 3, 3, 0, 1),
(135, 3, 1, '25', 1, 3, 0, 1),
(136, 3, 1, '26', 2, 3, 0, 1),
(137, 3, 1, '27', 4, 3, 0, 1),
(138, 3, 1, '28', 3, 3, 0, 1),
(139, 3, 1, '29', 1, 3, 0, 1),
(140, 3, 1, '30', 2, 3, 0, 1),
(141, 3, 1, '31', 4, 3, 0, 1),
(142, 3, 1, '32', 3, 3, 0, 1),
(143, 3, 1, '33', 1, 3, 0, 1),
(144, 3, 1, '34', 2, 3, 0, 1),
(145, 3, 1, '35', 4, 3, 0, 1),
(146, 3, 1, '36', 3, 3, 0, 1),
(147, 3, 1, '37', 1, 3, 0, 1),
(148, 3, 1, '38', 2, 3, 0, 1),
(149, 3, 1, '39', 4, 3, 0, 1),
(150, 3, 1, '40', 3, 3, 0, 1),
(151, 3, 1, '41', 1, 3, 0, 1),
(152, 3, 1, '42', 2, 3, 0, 1),
(153, 3, 1, '43', 5, 3, 0, 1),
(154, 3, 1, '44', 4, 3, 0, 1),
(155, 3, 1, '45', 3, 3, 0, 1),
(158, 3, 0, '46', 2, 3, 0, 1),
(159, 3, 0, '47', 1, 3, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `seats_lkp`
--

CREATE TABLE IF NOT EXISTS `seats_lkp` (
  `SeatTypeId` smallint(3) NOT NULL AUTO_INCREMENT,
  `SeatType` varchar(100) NOT NULL,
  `Description` text,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SeatTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `seats_lkp`
--

INSERT INTO `seats_lkp` (`SeatTypeId`, `SeatType`, `Description`, `AddedBy`, `status`) VALUES
(1, 'VIP', '', 1, 1),
(2, 'First Class', '', 1, 1),
(3, 'Business', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `seat_discounts`
--

CREATE TABLE IF NOT EXISTS `seat_discounts` (
  `seat_discounts_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `seat_id` int(11) NOT NULL,
  `discount_factor` varchar(50) DEFAULT NULL COMMENT '1 for money, 2 for percentage',
  `discount_value` varchar(30) DEFAULT NULL,
  `discount_from` date DEFAULT NULL,
  `discount_to` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`seat_discounts_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `seat_discounts`
--

INSERT INTO `seat_discounts` (`seat_discounts_id`, `seat_id`, `discount_factor`, `discount_value`, `discount_from`, `discount_to`, `status`) VALUES
(4, 0, '1', '100', '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seat_position_lkp`
--

CREATE TABLE IF NOT EXISTS `seat_position_lkp` (
  `SeatPositionLkp` smallint(3) NOT NULL AUTO_INCREMENT,
  `SeatPosition` varchar(50) NOT NULL,
  `SeatAlias` varchar(20) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SeatPositionLkp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `seat_position_lkp`
--

INSERT INTO `seat_position_lkp` (`SeatPositionLkp`, `SeatPosition`, `SeatAlias`, `AddedBy`, `status`) VALUES
(1, 'Left Window', 'LW', NULL, 1),
(2, 'Left Isle', 'LI', NULL, 1),
(3, 'Right window', 'RW', NULL, 1),
(4, 'Right Isle', 'RI', NULL, 1),
(5, 'Isle', 'I', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `SettingId` smallint(5) NOT NULL AUTO_INCREMENT,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` varchar(50) NOT NULL,
  `IsActive` enum('1','0') DEFAULT '0',
  `AddedBy` int(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`SettingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='settings table -  contains things like penalties, etc' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `SMS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SMS_Length` int(11) NOT NULL,
  `SMS_NO_Sent` int(11) NOT NULL,
  `SMS_Recipient` varchar(15) NOT NULL,
  `SMS_Function` varchar(50) NOT NULL,
  `SMS_Date_Sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SMS_Date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticketing`
--

CREATE TABLE IF NOT EXISTS `ticketing` (
  `t_route` varchar(100) DEFAULT NULL,
  `bookdate` date DEFAULT NULL,
  `booktime` varchar(50) DEFAULT NULL,
  `reporttime` varchar(50) DEFAULT NULL,
  `clientname` varchar(100) DEFAULT NULL,
  `t_idno` varchar(100) DEFAULT NULL,
  `seatno` int(11) DEFAULT NULL,
  `amount` float(13,5) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `transdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `t_username` varchar(100) DEFAULT NULL,
  `openbookref` int(11) DEFAULT '0',
  `ticketno` int(11) DEFAULT NULL,
  `t_station` varchar(100) DEFAULT NULL,
  `mobileno` varchar(50) DEFAULT NULL,
  `fromtown` varchar(100) DEFAULT NULL,
  `totown` varchar(100) DEFAULT NULL,
  `routecode` varchar(100) DEFAULT NULL,
  `ticketcount` int(11) DEFAULT '0',
  `ticketstatus` varchar(50) DEFAULT 'Active',
  `redeemed` varchar(50) DEFAULT 'No',
  `sub_station` varchar(100) DEFAULT NULL,
  `cancelled_by` varchar(100) DEFAULT NULL,
  `cancel_reason` varchar(500) DEFAULT NULL,
  `voucherno` int(11) DEFAULT NULL,
  `pick_point` varchar(100) DEFAULT NULL,
  `bus_type` varchar(100) DEFAULT NULL,
  `final_town` varchar(100) DEFAULT NULL,
  `bus_number` varchar(50) DEFAULT NULL,
  `newticketno` varchar(50) DEFAULT NULL,
  `specialstatus` varchar(50) DEFAULT 'NORMAL',
  `TicketId` bigint(20) NOT NULL AUTO_INCREMENT,
  `openticket_charge` float(13,5) DEFAULT '0.00000',
  `open_date` date DEFAULT NULL,
  `btime` date NOT NULL DEFAULT '0000-00-00',
  `cancel_date` date DEFAULT NULL,
  `amountug` float(13,5) DEFAULT '0.00000',
  `openticket_chargeug` float DEFAULT '0',
  `t_currency` varchar(50) DEFAULT NULL,
  `amounttz` float DEFAULT '0',
  `openticket_chargetz` float DEFAULT '0',
  `amountusd` float DEFAULT '0',
  `openticket_chargeusd` float DEFAULT '0',
  `amounteuro` float DEFAULT '0',
  `openticket_chargeeuro` float DEFAULT '0',
  `nationality` varchar(100) DEFAULT NULL,
  `mpesaref` varchar(50) DEFAULT NULL,
  `actual_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `selling_point` varchar(100) DEFAULT 'WEB',
  `barcode` varchar(100) DEFAULT NULL,
  `seat_class` varchar(50) DEFAULT NULL,
  `vouchercode` varchar(50) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `debitcard_no` varchar(100) DEFAULT NULL,
  `debitcard_transref` varchar(100) DEFAULT NULL,
  `channel` int(11) NOT NULL DEFAULT '1',
  `paymode` int(11) NOT NULL DEFAULT '1',
  `t_userid` int(11) NOT NULL DEFAULT '1',
  `schedule_id` int(11) NOT NULL,
  PRIMARY KEY (`TicketId`),
  KEY `seatno` (`seatno`),
  KEY `openbookref` (`openbookref`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1140017 ;

--
-- Dumping data for table `ticketing`
--

INSERT INTO `ticketing` (`t_route`, `bookdate`, `booktime`, `reporttime`, `clientname`, `t_idno`, `seatno`, `amount`, `reg_no`, `transdate`, `t_username`, `openbookref`, `ticketno`, `t_station`, `mobileno`, `fromtown`, `totown`, `routecode`, `ticketcount`, `ticketstatus`, `redeemed`, `sub_station`, `cancelled_by`, `cancel_reason`, `voucherno`, `pick_point`, `bus_type`, `final_town`, `bus_number`, `newticketno`, `specialstatus`, `TicketId`, `openticket_charge`, `open_date`, `btime`, `cancel_date`, `amountug`, `openticket_chargeug`, `t_currency`, `amounttz`, `openticket_chargetz`, `amountusd`, `openticket_chargeusd`, `amounteuro`, `openticket_chargeeuro`, `nationality`, `mpesaref`, `actual_time`, `selling_point`, `barcode`, `seat_class`, `vouchercode`, `status`, `debitcard_no`, `debitcard_transref`, `channel`, `paymode`, `t_userid`, `schedule_id`) VALUES
(NULL, '2013-02-01', '11:43:19', '1', 'wesley kosgey', '34343434', 1, 1740.00000, 'KBU 654A', '2013-02-01 11:43:19', 'John Admin', 0, NULL, '3', '0728401698', '2', '1', 'NRB-MSA11:00PM', 0, 'Inactive', 'No', NULL, 'John Admin', 'Client request', NULL, '5', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139933, 0.00000, NULL, '0000-00-00', '0000-00-00', 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-01 08:43:19', 'WEB', NULL, '1', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-01', '11:51:25', '1', 'Marcel Ogweno', '543455666', 3, 1740.00000, 'KBU 654A', '2013-02-01 11:51:25', 'John Admin', 0, NULL, '3', '0728401765', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, NULL, '3', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139934, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-01 08:51:25', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-01', '16:46:22', '1', 'Meshack Alloys', '12212324', 45, 1740.00000, 'KBU 654A', '2013-02-01 16:46:22', 'John Admin', 0, 123456, '3', '0721778654', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, NULL, '2', '1', '1', 'KBU 654A', NULL, 'NORMAL', 1139936, 0.00000, NULL, '0000-00-00', NULL, 300000.00000, 0, '2', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-01 13:46:22', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-02', '16:47:25', '1', 'Kamuzu Banda', '23242333', 10, 1600.00000, 'KBT 876Y', '2013-02-02 16:47:25', 'John Admin', 0, 123456, '3', '0721778635', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, NULL, '2', '1', '1', 'KBT 876Y', NULL, 'NORMAL', 1139937, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-02 13:47:25', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-12', '19:54:51', '1', 'Picoty Rono', '36543333', 13, 1800.00000, 'KBU 654A', '2013-02-12 19:54:51', 'John Admin', 0, 123456, '3', '0722344533', '2', '1', 'NRB-MSA11:00PM', 0, 'Inactive', 'No', NULL, 'John Admin', 'testing', NULL, '3', '1', '1', 'KBU 654A', NULL, 'NORMAL', 1139938, 0.00000, NULL, '0000-00-00', '2013-02-12', 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-12 16:54:51', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-12', '23:05:44', '1', 'Dorah Mtuwetah', '234556777', 14, 1800.00000, 'KBU 654A', '2013-02-12 23:05:44', 'John Admin', 0, 123456, '3', '0721778657', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, NULL, '3', '1', '1', 'KBU 654A', NULL, 'NORMAL', 1139939, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '4', 0, 0, 20, 0, 0, 0, NULL, NULL, '2013-02-12 20:05:44', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 5, 1, 3),
(NULL, '2013-02-13', '02:13:23', '1', 'John Mututho', '123445678', 12, 1800.00000, 'KBU 654A', '2013-02-13 02:13:23', 'John Admin', 0, NULL, '3', '0723456798', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, NULL, '3', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139940, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, 'DDQ2344', '2013-02-12 23:13:23', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-13', '05:38:36', '1', 'Marcel Auja Ogweno', '11237486', 20, 1800.00000, 'KBU 654A', '2013-02-13 05:38:36', 'John Admin', 0, NULL, '3', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '3', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139941, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, NULL, '2013-02-13 02:38:36', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-02-13', '05:44:22', '1', 'Marcel Auja Ogweno', '11237486', 28, 1800.00000, 'KBU 654A', '2013-02-13 05:44:22', 'John Admin', 0, NULL, '3', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '3', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139942, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-02-13 02:44:22', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:04:46', '1', 'Haroon O2 Butt', '5554535353', 5, 1200.00000, 'KBU 654A', '2013-04-17 21:04:46', 'John Admin', 0, NULL, '0', '0719110786', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139955, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:04:46', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:04:46', '1', 'Marcel Auja Ogweno', '11237486', 4, 1600.00000, 'KBU 654A', '2013-04-17 21:04:46', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139956, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:04:46', 'WEB', NULL, '1', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:04:47', '1', 'wesley kos', '224332434', 9, 1200.00000, 'KBU 654A', '2013-04-17 21:04:47', 'John Admin', 0, NULL, '0', '073444444', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139957, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:04:47', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:08:30', '1', 'Haroon O2 Butt', '5554535353', 5, 1200.00000, 'KBU 654A', '2013-04-17 21:08:30', 'John Admin', 0, NULL, '0', '0719110786', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139958, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:08:30', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:08:30', '1', 'Marcel Auja Ogweno', '11237486', 4, 1600.00000, 'KBU 654A', '2013-04-17 21:08:30', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139959, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:08:30', 'WEB', NULL, '1', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-17', '21:08:30', '1', 'wesley kos', '224332434', 9, 1200.00000, 'KBU 654A', '2013-04-17 21:08:30', 'John Admin', 0, NULL, '0', '073444444', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'KBU 654A', '123456', 'NORMAL', 1139960, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-17 18:08:30', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '14:46:57', '1', 'Haroon O2 Butt', '5554535353', 9, 2400.00000, 'A', '2013-04-23 14:46:57', 'John Admin', 0, NULL, '0', '0719110786', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', '123456', 'NORMAL', 1139964, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 11:46:57', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '14:46:57', '1', 'Marcel Auja Ogweno', '11237486', 2, 1200.00000, 'A', '2013-04-23 14:46:57', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', '123456', 'NORMAL', 1139963, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 11:46:57', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '14:46:57', '04/23/2013 21:00', 'Haroon O2 Butt', '5554535353', 8, 1200.00000, 'A', '2013-04-23 14:46:57', 'John Admin', 0, NULL, '0', '0719110786', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '1', 'A', '123456', 'NORMAL', 1139965, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 11:46:57', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '17:41:43', '1', 'Marcel Auja Ogweno', '11237486', 13, 1200.00000, 'A', '2013-04-23 17:41:43', 'Marcel Auja', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'A', '123456', 'NORMAL', 1139990, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 14:41:43', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '17:41:43', '04/23/2013 22:00', 'Haroon O2 Butt', '5554535353', 33, 1200.00000, 'A', '2013-04-23 17:41:43', 'Marcel Auja', 0, NULL, '0', '0719110786', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '2', 'A', '123456', 'NORMAL', 1139989, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 14:41:43', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '17:41:43', '1', 'Haroon O2 Butt', '5554535353', 6, 1200.00000, 'A', '2013-04-23 17:41:43', 'Marcel Auja', 0, NULL, '0', '0719110786', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'A', '123456', 'NORMAL', 1139988, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 14:41:43', 'WEB', NULL, '2', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '16:12:23', '1', 'Marcel Auja Ogweno', '11237486', 17, 0.00000, 'A', '2013-04-23 16:12:23', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', '123456', 'NORMAL', 1139985, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 13:12:23', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '16:12:23', '04/23/2013 21:00', 'Marcel Auja Ogweno', '11237486', 17, 1200.00000, 'A', '2013-04-23 16:12:23', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '1', 'A', '123456', 'NORMAL', 1139986, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 13:12:23', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-04-23', '16:17:58', '1', 'Marcel Auja Ogweno', '11237486', 18, 1200.00000, 'A', '2013-04-23 16:17:58', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', '123456', 'NORMAL', 1139987, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-04-23 13:17:58', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 3),
(NULL, '2013-05-01', '16:38:09', '2', 'Marcel Auja Ogweno', '11237486', 22, 1200.00000, 'A', '2013-05-01 16:38:09', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', 'MA1', 'SPECIAL', 1139991, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-01 13:38:09', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 25),
(NULL, '2013-05-01', '16:40:58', '2', 'Haroon O2 Butt', '5554535353', 21, 1200.00000, 'A', '2013-05-01 16:40:58', 'John Admin', 0, NULL, '0', '0719110786', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', 'MA2', 'SPECIAL', 1139992, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-01 13:40:58', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 25),
(NULL, '2013-05-02', '15:49:25', '2', 'Mwazito', '23423123', 24, 1200.00000, 'A', '2013-05-02 15:49:25', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', 'MA3', 'SPECIAL', 1139993, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-02 12:49:25', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 0),
(NULL, '2013-05-02', '15:49:25', '05/02/2013 19:30', 'Mwazito', '23423123', 39, 1200.00000, 'A', '2013-05-02 15:49:25', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '1', 'A', '123456', 'NORMAL', 1139994, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-02 12:49:25', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 0),
(NULL, '2013-05-02', '18:09:59', '2', 'Marcel Auja Ogweno', '11237486', 26, 1200.00000, 'A', '2013-05-02 18:09:59', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', 'MA4', 'SPECIAL', 1139995, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-02 15:09:59', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 4, 1, 0),
(NULL, '2013-05-02', '18:09:59', '05/02/2013 19:30', 'Marcel Auja Ogweno', '11237486', 40, 1200.00000, 'A', '2013-05-02 18:09:59', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '1', 'A', '123456', 'NORMAL', 1139996, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-02 15:09:59', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 1, 1, 1, 0),
(NULL, '2013-05-02', '18:09:59', '2', 'Haroon O2 Butt', '5554535353', 30, 1200.00000, 'A', '2013-05-02 18:09:59', 'John Admin', 0, NULL, '0', '0719110786', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'A', 'MA5', 'SPECIAL', 1139997, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-02 15:09:59', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 4, 1, 0),
(NULL, '2013-05-07', '01:58:58', '48', 'Haroon O2 Butt', '5554535353', 11, 42000.00000, 'B', '2013-05-07 01:58:58', 'John Admin', 0, NULL, '0', '0719110786', '5', '2', 'KAM-NRB10:00AMA', 0, 'Active', 'No', NULL, NULL, NULL, 0, '6', '2', '2', 'B', 'KA1', 'NORMAL', 1139998, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '2', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-06 22:58:58', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 28),
(NULL, '2013-05-08', '02:45:12', '2', 'Marcel Auja Ogweno', '11237486', 17, 1700.00000, 'B', '2013-05-08 02:45:12', 'John Admin', 0, NULL, '0', '0703212345', '1', '2', 'MSA-NRB11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '4', '1', '2', 'B', 'MA6', 'SPECIAL', 1139999, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-07 23:45:12', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 25),
(NULL, '2013-05-11', '18:07:54', '12:50 AM', 'Haroon O2 Butt', '5554535353', 13, 1700.00000, 'A', '2013-05-11 18:07:54', 'John Admin', 0, NULL, '0', '0719110786', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'A', 'NA2', 'SPECIAL', 1140016, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-11 15:07:54', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 24),
(NULL, '2013-05-11', '17:50:47', '12:50 AM', 'Marcel Auja Ogweno', '11237486', 9, 1700.00000, 'A', '2013-05-11 17:50:47', 'John Admin', 0, NULL, '0', '0703212345', '2', '1', 'NRB-MSA11:00PM', 0, 'Active', 'No', NULL, NULL, NULL, 0, '2', '1', '1', 'A', 'NA1', 'SPECIAL', 1140015, 0.00000, NULL, '0000-00-00', NULL, 0.00000, 0, '1', 0, 0, 0, 0, 0, 0, NULL, '', '2013-05-11 14:50:47', 'WEB', NULL, '3', NULL, 1, NULL, NULL, 0, 1, 1, 24);

--
-- Triggers `ticketing`
--
DROP TRIGGER IF EXISTS `trackit`;
DELIMITER //
CREATE TRIGGER `trackit` AFTER INSERT ON `ticketing`
 FOR EACH ROW BEGIN
  /* call trackticket();*/
    END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `TicketId` int(100) NOT NULL AUTO_INCREMENT,
  `TicketNumber` varchar(50) NOT NULL,
  `SeatNumber` smallint(2) NOT NULL,
  `BusLkp` smallint(3) NOT NULL,
  `RouteLkp` smallint(3) NOT NULL,
  `PaymentLkp` int(10) NOT NULL,
  `CustomerLkp` int(100) NOT NULL,
  `BookDate` date DEFAULT NULL,
  `PriceLkp` int(20) NOT NULL,
  `TicketDate` datetime NOT NULL,
  `IssuedBy` smallint(3) NOT NULL,
  `TicketStatus` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TicketId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_transfers`
--

CREATE TABLE IF NOT EXISTS `ticket_transfers` (
  `TransferId` int(100) NOT NULL AUTO_INCREMENT,
  `TicketLkp` varchar(100) NOT NULL,
  `OwnerLkp` int(100) NOT NULL,
  `TransferDate` datetime NOT NULL,
  `TransferBy` int(11) NOT NULL,
  `Reason` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TransferId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `towns_lkp`
--

CREATE TABLE IF NOT EXISTS `towns_lkp` (
  `TownId` smallint(3) NOT NULL AUTO_INCREMENT,
  `NationalityId` int(11) NOT NULL,
  `TownName` varchar(100) NOT NULL,
  `AddedBy` smallint(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `TownCode` varchar(30) DEFAULT NULL,
  `TownSCode` varchar(30) DEFAULT NULL,
  `Currency` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`TownId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `towns_lkp`
--

INSERT INTO `towns_lkp` (`TownId`, `NationalityId`, `TownName`, `AddedBy`, `status`, `TownCode`, `TownSCode`, `Currency`) VALUES
(1, 1, 'Mombasa', NULL, 1, 'MO', 'MA', 1),
(2, 1, 'Nairobi', NULL, 1, 'NO', 'NA', 1),
(3, 0, 'Nakuru', NULL, 1, 'NK', 'NR', 1),
(4, 0, 'Kisumu', NULL, 1, 'KI', 'KS', 1),
(5, 0, 'Kampala', NULL, 1, 'KA', 'KM', 2),
(6, 1, 'Voi', NULL, 1, 'VO', 'VI', 1),
(7, 1, 'Kitale', NULL, 1, 'KI', 'KT', 1),
(8, 0, 'Malindi', NULL, 1, 'ML', 'MS', 1),
(9, 0, 'Tanga', NULL, 1, 'TA', NULL, 3),
(10, 0, 'Dar-es-Salaam', NULL, 1, 'DA', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

CREATE TABLE IF NOT EXISTS `transaction_log` (
  `transaction_log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_log_name` varchar(100) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `pkName` varchar(100) NOT NULL,
  `pkValue_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`transaction_log_id`),
  KEY `pkValue_id` (`pkValue_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1418 ;

--
-- Dumping data for table `transaction_log`
--

INSERT INTO `transaction_log` (`transaction_log_id`, `transaction_log_name`, `table_name`, `pkName`, `pkValue_id`, `timestamp`, `user_id`, `description`) VALUES
(2, 'insert', 'buses', 'BusId', 4, '2013-01-17 09:26:52', 2, 'insert to buses table'),
(3, 'insert', 'bus_termini', 'TerminusId', 5, '2013-01-17 09:38:48', 2, 'insert to bus_termini table'),
(4, 'insert', 'bus_maintenance', 'MaintenanceId', 7, '2013-01-17 09:47:41', 2, 'insert to bus_maintenance table'),
(5, 'insert', 'route_prices', 'PriceId', 4, '2013-01-17 11:47:32', 2, 'insert to route_prices table'),
(6, 'update', 'route_prices', 'PriceId', 4, '2013-01-17 12:19:41', 2, 'update row described in route_prices table'),
(7, 'update', 'route_prices', 'PriceId', 4, '2013-01-17 12:19:58', 2, 'update row described in route_prices table'),
(8, 'update', 'bus_routes', 'RouteId', 43, '2013-01-17 13:02:56', 2, 'update row described in bus_routes table'),
(9, 'insert', 'bus_schedule', 'ScheduleId', 3, '2013-01-18 07:25:23', 2, 'insert to bus_schedule table'),
(10, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-18 08:04:40', 2, 'update row described in bus_schedule table'),
(11, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-18 08:05:59', 2, 'update row described in bus_schedule table'),
(12, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-18 08:06:38', 2, 'update row described in bus_schedule table'),
(13, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-18 08:14:14', 2, 'update row described in bus_schedule table'),
(14, 'insert', 'bus_staff', 'StaffId', 3, '2013-01-18 09:02:23', 2, 'insert to bus_staff table'),
(15, 'update', 'bus_staff', 'StaffId', 3, '2013-01-18 09:06:48', 2, 'update row described in bus_staff table'),
(16, 'update', 'bus_staff', 'StaffId', 3, '2013-01-18 09:07:10', 2, 'update row described in bus_staff table'),
(17, 'insert', 'petty_cash', 'PettyCashId', 1, '2013-01-18 12:46:00', 2, 'insert to petty_cash table'),
(18, 'insert', 'petty_cash', 'PettyCashId', 2, '2013-01-18 12:51:17', 2, 'insert to petty_cash table'),
(19, 'insert', 'petty_cash', 'PettyCashId', 3, '2013-01-18 12:56:58', 2, 'insert to petty_cash table'),
(20, 'insert', 'petty_cash', 'PettyCashId', 4, '2013-01-18 12:58:56', 2, 'insert to petty_cash table'),
(21, 'insert', 'petty_cash', 'PettyCashId', 5, '2013-01-18 13:03:40', 2, 'insert to petty_cash table'),
(22, 'insert', 'bus_maintenance', 'MaintenanceId', 8, '2013-01-18 14:26:21', 2, 'insert to bus_maintenance table'),
(23, 'insert', 'hirers', 'HirerId', 1, '2013-01-19 13:07:58', 2, 'insert to hirers table'),
(24, 'insert', 'bus_hire', 'BusHireId', 1, '2013-01-19 13:07:58', 2, 'insert to bus_hire table'),
(25, 'insert', 'hirers', 'HirerId', 2, '2013-01-19 13:21:19', 2, 'insert to hirers table'),
(26, 'insert', 'bus_hire', 'BusHireId', 2, '2013-01-19 13:21:19', 2, 'insert to bus_hire table'),
(27, 'update', 'hirers', 'HirerId', 2, '2013-01-19 13:57:11', 2, 'update row described in hirers table'),
(28, 'update', 'bus_hire', 'BusHireId', 2, '2013-01-19 13:57:11', 2, 'update row described in bus_hire table'),
(29, 'update', 'hirers', 'HirerId', 2, '2013-01-19 14:00:25', 2, 'update row described in hirers table'),
(30, 'update', 'bus_hire', 'BusHireId', 2, '2013-01-19 14:00:25', 2, 'update row described in bus_hire table'),
(31, 'update', 'hirers', 'HirerId', 1, '2013-01-19 14:00:52', 2, 'update row described in hirers table'),
(32, 'update', 'bus_hire', 'BusHireId', 1, '2013-01-19 14:00:52', 2, 'update row described in bus_hire table'),
(33, 'update', 'bus_hire', 'BusHireId', 2, '2013-01-21 11:44:50', 0, 'update row described in bus_hire table'),
(34, 'update', 'buses', 'BusId', 2, '2013-01-21 11:44:51', 0, 'update row described in buses table'),
(35, 'update', 'bus_hire', 'BusHireId', 2, '2013-01-21 11:48:20', 0, 'update row described in bus_hire table'),
(36, 'update', 'buses', 'BusId', 2, '2013-01-21 11:48:21', 0, 'update row described in buses table'),
(37, 'update', 'bus_staff', 'StaffId', 1, '2013-01-21 11:50:34', 0, 'update row described in bus_staff table'),
(38, 'update', 'route_prices', 'PriceId', 4, '2013-01-21 11:51:44', 0, 'update row described in route_prices table'),
(39, 'update', 'route_prices', 'PriceId', 4, '2013-01-21 11:56:17', 0, 'update row described in route_prices table'),
(40, 'update', 'bus_hire', 'BusHireId', 2, '2013-01-21 12:12:58', 0, 'update row described in bus_hire table'),
(41, 'update', 'buses', 'BusId', 2, '2013-01-21 12:12:58', 0, 'update row described in buses table'),
(42, 'update', 'bus_maintenance', 'MaintenanceId', 8, '2013-01-21 12:27:28', 0, 'update row described in bus_maintenance table'),
(43, 'update', 'bus_maintenance', 'MaintenanceId', 7, '2013-01-21 12:31:39', 0, 'update row described in bus_maintenance table'),
(44, 'update', 'bus_routes', 'RouteId', 43, '2013-01-21 12:42:15', 0, 'update row described in bus_routes table'),
(45, 'update', 'buses', 'BusId', 4, '2013-01-21 12:46:20', 0, 'update row described in buses table'),
(46, 'update', 'buses', 'BusId', 2, '2013-01-21 13:13:59', 0, 'update row described in buses table'),
(47, 'update', 'route_prices', 'PriceId', 4, '2013-01-21 13:18:18', 0, 'update row described in route_prices table'),
(48, 'insert', 'banking', 'BankingId', 1, '2013-01-21 15:06:44', 0, 'insert to banking table'),
(49, 'update', 'banking', 'BankingId', 1, '2013-01-21 15:09:52', 0, 'update row described in banking table'),
(50, 'update', 'banking', 'BankingId', 1, '2013-01-21 15:12:56', 0, 'update row described in banking table'),
(51, 'insert', 'banking', 'BankingId', 2, '2013-01-21 15:22:59', 0, 'insert to banking table'),
(52, 'update', 'banking', 'BankingId', 2, '2013-01-21 15:44:25', 0, 'update row described in banking table'),
(53, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-29 13:21:08', 0, 'update row described in bus_schedule table'),
(54, 'update', 'bus_maintenance', 'MaintenanceId', 1, '2013-01-30 12:56:36', 0, 'update row described in bus_maintenance table'),
(55, 'update', 'bus_maintenance', 'MaintenanceId', 4, '2013-01-30 12:56:48', 0, 'update row described in bus_maintenance table'),
(56, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-01-30 19:06:58', 0, 'update row described in bus_schedule table'),
(57, 'insert', 'bus_maintenance', 'MaintenanceId', 9, '2013-01-30 21:57:04', 0, 'insert to bus_maintenance table'),
(58, 'update', 'buses', 'BusId', 3, '2013-01-31 12:43:08', 0, 'update row described in buses table'),
(59, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-02-01 07:16:33', 0, 'update row described in bus_schedule table'),
(60, 'insert', 'bus_schedule', 'ScheduleId', 4, '2013-02-01 07:17:27', 0, 'insert to bus_schedule table'),
(61, 'insert', 'ticketing', 'TicketId', 1139933, '2013-02-01 08:43:19', 0, 'insert to ticketing table'),
(62, 'insert', 'ticketing', 'TicketId', 1139934, '2013-02-01 08:51:25', 0, 'insert to ticketing table'),
(63, 'update', 'ticketing', 'TicketId', 1139933, '2013-02-01 12:15:42', 0, 'update row described in ticketing table'),
(64, 'update', 'ticketing', 'TicketId', 1139933, '2013-02-01 12:22:04', 0, 'update row described in ticketing table'),
(65, 'insert', 'ticketing', 'TicketId', 1139935, '2013-02-01 13:22:50', 0, 'insert to ticketing table'),
(66, 'insert', 'ticketing', 'TicketId', 1139936, '2013-02-01 13:46:22', 0, 'insert to ticketing table'),
(67, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-02-02 10:52:39', 0, 'update row described in bus_schedule table'),
(68, 'update', 'bus_schedule', 'ScheduleId', 4, '2013-02-02 12:22:42', 0, 'update row described in bus_schedule table'),
(69, 'insert', 'ticketing', 'TicketId', 1139937, '2013-02-02 13:47:25', 0, 'insert to ticketing table'),
(70, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-02-12 11:32:27', 0, 'update row described in bus_schedule table'),
(71, 'insert', 'ticketing', 'TicketId', 1139938, '2013-02-12 16:54:51', 0, 'insert to ticketing table'),
(72, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 17:57:10', 0, 'update row described in ticketing table'),
(73, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:08', 0, 'update row described in ticketing table'),
(74, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:08', 0, 'update row described in ticketing table'),
(75, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:09', 0, 'update row described in ticketing table'),
(76, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:09', 0, 'update row described in ticketing table'),
(77, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:09', 0, 'update row described in ticketing table'),
(78, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:10', 0, 'update row described in ticketing table'),
(79, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:10', 0, 'update row described in ticketing table'),
(80, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:11', 0, 'update row described in ticketing table'),
(81, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:12', 0, 'update row described in ticketing table'),
(82, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:12', 0, 'update row described in ticketing table'),
(83, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:13', 0, 'update row described in ticketing table'),
(84, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:13', 0, 'update row described in ticketing table'),
(85, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:13', 0, 'update row described in ticketing table'),
(86, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:14', 0, 'update row described in ticketing table'),
(87, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:15', 0, 'update row described in ticketing table'),
(88, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:15', 0, 'update row described in ticketing table'),
(89, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:15', 0, 'update row described in ticketing table'),
(90, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:16', 0, 'update row described in ticketing table'),
(91, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:16', 0, 'update row described in ticketing table'),
(92, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:16', 0, 'update row described in ticketing table'),
(93, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:04:17', 0, 'update row described in ticketing table'),
(94, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:02', 0, 'update row described in ticketing table'),
(95, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:02', 0, 'update row described in ticketing table'),
(96, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:02', 0, 'update row described in ticketing table'),
(97, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:03', 0, 'update row described in ticketing table'),
(98, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:03', 0, 'update row described in ticketing table'),
(99, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:03', 0, 'update row described in ticketing table'),
(100, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:04', 0, 'update row described in ticketing table'),
(101, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:04', 0, 'update row described in ticketing table'),
(102, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:06', 0, 'update row described in ticketing table'),
(103, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:06', 0, 'update row described in ticketing table'),
(104, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:06', 0, 'update row described in ticketing table'),
(105, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:07', 0, 'update row described in ticketing table'),
(106, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:07', 0, 'update row described in ticketing table'),
(107, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:08', 0, 'update row described in ticketing table'),
(108, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:08', 0, 'update row described in ticketing table'),
(109, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:09', 0, 'update row described in ticketing table'),
(110, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:09', 0, 'update row described in ticketing table'),
(111, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:09', 0, 'update row described in ticketing table'),
(112, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:10', 0, 'update row described in ticketing table'),
(113, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:10', 0, 'update row described in ticketing table'),
(114, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:08:10', 0, 'update row described in ticketing table'),
(115, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:09:13', 0, 'update row described in ticketing table'),
(116, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:10:18', 0, 'update row described in ticketing table'),
(117, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:12:14', 0, 'update row described in ticketing table'),
(118, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:13:40', 0, 'update row described in ticketing table'),
(119, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:15:04', 0, 'update row described in ticketing table'),
(120, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 18:18:11', 0, 'update row described in ticketing table'),
(121, 'insert', 'ticketing', 'TicketId', 1139939, '2013-02-12 20:05:44', 0, 'insert to ticketing table'),
(122, 'update', 'ticketing', 'TicketId', 1139938, '2013-02-12 20:43:53', 0, 'update row described in ticketing table'),
(123, 'insert', 'ticketing', 'TicketId', 1139940, '2013-02-12 23:13:23', 0, 'insert to ticketing table'),
(124, 'insert', 'ticketing', 'TicketId', 1139941, '2013-02-13 02:38:36', 0, 'insert to ticketing table'),
(125, 'insert', 'ticketing', 'TicketId', 1139942, '2013-02-13 02:44:22', 0, 'insert to ticketing table'),
(126, 'insert', 'customer_loyalty_points', 'LoyaltyId', 1, '2013-02-13 02:44:22', 0, 'insert to customer_loyalty_points table'),
(127, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-02-13 18:32:15', 0, 'update row described in bus_schedule table'),
(128, 'update', 'buses', 'BusId', 1, '2013-03-18 12:30:28', 0, 'update row described in buses table'),
(129, 'update', 'buses', 'BusId', 3, '2013-03-18 12:30:49', 0, 'update row described in buses table'),
(130, 'update', 'buses', 'BusId', 2, '2013-03-18 12:31:04', 0, 'update row described in buses table'),
(131, 'update', 'buses', 'BusId', 4, '2013-03-18 12:31:32', 0, 'update row described in buses table'),
(132, 'update', 'seats', 'seat_id', 0, '2013-03-19 07:53:06', 0, 'update row described in seats table'),
(133, 'update', 'seats', 'seat_id', 0, '2013-03-19 08:03:25', 0, 'update row described in seats table'),
(134, 'update', 'seats', 'seat_id', 0, '2013-03-19 08:08:12', 0, 'update row described in seats table'),
(135, 'update', 'seats', 'seat_id', 0, '2013-03-19 08:25:17', 0, 'update row described in seats table'),
(136, 'update', 'bus_schedule', 'ScheduleId', 3, '2013-03-19 09:55:47', 0, 'update row described in bus_schedule table'),
(137, 'update', 'route_prices', 'PriceId', 4, '2013-03-19 21:43:07', 0, 'update row described in route_prices table'),
(138, 'insert', 'route_prices', 'PriceId', 5, '2013-03-19 21:50:01', 0, 'insert to route_prices table'),
(139, 'insert', 'route_prices', 'PriceId', 6, '2013-03-19 21:51:01', 0, 'insert to route_prices table'),
(140, 'update', 'route_prices', 'PriceId', 5, '2013-03-19 21:51:39', 0, 'update row described in route_prices table'),
(141, 'update', 'route_prices', 'PriceId', 6, '2013-03-19 21:52:05', 0, 'update row described in route_prices table'),
(142, 'insert', 'vouchers', 'vouchers_id', 1, '2013-04-11 18:06:59', 0, 'insert to vouchers table'),
(143, 'insert', 'vouchers', 'vouchers_id', 2, '2013-04-11 18:15:25', 0, 'insert to vouchers table'),
(144, 'update', 'vouchers', 'vouchers_id', 1, '2013-04-12 12:59:52', 0, 'update row described in vouchers table'),
(145, 'insert', 'voucher_items', 'voucher_items_id', 0, '2013-04-13 17:06:08', 0, 'insert to voucher_items table'),
(146, 'insert', 'voucher_items', 'voucher_items_id', 2, '2013-04-13 18:52:34', 0, 'insert to voucher_items table'),
(147, 'insert', 'voucher_items', 'voucher_items_id', 3, '2013-04-13 18:54:09', 0, 'insert to voucher_items table'),
(148, 'update', 'vouchers', 'vouchers_id', 2, '2013-04-14 05:32:50', 0, 'update row described in vouchers table'),
(149, 'insert', 'banking', 'BankingId', 3, '2013-04-14 09:07:09', 0, 'insert to banking table'),
(150, 'insert', 'route_prices', 'PriceId', 7, '2013-04-14 12:04:26', 0, 'insert to route_prices table'),
(151, 'insert', 'route_prices', 'PriceId', 8, '2013-04-14 12:04:58', 0, 'insert to route_prices table'),
(152, 'insert', 'route_prices', 'PriceId', 9, '2013-04-14 12:05:28', 0, 'insert to route_prices table'),
(153, 'insert', 'bus_schedule', 'ScheduleId', 5, '2013-04-14 12:06:10', 0, 'insert to bus_schedule table'),
(154, 'insert', 'petty_cash', 'PettyCashId', 5, '2013-04-15 06:47:55', 0, 'insert to petty_cash table'),
(155, 'insert', 'discounts', 'Disc_Id', 1, '2013-04-15 19:28:36', 0, 'insert to discounts table'),
(156, 'insert', 'discounts', 'Disc_Id', 2, '2013-04-15 20:50:34', 0, 'insert to discounts table'),
(157, 'insert', 'ticketing', 'TicketId', 1139943, '2013-04-17 17:44:47', 0, 'insert to ticketing table'),
(158, 'insert', 'customer_loyalty_points', 'LoyaltyId', 2, '2013-04-17 17:44:47', 0, 'insert to customer_loyalty_points table'),
(159, 'insert', 'ticketing', 'TicketId', 1139944, '2013-04-17 17:44:47', 0, 'insert to ticketing table'),
(160, 'insert', 'customer_loyalty_points', 'LoyaltyId', 3, '2013-04-17 17:44:47', 0, 'insert to customer_loyalty_points table'),
(161, 'insert', 'ticketing', 'TicketId', 1139945, '2013-04-17 17:44:47', 0, 'insert to ticketing table'),
(162, 'insert', 'ticketing', 'TicketId', 1139946, '2013-04-17 17:57:53', 0, 'insert to ticketing table'),
(163, 'insert', 'customer_loyalty_points', 'LoyaltyId', 4, '2013-04-17 17:57:53', 0, 'insert to customer_loyalty_points table'),
(164, 'insert', 'ticketing', 'TicketId', 1139947, '2013-04-17 17:57:53', 0, 'insert to ticketing table'),
(165, 'insert', 'customer_loyalty_points', 'LoyaltyId', 5, '2013-04-17 17:57:53', 0, 'insert to customer_loyalty_points table'),
(166, 'insert', 'ticketing', 'TicketId', 1139948, '2013-04-17 17:57:53', 0, 'insert to ticketing table'),
(167, 'insert', 'ticketing', 'TicketId', 1139949, '2013-04-17 18:01:56', 0, 'insert to ticketing table'),
(168, 'insert', 'customer_loyalty_points', 'LoyaltyId', 6, '2013-04-17 18:01:56', 0, 'insert to customer_loyalty_points table'),
(169, 'insert', 'ticketing', 'TicketId', 1139950, '2013-04-17 18:01:56', 0, 'insert to ticketing table'),
(170, 'insert', 'customer_loyalty_points', 'LoyaltyId', 7, '2013-04-17 18:01:56', 0, 'insert to customer_loyalty_points table'),
(171, 'insert', 'ticketing', 'TicketId', 1139951, '2013-04-17 18:01:56', 0, 'insert to ticketing table'),
(172, 'insert', 'ticketing', 'TicketId', 1139952, '2013-04-17 18:03:05', 0, 'insert to ticketing table'),
(173, 'insert', 'customer_loyalty_points', 'LoyaltyId', 8, '2013-04-17 18:03:05', 0, 'insert to customer_loyalty_points table'),
(174, 'insert', 'ticketing', 'TicketId', 1139953, '2013-04-17 18:03:05', 0, 'insert to ticketing table'),
(175, 'insert', 'customer_loyalty_points', 'LoyaltyId', 9, '2013-04-17 18:03:05', 0, 'insert to customer_loyalty_points table'),
(176, 'insert', 'ticketing', 'TicketId', 1139954, '2013-04-17 18:03:05', 0, 'insert to ticketing table'),
(177, 'insert', 'ticketing', 'TicketId', 1139955, '2013-04-17 18:04:46', 0, 'insert to ticketing table'),
(178, 'insert', 'customer_loyalty_points', 'LoyaltyId', 10, '2013-04-17 18:04:46', 0, 'insert to customer_loyalty_points table'),
(179, 'insert', 'ticketing', 'TicketId', 1139956, '2013-04-17 18:04:46', 0, 'insert to ticketing table'),
(180, 'insert', 'customer_loyalty_points', 'LoyaltyId', 11, '2013-04-17 18:04:46', 0, 'insert to customer_loyalty_points table'),
(181, 'insert', 'ticketing', 'TicketId', 1139957, '2013-04-17 18:04:47', 0, 'insert to ticketing table'),
(182, 'insert', 'ticketing', 'TicketId', 1139958, '2013-04-17 18:08:30', 0, 'insert to ticketing table'),
(183, 'insert', 'customer_loyalty_points', 'LoyaltyId', 12, '2013-04-17 18:08:30', 0, 'insert to customer_loyalty_points table'),
(184, 'insert', 'ticketing', 'TicketId', 1139959, '2013-04-17 18:08:30', 0, 'insert to ticketing table'),
(185, 'insert', 'customer_loyalty_points', 'LoyaltyId', 13, '2013-04-17 18:08:30', 0, 'insert to customer_loyalty_points table'),
(186, 'insert', 'ticketing', 'TicketId', 1139960, '2013-04-17 18:08:30', 0, 'insert to ticketing table'),
(187, 'insert', 'bus_schedule', 'ScheduleId', 6, '2013-04-23 02:07:26', 0, 'insert to bus_schedule table'),
(188, 'update', 'bus_schedule', 'ScheduleId', 6, '2013-04-23 02:08:43', 0, 'update row described in bus_schedule table'),
(189, 'insert', 'bus_schedule', 'ScheduleId', 7, '2013-04-23 02:09:11', 0, 'insert to bus_schedule table'),
(190, 'update', 'bus_schedule', 'ScheduleId', 7, '2013-04-23 02:10:29', 0, 'update row described in bus_schedule table'),
(191, 'insert', 'bus_schedule', 'ScheduleId', 8, '2013-04-23 02:10:48', 0, 'insert to bus_schedule table'),
(192, 'insert', 'bus_schedule', 'ScheduleId', 9, '2013-04-23 02:10:48', 0, 'insert to bus_schedule table'),
(193, 'insert', 'bus_schedule', 'ScheduleId', 10, '2013-04-23 02:10:48', 0, 'insert to bus_schedule table'),
(194, 'insert', 'bus_schedule', 'ScheduleId', 11, '2013-04-23 02:10:48', 0, 'insert to bus_schedule table'),
(195, 'update', 'bus_schedule', 'ScheduleId', 11, '2013-04-23 02:11:26', 0, 'update row described in bus_schedule table'),
(196, 'update', 'bus_schedule', 'ScheduleId', 10, '2013-04-23 02:11:29', 0, 'update row described in bus_schedule table'),
(197, 'update', 'bus_schedule', 'ScheduleId', 9, '2013-04-23 02:11:32', 0, 'update row described in bus_schedule table'),
(198, 'update', 'bus_schedule', 'ScheduleId', 8, '2013-04-23 02:11:36', 0, 'update row described in bus_schedule table'),
(199, 'insert', 'bus_schedule', 'ScheduleId', 12, '2013-04-23 02:12:04', 0, 'insert to bus_schedule table'),
(200, 'insert', 'bus_schedule', 'ScheduleId', 13, '2013-04-23 02:12:04', 0, 'insert to bus_schedule table'),
(201, 'insert', 'bus_schedule', 'ScheduleId', 14, '2013-04-23 02:12:04', 0, 'insert to bus_schedule table'),
(202, 'update', 'bus_schedule', 'ScheduleId', 14, '2013-04-23 02:12:59', 0, 'update row described in bus_schedule table'),
(203, 'update', 'bus_schedule', 'ScheduleId', 13, '2013-04-23 02:13:04', 0, 'update row described in bus_schedule table'),
(204, 'update', 'bus_schedule', 'ScheduleId', 12, '2013-04-23 02:13:09', 0, 'update row described in bus_schedule table'),
(205, 'insert', 'bus_schedule', 'ScheduleId', 15, '2013-04-23 02:13:32', 0, 'insert to bus_schedule table'),
(206, 'insert', 'bus_schedule', 'ScheduleId', 16, '2013-04-23 02:13:32', 0, 'insert to bus_schedule table'),
(207, 'insert', 'bus_schedule', 'ScheduleId', 17, '2013-04-23 02:13:32', 0, 'insert to bus_schedule table'),
(208, 'insert', 'bus_schedule', 'ScheduleId', 18, '2013-04-23 02:13:32', 0, 'insert to bus_schedule table'),
(209, 'insert', 'ticketing', 'TicketId', 1139961, '2013-04-23 11:39:09', 0, 'insert to ticketing table'),
(210, 'insert', 'customer_loyalty_points', 'LoyaltyId', 14, '2013-04-23 11:39:09', 0, 'insert to customer_loyalty_points table'),
(211, 'insert', 'ticketing', 'TicketId', 1139962, '2013-04-23 11:39:09', 0, 'insert to ticketing table'),
(212, 'insert', 'customer_loyalty_points', 'LoyaltyId', 15, '2013-04-23 11:39:09', 0, 'insert to customer_loyalty_points table'),
(213, 'insert', 'ticketing', 'TicketId', 1139963, '2013-04-23 11:46:57', 0, 'insert to ticketing table'),
(214, 'insert', 'customer_loyalty_points', 'LoyaltyId', 16, '2013-04-23 11:46:57', 0, 'insert to customer_loyalty_points table'),
(215, 'insert', 'ticketing', 'TicketId', 1139964, '2013-04-23 11:46:57', 0, 'insert to ticketing table'),
(216, 'insert', 'customer_loyalty_points', 'LoyaltyId', 17, '2013-04-23 11:46:57', 0, 'insert to customer_loyalty_points table'),
(217, 'insert', 'ticketing', 'TicketId', 1139965, '2013-04-23 11:46:57', 0, 'insert to ticketing table'),
(218, 'insert', 'customer_loyalty_points', 'LoyaltyId', 18, '2013-04-23 11:46:57', 0, 'insert to customer_loyalty_points table'),
(219, 'insert', 'ticketing', 'TicketId', 1139966, '2013-04-23 12:17:09', 0, 'insert to ticketing table'),
(220, 'insert', 'customer_loyalty_points', 'LoyaltyId', 19, '2013-04-23 12:17:09', 0, 'insert to customer_loyalty_points table'),
(221, 'insert', 'ticketing', 'TicketId', 1139967, '2013-04-23 12:19:01', 0, 'insert to ticketing table'),
(222, 'insert', 'customer_loyalty_points', 'LoyaltyId', 20, '2013-04-23 12:19:01', 0, 'insert to customer_loyalty_points table'),
(223, 'insert', 'ticketing', 'TicketId', 1139968, '2013-04-23 12:19:41', 0, 'insert to ticketing table'),
(224, 'insert', 'customer_loyalty_points', 'LoyaltyId', 21, '2013-04-23 12:19:41', 0, 'insert to customer_loyalty_points table'),
(225, 'insert', 'ticketing', 'TicketId', 1139969, '2013-04-23 12:20:35', 0, 'insert to ticketing table'),
(226, 'insert', 'customer_loyalty_points', 'LoyaltyId', 22, '2013-04-23 12:20:35', 0, 'insert to customer_loyalty_points table'),
(227, 'insert', 'ticketing', 'TicketId', 1139970, '2013-04-23 12:21:23', 0, 'insert to ticketing table'),
(228, 'insert', 'customer_loyalty_points', 'LoyaltyId', 23, '2013-04-23 12:21:23', 0, 'insert to customer_loyalty_points table'),
(229, 'insert', 'ticketing', 'TicketId', 1139971, '2013-04-23 12:22:44', 0, 'insert to ticketing table'),
(230, 'insert', 'customer_loyalty_points', 'LoyaltyId', 24, '2013-04-23 12:22:44', 0, 'insert to customer_loyalty_points table'),
(231, 'insert', 'ticketing', 'TicketId', 1139972, '2013-04-23 12:23:41', 0, 'insert to ticketing table'),
(232, 'insert', 'customer_loyalty_points', 'LoyaltyId', 25, '2013-04-23 12:23:41', 0, 'insert to customer_loyalty_points table'),
(233, 'insert', 'ticketing', 'TicketId', 1139973, '2013-04-23 12:27:45', 0, 'insert to ticketing table'),
(234, 'insert', 'ticketing', 'TicketId', 1139974, '2013-04-23 12:27:45', 0, 'insert to ticketing table'),
(235, 'insert', 'ticketing', 'TicketId', 1139975, '2013-04-23 12:36:34', 0, 'insert to ticketing table'),
(236, 'insert', 'ticketing', 'TicketId', 1139976, '2013-04-23 12:36:34', 0, 'insert to ticketing table'),
(237, 'insert', 'ticketing', 'TicketId', 1139977, '2013-04-23 12:40:27', 0, 'insert to ticketing table'),
(238, 'insert', 'ticketing', 'TicketId', 1139978, '2013-04-23 12:40:27', 0, 'insert to ticketing table'),
(239, 'insert', 'ticketing', 'TicketId', 1139979, '2013-04-23 12:43:00', 0, 'insert to ticketing table'),
(240, 'insert', 'ticketing', 'TicketId', 1139980, '2013-04-23 12:43:00', 0, 'insert to ticketing table'),
(241, 'insert', 'ticketing', 'TicketId', 1139981, '2013-04-23 12:44:36', 0, 'insert to ticketing table'),
(242, 'insert', 'ticketing', 'TicketId', 1139982, '2013-04-23 12:44:36', 0, 'insert to ticketing table'),
(243, 'insert', 'ticketing', 'TicketId', 1139983, '2013-04-23 13:07:49', 0, 'insert to ticketing table'),
(244, 'insert', 'customer_loyalty_points', 'LoyaltyId', 26, '2013-04-23 13:07:49', 0, 'insert to customer_loyalty_points table'),
(245, 'insert', 'ticketing', 'TicketId', 1139984, '2013-04-23 13:07:49', 0, 'insert to ticketing table'),
(246, 'insert', 'customer_loyalty_points', 'LoyaltyId', 27, '2013-04-23 13:07:49', 0, 'insert to customer_loyalty_points table'),
(247, 'insert', 'ticketing', 'TicketId', 1139985, '2013-04-23 13:12:23', 0, 'insert to ticketing table'),
(248, 'insert', 'customer_loyalty_points', 'LoyaltyId', 28, '2013-04-23 13:12:23', 0, 'insert to customer_loyalty_points table'),
(249, 'insert', 'ticketing', 'TicketId', 1139986, '2013-04-23 13:12:23', 0, 'insert to ticketing table'),
(250, 'insert', 'customer_loyalty_points', 'LoyaltyId', 29, '2013-04-23 13:12:23', 0, 'insert to customer_loyalty_points table'),
(251, 'insert', 'ticketing', 'TicketId', 1139987, '2013-04-23 13:17:58', 0, 'insert to ticketing table'),
(252, 'insert', 'customer_loyalty_points', 'LoyaltyId', 30, '2013-04-23 13:17:58', 0, 'insert to customer_loyalty_points table'),
(253, 'insert', 'bus_schedule', 'ScheduleId', 19, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(254, 'insert', 'bus_schedule', 'ScheduleId', 20, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(255, 'insert', 'bus_schedule', 'ScheduleId', 21, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(256, 'insert', 'bus_schedule', 'ScheduleId', 22, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(257, 'insert', 'bus_schedule', 'ScheduleId', 23, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(258, 'insert', 'bus_schedule', 'ScheduleId', 24, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(259, 'insert', 'bus_schedule', 'ScheduleId', 25, '2013-04-23 13:24:22', 0, 'insert to bus_schedule table'),
(260, 'insert', 'ticketing', 'TicketId', 1139988, '2013-04-23 14:41:43', 0, 'insert to ticketing table'),
(261, 'insert', 'customer_loyalty_points', 'LoyaltyId', 31, '2013-04-23 14:41:43', 0, 'insert to customer_loyalty_points table'),
(262, 'insert', 'ticketing', 'TicketId', 1139989, '2013-04-23 14:41:43', 0, 'insert to ticketing table'),
(263, 'insert', 'customer_loyalty_points', 'LoyaltyId', 32, '2013-04-23 14:41:43', 0, 'insert to customer_loyalty_points table'),
(264, 'insert', 'ticketing', 'TicketId', 1139990, '2013-04-23 14:41:43', 0, 'insert to ticketing table'),
(265, 'insert', 'customer_loyalty_points', 'LoyaltyId', 33, '2013-04-23 14:41:43', 0, 'insert to customer_loyalty_points table'),
(266, 'insert', 'discounts', 'Disc_Id', 3, '2013-04-23 15:17:51', 0, 'insert to discounts table'),
(267, 'insert', 'buses', 'BusId', 5, '2013-04-24 06:17:40', 0, 'insert to buses table'),
(268, 'update', 'bus_termini', 'TerminusId', 2, '2013-04-26 14:09:22', 0, 'update row described in bus_termini table'),
(269, 'update', 'buses', 'BusId', 4, '2013-04-29 08:30:47', 0, 'update row described in buses table'),
(270, 'update', 'bus_schedule', 'ScheduleId', 15, '2013-05-01 11:44:45', 0, 'update row described in bus_schedule table'),
(271, 'update', 'bus_schedule', 'ScheduleId', 25, '2013-05-01 12:21:45', 0, 'update row described in bus_schedule table'),
(272, 'update', 'bus_schedule', 'ScheduleId', 24, '2013-05-01 12:21:56', 0, 'update row described in bus_schedule table'),
(273, 'update', 'bus_schedule', 'ScheduleId', 23, '2013-05-01 12:26:03', 0, 'update row described in bus_schedule table'),
(274, 'update', 'bus_schedule', 'ScheduleId', 15, '2013-05-01 12:27:42', 0, 'update row described in bus_schedule table'),
(275, 'update', 'bus_schedule', 'ScheduleId', 22, '2013-05-01 12:27:52', 0, 'update row described in bus_schedule table'),
(276, 'update', 'bus_schedule', 'ScheduleId', 16, '2013-05-01 12:28:06', 0, 'update row described in bus_schedule table'),
(277, 'update', 'bus_schedule', 'ScheduleId', 15, '2013-05-01 12:28:20', 0, 'update row described in bus_schedule table'),
(278, 'insert', 'ticketing', 'TicketId', 1139991, '2013-05-01 13:38:09', 0, 'insert to ticketing table'),
(279, 'insert', 'customer_loyalty_points', 'LoyaltyId', 34, '2013-05-01 13:38:09', 0, 'insert to customer_loyalty_points table'),
(280, 'insert', 'ticketing', 'TicketId', 1139992, '2013-05-01 13:40:58', 0, 'insert to ticketing table'),
(281, 'insert', 'customer_loyalty_points', 'LoyaltyId', 35, '2013-05-01 13:40:58', 0, 'insert to customer_loyalty_points table'),
(282, 'insert', 'ticketing', 'TicketId', 1139993, '2013-05-02 12:49:25', 0, 'insert to ticketing table'),
(283, 'insert', 'ticketing', 'TicketId', 1139994, '2013-05-02 12:49:25', 0, 'insert to ticketing table'),
(284, 'insert', 'ticketing', 'TicketId', 1139995, '2013-05-02 15:09:59', 0, 'insert to ticketing table'),
(285, 'insert', 'customer_loyalty_points', 'LoyaltyId', 36, '2013-05-02 15:09:59', 0, 'insert to customer_loyalty_points table'),
(286, 'insert', 'ticketing', 'TicketId', 1139996, '2013-05-02 15:09:59', 0, 'insert to ticketing table'),
(287, 'insert', 'customer_loyalty_points', 'LoyaltyId', 37, '2013-05-02 15:09:59', 0, 'insert to customer_loyalty_points table'),
(288, 'insert', 'ticketing', 'TicketId', 1139997, '2013-05-02 15:09:59', 0, 'insert to ticketing table'),
(289, 'insert', 'customer_loyalty_points', 'LoyaltyId', 38, '2013-05-02 15:09:59', 0, 'insert to customer_loyalty_points table'),
(290, 'update', 'route_prices', 'PriceId', 7, '2013-05-03 12:02:52', 0, 'update row described in route_prices table'),
(291, 'insert', 'season_lkp', 'SeasonId', 2, '2013-05-03 12:25:16', 0, 'insert to season_lkp table'),
(292, 'update', 'season_lkp', 'SeasonId', 0, '2013-05-03 12:43:02', 0, 'update row described in season_lkp table'),
(293, 'update', 'season_lkp', 'SeasonId', 0, '2013-05-03 12:43:13', 0, 'update row described in season_lkp table'),
(294, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 12:44:38', 0, 'update row described in season_lkp table'),
(295, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 12:44:49', 0, 'update row described in season_lkp table'),
(296, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 12:44:58', 0, 'update row described in season_lkp table'),
(297, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 13:21:19', 0, 'update row described in season_lkp table'),
(298, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 22:18:15', 0, 'update row described in season_lkp table'),
(299, 'insert', 'season_lkp', 'SeasonId', 3, '2013-05-03 22:19:19', 0, 'insert to season_lkp table'),
(300, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-03 22:22:21', 0, 'update row described in season_lkp table'),
(301, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 22:24:27', 0, 'update row described in season_lkp table'),
(302, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 22:26:24', 0, 'update row described in season_lkp table'),
(303, 'insert', 'season_lkp', 'SeasonId', 4, '2013-05-03 22:31:28', 0, 'insert to season_lkp table'),
(304, 'update', 'season_lkp', 'SeasonId', 4, '2013-05-03 22:31:35', 0, 'update row described in season_lkp table'),
(305, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 22:41:44', 0, 'update row described in season_lkp table'),
(306, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 22:46:23', 0, 'update row described in season_lkp table'),
(307, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 22:46:28', 0, 'update row described in season_lkp table'),
(308, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-03 22:46:32', 0, 'update row described in season_lkp table'),
(309, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-03 22:46:39', 0, 'update row described in season_lkp table'),
(310, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 22:48:44', 0, 'update row described in season_lkp table'),
(311, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 22:48:48', 0, 'update row described in season_lkp table'),
(312, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-03 22:48:53', 0, 'update row described in season_lkp table'),
(313, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 22:49:54', 0, 'update row described in season_lkp table'),
(314, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-03 23:05:23', 0, 'update row described in season_lkp table'),
(315, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 23:05:34', 0, 'update row described in season_lkp table'),
(316, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-03 23:05:41', 0, 'update row described in season_lkp table'),
(317, 'insert', 'bus_schedule', 'ScheduleId', 26, '2013-05-05 09:30:10', 0, 'insert to bus_schedule table'),
(318, 'insert', 'bus_schedule', 'ScheduleId', 27, '2013-05-05 09:30:10', 0, 'insert to bus_schedule table'),
(319, 'insert', 'route_prices', 'PriceId', 10, '2013-05-05 09:31:48', 0, 'insert to route_prices table'),
(320, 'insert', 'route_prices', 'PriceId', 11, '2013-05-05 09:32:42', 0, 'insert to route_prices table'),
(321, 'insert', 'route_prices', 'PriceId', 12, '2013-05-05 09:33:07', 0, 'insert to route_prices table'),
(322, 'update', 'buses', 'BusId', 1, '2013-05-06 08:56:02', 1, 'update row described in buses table'),
(323, 'insert', 'route_prices', 'PriceId', 13, '2013-05-06 10:26:56', 1, 'insert to route_prices table'),
(324, 'update', 'route_prices', 'PriceId', 7, '2013-05-06 10:26:56', 1, 'update row described in route_prices table'),
(325, 'update', 'route_prices', 'PriceId', 4, '2013-05-06 10:26:56', 1, 'update row described in route_prices table'),
(326, 'insert', 'route_prices', 'PriceId', 14, '2013-05-06 10:26:56', 1, 'insert to route_prices table'),
(327, 'insert', 'route_prices', 'PriceId', 15, '2013-05-06 10:27:33', 1, 'insert to route_prices table'),
(328, 'update', 'route_prices', 'PriceId', 8, '2013-05-06 10:27:33', 1, 'update row described in route_prices table'),
(329, 'update', 'route_prices', 'PriceId', 5, '2013-05-06 10:27:33', 1, 'update row described in route_prices table'),
(330, 'insert', 'route_prices', 'PriceId', 16, '2013-05-06 10:27:33', 1, 'insert to route_prices table'),
(331, 'insert', 'route_prices', 'PriceId', 17, '2013-05-06 10:31:38', 1, 'insert to route_prices table'),
(332, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:31:38', 1, 'update row described in route_prices table'),
(333, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:31:38', 1, 'update row described in route_prices table'),
(334, 'insert', 'route_prices', 'PriceId', 18, '2013-05-06 10:31:38', 1, 'insert to route_prices table'),
(335, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:33:57', 1, 'update row described in route_prices table'),
(336, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:33:57', 1, 'update row described in route_prices table'),
(337, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:35:18', 1, 'update row described in route_prices table'),
(338, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:35:18', 1, 'update row described in route_prices table'),
(339, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:35:41', 1, 'update row described in route_prices table'),
(340, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:35:41', 1, 'update row described in route_prices table'),
(341, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:36:15', 1, 'update row described in route_prices table'),
(342, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:36:15', 1, 'update row described in route_prices table'),
(343, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:37:43', 1, 'update row described in route_prices table'),
(344, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:37:43', 1, 'update row described in route_prices table'),
(345, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:41:00', 1, 'update row described in route_prices table'),
(346, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:41:00', 1, 'update row described in route_prices table'),
(347, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:41:50', 1, 'update row described in route_prices table'),
(348, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:41:50', 1, 'update row described in route_prices table'),
(349, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 10:44:27', 1, 'update row described in route_prices table'),
(350, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:44:27', 1, 'update row described in route_prices table'),
(351, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:49:01', 1, 'update row described in route_prices table'),
(352, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 10:49:01', 1, 'update row described in route_prices table'),
(353, 'insert', 'route_prices', 'PriceId', 13, '2013-05-06 10:50:45', 1, 'insert to route_prices table'),
(354, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 10:50:45', 1, 'update row described in route_prices table'),
(355, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:15:37', 1, 'update row described in route_prices table'),
(356, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:15:37', 1, 'update row described in route_prices table'),
(357, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:24:01', 1, 'update row described in route_prices table'),
(358, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:24:01', 1, 'update row described in route_prices table'),
(359, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:27:52', 1, 'update row described in route_prices table'),
(360, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:27:52', 1, 'update row described in route_prices table'),
(361, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:28:29', 1, 'update row described in route_prices table'),
(362, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:28:29', 1, 'update row described in route_prices table'),
(363, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:28:53', 1, 'update row described in route_prices table'),
(364, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:28:53', 1, 'update row described in route_prices table'),
(365, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:29:09', 1, 'update row described in route_prices table'),
(366, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:29:09', 1, 'update row described in route_prices table'),
(367, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:32:14', 1, 'update row described in route_prices table'),
(368, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:32:14', 1, 'update row described in route_prices table'),
(369, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:32:25', 1, 'update row described in route_prices table'),
(370, 'update', 'route_prices', 'PriceId', 13, '2013-05-06 11:32:25', 1, 'update row described in route_prices table'),
(371, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 11:39:33', 1, 'update row described in route_prices table'),
(372, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 11:39:33', 1, 'update row described in route_prices table'),
(373, 'update', 'route_prices', 'PriceId', 9, '2013-05-06 11:39:42', 1, 'update row described in route_prices table'),
(374, 'update', 'route_prices', 'PriceId', 6, '2013-05-06 11:39:42', 1, 'update row described in route_prices table'),
(375, 'insert', 'route_prices', 'PriceId', 14, '2013-05-06 11:40:09', 1, 'insert to route_prices table'),
(376, 'update', 'route_prices', 'PriceId', 14, '2013-05-06 11:40:09', 1, 'update row described in route_prices table'),
(377, 'insert', 'route_prices', 'PriceId', 15, '2013-05-06 11:41:24', 1, 'insert to route_prices table'),
(378, 'update', 'route_prices', 'PriceId', 15, '2013-05-06 11:41:24', 1, 'update row described in route_prices table'),
(379, 'insert', 'route_prices', 'PriceId', 16, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(380, 'insert', 'route_prices', 'PriceId', 17, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(381, 'insert', 'route_prices', 'PriceId', 18, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(382, 'insert', 'route_prices', 'PriceId', 19, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(383, 'insert', 'route_prices', 'PriceId', 20, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(384, 'insert', 'route_prices', 'PriceId', 21, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(385, 'insert', 'route_prices', 'PriceId', 22, '2013-05-06 11:42:27', 1, 'insert to route_prices table'),
(386, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 11:42:27', 1, 'update row described in route_prices table'),
(387, 'insert', 'route_prices', 'PriceId', 23, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(388, 'insert', 'route_prices', 'PriceId', 24, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(389, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(390, 'insert', 'route_prices', 'PriceId', 25, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(391, 'insert', 'route_prices', 'PriceId', 26, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(392, 'insert', 'route_prices', 'PriceId', 27, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(393, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(394, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(395, 'insert', 'route_prices', 'PriceId', 28, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(396, 'insert', 'route_prices', 'PriceId', 29, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(397, 'insert', 'route_prices', 'PriceId', 30, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(398, 'insert', 'route_prices', 'PriceId', 31, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(399, 'insert', 'route_prices', 'PriceId', 32, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(400, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(401, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(402, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(403, 'insert', 'route_prices', 'PriceId', 33, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(404, 'insert', 'route_prices', 'PriceId', 34, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(405, 'insert', 'route_prices', 'PriceId', 35, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(406, 'insert', 'route_prices', 'PriceId', 36, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(407, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(408, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(409, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(410, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(411, 'insert', 'route_prices', 'PriceId', 37, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(412, 'insert', 'route_prices', 'PriceId', 38, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(413, 'insert', 'route_prices', 'PriceId', 39, '2013-05-06 11:42:28', 1, 'insert to route_prices table'),
(414, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(415, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(416, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 11:42:28', 1, 'update row described in route_prices table'),
(417, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(418, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(419, 'insert', 'route_prices', 'PriceId', 40, '2013-05-06 11:42:29', 1, 'insert to route_prices table'),
(420, 'insert', 'route_prices', 'PriceId', 41, '2013-05-06 11:42:29', 1, 'insert to route_prices table'),
(421, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(422, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(423, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(424, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(425, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(426, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(427, 'insert', 'route_prices', 'PriceId', 42, '2013-05-06 11:42:29', 1, 'insert to route_prices table'),
(428, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(429, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(430, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(431, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(432, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(433, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(434, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 11:42:29', 1, 'update row described in route_prices table'),
(435, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(436, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(437, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(438, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(439, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(440, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(441, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(442, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(443, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(444, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 11:43:40', 1, 'update row described in route_prices table');
INSERT INTO `transaction_log` (`transaction_log_id`, `transaction_log_name`, `table_name`, `pkName`, `pkValue_id`, `timestamp`, `user_id`, `description`) VALUES
(445, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(446, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 11:43:40', 1, 'update row described in route_prices table'),
(447, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(448, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(449, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(450, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(451, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(452, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(453, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(454, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(455, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(456, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(457, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(458, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(459, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(460, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(461, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(462, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(463, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(464, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(465, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(466, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(467, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(468, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(469, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(470, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(471, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(472, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(473, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(474, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(475, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(476, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 11:43:41', 1, 'update row described in route_prices table'),
(477, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(478, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(479, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(480, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(481, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(482, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(483, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(484, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(485, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(486, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(487, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(488, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(489, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(490, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 11:43:42', 1, 'update row described in route_prices table'),
(491, 'insert', 'season_lkp', 'SeasonId', 5, '2013-05-06 12:06:26', 1, 'insert to season_lkp table'),
(492, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-06 12:09:42', 1, 'update row described in season_lkp table'),
(493, 'insert', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:22:01', 1, 'insert to season_lkp table'),
(494, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:23:55', 1, 'update row described in season_lkp table'),
(495, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:24:06', 1, 'update row described in season_lkp table'),
(496, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:28:45', 1, 'update row described in season_lkp table'),
(497, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:28:55', 1, 'update row described in season_lkp table'),
(498, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 12:29:27', 1, 'update row described in season_lkp table'),
(499, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-06 12:41:02', 1, 'update row described in season_lkp table'),
(500, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-06 12:41:06', 1, 'update row described in season_lkp table'),
(501, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-06 12:41:09', 1, 'update row described in season_lkp table'),
(502, 'update', 'season_lkp', 'SeasonId', 3, '2013-05-06 12:41:13', 1, 'update row described in season_lkp table'),
(503, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-06 12:41:20', 1, 'update row described in season_lkp table'),
(504, 'update', 'season_lkp', 'SeasonId', 5, '2013-05-06 12:41:24', 1, 'update row described in season_lkp table'),
(505, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-06 12:41:32', 1, 'update row described in season_lkp table'),
(506, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-06 12:49:11', 1, 'update row described in season_lkp table'),
(507, 'update', 'season_lkp', 'SeasonId', 2, '2013-05-06 12:49:29', 1, 'update row described in season_lkp table'),
(508, 'update', 'season_lkp', 'SeasonId', 1, '2013-05-06 12:56:32', 1, 'update row described in season_lkp table'),
(509, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(510, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(511, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(512, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(513, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(514, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(515, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(516, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(517, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(518, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(519, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(520, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(521, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(522, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(523, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(524, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(525, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(526, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(527, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(528, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(529, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(530, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(531, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 13:05:52', 1, 'update row described in route_prices table'),
(532, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(533, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(534, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(535, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(536, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(537, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(538, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(539, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(540, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(541, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(542, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(543, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(544, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(545, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(546, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(547, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(548, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(549, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(550, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(551, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(552, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(553, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(554, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(555, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 13:05:53', 1, 'update row described in route_prices table'),
(556, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(557, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(558, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(559, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(560, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(561, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(562, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(563, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(564, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 13:05:54', 1, 'update row described in route_prices table'),
(565, 'update', 'season_lkp', 'SeasonId', 5, '2013-05-06 13:06:08', 1, 'update row described in season_lkp table'),
(566, 'update', 'bus_schedule', 'ScheduleId', 27, '2013-05-06 20:30:46', 1, 'update row described in bus_schedule table'),
(567, 'insert', 'route_prices', 'PriceId', 43, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(568, 'insert', 'route_prices', 'PriceId', 44, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(569, 'insert', 'route_prices', 'PriceId', 45, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(570, 'insert', 'route_prices', 'PriceId', 46, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(571, 'insert', 'route_prices', 'PriceId', 47, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(572, 'insert', 'route_prices', 'PriceId', 48, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(573, 'insert', 'route_prices', 'PriceId', 49, '2013-05-06 20:32:01', 1, 'insert to route_prices table'),
(574, 'update', 'route_prices', 'PriceId', 43, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(575, 'insert', 'route_prices', 'PriceId', 50, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(576, 'insert', 'route_prices', 'PriceId', 51, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(577, 'insert', 'route_prices', 'PriceId', 52, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(578, 'insert', 'route_prices', 'PriceId', 53, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(579, 'insert', 'route_prices', 'PriceId', 54, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(580, 'insert', 'route_prices', 'PriceId', 55, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(581, 'update', 'route_prices', 'PriceId', 44, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(582, 'update', 'route_prices', 'PriceId', 50, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(583, 'insert', 'route_prices', 'PriceId', 56, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(584, 'insert', 'route_prices', 'PriceId', 57, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(585, 'insert', 'route_prices', 'PriceId', 58, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(586, 'insert', 'route_prices', 'PriceId', 59, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(587, 'insert', 'route_prices', 'PriceId', 60, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(588, 'update', 'route_prices', 'PriceId', 45, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(589, 'update', 'route_prices', 'PriceId', 51, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(590, 'update', 'route_prices', 'PriceId', 56, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(591, 'insert', 'route_prices', 'PriceId', 61, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(592, 'insert', 'route_prices', 'PriceId', 62, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(593, 'insert', 'route_prices', 'PriceId', 63, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(594, 'insert', 'route_prices', 'PriceId', 64, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(595, 'update', 'route_prices', 'PriceId', 46, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(596, 'update', 'route_prices', 'PriceId', 52, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(597, 'update', 'route_prices', 'PriceId', 57, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(598, 'update', 'route_prices', 'PriceId', 61, '2013-05-06 20:32:02', 1, 'update row described in route_prices table'),
(599, 'insert', 'route_prices', 'PriceId', 65, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(600, 'insert', 'route_prices', 'PriceId', 66, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(601, 'insert', 'route_prices', 'PriceId', 67, '2013-05-06 20:32:02', 1, 'insert to route_prices table'),
(602, 'update', 'route_prices', 'PriceId', 47, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(603, 'update', 'route_prices', 'PriceId', 53, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(604, 'update', 'route_prices', 'PriceId', 58, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(605, 'update', 'route_prices', 'PriceId', 62, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(606, 'update', 'route_prices', 'PriceId', 65, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(607, 'insert', 'route_prices', 'PriceId', 68, '2013-05-06 20:32:03', 1, 'insert to route_prices table'),
(608, 'insert', 'route_prices', 'PriceId', 69, '2013-05-06 20:32:03', 1, 'insert to route_prices table'),
(609, 'update', 'route_prices', 'PriceId', 48, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(610, 'update', 'route_prices', 'PriceId', 54, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(611, 'update', 'route_prices', 'PriceId', 59, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(612, 'update', 'route_prices', 'PriceId', 63, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(613, 'update', 'route_prices', 'PriceId', 66, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(614, 'update', 'route_prices', 'PriceId', 68, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(615, 'insert', 'route_prices', 'PriceId', 70, '2013-05-06 20:32:03', 1, 'insert to route_prices table'),
(616, 'update', 'route_prices', 'PriceId', 49, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(617, 'update', 'route_prices', 'PriceId', 55, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(618, 'update', 'route_prices', 'PriceId', 60, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(619, 'update', 'route_prices', 'PriceId', 64, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(620, 'update', 'route_prices', 'PriceId', 67, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(621, 'update', 'route_prices', 'PriceId', 69, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(622, 'update', 'route_prices', 'PriceId', 70, '2013-05-06 20:32:03', 1, 'update row described in route_prices table'),
(623, 'update', 'route_prices', 'PriceId', 43, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(624, 'update', 'route_prices', 'PriceId', 44, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(625, 'update', 'route_prices', 'PriceId', 45, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(626, 'update', 'route_prices', 'PriceId', 46, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(627, 'update', 'route_prices', 'PriceId', 47, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(628, 'update', 'route_prices', 'PriceId', 48, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(629, 'update', 'route_prices', 'PriceId', 49, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(630, 'update', 'route_prices', 'PriceId', 43, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(631, 'update', 'route_prices', 'PriceId', 50, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(632, 'update', 'route_prices', 'PriceId', 51, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(633, 'update', 'route_prices', 'PriceId', 52, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(634, 'update', 'route_prices', 'PriceId', 53, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(635, 'update', 'route_prices', 'PriceId', 54, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(636, 'update', 'route_prices', 'PriceId', 55, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(637, 'update', 'route_prices', 'PriceId', 44, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(638, 'update', 'route_prices', 'PriceId', 50, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(639, 'update', 'route_prices', 'PriceId', 56, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(640, 'update', 'route_prices', 'PriceId', 57, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(641, 'update', 'route_prices', 'PriceId', 58, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(642, 'update', 'route_prices', 'PriceId', 59, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(643, 'update', 'route_prices', 'PriceId', 60, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(644, 'update', 'route_prices', 'PriceId', 45, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(645, 'update', 'route_prices', 'PriceId', 51, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(646, 'update', 'route_prices', 'PriceId', 56, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(647, 'update', 'route_prices', 'PriceId', 61, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(648, 'update', 'route_prices', 'PriceId', 62, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(649, 'update', 'route_prices', 'PriceId', 63, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(650, 'update', 'route_prices', 'PriceId', 64, '2013-05-06 20:33:54', 1, 'update row described in route_prices table'),
(651, 'update', 'route_prices', 'PriceId', 46, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(652, 'update', 'route_prices', 'PriceId', 52, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(653, 'update', 'route_prices', 'PriceId', 57, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(654, 'update', 'route_prices', 'PriceId', 61, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(655, 'update', 'route_prices', 'PriceId', 65, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(656, 'update', 'route_prices', 'PriceId', 66, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(657, 'update', 'route_prices', 'PriceId', 67, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(658, 'update', 'route_prices', 'PriceId', 47, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(659, 'update', 'route_prices', 'PriceId', 53, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(660, 'update', 'route_prices', 'PriceId', 58, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(661, 'update', 'route_prices', 'PriceId', 62, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(662, 'update', 'route_prices', 'PriceId', 65, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(663, 'update', 'route_prices', 'PriceId', 68, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(664, 'update', 'route_prices', 'PriceId', 69, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(665, 'update', 'route_prices', 'PriceId', 48, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(666, 'update', 'route_prices', 'PriceId', 54, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(667, 'update', 'route_prices', 'PriceId', 59, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(668, 'update', 'route_prices', 'PriceId', 63, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(669, 'update', 'route_prices', 'PriceId', 66, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(670, 'update', 'route_prices', 'PriceId', 68, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(671, 'update', 'route_prices', 'PriceId', 70, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(672, 'update', 'route_prices', 'PriceId', 49, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(673, 'update', 'route_prices', 'PriceId', 55, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(674, 'update', 'route_prices', 'PriceId', 60, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(675, 'update', 'route_prices', 'PriceId', 64, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(676, 'update', 'route_prices', 'PriceId', 67, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(677, 'update', 'route_prices', 'PriceId', 69, '2013-05-06 20:33:55', 1, 'update row described in route_prices table'),
(678, 'update', 'route_prices', 'PriceId', 70, '2013-05-06 20:33:56', 1, 'update row described in route_prices table'),
(679, 'insert', 'route_prices', 'PriceId', 71, '2013-05-06 22:19:45', 1, 'insert to route_prices table'),
(680, 'insert', 'route_prices', 'PriceId', 72, '2013-05-06 22:19:45', 1, 'insert to route_prices table'),
(681, 'insert', 'route_prices', 'PriceId', 73, '2013-05-06 22:19:45', 1, 'insert to route_prices table'),
(682, 'insert', 'route_prices', 'PriceId', 74, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(683, 'insert', 'route_prices', 'PriceId', 75, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(684, 'insert', 'route_prices', 'PriceId', 76, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(685, 'insert', 'route_prices', 'PriceId', 77, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(686, 'update', 'route_prices', 'PriceId', 71, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(687, 'insert', 'route_prices', 'PriceId', 78, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(688, 'insert', 'route_prices', 'PriceId', 79, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(689, 'insert', 'route_prices', 'PriceId', 80, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(690, 'insert', 'route_prices', 'PriceId', 81, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(691, 'insert', 'route_prices', 'PriceId', 82, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(692, 'insert', 'route_prices', 'PriceId', 83, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(693, 'update', 'route_prices', 'PriceId', 72, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(694, 'update', 'route_prices', 'PriceId', 78, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(695, 'insert', 'route_prices', 'PriceId', 84, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(696, 'insert', 'route_prices', 'PriceId', 85, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(697, 'insert', 'route_prices', 'PriceId', 86, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(698, 'insert', 'route_prices', 'PriceId', 87, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(699, 'insert', 'route_prices', 'PriceId', 88, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(700, 'update', 'route_prices', 'PriceId', 73, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(701, 'update', 'route_prices', 'PriceId', 79, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(702, 'update', 'route_prices', 'PriceId', 84, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(703, 'insert', 'route_prices', 'PriceId', 89, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(704, 'insert', 'route_prices', 'PriceId', 90, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(705, 'insert', 'route_prices', 'PriceId', 91, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(706, 'insert', 'route_prices', 'PriceId', 92, '2013-05-06 22:19:46', 1, 'insert to route_prices table'),
(707, 'update', 'route_prices', 'PriceId', 74, '2013-05-06 22:19:46', 1, 'update row described in route_prices table'),
(708, 'update', 'route_prices', 'PriceId', 80, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(709, 'update', 'route_prices', 'PriceId', 85, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(710, 'update', 'route_prices', 'PriceId', 89, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(711, 'insert', 'route_prices', 'PriceId', 93, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(712, 'insert', 'route_prices', 'PriceId', 94, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(713, 'insert', 'route_prices', 'PriceId', 95, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(714, 'update', 'route_prices', 'PriceId', 75, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(715, 'update', 'route_prices', 'PriceId', 81, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(716, 'update', 'route_prices', 'PriceId', 86, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(717, 'update', 'route_prices', 'PriceId', 90, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(718, 'update', 'route_prices', 'PriceId', 93, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(719, 'insert', 'route_prices', 'PriceId', 96, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(720, 'insert', 'route_prices', 'PriceId', 97, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(721, 'update', 'route_prices', 'PriceId', 76, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(722, 'update', 'route_prices', 'PriceId', 82, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(723, 'update', 'route_prices', 'PriceId', 87, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(724, 'update', 'route_prices', 'PriceId', 91, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(725, 'update', 'route_prices', 'PriceId', 94, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(726, 'update', 'route_prices', 'PriceId', 96, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(727, 'insert', 'route_prices', 'PriceId', 98, '2013-05-06 22:19:47', 1, 'insert to route_prices table'),
(728, 'update', 'route_prices', 'PriceId', 77, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(729, 'update', 'route_prices', 'PriceId', 83, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(730, 'update', 'route_prices', 'PriceId', 88, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(731, 'update', 'route_prices', 'PriceId', 92, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(732, 'update', 'route_prices', 'PriceId', 95, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(733, 'update', 'route_prices', 'PriceId', 97, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(734, 'update', 'route_prices', 'PriceId', 98, '2013-05-06 22:19:47', 1, 'update row described in route_prices table'),
(735, 'update', 'route_prices', 'PriceId', 71, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(736, 'update', 'route_prices', 'PriceId', 72, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(737, 'update', 'route_prices', 'PriceId', 73, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(738, 'update', 'route_prices', 'PriceId', 74, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(739, 'update', 'route_prices', 'PriceId', 75, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(740, 'update', 'route_prices', 'PriceId', 76, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(741, 'update', 'route_prices', 'PriceId', 77, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(742, 'update', 'route_prices', 'PriceId', 71, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(743, 'update', 'route_prices', 'PriceId', 78, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(744, 'update', 'route_prices', 'PriceId', 79, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(745, 'update', 'route_prices', 'PriceId', 80, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(746, 'update', 'route_prices', 'PriceId', 81, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(747, 'update', 'route_prices', 'PriceId', 82, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(748, 'update', 'route_prices', 'PriceId', 83, '2013-05-06 22:21:28', 1, 'update row described in route_prices table'),
(749, 'update', 'route_prices', 'PriceId', 72, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(750, 'update', 'route_prices', 'PriceId', 78, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(751, 'update', 'route_prices', 'PriceId', 84, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(752, 'update', 'route_prices', 'PriceId', 85, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(753, 'update', 'route_prices', 'PriceId', 86, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(754, 'update', 'route_prices', 'PriceId', 87, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(755, 'update', 'route_prices', 'PriceId', 88, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(756, 'update', 'route_prices', 'PriceId', 73, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(757, 'update', 'route_prices', 'PriceId', 79, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(758, 'update', 'route_prices', 'PriceId', 84, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(759, 'update', 'route_prices', 'PriceId', 89, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(760, 'update', 'route_prices', 'PriceId', 90, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(761, 'update', 'route_prices', 'PriceId', 91, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(762, 'update', 'route_prices', 'PriceId', 92, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(763, 'update', 'route_prices', 'PriceId', 74, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(764, 'update', 'route_prices', 'PriceId', 80, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(765, 'update', 'route_prices', 'PriceId', 85, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(766, 'update', 'route_prices', 'PriceId', 89, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(767, 'update', 'route_prices', 'PriceId', 93, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(768, 'update', 'route_prices', 'PriceId', 94, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(769, 'update', 'route_prices', 'PriceId', 95, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(770, 'update', 'route_prices', 'PriceId', 75, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(771, 'update', 'route_prices', 'PriceId', 81, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(772, 'update', 'route_prices', 'PriceId', 86, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(773, 'update', 'route_prices', 'PriceId', 90, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(774, 'update', 'route_prices', 'PriceId', 93, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(775, 'update', 'route_prices', 'PriceId', 96, '2013-05-06 22:21:29', 1, 'update row described in route_prices table'),
(776, 'update', 'route_prices', 'PriceId', 97, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(777, 'update', 'route_prices', 'PriceId', 76, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(778, 'update', 'route_prices', 'PriceId', 82, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(779, 'update', 'route_prices', 'PriceId', 87, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(780, 'update', 'route_prices', 'PriceId', 91, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(781, 'update', 'route_prices', 'PriceId', 94, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(782, 'update', 'route_prices', 'PriceId', 96, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(783, 'update', 'route_prices', 'PriceId', 98, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(784, 'update', 'route_prices', 'PriceId', 77, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(785, 'update', 'route_prices', 'PriceId', 83, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(786, 'update', 'route_prices', 'PriceId', 88, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(787, 'update', 'route_prices', 'PriceId', 92, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(788, 'update', 'route_prices', 'PriceId', 95, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(789, 'update', 'route_prices', 'PriceId', 97, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(790, 'update', 'route_prices', 'PriceId', 98, '2013-05-06 22:21:30', 1, 'update row described in route_prices table'),
(791, 'update', 'route_prices', 'PriceId', 71, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(792, 'update', 'route_prices', 'PriceId', 72, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(793, 'update', 'route_prices', 'PriceId', 73, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(794, 'update', 'route_prices', 'PriceId', 74, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(795, 'update', 'route_prices', 'PriceId', 75, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(796, 'update', 'route_prices', 'PriceId', 76, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(797, 'update', 'route_prices', 'PriceId', 77, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(798, 'update', 'route_prices', 'PriceId', 71, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(799, 'update', 'route_prices', 'PriceId', 78, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(800, 'update', 'route_prices', 'PriceId', 79, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(801, 'update', 'route_prices', 'PriceId', 80, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(802, 'update', 'route_prices', 'PriceId', 81, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(803, 'update', 'route_prices', 'PriceId', 82, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(804, 'update', 'route_prices', 'PriceId', 83, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(805, 'update', 'route_prices', 'PriceId', 72, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(806, 'update', 'route_prices', 'PriceId', 78, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(807, 'update', 'route_prices', 'PriceId', 84, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(808, 'update', 'route_prices', 'PriceId', 85, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(809, 'update', 'route_prices', 'PriceId', 86, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(810, 'update', 'route_prices', 'PriceId', 87, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(811, 'update', 'route_prices', 'PriceId', 88, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(812, 'update', 'route_prices', 'PriceId', 73, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(813, 'update', 'route_prices', 'PriceId', 79, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(814, 'update', 'route_prices', 'PriceId', 84, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(815, 'update', 'route_prices', 'PriceId', 89, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(816, 'update', 'route_prices', 'PriceId', 90, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(817, 'update', 'route_prices', 'PriceId', 91, '2013-05-06 22:22:04', 1, 'update row described in route_prices table'),
(818, 'update', 'route_prices', 'PriceId', 92, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(819, 'update', 'route_prices', 'PriceId', 74, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(820, 'update', 'route_prices', 'PriceId', 80, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(821, 'update', 'route_prices', 'PriceId', 85, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(822, 'update', 'route_prices', 'PriceId', 89, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(823, 'update', 'route_prices', 'PriceId', 93, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(824, 'update', 'route_prices', 'PriceId', 94, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(825, 'update', 'route_prices', 'PriceId', 95, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(826, 'update', 'route_prices', 'PriceId', 75, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(827, 'update', 'route_prices', 'PriceId', 81, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(828, 'update', 'route_prices', 'PriceId', 86, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(829, 'update', 'route_prices', 'PriceId', 90, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(830, 'update', 'route_prices', 'PriceId', 93, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(831, 'update', 'route_prices', 'PriceId', 96, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(832, 'update', 'route_prices', 'PriceId', 97, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(833, 'update', 'route_prices', 'PriceId', 76, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(834, 'update', 'route_prices', 'PriceId', 82, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(835, 'update', 'route_prices', 'PriceId', 87, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(836, 'update', 'route_prices', 'PriceId', 91, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(837, 'update', 'route_prices', 'PriceId', 94, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(838, 'update', 'route_prices', 'PriceId', 96, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(839, 'update', 'route_prices', 'PriceId', 98, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(840, 'update', 'route_prices', 'PriceId', 77, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(841, 'update', 'route_prices', 'PriceId', 83, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(842, 'update', 'route_prices', 'PriceId', 88, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(843, 'update', 'route_prices', 'PriceId', 92, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(844, 'update', 'route_prices', 'PriceId', 95, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(845, 'update', 'route_prices', 'PriceId', 97, '2013-05-06 22:22:05', 1, 'update row described in route_prices table'),
(846, 'update', 'route_prices', 'PriceId', 98, '2013-05-06 22:22:06', 1, 'update row described in route_prices table'),
(847, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(848, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(849, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(850, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(851, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(852, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(853, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(854, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(855, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(856, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(857, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(858, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(859, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(860, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(861, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(862, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(863, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(864, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(865, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(866, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(867, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(868, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(869, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(870, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(871, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 22:22:37', 1, 'update row described in route_prices table'),
(872, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(873, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 22:22:38', 1, 'update row described in route_prices table');
INSERT INTO `transaction_log` (`transaction_log_id`, `transaction_log_name`, `table_name`, `pkName`, `pkValue_id`, `timestamp`, `user_id`, `description`) VALUES
(874, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(875, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(876, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(877, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(878, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(879, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(880, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(881, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(882, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(883, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(884, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(885, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(886, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(887, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(888, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(889, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(890, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(891, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(892, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(893, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(894, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(895, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(896, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(897, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(898, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(899, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 22:22:38', 1, 'update row described in route_prices table'),
(900, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 22:22:39', 1, 'update row described in route_prices table'),
(901, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 22:22:39', 1, 'update row described in route_prices table'),
(902, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 22:22:39', 1, 'update row described in route_prices table'),
(903, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(904, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(905, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(906, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(907, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(908, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(909, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(910, 'update', 'route_prices', 'PriceId', 16, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(911, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(912, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(913, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(914, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(915, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(916, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(917, 'update', 'route_prices', 'PriceId', 17, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(918, 'update', 'route_prices', 'PriceId', 23, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(919, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(920, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(921, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(922, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 22:23:13', 1, 'update row described in route_prices table'),
(923, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(924, 'update', 'route_prices', 'PriceId', 18, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(925, 'update', 'route_prices', 'PriceId', 24, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(926, 'update', 'route_prices', 'PriceId', 28, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(927, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(928, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(929, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(930, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(931, 'update', 'route_prices', 'PriceId', 19, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(932, 'update', 'route_prices', 'PriceId', 10, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(933, 'update', 'route_prices', 'PriceId', 29, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(934, 'update', 'route_prices', 'PriceId', 33, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(935, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(936, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(937, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(938, 'update', 'route_prices', 'PriceId', 20, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(939, 'update', 'route_prices', 'PriceId', 25, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(940, 'update', 'route_prices', 'PriceId', 30, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(941, 'update', 'route_prices', 'PriceId', 34, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(942, 'update', 'route_prices', 'PriceId', 37, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(943, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(944, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(945, 'update', 'route_prices', 'PriceId', 21, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(946, 'update', 'route_prices', 'PriceId', 26, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(947, 'update', 'route_prices', 'PriceId', 31, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(948, 'update', 'route_prices', 'PriceId', 35, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(949, 'update', 'route_prices', 'PriceId', 38, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(950, 'update', 'route_prices', 'PriceId', 40, '2013-05-06 22:23:14', 1, 'update row described in route_prices table'),
(951, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(952, 'update', 'route_prices', 'PriceId', 22, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(953, 'update', 'route_prices', 'PriceId', 27, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(954, 'update', 'route_prices', 'PriceId', 32, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(955, 'update', 'route_prices', 'PriceId', 36, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(956, 'update', 'route_prices', 'PriceId', 39, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(957, 'update', 'route_prices', 'PriceId', 41, '2013-05-06 22:23:15', 1, 'update row described in route_prices table'),
(958, 'update', 'route_prices', 'PriceId', 42, '2013-05-06 22:23:16', 1, 'update row described in route_prices table'),
(959, 'insert', 'bus_schedule', 'ScheduleId', 28, '2013-05-06 22:30:27', 1, 'insert to bus_schedule table'),
(960, 'insert', 'bus_termini', 'TerminusId', 6, '2013-05-06 22:58:41', 1, 'insert to bus_termini table'),
(961, 'insert', 'ticketing', 'TicketId', 1139998, '2013-05-06 22:58:58', 1, 'insert to ticketing table'),
(962, 'insert', 'customer_loyalty_points', 'LoyaltyId', 39, '2013-05-06 22:58:58', 1, 'insert to customer_loyalty_points table'),
(963, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-06 23:18:53', 1, 'update row described in season_lkp table'),
(964, 'insert', 'bus_staff', 'StaffId', 4, '2013-05-07 09:22:38', 1, 'insert to bus_staff table'),
(965, 'update', 'season_lkp', 'SeasonId', 6, '2013-05-07 09:46:19', 1, 'update row described in season_lkp table'),
(966, 'update', 'bus_staff', 'StaffId', 1, '2013-05-07 10:09:47', 1, 'update row described in bus_staff table'),
(967, 'update', 'bus_staff', 'StaffId', 1, '2013-05-07 10:11:54', 1, 'update row described in bus_staff table'),
(968, 'update', 'bus_staff', 'StaffId', 1, '2013-05-07 10:12:19', 1, 'update row described in bus_staff table'),
(969, 'update', 'buses', 'BusId', 1, '2013-05-07 10:37:09', 1, 'update row described in buses table'),
(970, 'update', 'route_prices', 'PriceId', 9, '2013-05-07 12:12:13', 1, 'update row described in route_prices table'),
(971, 'update', 'route_prices', 'PriceId', 6, '2013-05-07 12:12:13', 1, 'update row described in route_prices table'),
(972, 'update', 'route_prices', 'PriceId', 9, '2013-05-07 12:12:32', 1, 'update row described in route_prices table'),
(973, 'update', 'route_prices', 'PriceId', 6, '2013-05-07 12:12:32', 1, 'update row described in route_prices table'),
(974, 'update', 'route_prices', 'PriceId', 9, '2013-05-07 12:13:00', 1, 'update row described in route_prices table'),
(975, 'update', 'route_prices', 'PriceId', 6, '2013-05-07 12:13:01', 1, 'update row described in route_prices table'),
(976, 'update', 'route_prices', 'PriceId', 9, '2013-05-07 12:13:21', 1, 'update row described in route_prices table'),
(977, 'update', 'route_prices', 'PriceId', 6, '2013-05-07 12:13:21', 1, 'update row described in route_prices table'),
(978, 'update', 'route_prices', 'PriceId', 14, '2013-05-07 12:13:58', 1, 'update row described in route_prices table'),
(979, 'update', 'route_prices', 'PriceId', 14, '2013-05-07 12:13:58', 1, 'update row described in route_prices table'),
(980, 'update', 'route_prices', 'PriceId', 15, '2013-05-07 12:15:17', 1, 'update row described in route_prices table'),
(981, 'update', 'route_prices', 'PriceId', 15, '2013-05-07 12:15:17', 1, 'update row described in route_prices table'),
(982, 'update', 'route_prices', 'PriceId', 16, '2013-05-07 12:18:14', 1, 'update row described in route_prices table'),
(983, 'update', 'route_prices', 'PriceId', 17, '2013-05-07 12:18:14', 1, 'update row described in route_prices table'),
(984, 'update', 'route_prices', 'PriceId', 18, '2013-05-07 12:18:14', 1, 'update row described in route_prices table'),
(985, 'update', 'route_prices', 'PriceId', 19, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(986, 'update', 'route_prices', 'PriceId', 20, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(987, 'update', 'route_prices', 'PriceId', 21, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(988, 'update', 'route_prices', 'PriceId', 22, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(989, 'insert', 'route_prices', 'PriceId', 99, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(990, 'insert', 'route_prices', 'PriceId', 100, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(991, 'update', 'route_prices', 'PriceId', 16, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(992, 'update', 'route_prices', 'PriceId', 23, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(993, 'update', 'route_prices', 'PriceId', 24, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(994, 'update', 'route_prices', 'PriceId', 10, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(995, 'update', 'route_prices', 'PriceId', 25, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(996, 'update', 'route_prices', 'PriceId', 26, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(997, 'update', 'route_prices', 'PriceId', 27, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(998, 'insert', 'route_prices', 'PriceId', 101, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(999, 'insert', 'route_prices', 'PriceId', 102, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(1000, 'update', 'route_prices', 'PriceId', 17, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1001, 'update', 'route_prices', 'PriceId', 23, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1002, 'update', 'route_prices', 'PriceId', 28, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1003, 'update', 'route_prices', 'PriceId', 29, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1004, 'update', 'route_prices', 'PriceId', 30, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1005, 'update', 'route_prices', 'PriceId', 31, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1006, 'update', 'route_prices', 'PriceId', 32, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1007, 'insert', 'route_prices', 'PriceId', 103, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(1008, 'insert', 'route_prices', 'PriceId', 104, '2013-05-07 12:18:15', 1, 'insert to route_prices table'),
(1009, 'update', 'route_prices', 'PriceId', 18, '2013-05-07 12:18:15', 1, 'update row described in route_prices table'),
(1010, 'update', 'route_prices', 'PriceId', 24, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1011, 'update', 'route_prices', 'PriceId', 28, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1012, 'update', 'route_prices', 'PriceId', 33, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1013, 'update', 'route_prices', 'PriceId', 34, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1014, 'update', 'route_prices', 'PriceId', 35, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1015, 'update', 'route_prices', 'PriceId', 36, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1016, 'insert', 'route_prices', 'PriceId', 105, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1017, 'insert', 'route_prices', 'PriceId', 106, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1018, 'update', 'route_prices', 'PriceId', 19, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1019, 'update', 'route_prices', 'PriceId', 10, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1020, 'update', 'route_prices', 'PriceId', 29, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1021, 'update', 'route_prices', 'PriceId', 33, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1022, 'update', 'route_prices', 'PriceId', 37, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1023, 'update', 'route_prices', 'PriceId', 38, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1024, 'update', 'route_prices', 'PriceId', 39, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1025, 'insert', 'route_prices', 'PriceId', 107, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1026, 'insert', 'route_prices', 'PriceId', 108, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1027, 'update', 'route_prices', 'PriceId', 20, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1028, 'update', 'route_prices', 'PriceId', 25, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1029, 'update', 'route_prices', 'PriceId', 30, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1030, 'update', 'route_prices', 'PriceId', 34, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1031, 'update', 'route_prices', 'PriceId', 37, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1032, 'update', 'route_prices', 'PriceId', 40, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1033, 'update', 'route_prices', 'PriceId', 41, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1034, 'insert', 'route_prices', 'PriceId', 109, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1035, 'insert', 'route_prices', 'PriceId', 110, '2013-05-07 12:18:16', 1, 'insert to route_prices table'),
(1036, 'update', 'route_prices', 'PriceId', 21, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1037, 'update', 'route_prices', 'PriceId', 26, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1038, 'update', 'route_prices', 'PriceId', 31, '2013-05-07 12:18:16', 1, 'update row described in route_prices table'),
(1039, 'update', 'route_prices', 'PriceId', 35, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1040, 'update', 'route_prices', 'PriceId', 38, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1041, 'update', 'route_prices', 'PriceId', 40, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1042, 'update', 'route_prices', 'PriceId', 42, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1043, 'insert', 'route_prices', 'PriceId', 111, '2013-05-07 12:18:17', 1, 'insert to route_prices table'),
(1044, 'insert', 'route_prices', 'PriceId', 112, '2013-05-07 12:18:17', 1, 'insert to route_prices table'),
(1045, 'update', 'route_prices', 'PriceId', 22, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1046, 'update', 'route_prices', 'PriceId', 27, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1047, 'update', 'route_prices', 'PriceId', 32, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1048, 'update', 'route_prices', 'PriceId', 36, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1049, 'update', 'route_prices', 'PriceId', 39, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1050, 'update', 'route_prices', 'PriceId', 41, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1051, 'update', 'route_prices', 'PriceId', 42, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1052, 'insert', 'route_prices', 'PriceId', 113, '2013-05-07 12:18:17', 1, 'insert to route_prices table'),
(1053, 'insert', 'route_prices', 'PriceId', 114, '2013-05-07 12:18:17', 1, 'insert to route_prices table'),
(1054, 'update', 'route_prices', 'PriceId', 99, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1055, 'update', 'route_prices', 'PriceId', 101, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1056, 'update', 'route_prices', 'PriceId', 103, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1057, 'update', 'route_prices', 'PriceId', 105, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1058, 'update', 'route_prices', 'PriceId', 107, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1059, 'update', 'route_prices', 'PriceId', 109, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1060, 'update', 'route_prices', 'PriceId', 111, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1061, 'update', 'route_prices', 'PriceId', 113, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1062, 'insert', 'route_prices', 'PriceId', 115, '2013-05-07 12:18:17', 1, 'insert to route_prices table'),
(1063, 'update', 'route_prices', 'PriceId', 100, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1064, 'update', 'route_prices', 'PriceId', 102, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1065, 'update', 'route_prices', 'PriceId', 104, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1066, 'update', 'route_prices', 'PriceId', 106, '2013-05-07 12:18:17', 1, 'update row described in route_prices table'),
(1067, 'update', 'route_prices', 'PriceId', 108, '2013-05-07 12:18:18', 1, 'update row described in route_prices table'),
(1068, 'update', 'route_prices', 'PriceId', 110, '2013-05-07 12:18:18', 1, 'update row described in route_prices table'),
(1069, 'update', 'route_prices', 'PriceId', 112, '2013-05-07 12:18:18', 1, 'update row described in route_prices table'),
(1070, 'update', 'route_prices', 'PriceId', 114, '2013-05-07 12:18:18', 1, 'update row described in route_prices table'),
(1071, 'update', 'route_prices', 'PriceId', 115, '2013-05-07 12:18:18', 1, 'update row described in route_prices table'),
(1072, 'update', 'route_prices', 'PriceId', 71, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1073, 'update', 'route_prices', 'PriceId', 72, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1074, 'update', 'route_prices', 'PriceId', 73, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1075, 'update', 'route_prices', 'PriceId', 74, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1076, 'update', 'route_prices', 'PriceId', 75, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1077, 'update', 'route_prices', 'PriceId', 76, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1078, 'update', 'route_prices', 'PriceId', 77, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1079, 'insert', 'route_prices', 'PriceId', 116, '2013-05-07 12:19:03', 1, 'insert to route_prices table'),
(1080, 'insert', 'route_prices', 'PriceId', 117, '2013-05-07 12:19:03', 1, 'insert to route_prices table'),
(1081, 'update', 'route_prices', 'PriceId', 71, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1082, 'update', 'route_prices', 'PriceId', 78, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1083, 'update', 'route_prices', 'PriceId', 79, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1084, 'update', 'route_prices', 'PriceId', 80, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1085, 'update', 'route_prices', 'PriceId', 81, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1086, 'update', 'route_prices', 'PriceId', 82, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1087, 'update', 'route_prices', 'PriceId', 83, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1088, 'insert', 'route_prices', 'PriceId', 118, '2013-05-07 12:19:03', 1, 'insert to route_prices table'),
(1089, 'insert', 'route_prices', 'PriceId', 119, '2013-05-07 12:19:03', 1, 'insert to route_prices table'),
(1090, 'update', 'route_prices', 'PriceId', 72, '2013-05-07 12:19:03', 1, 'update row described in route_prices table'),
(1091, 'update', 'route_prices', 'PriceId', 78, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1092, 'update', 'route_prices', 'PriceId', 84, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1093, 'update', 'route_prices', 'PriceId', 85, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1094, 'update', 'route_prices', 'PriceId', 86, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1095, 'update', 'route_prices', 'PriceId', 87, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1096, 'update', 'route_prices', 'PriceId', 88, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1097, 'insert', 'route_prices', 'PriceId', 120, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1098, 'insert', 'route_prices', 'PriceId', 121, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1099, 'update', 'route_prices', 'PriceId', 73, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1100, 'update', 'route_prices', 'PriceId', 79, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1101, 'update', 'route_prices', 'PriceId', 84, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1102, 'update', 'route_prices', 'PriceId', 89, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1103, 'update', 'route_prices', 'PriceId', 90, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1104, 'update', 'route_prices', 'PriceId', 91, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1105, 'update', 'route_prices', 'PriceId', 92, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1106, 'insert', 'route_prices', 'PriceId', 122, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1107, 'insert', 'route_prices', 'PriceId', 123, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1108, 'update', 'route_prices', 'PriceId', 74, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1109, 'update', 'route_prices', 'PriceId', 80, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1110, 'update', 'route_prices', 'PriceId', 85, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1111, 'update', 'route_prices', 'PriceId', 89, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1112, 'update', 'route_prices', 'PriceId', 93, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1113, 'update', 'route_prices', 'PriceId', 94, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1114, 'update', 'route_prices', 'PriceId', 95, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1115, 'insert', 'route_prices', 'PriceId', 124, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1116, 'insert', 'route_prices', 'PriceId', 125, '2013-05-07 12:19:04', 1, 'insert to route_prices table'),
(1117, 'update', 'route_prices', 'PriceId', 75, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1118, 'update', 'route_prices', 'PriceId', 81, '2013-05-07 12:19:04', 1, 'update row described in route_prices table'),
(1119, 'update', 'route_prices', 'PriceId', 86, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1120, 'update', 'route_prices', 'PriceId', 90, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1121, 'update', 'route_prices', 'PriceId', 93, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1122, 'update', 'route_prices', 'PriceId', 96, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1123, 'update', 'route_prices', 'PriceId', 97, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1124, 'insert', 'route_prices', 'PriceId', 126, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1125, 'insert', 'route_prices', 'PriceId', 127, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1126, 'update', 'route_prices', 'PriceId', 76, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1127, 'update', 'route_prices', 'PriceId', 82, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1128, 'update', 'route_prices', 'PriceId', 87, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1129, 'update', 'route_prices', 'PriceId', 91, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1130, 'update', 'route_prices', 'PriceId', 94, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1131, 'update', 'route_prices', 'PriceId', 96, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1132, 'update', 'route_prices', 'PriceId', 98, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1133, 'insert', 'route_prices', 'PriceId', 128, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1134, 'insert', 'route_prices', 'PriceId', 129, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1135, 'update', 'route_prices', 'PriceId', 77, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1136, 'update', 'route_prices', 'PriceId', 83, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1137, 'update', 'route_prices', 'PriceId', 88, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1138, 'update', 'route_prices', 'PriceId', 92, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1139, 'update', 'route_prices', 'PriceId', 95, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1140, 'update', 'route_prices', 'PriceId', 97, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1141, 'update', 'route_prices', 'PriceId', 98, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1142, 'insert', 'route_prices', 'PriceId', 130, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1143, 'insert', 'route_prices', 'PriceId', 131, '2013-05-07 12:19:05', 1, 'insert to route_prices table'),
(1144, 'update', 'route_prices', 'PriceId', 116, '2013-05-07 12:19:05', 1, 'update row described in route_prices table'),
(1145, 'update', 'route_prices', 'PriceId', 118, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1146, 'update', 'route_prices', 'PriceId', 120, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1147, 'update', 'route_prices', 'PriceId', 122, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1148, 'update', 'route_prices', 'PriceId', 124, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1149, 'update', 'route_prices', 'PriceId', 126, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1150, 'update', 'route_prices', 'PriceId', 128, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1151, 'update', 'route_prices', 'PriceId', 130, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1152, 'insert', 'route_prices', 'PriceId', 132, '2013-05-07 12:19:06', 1, 'insert to route_prices table'),
(1153, 'update', 'route_prices', 'PriceId', 117, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1154, 'update', 'route_prices', 'PriceId', 119, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1155, 'update', 'route_prices', 'PriceId', 121, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1156, 'update', 'route_prices', 'PriceId', 123, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1157, 'update', 'route_prices', 'PriceId', 125, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1158, 'update', 'route_prices', 'PriceId', 127, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1159, 'update', 'route_prices', 'PriceId', 129, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1160, 'update', 'route_prices', 'PriceId', 131, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1161, 'update', 'route_prices', 'PriceId', 132, '2013-05-07 12:19:06', 1, 'update row described in route_prices table'),
(1162, 'update', 'route_prices', 'PriceId', 43, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1163, 'update', 'route_prices', 'PriceId', 44, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1164, 'update', 'route_prices', 'PriceId', 45, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1165, 'update', 'route_prices', 'PriceId', 46, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1166, 'update', 'route_prices', 'PriceId', 47, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1167, 'update', 'route_prices', 'PriceId', 48, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1168, 'update', 'route_prices', 'PriceId', 49, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1169, 'insert', 'route_prices', 'PriceId', 133, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1170, 'insert', 'route_prices', 'PriceId', 134, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1171, 'update', 'route_prices', 'PriceId', 43, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1172, 'update', 'route_prices', 'PriceId', 50, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1173, 'update', 'route_prices', 'PriceId', 51, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1174, 'update', 'route_prices', 'PriceId', 52, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1175, 'update', 'route_prices', 'PriceId', 53, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1176, 'update', 'route_prices', 'PriceId', 54, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1177, 'update', 'route_prices', 'PriceId', 55, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1178, 'insert', 'route_prices', 'PriceId', 135, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1179, 'insert', 'route_prices', 'PriceId', 136, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1180, 'update', 'route_prices', 'PriceId', 44, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1181, 'update', 'route_prices', 'PriceId', 50, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1182, 'update', 'route_prices', 'PriceId', 56, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1183, 'update', 'route_prices', 'PriceId', 57, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1184, 'update', 'route_prices', 'PriceId', 58, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1185, 'update', 'route_prices', 'PriceId', 59, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1186, 'update', 'route_prices', 'PriceId', 60, '2013-05-07 12:19:41', 1, 'update row described in route_prices table'),
(1187, 'insert', 'route_prices', 'PriceId', 137, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1188, 'insert', 'route_prices', 'PriceId', 138, '2013-05-07 12:19:41', 1, 'insert to route_prices table'),
(1189, 'update', 'route_prices', 'PriceId', 45, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1190, 'update', 'route_prices', 'PriceId', 51, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1191, 'update', 'route_prices', 'PriceId', 56, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1192, 'update', 'route_prices', 'PriceId', 61, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1193, 'update', 'route_prices', 'PriceId', 62, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1194, 'update', 'route_prices', 'PriceId', 63, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1195, 'update', 'route_prices', 'PriceId', 64, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1196, 'insert', 'route_prices', 'PriceId', 139, '2013-05-07 12:19:42', 1, 'insert to route_prices table'),
(1197, 'insert', 'route_prices', 'PriceId', 140, '2013-05-07 12:19:42', 1, 'insert to route_prices table'),
(1198, 'update', 'route_prices', 'PriceId', 46, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1199, 'update', 'route_prices', 'PriceId', 52, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1200, 'update', 'route_prices', 'PriceId', 57, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1201, 'update', 'route_prices', 'PriceId', 61, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1202, 'update', 'route_prices', 'PriceId', 65, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1203, 'update', 'route_prices', 'PriceId', 66, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1204, 'update', 'route_prices', 'PriceId', 67, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1205, 'insert', 'route_prices', 'PriceId', 141, '2013-05-07 12:19:42', 1, 'insert to route_prices table'),
(1206, 'insert', 'route_prices', 'PriceId', 142, '2013-05-07 12:19:42', 1, 'insert to route_prices table'),
(1207, 'update', 'route_prices', 'PriceId', 47, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1208, 'update', 'route_prices', 'PriceId', 53, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1209, 'update', 'route_prices', 'PriceId', 58, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1210, 'update', 'route_prices', 'PriceId', 62, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1211, 'update', 'route_prices', 'PriceId', 65, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1212, 'update', 'route_prices', 'PriceId', 68, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1213, 'update', 'route_prices', 'PriceId', 69, '2013-05-07 12:19:42', 1, 'update row described in route_prices table'),
(1214, 'insert', 'route_prices', 'PriceId', 143, '2013-05-07 12:19:42', 1, 'insert to route_prices table'),
(1215, 'insert', 'route_prices', 'PriceId', 144, '2013-05-07 12:19:43', 1, 'insert to route_prices table'),
(1216, 'update', 'route_prices', 'PriceId', 48, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1217, 'update', 'route_prices', 'PriceId', 54, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1218, 'update', 'route_prices', 'PriceId', 59, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1219, 'update', 'route_prices', 'PriceId', 63, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1220, 'update', 'route_prices', 'PriceId', 66, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1221, 'update', 'route_prices', 'PriceId', 68, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1222, 'update', 'route_prices', 'PriceId', 70, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1223, 'insert', 'route_prices', 'PriceId', 145, '2013-05-07 12:19:43', 1, 'insert to route_prices table'),
(1224, 'insert', 'route_prices', 'PriceId', 146, '2013-05-07 12:19:43', 1, 'insert to route_prices table'),
(1225, 'update', 'route_prices', 'PriceId', 49, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1226, 'update', 'route_prices', 'PriceId', 55, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1227, 'update', 'route_prices', 'PriceId', 60, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1228, 'update', 'route_prices', 'PriceId', 64, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1229, 'update', 'route_prices', 'PriceId', 67, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1230, 'update', 'route_prices', 'PriceId', 69, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1231, 'update', 'route_prices', 'PriceId', 70, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1232, 'insert', 'route_prices', 'PriceId', 147, '2013-05-07 12:19:43', 1, 'insert to route_prices table'),
(1233, 'insert', 'route_prices', 'PriceId', 148, '2013-05-07 12:19:43', 1, 'insert to route_prices table'),
(1234, 'update', 'route_prices', 'PriceId', 133, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1235, 'update', 'route_prices', 'PriceId', 135, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1236, 'update', 'route_prices', 'PriceId', 137, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1237, 'update', 'route_prices', 'PriceId', 139, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1238, 'update', 'route_prices', 'PriceId', 141, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1239, 'update', 'route_prices', 'PriceId', 143, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1240, 'update', 'route_prices', 'PriceId', 145, '2013-05-07 12:19:43', 1, 'update row described in route_prices table'),
(1241, 'update', 'route_prices', 'PriceId', 147, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1242, 'insert', 'route_prices', 'PriceId', 149, '2013-05-07 12:19:44', 1, 'insert to route_prices table'),
(1243, 'update', 'route_prices', 'PriceId', 134, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1244, 'update', 'route_prices', 'PriceId', 136, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1245, 'update', 'route_prices', 'PriceId', 138, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1246, 'update', 'route_prices', 'PriceId', 140, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1247, 'update', 'route_prices', 'PriceId', 142, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1248, 'update', 'route_prices', 'PriceId', 144, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1249, 'update', 'route_prices', 'PriceId', 146, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1250, 'update', 'route_prices', 'PriceId', 148, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1251, 'update', 'route_prices', 'PriceId', 149, '2013-05-07 12:19:44', 1, 'update row described in route_prices table'),
(1252, 'update', 'route_prices', 'PriceId', 13, '2013-05-07 12:36:15', 1, 'update row described in route_prices table'),
(1253, 'insert', 'route_prices', 'PriceId', 150, '2013-05-07 12:36:15', 1, 'insert to route_prices table'),
(1254, 'update', 'route_prices', 'PriceId', 13, '2013-05-07 12:36:15', 1, 'update row described in route_prices table'),
(1255, 'insert', 'route_prices', 'PriceId', 151, '2013-05-07 12:36:15', 1, 'insert to route_prices table'),
(1256, 'update', 'route_prices', 'PriceId', 150, '2013-05-07 12:36:15', 1, 'update row described in route_prices table'),
(1257, 'update', 'route_prices', 'PriceId', 151, '2013-05-07 12:36:15', 1, 'update row described in route_prices table'),
(1258, 'insert', 'route_prices', 'PriceId', 152, '2013-05-07 12:50:26', 1, 'insert to route_prices table'),
(1259, 'insert', 'route_prices', 'PriceId', 153, '2013-05-07 12:50:26', 1, 'insert to route_prices table'),
(1260, 'update', 'route_prices', 'PriceId', 152, '2013-05-07 12:50:27', 1, 'update row described in route_prices table'),
(1261, 'insert', 'route_prices', 'PriceId', 154, '2013-05-07 12:50:27', 1, 'insert to route_prices table'),
(1262, 'update', 'route_prices', 'PriceId', 153, '2013-05-07 12:50:27', 1, 'update row described in route_prices table'),
(1263, 'update', 'route_prices', 'PriceId', 154, '2013-05-07 12:50:27', 1, 'update row described in route_prices table'),
(1264, 'insert', 'route_prices', 'PriceId', 155, '2013-05-07 12:53:44', 1, 'insert to route_prices table'),
(1265, 'insert', 'route_prices', 'PriceId', 156, '2013-05-07 12:53:45', 1, 'insert to route_prices table'),
(1266, 'update', 'route_prices', 'PriceId', 155, '2013-05-07 12:53:45', 1, 'update row described in route_prices table'),
(1267, 'insert', 'route_prices', 'PriceId', 157, '2013-05-07 12:53:45', 1, 'insert to route_prices table'),
(1268, 'update', 'route_prices', 'PriceId', 156, '2013-05-07 12:53:45', 1, 'update row described in route_prices table'),
(1269, 'update', 'route_prices', 'PriceId', 157, '2013-05-07 12:53:45', 1, 'update row described in route_prices table'),
(1270, 'insert', 'buses', 'BusId', 6, '2013-05-07 13:42:31', 1, 'insert to buses table'),
(1271, 'insert', 'buses', 'BusId', 7, '2013-05-07 13:44:36', 1, 'insert to buses table'),
(1272, 'insert', 'buses', 'BusId', 8, '2013-05-07 13:45:43', 1, 'insert to buses table'),
(1273, 'insert', 'buses', 'BusId', 9, '2013-05-07 14:03:57', 1, 'insert to buses table'),
(1274, 'insert', 'buses', 'BusId', 10, '2013-05-07 14:13:56', 1, 'insert to buses table'),
(1275, 'insert', 'buses', 'BusId', 6, '2013-05-07 14:17:45', 1, 'insert to buses table'),
(1276, 'insert', 'buses', 'BusId', 7, '2013-05-07 14:18:49', 1, 'insert to buses table'),
(1277, 'insert', 'buses', 'BusId', 8, '2013-05-07 14:19:48', 1, 'insert to buses table'),
(1278, 'insert', 'buses', 'BusId', 9, '2013-05-07 14:21:35', 1, 'insert to buses table'),
(1279, 'insert', 'buses', 'BusId', 10, '2013-05-07 14:22:17', 1, 'insert to buses table'),
(1280, 'insert', 'ticketing', 'TicketId', 1139999, '2013-05-07 23:45:12', 1, 'insert to ticketing table'),
(1281, 'insert', 'customer_loyalty_points', 'LoyaltyId', 40, '2013-05-07 23:45:12', 1, 'insert to customer_loyalty_points table'),
(1282, 'update', 'route_prices', 'PriceId', 9, '2013-05-09 16:21:54', 1, 'update row described in route_prices table'),
(1283, 'insert', 'route_prices', 'PriceId', 158, '2013-05-09 16:21:54', 1, 'insert to route_prices table'),
(1284, 'update', 'route_prices', 'PriceId', 6, '2013-05-09 16:21:54', 1, 'update row described in route_prices table'),
(1285, 'insert', 'route_prices', 'PriceId', 159, '2013-05-09 16:21:55', 1, 'insert to route_prices table'),
(1286, 'update', 'route_prices', 'PriceId', 158, '2013-05-09 16:21:55', 1, 'update row described in route_prices table'),
(1287, 'update', 'route_prices', 'PriceId', 159, '2013-05-09 16:21:55', 1, 'update row described in route_prices table'),
(1288, 'update', 'route_prices', 'PriceId', 14, '2013-05-09 16:23:15', 1, 'update row described in route_prices table'),
(1289, 'insert', 'route_prices', 'PriceId', 160, '2013-05-09 16:23:15', 1, 'insert to route_prices table'),
(1290, 'update', 'route_prices', 'PriceId', 14, '2013-05-09 16:23:15', 1, 'update row described in route_prices table'),
(1291, 'insert', 'route_prices', 'PriceId', 161, '2013-05-09 16:23:15', 1, 'insert to route_prices table'),
(1292, 'update', 'route_prices', 'PriceId', 160, '2013-05-09 16:23:15', 1, 'update row described in route_prices table'),
(1293, 'update', 'route_prices', 'PriceId', 161, '2013-05-09 16:23:15', 1, 'update row described in route_prices table'),
(1294, 'insert', 'bus_schedule', 'ScheduleId', 29, '2013-05-09 17:05:19', 1, 'insert to bus_schedule table'),
(1295, 'update', 'bus_schedule', 'ScheduleId', 29, '2013-05-09 17:06:03', 1, 'update row described in bus_schedule table'),
(1296, 'update', 'route_prices', 'PriceId', 71, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1297, 'update', 'route_prices', 'PriceId', 72, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1298, 'update', 'route_prices', 'PriceId', 73, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1299, 'update', 'route_prices', 'PriceId', 74, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1300, 'update', 'route_prices', 'PriceId', 75, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1301, 'update', 'route_prices', 'PriceId', 76, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1302, 'update', 'route_prices', 'PriceId', 77, '2013-05-09 17:10:53', 1, 'update row described in route_prices table');
INSERT INTO `transaction_log` (`transaction_log_id`, `transaction_log_name`, `table_name`, `pkName`, `pkValue_id`, `timestamp`, `user_id`, `description`) VALUES
(1303, 'update', 'route_prices', 'PriceId', 116, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1304, 'update', 'route_prices', 'PriceId', 117, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1305, 'update', 'route_prices', 'PriceId', 71, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1306, 'update', 'route_prices', 'PriceId', 78, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1307, 'update', 'route_prices', 'PriceId', 79, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1308, 'update', 'route_prices', 'PriceId', 80, '2013-05-09 17:10:53', 1, 'update row described in route_prices table'),
(1309, 'update', 'route_prices', 'PriceId', 81, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1310, 'update', 'route_prices', 'PriceId', 82, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1311, 'update', 'route_prices', 'PriceId', 83, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1312, 'update', 'route_prices', 'PriceId', 118, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1313, 'update', 'route_prices', 'PriceId', 119, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1314, 'update', 'route_prices', 'PriceId', 72, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1315, 'update', 'route_prices', 'PriceId', 78, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1316, 'update', 'route_prices', 'PriceId', 84, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1317, 'update', 'route_prices', 'PriceId', 85, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1318, 'update', 'route_prices', 'PriceId', 86, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1319, 'update', 'route_prices', 'PriceId', 87, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1320, 'update', 'route_prices', 'PriceId', 88, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1321, 'update', 'route_prices', 'PriceId', 120, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1322, 'update', 'route_prices', 'PriceId', 121, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1323, 'update', 'route_prices', 'PriceId', 73, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1324, 'update', 'route_prices', 'PriceId', 79, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1325, 'update', 'route_prices', 'PriceId', 84, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1326, 'update', 'route_prices', 'PriceId', 89, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1327, 'update', 'route_prices', 'PriceId', 90, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1328, 'update', 'route_prices', 'PriceId', 91, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1329, 'update', 'route_prices', 'PriceId', 92, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1330, 'update', 'route_prices', 'PriceId', 122, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1331, 'update', 'route_prices', 'PriceId', 123, '2013-05-09 17:10:54', 1, 'update row described in route_prices table'),
(1332, 'update', 'route_prices', 'PriceId', 74, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1333, 'update', 'route_prices', 'PriceId', 80, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1334, 'update', 'route_prices', 'PriceId', 85, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1335, 'update', 'route_prices', 'PriceId', 89, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1336, 'update', 'route_prices', 'PriceId', 93, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1337, 'update', 'route_prices', 'PriceId', 94, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1338, 'update', 'route_prices', 'PriceId', 95, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1339, 'update', 'route_prices', 'PriceId', 124, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1340, 'update', 'route_prices', 'PriceId', 125, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1341, 'update', 'route_prices', 'PriceId', 75, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1342, 'update', 'route_prices', 'PriceId', 81, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1343, 'update', 'route_prices', 'PriceId', 86, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1344, 'update', 'route_prices', 'PriceId', 90, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1345, 'update', 'route_prices', 'PriceId', 93, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1346, 'update', 'route_prices', 'PriceId', 96, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1347, 'update', 'route_prices', 'PriceId', 97, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1348, 'update', 'route_prices', 'PriceId', 126, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1349, 'update', 'route_prices', 'PriceId', 127, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1350, 'update', 'route_prices', 'PriceId', 76, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1351, 'update', 'route_prices', 'PriceId', 82, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1352, 'update', 'route_prices', 'PriceId', 87, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1353, 'update', 'route_prices', 'PriceId', 91, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1354, 'update', 'route_prices', 'PriceId', 94, '2013-05-09 17:10:55', 1, 'update row described in route_prices table'),
(1355, 'update', 'route_prices', 'PriceId', 96, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1356, 'update', 'route_prices', 'PriceId', 98, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1357, 'update', 'route_prices', 'PriceId', 128, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1358, 'update', 'route_prices', 'PriceId', 129, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1359, 'update', 'route_prices', 'PriceId', 77, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1360, 'update', 'route_prices', 'PriceId', 83, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1361, 'update', 'route_prices', 'PriceId', 88, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1362, 'update', 'route_prices', 'PriceId', 92, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1363, 'update', 'route_prices', 'PriceId', 95, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1364, 'update', 'route_prices', 'PriceId', 97, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1365, 'update', 'route_prices', 'PriceId', 98, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1366, 'update', 'route_prices', 'PriceId', 130, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1367, 'update', 'route_prices', 'PriceId', 131, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1368, 'update', 'route_prices', 'PriceId', 116, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1369, 'update', 'route_prices', 'PriceId', 118, '2013-05-09 17:10:56', 1, 'update row described in route_prices table'),
(1370, 'update', 'route_prices', 'PriceId', 120, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1371, 'update', 'route_prices', 'PriceId', 122, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1372, 'update', 'route_prices', 'PriceId', 124, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1373, 'update', 'route_prices', 'PriceId', 126, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1374, 'update', 'route_prices', 'PriceId', 128, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1375, 'update', 'route_prices', 'PriceId', 130, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1376, 'update', 'route_prices', 'PriceId', 132, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1377, 'update', 'route_prices', 'PriceId', 117, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1378, 'update', 'route_prices', 'PriceId', 119, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1379, 'update', 'route_prices', 'PriceId', 121, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1380, 'update', 'route_prices', 'PriceId', 123, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1381, 'update', 'route_prices', 'PriceId', 125, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1382, 'update', 'route_prices', 'PriceId', 127, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1383, 'update', 'route_prices', 'PriceId', 129, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1384, 'update', 'route_prices', 'PriceId', 131, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1385, 'update', 'route_prices', 'PriceId', 132, '2013-05-09 17:10:57', 1, 'update row described in route_prices table'),
(1386, 'insert', 'ticketing', 'TicketId', 1140000, '2013-05-11 14:40:38', 1, 'insert to ticketing table'),
(1387, 'insert', 'ticketing', 'TicketId', 1140001, '2013-05-11 14:40:39', 1, 'insert to ticketing table'),
(1388, 'insert', 'ticketing', 'TicketId', 1140002, '2013-05-11 14:42:34', 1, 'insert to ticketing table'),
(1389, 'insert', 'customer_loyalty_points', 'LoyaltyId', 41, '2013-05-11 14:42:35', 1, 'insert to customer_loyalty_points table'),
(1390, 'insert', 'ticketing', 'TicketId', 1140003, '2013-05-11 14:43:14', 1, 'insert to ticketing table'),
(1391, 'insert', 'customer_loyalty_points', 'LoyaltyId', 42, '2013-05-11 14:43:14', 1, 'insert to customer_loyalty_points table'),
(1392, 'insert', 'ticketing', 'TicketId', 1140004, '2013-05-11 14:43:45', 1, 'insert to ticketing table'),
(1393, 'insert', 'customer_loyalty_points', 'LoyaltyId', 43, '2013-05-11 14:43:45', 1, 'insert to customer_loyalty_points table'),
(1394, 'insert', 'ticketing', 'TicketId', 1140005, '2013-05-11 14:43:54', 1, 'insert to ticketing table'),
(1395, 'insert', 'ticketing', 'TicketId', 1140006, '2013-05-11 14:44:32', 1, 'insert to ticketing table'),
(1396, 'insert', 'ticketing', 'TicketId', 1140007, '2013-05-11 14:44:55', 1, 'insert to ticketing table'),
(1397, 'insert', 'ticketing', 'TicketId', 1140008, '2013-05-11 14:45:03', 1, 'insert to ticketing table'),
(1398, 'insert', 'ticketing', 'TicketId', 1140009, '2013-05-11 14:45:18', 1, 'insert to ticketing table'),
(1399, 'insert', 'ticketing', 'TicketId', 1140010, '2013-05-11 14:46:02', 1, 'insert to ticketing table'),
(1400, 'insert', 'ticketing', 'TicketId', 1140011, '2013-05-11 14:46:47', 1, 'insert to ticketing table'),
(1401, 'insert', 'ticketing', 'TicketId', 1140012, '2013-05-11 14:47:05', 1, 'insert to ticketing table'),
(1402, 'insert', 'ticketing', 'TicketId', 1140013, '2013-05-11 14:47:10', 1, 'insert to ticketing table'),
(1403, 'insert', 'ticketing', 'TicketId', 1140014, '2013-05-11 14:48:52', 1, 'insert to ticketing table'),
(1404, 'insert', 'ticketing', 'TicketId', 1140015, '2013-05-11 14:50:47', 1, 'insert to ticketing table'),
(1405, 'insert', 'customer_loyalty_points', 'LoyaltyId', 44, '2013-05-11 14:50:47', 1, 'insert to customer_loyalty_points table'),
(1406, 'insert', 'ticketing', 'TicketId', 1140016, '2013-05-11 15:07:54', 1, 'insert to ticketing table'),
(1407, 'insert', 'customer_loyalty_points', 'LoyaltyId', 45, '2013-05-11 15:07:54', 1, 'insert to customer_loyalty_points table'),
(1408, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:10:03', 1, 'update row described in discounts table'),
(1409, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:14:16', 1, 'update row described in discounts table'),
(1410, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:15:00', 1, 'update row described in discounts table'),
(1411, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:15:13', 1, 'update row described in discounts table'),
(1412, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:17:03', 1, 'update row described in discounts table'),
(1413, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:18:08', 1, 'update row described in discounts table'),
(1414, 'update', 'discounts', 'Disc_Id', 5, '2013-05-11 16:18:17', 1, 'update row described in discounts table'),
(1415, 'update', 'discounts', 'Disc_Id', 4, '2013-05-11 16:18:40', 1, 'update row described in discounts table'),
(1416, 'insert', 'bus_schedule', 'ScheduleId', 30, '2013-05-11 16:36:37', 1, 'insert to bus_schedule table'),
(1417, 'insert', 'bus_schedule', 'ScheduleId', 31, '2013-05-11 16:36:37', 1, 'insert to bus_schedule table');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE IF NOT EXISTS `user_accounts` (
  `uacc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uacc_group_fk` smallint(5) unsigned NOT NULL,
  `uacc_email` varchar(100) NOT NULL,
  `uacc_username` varchar(15) NOT NULL,
  `uacc_password` varchar(60) NOT NULL,
  `uacc_ip_address` varchar(40) NOT NULL,
  `uacc_salt` varchar(40) NOT NULL,
  `uacc_activation_token` varchar(40) NOT NULL,
  `uacc_forgotten_password_token` varchar(40) NOT NULL,
  `uacc_forgotten_password_expire` datetime NOT NULL,
  `uacc_update_email_token` varchar(40) NOT NULL,
  `uacc_update_email` varchar(100) NOT NULL,
  `uacc_active` tinyint(1) unsigned NOT NULL,
  `uacc_suspend` tinyint(1) unsigned NOT NULL,
  `uacc_fail_login_attempts` smallint(5) NOT NULL,
  `uacc_fail_login_ip_address` varchar(40) NOT NULL,
  `uacc_date_fail_login_ban` datetime NOT NULL COMMENT 'Time user is banned until due to repeated failed logins',
  `uacc_date_last_login` datetime NOT NULL,
  `uacc_date_added` datetime NOT NULL,
  `t_station` int(11) NOT NULL,
  PRIMARY KEY (`uacc_id`),
  UNIQUE KEY `uacc_id` (`uacc_id`),
  KEY `uacc_group_fk` (`uacc_group_fk`),
  KEY `uacc_email` (`uacc_email`),
  KEY `uacc_username` (`uacc_username`),
  KEY `uacc_fail_login_ip_address` (`uacc_fail_login_ip_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`uacc_id`, `uacc_group_fk`, `uacc_email`, `uacc_username`, `uacc_password`, `uacc_ip_address`, `uacc_salt`, `uacc_activation_token`, `uacc_forgotten_password_token`, `uacc_forgotten_password_expire`, `uacc_update_email_token`, `uacc_update_email`, `uacc_active`, `uacc_suspend`, `uacc_fail_login_attempts`, `uacc_fail_login_ip_address`, `uacc_date_fail_login_ban`, `uacc_date_last_login`, `uacc_date_added`, `t_station`) VALUES
(1, 3, 'admin@admin.com', 'admin', '$2a$08$ort0GZyvLY/PcKcuviwSf.cfPbcrVn6A/UQVERnQOABd9wZg.V6P6', '::1', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '3c480eb6c1ad2b205d58412f81e92f47bbdb2673', 'wesleygk@gmail.com', 1, 0, 0, '', '0000-00-00 00:00:00', '2013-05-11 16:54:39', '2011-01-01 00:00:00', 1),
(2, 2, 'moderator@moderator.com', 'moderator', '$2a$08$q.0ZhovC5ZkVpkBLJ.Mz.O4VjWsKohYckJNx4KM40MXdP/zEZpwcm', '192.168.1.12', 'ZC38NNBPjF', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2013-01-30 15:56:26', '2011-08-04 16:49:07', 1),
(3, 1, 'public@public.com', 'public', '$2a$08$GlxQ00VKlev2t.CpvbTOlepTJljxF2RocJghON37r40mbDl4vJLv2', '::1', 'CDNFV6dHmn', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2013-04-22 16:19:30', '2011-09-15 12:24:45', 1),
(4, 1, 'a.marcel77@gmail.com', 'marcel', '$2a$08$Zzr8EUbfMlFfH7IunN0nEuscEhc34Qyp70vhs5LueHghBynr011pO', '::1', 'qDZ6grRQ8x', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2013-05-11 01:28:24', '2013-04-22 16:08:02', 5),
(6, 1, 'marcel@mtl.co.ke', 'meshack', '$2a$08$SwN53npM0bOOZQch4fd0E.xUyxG/2xTIuTnDCud5ZlHuuYC0asw9a', '::1', 'TDBbT8ChY5', '1912ec128f4a6163bbaab2cea6779cd2cbbaaccd', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2013-04-22 16:46:10', '2013-04-22 16:46:10', 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `ugrp_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `ugrp_name` varchar(20) NOT NULL,
  `ugrp_desc` varchar(100) NOT NULL,
  `ugrp_admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`ugrp_id`),
  UNIQUE KEY `ugrp_id` (`ugrp_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`ugrp_id`, `ugrp_name`, `ugrp_desc`, `ugrp_admin`) VALUES
(1, 'Clerk', 'Clerk User : has no admin access rights.', 0),
(2, 'Branch Manager', 'Branch Manager : has partial admin access rights.', 1),
(3, 'Master Admin', 'Master Admin : has full admin access rights.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login_sessions`
--

CREATE TABLE IF NOT EXISTS `user_login_sessions` (
  `usess_uacc_fk` int(11) NOT NULL,
  `usess_series` varchar(40) NOT NULL,
  `usess_token` varchar(40) NOT NULL,
  `usess_login_date` datetime NOT NULL,
  PRIMARY KEY (`usess_token`),
  UNIQUE KEY `usess_token` (`usess_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login_sessions`
--

INSERT INTO `user_login_sessions` (`usess_uacc_fk`, `usess_series`, `usess_token`, `usess_login_date`) VALUES
(1, '', '2f4c55dec83cc52591a9a8e260efda2aaf6be0dc', '2013-05-11 19:36:41'),
(4, '', 'ea9f3ae60c8b72ea989c7949cb85549c012fc4f8', '2013-05-11 01:29:44'),
(1, '', 'f13df7483163428b5a97474eb87a9c7c0daf15bf', '2013-05-11 14:11:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_privileges`
--

CREATE TABLE IF NOT EXISTS `user_privileges` (
  `upriv_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `upriv_name` varchar(20) NOT NULL,
  `upriv_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`upriv_id`),
  UNIQUE KEY `upriv_id` (`upriv_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user_privileges`
--

INSERT INTO `user_privileges` (`upriv_id`, `upriv_name`, `upriv_desc`) VALUES
(1, 'View Users', 'User can view user account details.'),
(2, 'View User Groups', 'User can view user groups.'),
(3, 'View Privileges', 'User can view privileges.'),
(4, 'Insert User Groups', 'User can insert new user groups.'),
(5, 'Insert Privileges', 'User can insert privileges.'),
(6, 'Update Users', 'User can update user account details.'),
(7, 'Update User Groups', 'User can update user groups.'),
(8, 'Update Privileges', 'User can update user privileges.'),
(9, 'Delete Users', 'User can delete user accounts.'),
(10, 'Delete User Groups', 'User can delete user groups.'),
(11, 'Delete Privileges', 'User can delete user privileges.'),
(12, 'petty cash', '<p>view petty cash tab</p>'),
(13, 'bus hire', '<p>view bus hire tab</p>'),
(14, 'messages', '<p>view messages</p>');

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege_users`
--

CREATE TABLE IF NOT EXISTS `user_privilege_users` (
  `upriv_users_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `upriv_users_uacc_fk` int(11) NOT NULL,
  `upriv_users_upriv_fk` smallint(5) NOT NULL,
  PRIMARY KEY (`upriv_users_id`),
  UNIQUE KEY `upriv_users_id` (`upriv_users_id`) USING BTREE,
  KEY `upriv_users_uacc_fk` (`upriv_users_uacc_fk`),
  KEY `upriv_users_upriv_fk` (`upriv_users_upriv_fk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `user_privilege_users`
--

INSERT INTO `user_privilege_users` (`upriv_users_id`, `upriv_users_uacc_fk`, `upriv_users_upriv_fk`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 2, 1),
(13, 2, 2),
(14, 2, 3),
(15, 2, 6),
(16, 4, 1),
(17, 4, 2),
(18, 4, 3),
(19, 4, 4),
(20, 4, 5),
(21, 4, 6),
(22, 4, 7),
(23, 4, 8),
(24, 4, 9),
(25, 4, 10),
(26, 4, 11),
(27, 4, 12),
(28, 4, 13),
(29, 4, 14);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE IF NOT EXISTS `vouchers` (
  `vouchers_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vname` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `vamount` double(10,2) NOT NULL,
  `vdatefrom` date NOT NULL,
  `vdateto` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vouchers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`vouchers_id`, `vname`, `description`, `quantity`, `vamount`, `vdatefrom`, `vdateto`, `status`) VALUES
(1, 'Easter', 'Easter voucher', 48, 1000.00, '2013-04-01', '2013-04-30', 1),
(2, 'Madaraka', 'Madaraka treat', 47, 500.00, '2013-06-01', '2013-06-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `voucher_items`
--

CREATE TABLE IF NOT EXISTS `voucher_items` (
  `voucher_items_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CustomerId` bigint(20) NOT NULL,
  `vouchers_id` bigint(20) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `dateawarded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `redeemed` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`voucher_items_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `voucher_items`
--

INSERT INTO `voucher_items` (`voucher_items_id`, `CustomerId`, `vouchers_id`, `voucher_code`, `dateawarded`, `redeemed`, `status`) VALUES
(1, 1, 1, 'A937YL6X', '2013-04-12 21:00:00', 0, 1),
(2, 2, 2, '1D2UH6LA', '2013-04-12 21:00:00', 0, 1),
(3, 2, 1, '2R1JN8K9', '2013-04-12 21:00:00', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
