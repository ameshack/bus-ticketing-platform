-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2013 at 11:08 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tms`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
  `Agent_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Agent_First_Name` varchar(100) DEFAULT NULL,
  `Agent_Last_Name` varchar(50) DEFAULT NULL,
  `Agent_Company` varchar(30) DEFAULT NULL,
  `Agent_Mobile_No` varchar(20) DEFAULT NULL,
  `Agent_Fax_No` varchar(20) DEFAULT NULL,
  `Agent_Country` varchar(50) DEFAULT NULL,
  `Agent_City` varchar(50) DEFAULT NULL,
  `Agent_Website` varchar(50) DEFAULT NULL,
  `Agent_E_mail` varchar(50) DEFAULT NULL,
  `Agent_Street_Address` varchar(50) DEFAULT NULL,
  `Agent_Postal_Address` varchar(50) DEFAULT NULL,
  `pinno` int(4) DEFAULT NULL,
  `discountamount` double NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Agent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`Agent_id`, `Agent_First_Name`, `Agent_Last_Name`, `Agent_Company`, `Agent_Mobile_No`, `Agent_Fax_No`, `Agent_Country`, `Agent_City`, `Agent_Website`, `Agent_E_mail`, `Agent_Street_Address`, `Agent_Postal_Address`, `pinno`, `discountamount`, `status`) VALUES
(1, 'Evanson', 'Biwott', 'Evanson Permanganate Ventures', '254723680311', '', 'Kenya', 'Nairobi', '', 'evansonbiwot@gmail.com', 'Nairobi', 'private bag', NULL, 0, 1),
(2, 'qweq', 'qweqw', 'aqwa', '254723680311', '', 'Kenya', 'Nairobi', '', 'evansonbiwott@gmail.com', 'qwda', '', NULL, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
