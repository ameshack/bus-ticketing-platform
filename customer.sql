-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2013 at 04:43 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tms`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `Cust_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Cust_First_Name` varchar(100) DEFAULT NULL,
  `Cust_Last_Name` varchar(50) DEFAULT NULL,
  `Cust_Company` varchar(30) DEFAULT NULL,
  `Cust_Mobile_No` varchar(20) DEFAULT NULL,
  `Cust_Fax_No` varchar(20) DEFAULT NULL,
  `Cust_Country` varchar(50) DEFAULT NULL,
  `Cust_City` varchar(50) DEFAULT NULL,
  `Cust_Website` varchar(50) DEFAULT NULL,
  `Cust_E_mail` varchar(50) DEFAULT NULL,
  `Cust_Street_Address` varchar(50) DEFAULT NULL,
  `Cust_Postal_Address` varchar(50) DEFAULT NULL,
  `pin` int(4) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Cust_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`Cust_id`, `Cust_First_Name`, `Cust_Last_Name`, `Cust_Company`, `Cust_Mobile_No`, `Cust_Fax_No`, `Cust_Country`, `Cust_City`, `Cust_Website`, `Cust_E_mail`, `Cust_Street_Address`, `Cust_Postal_Address`, `pin`, `status`) VALUES
(1, 'Happy', 'Ngumbao', 'Ideal Ceramics', '254718000008', 'N/A', 'Kenya', 'Mombasa', 'na', 'happy@idealceramics.com', 'mombasa', 'na', NULL, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
